package com.app.inn.peepsin;

import static com.app.inn.peepsin.Constants.Constants.kEmptyString;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

import java.util.Calendar;
import java.util.Date;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {

  @Test
  public void addition_isCorrect() throws Exception {
    assertEquals(4, 2 + 2);
  }


  @Test
  public void lastName_isCorrect() throws Exception {
    String name = "aka Mei tai le ka";
    String[] names = name.split(" ");
    StringBuilder lastName = new StringBuilder(kEmptyString);
    if (names.length > 1) {
      for (int i = 1; i < names.length; i++) {
        lastName.append(" ").append(names[i]);
      }
    }

    assertEquals("Mei tai le ka", lastName.toString().trim());
  }

  @Test
  public void firtName_isCorrect() throws Exception {
    String name = "";
    String[] names = name.split(" ");
    String firstName;
    if (names.length == 0) {
      firstName = kEmptyString;
    } else {
      firstName = names[0];
    }

    assertEquals("", firstName.trim());
  }


  @Test
  public void timpstampTesting() throws Exception {
   String timestamp = Calendar.getInstance().getTime().toString();
   Date date = Calendar.getInstance().getTime();
   assertEquals(1,1);
  }

  public static String getFirstName(String[] names) {
    String firstName;
    if (names.length == 0) {
      firstName = kEmptyString;
    } else {
      firstName = names[0];
    }
    return firstName;
  }


}