package com.app.inn.peepsin.UI.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.inn.peepsin.BuildConfig;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Libraries.Firebase.utils.NotificationUtils;
import com.app.inn.peepsin.Managers.BaseManager.ApplicationManager;
import com.app.inn.peepsin.Managers.BaseManager.DateManager;
import com.app.inn.peepsin.Managers.BaseManager.ExceptionHandler;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Managers.XMPPManager.Constant;
import com.app.inn.peepsin.Managers.XMPPManager.RoosterManager;
import com.app.inn.peepsin.Models.ChatProduct;
import com.app.inn.peepsin.Models.CurrentUser;
import com.app.inn.peepsin.Models.Message;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.adapters.ChatAdapter;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kMessage;
import static com.app.inn.peepsin.Constants.Constants.kRecords;
import static com.app.inn.peepsin.Constants.Constants.kType;

public class ChatActivity extends AppCompatActivity {

    @BindView(R.id.recycler_view_chat)
    RecyclerView recyclerViewChat;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_user_image)
    ImageView ivUserImage;
    @BindView(R.id.iv_item)
    ImageView ivProductImage;
    @BindView(R.id.tv_name)
    TextView tvProductName;
    @BindView(R.id.tv_price)
    TextView tvProductPrice;
    @BindView(R.id.edt_message)
    EditText edtMessage;
    @BindView(R.id.empty_view)
    View emptyView;
    @BindView(R.id.view_product)
    View productView;

    private ChatAdapter chatAdapter;
    private ChatProduct chatUser;
    private ChatType mChatType;
    private BroadcastReceiver mRegistrationBroadcastReceiver;

    public static Intent getIntent(Context context, ChatProduct product, ChatType type) {
        Intent intent = new Intent(context, ChatActivity.class);
        intent.putExtra(kRecords, product);
        intent.putExtra(kType, type);
        return intent;
    }

    public enum ChatType {
        USER_CHAT,
        PRODUCT_CHAT,
        MAKE_OFFER
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);

        if (!BuildConfig.DEBUG) {
            // do something for a debug build
            Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        }

        Intent intent = getIntent();
        if (intent != null) {
            chatUser = (ChatProduct) intent.getSerializableExtra(kRecords);
            mChatType = (ChatType) intent.getSerializableExtra(kType);
            if (chatUser != null) {
                setupProductRow(chatUser);
            } else {
                productView.setVisibility(View.GONE);
            }
        }
        String senderName = chatUser.getUserName();
        chatAdapter = new ChatAdapter(new ArrayList<>(), senderName);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerViewChat.setLayoutManager(layoutManager);
        recyclerViewChat.setAdapter(chatAdapter);
        edtMessage.requestFocus();
        edtMessage.setOnEditorActionListener(onEditorActionListener);
        handleBroadcast();

        if (mChatType.equals(ChatType.MAKE_OFFER)) {
            sendMessage("Hi, i want to buy your product");
        } else if (mChatType.equals(ChatType.USER_CHAT)) {
            productView.setVisibility(View.GONE);
        }

        loadData();
    }

    private void loadData() {
        chatAdapter.addChats(chatUser.getMessageList());
        chatAdapter.notifyDataSetChanged();
    }

    private void setupProductRow(ChatProduct product) {
        String userName = product.getUserName();
        String productName = product.getProductName();
        String price = getString(R.string.Rs) + " " + product.getPrice();
        String productImageURL = product.getProductThumbnailURL();
        String userImageURL = product.getUserImageURL();
        userImageURL = (userImageURL == null) ? "" : userImageURL;
        productImageURL = (productImageURL == null) ? "" : productImageURL;
        tvTitle.setText(userName);
        if (!userImageURL.isEmpty()) {
            Picasso.with(this)
                    .load(userImageURL)
                    .resize(100, 100)
                    .into(ivUserImage);
        } else {
            ivUserImage
                    .setImageDrawable(getResources().getDrawable(R.drawable.img_profile_placeholder));
        }

        if (!productImageURL.isEmpty()) {
            Picasso.with(this)
                    .load(productImageURL)
                    .resize(100, 100)
                    .into(ivProductImage);
        }
        tvProductName.setText(productName);
        tvProductPrice.setText(price);
    }

    EditText.OnEditorActionListener onEditorActionListener = (v, actionId, event) -> {
        if (actionId == EditorInfo.IME_ACTION_SEND) {
            send();
            return true;
        }
        return false;
    };


    @OnClick(R.id.iv_send)
    void send() {
        String msgTxt = edtMessage.getText().toString().trim();
        if (msgTxt.isEmpty()) {
            return;
        }
        sendMessage(msgTxt);
    }

    private void sendMessage(String msgTxt) {
        ChatProduct chatProduct = makeMessage(msgTxt);
        RoosterManager.roost().sendMessage(chatProduct, (Constants.Status iStatus) -> {
            List<Message> messageList = chatProduct.getMessageList();
            Message message = messageList.get(messageList.size() - 1);
            if (iStatus == Constants.Status.success) {
                message.setStatus(Constants.MessageStatus.send.getValue().toString());
            } else {
                Toaster.toast("PeepsIn Chat Serveris  not responding.");
                message.setStatus(Constants.MessageStatus.error.getValue().toString());
            }
            chatAdapter.addItem(message);
            edtMessage.setText("");
            recyclerViewChat.smoothScrollToPosition(chatAdapter.getItemCount());
            checkEmptyView();
        });
    }

    private ChatProduct makeMessage(String msgTxt) {
        ChatProduct chatProduct = new ChatProduct();
        CurrentUser user = ModelManager.modelManager().getCurrentUser();
        chatProduct.setJabberId(chatUser.getJabberId());
        chatProduct.setUserId(user.getUserId().toString());
        chatProduct.setUserName(chatUser.getUserName());
        chatProduct.setUserImageURL(chatUser.getUserImageURL());
        chatProduct.setChatOn(chatUser.getChatOn());
        chatProduct.setConnectionLevel(chatUser.getConnectionLevel());
        chatProduct.setProductId(chatUser.getProductId());
        chatProduct.setProductName(chatUser.getProductName());
        chatProduct.setProductThumbnailURL(chatUser.getProductThumbnailURL());
        chatProduct.setPrice(chatUser.getPrice());
        Message message = new Message();
        message.setBody(msgTxt);
        message.setDirection(Constants.MessageDirection.send.getValue().toString());
        message.setStatus(Constants.MessageStatus.sending.getValue().toString());
        message.setTime(DateManager.getCurrentTimestamp().toString());
        chatProduct.getMessageList().add(message);
        return chatProduct;
    }

    private void handleBroadcast() {
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(Constant.kIncomingMessage)) {
                    ChatProduct chatProduct = (ChatProduct) intent.getSerializableExtra(kMessage);
                    if (chatProduct.getUniqueId().equals(chatUser.getUniqueId())) {
                        chatAdapter.addItem(chatProduct.getLastMessage());
                        recyclerViewChat.smoothScrollToPosition(chatAdapter.getItemCount());
                    } else {
                        showNotificationMessage(chatProduct);
                    }
                }
            }
        };
    }

    private void showNotificationMessage(ChatProduct chatProduct) {
        NotificationUtils notificationUtils = new NotificationUtils(ApplicationManager.getContext());
        String username = chatProduct.getUserName();
        String messageBody = chatProduct.getLastMessage().getBody();
        String time = chatProduct.getLastMessage().getTime();
        notificationUtils.showNotificationMessage(username, messageBody, time,
                SplashActivity.getIntent(ApplicationManager.getContext()));
    }


    private void checkEmptyView() {
        if (chatAdapter.getItemCount() > 0) {
            emptyView.setVisibility(View.GONE);
        } else {
            emptyView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
    }

    @OnClick(R.id.btn_back)
    void onBack() {
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Constant.kIncomingMessage));
    }
}
