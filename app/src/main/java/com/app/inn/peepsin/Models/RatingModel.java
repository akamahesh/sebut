package com.app.inn.peepsin.Models;

import org.json.JSONObject;

/**
 * Created by root on 30/6/17.
 */

public class RatingModel extends BaseModel {

    private Integer productId;
    private String rating;
    private Integer totalRating;
    private Integer myRating;
    private String myReview;
    private Integer firstProgress;
    private Integer secondProgress;
    private Integer thirdProgress;
    private Integer forthProgress;
    private Integer fifthProgress;

    public RatingModel(JSONObject jsonResponse){
        this.productId = getValue(jsonResponse, kProductId, Integer.class);
        this.rating = getValue(jsonResponse,kRating,String.class);
        this.totalRating = getValue(jsonResponse, kTotalRatingCount,Integer.class);
        this.myRating = getValue(jsonResponse,kMyRating,Integer.class);
        this.myReview = getValue(jsonResponse,kMyReview,String.class);
        ratingProgress(getValue(jsonResponse,kSeperateRatingReview,JSONObject.class));
    }

    private void ratingProgress(JSONObject jsonResponse){
        this.firstProgress  = getValue(jsonResponse,kFirstRating,Integer.class);
        this.secondProgress = getValue(jsonResponse,kSecondRating,Integer.class);
        this.thirdProgress  = getValue(jsonResponse,kThirdRating,Integer.class);
        this.forthProgress  = getValue(jsonResponse,kFourthRating,Integer.class);
        this.fifthProgress  = getValue(jsonResponse,kFifthRating,Integer.class);
    }

    public Integer getProductId() {
        return productId;
    }

    public String getRating() {
        return rating;
    }

    public Integer getTotalRating() {
        return totalRating;
    }

    public Integer getMyRating() {
        return myRating;
    }

    public String getMyReview() {
        return myReview;
    }

    public Integer getFirstProgress() {
        return firstProgress;
    }

    public Integer getSecondProgress() {
        return secondProgress;
    }

    public Integer getThirdProgress() {
        return thirdProgress;
    }

    public Integer getForthProgress() {
        return forthProgress;
    }

    public Integer getFifthProgress() {
        return fifthProgress;
    }
}
