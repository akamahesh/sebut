package com.app.inn.peepsin.UI.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.inn.peepsin.Models.Feeds;
import com.app.inn.peepsin.Models.ShopCategory;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.fragments.Feeds.FeedsFragment;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Harsh on 7/3/2017.
 */

public class SellingStuffInventroryAdapter extends RecyclerView.Adapter<SellingStuffInventroryAdapter.FeedInventoryViewHolder> {

    private List<ShopCategory> inventoryList;
    Context context;

    private SellingStuffInventroryAdapter(List<ShopCategory> list, Context context) {
        this.inventoryList = list;
        this.context = context;
    }

    @Override
    public SellingStuffInventroryAdapter.FeedInventoryViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_feeds_inventrory_base, viewGroup, false);
        return new SellingStuffInventroryAdapter.FeedInventoryViewHolder(v);
    }

    @Override
    public void onBindViewHolder(SellingStuffInventroryAdapter.FeedInventoryViewHolder holder, int position) {
        ShopCategory shop = inventoryList.get(position);
        holder.bindContent(shop);
        Picasso.with(context)
                .load(shop.getImageUrl())
                .placeholder(R.drawable.img_feeds_particular_item)
                .into(holder.ivItemImage);
        holder.tvItemName.setText(inventoryList.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return inventoryList.size();
    }

    public void addItems(CopyOnWriteArrayList<ShopCategory> list){
        inventoryList.addAll(list);
        notifyDataSetChanged();
    }

    class FeedInventoryViewHolder extends RecyclerView.ViewHolder{
        ShopCategory item;

        @BindView(R.id.iv_item_image)
        ImageView ivItemImage;
        @BindView(R.id.tv_item_name)
        TextView tvItemName;

        FeedInventoryViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        @OnClick(R.id.item_view)
        void onInventory(){
            //loadFilterList(item.getId());
        }

        public void bindContent(ShopCategory shop) {
            this.item = shop;
        }


    }
}
