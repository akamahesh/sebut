package com.app.inn.peepsin.Models;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by dhruv on 24/8/17.
 */

public class ProductInventory extends BaseModel implements Serializable {

    private Integer productId;
    private String SKUNumber;
    private String price;
    private String sellingPrice;
    private String netPrice;
    private Integer quantity;
    private String discountTagLine;

    public ProductInventory(JSONObject records) {
        this.productId    = getValue(records,       kProductId,           Integer.class);
        this.SKUNumber    = getValue(records,       kSKUNumber,           String.class);
        this.price        = getValue(records,       kPrice,               String.class);
        this.sellingPrice = getValue(records,       kSellingPrice,        String.class);
        this.netPrice     = getValue(records,       kNetPrice,            String.class);
        this.quantity     = getValue(records,       kQuantity,            Integer.class);
        this.discountTagLine  = getValue(records,   kDiscountTagLine,     String.class);
    }

    public Integer getProductId() {
        return productId;
    }

    public String getSKUNumber() {
        return SKUNumber;
    }

    public String getPrice() {
        return price;
    }

    public String getSellingPrice() {
        return sellingPrice;
    }

    public String getNetPrice() {
        return netPrice;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public String getDiscountTagLine() {
        return discountTagLine;
    }
}
