package com.app.inn.peepsin.UI.fragments.Permissions;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.helpers.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.Manifest.permission.CAMERA;


public class CameraPermissionFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private final int REQUEST_PERMISSION_CAMERA = 102;
    @BindView(R.id.btn_grant_access) Button btnGrantAccess;

    public CameraPermissionFragment() {
        // Required empty public constructor
    }

    public static CameraPermissionFragment newInstance() {
        return new CameraPermissionFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_camera_permission, container, false);
        ButterKnife.bind(this, view);

        if (ContextCompat.checkSelfPermission(getActivity(), CAMERA) == PackageManager.PERMISSION_GRANTED) {
            btnGrantAccess.setText(getString(R.string.permission_granted));
            btnGrantAccess.setEnabled(false);
        }
        return view;
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @OnClick(R.id.btn_grant_access)
    void grantAccess(Button button) {
        if (ContextCompat.checkSelfPermission(getActivity(), CAMERA) == PackageManager.PERMISSION_GRANTED) {
            mListener.onSkip(4);
        } else if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), CAMERA)) {
            // We've been denied once before. Explain why we need the permission, then ask again.
            Utils.showDialog(getContext(),
                    getString(R.string.camera_permission_required_text),
                    getString(R.string.ask_permission_text),
                    getString(R.string.discard_text),
                    (dialog, which) -> {
                        if (which == -1)
                            requestPermissions(new String[]{CAMERA}, REQUEST_PERMISSION_CAMERA);
                        else
                            dialog.dismiss();
                    });
        } else {
            // We've never asked. Just do it.
            requestPermissions(new String[]{CAMERA}, REQUEST_PERMISSION_CAMERA);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PERMISSION_CAMERA && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            mListener.onSkip(4);
        } else {
            // We were not granted permission this time, so don't try to show the contact picker
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public interface OnFragmentInteractionListener {
        void onSkip(int code);
    }
}
