package com.app.inn.peepsin.Managers.UploadManager;

import android.util.Log;

import com.app.inn.peepsin.Constants.Blocks.Block;
import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Libraries.DispatchQueue.DispatchQueue;
import com.app.inn.peepsin.Managers.APIManager.APIManager;
import com.app.inn.peepsin.Managers.BaseManager.ApplicationManager;
import com.app.inn.peepsin.Managers.BaseManager.DateManager;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.BaseModel;
import com.app.inn.peepsin.UI.helpers.Utils;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import static com.app.inn.peepsin.Constants.Constants.kCoverImageUploadId;
import static com.app.inn.peepsin.Constants.Constants.kImageUploadId1;
import static com.app.inn.peepsin.Constants.Constants.kImageUploadId2;
import static com.app.inn.peepsin.Constants.Constants.kImageUploadId3;
import static com.app.inn.peepsin.Constants.Constants.kImageUploadId4;
import static com.app.inn.peepsin.Constants.Constants.kImageUploadId5;
import static com.app.inn.peepsin.Constants.Constants.kMedia;
import static com.app.inn.peepsin.Constants.Constants.kMediaIndex;
import static com.app.inn.peepsin.Constants.Constants.kMediaType;
import static com.app.inn.peepsin.Constants.Constants.kMediaUploadId;
import static com.app.inn.peepsin.Constants.Constants.kMessageInternalInconsistency;
import static com.app.inn.peepsin.Constants.Constants.kRequestId;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kUploadMediaToServer;

/**
 * Created by akaMahesh on 5/7/17.
 * copyright to : Innverse Technologies
 */

public class UploadManager implements UploadHelper{
    private final static String TAG = UploadManager.class.getSimpleName();
    private static UploadManager _UploadManager;
    private CopyOnWriteArrayList<HashMap<String,Object>> hashMapList = new CopyOnWriteArrayList<>();

    public static UploadManager uploadManager(){
        if(_UploadManager==null)
            _UploadManager = new UploadManager();
        return _UploadManager;
    }

    private UploadManager() {

    }
    public void upload(List<String> filePathList, Block.Completion<HashMap<String,Object>> completion) {
        counter = 0;
        final String timestamp = DateManager.getCurrentTimestamp().toString();
        DispatchQueue.global(DispatchQueue.QoS.userInteractive).async(()->{
            List<HashMap<String,Object>> hashMapList = new ArrayList<>();
            for (int i =0; i<filePathList.size();i++) {
                HashMap<String,Object> parameters = new HashMap<>();
                String filepath = filePathList.get(i);
                File imageFile = new File(filepath);
                parameters.put(kMedia, imageFile);
                parameters.put(kMediaType, Constants.MediaType.image.getValue());
                parameters.put(kRequestId, timestamp);
                parameters.put(kMediaIndex, i+1);
                uploadMediaToServer(parameters,(Constants.Status iStatus, GenricResponse<HashMap<String,Object>> genricResponse) -> {
                    ++counter;
                    HashMap<String,Object> hashMap = genricResponse.getObject();
                    hashMapList.add(hashMap);
                    if(filePathList.size()==counter){
                        HashMap<String,Object> partListMap = new HashMap<>();
                        for (HashMap<String,Object> hashMap1 : hashMapList){
                            Integer mediaIndex = (Integer) hashMap1.get(kMediaIndex);
                            partListMap.put(kRequestId,hashMap1.get(kRequestId));
                            switch (mediaIndex){
                                case 1:
                                    partListMap.put(kCoverImageUploadId,hashMap1.get(kMediaUploadId));
                                    break;
                                case 2:
                                    partListMap.put(kImageUploadId1,hashMap1.get(kMediaUploadId));
                                    break;
                                case 3:
                                    partListMap.put(kImageUploadId2,hashMap1.get(kMediaUploadId));
                                    break;
                                case 4:
                                    partListMap.put(kImageUploadId3,hashMap1.get(kMediaUploadId));
                                    break;
                                case 5:
                                    partListMap.put(kImageUploadId4,hashMap1.get(kMediaUploadId));
                                    break;
                                case 6:
                                    partListMap.put(kImageUploadId5,hashMap1.get(kMediaUploadId));
                                    break;
                            }
                        }

                        GenricResponse<HashMap<String,Object>> genricResponse1 = new GenricResponse<>(partListMap);
                        DispatchQueue.main(() -> completion.iCompletion(Constants.Status.success,genricResponse1));
                    }
                }, (Constants.Status iStatus, String message) -> {
                    ++counter;
                    if(filePathList.size()==counter){
                        DispatchQueue.main(() -> completion.iCompletion(Constants.Status.fail,null));
                    }
                    Log.v(TAG,"uploadMediaToServer failure  "+ counter+"message : "+message);
                });
            }
        });
    }


    private Integer counter;
    private Integer successCounter;
    private String requestId="-1";
    public void editUploadImage(HashMap<String,Object> hashMap, Block.Completion<HashMap<String,Object>> completion) {
        counter = 0;
        successCounter = 0;

        final String timestamp = DateManager.getCurrentTimestamp().toString();
        DispatchQueue.global(DispatchQueue.QoS.userInteractive).async(()->{
            for(String key:hashMap.keySet()){
                String filepath = (String) hashMap.get(key);
                File imageFile = Utils.getFile(filepath);
                HashMap<String,Object> parameters = new HashMap<>();
                parameters.put(kMedia, (imageFile==null)?"":imageFile);
                parameters.put(kMediaType, Constants.MediaType.image.getValue());
                parameters.put(kRequestId, timestamp);
                parameters.put(kMediaIndex, ++counter);
                uploadMediaToServer(parameters,(Constants.Status iStatus, GenricResponse<HashMap<String,Object>> genricResponse) -> {
                    successCounter++;
                    HashMap<String,Object> genericHashMap = genricResponse.getObject();
                    Integer mediaUploadId = (Integer) genericHashMap.get(kMediaUploadId);
                    requestId  = (String) genericHashMap.get(kRequestId);
                    hashMap.put(key,mediaUploadId);
                    if(successCounter==hashMap.size()){
                        hashMap.put(kRequestId,requestId);
                        GenricResponse<HashMap<String,Object>> genricResponse1 = new GenricResponse<>(hashMap);
                        DispatchQueue.main(() -> completion.iCompletion(Constants.Status.success,genricResponse1));
                    }
                }, (Constants.Status iStatus, String message) -> {
                    successCounter++;
                    if(message.equals("media should not be empty."))
                        hashMap.put(key,-1);
                    if(successCounter==hashMap.size()){
                        hashMap.put(kRequestId,requestId);
                        GenricResponse<HashMap<String,Object>> genricResponse1 = new GenricResponse<>(hashMap);
                        DispatchQueue.main(() -> completion.iCompletion(Constants.Status.fail,genricResponse1));
                    }
                    Log.v(TAG,"uploadMediaToServer failure  "+ successCounter+"message : "+message);
                });
            }
        });
    }


    /**
     * method  will used to add the upload the media (image, audio, video) to AWS server.
     * App server will create images and upload them to media server like AWS S3 bucket.
     * And return the mediaUploadId for that media, which passon in further api.
     * @param parameters hashmap contains request parameters
     * @param success  Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void uploadMediaToServer(HashMap<String,Object> parameters, Block.Success<HashMap<String,Object >> success, Block.Failure failure) {
        DispatchQueue.global(DispatchQueue.QoS.userInteractive).async(() -> {
            parameters.put(BaseModel.kAuthToken, ModelManager.modelManager().getCurrentUser().getAuthToken());
            APIManager.APIManager().processFormRequest(kUploadMediaToServer, parameters, (Constants.Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                try {
                    JSONObject jsonObject = genricResponse.getObject();
                    HashMap<String,Object> hashMap = new HashMap<>();
                    Integer mediaUploadId   = (Integer) jsonObject.get(kMediaUploadId);
                    Integer mediaType       = (Integer) jsonObject.get(kMediaType);
                    String requestId       = (String) jsonObject.get(kRequestId);
                    Object mediaIndex      = jsonObject.get(kMediaIndex);

                    hashMap.put(kMediaUploadId,mediaUploadId);
                    hashMap.put(kMediaType,mediaType);
                    hashMap.put(kRequestId,requestId);
                    hashMap.put(kMediaIndex,Integer.parseInt(mediaIndex.toString()));

                    GenricResponse<HashMap<String,Object>> genricResponseMap = new GenricResponse<>(hashMap);
                    success.iSuccess(iStatus,genricResponseMap);
                } catch (Exception e) {
                    e.printStackTrace();
                    failure.iFailure(iStatus, kMessageInternalInconsistency);
                }
            }, (Constants.Status statusFail, String message) -> {
                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
            });
        });
    }
/*
    HashMap<String,Object> parameters = new HashMap<>();
                parameters.put(kMedia,getFile());
                parameters.put(kMediaType, Constants.MediaType.image.getValue());
                parameters.put(kRequestId, DateManager.getCurrentTimestamp().toString());

                ModelManager.modelManager().uploadMediaToServer(parameters,(Constants.Status iStatus) -> {
        Log.e(TAG,iStatus.toString());
        ++imageCounter;
        if(imageCounter==pathList.size())
            Toaster.toast(imageCounter+" Image uploaded successfully");
    }, (Constants.Status iStatus, String message) -> {
        Log.e(TAG,message);
        ++imageCounter;
    });*/
}
