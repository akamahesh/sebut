package com.app.inn.peepsin.Libraries.Firebase.service;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.app.inn.peepsin.Libraries.Firebase.app.Config;
import com.app.inn.peepsin.Libraries.Firebase.utils.NotificationUtils;
import com.app.inn.peepsin.UI.activities.SplashActivity;
import com.app.inn.peepsin.UI.helpers.Utils;

import org.json.JSONObject;

import static com.app.inn.peepsin.Constants.Constants.kBody;
import static com.app.inn.peepsin.Constants.Constants.kProductCategoryId;
import static com.app.inn.peepsin.Constants.Constants.kProductSubCategoryId;
import static com.app.inn.peepsin.Constants.Constants.kTitle;
import static com.app.inn.peepsin.Constants.Constants.kType;
import static com.app.inn.peepsin.Constants.Constants.kUserId;


/**
 * Created by mahesh on 23/3/17.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "PUSH";

    private NotificationUtils notificationUtils;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        if (remoteMessage == null)
            return;

        try {
            handleMyDataMessage(remoteMessage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void handleMyDataMessage(RemoteMessage remoteMessage) throws Exception {

        String body = remoteMessage.getNotification().getBody();
        String title = remoteMessage.getNotification().getTitle();
        Log.v(TAG," body "+body);
        Log.v(TAG," title "+title);

       /* JSONObject dataJSON = null;
        try {
            dataJSON = new JSONObject(remoteMessage.getData().toString());
            Log.v(TAG, "FCM ==>" + "remoteMessage to jsonObj  "+dataJSON.toString(4));
        }catch (Exception e){
            e.printStackTrace();
        }

        Integer type                    = null;
        Integer productCategoryId = null;
        Integer productSubcategoryId = null;
        Integer userId = null;

        if (dataJSON != null) {
            type                    = convertIntoInteger(dataJSON.getString(kType));
            productCategoryId       = convertIntoInteger(dataJSON.getString(kProductCategoryId));
            productSubcategoryId    = convertIntoInteger(dataJSON.getString(kProductSubCategoryId));
            userId                  = convertIntoInteger(dataJSON.getString(kUserId));
        }*/

        if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
            // app is in foreground, broadcast the push message
            Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
            pushNotification.putExtra(kBody,body);
            pushNotification.putExtra(kTitle,title);

            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

            // play notification sound
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.playNotificationSound();

        } else {
            Intent resultIntent = SplashActivity.getIntent(getApplicationContext());
            showNotificationMessage(getApplicationContext(), title, body, Utils.generateTimeStamp(), resultIntent);
        }


    }

    private Integer convertIntoInteger(String value){
        Integer intvalue = 0;
        try {
            intvalue = Integer.parseInt(value);
        }catch (Exception e){
            e.printStackTrace();
        }
        return intvalue;
    }


    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }


    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }


}
