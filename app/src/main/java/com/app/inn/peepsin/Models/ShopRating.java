package com.app.inn.peepsin.Models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by dhruv on 1/8/17.
 */

public class ShopRating extends BaseModel implements Serializable {

    private Integer shopId;
    private String rating;
    private Integer totalRating;
    private Integer totalRecords;
    private Integer firstProgress;
    private Integer secondProgress;
    private Integer thirdProgress;
    private Integer forthProgress;
    private Integer fifthProgress;
    private CopyOnWriteArrayList<ShopReview> shopReviewList;

    public ShopRating(JSONObject jsonResponse){
        this.shopId = getValue(jsonResponse, kProductId, Integer.class);
        this.rating = getValue(jsonResponse,kRating,String.class);
        this.totalRecords = getValue(jsonResponse,kTotalRecords,Integer.class);
        this.totalRating = getValue(jsonResponse, kTotalRatingCount,Integer.class);
        ratingProgress(getValue(jsonResponse,kSeperateRatingReview,JSONObject.class));
        try{
            this.shopReviewList = handleReviewList(getValue(jsonResponse, kRecords, JSONArray.class));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void ratingProgress(JSONObject jsonResponse){
        this.firstProgress  = getValue(jsonResponse,kFirstRating,Integer.class);
        this.secondProgress = getValue(jsonResponse,kSecondRating,Integer.class);
        this.thirdProgress  = getValue(jsonResponse,kThirdRating,Integer.class);
        this.forthProgress  = getValue(jsonResponse,kFourthRating,Integer.class);
        this.fifthProgress  = getValue(jsonResponse,kFifthRating,Integer.class);
    }

    private CopyOnWriteArrayList<ShopReview> handleReviewList(JSONArray jsonArray) throws JSONException {
        CopyOnWriteArrayList<ShopReview> shopReviewList = new CopyOnWriteArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            shopReviewList.add(new ShopReview(jsonArray.getJSONObject(i)));
        }
        return shopReviewList;
    }


    public Integer getShopId() {
        return shopId;
    }

    public String getRating() {
        return rating;
    }

    public Integer getTotalRating() {
        return totalRating;
    }

    public Integer getTotalRecords() {
        return totalRecords;
    }

    public Integer getFirstProgress() {
        return firstProgress;
    }

    public Integer getSecondProgress() {
        return secondProgress;
    }

    public Integer getThirdProgress() {
        return thirdProgress;
    }

    public Integer getForthProgress() {
        return forthProgress;
    }

    public Integer getFifthProgress() {
        return fifthProgress;
    }

    public CopyOnWriteArrayList<ShopReview> getShopReviewList() {
        return shopReviewList;
    }
}
