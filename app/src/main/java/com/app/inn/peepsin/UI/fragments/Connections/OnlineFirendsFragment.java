package com.app.inn.peepsin.UI.fragments.Connections;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.Connection;
import com.app.inn.peepsin.Models.CurrentUser;
import com.app.inn.peepsin.Models.People;
import com.app.inn.peepsin.R;

import com.app.inn.peepsin.UI.fragments.Profile.UserProfileFragment;

import com.app.inn.peepsin.UI.helpers.DividerItemRecyclerDecoration;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kFlag;
import static com.app.inn.peepsin.Constants.Constants.kUserId;

/**
 * Created by mahesh on 6/4/17.
 */

public class OnlineFirendsFragment extends Fragment {
    private final String TAG = getClass().getSimpleName();
    private List<People> userList;
    private PeopleAdapter peopleAdapter;

    @BindView(R.id.empty_view) View emptyView;
    @BindView(R.id.tv_title) TextView toolbarTitle;
    @BindView(R.id.recycler_view_people) RecyclerView recyclerView;
    /* @BindView(R.id.swipeRefreshLayout)
     SwipeRefreshLayout swipeRefreshLayout;*/
    @BindView(R.id.edt_search) EditText edtSearch;
    @BindView(R.id.tv_empty_title) TextView emptyTitle;
    @BindView(R.id.tv_empty_message) TextView emptyMessage;
    @BindView(R.id.btn_skip) Button btnSkip;
    @BindView(R.id.near_progressBar1)
    ProgressBar progressBar;
    private ProgressDialog progressDialog;
    private static Switcher switcherListener;
    private WrapContentLinearLayoutManager mLayoutManager;
    private int TYPE;
    private int mPage;
    private boolean loading;
    private int pageSize;
    private CurrentUser user;
    private String lat="",lng="";
    private String filterString;
    // AppBarLayout appbarLayout;

    //type will b1 to make btn skip visible
    public static Fragment newInstance(Switcher switcher, int type) {
        Fragment fragment =  new OnlineFirendsFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(kFlag,type);
        fragment.setArguments(bundle);
        switcherListener = switcher;
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = Utils.generateProgressDialog(getContext(), true);
        userList = new ArrayList<>();
        Bundle bundle = getArguments();
        if(bundle!=null)
            TYPE = bundle.getInt(kFlag);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_online_search, container, false);
        ButterKnife.bind(this, view);
       /* appbarLayout=((HomeActivity) getActivity()).getTabLayout();
        Utils.isfriendClicked=1;*/
        toolbarTitle.setText(getString(R.string.title_people_you_may_know));
        user = ModelManager.modelManager().getCurrentUser();
        try{
            lat = user.getUserLocation().getLatitude();
            lng = user.getUserLocation().getLongitude();
        }catch (Exception e){
            e.printStackTrace();
        }

        recyclerView.setHasFixedSize(true);
        mLayoutManager = new WrapContentLinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new DividerItemRecyclerDecoration(getActivity(), R.drawable.canvas_recycler_divider));
        peopleAdapter = new PeopleAdapter(userList);
        recyclerView.setAdapter(peopleAdapter);
        recyclerView.addOnScrollListener(onScrollListener);

        //swipeRefreshLayout.setOnRefreshListener(onRefreshListener);

        addTextListener(edtSearch);
        edtSearch.setOnEditorActionListener(actionListener);

        if(TYPE==1)
            btnSkip.setVisibility(View.VISIBLE);
        else
            btnSkip.setVisibility(View.INVISIBLE);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //loadData();
    }

    //SwipeRefreshLayout.OnRefreshListener onRefreshListener = this::refreshData;

    @OnClick(R.id.btn_back)
    void onBack() {
        Utils.hideKeyboard(getContext());
        getFragmentManager().popBackStack();
    }
    @Override
    public void onResume() {
        super.onResume();
    }


    public void initialState(){
        mPage=1;
        loading=true;
        pageSize = userList.size();
    }

    RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            if(dy > 0) //check for scroll down
            {
                int visibleItemCount = mLayoutManager.getChildCount();
                int totalItemCount = mLayoutManager.getItemCount();
                int firstVisibleItemPosition = mLayoutManager.findFirstVisibleItemPosition();

                if (loading)
                {
                    if ( (visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= pageSize) {
                        loading = false;
                        ++mPage;
                        //Do pagination.. i.e. fetch new data
                        //loadMoreData();
                    }
                }
            }
        }
    };

    public void loadData(String searchString) {
        //showProgress(true);
        progressBar.setVisibility(View.VISIBLE);
        ModelManager.modelManager().OnlineSearchFriends(1,searchString,(Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<People>> genericResponse) -> {
            userList = genericResponse.getObject();
            peopleAdapter.addUsers(userList);
            filterString = searchString.toString().toLowerCase();
            peopleAdapter.getFilter().filter(searchString);

            //showProgress(false);
            initialState();
            checkEmptyScreen();
            progressBar.setVisibility(View.GONE);

        }, (Constants.Status iStatus, String message) -> {
            Log.e(TAG, message);
            // showProgress(false);
            checkEmptyScreen();
        });
    }



    TextView.OnEditorActionListener actionListener = (v, actionId, event) -> {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            Utils.hideKeyboard(getContext());
        }
        return true;
    };

    private void addTextListener(EditText edtSearch) {
        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                s = s.toString().toLowerCase();
             /*   if(s.equals("")){
                    return;
                }*/
                loadData(String.valueOf(s));

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    private void checkEmptyScreen() {
        try {
            if (peopleAdapter.getItemCount() > 0) {
                emptyView.setVisibility(View.GONE);
            } else {
                emptyView.setVisibility(View.VISIBLE);
                emptyTitle.setText(getString(R.string.empty_title_online_title));
                emptyMessage.setText(getString(R.string.empty_online_message));
            }
        }catch (IllegalStateException e){
            e.printStackTrace();
        }
    }

    public void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) progressDialog.cancel();
        }
    }

   /* private void swipeProgress(boolean b) {
        if (b) {
            swipeRefreshLayout.setRefreshing(true);
        } else {
            swipeRefreshLayout.setRefreshing(false);
        }
    }*/

    class PeopleAdapter extends RecyclerView.Adapter<PeopleAdapter.MyConnectionViewHolder> {
        private List<People> peoples;
        private List<People> filterList;
        private ColorGenerator generator = ColorGenerator.MATERIAL;

        PeopleAdapter(List<People> userList) {
            this.peoples = userList;
            this.filterList = userList;
        }

        @Override
        public MyConnectionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(getContext()).inflate(R.layout.row_people_you_may_know_layout, parent, false);
            return new MyConnectionViewHolder(view);
        }

        @Override
        public void onBindViewHolder(MyConnectionViewHolder holder, int position) {
                People user = peoples.get(position);
                holder.bindContent(user);
                String name = user.getFullName();
                String rating = Integer.toString(user.getRating());
                String profileURL = user.getProfilePicURL();
                String suggestionTag = user.getSuggestionTag();
                String address = "No Location Available";
                if (user.getPrimaryAddress() != null) {
                    String city = user.getPrimaryAddress().getCity();
                    String state = user.getPrimaryAddress().getState();
                    String country = user.getPrimaryAddress().getCountry();
                    address = city + " , " + state + " , " + country;
                }

                holder.tvName.setText(name);
                holder.tvAddress.setText(address);
                holder.tvStatus.setText(suggestionTag);
                // holder.tvShopName.setText(skill);

                if (user.getShopName() != "") {
                    holder.vLayout.setVisibility(View.VISIBLE);
                    holder.tvShopName.setText("Shop: " + user.getShopName());
                } else holder.vLayout.setVisibility(View.INVISIBLE);

                holder.tvShopRating.setText(rating);


                if (profileURL.isEmpty()) {
                    holder.ivCircularUserImage.setVisibility(View.INVISIBLE);
                    holder.ivUserImage.setVisibility(View.VISIBLE);
                } else {
                    holder.ivCircularUserImage.setVisibility(View.VISIBLE);
                    holder.ivUserImage.setVisibility(View.INVISIBLE);
                }

                TextDrawable drawable = TextDrawable.builder().buildRound(String.valueOf(name.toUpperCase().charAt(0)), generator.getRandomColor());

                if (!profileURL.isEmpty()) {
                    Picasso.with(getContext())
                            .load(profileURL)
                            .placeholder(R.drawable.img_profile_placeholder)
                            .resize(100, 100)
                            .into(holder.ivCircularUserImage);
                } else {
                    holder.ivUserImage.setImageDrawable(drawable);
                }

                holder.checkUserStatus(user);

        }

        @Override
        public int getItemCount() {
            return peoples.size();
        }

        private Filter getFilter() {
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence charSequence) {

                    String charString = charSequence.toString();
                    if (charString.isEmpty()) {
                        peoples = filterList;
                    } else {
                        ArrayList<People> filteredList = new ArrayList<>();
                        for (People contacts : peoples) {
                            if (contacts.getFirstName().toLowerCase().contains(charString)) {
                                filteredList.add(contacts);
                            }
                        }
                        peoples = filteredList;
                    }
                    FilterResults filterResults = new FilterResults();
                    filterResults.values = peoples;
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                    peoples = (List<People>) filterResults.values;
                    notifyDataSetChanged();
                    checkEmptyScreen();
                }
            };
        }

        void addUsers(List<People> users) {
            peoples.clear();
            peoples.addAll(users);
            notifyDataSetChanged();
        }

        void addNewUsers(List<People> users) {
            peoples.addAll(users);
            notifyDataSetChanged();
        }

        class MyConnectionViewHolder extends RecyclerView.ViewHolder {
            private People people;
            @BindView(R.id.tv_name) TextView tvName;
            @BindView(R.id.tv_shopName) TextView tvShopName;
            @BindView(R.id.tv_address) TextView tvAddress;
            @BindView(R.id.tv_status) TextView tvStatus;
            @BindView(R.id.iv_user_image) ImageView ivUserImage;
            @BindView(R.id.iv_user_image_circular) ImageView ivCircularUserImage;
            @BindView(R.id.ll_layout) View vLayout;
            //     @BindView(R.id.tv_level) TextView tvConnectionLevel;
            @BindView(R.id.tv_shop_rating) TextView tvShopRating;
            @BindView(R.id.btn_add_friend) Button btnAddFriends;

            MyConnectionViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            void bindContent(People people){
                this.people = people;
            }

            @OnClick(R.id.btn_add_friend)
            void onAddFriend(){
                if(people.getUserConnectionStatus() == 1){
                }else if(people.getUserConnectionStatus() == 2){
                }else {sendFriendRequest(people);}
            }



            private void sendFriendRequest(People people){
                HashMap<String,Object> parameters = new HashMap<>();
                parameters.put(kUserId, people.getUserId());
                showProgress(true);
                ModelManager.modelManager().sendFriendRequest(parameters,(Constants.Status iStatus) -> {
                 /*   peoples.remove(people);
                    filterList.remove(people);*/
                    //peopleAdapter.notifyDataSetChanged();
                    btnAddFriends.setText("Request Sent");
                    btnAddFriends.setBackgroundResource(R.color.transparent);
                    //checkUserStatus(people);
                    //notifyDataSetChanged();
                    //  getFilter().filter("");
                    edtSearch.setText("");
                    checkEmptyScreen();
                    showProgress(false);
                }, (Constants.Status iStatus, String message) -> {
                    showProgress(false);
                    checkEmptyScreen();
                    Toaster.toast(message);
                });
            }

            @OnClick(R.id.item_view)
            void onItemView(){
                getUserProfile(people.getUserId());
            }



            public void checkUserStatus(People people){
                if(people.getUserConnectionStatus() == 1){
                    btnAddFriends.setText("Request Sent");
                    btnAddFriends.setBackgroundResource(R.color.transparent);
                }else if(people.getUserConnectionStatus() == 2){
                    btnAddFriends.setText("Friends");
                    btnAddFriends.setBackgroundResource(R.color.transparent);
                }else {
                    btnAddFriends.setText(" Add Friend ");
                    btnAddFriends.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.canvas_round_corner_red));
                }

            }

            void getUserProfile(int userId) {
                showProgress(true);
                HashMap<String, Object> parameters = new HashMap<>();
                parameters.put(kUserId,userId);
                ModelManager.modelManager().getUserProfile(parameters,(Constants.Status iStatus, GenricResponse<Connection> genricResponse) -> {
                    showProgress(false);
                    Connection connection = genricResponse.getObject();
                    if(connection.getProfileVisibility()!=0){
                        if(switcherListener!=null)
                            switcherListener.switchFragment(UserProfileFragment.newInstance(switcherListener,connection),true,true);
                    }else
                        Utils.showAlertDialog(getContext(),"Message","User Profile permission is denied");

                }, (Constants.Status iStatus, String message) -> {
                    Log.e("User Profile",message);
                    Toaster.toast(message);
                    showProgress(false);
                });
            }



        }
    }




    public class WrapContentLinearLayoutManager extends LinearLayoutManager {

        public WrapContentLinearLayoutManager(Context context) {
            super(context);
        }
        @Override
        public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
            try {
                super.onLayoutChildren(recycler, state);
            } catch (IndexOutOfBoundsException e) {
                Log.e("probe", "meet a IOOBE in RecyclerView");
            }
        }
    }

}

