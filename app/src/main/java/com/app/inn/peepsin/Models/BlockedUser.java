package com.app.inn.peepsin.Models;


import android.util.Log;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Harsh on 5/22/2017.
 */

public class BlockedUser extends BaseModel implements Serializable {
    private final String TAG = getClass().getSimpleName();
    private Integer userId;
    private String firstName;
    private String lastName;
    private String profilePicURL;
    private Address primaryAddress;
    private Boolean isUserBlocked;


    public BlockedUser(JSONObject jsonResponse) {
        this.userId         = getValue(jsonResponse, kUserId,       Integer.class);
        this.firstName      = getValue(jsonResponse, kFirstName,    String.class);
        this.lastName       = getValue(jsonResponse, kLastName,     String.class);
        this.profilePicURL  = getValue(jsonResponse, kProfilePicUrl, String.class);
        this.isUserBlocked  = getValue(jsonResponse, kIsUserBlocked, Boolean.class);

        try {
            this.primaryAddress = new Address(getValue(jsonResponse, kPrimaryAddress, JSONObject.class));
        } catch (Exception ex) {
            Log.e(TAG, ex.getMessage());
        }
    }

    public Integer getUserId() {
        return userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getProfilePicURL() {
        return profilePicURL;
    }

    public Address getPrimaryAddress() {
        return primaryAddress;
    }

    public String getTAG() {
        return TAG;
    }

    public Boolean getUserBlocked() {
        return isUserBlocked;
    }

}
