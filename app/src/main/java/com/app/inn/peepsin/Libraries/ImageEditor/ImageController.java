package com.app.inn.peepsin.Libraries.ImageEditor;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.net.Uri;
import android.provider.MediaStore;
import com.app.inn.peepsin.Managers.BaseManager.ApplicationManager;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

public class ImageController {

    public static File getImageFile(Context context,Bitmap inImage) {
        String fileName = "image" + inImage.getGenerationId();
        FileOutputStream fos = null;
        File file = null;
        try {
            file = new File(context.getCacheDir(), fileName);
            if (file.createNewFile()) {
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                inImage.compress(Bitmap.CompressFormat.PNG, 0, bos);
                byte[] bitmapdata = bos.toByteArray();

                fos = new FileOutputStream(file);
                fos.write(bitmapdata);
                fos.flush();
                fos.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return file;
    }


    public static Uri getImageUri(Context context,Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(CompressFormat.PNG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);

    }

}
