package com.app.inn.peepsin.UI.fragments.Board;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.DataBase.DataBaseHandler;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.Board;
import com.app.inn.peepsin.Models.CurrentUser;
import com.app.inn.peepsin.Models.MyConnections;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.activities.AddBoardActivity;
import com.app.inn.peepsin.UI.activities.HomeActivity;
import com.app.inn.peepsin.UI.adapters.BoardAdapter;
import com.app.inn.peepsin.UI.fragments.Feeds.CustomBottomSheetDialogFragment;
import com.app.inn.peepsin.UI.fragments.Profile.ShoppingCartFragment;
import com.app.inn.peepsin.UI.helpers.ItemOffsetDecoration;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.interfaces.Switcher;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;
import static com.app.inn.peepsin.Constants.Constants.CART_UPDATE;
import static com.app.inn.peepsin.Constants.Constants.SHARE_PRODUCT;
import static com.app.inn.peepsin.Constants.Constants.kData;
import static com.app.inn.peepsin.Constants.Constants.kRecords;
import static com.app.inn.peepsin.Constants.Constants.kReload;
import static com.app.inn.peepsin.Constants.Constants.kTotalRecords;

/**
 * Created by akamahesh on 2/5/17.
 */

public class PeepsBoardFragment extends Fragment {

    private String TAG = getTag();
    private static Switcher switcherListener;

    private CustomBottomSheetDialogFragment bottomSheetDialogFragment = null;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.recycler_view_posts)
    RecyclerView recyclerView;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.fab)
    FloatingActionButton floatingActionButton;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.empty_view)
    View emptyView;
    @BindView(R.id.ib_back)
    ImageButton ibBack;
    @BindView(R.id.appBarLayout)
    AppBarLayout appbar;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_cartCount) TextView tvCartCount;
    DataBaseHandler dbHandler;
    private LinearLayoutManager mLayoutManager;
    private BoardAdapter mBoardAdapter;
    private CopyOnWriteArrayList<Board> boardList;
    CurrentUser currentUser;
    int mPage = 1;
    boolean isLoading = true;
    int pageSize;
    private Integer mTotalRecords;
    AppBarLayout appBarLayout;

    BoardAdapter.BoardAdapterInterface boardAdapterInterface = boardId -> startActivityForResult(AddBoardActivity.getIntent(getContext(), 2, boardId), 2);

    public static Fragment newInstance(Switcher switcher) { //flag 1= from timeline and flag 2 = from profile
        switcherListener = switcher;
        return new PeepsBoardFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        boardList = new CopyOnWriteArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_timeline, container, false);
        ButterKnife.bind(this, view);
        //appBarLayout=((HomeActivity) getActivity()).getTabLayout();
        tvTitle.setText("Activity");
        ibBack.setVisibility(View.GONE);
        dbHandler = new DataBaseHandler(getContext());
        mBoardAdapter = new BoardAdapter(getContext(),boardList, switcherListener, boardAdapterInterface,this);
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(mBoardAdapter);
        recyclerView.addItemDecoration(new ItemOffsetDecoration(getContext(), R.dimen.item_offset));
        recyclerView.addOnScrollListener(onScrollListener);
        checkEmptyScreen();
        swipeRefreshLayout.setOnRefreshListener(onRefreshListener);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData();
        loadDataNotifyChange();
         currentUser = ModelManager.modelManager().getCurrentUser();
        setCartUpdate(currentUser.getShoppingCartCount());

    }


    public void setCartUpdate(int count){
        if(count==0)
            tvCartCount.setVisibility(View.GONE);
        else{
            tvCartCount.setVisibility(View.VISIBLE);
            tvCartCount.setText(String.valueOf(count));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        // register cart update receiver
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mBroadcastReceiver,
                new IntentFilter(CART_UPDATE));
       /* if(Utils.isBoardClicked==0) {
            appBarLayout.setVisibility(View.VISIBLE);
        }else{
            appBarLayout.setVisibility(View.GONE);
        }*/
    }

    BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction().equals(CART_UPDATE)){
                setCartUpdate(intent.getIntExtra(kData,0));
            }
        }
    };

    SwipeRefreshLayout.OnRefreshListener onRefreshListener = this::loadData;

    public void initialState(){
        mPage=1;
        isLoading=true;
        pageSize = boardList.size();
    }

    RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            if(dy > 0) //check for scroll down
            {
                int visibleItemCount = mLayoutManager.getChildCount();
                int totalItemCount = mLayoutManager.getItemCount();
                int firstVisibleItemPosition = mLayoutManager.findFirstVisibleItemPosition();

                if (isLoading)
                {
                    if ( (visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= pageSize) {
                        isLoading = false;
                        ++mPage;
                        //Do pagination.. i.e. fetch new data
                        loadMorePeepsBoards();
                    }
                }
            }
        }
    };

    @OnClick(R.id.btn_cart)
    void onCart(){
        if(switcherListener!=null)
            switcherListener.switchFragment(ShoppingCartFragment.newInstance(switcherListener),true,true);
        //startActivity(ShoppingCartActivity.getIntent(getContext()));
    }

    @OnClick(R.id.fab)
    void onAddPost() {
        startActivityForResult(AddBoardActivity.getIntent(getContext(), 1), 1);
    }

    public void loadData() {
        swipeRefreshLayout.setRefreshing(true);
        ModelManager.modelManager().getPeepsInBoards(1, (Constants.Status iStatus, GenricResponse<HashMap<String,Object>> genericResponse) -> {
            HashMap<String,Object> hashMap = genericResponse.getObject();
            boardList = (CopyOnWriteArrayList<Board>) hashMap.get(kRecords);
            mTotalRecords  = (Integer) hashMap.get(kTotalRecords);
            mBoardAdapter.addItems(boardList);
            swipeRefreshLayout.setRefreshing(false);
            initialState();
            checkEmptyScreen();
        }, (Constants.Status iStatus, String message) -> {
            Log.e(TAG, message);
            checkEmptyScreen();
            swipeRefreshLayout.setRefreshing(false);
        });
    }

    private void loadMorePeepsBoards() {
        swipeRefreshLayout.setRefreshing(true);
        ModelManager.modelManager().getPeepsInBoards(mPage, (Constants.Status iStatus, GenricResponse<HashMap<String,Object>> genericResponse) -> {
            HashMap<String,Object> hashMap = genericResponse.getObject();
            CopyOnWriteArrayList<Board> boards = (CopyOnWriteArrayList<Board>) hashMap.get(kRecords);
            boardList.addAll(boards);
            mBoardAdapter.addMoreItems(boards);
            swipeRefreshLayout.setRefreshing(false);
            isLoading = !boards.isEmpty();
        }, (Constants.Status iStatus, String message) -> {
            Log.e(TAG, message);
            swipeRefreshLayout.setRefreshing(false);
        });
    }


    // Notify the list when user comment on board
    public void loadDataNotifyChange() {
        ModelManager.modelManager().getPeepsInBoards(1, (Constants.Status iStatus, GenricResponse<HashMap<String,Object>> genericResponse) -> {
            HashMap<String,Object> hashMap = genericResponse.getObject();
            CopyOnWriteArrayList<Board> boards = (CopyOnWriteArrayList<Board>) hashMap.get(kRecords);
            mTotalRecords  = (Integer) hashMap.get(kTotalRecords);
            mBoardAdapter.addItems(boards);
            initialState();
        }, (Constants.Status iStatus, String message) -> {
            Log.e(TAG, message);
        });
    }

    public void shareCount(int boardId){
        if(!dbHandler.getConnectionsFromDatabase(String.valueOf(currentUser.getUserId()),"0").isEmpty()){
            if(bottomSheetDialogFragment==null){
                bottomSheetDialogFragment = CustomBottomSheetDialogFragment.newInstance(3,boardId); //1 for map and 2 for share
                bottomSheetDialogFragment.setTargetFragment(this,SHARE_PRODUCT);
                bottomSheetDialogFragment.show(((HomeActivity) getContext()).getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
            }else if(!bottomSheetDialogFragment.isVisible()){
                bottomSheetDialogFragment= null;
                shareCount(boardId);
            }
        }else
            Toaster.toast("You don't have connection");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                if (data.getBooleanExtra(kReload, false)) {
                    loadData();
                }
            }
        } else if (requestCode == 2) {
            if (resultCode == RESULT_OK) {
                if (data.getBooleanExtra(kReload, false)) {
                    loadData();
                }
            }
        } else
            loadData();
    }

    private void checkEmptyScreen() {
        if (mBoardAdapter.getItemCount() > 0) {
            emptyView.setVisibility(View.GONE);
        } else {
            emptyView.setVisibility(View.VISIBLE);
            appbar.setExpanded(true);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        switcherListener = null;
    }

}
