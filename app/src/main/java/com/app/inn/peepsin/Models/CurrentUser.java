package com.app.inn.peepsin.Models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by akaMahesh on 15/5/17.
 * copyright to : Innverse Technologies
 */

public class CurrentUser extends BaseModel implements Serializable {

    private String authToken;
    private Integer userId;
    private Integer shopId;
    private Integer shopStatus;
    private String jabberId;
    private String firstName;
    private String lastName;
    private SocialLink email;
    private SocialLink phone;
    private Integer gender;
    private Integer contactIdType;
    private String dateOfBirth;
    private String joinedDate;
    private String profilePicURL;
    private String referralCode;
    private UserRegType regType;
    private Integer sellingProductCount;
    private Integer favoriteProductCount;
    private Integer favoriteShopCount;
    private Integer friendsCount;
    private Integer friendsCountSecondLevel;
    private Integer friendsCountThirdLevel;
    private Integer peepsBoardCount;
    private CopyOnWriteArrayList<SocialLink> userSocialLinks;
    private Address primaryAddress;
    private Boolean isAccountActivated;
    private Integer shoppingCartCount;
    private Integer productType;
    private MyShop myShop;
    private UserLocation userLocation;
    private CopyOnWriteArrayList<TAX> taxList;
    private CopyOnWriteArrayList<OrderStatusList> orderStatus;
    private CopyOnWriteArrayList<ShopModel> featureShopList;
    private CopyOnWriteArrayList<ShopModel> favShopList;
    private CopyOnWriteArrayList<ShopModel> shopNearList;
    private CopyOnWriteArrayList<SharedProduct> recommendedList;
    private CopyOnWriteArrayList<ProductType> productTypeList;
    private CopyOnWriteArrayList<Feeds> feedsList;
    private CopyOnWriteArrayList<Connection> connectionList;

    public CurrentUser(JSONObject jsonResponse) {
        this.userId = getValue(jsonResponse, kUserId, Integer.class);
        this.shopId = getValue(jsonResponse, kShopId, Integer.class);
        this.shopStatus = getValue(jsonResponse, kShopStatus, Integer.class);
        this.jabberId = getValue(jsonResponse, kJabberId, String.class);
        this.authToken = getValue(jsonResponse, kAuthToken, String.class);
        this.firstName = getValue(jsonResponse, kFirstName, String.class);
        this.lastName = getValue(jsonResponse, kLastName, String.class);
        this.referralCode = getValue(jsonResponse, kReferralCode, String.class);
        this.gender = getValue(jsonResponse, kGender, Integer.class);
        this.email = new SocialLink(getValue(jsonResponse, kEmail, JSONObject.class));
        this.phone = new SocialLink(getValue(jsonResponse, kPhone, JSONObject.class));
        this.dateOfBirth = getValue(jsonResponse, kDateOfBirth, String.class);
        this.profilePicURL = getValue(jsonResponse, kProfilePicUrl, String.class);
        try {
            this.contactIdType = getValue(jsonResponse, kContactIdType, Integer.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.regType = UserRegType.getRegType(getValue(jsonResponse, kRegType, Integer.class));
        this.joinedDate = getValue(jsonResponse, kJoinedDate, String.class);
        this.sellingProductCount = getValue(jsonResponse, kSellingProductCount, Integer.class);
        this.favoriteProductCount = getValue(jsonResponse, kFavoriteProductCount, Integer.class);
        this.favoriteShopCount = getValue(jsonResponse, kFavoriteShopCount, Integer.class);
        this.peepsBoardCount = getValue(jsonResponse, kPeepsBoardCount, Integer.class);
        this.friendsCount = getValue(jsonResponse, kFriendsCount, Integer.class);
        this.friendsCountSecondLevel = getValue(jsonResponse, kFriendsCountSecondLevel, Integer.class);
        this.friendsCountThirdLevel = getValue(jsonResponse, kFriendsCountThirdLevel, Integer.class);
        try {
            this.userSocialLinks = handleSocialLinks(getValue(jsonResponse, kUserSocialLinks, JSONArray.class));
            this.primaryAddress = new Address(getValue(jsonResponse, kPrimaryAddress, JSONObject.class));
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.isAccountActivated = getValue(jsonResponse, kIsAccountActivated, Boolean.class);
    }

    private CopyOnWriteArrayList<SocialLink> handleSocialLinks(JSONArray jsonArray) throws JSONException {
        CopyOnWriteArrayList<SocialLink> userSocialLinks = new CopyOnWriteArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            userSocialLinks.add(new SocialLink(jsonArray.getJSONObject(i)));
        }
        return userSocialLinks;
    }

    /**
     * @return fullname = Firstname+lastName
     */
    public String getFullName() {
        return firstName + " " + lastName;
    }

    public String getAuthToken() {
        return authToken;
    }

    public Integer getUserId() {
        return userId;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopStatus(Integer shopStatus) {
        this.shopStatus = shopStatus;
    }

    public Integer getShopStatus() {
        return shopStatus;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public String getJabberId() {
        return jabberId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public void setJabberId(String jabberId) {
        this.jabberId = jabberId;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setContactIdType(Integer contactIdType) {
        this.contactIdType = contactIdType;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public void setJoinedDate(String joinedDate) {
        this.joinedDate = joinedDate;
    }

    public void setProfilePicURL(String profilePicURL) {
        this.profilePicURL = profilePicURL;
    }

    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }

    public void setRegType(UserRegType regType) {
        this.regType = regType;
    }

    public void setFriendsCountSecondLevel(Integer friendsCountSecondLevel) {
        this.friendsCountSecondLevel = friendsCountSecondLevel;
    }

    public void setFriendsCountThirdLevel(Integer friendsCountThirdLevel) {
        this.friendsCountThirdLevel = friendsCountThirdLevel;
    }

    public void setPrimaryAddress(Address primaryAddress) {
        this.primaryAddress = primaryAddress;
    }

    public void setProductTypeList(
        CopyOnWriteArrayList<ProductType> productTypeList) {
        this.productTypeList = productTypeList;
    }

    public String getLastName() {
        return lastName;
    }

    public SocialLink getEmail() {
        return email;
    }

    public SocialLink getPhone() {
        return phone;
    }

    public void setEmail(SocialLink email) {
        this.email = email;
    }

    public void setPhone(SocialLink phone) {
        this.phone = phone;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public Integer getContactIdType() {
        return contactIdType;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public String getProfilePicURL() {
        return profilePicURL;
    }

    public UserRegType getRegType() {
        return regType;
    }

    public String getJoinedDate() {
        return joinedDate;
    }

    public String getReferralCode() {
        return referralCode;
    }

    public Integer getSellingProductCount() {
        return sellingProductCount;
    }

    public void setSellingProductCount(Integer sellingProductCount) {
        this.sellingProductCount = sellingProductCount;
    }

    public Integer getFriendsCount() {
        if(friendsCount<0)
            return 0;
        else
            return friendsCount;
    }

    public void setFriendsCount(Integer friendsCount) {
        this.friendsCount = friendsCount;
    }

    public Integer getFriendsCountSecondLevel() {
        return friendsCountSecondLevel;
    }

    public Integer getFriendsCountThirdLevel() {
        return friendsCountThirdLevel;
    }

    public Integer getPeepsBoardCount() {
        return peepsBoardCount;
    }

    public void setPeepsBoardCount(Integer peepsBoardCount) {
        this.peepsBoardCount = peepsBoardCount;
    }

    public CopyOnWriteArrayList<TAX> getTaxList() {
        return taxList;
    }

    public void setTaxList(CopyOnWriteArrayList<TAX> taxList) {
        this.taxList = taxList;
    }

    public CopyOnWriteArrayList<OrderStatusList> getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(CopyOnWriteArrayList<OrderStatusList> orderStatus) {
        this.orderStatus = orderStatus;
    }

    public CopyOnWriteArrayList<ProductType> getProductTypeList() {
        if(productTypeList==null)
            return new CopyOnWriteArrayList<>();
        return productTypeList;
    }

    public void setProductList(CopyOnWriteArrayList<ProductType> typeList) {
        this.productTypeList = typeList;
    }

    public Integer getFavoriteShopCount() {
        return favoriteShopCount;
    }

    public void setFavoriteShopCount(Integer favoriteShopCount) {
        this.favoriteShopCount = favoriteShopCount;
    }

    public Integer getFavoriteProductCount() {
        return favoriteProductCount;
    }

    public void setFavoriteProductCount(Integer favoriteProductCount) {
        this.favoriteProductCount = favoriteProductCount;
    }

    public Integer getProductType() {
        return productType;
    }

    public void setProductType(Integer productType) {
        this.productType = productType;
    }

    public Address getPrimaryAddress() {
        return primaryAddress;
    }

    public List<SocialLink> getUserSocialLinks() {
        return userSocialLinks;
    }

    public void setUserSocialLinks(CopyOnWriteArrayList<SocialLink> userSocialLinks) {
        this.userSocialLinks = userSocialLinks;
    }

    public Integer getShoppingCartCount() {
        return (shoppingCartCount == null) ? 0 : shoppingCartCount;
    }

    public void setShoppingCartCount(Integer shoppingCartCount) {
        this.shoppingCartCount = shoppingCartCount;
    }

    public String getContactId() {
        if (email.getContactId().isEmpty())
            return phone.getContactId();
        else return email.getContactId();
    }

    public CopyOnWriteArrayList<Feeds> getFeedsList() {
        return feedsList;
    }

    public void setFeedsList(CopyOnWriteArrayList<Feeds> feedsList) {
        this.feedsList = feedsList;
    }

    public List<Connection> getFilteredConnectionList() {
        List<Connection> connections = new ArrayList<>();
        for (Connection connection : getConnectionList()) {
            if (!connection.getIsBlocked())
                connections.add(connection);
        }
        return connections;
    }

    public CopyOnWriteArrayList<Connection> getConnectionList() {
        return (connectionList==null)?new CopyOnWriteArrayList<>():connectionList;
    }

    public void setConnectionList(CopyOnWriteArrayList<Connection> connections) {
        connectionList = connections;
    }

    public MyShop getMyShop() {
        return myShop;
    }

    public void setMyShop(MyShop myShop) {
        this.myShop = myShop;
    }



    public Boolean getAccountActivated() {
        return isAccountActivated;
    }

    public void setAccountActivated(Boolean accountActivated) {
        isAccountActivated = accountActivated;
    }

    public UserLocation getUserLocation() {
        return userLocation;
    }

    public void setUserLocation(UserLocation userLocation) {
        this.userLocation = userLocation;
    }

    /**
     * send -1 for whole list
     * @return
     */
    public CopyOnWriteArrayList<ShopModel> getFeatureShopList(int count) {
        if(featureShopList==null)
            return new CopyOnWriteArrayList<>();
        else if(count<0)
            return featureShopList;
        else if(featureShopList.size()<=count)
            return featureShopList;
        else {
            CopyOnWriteArrayList<ShopModel> list = new CopyOnWriteArrayList<>();
            for(int n=0;n<count;n++){
                list.add(featureShopList.get(n));
            }
            return list;
        }
    }

    public void setFeatureShopList(CopyOnWriteArrayList<ShopModel> topShopList) {
        this.featureShopList = topShopList;
    }

    public CopyOnWriteArrayList<ShopModel> getFavShopList() {
        return (favShopList==null)?new CopyOnWriteArrayList<>():favShopList;
    }

    public void setFavShopList(CopyOnWriteArrayList<ShopModel> favShopList) {
        this.favShopList = favShopList;
    }

    public CopyOnWriteArrayList<SharedProduct> getRecommendedList() {
        if(recommendedList==null)
            return new CopyOnWriteArrayList<>();
        return recommendedList;
    }

    public void setRecommendedList(CopyOnWriteArrayList<SharedProduct> recommendedList) {
        this.recommendedList = recommendedList;
    }

    public CopyOnWriteArrayList<ShopModel> getShopNearList(int count) {
        if(shopNearList==null)
            return new CopyOnWriteArrayList<>();
        else if(count<0)
            return shopNearList;
        else if(shopNearList.size()<count)
            return shopNearList;
        else {
            CopyOnWriteArrayList<ShopModel> list = new CopyOnWriteArrayList<>();
            for(int n=0;n<count;n++){
                list.add(shopNearList.get(n));
            }
            return list;
        }
    }

    public void setShopNearList(CopyOnWriteArrayList<ShopModel> shopNearList) {
        this.shopNearList = shopNearList;
    }
}
