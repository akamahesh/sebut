package com.app.inn.peepsin.Models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by akaMahesh on 11/5/17.
 * copyright to : Innverse Technologies
 */

public class PeepsChat implements Serializable {
    private CopyOnWriteArrayList<ChatProduct> chatProducts;
    private Integer count;

    public  PeepsChat(){
        chatProducts= new CopyOnWriteArrayList<>();
    }

    public CopyOnWriteArrayList<ChatProduct> getChatProductList() {
        return chatProducts;
    }

    public void setChatProducts(CopyOnWriteArrayList<ChatProduct> chatProducts) {
        this.chatProducts = chatProducts;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
