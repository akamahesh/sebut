package com.app.inn.peepsin.UI.fragments.SignIn;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.helpers.Validations;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kContactId;
import static com.app.inn.peepsin.Constants.Constants.kOTP;
import static com.app.inn.peepsin.Constants.Constants.kPassword;

/**
 * Created by akamahesh on 2/5/17.
 */

public class ResetPasswordFragment extends Fragment{


    @BindView(R.id.edt_new_password) EditText edtNewPassword;
    @BindView(R.id.edt_confirm_password) EditText edtConfirmPassword;
    @BindView(R.id.tv_title) TextView tvTitle;
    private OnResetPasswordFragmentInteractionListener mListener;
    String contactId;
    Integer OTP;
    private ProgressDialog progressDialog;

    public static Fragment newInstance(String contactId,int otp){
        Fragment fragment =  new ResetPasswordFragment();
        Bundle bundle = new Bundle();
        bundle.putString(kContactId,contactId);
        bundle.putInt(kOTP,otp);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnResetPasswordFragmentInteractionListener) {
            mListener = (OnResetPasswordFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnResetPasswordFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if(bundle!=null){
            OTP= bundle.getInt(kOTP);
            contactId = bundle.getString(kContactId);
        }

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_reset_password, container, false);
        ButterKnife.bind(this,view);
        progressDialog = Utils.generateProgressDialog(getContext(),false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @OnClick(R.id.btn_reset)
    void resetPassword(){
        String newPassword = edtNewPassword.getText().toString().trim();
        String confirmPassword = edtConfirmPassword.getText().toString().trim();

        if(!Validations.isValidPassword(newPassword)){
            edtNewPassword.setError("Please Enter a valid Password!");
            return;
        }

        if(newPassword.equals(confirmPassword))
            resetPassword(newPassword);

        else
            edtConfirmPassword.setError("Password Don't match");

    }

    private void resetPassword(String password) {
        HashMap<String,Object> dataMap = new HashMap<>();
        dataMap.put(kContactId,contactId);
        dataMap.put(kPassword,password);
        dataMap.put(kOTP,OTP);
        showProgress(true);
        ModelManager.modelManager().resetPassword(dataMap,(Constants.Status iStatus) -> {
            showProgress(false);
            mListener.onPasswordChanged();
        }, (Constants.Status iStatus, String message) -> {
            Toaster.toast(message);
            showProgress(false);
        });


    }

    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) progressDialog.dismiss();
        }
    }

    @OnClick(R.id.btn_back)
    void onBack(){
        getFragmentManager().popBackStack();
        getFragmentManager().popBackStack();
        getFragmentManager().popBackStack();
    }

    public interface OnResetPasswordFragmentInteractionListener {
        void onPasswordChanged();
        void onBack();
    }

}
