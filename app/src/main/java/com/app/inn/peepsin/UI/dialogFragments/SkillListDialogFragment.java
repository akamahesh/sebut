package com.app.inn.peepsin.UI.dialogFragments;

import static com.app.inn.peepsin.Constants.Constants.kExperienceLevel;
import static com.app.inn.peepsin.Constants.Constants.kExperienceLevelId;
import static com.app.inn.peepsin.Constants.Constants.kRecords;
import static com.app.inn.peepsin.Constants.Constants.kSkill;
import static com.app.inn.peepsin.Constants.Constants.kSkillArea;
import static com.app.inn.peepsin.Constants.Constants.kSkillAreaId;
import static com.app.inn.peepsin.Constants.Constants.kSkillId;
import static com.app.inn.peepsin.Constants.Constants.kType;
import static com.app.inn.peepsin.Constants.Constants.kYearsOfExperience;
import static com.app.inn.peepsin.Constants.Constants.kYearsOfExperienceId;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.helpers.Utils;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * <p>A fragment that shows a list of items as a modal bottom sheet.</p>
 * <p>You can show this modal bottom sheet from your activity like this:</p>
 * <pre>
 *     SkillListDialogFragment.newInstance(30).show(getSupportFragmentManager(), "dialog");
 * </pre>
 * <p>You activity (or fragment) needs to implement {@link SkillListDialogFragment.Listener}.</p>
 */
public class SkillListDialogFragment extends BottomSheetDialogFragment {

  private Listener mListener;
  private List<HashMap<String, Object>> skillAreaList = new ArrayList<>();
  private SkillAdapter adapter;
  private Integer TYPE = 0;

  @BindView(R.id.edt_search)
  EditText edtSearch;
  @BindView(R.id.recycler_view)
  RecyclerView recyclerView;

  /**
   * @param type 1= skillAreaList, 2 = skillList, 3= experienceLevel, 4= Year of Experience
   */
  public static SkillListDialogFragment newInstance(int type,
      List<HashMap<String, Object>> skillAreaList) {
    final SkillListDialogFragment fragment = new SkillListDialogFragment();
    final Bundle args = new Bundle();
    args.putInt(kType, type);
    args.putSerializable(kRecords, (Serializable) skillAreaList);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    final Fragment parent = getParentFragment();
    if (parent != null) {
      mListener = (Listener) parent;
    } else {
      mListener = (Listener) context;
    }
  }

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    Bundle bundle = getArguments();
    skillAreaList = (List<HashMap<String, Object>>) bundle.getSerializable(kRecords);
    TYPE = bundle.getInt(kType);
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_skill_list_dialog, container, false);
    ButterKnife.bind(this, view);

    return view;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    adapter = new SkillAdapter(skillAreaList);
    recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    recyclerView.setAdapter(adapter);
    edtSearch.setOnEditorActionListener(actionListener);
    edtSearch.addTextChangedListener(searchTextWatcher);
  }

  TextView.OnEditorActionListener actionListener = (v, actionId, event) -> {
    if (actionId == EditorInfo.IME_ACTION_SEARCH) {
      Utils.hideKeyboard(getContext());
    }
    return true;
  };

  TextWatcher searchTextWatcher = new TextWatcher() {
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
      charSequence = charSequence.toString().toLowerCase();
      final List<HashMap<String, Object>> filteredList = new ArrayList<>();
      String name = "";
      for (int i = 0; i < skillAreaList.size(); i++) {
        if (TYPE == 1) { //skill area list
          name = skillAreaList.get(i).get(kSkillArea).toString().toLowerCase();
        } else if (TYPE == 2) { //skill list
          name = skillAreaList.get(i).get(kSkill).toString().toLowerCase();
        } else if (TYPE == 3) { //experienceLevelList
          name = skillAreaList.get(i).get(kExperienceLevel).toString().toLowerCase();
        } else if (TYPE == 4) { //year of Experience
          name = skillAreaList.get(i).get(kYearsOfExperience).toString().toLowerCase();
        }
        if (name.contains(charSequence)) {
          filteredList.add(skillAreaList.get(i));
        }
      }

      recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
      adapter = new SkillAdapter(filteredList);
      recyclerView.setAdapter(adapter);
      adapter.notifyDataSetChanged();
    }

    @Override
    public void afterTextChanged(Editable s) {
    }
  };


  @Override
  public void onDetach() {
    mListener = null;
    super.onDetach();
  }

  public interface Listener {

    void onSkillClicked(String name, int tag, int type);
  }

  class SkillAdapter extends RecyclerView.Adapter<SkillAdapter.ViewHolder> {

    List<HashMap<String, Object>> mapList;

    SkillAdapter(List<HashMap<String, Object>> itemCount) {
      mapList = itemCount;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      View layoutView = LayoutInflater.from(parent.getContext())
          .inflate(R.layout.fragment_skill_list_dialog_item, parent, false);
      return new ViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
      HashMap<String, Object> map = mapList.get(position);
      String name = "";
      Integer id = 0;
      if (TYPE == 1) { //skill area list
        name = (String) map.get(kSkillArea);
        id = (Integer) map.get(kSkillAreaId);
      } else if (TYPE == 2) { //skill list
        name = (String) map.get(kSkill);
        id = (Integer) map.get(kSkillId);
      } else if (TYPE == 3) { //experienceLevelList
        name = (String) map.get(kExperienceLevel);
        id = (Integer) map.get(kExperienceLevelId);
      } else if (TYPE == 4) { //year of Experience
        name = (String) map.get(kYearsOfExperience);
        id = (Integer) map.get(kYearsOfExperienceId);
      }

      holder.tvItem.setText(name);
      holder.tvItem.setTag(id);
    }

    @Override
    public int getItemCount() {
      return mapList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

      @BindView(R.id.text)
      TextView tvItem;

      public ViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
      }

      @OnClick(R.id.text)
      void onItemClick() {
        if (mListener != null) {
          mListener.onSkillClicked(tvItem.getText().toString(), (Integer) tvItem.getTag(), TYPE);
          dismiss();
        }
      }

            /*ViewHolder(LayoutInflater inflater, ViewGroup parent) {
                super(inflater.inflate(R.layout.fragment_skill_list_dialog_item, parent, false));
            }*/

    }

  }

}
