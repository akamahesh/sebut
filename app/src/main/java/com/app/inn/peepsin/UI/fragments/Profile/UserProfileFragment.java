package com.app.inn.peepsin.UI.fragments.Profile;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Constants.Constants.ChatON;
import com.app.inn.peepsin.DataBase.DataBaseHandler;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Managers.XMPPManager.RoosterHelper;
import com.app.inn.peepsin.Models.CelebrationEvent;
import com.app.inn.peepsin.Models.ChatProduct;
import com.app.inn.peepsin.Models.Connection;
import com.app.inn.peepsin.Models.SocialLink;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.activities.ChatActivity;
import com.app.inn.peepsin.UI.activities.ChatActivity.ChatType;
import com.app.inn.peepsin.UI.adapters.CelebrationEventAdapter;
import com.app.inn.peepsin.UI.fragments.Shop.NewShopDetailsFragment;
import com.app.inn.peepsin.UI.helpers.DividerItemRecyclerDecoration;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kIsUserBlocked;
import static com.app.inn.peepsin.Constants.Constants.kRecords;
import static com.app.inn.peepsin.Constants.Constants.kUserId;

public class UserProfileFragment extends Fragment {

    @BindColor(R.color.navigation_inactive_tab)
    int colorInactive;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_profile_image)
    ImageView ivProfileImage;
    @BindView(R.id.iv_user_image)
    ImageView ivUserImage;
    @BindView(R.id.iv_blocked_user)
    ImageView ivBlockUser;
    @BindView(R.id.iv_phone_verification)
    ImageView ivPhoneVerification;
    @BindView(R.id.iv_email_verification)
    ImageView ivEmailVerification;
    @BindView(R.id.iv_facebook_verification)
    ImageView ivFacebookVerification;
    @BindView(R.id.iv_google_verification)
    ImageView ivGoogleVerification;
    @BindView(R.id.tv_user_name) TextView tvUserName;
    @BindView(R.id.tv_addFriend) TextView tvAddFriend;
    @BindView(R.id.tv_about)
    TextView tvAbout;
    @BindView(R.id.tv_joined)
    TextView tvJoined;
    @BindView(R.id.tv_name)
    TextView tvFullName;
    @BindView(R.id.tv_userFriendsLevel)
    TextView tvUserFriendsLevel;
    @BindView(R.id.rl_userLevel)
    View rlUserLevel;
    @BindView(R.id.view_gender)
    View viewGender;
    @BindView(R.id.tv_gender)
    TextView tvGender;
    @BindView(R.id.tv_circle_user)
    TextView tvCircleCount;
    @BindView(R.id.view_email)
    RelativeLayout emailLayout;
    @BindView(R.id.tv_email)
    TextView tvEmail;
    @BindView(R.id.view_contact)
    RelativeLayout phoneLayout;
    @BindView(R.id.tv_phone)
    TextView tvPhone;
    @BindView(R.id.view_dob)
    RelativeLayout dobLayout;
    @BindView(R.id.tv_dob)
    TextView tvDOB;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.tv_block_user)
    TextView tvBlockUser;
    //@BindView(R.id.iv_request_send) TextView tvRequestSend;
    @BindView(R.id.ll_otherDetails) View tvDetailView;
    @BindView(R.id.celebration_view)
    View celebartionView;
    @BindView(R.id.recycler_events)
    RecyclerView recyclerView;
    @BindView(R.id.tv_event_status)
    TextView tvViewAll;
    @BindView(R.id.tv_empty_event)
    TextView tvEmptyEvent;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.appBarLayout)
    AppBarLayout appBarLayout;
    @BindView(R.id.btn_edit)
    ImageButton editMenu;
    @BindView(R.id.iv_chat)
    ImageView ivChat;

    private ProgressDialog progressDialog;
    private static Switcher profileSwitcherListener;
    private Connection connection;
    DataBaseHandler dbHandler;
    private CelebrationEventAdapter eventAdapter;

    public UserProfileFragment() {
        // Required empty public constructor

    }

    public static UserProfileFragment newInstance(Switcher switcher, Connection connection) {
        UserProfileFragment fragment = new UserProfileFragment();
        profileSwitcherListener = switcher;
        Bundle bundle = new Bundle();
        bundle.putSerializable(kRecords, connection);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setRetainInstance(true);
        setHasOptionsMenu(true);
        Bundle bundle = getArguments();
        if (bundle != null) {
            connection = (Connection) bundle.getSerializable(kRecords);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_profile, container, false);
        ButterKnife.bind(this, view);

        dbHandler = new DataBaseHandler(getContext());

        String title = connection.getFirstName() + "'s profile";
        tvTitle.setText(title);
        progressDialog = Utils.generateProgressDialog(getContext(), true);

        if (connection.getConnectionLevel() == 1) {
            eventAdapter = new CelebrationEventAdapter(getContext(), new ArrayList<>(), false, profileSwitcherListener);
            recyclerView.setHasFixedSize(true);
            recyclerView.setNestedScrollingEnabled(false);
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            recyclerView.setAdapter(eventAdapter);
            recyclerView.addItemDecoration(new DividerItemRecyclerDecoration(getActivity(), R.drawable.canvas_recycler_divider));
            checkEmptyView();
            checkEvents();
        } else
            celebartionView.setVisibility(View.GONE);

        //setupToolbar();
        //setHasOptionsMenu(true);

        //to only show chat icon when its first level connection.
        if (connection.getConnectionLevel() == 1) {
            ivChat.setVisibility(View.VISIBLE);
        } else {
            ivChat.setVisibility(View.INVISIBLE);
        }

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setRetainInstance(true);
        //setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {
            updateUserData(connection);
            if (connection.getConnectionLevel() == 1)
                loadEvents();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.view_my_shop_user)
    void sellingStuff() {
        try {
            Integer shopId = connection.getShopId();
            Integer sellingProductCount = connection.getSellingProductCount();

            if (sellingProductCount == 0 && shopId > 0) {
                Toaster.toastRangeen("You have no permission to see user shop");
            } else if (shopId == 0) {
                Toaster.toastRangeen("User have no shop");
            } else if (shopId == -1) {
                Toaster.toastRangeen("User have no shop");
            } else {
                if (profileSwitcherListener != null) {
                    profileSwitcherListener
                            .switchFragment(NewShopDetailsFragment.newInstance(profileSwitcherListener, shopId),
                                    true, true);
                }

            }
        } catch (Exception e) {
            Toaster.toastRangeen("User have no shop");
            e.printStackTrace();
        }

    }


    @OnClick(R.id.iv_chat)
    void onChat() {
        String jabberId = connection.getJabberId();
        String userID = String.valueOf(connection.getUserId());
        String username = connection.getFullName();
        String connectionLevel = String.valueOf(connection.getConnectionLevel());
        ChatProduct chatProduct = new ChatProduct();
        chatProduct.setJabberId(jabberId);
        chatProduct.setUserId(userID);
        chatProduct.setUserName(username);
        chatProduct.setUserImageURL(connection.getProfilePicURL());
        chatProduct.setChatOn(String.valueOf(ChatON.user.getValue()));//1 for user chat
        chatProduct.setConnectionLevel(connectionLevel);
        boolean hasChat = false;
        List<ChatProduct> chatProducts = RoosterHelper.getChatThreads();
        for (ChatProduct chatur : chatProducts) {
            String chaturUNIQUEID = chatur.getUniqueId();
            String chatterUNIQUEID = chatProduct.getUniqueId();
            if (chaturUNIQUEID.equals(chatterUNIQUEID)) {
                hasChat = true;
                chatProduct = chatur;
                break;
            }
        }
        startActivity(ChatActivity.getIntent(getContext(), chatProduct, ChatType.USER_CHAT));
    }

    @OnClick(R.id.view_circle_user)
    void getFriends() {
        if (profileSwitcherListener != null) {
            profileSwitcherListener.switchFragment(
                    UserProfileFriendFragment.newInstance(profileSwitcherListener, connection), true, true);
        }
    }

    @OnClick(R.id.tv_event_status)
    void addEvent() {
        if (profileSwitcherListener != null) {
            if (eventAdapter.getItemCount() == 0)
                profileSwitcherListener.switchFragment(AddEventFragment.newInstance(connection.getUserId(), null, 1, profileSwitcherListener), true, true);
            else
                profileSwitcherListener.switchFragment(CelebrationEventFragment.newInstance(connection.getUserId(), connection.getFullName(), profileSwitcherListener), true, true);
        }
    }

    @OnClick(R.id.btn_back)
    void onBack() {
        getFragmentManager().popBackStack();
    }

    @OnClick(R.id.tv_addFriend)
    void onAddFriend() {
        if (connection.getUserConnectionStatus() == 1) {
            tvAddFriend.setText("Request Sent");
            tvAddFriend.setVisibility(View.VISIBLE);
            Toaster.toastRangeen(getString(R.string.friend_request_already));
        } else if (connection.getUserConnectionStatus() == 2) {
            tvAddFriend.setText(getString(R.string.add_friend_text));
            Toaster.toastRangeen(getString(R.string.already_friend));
        } else {
           // tvRequestSend.setVisibility(View.GONE);
            checkConnection();
        }
    }

    @OnClick(R.id.view_block_user)
    void onBlockUsers() {
        if (connection.getConnectionLevel() == 1) {
            blockUnblockUser();
           // tvRequestSend.setVisibility(View.GONE);
        }/* else {
            if (connection.getUserConnectionStatus() == 1) {
                tvRequestSend.setVisibility(View.VISIBLE);
                tvBlockUser.setText(getString(R.string.add_friend_text));
                Toaster.toastRangeen(getString(R.string.friend_request_already));
            } else if (connection.getUserConnectionStatus() == 2) {
                tvBlockUser.setText(getString(R.string.add_friend_text));
                tvRequestSend.setText(getString(R.string.already_friend));
                Toaster.toastRangeen(getString(R.string.already_friend));
            } else {
                tvRequestSend.setVisibility(View.GONE);
                checkConnection();
            }
        }*/
    }


    // Check LevelWise Connection block,Unblock and Add friends request
    private void checkConnection() {
        if (tvBlockUser.getText().equals(getString(R.string.unfriend_text))) {
            removeUserConnection(connection.getUserId(), tvBlockUser);
        } else if (tvAddFriend.getText().equals(getString(R.string.add_friend_text))) {
            if (connection.getEmail() != null) {
                sendFriendRequestUser(connection.getUserId(), tvAddFriend);
                tvAddFriend.setVisibility(View.VISIBLE);
                tvAddFriend.setText("Request Sent");
                //tvRequestSend.setVisibility(View.VISIBLE);
                Toaster.toastRangeen(getString(R.string.friend_request_successfull_send));
            } else if (connection.getPhone() != null) {
                sendFriendRequestUser(connection.getUserId(), tvAddFriend);
                tvAddFriend.setVisibility(View.VISIBLE);
                tvAddFriend.setText("Request Sent");
               // tvRequestSend.setVisibility(View.VISIBLE);
                Toaster.toastRangeen(getString(R.string.friend_request_successfull_send));
            }
        }
    }


    private void removeUserConnection(int userId, TextView t) {
        showProgress(true);
        ModelManager.modelManager().removeUserConnection(dbHandler, userId, (Constants.Status iStatus) -> {
            showProgress(false);
            connection.setUserConnectionStatus(0);
            t.setText(getString(R.string.add_friend_text));
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Toaster.toast(message);
        });
    }

    private void sendFriendRequestUser(Integer userId, TextView t) {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(kUserId, userId);
        showProgress(true);
        ModelManager.modelManager().sendFriendRequest(parameters, (Constants.Status iStatus) -> {
            connection.setUserConnectionStatus(1);
            t.setText("Request Sent");
            showProgress(false);
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Toaster.toast(message);
        });
    }

    // Block and UnBlocked the User
    public void blockUnblockUser() {
        showProgress(true);
        int userId = connection.getUserId();
        int shouldBlock = connection.getIsBlocked() ? 0 : 1;
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(kUserId, userId);
        parameters.put(kIsUserBlocked, shouldBlock);
        ModelManager.modelManager().blockUnblockUser(dbHandler, parameters, (Constants.Status iStatus, GenricResponse<Integer> genricResponse) -> {
            showProgress(false);
            Integer isBlocked = genricResponse.getObject();
            //connection.setIsBlocked(isBlocked == 1);
            if (isBlocked == 1) {
                connection.setIsBlocked(true);
                String unblockText = getString(R.string.unblock_text) + " " + connection.getFirstName();
                tvBlockUser.setText(unblockText);
                shareDialogOpen(getString(R.string.block_user_text));
            } else {
                connection.setIsBlocked(false);
                String unblockText = getString(R.string.block_text) + " " + connection.getFirstName();
                tvBlockUser.setText(unblockText);
                shareDialogOpen(getString(R.string.unblock_user_text));
            }
            // onBack();
        }, (Constants.Status iStatus, String message) -> {
            Toaster.toast(message);
        });

    }

    private void loadEvents() {
        ModelManager.modelManager().getCelebrationEventList(connection.getUserId(), (Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<CelebrationEvent>> genricResponse) -> {
            CopyOnWriteArrayList<CelebrationEvent> events = genricResponse.getObject();
            if (events.size() != 0) {
                eventAdapter.addItem(events.get(0));
            }
            checkEmptyView();
            checkEvents();
        }, (Constants.Status iStatus, String message) -> {
            checkEmptyView();
            checkEvents();
            showProgress(false);
        });
    }

    private void checkEvents() {
        if (eventAdapter.getItemCount() > 0) {
            tvViewAll.setText("View All");
        } else {
            tvViewAll.setText("Add Event");
        }
    }

    private void checkEmptyView() {
        if (eventAdapter.getItemCount() > 0) {
            tvEmptyEvent.setVisibility(View.GONE);
        } else {
            tvEmptyEvent.setVisibility(View.VISIBLE);
        }
    }


    //Show dialog
    private void shareDialogOpen(String title) {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_dialog_block);
        TextView tvTitle = ButterKnife.findById(dialog, R.id.tv_dialog);
        Button tvOk = ButterKnife.findById(dialog, R.id.btn_ok);
        tvTitle.setText(title);
        tvOk.setOnClickListener(v -> dialog.dismiss());
        dialog.show();
    }


    @Override
    public void onResume() {
        super.onResume();
        getActivity().invalidateOptionsMenu();

    }

    @OnClick(R.id.btn_edit)
    void onClick() {
        //creating a popup menu
        PopupMenu popup = new PopupMenu(getContext(), editMenu);
        //inflating menu from xml resource
        popup.inflate(R.menu.menu_user_profile);
        setMenuView(popup);
        //adding click listener
        popup.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.action_add_friend:
                    if (item.getTitle().equals(getString(R.string.unfriend_text))) {
                        removeConnection(connection.getUserId(), item);
                    } else if (item.getTitle().equals(getString(R.string.add_friend_text))) {
                        if (connection.getEmail() != null) {
                            sendFriendRequest(connection.getUserId(), item);
                        } else if (connection.getPhone() != null) {
                            sendFriendRequest(connection.getUserId(), item);
                        }
                    }
                    break;
            }
            return false;
        });
        //displaying the popup
        popup.show();
    }

    private void setMenuView(PopupMenu menu) {
        MenuItem actionFriend = menu.getMenu().getItem(0);
        if (connection.getUserConnectionStatus() == 0) {
            actionFriend.setTitle(getString(R.string.add_friend_text));
        } else if (connection.getUserConnectionStatus() == 1) {
            actionFriend.setTitle("Request Sent");
        } else if (connection.getUserConnectionStatus() == 2) {
            actionFriend.setTitle("UnFriend");
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.clear();
        getActivity().getMenuInflater().inflate(R.menu.menu_user_profile, menu);
        MenuItem actionFriend = menu.findItem(R.id.action_add_friend);

        if (connection.getUserConnectionStatus() == 0) {
            actionFriend.setTitle(getString(R.string.add_friend_text));
        } else if (connection.getUserConnectionStatus() == 1) {
            actionFriend.setTitle("Request Sent");
        } else if (connection.getUserConnectionStatus() == 2) {
            actionFriend.setTitle("UnFriend");
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_user_profile, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add_friend:
                if (item.getTitle().equals(getString(R.string.unfriend_text))) {
                    removeConnection(connection.getUserId(), item);
                } else if (item.getTitle().equals(getString(R.string.add_friend_text))) {
                    if (connection.getEmail() != null) {
                        sendFriendRequest(connection.getUserId(), item);
                    } else if (connection.getPhone() != null) {
                        sendFriendRequest(connection.getUserId(), item);
                    }
                }
                break;
        }
        return false;
    }


    private void removeConnection(int userId, MenuItem item) {
        showProgress(true);
        ModelManager.modelManager().removeUserConnection(dbHandler, userId, (Constants.Status iStatus) -> {
            showProgress(false);
            connection.setUserConnectionStatus(0);
            item.setTitle(getString(R.string.add_friend_text));
        }, (Constants.Status iStatus, String message) -> {
            tvBlockUser.setText(getString(R.string.add_friend_text));
            showProgress(false);
            Toaster.toast(message);
        });
    }


    private void sendFriendRequest(Integer userId, MenuItem item) {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(kUserId, userId);
        showProgress(true);
        ModelManager.modelManager().sendFriendRequest(parameters, (Constants.Status iStatus) -> {
            connection.setUserConnectionStatus(1);
            item.setTitle(getString(R.string.sent_request_text));
           // tvRequestSend.setVisibility(View.VISIBLE);
            tvAddFriend.setVisibility(View.VISIBLE);
            tvAddFriend.setText("Request Sent");
            showProgress(false);
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Toaster.toast(message);
        });
    }

    private void updateUserData(Connection connection) throws NullPointerException {
        String aboutText = getString(R.string.about_);
        String joinedText = getString(R.string.member_since) + " " + connection.getJoinedDate();

        if (connection.getEmail().getContactId().isEmpty()) {
            emailLayout.setVisibility(View.GONE);
        } else {
            String emailText = connection.getEmail().getContactId();
            tvEmail.setText(emailText);
        }

        if (connection.getPhone().getContactId().isEmpty()) {
            phoneLayout.setVisibility(View.GONE);
        } else {
            String phoneText = "Contact us on " + connection.getPhone().getContactId();
            tvPhone.setText(phoneText);
        }

        if (connection.getDateOfBirth().isEmpty()) {
            dobLayout.setVisibility(View.GONE);
        } else {
            String birthText = "Birthday " + connection.getDateOfBirth();
            tvDOB.setText(birthText);
        }

        if (connection.getGender() != -1) {
            String gender;

            if (connection.getGender() == 0) {
                gender = "Gender as Female";
            } else {
                gender = "Gender as Male";
            }
            tvGender.setText(gender);
        } else {
            viewGender.setVisibility(View.GONE);
        }

        String addressText;
        if (connection.getPrimaryAddress() == null) {
            addressText = "No Location Available";
        } else {
            addressText = "Lives in " + connection.getPrimaryAddress().getCity() + ", " + connection.getPrimaryAddress().getState() + ", " + connection.getPrimaryAddress().getCountry();
        }
        String imageUrl = connection.getProfilePicURL();
        String sellingCount = String.valueOf(connection.getSellingProductCount());
        String friendsCount = String.valueOf(connection.getFriendsCount());
        String userFriends = "No friend in your circle";
        if (connection.getFriendsCount() > 0) {
            Integer circle = connection.getFriendsCount() + connection.getFriendsCountSecondLevel() + connection.getFriendsCountThirdLevel();
            userFriends = "Circle - " + circle + " (" + "Direct - " + connection.getFriendsCount() + "," + " 2nd Level " + connection.getFriendsCountSecondLevel()
                    + "," + " 3rd Level " + connection.getFriendsCountThirdLevel() + " )";
            tvUserFriendsLevel.setText(userFriends);
        } else {
            tvUserFriendsLevel.setText(userFriends);
        }

        if (connection.getConnectionLevel() == 1) {
            if (connection.getIsBlocked()) {
                String unblockText = getString(R.string.unblock_text) + " " + connection.getFirstName();
                tvBlockUser.setText(unblockText);
                ivBlockUser.setImageResource(R.drawable.ic_block);
                //tvRequestSend.setVisibility(View.GONE);
            } else {
                String blockText = getString(R.string.block_text) + " " + connection.getFirstName();
                tvBlockUser.setText(blockText);
                ivBlockUser.setImageResource(R.drawable.ic_block);
                //tvRequestSend.setVisibility(View.GONE);
            }
        } else {
            if (connection.getUserConnectionStatus() == 0) {
                tvAddFriend.setText(getString(R.string.add_friend_text));
                tvAddFriend.setVisibility(View.VISIBLE);
                tvDetailView.setVisibility(View.INVISIBLE);
               // tvBlockUser.setText(getString(R.string.add_friend_text));
                //tvRequestSend.setVisibility(View.GONE);
                //ivBlockUser.setImageResource(R.drawable.ic_add_friend);
            } else if (connection.getUserConnectionStatus() == 1) {
                tvAddFriend.setText("Request Sent");
                tvAddFriend.setVisibility(View.VISIBLE);
                tvDetailView.setVisibility(View.INVISIBLE);
               // tvBlockUser.setText(getString(R.string.add_friend_text));
                //tvRequestSend.setVisibility(View.VISIBLE);
                //ivBlockUser.setImageResource(R.drawable.ic_add_friend);
            } else if (connection.getUserConnectionStatus() == 2) {
                String unblockText = getString(R.string.block_text) + " " + connection.getFirstName();
                tvBlockUser.setText(unblockText);
                ivBlockUser.setImageResource(R.drawable.ic_block);
                tvAddFriend.setVisibility(View.GONE);
            }

        }
        if (!friendsCount.equals("0")) {
            tvCircleCount.setText(friendsCount);
        } else {
            tvCircleCount.setVisibility(View.INVISIBLE);
        }

        String username = connection.getFullName();
        int connectionLevel = connection.getConnectionLevel();
        tvCircleCount.setText(friendsCount);
        tvUserName.setText(Utils.getUserName(username, connectionLevel));
        tvFullName.setText(connection.getFullName());
        tvAbout.setText(aboutText);
        tvJoined.setText(joinedText);
        tvAddress.setText(addressText);
        String name = connection.getFirstName();
        TextDrawable drawable = TextDrawable.builder()
                .buildRound(String.valueOf(name.toUpperCase().charAt(0)),
                        ColorGenerator.MATERIAL.getRandomColor());
        if (!imageUrl.isEmpty()) {
            ivUserImage.setVisibility(View.INVISIBLE);
            ivProfileImage.setVisibility(View.VISIBLE);
            Picasso.with(getContext())
                    .load(imageUrl)
                    .resize(200, 200)
                    .placeholder(R.drawable.img_profile_placeholder)
                    .into(ivProfileImage);
        } else {
            ivProfileImage.setVisibility(View.INVISIBLE);
            ivUserImage.setVisibility(View.VISIBLE);
            ivUserImage.setImageDrawable(drawable);
        }


        checkVerification(connection);
    }

    private void checkVerification(Connection connection) {
        boolean isEmailVerified = connection.getEmail().getVerified();
        boolean isPhoneVerified = connection.getPhone().getVerified();
        boolean isFacebookVerified = false;
        boolean isGoogleVerified = false;
        List<SocialLink> socialLinkList = connection.getUserSocialLinks();
        for (SocialLink socialLink : socialLinkList) {
            if (socialLink.getType() == 2) //type 2 for facebook
            {
                isFacebookVerified = (!socialLink.getSocialId().isEmpty());
            } else if (socialLink.getType() == 3) // type 3 for google
            {
                isGoogleVerified = (!socialLink.getSocialId().isEmpty());
            }
        }
        if (isEmailVerified) {
            ivEmailVerification.setImageResource(R.drawable.ic_email_verified);
            ivEmailVerification.setSelected(true);
        } else {
            ivEmailVerification.setColorFilter(colorInactive);
            ivEmailVerification.setSelected(false);
        }
        if (isPhoneVerified) {
            ivPhoneVerification.setImageResource(R.drawable.ic_phone_verified);
            ivPhoneVerification.setSelected(true);
        } else {
            ivPhoneVerification.setColorFilter(colorInactive);
            ivPhoneVerification.setSelected(false);
        }
        if (isFacebookVerified) {
            ivFacebookVerification.setImageResource(R.drawable.ic_facebook_verified);
            ivFacebookVerification.setSelected(true);
        } else {
            ivFacebookVerification.setColorFilter(colorInactive);
            ivFacebookVerification.setSelected(false);
        }
        if (isGoogleVerified) {
            ivGoogleVerification.setImageResource(R.drawable.ic_google_verified);
            ivGoogleVerification.setSelected(true);
        } else {
            ivGoogleVerification.setColorFilter(colorInactive);
            ivGoogleVerification.setSelected(false);
        }
    }

    private void setUserFriendsLevel() {
        SpannableStringBuilder builder = new SpannableStringBuilder();
        SpannableString strIInd = new SpannableString("IInd");
        strIInd.setSpan(new ForegroundColorSpan(Color.parseColor("#FF4A5B")), 0, strIInd.length(), 0);
        builder.append(strIInd);

        SpannableString strIIndLevel = new SpannableString(
                " Level Friends " + connection.getFriendsCountSecondLevel() + ", ");
        strIIndLevel
                .setSpan(new ForegroundColorSpan(Color.parseColor("#444848")), 0, strIIndLevel.length(), 0);
        builder.append(strIIndLevel);

        SpannableString strIIInd = new SpannableString("IIIrd");
        strIIInd.setSpan(new ForegroundColorSpan(Color.parseColor("#FF4A5B")), 0, strIIInd.length(), 0);
        builder.append(strIIInd);

        SpannableString strIIIndLevel = new SpannableString(
                " Level Friends " + connection.getFriendsCountThirdLevel() + ", ");
        strIIIndLevel
                .setSpan(new ForegroundColorSpan(Color.parseColor("#444848")), 0, strIIIndLevel.length(),
                        0);
        builder.append(strIIIndLevel);

        tvUserFriendsLevel.setText(builder, TextView.BufferType.SPANNABLE);
    }

    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) {
                progressDialog.cancel();
            }
        }
    }


    

}