package com.app.inn.peepsin.UI.fragments.Search;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.Search;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.helpers.DividerItemRecyclerDecoration;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnEditorAction;

public class SearchFragment extends Fragment {

    @BindView(R.id.edt_search)
    EditText edtSearch;
    @BindView(R.id.recent_search)
    View recentView;
    @BindView(R.id.empty_view)
    View emptyView;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.recycler_view_history)
    RecyclerView recyclerView;
    @BindView(R.id.recycler_view_recent)
    RecyclerView recyclerViewRecent;

    private static Switcher profileSwitcherListener;
    String kSearchString = "searchString";
    private SearchAdapter searchAdapter;
    ProgressDialog progressDialog;

    public static SearchFragment newInstance(Switcher switcher) {
        SearchFragment fragment = new SearchFragment();
        profileSwitcherListener = switcher;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        ButterKnife.bind(this, view);
        tvTitle.setText(getString(R.string.title_search));
        progressDialog = Utils.generateProgressDialog(getContext(), true);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DividerItemRecyclerDecoration(getActivity(), R.drawable.canvas_recycler_divider));
        searchAdapter = new SearchAdapter(new ArrayList<>());
        recyclerView.setAdapter(searchAdapter);

        addTextListener();
        edtSearch.setOnKeyListener(keyListener);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        checkEmptyScreen();
    }

    @OnEditorAction(R.id.edt_search)
    protected boolean onEditSearch(int actionId) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            Utils.hideKeyboard(getContext());
            searchProduct(edtSearch.getText().toString());
            return true;
        }
        return false;
    }

    private View.OnKeyListener keyListener = (v, keyCode, event) -> {
        if(keyCode == KeyEvent.KEYCODE_DEL) {
            edtSearch.setText("");
            searchAdapter.clearList();
            return true;
        }
        return false;
    };


    private void addTextListener() {
        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                s = s.toString().toLowerCase();
                String str = s.toString();
                char[] ch = str.toCharArray();
                for (char aCh : ch) {
                    if (aCh == ' ') {
                        String wihtoutSpaceStr = str.replaceAll("\\s+", "");
                        searchProduct(wihtoutSpaceStr);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });
    }


    public void searchProduct(String wihtoutSpaceStr) {
        HashMap<String, Object> searchmap = new HashMap<>();
        searchmap.put(kSearchString, wihtoutSpaceStr);
        ModelManager.modelManager().getSearch(searchmap, (Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<Search>> genericList) -> {
            searchAdapter.addItems(genericList.getObject());
            checkEmptyScreen();
        }, (Constants.Status iStatus, String message) -> {
            Toaster.toast(message);
            checkEmptyScreen();
        });
    }

    private void checkEmptyScreen() {
        if (searchAdapter.getItemCount() > 0) {
            emptyView.setVisibility(View.GONE);
            recentView.setVisibility(View.VISIBLE);
        } else {
            recentView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        }
    }


    /***************************************************************************************************************************************/

    class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.SearchHolder> {
        private List<Search> searchList;

        SearchAdapter(List<Search> itemsList) {
            this.searchList = itemsList;
        }

        @Override
        public SearchHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(getContext()).inflate(R.layout.row_search_layout, parent, false);
            return new SearchHolder(view);
        }

        @Override
        public void onBindViewHolder(SearchHolder holder, int position) {
            Search item = searchList.get(position);
            holder.bindContent(item);
            holder.tvName.setText(item.getSearchString());
        }

        @Override
        public int getItemCount() {
            return searchList.size();
        }

        private void addItems(List<Search> list) {
            searchList.clear();
            searchList.addAll(list);
            notifyDataSetChanged();
        }

        private void clearList(){
            searchList.clear();
            notifyDataSetChanged();
        }

        class SearchHolder extends RecyclerView.ViewHolder {
            Search item;
            @BindView(R.id.tv_item_name) TextView tvName;

            SearchHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            @OnClick(R.id.row_item)
            void onRowSelected() {
                Utils.hideKeyboard(getContext());
                if (profileSwitcherListener != null)
                    profileSwitcherListener.switchFragment(SearchResultFragment.newInstance(item.getCategoryId(), profileSwitcherListener), true, false);
            }

            public void bindContent(Search item) {
                this.item = item;
            }
        }
    }
}

