package com.app.inn.peepsin.UI.fragments.Connections;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.app.inn.peepsin.Models.PhoneContact;
import com.app.inn.peepsin.UI.activities.HomeActivity;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.widget.LoginButton;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.DataBase.DataBaseHandler;
import com.app.inn.peepsin.Managers.APIManager.ReachabilityManager;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Managers.SocialManager.FacebookManager;
import com.app.inn.peepsin.Models.CurrentUser;
import com.app.inn.peepsin.Models.FacebookContact;
import com.app.inn.peepsin.Models.SocialUser;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.helpers.DividerItemRecyclerDecoration;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by akamahesh on 28/4/17.
 */

public class FacebookFragment extends Fragment {
    private final String TAG = getClass().getSimpleName();
    private FacebookAdapter facebookAdapter;
    List<FacebookContact> facebookFriendList;

    @BindView(R.id.facebook_login_button) LoginButton loginButton;
    @BindView(R.id.btn_facebook) Button btnImportContacts;
    @BindView(R.id.empty_view) View emptyView;
    @BindView(R.id.tv_title) TextView tvTitle;
    @BindView(R.id.tv_empty_title) TextView tvEmptyTitle;
    @BindView(R.id.tv_empty_message) TextView getTvEmptyMeesage;
    @BindView(R.id.recycler_view) RecyclerView recyclerView;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.edt_search) EditText edtSearch;
    @BindView(R.id.progress_bar) ProgressBar progressBar;

    private FacebookManager facebookManager;
    private DataBaseHandler dbHandler;
    private CurrentUser user;
    private String mUserId;
    private String filterString;
    private int invitePress=0;
    ShareDialog shareDialog;
   // AppBarLayout appbarLayout;

    public static Fragment newInstance(){
        return new FacebookFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        facebookFriendList = new ArrayList<>();
        facebookManager = new FacebookManager(getContext(),facebookManagerListener);
        LoginManager.getInstance().registerCallback(facebookManager.getCallbackManager(), facebookManager.getFacebookCallback());

        dbHandler = new DataBaseHandler(getContext());
        shareDialog = new ShareDialog(this);
        user = ModelManager.modelManager().getCurrentUser();
        mUserId = String.valueOf(user.getUserId());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_facebook, container, false);
        ButterKnife.bind(this,view);
        Utils.hideKeyboard(getContext());
        /*appbarLayout=((HomeActivity) getActivity()).getTabLayout();
        Utils.isfriendClicked=1;*/
        tvTitle.setText(getString(R.string.title_facebook_contacts));
        tvEmptyTitle.setText(R.string.empty_title_facebook_contact);
        getTvEmptyMeesage.setVisibility(View.GONE);
        loginButton.setFragment(this);
        loginButton.setReadPermissions("email");

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.addItemDecoration(new DividerItemRecyclerDecoration(getContext(), R.drawable.canvas_recycler_divider));
        facebookAdapter = new FacebookAdapter(facebookFriendList);
        recyclerView.setAdapter(facebookAdapter);

        swipeRefreshLayout.setEnabled(false);
        swipeRefreshLayout.setOnRefreshListener(onRefreshListener);

        edtSearch.setOnEditorActionListener(actionListener);
        edtSearch.addTextChangedListener(textWatcher);
        return view;
    }

    SwipeRefreshLayout.OnRefreshListener onRefreshListener = this::importContacts;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        List<FacebookContact> contactList = dbHandler.getFacebookContactFromDatabase(mUserId,"0");
        List<FacebookContact> sentList = dbHandler.getFacebookContactFromDatabase(mUserId,"1");
        facebookFriendList.addAll(contactList);
        facebookAdapter.notifyDataSetChanged();
        if(contactList.isEmpty()&&sentList.isEmpty())
            btnImportContacts.setVisibility(View.VISIBLE);
        else
            btnImportContacts.setVisibility(View.GONE);
    }

    FacebookManager.FacebookManagerInterface facebookManagerListener = new FacebookManager.FacebookManagerInterface() {
        @Override
        public void success(SocialUser socialUser) {
            if(user.getEmail().getContactId().equals(socialUser.getEmail())) {
                if(invitePress==1){
                    shareOnFBDialog();
                    invitePress=0;
                }
                else
                    CommonFriendsList();
            } else {
                Utils.showAlertDialog(getContext(),"Error","You need to be logged in facebook app with "+ user.getEmail().getContactId());
                btnImportContacts.setVisibility(View.VISIBLE);
                AccessToken.setCurrentAccessToken(null);
                LoginManager.getInstance().logOut();
            }
        }

        @Override
        public void failure(String s) {
            Utils.showAlertDialog(getContext(),"Error","Error Completing Facebook authentication. Please try again!");
            btnImportContacts.setVisibility(View.VISIBLE);
        }
    };

    TextView.OnEditorActionListener actionListener = (v, actionId, event) -> {
        if(actionId == EditorInfo.IME_ACTION_SEARCH){
                Utils.hideKeyboard(getContext());
        }
        return true;
    };

    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            s = s.toString().toLowerCase();
            filterString = s.toString().toLowerCase();
            facebookAdapter.getFilter().filter(s);
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    @OnClick(R.id.view_invite)
    void inviteContact() {
        login();
        invitePress = 1;
    }

    public void shareOnFBDialog(){
        ShareLinkContent linkContent = new ShareLinkContent.Builder()
                .setContentUrl(Uri.parse("Peepsin.com"))
                .setContentDescription("Peepsin \n Peepsin")
                .build();
        shareDialog.show(linkContent);
    }

    @OnClick(R.id.btn_back)
    void onBack(){
        Utils.hideKeyboard(getContext());
        getFragmentManager().popBackStack();
       /* appbarLayout.setVisibility(View.VISIBLE);
        Utils.isfriendClicked=0;*/
    }
    @Override
    public void onResume() {
        super.onResume();
        //appbarLayout.setVisibility(View.GONE);

    }
    @OnClick(R.id.btn_facebook)
    void importContacts() {
        if (ReachabilityManager.getNetworkStatus()) {
            login();
            btnImportContacts.setVisibility(View.GONE);
        } else {
            Utils.showAlertDialog(getContext(),"Network Error!","Need an internet connected to get contact details");
        }
    }

    private void login(){
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("user_friends"));
    }

    public void CommonFriendsList() {
        showProgress(true);
        Bundle param = new Bundle();
        param.putString(Constants.kFacebookCommanFields, Constants.kFacebookCommanAllFields);
        GraphRequest graphRequest = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), (jsonObject, graphResponse) -> {
            try {
                Map<String, Object> commanfriendsMap = new Gson().fromJson(graphResponse.getRawResponse(),
                        new TypeToken<HashMap<String, Object>>() {
                        }.getType());

                Map<String, Object> friends = (Map<String, Object>) commanfriendsMap.get(Constants.kFacebookCommanFriends);
                List<Map<String, Object>> friendsList = (ArrayList<Map<String, Object>>) friends.get(Constants.kFacebookCommanData);
                for (int count = 0; count < friendsList.size(); count++) {
                    FacebookContact facebookContact = new FacebookContact(friendsList.get(count));
                    facebookFriendList.clear();
                    facebookFriendList.add(facebookContact);
                    if(!dbHandler.isFbUserExist(mUserId,facebookContact.getId())) {
                        Log.i(getClass().getSimpleName(),facebookContact.toString());
                        dbHandler.addFacebookContacts(mUserId,facebookContact.getName(),facebookContact.getId(),"0");
                    }
                }
                facebookAdapter.notifyDataSetChanged();
                checkEmptyState();
                showProgress(false);
                swipeRefreshLayout.setRefreshing(false);
            } catch (Exception e) {
                e.printStackTrace();
                showProgress(false);
                swipeRefreshLayout.setRefreshing(false);
            }
        });
        graphRequest.setParameters(param);
        graphRequest.executeAsync();
    }

    private void checkEmptyState() {
        if (facebookAdapter.getItemCount() > 0) {
            emptyView.setVisibility(View.GONE);
            swipeRefreshLayout.setEnabled(true);
        } else {
            emptyView.setVisibility(View.VISIBLE);
            swipeRefreshLayout.setEnabled(true);
        }
    }

    private void showProgress(boolean b) {
        if(b){
            progressBar.setVisibility(View.VISIBLE);
        }else {
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        facebookManager.getCallbackManager().onActivityResult(requestCode, resultCode, data);
    }

    class FacebookAdapter extends RecyclerView.Adapter<FacebookAdapter.ContactHolder> {
        private List<FacebookContact> contactList;
        private List<FacebookContact> filterList;
        private ColorGenerator generator = ColorGenerator.MATERIAL;

        FacebookAdapter(List<FacebookContact> contactList) {
            this.contactList = contactList;
            this.filterList = contactList;
        }

        @Override
        public ContactHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_facebook_layout, parent, false);
            return new ContactHolder(layoutView);
        }

        @Override
        public void onBindViewHolder(ContactHolder holder, int position) {
            FacebookContact contact = contactList.get(position);
            holder.bindContent(contact);

            String name = contact.getName();
            holder.tvName.setText(name);

            holder.ivUserImage.setVisibility(View.VISIBLE);
            TextDrawable drawable = TextDrawable.builder().buildRound(String.valueOf(name.toUpperCase().charAt(0)), generator.getRandomColor());
            holder.ivUserImage.setImageDrawable(drawable);
        }

        @Override
        public int getItemCount() {
            return contactList.size();
        }

        private Filter getFilter() {
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence charSequence) {

                    String charString = charSequence.toString();
                    if (charString.isEmpty()) {
                        contactList = filterList;
                    } else {
                        ArrayList<FacebookContact> filteredList = new ArrayList<>();
                        for (FacebookContact contacts : contactList) {
                            if (contacts.getName().toLowerCase().contains(charString)) {
                                filteredList.add(contacts);
                            }
                        }
                        contactList = filteredList;
                    }
                    FilterResults filterResults = new FilterResults();
                    filterResults.values = contactList;
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                    contactList = (List<FacebookContact>) filterResults.values;
                    notifyDataSetChanged();
                    checkEmptyState();
                }
            };
        }

        class ContactHolder extends RecyclerView.ViewHolder {
            private FacebookContact contact;
            @BindView(R.id.tv_name)
            TextView tvName;
            @BindView(R.id.iv_user_image)
            ImageView ivUserImage;


            ContactHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            @OnClick(R.id.item_view)
            void onRowSelected(){
                inviteContact(contact);
            }

            private void inviteContact(FacebookContact contact) {
                showProgress(true);
                ModelManager.modelManager().inviteFacebookContact(dbHandler,contact, (Constants.Status iStatus) -> {
                    showProgress(false);
                    contactList.remove(contact);
                    filterList.remove(contact);
                    notifyDataSetChanged();
                    getFilter().filter("");
                    edtSearch.setText("");
                    checkEmptyState();
                }, (Constants.Status iStatus, String message) -> {
                    showProgress(false);
                    Toaster.toast(message);
                });
            }

            void bindContent(FacebookContact contacts) {
                this.contact = contacts;
            }
        }
    }

}
