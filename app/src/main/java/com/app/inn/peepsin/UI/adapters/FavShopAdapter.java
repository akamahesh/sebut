package com.app.inn.peepsin.UI.adapters;

import static com.app.inn.peepsin.Constants.Constants.kImageUrl;
import static com.app.inn.peepsin.Constants.Constants.kLatitude;
import static com.app.inn.peepsin.Constants.Constants.kLongitude;
import static com.app.inn.peepsin.Constants.Constants.kOwnerName;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.Address;
import com.app.inn.peepsin.Models.CurrentUser;
import com.app.inn.peepsin.Models.ShopModel;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.fragments.Feeds.MapDialogFragment;
import com.app.inn.peepsin.UI.fragments.Shop.FavoriteShopsFragment;
import com.app.inn.peepsin.UI.fragments.Shop.NewShopDetailsFragment;
import com.app.inn.peepsin.UI.fragments.Shop.ShoppingFragment;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import com.squareup.picasso.Picasso;
import java.util.List;

/**
 * Created by dhruv on 13/9/17.
 */

public class FavShopAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

  private List<ShopModel> shopList;
  private Context context;
  private static Switcher switcherListener;
  private static final int  MAX_SIZE = 7;
  private ShoppingFragment shoppingFragment;

  public FavShopAdapter(Context context, List<ShopModel> items, Switcher switcher,ShoppingFragment fragment) {
    switcherListener = switcher;
    shopList = items;
    this.context = context;
    this.shoppingFragment = fragment;
  }

  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View layoutView = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.card_favorite_shops, parent, false);
    return new FeedViewHolder(layoutView);
  }

  @Override
  public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
      FeedViewHolder holder = (FeedViewHolder) viewHolder;
      ShopModel item = shopList.get(position);
      //when position equal MAX_SIZE
      if(position+1==MAX_SIZE){
        holder.viewSeeAll.setVisibility(View.VISIBLE);
      }else {
        holder.viewSeeAll.setVisibility(View.GONE);
      }
      holder.bindContent(item);
      holder.tvTitle.setText(Utils.getFirstLetterInUpperCase(item.getName()));
      Address address = item.getAddress();
      String loc = "No Location Available";
      if (address != null) {
        loc = address.getCity() + "," + address.getState() + "," + address.getCountry();
      }
      holder.tvAddress.setText(loc);

      String bannerUrl = item.getBannerUrl();
      TextDrawable drawable = TextDrawable.builder()
          .buildRoundRect(String.valueOf(item.getName().toUpperCase().charAt(0)),
              ColorGenerator.MATERIAL.getRandomColor(), 5);
      if (!bannerUrl.isEmpty()) {
        Picasso.with(context)
            .load(bannerUrl)
            .fit()
            .placeholder(drawable)
            .error(drawable)
            .into(holder.ivBanner);
      } else {
        holder.ivBanner.setImageDrawable(drawable);
      }
    }

  @Override
  public int getItemCount() {
    if(shopList.size()<MAX_SIZE)
      return shopList.size();
    return MAX_SIZE;
  }

  class FeedViewHolder extends RecyclerView.ViewHolder {

    ShopModel item;
    @BindView(R.id.iv_banner_image)
    ImageView ivBanner;
    @BindView(R.id.tv_shop_name)
    TextView tvTitle;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.iv_like)
    ImageView ivLike;
    @BindView(R.id.fav_progressBar1)
    ProgressBar favProgressBar;
    @BindView(R.id.view_see_all)
    View viewSeeAll;

    FeedViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }

    @OnClick(R.id.card_view)
    void onRowSelected() {
      if (switcherListener != null) {
        switcherListener
            .switchFragment(NewShopDetailsFragment.newInstance(switcherListener, item.getId()),
                true, true);
      }
    }
    @OnClick(R.id.view_see_all)
    void onSeeAll() {
      if (switcherListener != null) {
        switcherListener
            .switchFragment(FavoriteShopsFragment.newInstance(switcherListener),
                true, true);
      }
    }

    @OnClick(R.id.iv_like)
    void onLike() {
      makeFavourite(item);
    }

    public void makeFavourite(ShopModel shopModel) {
      favProgressBar.setVisibility(View.VISIBLE);
      ivLike.setVisibility(View.INVISIBLE);
      ModelManager.modelManager()
          .setFavouriteShop(shopModel.getId(), 0, (Constants.Status iStatus) -> {
            favProgressBar.setVisibility(View.INVISIBLE);
            ivLike.setVisibility(View.VISIBLE);
            shopList.remove(getAdapterPosition());
            notifyDataSetChanged();
            shoppingFragment.refreshShopList(shopModel.getId());
            shoppingFragment.checkFavEmpty();
          }, (Constants.Status iStatus, String message) -> {
            favProgressBar.setVisibility(View.INVISIBLE);
            ivLike.setVisibility(View.VISIBLE);
            Toaster.toast(message);
          });
    }

    @OnClick(R.id.iv_location)
    void getLocation() {
      try {
        CurrentUser user = ModelManager.modelManager().getCurrentUser();
        String lat = user.getPrimaryAddress().getLatitude();
        String sLat = item.getAddress().getLatitude();
        String sLon = item.getAddress().getLongitude();
        if (!lat.isEmpty() && !sLat.isEmpty()) {
          MapDialogFragment mapDialogFragment = MapDialogFragment.newInstance();
          Bundle bdl = new Bundle();
          bdl.putString(kLatitude, sLat);
          bdl.putString(kLongitude, sLon);
          bdl.putString(kOwnerName, item.getName());
          bdl.putString(kImageUrl, item.getBannerUrl());
          mapDialogFragment.setArguments(bdl);
          mapDialogFragment.setCancelable(true);
          mapDialogFragment.show(((AppCompatActivity) context).getSupportFragmentManager(),
              mapDialogFragment.getClass().getSimpleName());
        } else {
          Toaster.toast("Sorry Lat Long UnAvailable");
        }
      } catch (Exception e) {
        Toaster.toast("Sorry Lat Long UnAvailable");
        e.printStackTrace();
      }
    }

    private void bindContent(ShopModel item) {
      this.item = item;
    }
  }
}



