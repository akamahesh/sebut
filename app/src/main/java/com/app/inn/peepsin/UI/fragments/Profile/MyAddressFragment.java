package com.app.inn.peepsin.UI.fragments.Profile;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.Address;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.helpers.DividerItemRecyclerDecoration;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kFlag;

/**
 * Created by Harsh on 5/10/2017.
 */

public class MyAddressFragment extends Fragment {
    @BindView(R.id.recycler_view_feeds)
    RecyclerView recyclerViewFeeds;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.empty_view)
    View emptyView;
    @BindView(R.id.tv_empty_message)
    TextView tvEmptyTextMessage;
    @BindView(R.id.tv_empty_title)
    TextView tvEmptyTitle;
    @BindView(R.id.btn_skip)
    Button btnSkip;
    @BindView(R.id.btn_back)
    ImageButton btnBack;
    ProgressDialog progressDialog;
    private AddressAdapter addressAdapter;
    private static Switcher profileSwitcherListener;
    private static Integer TYPE;
    Paint p = new Paint();
    //AppBarLayout appbarLayout;


    /**
     * @param switcher listener
     * @param type     if type = 0 , make btnSkip  visible
     * @return @link MyAddressFragment
     */
    public static Fragment newInstance(Switcher switcher, int type) {
        profileSwitcherListener = switcher;
        Fragment fragment = new MyAddressFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(kFlag, type);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null)
            TYPE = bundle.getInt(kFlag);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_my_address, container, false);
        ButterKnife.bind(this, view);
        /*appbarLayout=((HomeActivity) getActivity()).getTabLayout();
        Utils.isprofileClicked=1;*/
        progressDialog = Utils.generateProgressDialog(getContext(), true);
        addressAdapter = new AddressAdapter(new ArrayList<>());
        recyclerViewFeeds.setHasFixedSize(true);
        recyclerViewFeeds.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerViewFeeds.setAdapter(addressAdapter);
        recyclerViewFeeds.addItemDecoration(new DividerItemRecyclerDecoration(getActivity(), R.drawable.canvas_recycler_divider));
        tvTitle.setText(getString(R.string.my_addresses));
        if (TYPE.equals(0)) {
            btnSkip.setVisibility(View.VISIBLE);
            btnBack.setVisibility(View.GONE);
        }

        swipeToDismissTouchHelper.attachToRecyclerView(recyclerViewFeeds);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData();
    }

    ItemTouchHelper swipeToDismissTouchHelper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            int swipedPosition = viewHolder.getAdapterPosition();
            Address address = addressAdapter.getItem(swipedPosition);
            deleteAddress(address, address.getAddressId(), swipedPosition,address.getPrimary());
        }

        @Override
        public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
            Bitmap icon;
            if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
                View itemView = viewHolder.itemView;
                float height = (float) itemView.getBottom() - (float) itemView.getTop();
                float width = height / 3;

                p.setColor(Color.parseColor("#D32F2F"));
                RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(), (float) itemView.getRight(), (float) itemView.getBottom());
                c.drawRect(background, p);
                icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_close_white_24dp);
                RectF icon_dest = new RectF((float) itemView.getRight() - 2 * width, (float) itemView.getTop() + width, (float) itemView.getRight() - width, (float) itemView.getBottom() - width);
                c.drawBitmap(icon, null, icon_dest, p);

            }
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
        }
    });


    @Override
    public void onDetach() {
        super.onDetach();
        profileSwitcherListener = null;
    }

    private void loadData() {
        showProgress(true);
        ModelManager.modelManager().getMyAddress((Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<Address>> genericList) -> {
            addressAdapter.addAddress(genericList.getObject());
            checkEmptyScreen();
            showProgress(false);
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Toaster.toast(message);
            checkEmptyScreen();
        });
    }

    private void deleteAddress(Address address, int addressId, int swipedPosition,boolean isPrimary) {
        showProgress(true);
        ModelManager.modelManager().getDeleteAddress(addressId, (Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<Address>> genericList) -> {
            addressAdapter.addAddress(genericList.getObject());
            if(isPrimary)
                ModelManager.modelManager().getCurrentUser().setPrimaryAddress(null);
            checkEmptyScreen();
            showProgress(false);
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Toaster.toast(message);
            checkEmptyScreen();
            addressAdapter.notifyDataSetChanged();
        });
    }

/*    private void updateProfile() {
        ModelManager.modelManager().getMyProfile(ModelManager.modelManager().getCurrentUser().getAuthToken(),(Constants.Status iStatus) -> {
            showProgress(false);
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Log.e("Update Address",message);
        });
    }*/

    @OnClick(R.id.btn_back)
    void onBack() {
        getFragmentManager().popBackStack();
        /*appbarLayout.setVisibility(View.VISIBLE);
        Utils.isprofileClicked=0;*/
        Utils.isAddressUpdated = 1;
    }

    @Override
    public void onResume() {
        super.onResume();
        // appbarLayout.setVisibility(View.GONE);

    }

    @OnClick(R.id.fab)
    public void addNewAddress() {
        if (profileSwitcherListener != null)
            profileSwitcherListener.switchFragment(AddNewAddressFragment.newInstance(1, null,profileSwitcherListener, null), true, true);
    }

    private void checkEmptyScreen() {
        if (addressAdapter.getItemCount() > 0) {
            emptyView.setVisibility(View.GONE);
            btnSkip.setText("Next");
        } else {
            emptyView.setVisibility(View.VISIBLE);
        }
    }

    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) progressDialog.cancel();
        }
    }


    class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.StuffHolder> {
        private List<Address> addressList;

        AddressAdapter(List<Address> addresses) {
            this.addressList = addresses;
        }

        @Override
        public AddressAdapter.StuffHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_my_address_list, parent, false);
            return new StuffHolder(layoutView);
        }

        @Override
        public void onBindViewHolder(StuffHolder holder, int position) {
            Address address = addressList.get(position);
            holder.bindContent(address);
            String addressLineOne = address.getStreetAddress() + ", " + address.getCity() + ", " + address.getState();
            String addressLineTwo = address.getCountry() + ", " + address.getZipCode();
            String phone = address.getPhone();

            if (!address.getFullName().isEmpty())
                holder.tvName.setText(address.getFullName());
            else holder.tvName.setVisibility(View.GONE);
            if (!address.getStreetAddress().isEmpty() && !address.getCity().isEmpty())
                holder.tvAddressId.setText(addressLineOne);
            else holder.tvAddressId.setVisibility(View.GONE);
            if (!address.getCountry().isEmpty() && !address.getZipCode().isEmpty())
                holder.tvCity.setText(addressLineTwo);
            else holder.tvCity.setVisibility(View.GONE);
            if (!phone.isEmpty())
                holder.tvPhone.setText(phone);
            else holder.tvPhone.setVisibility(View.GONE);


            if (address.getPrimary()) {
                holder.ivPrimaryImage.setVisibility(View.VISIBLE);
            } else {
                holder.ivPrimaryImage.setVisibility(View.GONE);
            }
        }

        @Override
        public int getItemCount() {
            return addressList.size();
        }

        Address getItem(int index) {
            return addressList.get(index);
        }

        void addAddress(List<Address> addresss) {
            addressList.clear();
            addressList.addAll(addresss);
            notifyDataSetChanged();
        }

        class StuffHolder extends RecyclerView.ViewHolder {
            private Address address;
            @BindView(R.id.iv_primary_image)
            ImageView ivPrimaryImage;
            @BindView(R.id.tv_address_line_one)
            TextView tvAddressId;
            @BindView(R.id.tv_address_line_two)
            TextView tvCity;
            @BindView(R.id.tv_phone_no)
            TextView tvPhone;
            @BindView(R.id.tv_name)
            TextView tvName;

            StuffHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            void bindContent(Address address) {
                this.address = address;
            }

            @OnClick(R.id.item_view)
            void editAddress() {
                if (profileSwitcherListener != null)
                    profileSwitcherListener.switchFragment(AddNewAddressFragment.newInstance(2, address,profileSwitcherListener, null), true, true);
            }
        }
    }


}
