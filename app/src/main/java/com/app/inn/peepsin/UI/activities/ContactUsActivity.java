package com.app.inn.peepsin.UI.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindColor;
import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.BaseManager.DateManager;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.adapters.InfinityAdapter;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.MESSAGE_DIRECTION;
import static com.app.inn.peepsin.Constants.Constants.kMessage;
import static com.app.inn.peepsin.Constants.Constants.kMessageDirection;
import static com.app.inn.peepsin.Constants.Constants.kShopId;
import static com.app.inn.peepsin.Constants.Constants.kTimeStamp;

public class ContactUsActivity extends AppCompatActivity {

    @BindColor(R.color.more_color)
    int activeColor;
    @BindColor(R.color.text_color_regular)
    int inactiveColor;
    @BindView(R.id.recycler_view_chat)
    RecyclerView recyclerViewChat;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.edt_message)
    EditText edtMessage;
    @BindView(R.id.iv_send)
    TextView tvSend;
    @BindView(R.id.empty_view)
    View emptyView;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;
    Integer mShopId;
    private Integer mPage = 1;

    private ChatAdapter chatAdapter;
    private ProgressDialog progressDialog;
    private List<HashMap<String, Object>> chatMapList;
    private Queue<HashMap<String,Object>> mapQueue;

    public static Intent getIntent(Context context, Integer shopId) {
        Intent intent = new Intent(context, ContactUsActivity.class);
        intent.putExtra(kShopId, shopId);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
        setContentView(R.layout.activity_contact_us);
        ButterKnife.bind(this);
        if (getIntent() != null)
            mShopId = getIntent().getIntExtra(kShopId, 0);
        chatMapList = new ArrayList<>();
        progressDialog = Utils.generateProgressDialog(this, false);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerViewChat.setLayoutManager(layoutManager);
        chatAdapter = new ChatAdapter(chatMapList, ModelManager.modelManager().getCurrentUser().getFullName(), recyclerViewChat);
        recyclerViewChat.setAdapter(chatAdapter);
        edtMessage.requestFocus();
        edtMessage.addTextChangedListener(textWatcher);
        tvTitle.setText("Contact Us");
        edtMessage.setOnEditorActionListener(onEditorActionListener);
        swipeRefreshLayout.setOnRefreshListener(onRefreshListener);
        loadData(mShopId, mPage++);

    }

    SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            loadData(mShopId,mPage++);
        }
    };

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
    }

    @OnClick(R.id.btn_back)
    void onBack() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void loadData(Integer mShopId, int page) {
        swipeRefreshLayout.setRefreshing(true);
        ModelManager.modelManager().receiveMessageHistory(mShopId, page, (Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<HashMap<String, Object>>> genericResponse) -> {
            swipeRefreshLayout.setRefreshing(false);
            chatMapList.addAll(0,genericResponse.getObject());
            chatAdapter.notifyDataSetChanged();
            checkEmptyState();
        }, (Constants.Status iStatus, String message) -> {
            Toaster.toast(message);
            swipeRefreshLayout.setRefreshing(false);
            checkEmptyState();

        });
    }

    private void checkEmptyState() {
        if (chatAdapter.getItemCount() > 0) {
            emptyView.setVisibility(View.GONE);
        } else {
            emptyView.setVisibility(View.VISIBLE);
        }
    }

    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            String text = String.valueOf(s);
            if(text.isEmpty())
                tvSend.setTextColor(inactiveColor);
            else tvSend.setTextColor(activeColor);
        }
    };

    EditText.OnEditorActionListener onEditorActionListener = (v, actionId, event) -> {
        if (actionId == EditorInfo.IME_ACTION_SEND) {
            send();
            return true;
        }
        return false;
    };

    @OnClick(R.id.iv_send)
    void send() {
        String msgTxt = edtMessage.getText().toString().trim();
        if (msgTxt.isEmpty())
            return;
        sendMessage(mShopId, msgTxt);
        edtMessage.setText("");
    }

    private void sendMessage(Integer shopId, String messagi) {
        ModelManager.modelManager().sendMessage(shopId, messagi, (Constants.Status iStatus, GenricResponse<HashMap<String, Object>> genericResponse) -> {
            HashMap<String, Object> hashMapM = genericResponse.getObject();
            hashMapM.put(MESSAGE_DIRECTION, 0);
            chatMapList.add(genericResponse.getObject());
            chatAdapter.notifyDataSetChanged();
            recyclerViewChat.smoothScrollToPosition(chatAdapter.getItemCount());
            checkEmptyState();
        }, (Constants.Status iStatus, String message) -> {
            Toaster.toast(message);
            showProgress(false);
            checkEmptyState();
        });
    }


    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null)
                progressDialog.cancel();
        }
    }

    class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ChatViewHolder> {

        private List<HashMap<String, Object>> chatList;
        private ColorGenerator generator = ColorGenerator.MATERIAL;
        private String senderName;
        private String receiverName;

        ChatAdapter(List<HashMap<String, Object>> chatList, String receiverName, RecyclerView recyclerViewChat) {
            this.chatList = chatList;
            senderName = ModelManager.modelManager().getCurrentUser().getFirstName();
            this.receiverName = receiverName;
        }

        @Override
        public ChatViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View layoutView;
                if (viewType == 0) {
                    layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_outgoing_text, parent, false);
                } else {
                    layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_incoming_text, parent, false);
                }

            return new ChatViewHolder(layoutView);
        }

        @Override
        public void onBindViewHolder(ChatAdapter.ChatViewHolder holder, int position) {
            HashMap<String, Object> chat = chatList.get(position);
            int viewType = getItemViewType(position);
            String mgsTxt = (String) chat.get(kMessage);
            String timeStamp = (String) chat.get(kTimeStamp);
            timeStamp = DateManager.dispalyValue(timeStamp);
            holder.tvMessage.setText(mgsTxt);
            holder.tvTime.setText(timeStamp);
            String textD = "Buyer";
            if (viewType == Constants.MessageDirection.send.getValue())
                textD = senderName;
            else
                textD = (receiverName != null) ? receiverName : textD;

            TextDrawable drawable = TextDrawable.builder().buildRound(String.valueOf(textD.toUpperCase().charAt(0)), generator.getRandomColor());
            holder.ivUserImage.setImageDrawable(drawable);
        }

        @Override
        public int getItemCount() {
            return chatList.size();
        }

        @Override
        public int getItemViewType(int position) {
            HashMap<String, Object> map = chatList.get(position);
            int direction = (int) map.get(MESSAGE_DIRECTION);
            return direction;
        }

        class ChatViewHolder extends RecyclerView.ViewHolder {
            private HashMap<String, Object> chat;
            @BindDrawable(R.drawable.ic_schedule_grey_500_18dp)
            Drawable drawableSending;
            @BindDrawable(R.drawable.ic_check_grey_500_18dp)
            Drawable drawableSend;
            @BindDrawable(R.drawable.ic_done_all_grey_500_18dp)
            Drawable drawableSent;


            @BindView(R.id.tv_message)
            TextView tvMessage;
            @BindView(R.id.tv_time)
            TextView tvTime;
            @BindView(R.id.iv_user_image)
            ImageView ivUserImage;


            ChatViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            void bindContent(HashMap<String, Object> chat) {
                this.chat = chat;
            }
        }
    }

}
