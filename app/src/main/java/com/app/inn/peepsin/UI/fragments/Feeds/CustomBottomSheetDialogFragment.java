package com.app.inn.peepsin.UI.fragments.Feeds;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.dialogFragments.TimeZoneListDialogFragment;
import com.app.inn.peepsin.UI.fragments.Board.ShareBoardFragment;
import com.app.inn.peepsin.UI.helpers.Toaster;

import static com.app.inn.peepsin.Constants.Constants.kId;
import static com.app.inn.peepsin.Constants.Constants.kType;

/**
 * Created by akamahesh on 8/5/17.
 */

public class CustomBottomSheetDialogFragment extends BottomSheetDialogFragment{

    public static int TYPE= 0;
    public static final int SHARE_FRIEND_TYPE=2;
    public static final int SHARE_BOARD_TYPE=3;

    private Dialog dialogInstance;
    private int id;

    public static CustomBottomSheetDialogFragment newInstance(int type, int id){
        CustomBottomSheetDialogFragment customBottomSheetDialogFragment =  new CustomBottomSheetDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(kType,type);
        bundle.putInt(kId,id);
        customBottomSheetDialogFragment.setArguments(bundle);
        return customBottomSheetDialogFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if(bundle!=null){
            TYPE= bundle.getInt(kType);
            id = bundle.getInt(kId);
        }
    }

    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {
        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }
        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {}
    };


    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        Log.v("bottomsup" ," : setupDialog");
        dialogInstance = dialog;
        View contentView = View.inflate(getContext(), R.layout.dialog_modal, null);
        dialog.setContentView(contentView);
        CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams)
                ((View) contentView.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = layoutParams.getBehavior();
        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.dialog_modal, container, false);
        if(TYPE==SHARE_FRIEND_TYPE)
            changeFragment(ShareProductFragment.newInstance(id,dialogInstance),true,false);
        else if(TYPE==SHARE_BOARD_TYPE)
            changeFragment(ShareBoardFragment.newInstance(id,dialogInstance,this),true,false);
        return view;
    }

    /**
     * Change the current displayed fragment by a new one.
     * - if the fragmebt is in backstack, it will pop it
     * - if the fragment is already displayed (trying to change the fragment with the same), it will not do anything
     *
     * @param fragment        the new fragment display
     * @param saveInBackstack if we want the fragment to be in backstack
     * @param animate         if we want a nice animation or not
     */
    public void changeFragment(Fragment fragment, boolean saveInBackstack, boolean animate) {
        String backStateName = fragment.getClass().getName();
        try {
            FragmentManager fragmentManager = getChildFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            if (animate) {
                transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
            }
            transaction.replace(R.id.container, fragment, backStateName);
            if (saveInBackstack) {
                transaction.addToBackStack(backStateName);
            }

            transaction.commit();

        } catch (IllegalStateException e) {
            Toaster.toast(e.getMessage());
        }
    }


    public void getCount(){
        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK,getActivity().getIntent());
    }

}
