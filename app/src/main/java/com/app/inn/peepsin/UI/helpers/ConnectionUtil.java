package com.app.inn.peepsin.UI.helpers;

import com.app.inn.peepsin.Models.Connection;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mahesh on 5/6/17.
 */

public class ConnectionUtil {

    public static List<Connection> filterConnectionList(List<Connection> connectionList) {
        List<Connection> list= new ArrayList<>();
        for (Connection connection : connectionList) {
            if(!connection.getIsBlocked())
                list.add(connection);
        }
        return list;
    }

}
