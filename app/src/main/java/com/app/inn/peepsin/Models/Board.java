package com.app.inn.peepsin.Models;

import android.util.Log;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by mahesh on 7/6/17.
 */

public class Board extends BaseModel implements Serializable {

    private Integer boardId;
    private Integer userId;
    private String description;
    private String firstName;
    private String lastName;
    private String profilePicURL;
    private String boardTime;
    private Boolean isLike;
    private Integer likeCount;
    private Integer commentCount;
    private Integer shareCount;
    private Comment lastComment;


    public Board(JSONObject jsonResponse) {
        this.boardId         = getValue(jsonResponse, kBoardId,         Integer.class);
        this.userId          = getValue(jsonResponse, kUserId,          Integer.class);
        this.firstName       = getValue(jsonResponse, kFirstName,       String.class);
        this.lastName        = getValue(jsonResponse, kLastName,        String.class);
        this.description     = getValue(jsonResponse, kDescription,     String.class);
        this.profilePicURL   = getValue(jsonResponse, kProfilePicUrl,   String.class);
        this.boardTime       = getValue(jsonResponse, kBoardtTime,      String.class);
        this.isLike          = getValue(jsonResponse, kIsLike,          Boolean.class);
        this.likeCount       = getValue(jsonResponse, kLikeCount,       Integer.class);
        this.commentCount    = getValue(jsonResponse, kCommentCount,    Integer.class);
        this.shareCount    = getValue(jsonResponse, kShareCount,    Integer.class);
        try {
            this.lastComment     = new Comment( getValue(jsonResponse, kLastComment, JSONObject.class));
        }catch (Exception e){
            Log.e(getClass().getSimpleName(),e.getMessage()+" last comment");
            lastComment = null;
        }
    }


    public String getBoardTime() {
        return boardTime;
    }

    public void setBoardTime(String boardTime) {
        this.boardTime = boardTime;
    }

    public static class Skill{
        Integer userSkillId;
        String skill;
        String skillArea;

        public Skill(JSONObject skillJsonReponse) throws ClassCastException {
            this.userSkillId            = getValue(skillJsonReponse, kUserSkillId,    Integer.class);
            this.skill                  = getValue(skillJsonReponse, kSkill,          String.class);
            this.skillArea              = getValue(skillJsonReponse, kSkillArea,      String .class);
        }

        public Integer getUserSkillId() {
            return userSkillId;
        }

        public void setUserSkillId(Integer userSkillId) {
            this.userSkillId = userSkillId;
        }

        public String getSkill() {
            return skill;
        }

        public void setSkill(String skill) {
            this.skill = skill;
        }

        public String getSkillArea() {
            return skillArea;
        }

        public void setSkillArea(String skillArea) {
            this.skillArea = skillArea;
        }
    }

    public Integer getBoardId() {
        return boardId;
    }

    public Integer getUserId() {
        return userId;
    }

    public String getFullName() {
        return firstName+" "+lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getDescription() {
        return description;
    }

    public String getProfilePicURL() {
        return profilePicURL;
    }

    public Boolean getLike() {
        return isLike;
    }

    public void setLike(Boolean like) {
        isLike = like;
    }

    public Integer getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(Integer likeCount) {
        this.likeCount = likeCount;
    }

    public Integer getCommentCount() {
        return commentCount;
    }

    public Integer getShareCount() {
        return shareCount;
    }

    public Comment getLastComment() {
        return lastComment;
    }

}
