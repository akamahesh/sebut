package com.app.inn.peepsin.UI.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.BaseManager.ExceptionHandler;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.OrderHistory;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.adapters.OrderSummaryAdapter;
import com.app.inn.peepsin.UI.helpers.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Harsh on 7/19/2017.
 */

public class OrderSummaryActivity extends AppCompatActivity {

    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.expandable_list_view)
    ExpandableListView expandableListView;
    @BindView(R.id.ib_back)
    ImageView backImage;
    private String TAG = getClass().getName();
    ProgressDialog progressDialog;
    Integer orderId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_summary);
        ButterKnife.bind(this);
        backImage.setVisibility(View.GONE);
        progressDialog = Utils.generateProgressDialog(this, false);
        //Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        tvTitle.setText(R.string.order_summary);
        orderId = getIntent().getIntExtra("OrderId", 0);
        loadOrderSummary(orderId);
    }


   /* @OnClick(R.id.ib_back)
    public void onBack() {
        onBackPressed();
    }*/

    public void loadOrderSummary(Integer orderId) {
       showProgress(true);
        ModelManager.modelManager().getSummaryAfterPayment(orderId, (Constants.Status iStatus, GenricResponse<OrderHistory> genericResponse) -> {
            showProgress(false);
            OrderHistory orderHistory = genericResponse.getObject();
            setOrderSummary(orderHistory);
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Log.e(TAG, message);
        });
    }

    @OnClick(R.id.btn_continue_to_shopping)
    void continueShopping(){
        Intent in= new Intent(OrderSummaryActivity.this, HomeActivity.class);
        startActivity(in);

    }

    public void setOrderSummary(OrderHistory orderHistory) {
        OrderSummaryAdapter orderSummaryAdapter = new OrderSummaryAdapter(this, orderHistory);
        expandableListView.setAdapter(orderSummaryAdapter);
    }

    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) progressDialog.cancel();
        }
    }
}
