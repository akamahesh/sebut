package com.app.inn.peepsin.UI.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.app.inn.peepsin.Managers.BaseManager.ExceptionHandler;
import com.app.inn.peepsin.Models.PaymentModel;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.adapters.PaymentAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PaymentActivity extends BaseActivity {

    @BindView(R.id.recycler_view_payment_posts)
    RecyclerView recyclerPaymentItem;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    private LinearLayoutManager mLayoutManager;
    private PaymentAdapter paymentAdapter;

    public static Intent getIntent(Context context) {
        return new Intent(context, PaymentActivity.class);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
        setContentView(R.layout.activity_payment);
        ButterKnife.bind(this);

        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        tvTitle.setText("Cart");


        List<PaymentModel> data = fill_with_data();
        paymentAdapter = new PaymentAdapter(data,getApplicationContext());
        recyclerPaymentItem.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        recyclerPaymentItem.setLayoutManager(mLayoutManager);
        recyclerPaymentItem.setAdapter(paymentAdapter);


    }



    public List<PaymentModel> fill_with_data() {
        List<PaymentModel> data = new ArrayList<>();
        data.add(new PaymentModel("Electronics"));
        data.add(new PaymentModel("Clothes"));
        data.add(new PaymentModel("Formal"));
        data.add(new PaymentModel("Laptops"));
        data.add(new PaymentModel("Casual"));
        data.add(new PaymentModel("Mobiles"));
        data.add(new PaymentModel("Sports Wear"));
        data.add(new PaymentModel("Laptops"));
        data.add(new PaymentModel("Casual"));
        data.add(new PaymentModel("Foot Wear"));
        data.add(new PaymentModel("Mobiles"));
        data.add(new PaymentModel("Electronics"));
        return data;
    }



}
