package com.app.inn.peepsin.UI.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.Feeds;
import com.app.inn.peepsin.Models.ShoppingCart;
import com.app.inn.peepsin.Models.ShoppingProduct;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.fragments.Feeds.ProductDetailFragment;
import com.app.inn.peepsin.UI.fragments.Profile.ShoppingCartFragment;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import com.squareup.picasso.Picasso;
import java.util.List;

/**
 * Created by root on 6/7/17.
 */
public class CartProductAdapter extends RecyclerView.Adapter<CartProductAdapter.ViewHolder> {

  private List<ShoppingProduct> productList;
  private Context mContext;
  int count = 0;
  private static Switcher switcherListener;
  private ProgressDialog progressDialog;
  private ShoppingCartFragment fragment;

  public CartProductAdapter(Switcher switcher, Context context, List<ShoppingProduct> productShop,
      ShoppingCartFragment fragment) {
    this.mContext = context;
    this.productList = productShop;
    this.fragment = fragment;
    switcherListener = switcher;
    progressDialog = Utils.generateProgressDialog(context, false);

  }

  @Override
  public CartProductAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View itemView = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.row_new_shopping_product_layout, parent, false);
    return new CartProductAdapter.ViewHolder(itemView);
  }

  @Override
  public void onBindViewHolder(final CartProductAdapter.ViewHolder holder, int position) {

    ShoppingProduct product = productList.get(position);
    holder.bindContent(product);
    // Product Details
    holder.tvProductName.setText(product.getName());
    String price = mContext.getString(R.string.product_price) + "0/-";
    if (!product.getPrice().isEmpty()) {
      price = mContext.getString(R.string.product_price) + "\t" + product.getPrice();
    }
    holder.tvPrice.setText(price);
    String quantity = mContext.getString(R.string.product_quantity) + "0";
    if (product.getQuantity() != 0) {
      quantity = mContext.getString(R.string.product_quantity) + "\t" + product.getQuantity();
    }
    count = product.getQuantity();
    // String quantity_value = mContext.getString(R.string.product_quantity)+"0";
  //  holder.tvQuantity.setText(quantity);
    holder.tvAddQuantity.setText(product.getQuantity().toString());
   // holder.tvDiscount.setText(product.getDiscountTagLine());
    holder.tvSpecialOffer.setText(product.getDiscountTagLine());
    Picasso.with(mContext)
        .load(product.getCoverImageUrl())
        .fit()
        .placeholder(R.drawable.img_product_placeholder)
        .into(holder.ivProductPic);
  }

  @Override
  public int getItemCount() {
    return productList.size();
  }

  public class ViewHolder extends RecyclerView.ViewHolder {

    ShoppingProduct item;
    @BindView(R.id.iv_product)
    ImageView ivProductPic;
    @BindView(R.id.tv_product_name)
    TextView tvProductName;
    @BindView(R.id.tv_special_offer)
    TextView tvSpecialOffer;
    @BindView(R.id.tv_product_price)
    TextView tvPrice;
   /* @BindView(R.id.tv_discount_tag)
    TextView tvDiscount;*/
    @BindView(R.id.tv_addquantity)
    TextView tvAddQuantity;


    ViewHolder(View view) {
      super(view);
      ButterKnife.bind(this, view);
    }

    @OnClick(R.id.imgbtn_remove)
    void onRemoveItem() {
      ModelManager.modelManager().removeShoppingCart(item.getId(), (Constants.Status iStatus) -> {
        productList.remove(getAdapterPosition());
        notifyItemRemoved(getAdapterPosition());
        notifyDataSetChanged();
        if (productList.size() == 0) {
          fragment.loadShoppingCart();
        }
          //((ShoppingCartActivity)mContext).loadShoppingCart();
        else {
          fragment.cartUpdate();
        }
        //((ShoppingCartActivity)mContext).cartUpdate();
      }, (Constants.Status iStatus, String message) -> Toaster.toast(message));
    }

    @OnClick(R.id.btn_remove)
    void onRemove() {
      ModelManager.modelManager().removeShoppingCart(item.getId(), (Constants.Status iStatus) -> {
        productList.remove(getAdapterPosition());
        notifyItemRemoved(getAdapterPosition());
        notifyDataSetChanged();
        if (productList.size() == 0) {
          fragment.loadShoppingCart();
        }
          //((ShoppingCartActivity)mContext).loadShoppingCart();
        else {
          fragment.cartUpdate();
        }
        //((ShoppingCartActivity)mContext).cartUpdate();
      }, (Constants.Status iStatus, String message) -> Toaster.toast(message));
    }

    @OnClick(R.id.imgbtn_add)
    void onAdd() {
      ModelManager.modelManager().addRemoveProduct(item.getId(), 1,
          (Constants.Status iStatus, GenricResponse<ShoppingCart> genericResponse) -> {
            fragment.refreshNewData(genericResponse);
            //((ShoppingCartActivity)mContext).refreshNewData(genericResponse);
          }, (Constants.Status iStatus, String message) -> Toaster.toast(message));
    }

    @OnClick(R.id.imgbtn_minus)
    void onSubtract() {
      if (count > 1) {
        ModelManager.modelManager().addRemoveProduct(item.getId(), 0,
            (Constants.Status iStatus, GenricResponse<ShoppingCart> genericResponse) -> {
              fragment.refreshNewData(genericResponse);
              //((ShoppingCartActivity) mContext).refreshNewData(genericResponse);
            }, (Constants.Status iStatus, String message) -> Toaster.toast(message));
      }
    }

    @OnClick(R.id.card_detail)
    void onRowSelected() {
      getFeedDetails(item.getId());
    }

    private void getFeedDetails(int productId) {
      showProgress(true);
      ModelManager.modelManager().getProductDetails(productId,
          (Constants.Status iStatus, GenricResponse<Feeds> genericResponse) -> {
            showProgress(false);
            if (switcherListener != null) {
              switcherListener.switchFragment(ProductDetailFragment
                      .newInstance(productId, genericResponse.getObject(), switcherListener), true,
                  true);
            }
          }, (Constants.Status iStatus, String message) -> {
            Toaster.toast(message);
            showProgress(false);
          });
    }

    public void bindContent(ShoppingProduct product) {
      this.item = product;
    }
  }

  private void showProgress(boolean b) {
    if (b) {
      progressDialog.show();
    } else {
      if (progressDialog != null) {
        progressDialog.cancel();
      }
    }
  }
}
