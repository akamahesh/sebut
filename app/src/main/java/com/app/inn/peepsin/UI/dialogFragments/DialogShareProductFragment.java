package com.app.inn.peepsin.UI.dialogFragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.app.inn.peepsin.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kWantToShare2ndLevel;
import static com.app.inn.peepsin.Constants.Constants.kWantToShare3ndLevel;

/**
 * Created by dhruv on 1/8/17.
 */

public class DialogShareProductFragment extends DialogFragment {

    @BindView(R.id.share_2nd_checkBox)
    CheckBox checkBox2Share;
    @BindView(R.id.share_3rd_checkbox)
    CheckBox checkBox3Share;
    int share2ndLevel = 0,share3rdLevel = 0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL,  R.style.CustomDialog);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_share_product_view, container, false);
        ButterKnife.bind(this, rootView);
        checkBox2Share.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if(isChecked)
                share2ndLevel=1;
            else
                share2ndLevel=0;
        });

        checkBox3Share.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if(isChecked)
                share3rdLevel=1;
            else
                share3rdLevel=0;
        });
        return rootView;
    }

    @OnClick(R.id.btn_continue)
    public void onContinue() {
        Intent in = getActivity().getIntent();
        in.putExtra(kWantToShare2ndLevel,share2ndLevel);
        in.putExtra(kWantToShare3ndLevel,share3rdLevel);
        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK,in);
        dismiss();
    }

    @OnClick(R.id.btn_cancel)
    public void onCancel(){
        dismiss();
    }


}
