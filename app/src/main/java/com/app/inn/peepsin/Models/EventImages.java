package com.app.inn.peepsin.Models;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by dhruv on 25/9/17.
 */

public class EventImages extends BaseModel implements Serializable {

    private int imageId;
    private String imageUrl;
    private int eventType;

    public EventImages(JSONObject jsonResponse){
        this.imageId = getValue(jsonResponse,kImageId,  Integer.class);
        this.imageUrl = getValue(jsonResponse,kImageURL, String.class);
        this.eventType = getValue(jsonResponse,kEventType, Integer.class);
    }

    public int getImageId() {
        return imageId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public int getEventType() {
        return eventType;
    }
}
