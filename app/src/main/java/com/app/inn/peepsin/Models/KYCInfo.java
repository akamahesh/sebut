package com.app.inn.peepsin.Models;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by dhruv on 21/7/17.
 */

public class KYCInfo extends BaseModel implements Serializable {

    private String PAN;
    private String TAN;
    private String GST;

    public KYCInfo(String pan, String tin , String vat){
        this.PAN=pan;
        this.TAN=tin;
        this.GST=vat;
    }

    public KYCInfo(JSONObject jsonResponse){
        this.PAN = getValue(jsonResponse, kPAN, String.class);
        this.TAN = getValue(jsonResponse, kTAN, String.class);
        this.GST = getValue(jsonResponse, kGST, String.class);
    }

    public String getPAN() {
        return PAN;
    }

    public String getTAN() {
        return TAN;
    }

    public String getGST() {
        return GST;
    }
}
