package com.app.inn.peepsin.UI.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.CurrentUser;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.helpers.Utils;


public class SplashActivity extends BaseActivity {

    private static final String TAG = SplashActivity.class.getSimpleName();

    public static Intent getIntent(Context context) {
        return new Intent(context, SplashActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CurrentUser currentUser = ModelManager.modelManager().getCurrentUser();
       /* if(true){
          startActivity(AppPermissionActivity.getIntent(SplashActivity.this,AppPermissionActivity.Type.SIGNIN));
        } else */

        // Generic AuthToken
        ModelManager.modelManager().getGenericAuthToken((Constants.Status iStatus, GenricResponse<String> genricResponse) -> {
            getUser(currentUser);
        }, (Constants.Status iStatus, String message) -> {
            Log.e(getClass().getSimpleName(), message);
            getUser(currentUser);
        });
    }

    private void getUser(CurrentUser currentUser) {
        if (currentUser == null) {
            // startActivity(AppPermissionActivity.getIntent(SplashActivity.this,AppPermissionActivity.Type.SIGNIN));
            startActivity(FrontActivity.getIntent(SplashActivity.this));
            finish();
        } else {
            setContentView(R.layout.activity_splash);
            String versionName = Utils.getVersionName(this, false);
            TextView tvVersion = (TextView) findViewById(R.id.tv_version);
            tvVersion.setText(versionName + "");
            final Handler handler = new Handler();
            handler.postDelayed(() -> {
                startActivity(HomeActivity.getIntent(SplashActivity.this));
                finish();
            }, 2000);

            //updateModels(currentUser);
        }
    }

}
