package com.app.inn.peepsin.Models;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by akamahesh on 15/5/17.
 */

public class ShopAddress extends BaseModel implements Serializable {
    private Integer addressId;
    private String fullName;
    private String phone;
    private String streetAddress;
    private String city;
    private String state;
    private String country;
    private String zipCode;
    private Boolean isPrimary;
    private String latitude;
    private String longitude;

    public ShopAddress(JSONObject jsonResponse) {
        this.addressId                = getValue(jsonResponse, kAddressId,            Integer.class);
        this.fullName                 = getValue(jsonResponse, kFullName,             String.class);
        this.phone                    = getValue(jsonResponse, kPhone,                String.class);
        this.streetAddress            = getValue(jsonResponse, kStreetAddress,        String.class);
        this.city                     = getValue(jsonResponse, kCity,                 String.class);
        this.state                    = getValue(jsonResponse, kState,                String.class);
        this.country                  = getValue(jsonResponse, kCountry,              String.class);
        this.zipCode                  = getValue(jsonResponse, kZipcode,              String.class);
        this.latitude                 = getValue(jsonResponse, kLatitude,             String.class);
        this.longitude                = getValue(jsonResponse, kLongitude,            String.class);
        this.isPrimary                = getValue(jsonResponse, kIsPrimary,            Boolean.class);
    }


    public Integer getAddressId() {
        return addressId;
    }

    public void setAddressId(Integer addressId) {
        this.addressId = addressId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public Boolean getPrimary() {
        return isPrimary;
    }

    public void setPrimary(Boolean primary) {
        isPrimary = primary;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
