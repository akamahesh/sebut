package com.app.inn.peepsin.Models;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Harsh on 5/25/2017.
 */

public class Search extends BaseModel implements Serializable {
    private Integer categoryId;
    private String searchString;

    public Search(JSONObject jsonResponse) {
        this.categoryId = getValue(jsonResponse, kCategoryId, Integer.class);
        this.searchString = getValue(jsonResponse, kSearchString, String.class);
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }
}
