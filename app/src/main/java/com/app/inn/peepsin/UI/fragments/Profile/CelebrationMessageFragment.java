package com.app.inn.peepsin.UI.fragments.Profile;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.CelebrationMessage;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.adapters.CelebrationMessageAdapter;
import com.app.inn.peepsin.UI.helpers.DividerItemRecyclerDecoration;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;

import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by dhruv on 29/9/17.
 */

public class CelebrationMessageFragment extends Fragment {
    private ProgressDialog progressDialog;
    private CelebrationMessageAdapter messageAdapter;
    private static Switcher profileSwitcherListener;

    @BindView(R.id.recycler_view_messages)
    RecyclerView recyclerView;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.empty_view)
    View emptyView;

    public static CelebrationMessageFragment newInstance(Switcher switcher) {
        CelebrationMessageFragment fragment = new CelebrationMessageFragment();
        profileSwitcherListener = switcher;
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = Utils.generateProgressDialog(getContext(), false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_message_list_layout, container, false);
        ButterKnife.bind(this, view);
        tvTitle.setText(getString(R.string.title_messages));

        messageAdapter = new CelebrationMessageAdapter(getContext(), new ArrayList<>(), true, profileSwitcherListener);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(messageAdapter);
        recyclerView.addItemDecoration(new DividerItemRecyclerDecoration(getActivity(), R.drawable.canvas_recycler_divider));
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadMessages();
    }

    private void loadMessages() {
        showProgress(true);
        ModelManager.modelManager().getCelebrationMessageList((Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<CelebrationMessage>> genricResponse) -> {
            CopyOnWriteArrayList<CelebrationMessage> events = genricResponse.getObject();
            if (events.size()!=0) {
                messageAdapter.addItems(events);
            }
            checkEmptyView();
            showProgress(false);
        }, (Constants.Status iStatus, String message) -> {
            Toaster.toast(message);
            checkEmptyView();
            showProgress(false);
        });
    }

    @OnClick(R.id.btn_back)
    void onBack() {
        getFragmentManager().popBackStack();
    }

    private void checkEmptyView(){
        if(messageAdapter.getItemCount()>0)
            emptyView.setVisibility(View.GONE);
        else
            emptyView.setVisibility(View.VISIBLE);
    }

    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) progressDialog.cancel();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        profileSwitcherListener = null;
    }
}
