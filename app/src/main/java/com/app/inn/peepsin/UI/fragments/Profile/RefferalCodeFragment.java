package com.app.inn.peepsin.UI.fragments.Profile;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Telephony;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.CurrentUser;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by root on 1/9/17.
 */

public class RefferalCodeFragment extends Fragment {


    private static Switcher profileSwitcherListener;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_userName)
    TextView tvUserName;
    private CurrentUser currentUser;
    @BindView(R.id.tvReffealCode)
    TextView tvReffealCode;
    private ProgressDialog progressDialog;

    public static Fragment newInstance(Switcher switcher) {
        profileSwitcherListener = switcher;
        return new RefferalCodeFragment();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        currentUser = ModelManager.modelManager().getCurrentUser();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_refercode, container, false);
        ButterKnife.bind(this, view);
        tvTitle.setText(getString(R.string.title_refer_and_earn));
        currentUser = ModelManager.modelManager().getCurrentUser();
        getUserData();
        setReffealCodeData();
        progressDialog = Utils.generateProgressDialog(getContext(), true);
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    @OnClick(R.id.btn_inviteRefer)
    public void onInviteRefer() {
        openBottomSheetMenu();
    }


    @OnClick(R.id.ib_back)
    public void onBack() {
        Utils.hideKeyboard(getContext());
        getFragmentManager().popBackStack();
    }


    private void setReffealCodeData() {
        if (currentUser.getReferralCode() != "")
            setReferalCode();
    }


    private void setReferalCode() {
        SpannableStringBuilder builder = new SpannableStringBuilder();
        SpannableString strReferCode = new SpannableString(getString(R.string.refer_and_earn_code));
        strReferCode.setSpan(new ForegroundColorSpan(Color.parseColor("#939393")), 0, strReferCode.length(), 0);
        builder.append(strReferCode);

        SpannableString strReferalCode = new SpannableString(currentUser.getReferralCode());
        strReferalCode
                .setSpan(new ForegroundColorSpan(Color.parseColor("#DC9369")), 0, strReferalCode.length(), 0);
        builder.append(strReferalCode);
        tvReffealCode.setText(builder, TextView.BufferType.SPANNABLE);
    }

    public void getInviteLink(int type) {
        showProgress(true);
        ModelManager.modelManager().getInviteLink((Constants.Status iStatus, GenricResponse<String> genericResponse) -> {
            showProgress(false);
            String inviteLink = genericResponse.getObject();
            if (type == 1)
                sendInviteLinkSMS(inviteLink);
            else if (type == 2)
                sendInviteLinkEmail(inviteLink);
            else
                shareIntent(inviteLink);
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Toaster.toast(message);
        });
    }


    private void getUserData() {

        tvUserName.setText(getString(R.string.refer_congrats) + currentUser.getFirstName() + " has Sigined up");

    }


    //Open shareIntent for Invite
    private void shareIntent(String link) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.refer_invite_message) + currentUser.getReferralCode() + "\n\n" + "while signing up or click on URL" + "\n" + link);
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }


    private void openBottomSheetMenu() {
        View view = getActivity().getLayoutInflater().inflate(R.layout.bottom_sheet_invite, null);

        final Dialog mBottomSheetDialog = new Dialog(getContext(), R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.getWindow().setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.canvas_dialog_background_inset));
        mBottomSheetDialog.show();

        view.findViewById(R.id.message_view).setOnClickListener(v -> getInviteLink(1));
        view.findViewById(R.id.gmail_view).setOnClickListener(v -> getInviteLink(2));
        view.findViewById(R.id.more_view).setOnClickListener(v -> getInviteLink(3));
    }


    void sendInviteLinkSMS(String link) {
        Intent sendIntent;
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            String defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(getActivity());

            sendIntent = new Intent(Intent.ACTION_SEND);
            sendIntent.setType("text/plain");
            sendIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.refer_invite_message) + currentUser.getReferralCode() + "\n\n" + "while signing up or click on URL" + "\n" + link);

            if (defaultSmsPackageName != null) {
                sendIntent.setPackage(defaultSmsPackageName);
                startActivity(sendIntent);
            } else {
                Toaster.toast("Device does not have SMS functionality");
            }
        } else {
            PackageManager pm = getContext().getPackageManager();
            if (pm.hasSystemFeature(PackageManager.FEATURE_TELEPHONY)) {
                sendIntent = new Intent(Intent.ACTION_VIEW);
                sendIntent.putExtra("sms_body", "Check out Peepsin app for your Android.Download it today from" + " " + link);
                sendIntent.setData(Uri.parse("smsto:" + Uri.encode("")));
                sendIntent.setType("vnd.android-dir/mms-sms");
                startActivity(sendIntent);
            } else {
                Toaster.toast("Device does not have SMS functionality");
            }
        }
    }

    void sendInviteLinkEmail(String link) {
        String to = "";
        String subject = "Invitation for PeepsIn";
        String message = getString(R.string.refer_invite_message) + currentUser.getReferralCode() + "\n\n" + "while signing up or click on URL" + "\n" + link;
        Intent email = new Intent(Intent.ACTION_SEND);
        email.putExtra(Intent.EXTRA_EMAIL, new String[]{to});
        email.putExtra(Intent.EXTRA_SUBJECT, subject);
        email.putExtra(Intent.EXTRA_TEXT, message);
        //need this to prompts email client only
        email.setType("message/rfc822");
        startActivity(Intent.createChooser(email, "Choose an Email client :"));
    }


    @Override
    public void onDetach() {
        super.onDetach();
        profileSwitcherListener = null;
    }

    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) progressDialog.cancel();
        }
    }


}
