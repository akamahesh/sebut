/*
 * Copyright (C) 2016 Jared Rummler <jared.rummler@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.app.inn.peepsin.UI.helpers;

import android.graphics.Color;

/**
 * Some SVGs to play with
 */
public enum SVG {

    PEEPSIN_LOGO(
            new String[]{
                    "M85.25,70.5L105,33.25h80.75c0,0 53,12 27.75,67.5c0,0 -17.25,37 -59,43.75H46.75l20.5,-38.5l103,0.25l25.25,-35.5L85.25,70.5z",
                    "M38,161l-20,37.75l52,0l22.5,-37.75z",
                    "M113,170l7,-9l34.17,0l-6.5,9l-10.83,0l-17.67,41.83l17.83,0l-5.17,9.17l-38.67,0l7.33,-9l9.33,-0.17l17.83,-42.17z"
            },
            new int[]{
                    Color.argb(255, 209,64,75),
                    Color.argb(255, 209,64,75),
                    Color.argb(255, 209,64,75)
            },
            250, 250
    );

    public final String[] glyphs;
    public final int[] colors;
    public final float width;
    public final float height;

    SVG(String[] glyphs, int[] colors, float width, float height) {
        this.glyphs = glyphs;
        this.colors = colors;
        this.width = width;
        this.height = height;
    }

}
