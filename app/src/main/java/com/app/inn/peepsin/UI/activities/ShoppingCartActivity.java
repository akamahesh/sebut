package com.app.inn.peepsin.UI.activities;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.Address;
import com.app.inn.peepsin.Models.CartShop;
import com.app.inn.peepsin.Models.ShoppingCart;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.dialogFragments.AddressDialogFragment;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.ADDRESS_RESULT;
import static com.app.inn.peepsin.Constants.Constants.KSCREENHEIGHT;
import static com.app.inn.peepsin.Constants.Constants.KSCREENWIDTH;
import static com.app.inn.peepsin.Constants.Constants.kData;

/**
 * Created by root on 5/7/17.
 */

public class ShoppingCartActivity extends AppCompatActivity {

    private String TAG = getClass().getName();
    @BindView(R.id.recycler_cart)
    RecyclerView recyclerCart;
    @BindView(R.id.empty_view)
    View emptyView;
    @BindView(R.id.bottom_layout)
    View paymentView;
    @BindView(R.id.tv_title)
    TextView tvTitle;
   /* @BindView(R.id.tv_cart_price)
    TextView tvCartPrice;*/
  /*  @BindView(R.id.tv_total_product)
    TextView tvTotalProduct;*/
    @BindView(R.id.btn_continue)
    Button btnContinue;
    private ShoppingCart cart;
    private String cartPrice = "0";
    private int totalCount = 0;

    private ProgressDialog progressDialog;
    private CartAdapter cartAdapter;

    public static Intent getIntent(Context context) {
        return new Intent(context, ShoppingCartActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = Utils.generateProgressDialog(ShoppingCartActivity.this, true);
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
        setContentView(R.layout.activity_shopping_cart);
        ButterKnife.bind(this);

       // Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        tvTitle.setText(getString(R.string.title_cart));

        cartAdapter = new CartAdapter(new ArrayList<>());
        recyclerCart.setHasFixedSize(true);
        recyclerCart.setLayoutManager(new LinearLayoutManager(this));
        recyclerCart.setAdapter(cartAdapter);
        checkEmptyScreen();
        loadShoppingCart();
    }


    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
    }

    @OnClick(R.id.ib_back)
    public void onBack() {
        onBackPressed();
        // Utils.isfeedsClicked=1;
    }

    @OnClick(R.id.btn_continue)
    public void btnClick() {
        //TODO here to add a dialog
        showAddressDialog("address");
    }

    private void setUpView() {
        String price = "Total "+getString(R.string.Rs)+" "+cartPrice+" ->";
        String count = getString(R.string.cart_total_product) + "\t" + totalCount;
       // tvCartPrice.setText(price);
        //tvTotalProduct.setText(count);
        btnContinue.setText(price);
    }

    public void loadShoppingCart() {
        showProgress(true);
        ModelManager.modelManager().getShoppingCart((Constants.Status iStatus, GenricResponse<ShoppingCart> genericResponse) -> {
            cart = genericResponse.getObject();
            cartAdapter.addItems(cart.getShopList());
            cartPrice = cart.getTotalCartPrice();
            totalCount = cart.getCartItemCount();
            setUpView();
            showProgress(false);
            checkEmptyScreen();
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Log.e(TAG, message);
        });
    }

    public void cartUpdate() {
        showProgress(true);
        ModelManager.modelManager().getShoppingCart((Constants.Status iStatus, GenricResponse<ShoppingCart> genericResponse) -> {
            cart = genericResponse.getObject();
            cartPrice = cart.getTotalCartPrice();
            totalCount = cart.getCartItemCount();
            setUpView();
            showProgress(false);
            checkEmptyScreen();
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Utils.showAlertDialog(ShoppingCartActivity.this, "Alert", message);
            Log.e(TAG, message);
        });
    }
    public void refreshNewData(GenricResponse<ShoppingCart> genericResponse) {
        showProgress(true);
            cart = genericResponse.getObject();
            cartAdapter.addItems(cart.getShopList());
            cartPrice = cart.getTotalCartPrice();
            totalCount = cart.getCartItemCount();
            setUpView();
            showProgress(false);
            checkEmptyScreen();

    }

    private void checkEmptyScreen() {
        if (cartAdapter.getItemCount() > 0) {
            emptyView.setVisibility(View.GONE);
            paymentView.setVisibility(View.VISIBLE);
        } else {
            emptyView.setVisibility(View.VISIBLE);
            paymentView.setVisibility(View.GONE);
        }
    }

    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) progressDialog.cancel();
        }
    }

    /*
    Shop Cart Adapter for shop details
     */
    public class CartAdapter extends RecyclerView.Adapter<CartAdapter.MyViewHolder> {

        private List<CartShop> shopList;

        private CartAdapter(List<CartShop> cartShop) {
            this.shopList = cartShop;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_shoping_cart_layout, parent, false);
            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, int position) {

            CartShop shop = shopList.get(position);
            holder.bindContent(shop);
            // Shop Details
            holder.tvShopName.setText(shop.getShopName());
            holder.tvOwnerName.setText(shop.getOwnerName());
            //holder.productRating.setRating(shop.getRating());
            String address = "No Address Available";
            if (shop.getShopAddress() != null) {
                address = shop.getShopAddress().getCity() + ", "
                        + shop.getShopAddress().getState() + ", "
                        + shop.getShopAddress().getCountry();
            }
            holder.tvShopAddress.setText(address);

            Picasso.with(ShoppingCartActivity.this)
                    .load(shop.getShopBannerImageUrl()).fit()
                    .placeholder(R.drawable.img_product_placeholder)
                    .into(holder.ivShopBanner);

            /*CartProductAdapter productAdapter = new CartProductAdapter(ShoppingCartActivity.this, shop.getProductList());
            holder.productRecycler.setLayoutManager(new LinearLayoutManager(ShoppingCartActivity.this));
            holder.productRecycler.setAdapter(productAdapter);*/
        }

        public void addItems(CopyOnWriteArrayList<CartShop> list) {
            shopList.clear();
            shopList.addAll(list);
            notifyDataSetChanged();
        }

        @Override
        public int getItemCount() {
            return shopList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            CartShop item;

            @BindView(R.id.product_recycler)
            RecyclerView productRecycler;
            @BindView(R.id.iv_shop_banner)
            ImageView ivShopBanner;
            @BindView(R.id.tv_shop_name)
            TextView tvShopName;
            @BindView(R.id.tv_owner_name)
            TextView tvOwnerName;
            @BindView(R.id.tv_shop_address)
            TextView tvShopAddress;
            @BindView(R.id.product_rating)
            RatingBar productRating;

            MyViewHolder(View view) {
                super(view);
                ButterKnife.bind(this, view);
            }

            public void bindContent(CartShop shop) {
                this.item = shop;
            }
        }

    }

    public void setBarlayotVisibility() {
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.activity_home, null);
        AppBarLayout appBarLayout = (AppBarLayout) view.findViewById(R.id.appBarLayout);
        // LinearLayout bar = (LinearLayout) ll_ActivityA.findViewById(R.id.actionbarhome);
        appBarLayout.setVisibility(View.GONE);

    }

    void showAddressDialog(String address) {
        Address primaryAddress = ModelManager.modelManager().getCurrentUser().getPrimaryAddress();
        String streetAddress = "primary address";
        if(primaryAddress!=null){
            streetAddress = primaryAddress.getStreetAddress()+", "+primaryAddress.getState();
        }
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_change_address_prompt);
        TextView tvAddress = ButterKnife.findById(dialog, R.id.tv_address);
        tvAddress.setText(streetAddress);
        Button btnChange = ButterKnife.findById(dialog, R.id.btn_change);
        btnChange.setOnClickListener(v -> {
            dialog.dismiss();
            getAddress();
        });
        Button btnContinue = ButterKnife.findById(dialog, R.id.btn_continue);
        btnContinue.setOnClickListener(v -> {
            dialog.dismiss();
            startActivity(TransactionDetailActivity.getIntent(this,primaryAddress));
            ShoppingCartActivity.this.finish();
        });
        dialog.show();

    }

    void getAddress() {
        DialogFragment fragment = AddressDialogFragment.newInstance(null);
        Bundle bdl = new Bundle();
        bdl.putInt(KSCREENWIDTH, 0);
        bdl.putInt(KSCREENHEIGHT, 0);
        fragment.setArguments(bdl);
        fragment.show(getSupportFragmentManager(), fragment.getClass().getSimpleName());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case ADDRESS_RESULT:
                if (resultCode == Activity.RESULT_OK) {
                    Address address = (Address) data.getSerializableExtra(kData);
                    startActivity(TransactionDetailActivity.getIntent(this,address));
                    ShoppingCartActivity.this.finish();
                }
                break;
        }
    }

}
