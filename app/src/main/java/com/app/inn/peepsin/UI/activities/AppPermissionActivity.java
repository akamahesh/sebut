package com.app.inn.peepsin.UI.activities;

import static com.app.inn.peepsin.Constants.Constants.kAuthToken;
import static com.app.inn.peepsin.Constants.Constants.kLatitude;
import static com.app.inn.peepsin.Constants.Constants.kLongitude;
import static com.app.inn.peepsin.Constants.Constants.kResults;
import static com.app.inn.peepsin.Constants.Constants.kType;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import butterknife.ButterKnife;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.APIManager.APIManager;
import com.app.inn.peepsin.Managers.APIManager.APIRequestHelper;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.CurrentUser;
import com.app.inn.peepsin.Models.GeoAddress;
import com.app.inn.peepsin.Models.ShopModel;
import com.app.inn.peepsin.Models.UserLocation;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.fragments.Permissions.LocationPermissionFragment;
import com.app.inn.peepsin.UI.helpers.FragmentUtil;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import com.google.gson.JsonObject;
import java.util.HashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by akaMahesh on 26/4/17.
 * copyright to : Mahesh Bhatt
 * contact : mckay1718@gmail.com
 */

public class AppPermissionActivity extends AppCompatActivity
    implements LocationPermissionFragment.OnFragmentInteractionListener {

  private static final String TAG = AppPermissionActivity.class.getSimpleName();
  private Type type;
  private ProgressDialog progressDialog;

  public enum Type {
    SIGNIN, SIGNUP
  }

  public static Intent getIntent(Context context, Type type) {
    Intent intent = new Intent(context, AppPermissionActivity.class);
    intent.putExtra(kType, type);
    return intent;
  }

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.fragment_root);
    overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
    progressDialog = Utils.generateProgressDialog(this, false);
    ButterKnife.bind(this);
    type = (Type) getIntent().getSerializableExtra(kType);
    switcher.switchFragment(LocationPermissionFragment.newInstance(switcher, type), false, true);
  }

  Switcher switcher = (fragment, s, a) -> {
    FragmentUtil.changeFragment(getSupportFragmentManager(), fragment, s, a);
  };

  @Override
  protected void onPause() {
    super.onPause();
    overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
  }

  @Override
  public void onSkip(double latitude, double longitude) {
    if (latitude == 0.0 || longitude == 0.0) {
      Toaster.kalaToast("Can't fetch your coordinates\n please try again!");
      return;
    }
    requestAddress(latitude, longitude);
  }

  public void requestAddress(double latitude, double longitude) {
    showProgress(true);
    String latlngText = latitude + "," + longitude;
    Retrofit retrofit = new Retrofit.Builder().baseUrl(APIManager.kGoogleMapsBaseURL)
        .addConverterFactory(GsonConverterFactory.create())
        .build();

    APIRequestHelper service = retrofit.create(APIRequestHelper.class);
    service.getLocation(latlngText, getString(R.string.google_api_key))
        .enqueue(new Callback<JsonObject>() {
          @Override
          public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
            try {
              JsonObject jsonObject = response.body();
              JSONObject jsonResponse = new JSONObject(jsonObject.toString());
              Log.i(TAG, jsonResponse.toString(4));
              JSONArray jsonResult = jsonResponse.getJSONArray(kResults);
              GeoAddress geoAddress = new GeoAddress(jsonResult.getJSONObject(0));
              HashMap<String, Object> addressMap = Utils.handlePlacesJSON(geoAddress);
              addressMap.put(kAuthToken,
                  ModelManager.modelManager().getCurrentUser().getAuthToken());
              updateAddress(addressMap);
            } catch (JSONException e) {
              e.printStackTrace();
              showProgress(false);
            }
          }

          @Override
          public void onFailure(Call<JsonObject> call, Throwable t) {
            Log.i(TAG, t.toString());
            showProgress(false);
          }
        });
  }

  private void updateAddress(HashMap<String, Object> addressMap) {
    ModelManager.modelManager().updateUserLocation(addressMap, (Constants.Status iStatus) -> {
      UserLocation userLocation = ModelManager.modelManager().getCurrentUser().getUserLocation();
      String lat = "28.5355";
      String lng = "77.3910";
      if(userLocation!=null){
        lat = userLocation.getLatitude();
        lng = userLocation.getLongitude();
      }
      getFeaturedShops(lat,lng);
    }, (Constants.Status iStatus, String message) -> {
      Utils.showAlertDialog(this, "Alert", message);
      showProgress(false);
    });
  }

  private void getFeaturedShops(String lat,String lng){
    ModelManager.modelManager().getFeatureShopList(1,  lat,lng ,
            (Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<ShopModel>> genericResponse) -> {
      getShopList(lat,lng);
    }, (Constants.Status iStatus, String message) -> {
      getShopList(lat,lng);
      Log.e(TAG, message);
    });
  }

  public void getShopList(String lat, String lng){
    HashMap<String ,Object> map = new HashMap<>();
    map.put(kLatitude,lat);
    map.put(kLongitude,lng);
    ModelManager.modelManager().getShopList(map,1,
            (Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<ShopModel>> genericResponse) -> {
      showProgress(false);
      startActivity(IntermediateActivity.getIntent(AppPermissionActivity.this));
    }, (Constants.Status iStatus, String message) -> {
      showProgress(false);
      startActivity(IntermediateActivity.getIntent(AppPermissionActivity.this));
      Log.e(TAG, message);
    });
  }

  private void showProgress(boolean b) {
    if (b) {
      progressDialog.show();
    } else {
      if (progressDialog != null) {
        progressDialog.dismiss();
      }
    }
  }

  @Override
  public void onBackPressed() {
    Toaster.kalaToast("Please use Navigation bar [skip]");
  }
}
