package com.app.inn.peepsin.UI.activities;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Libraries.Firebase.app.Config;
import com.app.inn.peepsin.Managers.APIManager.ReachabilityManager;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.activities.AppPermissionActivity.Type;
import com.app.inn.peepsin.UI.fragments.SignIn.ForgetPasswordFragment;
import com.app.inn.peepsin.UI.fragments.SignIn.ReferralFragment;
import com.app.inn.peepsin.UI.fragments.SignIn.SignInFragment;
import com.app.inn.peepsin.UI.fragments.SignIn.SignInResetPasswordFragment;
import com.app.inn.peepsin.UI.fragments.SignIn.SignUpFragment;
import com.app.inn.peepsin.UI.fragments.SignIn.SingInOTPFragment;
import com.app.inn.peepsin.UI.fragments.SignIn.VerifyOTPFragment;
import com.app.inn.peepsin.UI.helpers.FragmentUtil;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kAuthToken;
import static com.app.inn.peepsin.Constants.Constants.kEmail;
import static com.app.inn.peepsin.Constants.Constants.kIsReferralCodeValidated;
import static com.app.inn.peepsin.Constants.Constants.kOTP;
import static com.app.inn.peepsin.Constants.Constants.kReferralCode;
import static com.app.inn.peepsin.Constants.Constants.kType;

public class LogInActivity extends AppCompatActivity implements
        SignInFragment.OnSignInFragmentInteractionListener,
        SignUpFragment.OnSignUpFragmentInteractionListener,
        SignInResetPasswordFragment.OnResetPasswordFragmentInteractionListener,
        ForgetPasswordFragment.OnForgetPasswordFragmentInteractionListener,
        SingInOTPFragment.OnOTPFragmentInteractionListener,
        VerifyOTPFragment.OnOTPFragmentInteractionListener,
        ReferralFragment.OnReferralFragmentInteractionListener {

    private static String TAG = LogInActivity.class.getSimpleName();
    private static int TYPE = 0;
    private static String EMAIL = "";
    private static final int SIGN_IN = 1;
    private static final int SIGN_UP = 2;
    private static final int FORGET_PASS = 3;
    private ProgressDialog progressDialog;
    String firebaseToken = "";
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    /**
     * @param email = email retrive from social ids
     * @param type  = 1 for signIn screen, 2 for signUp screen, 3 for forget_pass screen
     */
    public static Intent getIntent(Context context, String email, int type) {
        Intent intent = new Intent(context, LogInActivity.class);
        intent.putExtra(kType, type);
        intent.putExtra(kEmail, email);
        return intent;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
        setContentView(R.layout.fragment_login_root);
        ButterKnife.bind(this);
        tvTitle.setText(getString(R.string.title_login));
        progressDialog = Utils.generateProgressDialog(this, false);
        Intent intent = getIntent();
        if (intent != null) {
            TYPE = intent.getIntExtra(kType, 0);
            EMAIL = intent.getStringExtra(kEmail);
        }
        if (TYPE == SIGN_IN) {
            FragmentUtil
                    .replaceFragment(getSupportFragmentManager(), SignInFragment.newInstance(EMAIL), false,
                            false);
            setFireBaseToken();
        } else if (TYPE == SIGN_UP) {
            FragmentUtil
                    .replaceFragment(getSupportFragmentManager(), SignUpFragment.newInstance(EMAIL), false,
                            false);
            tvTitle.setText(getString(R.string.title_sign_up));
            setFireBaseToken();
        } else if (TYPE == FORGET_PASS) {
            getOtpResetPassword(EMAIL);
        }
    }


    public void setTitle(String title) {
        tvTitle.setText(title);
    }

    public void hideToolbar() {
        toolbar.setVisibility(View.GONE);
    }

    private void setFireBaseToken() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        firebaseToken = pref.getString("regId", null);
    }


    @OnClick(R.id.ib_back)
    void onCustomBack() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
        showProgress(false);
    }

    private void getOtpResetPassword(String email) {
        showProgress(true);
        ModelManager.modelManager().getForgotPasswordOTP(email,
                (Constants.Status iStatus, GenricResponse<HashMap<String, Object>> genericResponse) -> {
                    HashMap<String, Object> map = genericResponse.getObject();
                    String OTP = (String) map.get(kOTP);
                    FragmentUtil.replaceFragment(getSupportFragmentManager(),
                            SignInResetPasswordFragment.newInstance(email, OTP), false, true);
                    tvTitle.setText(getString(R.string.title_reset_password));
                    showProgress(false);
                }, (Constants.Status iStatus, String message) -> {
                    Utils.showAlertDialog(this, "Alert", message);
                    showProgress(false);
                });
    }


    /***Sign In Fragment callbacks**/
    @Override
    public void onSignIn(HashMap<String, Object> loginMap) {
        if (!ReachabilityManager.getNetworkStatus()) {
            Utils.showAlertDialog(this, "Login Failed!",
                    "Sorry, login Failed to reach PeepsIn servers. Please check your network or try again later.");
            return;
        }
        showProgress(true);
        Utils.hideKeyboard(LogInActivity.this);
        ModelManager.modelManager().userSystemLogin(loginMap,
                (Constants.Status iStatus, GenricResponse<Map<String, Object>> genricResponse) -> {
                    Map<String, Object> map = genricResponse.getObject();
                    String authToken = (String) map.get(kAuthToken);
                    startActivity(UpdationActivity.getIntent(this, authToken, Type.SIGNIN));
                }, (Constants.Status iStatus, String message) -> {
                    showProgress(false);
                    Utils.showAlertDialog(LogInActivity.this, "Invalid Login Details!", message);
                });
    }

    @Override
    public void onSignUp(HashMap<String, Object> userMap) {
        if (!ReachabilityManager.getNetworkStatus()) {
            Utils.showAlertDialog(this, "Signup Failed!",
                    "Sorry, Signup Failed to reach PeepsIn servers. Please check your network or try again later.");
            return;
        }
        showProgress(true);
        ModelManager.modelManager().userSystemRegisteration(userMap, (Constants.Status iStatus, GenricResponse<String> genricResponse) -> {
            String authToken = genricResponse.getObject();
            String referralCode = (String) userMap.get(kReferralCode);
            if(referralCode.isEmpty())
                onReferralDone(authToken);
            else
                checkReferralCode(authToken, referralCode);
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Utils.showAlertDialog(LogInActivity.this, "Signup Failed!", message);
        });
    }

    private void checkReferralCode(String authToken, String referralCode) {
        ModelManager.modelManager().validateReferralCode(referralCode, authToken, (Constants.Status iStatus, GenricResponse<HashMap<String, Object>> genericResponse) -> {
            Log.d(TAG, "validateReferralCode : success");
            showProgress(false);
            HashMap<String, Object> hashMap = genericResponse.getObject();
            int isValidate = (int) hashMap.get(kIsReferralCodeValidated);
            if (isValidate == 0) {//not validated
                FragmentUtil.replaceFragment(getSupportFragmentManager(), ReferralFragment.newInstance(authToken), true, true);
            } else {
                onReferralDone(authToken);
            }
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Toaster.toast(message);
            Log.e(TAG, "validateReferralCode : failure ");
        });
    }


    @Override
    public void moveToForgetPassword() {
        FragmentUtil.replaceFragment(getSupportFragmentManager(), ForgetPasswordFragment.newInstance(), true, true);
        tvTitle.setText(getString(R.string.title_forget_password));
    }

    @Override
    public void moveToOTPScreen(int type, String otp, String contactID, String authtoken) {
        FragmentUtil.replaceFragment(getSupportFragmentManager(), SingInOTPFragment.newInstance(type, otp, contactID, authtoken), true, true);
    }

    @Override
    public void onPasswordChanged() {
        moveToSignIn("");
    }

    @Override
    public void onBack() {
        super.onBackPressed();
    }

    @Override
    public void verifyUser(String otp, String contactID, HashMap<String, Object> userMap) {
        FragmentUtil.replaceFragment(getSupportFragmentManager(), VerifyOTPFragment.newInstance(otp, contactID, userMap), true, true);
    }


    /**
     * ForgetPassword Fragment callback
     *
     * @param otp 4 digit otp
     */
    @Override
    public void onForgetPassword(String otp, String email) {
        FragmentUtil.replaceFragment(getSupportFragmentManager(), SingInOTPFragment.newInstance(3, otp, email, ""),
                        false, true);
        tvTitle.setText(getString(R.string.title_forget_password));
    }


    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        }
    }


    @Override
    public void moveToSignIn(String contactID) {
        FragmentUtil.replaceFragment(getSupportFragmentManager(), SignInFragment.newInstance(contactID), false, true);
        tvTitle.setText(getString(R.string.title_sign_in));
    }

    @Override
    public void onSignInVerified(String authToken, String otp) {
        startActivity(UpdationActivity.getIntent(this, authToken, Type.SIGNIN));
    }


    @Override
    public void onSignUpVerified(HashMap<String, Object> userMap) {
        onSignUp(userMap);
    }

    @Override
    public void onReferralDone(String mAuthToken) {
        startActivity(UpdationActivity.getIntent(this, mAuthToken, Type.SIGNUP));
    }


}
