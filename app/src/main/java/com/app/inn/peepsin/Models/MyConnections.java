package com.app.inn.peepsin.Models;

import static com.app.inn.peepsin.Constants.Constants.kEmptyString;

/**
 * Created by mahesh on 30/5/17.
 */


public class MyConnections {
    private String firstName;
    private String lastName;
    private String imageURL;
    private String contactId;
    private boolean isSelected;
    private Integer sellingProductCount;
    private Integer friendsCount;
    private Address primaryAddress;
    private Integer connectionLevel;
    private Integer rating;
    private Integer friendsCountSecondLevel;
    private Integer friendsCountThirdLevel;
    private String isBlocked;
    private Boolean profileVisibility;
    private Integer userConnectionStatus;
    private String shopname;
    private String address;
    private Integer userId;



    public MyConnections(Integer userid,String fname, String lastname,String imageURL,String isBlocked,Integer rating,Integer connectionLevel,String shopname,String address) {
        this.firstName = fname;
        this.lastName = lastname;
        this.imageURL = imageURL;
        this.rating=rating;
        this.isBlocked=isBlocked;
        this.connectionLevel= connectionLevel;
        this.shopname=shopname;
        this.address=address;
        this.userId=userid;
    }


    public String getFName() {
        return firstName;
    }

    public void setFName(String name) {
        this.firstName = name;
    }

    public String getLName() {
        return lastName;
    }

    public void setLName(String lName) {
        this.lastName = lName;
    }
    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public String getImageURL() {
        return (imageURL==null)?kEmptyString:imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getIsBlocked() {
        return isBlocked;
    }

    public void setIsBlocked(String isBlocked) {
        this.isBlocked = isBlocked;
    }

    public Integer getConnectionLevel() {
        return connectionLevel;
    }

    public void setConnectionLevel(Integer connectionLevel) {
        this.connectionLevel = connectionLevel;
    }

    public String getAddress() {
        return address;
    }

    public void setaddress(String adress) {
        this.address = address;
    }
    public String getShopName() {
        return shopname;
    }

    public void setShopname(String shopname) {
        this.shopname = shopname;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
