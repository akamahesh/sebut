package com.app.inn.peepsin.Models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.Serializable;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by akaMahesh on 17/7/17.
 * copyright to : Innverse Technologies
 */

public class MyShop extends BaseModel implements Serializable {
    private Integer id;
    private String name;
    private String bannerUrl;
    private String logoUrl;
    private String profilePicURL;
    private Integer rating;
    private Address address;
    private Integer favoriteCount;
    private Integer productCount;
    private Integer reviewCount;
    private Integer receivedOrderCount;
    private String ownerName;
    private String ownerFirstName;
    private String ownerLastName;
    private String email;
    private String phone;
    private String joinedDate;
    private Integer connectionLevel;
    private Integer shopStatus;
    private Integer shopType;
    private Integer visibilityRange;
    private Integer visibilityLevel;
    private Integer businessType;
    private KYCInfo kycInfo;
    private CompanyDetails companyDetails;
    private BankDetails bankDetails;
    private DeliveryType deliveryType;
    private CopyOnWriteArrayList<ProductType> productTypeStore;

    public MyShop(JSONObject jsonResponse){
        this.id                 = getValue(jsonResponse, kShopId,            Integer.class);
        this.name               = getValue(jsonResponse, kShopName,          String.class);
        this.bannerUrl          = getValue(jsonResponse, kShopBannerImageURl,     String.class);
        this.logoUrl            = getValue(jsonResponse, kShopLogoImageURL,  String .class);
        this.profilePicURL      = getValue(jsonResponse, kProfilePicUrl,  String .class);

        this.rating             = getValue(jsonResponse, kRating,            Integer.class);
        this.favoriteCount      = getValue(jsonResponse, kFavoriteCount,     Integer.class);
        this.productCount       = getValue(jsonResponse, kProductCount,      Integer.class);
        this.reviewCount        = getValue(jsonResponse, kReviewCount,       Integer.class);
        this.receivedOrderCount = getValue(jsonResponse, kReceivedOrderCount,Integer.class);
        this.ownerName          = getValue(jsonResponse, kOwnerName,         String.class);
        this.ownerFirstName     = getValue(jsonResponse, kOwnerFirstName,    String.class);
        this.ownerLastName      = getValue(jsonResponse, kOwnerLastName,     String.class);
        this.email              = getValue(jsonResponse, kEmail,             String.class);
        this.phone              = getValue(jsonResponse, kPhone,             String.class);
        this.joinedDate         = getValue(jsonResponse, kJoinedDate,        String.class);
        this.connectionLevel    = getValue(jsonResponse, kConnectionLevel,   Integer.class);
        try {
            this.address        = new Address(getValue(jsonResponse,kShopAddress,JSONObject.class));
        }catch (Exception e){
            e.printStackTrace();
        }
        this.shopStatus         = getValue(jsonResponse, kShopStatus,        Integer.class);
        this.businessType       = getValue(jsonResponse, kBusinessType,      Integer.class);
        this.shopType           = getValue(jsonResponse, kShopType,          Integer.class);
        this.visibilityRange    = getValue(jsonResponse, kVisibilityRange,   Integer.class);
        this.visibilityLevel    = getValue(jsonResponse, kVisibilityLevel,   Integer.class);
        try{
            this.kycInfo        = new KYCInfo(getValue(jsonResponse,kKycInfo,JSONObject.class));
            this.companyDetails = new CompanyDetails(getValue(jsonResponse,kCompanyRegistrationDetail,JSONObject.class));
            this.bankDetails    = new BankDetails(getValue(jsonResponse,kBankDetails,JSONObject.class));
        }catch (Exception e){
            e.printStackTrace();
        }
        try {
            this.productTypeStore    = handleProductTypes(getValue(jsonResponse, kProductTypeStore, JSONArray.class));
        }catch (Exception e){
            e.printStackTrace();
        }

        try {
            this.deliveryType   = new DeliveryType(getValue(jsonResponse,kDeliveryTime,JSONObject.class));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public Integer getId() {
        return id;
    }

    public Integer getShopStatus() {
        return shopStatus;
    }

    public void setShopStatus(Integer shopStatus) {
        this.shopStatus = shopStatus;
    }

    public Integer getShopType() {
        return shopType;
    }

    public Integer getVisibilityRange() {
        return visibilityRange;
    }

    public Integer getVisibilityLevel() {
        return visibilityLevel;
    }

    public Integer getBusinessType() {
        return businessType;
    }

    public String getName() {
        return name;
    }

    public String getBannerUrl() {
        return bannerUrl;
    }

    public Integer getRating() {
        return rating;
    }

    public Address getAddress() {
        return address;
    }

    public Integer getFavoriteCount() {
        return favoriteCount;
    }

    public Integer getReviewCount() {
        return reviewCount;
    }

    public Integer getReceivedOrderCount() {
        return receivedOrderCount;
    }

    public Integer getProductCount() {
        return productCount;
    }

    public String getOwnerFirstName() {
        return ownerFirstName;
    }

    public String getOwnerLastName() {
        return ownerLastName;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getJoinedDate() {
        return joinedDate;
    }

    public Integer getConnectionLevel() {
        return connectionLevel;
    }

    public KYCInfo getKycInfo() {
        return kycInfo;
    }

    public BankDetails getBankDetails() {
        return bankDetails;
    }

    public CompanyDetails getCompanyDetails() {
        return companyDetails;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public DeliveryType getDeliveryType() {
        return deliveryType;
    }

    private CopyOnWriteArrayList<ProductType> handleProductTypes(JSONArray jsonArray) throws JSONException {
        CopyOnWriteArrayList<ProductType> productTypes = new CopyOnWriteArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            productTypes.add(new ProductType(jsonArray.getJSONObject(i)));
        }
        return productTypes;
    }

    public CopyOnWriteArrayList<ProductType> getProductTypeStore() {
        return productTypeStore;
    }


    public String getProfilePicURL() {
        return profilePicURL;
    }
}
