package com.app.inn.peepsin.UI.helpers;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.app.inn.peepsin.Models.Search;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SharedPreference {

    // Avoid magic numbers.
    private static final int MAX_SIZE = 3;


    public SharedPreference() {
        super();
    }

    public static ArrayList<String> loadList(Context context, String pref_name, String key) {

        SharedPreferences settings;
        List<String> favorites;
        settings = context.getSharedPreferences(pref_name, Context.MODE_PRIVATE);
        if (settings.contains(key)) {
            String jsonFavorites = settings.getString(key, null);
            Gson gson = new Gson();
            String[] favoriteItems = gson.fromJson(jsonFavorites, String[].class);
            favorites = Arrays.asList(favoriteItems);
            favorites = new ArrayList<>(favorites);
        } else
            return null;
        return (ArrayList<String>) favorites;
    }

    public static List<Search> getSearchList(Context context,String pref_name, String key) {

        SharedPreferences settings;
        List<Search> searchList;
        settings = context.getSharedPreferences(pref_name, Context.MODE_PRIVATE);

        if (settings.contains(key)) {
            String jsonFavorites = settings.getString(key, null);
            Gson gson = new Gson();
            Search[] favoriteItems = gson.fromJson(jsonFavorites, Search[].class);
            searchList = Arrays.asList(favoriteItems);
            searchList = new ArrayList<>(searchList);
        } else
            return null;

        return searchList;
    }

    public static void addSearch(Context context, Search product,String pref_name, String key) {
        List<Search> searches = getSearchList(context,pref_name,key);
        if (searches == null)
            searches = new ArrayList<>();

        if(searches.size() > MAX_SIZE) {
            searches.clear();
        }

        searches.add(product);
        saveSearch(context, searches,pref_name,key);
    }

    public static void removeSearch(Context context,Search product,String pref_name, String key) {
        List<Search> searches = getSearchList(context, pref_name,key);
        if (searches != null) {
            searches.remove(product);
            saveSearch(context, searches,pref_name, key);
        }
    }

    private static void saveSearch(Context context, List<Search> search,String pref_name,String key) {

        SharedPreferences settings;
        SharedPreferences.Editor editor;
        settings = context.getSharedPreferences(pref_name, Context.MODE_PRIVATE);
        editor = settings.edit();

        Gson gson = new Gson();
        String jsonSearch = gson.toJson(search);

        editor.putString(key, jsonSearch);
        editor.apply();
    }


    public static void addList(Context context, String pref_name, String key, String country) {
        List<String> favorites = loadList(context, pref_name, key);
        if (favorites == null)
            favorites = new ArrayList<>();

        if(favorites.size() > MAX_SIZE) {
            favorites.clear();
            deleteList(context, pref_name);
        }

        if(favorites.contains(country)){
            favorites.remove(country);
        }

        favorites.add(country);
        storeList(context, pref_name, key, favorites);
    }

    private static void storeList(Context context, String pref_name, String key, List countries) {

        SharedPreferences settings;
        SharedPreferences.Editor editor;
        settings = context.getSharedPreferences(pref_name, Context.MODE_PRIVATE);
        editor = settings.edit();
        Gson gson = new Gson();
        String jsonFavorites = gson.toJson(countries);
        editor.putString(key, jsonFavorites);
        editor.apply();
    }

//    public static void removeList(Context context,String pref_name, String key, String country) {
//        ArrayList favorites = loadList(context, pref_name,key);
//        if (favorites != null) {
//            favorites.remove(country);
//            storeList(context, pref_name, key, favorites);
//        }
//    }


    private static void deleteList(Context context, String pref_name){

        SharedPreferences myPrefs = context.getSharedPreferences(pref_name, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = myPrefs.edit();
        editor.clear();
        editor.apply();
    }
}