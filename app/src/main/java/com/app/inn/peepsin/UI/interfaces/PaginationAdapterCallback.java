package com.app.inn.peepsin.UI.interfaces;

/**
 * Created by akaMahesh on 4/7/17.
 * copyright to : Innverse Technologies
 */

public interface PaginationAdapterCallback {
    void retryPageLoad();
}
