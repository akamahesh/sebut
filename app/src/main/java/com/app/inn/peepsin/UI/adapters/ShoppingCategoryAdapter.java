package com.app.inn.peepsin.UI.adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.inn.peepsin.Models.ProductType;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.fragments.Shop.CategoryShopFragment;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by dhruv on 14/9/17.
 */

public class ShoppingCategoryAdapter extends RecyclerView.Adapter<ShoppingCategoryAdapter.ViewHolder> {

    private Switcher switcherListener;
    private Context context;
    private List<ProductType> categoryList;


    public ShoppingCategoryAdapter(Context context, List<ProductType> inventoryList, Switcher switcher) {
        this.context = context;
        this.categoryList = inventoryList;
        this.switcherListener = switcher;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_category_grid_base, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ShoppingCategoryAdapter.ViewHolder holder, int position) {
        ProductType shop = categoryList.get(position);
        holder.bindContent(shop);
        String name = shop.getName();
        holder.tvItemName.setText(name);

        String imageURL = shop.getUrl();
        if (!imageURL.isEmpty()) {
            Picasso.with(context)
                    .load(imageURL)
                    .placeholder(R.drawable.img_product_placeholder)
                    .into(holder.ivItemImage);
        } else {
            holder.ivItemImage.setImageDrawable(
                    context.getResources().getDrawable(R.drawable.img_product_placeholder));
        }
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public void addItems(List<ProductType> inventoryList) {
        this.categoryList.clear();
        this.categoryList.addAll(inventoryList);
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ProductType shopCategory;
        @BindView(R.id.iv_item_image)
        ImageView ivItemImage;
        @BindView(R.id.tv_item_name)
        TextView tvItemName;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindContent(ProductType shop) {
            this.shopCategory = shop;
        }

        @OnClick(R.id.item_view)
        void onInventory() {
            if (switcherListener != null) {
                switcherListener.switchFragment(CategoryShopFragment
                                .newInstance(switcherListener, shopCategory.getId(),shopCategory.getName(),shopCategory.getUrl(), categoryList), true, true);
            }
        }
    }
}
