package com.app.inn.peepsin.UI.fragments.Profile;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.DataBase.DataBaseHandler;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.CelebrationEvent;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.Services.EventNotificationService;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kEventMap;

/**
 * Created by dhruv on 28/9/17.
 */

public class CelebrationEventDetailsFragment extends Fragment{

    private final String TAG = getClass().getSimpleName() + ">>>";
    private static Switcher switcherListener;

    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.ivEventImage)
    ImageView ivEventImage;
    @BindView(R.id.tv_event)
    TextView tvName;
    @BindView(R.id.tv_type)
    TextView tvType;
    @BindView(R.id.tv_date)
    TextView tvDate;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.tv_timezone)
    TextView tvTimeZone;
    @BindView(R.id.tv_message)
    TextView tvMessage;
    @BindView(R.id.tv_notify_before)
    TextView tvNotifyBefore;
    @BindView(R.id.tv_repetition)
    TextView tvRepetition;
    @BindView(R.id.tv_celebrate_via)
    TextView tvCelebrate;

    private ProgressDialog progressDialog;
    private CelebrationEvent event;
    DataBaseHandler dbHandler;

    public static Fragment newInstance(CelebrationEvent event, Switcher switcher) {
        switcherListener = switcher;
        CelebrationEventDetailsFragment fragment = new CelebrationEventDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(kEventMap,event);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = Utils.generateProgressDialog(getContext(), false);
        dbHandler = new DataBaseHandler(getContext());
        Bundle bdl = getArguments();
        if(bdl!=null)
            event = (CelebrationEvent) bdl.getSerializable(kEventMap);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_event_details_layout, container, false);
        ButterKnife.bind(this, view);
        tvTitle.setText(getString(R.string.event_details));
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadEventDetails();
    }

    private void setEventInfo(CelebrationEvent event){
        tvName.setText(event.getEventTitle());
        tvType.setText(event.getEventTypeName());
        tvDate.setText(Utils.getTimeStampDate(event.getSchedulingTimeStamp()));
        tvTime.setText(Utils.getTimeStampTime(event.getSchedulingTimeStamp()));
        tvTimeZone.setText(event.getTimeZone());
        tvMessage.setText(event.getMessage());
        JSONArray celebrationMode = event.getCelebrationMode();

        String imageUrl = event.getImageUrl();
        if(!imageUrl.isEmpty())
            Picasso.with(getContext()).load(imageUrl).fit()
                    .placeholder(R.drawable.img_event_banner).into(ivEventImage);

        StringBuilder notifyBefore = new StringBuilder();
        notifyBefore.append(getString(R.string.event_notify_before)).append(" ");
        switch (event.getNotifyMeDuration()){
            case 1: notifyBefore.append(getString(R.string.event_15min));
                break;
            case 2: notifyBefore.append(getString(R.string.event_1hr));
                break;
            case 3: notifyBefore.append(getString(R.string.event_1day));
                break;
        }
        tvNotifyBefore.setText(notifyBefore);

        StringBuilder repetition = new StringBuilder();
        repetition.append(getString(R.string.event_repetition)).append(" ");
        switch (event.getNotifyMeDuration()){
            case 1: repetition.append(getString(R.string.event_oneTime));
                break;
            case 2: repetition.append(getString(R.string.event_everyDay));
                break;
            case 3: repetition.append(getString(R.string.event_everyMonth));
                break;
            case 4: repetition.append(getString(R.string.event_everyYear));
                break;
        }
        tvRepetition.setText(repetition);

        StringBuilder celebrationVia = new StringBuilder();
        celebrationVia.append(getString(R.string.event_celebrate_via)).append(" ");
        for(int i=0;i<celebrationMode.length();i++){
            try {
                switch (celebrationMode.getString(i)){
                    case "1": celebrationVia.append(getString(R.string.event_text));
                        break;
                    case "2": celebrationVia.append(getString(R.string.event_email));
                        break;
                    case "3": celebrationVia.append(getString(R.string.event_notify));
                        break;
                    case "4": celebrationVia.append(getString(R.string.event_chat));
                        break;
                }
                if(i!=celebrationMode.length()-1)
                    celebrationVia.append(" , ");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        tvCelebrate.setText(celebrationVia);

    }

    @OnClick(R.id.btn_edit)
    void onEdit() {
        if(event!=null)
            openBottomSheetMenu();
    }

    @OnClick(R.id.btn_back)
    public void onBackPress() {
        getFragmentManager().popBackStack();
    }

    private void openBottomSheetMenu() {
        LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.bottom_sheet_shop_detais, null);
        final Dialog mBottomSheetDialog = new Dialog(getContext(), R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.canvas_dialog_background_inset));
        mBottomSheetDialog.show();
        view.findViewById(R.id.view_shop_status).setVisibility(View.GONE);

        TextView tvEdit = (TextView)view.findViewById(R.id.tv_edit_shop);
        tvEdit.setText(getString(R.string.event_edit));
        tvEdit.setOnClickListener(v -> {
            mBottomSheetDialog.dismiss();
            if (switcherListener != null)
                switcherListener.switchFragment(AddEventFragment.newInstance(event.getUserId(),event,2,switcherListener), true, true);
        });

        TextView tvDelete = (TextView)view.findViewById(R.id.tv_delete_shop);
        tvDelete.setText(getString(R.string.event_delete));
        tvDelete.setOnClickListener(v -> {
            mBottomSheetDialog.dismiss();
            Utils.showAlertDialog(getContext(), "Alert Message", "Do you really want to delete event?", (dialog, which) -> deleteEvent());
        });

        view.findViewById(R.id.tv_cancel).setOnClickListener(v -> mBottomSheetDialog.dismiss());
    }

    void deleteEvent(){
        showProgress(true);
        ModelManager.modelManager().deleteCelebrationEvent(dbHandler,event.getUserId(),event.getEventId(), (Constants.Status iStatus) -> {
            showProgress(false);
            if(dbHandler.getEventsFromDatabase().size()>0){
                Intent intent = new Intent(getContext(), EventNotificationService.class);
                intent.putExtra("RQS", "active");
                getContext().startService(intent);
            }
            onBackPress();
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Log.e(TAG,message);
            Toaster.toast(message);
        });
    }

    void loadEventDetails(){
        showProgress(true);
        ModelManager.modelManager().getCelebrationEventDetails(event.getEventId(), (Constants.Status iStatus, GenricResponse<CelebrationEvent> genricResponse) -> {
            showProgress(false);
            event = genricResponse.getObject();
            setEventInfo(event);
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Log.e(TAG,message);
            Toaster.toast(message);
        });
    }

    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) progressDialog.dismiss();
        }
    }
}
