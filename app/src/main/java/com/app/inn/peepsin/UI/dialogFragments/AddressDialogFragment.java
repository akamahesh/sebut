package com.app.inn.peepsin.UI.dialogFragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.inn.peepsin.Models.Address;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.activities.ShoppingCartActivity;
import com.app.inn.peepsin.UI.fragments.AddressFragment;
import com.app.inn.peepsin.UI.helpers.FragmentUtil;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.AddressListener;
import com.app.inn.peepsin.UI.interfaces.Switcher;

import butterknife.ButterKnife;

import static com.app.inn.peepsin.Constants.Constants.ADDRESS_RESULT;
import static com.app.inn.peepsin.Constants.Constants.KSCREENHEIGHT;
import static com.app.inn.peepsin.Constants.Constants.KSCREENWIDTH;
import static com.app.inn.peepsin.Constants.Constants.kData;

/**
 * Created by root on 10/7/17.
 */

public class AddressDialogFragment extends DialogFragment {

    private static Switcher profileSwitcherListener;

    public static AddressDialogFragment newInstance(Switcher switcher) {
        profileSwitcherListener = switcher;
        return new AddressDialogFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.MY_DIALOG);
        Bundle mArgs = getArguments();
        int pHeight = mArgs.getInt(KSCREENHEIGHT);
        int pWidth = mArgs.getInt(KSCREENWIDTH);
        Dialog d = getDialog();
        if (d != null) {
            d.getWindow().setLayout(pWidth - 100, pHeight - 100);
            d.getWindow().getAttributes().windowAnimations = R.style.MaterialDialogSheetAnimation;
            Drawable drawable = getResources().getDrawable(R.drawable.canvas_dialog_bg);
            d.getWindow().setBackgroundDrawable(drawable);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_root, container, false);
        ButterKnife.bind(this, view);
        FragmentUtil.changeFragment(getChildFragmentManager(), AddressFragment.newInstance(listener), false, true);
        return view;
    }


    AddressListener listener = new AddressListener() {
        @Override
        public void switchFragment(Fragment fragment, boolean saveInBackstack, boolean animate) {
            Utils.hideKeyboard(getContext());
            FragmentUtil.changeFragment(getChildFragmentManager(), fragment, saveInBackstack, animate);
        }

        @Override
        public void address(Address address) {
            Intent in=null;
            if(address!=null)
                 in = getActivity().getIntent().putExtra(kData, address);
            try {
                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, in);
            } catch (NullPointerException e) {
                ((ShoppingCartActivity) getActivity()).onActivityResult(ADDRESS_RESULT, Activity.RESULT_OK, in);
            }
            onBack();
        }

        @Override
        public void onBack() {
            dismiss();
        }
    };


    @Override
    public void onDetach() {
        super.onDetach();
        Log.v("Lifecycle :  ", "onDetach" + getClass().getName());
        listener = null;
    }

}
