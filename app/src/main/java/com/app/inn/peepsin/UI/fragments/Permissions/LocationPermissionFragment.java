package com.app.inn.peepsin.UI.fragments.Permissions;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.view.View.VISIBLE;
import static com.app.inn.peepsin.Constants.Constants.kAuthToken;
import static com.app.inn.peepsin.Constants.Constants.kCity;
import static com.app.inn.peepsin.Constants.Constants.kCountry;
import static com.app.inn.peepsin.Constants.Constants.kLatitude;
import static com.app.inn.peepsin.Constants.Constants.kLongitude;
import static com.app.inn.peepsin.Constants.Constants.kState;
import static com.app.inn.peepsin.Constants.Constants.kStreetAddress;
import static com.app.inn.peepsin.Constants.Constants.kType;
import static com.app.inn.peepsin.Constants.Constants.kZipcode;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.app.inn.peepsin.Constants.Blocks.Block;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Libraries.DispatchQueue.DispatchQueue;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.Services.GPSTracker;
import com.app.inn.peepsin.UI.activities.AppPermissionActivity;
import com.app.inn.peepsin.UI.activities.LocationChangeActivity;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import com.transitionseverywhere.Slide;
import com.transitionseverywhere.Transition;
import com.transitionseverywhere.TransitionManager;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;


public class LocationPermissionFragment extends Fragment {

  private OnFragmentInteractionListener mListener;
  private final int REQUEST_PERMISSION_LOCATION = 102;
  private static Switcher switcherListener;
  private AppPermissionActivity.Type type;

  @BindView(R.id.iv_location_pink500)
  ImageView ivLocation1;
  @BindView(R.id.iv_location_blue700)
  ImageView ivLocation2;
  @BindView(R.id.iv_location_blue500)
  ImageView ivLocation3;
  @BindView(R.id.iv_location_indigo_a400)
  ImageView ivLocation4;
  @BindView(R.id.view_group_transition)
  ViewGroup vgLocation;


  public LocationPermissionFragment() {
    // Required empty public constructor
  }

  public static Fragment newInstance(Switcher switcher, AppPermissionActivity.Type type) {
    switcherListener = switcher;
    Fragment fragment = new LocationPermissionFragment();
    Bundle bundle = new Bundle();
    bundle.putSerializable(kType, type);
    fragment.setArguments(bundle);
    return fragment;
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    if (context instanceof OnFragmentInteractionListener) {
      mListener = (OnFragmentInteractionListener) context;
    } else {
      throw new RuntimeException(context.toString()
          + " must implement OnFragmentInteractionListener");
    }
  }


  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    Bundle bundle = getArguments();
    if (bundle != null) {
      type = (AppPermissionActivity.Type) bundle.getSerializable(kType);
    }
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_location_permission, container, false);
    ButterKnife.bind(this, view);
    return view;
  }


  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    animateLocation();
  }

  private void animateLocation() {
    final Handler handler = new Handler();
    handler.postDelayed(() -> {
      TransitionManager.beginDelayedTransition(vgLocation, new Slide(Gravity.TOP));
      ivLocation1.setVisibility(VISIBLE);
      TransitionManager.beginDelayedTransition(vgLocation, new Slide(Gravity.TOP));
      ivLocation2.setVisibility(VISIBLE);
      TransitionManager.beginDelayedTransition(vgLocation, new Slide(Gravity.TOP));
      ivLocation3.setVisibility(VISIBLE);
      TransitionManager.beginDelayedTransition(vgLocation, new Slide(Gravity.TOP));
      ivLocation4.setVisibility(VISIBLE);
    }, 1000);
  }

  @OnClick(R.id.btn_skip)
  void skip() {
    if (switcherListener != null) {
      switcherListener.switchFragment(ManualAddressFragment.newInstance(type), true, true);
    }

  }

  @Override
  public void onDetach() {
    super.onDetach();
    mListener = null;
  }


  @OnClick(R.id.btn_grant_access)
  void onGrantAccess() {
    if (ContextCompat.checkSelfPermission(getActivity(), ACCESS_FINE_LOCATION)
        == PackageManager.PERMISSION_GRANTED
        && ContextCompat.checkSelfPermission(getActivity(), ACCESS_COARSE_LOCATION)
        == PackageManager.PERMISSION_GRANTED) {
      getlocation();

    } else if (
        ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), ACCESS_FINE_LOCATION)
            && ActivityCompat
            .shouldShowRequestPermissionRationale(getActivity(), ACCESS_COARSE_LOCATION)) {
      // We've been denied once before. Explain why we need the permission, then ask again.
      Utils.showDialog(getContext(),
          getString(R.string.location_permission_required_text),
          getString(R.string.ask_permission_text),
          getString(R.string.discard_text),
          (dialog, which) -> {
            if (which == -1) {
              requestPermissions(new String[]{ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION},
                  REQUEST_PERMISSION_LOCATION);
            } else {
              dialog.dismiss();
            }
          });
    } else {
      // We've never asked. Just do it.
      requestPermissions(new String[]{ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION},
          REQUEST_PERMISSION_LOCATION);
    }

  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
      @NonNull int[] grantResults) {
    if (requestCode == REQUEST_PERMISSION_LOCATION
        && grantResults[0] == PackageManager.PERMISSION_GRANTED
        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
      getlocation();
    } else {
      // We were not granted permission this time, so don't try to show the contact picker
      super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
  }

  public void getlocation() {
    GPSTracker gps = new GPSTracker(getActivity());
    // check if GPS enabled
    if (gps.canGetLocation()) {
      double latitude = gps.getLatitude();
      double longitude = gps.getLongitude();

      Log.i(getClass().getSimpleName(),
          "Your Location is - \nLat: " + latitude + "\nLong: " + longitude);
      mListener.onSkip(latitude,longitude);
    } else {
      Utils.showAlertDialog(getContext(), "Error!",
          "Sorry unable to get current location. Make sure your GPS is ON!");
    }
  }

  public interface OnFragmentInteractionListener {
    void onSkip(double latitude, double longitude);
  }

}


