package com.app.inn.peepsin.UI.fragments.SellStuffModule;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.BaseModel;
import com.app.inn.peepsin.Models.CurrentUser;
import com.app.inn.peepsin.Models.MyShop;
import com.app.inn.peepsin.Models.SellingProduct;
import com.app.inn.peepsin.Models.ShopCategory;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.adapters.InventoryAdapter;
import com.app.inn.peepsin.UI.adapters.SellingStuffAdapter;
import com.app.inn.peepsin.UI.adapters.ShopInventoryAdapter;
import com.app.inn.peepsin.UI.fragments.Profile.ShoppingCartFragment;
import com.app.inn.peepsin.UI.helpers.DividerItemRecyclerDecoration;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kFirstName;
import static com.app.inn.peepsin.Constants.Constants.kShop;
import static com.app.inn.peepsin.Constants.Constants.kUserId;

public class UserSellingStuffFragment extends Fragment implements ShopInventoryAdapter.InventoryFilter{
    private String TAG = UserSellingStuffFragment.class.getSimpleName();
    private static Switcher switcherListener;
    @BindView(R.id.recycler_view_feeds) RecyclerView recyclerViewFeeds;
    @BindView(R.id.tv_title) TextView tvTitle;
    @BindView(R.id.empty_view) View  emptyView;
    @BindView(R.id.tv_empty_title) TextView tvEmptyTitle;
    @BindView(R.id.tv_empty_message) TextView tvEmptyMessage;
    @BindView(R.id.tv_cartCount) TextView tvCartCount;

    @BindView(R.id.recycler_view_inventrory_item) RecyclerView recyclerViewInventory;
    @BindView(R.id.swipeRefreshLayout) SwipeRefreshLayout swipeRefreshLayout;

    private ProgressDialog progressDialog;
    private List<SellingProduct> productList;
    private List<ShopCategory> inventoryList;
    private SellingStuffAdapter stuffAdapter;
    private ShopInventoryAdapter inventoryAdapter;
    private int shopId;
    private Integer mUserId;
    private String  mUserName;
    private MyShop shop;

    SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            refreshData(mUserId);
            loadShopCategory();
        }
    };

    /**
     * @param switcher listener to switch between fragments
     * @param userId userId for which selling stuff to be shown
     * @param name username for toolbar title
     * @param shop shop object for product details
     * @return @{@link UserSellingStuffFragment}
     */
    public static UserSellingStuffFragment newInstance(Switcher switcher, int userId, String name, MyShop shop) {
        switcherListener = switcher;
        UserSellingStuffFragment fragment = new UserSellingStuffFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(kUserId,userId);
        bundle.putString(kFirstName,name);
        bundle.putSerializable(kShop,shop);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = Utils.generateProgressDialog(getContext(), true);
        productList = new ArrayList<>();
        inventoryList = new ArrayList<>();
        Bundle bundle = getArguments();
        if(bundle!= null){
            mUserId = bundle.getInt(kUserId);
            mUserName = bundle.getString(kFirstName);
            shop = (MyShop)bundle.getSerializable(kShop);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_user_selling_stuff, container, false);
        ButterKnife.bind(this,view);
        String title = mUserName+"'s "+ getString(R.string.title_selling_stuff);
        tvTitle.setText(title);
        shopId = shop.getId();
        tvEmptyTitle.setText(getString(R.string.empty_title_no_selling_stuff));
        tvEmptyMessage.setText(getString(R.string.empty_message_no_selling_stuff));

        stuffAdapter = new SellingStuffAdapter(getContext(), productList, switcherListener,1,shop);
        recyclerViewFeeds.setHasFixedSize(true);
        recyclerViewFeeds.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerViewFeeds.setAdapter(stuffAdapter);
        recyclerViewFeeds.addItemDecoration(new DividerItemRecyclerDecoration(getActivity(), R.drawable.canvas_recycler_divider));
        swipeRefreshLayout.setOnRefreshListener(onRefreshListener);
        checkEmptyScreen();

        // Inventory Adapter and Inventory List
        inventoryAdapter = new ShopInventoryAdapter(getContext(),inventoryList, this);
        LinearLayoutManager HorizontalLayout = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerViewInventory.setLayoutManager(HorizontalLayout);
        recyclerViewInventory.setAdapter(inventoryAdapter);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData(mUserId);
        loadShopCategory();
    }

    private void refreshData(Integer userId) {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(BaseModel.kUserId, userId);
        ModelManager.modelManager().getSellingProducts(parameters, (Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<SellingProduct>> genericResponse) -> {
            stuffAdapter.addNewItems(genericResponse.getObject());
            checkEmptyScreen();
            swipeRefreshLayout.setRefreshing(false);
        }, (Constants.Status iStatus, String message) -> {
            Toaster.toast(message);
            swipeRefreshLayout.setRefreshing(false);
            checkEmptyScreen();
        });
    }


    @OnClick(R.id.btn_back)
    void onBack(){
        getFragmentManager().popBackStack();
    }


    @OnClick(R.id.btn_cart)
    void onCart(){
        if(switcherListener!=null)
            switcherListener.switchFragment(ShoppingCartFragment.newInstance(switcherListener),true,true);
        //startActivity(ShoppingCartActivity.getIntent(getContext()));
    }

    @Override
    public void onResume() {
        super.onResume();
        CurrentUser currentUser = ModelManager.modelManager().getCurrentUser();
        setCartUpdate(currentUser.getShoppingCartCount());
    }

    public void setCartUpdate(int count){
        if(count==0)
            tvCartCount.setVisibility(View.GONE);
        else{
            tvCartCount.setVisibility(View.VISIBLE);

        }
    }


    private void loadData(int userId) {
        showProgress(true);
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(BaseModel.kUserId, userId);
        ModelManager.modelManager().getSellingProducts(parameters,(Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<SellingProduct>> genericResponse) -> {
            productList = genericResponse.getObject();
            stuffAdapter.addNewItems(genericResponse.getObject());
            showProgress(false);
            checkEmptyScreen();
        }, (Constants.Status iStatus, String message) -> {
            Toaster.toast(message);
            showProgress(false);
            checkEmptyScreen();
        });
    }

    public void loadShopCategory() {
        ModelManager.modelManager().getShopCategoryList(shopId, (Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<ShopCategory>> genericResponse) -> {
            inventoryList=genericResponse.getObject();
            inventoryAdapter.addItems(inventoryList);
        },(Constants.Status iStatus, String message) ->{
            Log.e(TAG, message);
            Toaster.toast(message);
        });
    }

    private void checkEmptyScreen() {
        if(stuffAdapter.getItemCount()>0){
            emptyView.setVisibility(View.GONE);
        }else {
            emptyView.setVisibility(View.VISIBLE);
        }
    }

    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) progressDialog.cancel();
        }
    }

    @Override
    public void filter(Integer id) {
        showProgress(true);
        CopyOnWriteArrayList<SellingProduct> list = new CopyOnWriteArrayList<>();
        for (int i = 0; i < productList.size(); i++) {
            if (productList.get(i).getCategoryId().equals(id))
                list.add(productList.get(i));
        }
        stuffAdapter.addNewItems(list);
        checkEmptyScreen();
        showProgress(false);
    }
}
