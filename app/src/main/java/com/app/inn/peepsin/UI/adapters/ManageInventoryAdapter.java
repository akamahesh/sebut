package com.app.inn.peepsin.UI.adapters;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.BaseModel;
import com.app.inn.peepsin.Models.ProductInventory;
import com.app.inn.peepsin.Models.SellingProduct;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.fragments.Shop.ManageInventoryFragment;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Harsh on 8/24/2017.
 */

public class ManageInventoryAdapter extends RecyclerView.Adapter<ManageInventoryAdapter.ViewHolder> {

    private Context context;
    private List<SellingProduct> feedList;
    private ProgressDialog progressDialog;
    private ManageInventoryFragment fragment;

    public ManageInventoryAdapter(Context context, List<SellingProduct> items, ManageInventoryFragment fragment) {
        this.context = context;
        this.fragment = fragment;
        feedList = items;
        progressDialog = Utils.generateProgressDialog(context, false);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_manage_inventory, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ManageInventoryAdapter.ViewHolder holder, int position) {
        SellingProduct item = feedList.get(position);
        holder.bindContent(item);
        String name = item.getName();
        String costPrice    = context.getString(R.string.Rs) + " " + item.getPrice();
        String sellPrice    = context.getString(R.string.Rs) + " " + item.getSellingPrice();
        String thumbnailURL = item.getImageThumbUrl();
        String skui         = item.getSKUNumber();
        String quantity     = String.valueOf(item.getQuantity()) ;
        String discount     = context.getString(R.string.no_offer);
        if (!item.getDiscountTagLine().isEmpty()){
            discount = item.getDiscountTagLine();
            holder.tvCostPrice.setPaintFlags(holder.tvCostPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }


        if(item.getPrice().equals(item.getSellingPrice())){
            holder.vPrice.setVisibility(View.GONE);
        }else {
            holder.vPrice.setVisibility(View.VISIBLE);
        }

        holder.tvName.setText(name);
        holder.tvSKUI.setText(skui);
        holder.tvCostPrice.setText(costPrice);
        holder.tvSellPrice.setText(sellPrice);
        holder.tvOffer.setText(discount);
        holder.tvQuantity.setText(quantity);
        if (!thumbnailURL.isEmpty())
            Picasso.with(context)
                    .load(thumbnailURL)
                    .placeholder(R.drawable.img_product_placeholder)
                    .into(holder.ivProduct);
    }

    public void addItems(List<SellingProduct> list) {
        feedList.clear();
        feedList.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return feedList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        SellingProduct item;
        @BindView(R.id.iv_item) ImageView ivProduct;
        @BindView(R.id.tv_name) TextView tvName;
        @BindView(R.id.tv_sku_no) TextView tvSKUI;
        @BindView(R.id.tv_price) TextView tvCostPrice;
        @BindView(R.id.tv_offer_price) TextView tvSellPrice;
        @BindView(R.id.v_price) View vPrice;
        @BindView(R.id.tv_offer) TextView tvOffer;
        @BindView(R.id.tv_quantity) TextView tvQuantity;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.item_view)
        void onRowSelected() {
            editInventoryDialog(item);
        }

        private void bindContent(SellingProduct item) {
            this.item = item;
        }

        private void editInventoryDialog(SellingProduct item) {
            final Dialog dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.dialog_new_manage_inventry);
            EditText edtPrice = ButterKnife.findById(dialog, R.id.edt_price);
            EditText edtSell  = ButterKnife.findById(dialog, R.id.edt_offer_price);
            EditText edtDiscount = ButterKnife.findById(dialog, R.id.edt_discount);
            EditText edtQuantity = ButterKnife.findById(dialog, R.id.edt_quantity);

            String costPrice = item.getPrice().replace(".00/-","");
            String sellPrice = item.getSellingPrice().replace(".00/-","");
            edtPrice.setText(costPrice);
            edtPrice.setSelection(edtPrice.getText().length());
            edtSell.setText(sellPrice);
            edtDiscount.setText(item.getDiscountTagLine());
            edtQuantity.setText(String.valueOf(item.getQuantity()));

            editTextListener(edtPrice,edtSell,edtDiscount);

            Button btnChange = ButterKnife.findById(dialog, R.id.btn_save);
            btnChange.setOnClickListener(v -> {
                if(edtPrice.getText().toString().isEmpty()) {
                    edtPrice.setError("Price is must");
                } else if (edtSell.getText().toString().isEmpty()) {
                    edtSell.setError("Sale Price is must");
                } else if (!edtPrice.getText().toString().isEmpty() && !edtSell.getText().toString().isEmpty()) {
                    int cost = Integer.parseInt(edtPrice.getText().toString());
                    int sell = Integer.parseInt(edtSell.getText().toString());
                    if(cost<sell) {
                        Toaster.customToast("Cost Price cannot be less than sell price");
                    }else if(cost>200000 || sell>200000) {
                        Toaster.customToast("Price can't exceed more than 2 Lakhs");
                    }
                    else if (edtQuantity.getText().toString().isEmpty()) {
                        edtQuantity.setError("Quantity is must");
                    } else{
                        dialog.dismiss();
                        HashMap<String ,Object> parameters = new HashMap<>();
                        parameters.put(BaseModel.kProductId, item.getId());
                        parameters.put(BaseModel.kSKUNumber, item.getSKUNumber());
                        parameters.put(BaseModel.kPrice, edtPrice.getText().toString());
                        parameters.put(BaseModel.kSellingPrice, edtSell.getText().toString());
                        parameters.put(BaseModel.kQuantity, edtQuantity.getText().toString());
                        parameters.put(BaseModel.kDiscountTagLine, edtDiscount.getText().toString());
                        editInventory(parameters);
                    }
                }
            });
            dialog.show();
        }

        private void editTextListener(EditText tvPrice,EditText tvSalePrice,EditText tvDiscount) {
            tvPrice.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void afterTextChanged(Editable start) {
                    if (!tvSalePrice.getText().toString().isEmpty() && !tvPrice.getText().toString().isEmpty()) {
                        int costPrice = Integer.parseInt(tvPrice.getText().toString());
                        int sellPrice = Integer.parseInt(tvSalePrice.getText().toString());
                        if (costPrice > sellPrice) {
                            int discountPrice = costPrice - sellPrice;
                            float discount = (float) discountPrice / costPrice;
                            String discountTag = (int) (discount * 100) + "% off";
                            tvDiscount.setText(discountTag);
                        } else {
                            tvDiscount.setText("");
                        }
                    } else
                        tvDiscount.setText("");
                }{
                    tvDiscount.setText("");
                }
            });
            tvSalePrice.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void afterTextChanged(Editable start) {
                    if (!tvSalePrice.getText().toString().isEmpty() && !tvPrice.getText().toString().isEmpty()) {
                        int costPrice = Integer.parseInt(tvPrice.getText().toString());
                        int sellPrice = Integer.parseInt(tvSalePrice.getText().toString());
                        if (costPrice > sellPrice) {
                            int discountPrice = costPrice - sellPrice;
                            float discount = (float) discountPrice / costPrice;
                            String discountTag = (int) (discount * 100) + "% off";
                            tvDiscount.setText(discountTag);
                        } else {
                            tvDiscount.setText("");
                        }
                    } else
                        tvDiscount.setText("");
                }
            });
        }

        private void editInventory(HashMap<String,Object> parameters){
            showProgress(true);
            ModelManager.modelManager().editProductInventory(parameters, (Constants.Status iStatus, GenricResponse<ProductInventory> genericResponse) -> {
                ProductInventory inventory = genericResponse.getObject();
                fragment.loadData(ModelManager.modelManager().getCurrentUser().getUserId());
                showProgress(false);
            }, (Constants.Status iStatus, String message) -> {
                Toaster.toast(message);
                showProgress(false);
            });
        }

    }

    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) {
                progressDialog.cancel();
            }
        }
    }
}
