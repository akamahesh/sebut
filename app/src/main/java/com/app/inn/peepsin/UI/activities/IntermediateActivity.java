package com.app.inn.peepsin.UI.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.fragments.Shop.ShopIntermediateFragment;
import com.app.inn.peepsin.UI.helpers.FragmentUtil;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.interfaces.Switcher;

import butterknife.ButterKnife;

/**
 * Created by Harsh on 5/15/2017.
 */

public class IntermediateActivity extends AppCompatActivity {

    public static Intent getIntent(Context context){
        Intent in = new Intent(context,IntermediateActivity.class);
        in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return in;
    }

    Switcher switcher = (fragment, saveInBackstack, animate) -> {
        FragmentUtil.changeFragment(getSupportFragmentManager(),fragment,saveInBackstack,animate);
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
        setContentView(R.layout.fragment_root);
        ButterKnife.bind(this);

        FragmentUtil.changeFragment(getSupportFragmentManager(), ShopIntermediateFragment.newInstance(switcher),false,true);
    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
    }

    @Override
    public void onBackPressed() {
        int count = getSupportFragmentManager().getBackStackEntryCount();
         if (count > 0) {
             if(getSupportFragmentManager().getBackStackEntryAt(count-1).getName().equals("com.app.inn.peepsin.UI.fragments.Shop.ProductIntermediateFragment"))
                 Toaster.toast("Please Use [Skip] in Navigation bar to skip this step.");
             else
                 getSupportFragmentManager().popBackStack();
        }else {
            Toaster.toast("Please Use [Skip] in Navigation bar to skip this step.");
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        switcher=null;
    }
}

