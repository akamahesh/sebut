package com.app.inn.peepsin.UI.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.FinalProductsRecords;
import com.app.inn.peepsin.Models.OrderHistory;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.fragments.Profile.OrderHistoryDetailFragment;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import static com.app.inn.peepsin.Constants.Constants.kIsSuccessfullyCanceled;
import static com.app.inn.peepsin.Constants.Constants.kIsSuccessfullyRemoved;

/**
 * Created by dhruv on 4/8/17.
 */

public class OrderHistoryAdapter extends RecyclerView.Adapter<OrderHistoryAdapter.ViewHolder> {

    private CopyOnWriteArrayList<OrderHistory> orderList;
    private Switcher switchListener;
    private Context context;
    private ProgressDialog progressDialog;

    public OrderHistoryAdapter(Switcher switcher, Context context, CopyOnWriteArrayList<OrderHistory> orderHistoryList) {
        progressDialog = Utils.generateProgressDialog(context, true);
        this.orderList = orderHistoryList;
        this.switchListener = switcher;
        this.context = context;
    }

    @Override
    public OrderHistoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_new_order_history, parent, false);
        return new OrderHistoryAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(OrderHistoryAdapter.ViewHolder holder, int position) {
        OrderHistory item = orderList.get(position);
        holder.bindContent(item);
        String orderId = String.valueOf(item.getOrderId());
        String orderDate = item.getOrderDate();
        holder.tvOrderDate.setText(orderDate);
        holder.tvOrderIdNumber.setText(orderId);
        if (position == 0)
            holder.dateLayout.setVisibility(View.GONE);
        else {
            if (!orderDate.equals(orderList.get(position - 1).getOrderDate()))
                holder.dateLayout.setVisibility(View.VISIBLE);
            else
                holder.dateLayout.setVisibility(View.GONE);
        }

        // Order Status
        holder.tvOrderStatus.setText(Utils.getStatus(item.getOrderStatus()));
        if (item.getOrderStatus() == 5) {
            holder.tvOrderStatus.setTextColor(Color.parseColor("#FFFF0000"));
        } else {
            holder.tvOrderStatus.setTextColor(Color.parseColor("#30C430"));
        }
        switch(item.getOrderStatus()) {
            case 1 :
                holder.ivStatusIcon.setImageResource(R.drawable.ic_packed_img);
                break; // optional
            case 2 :
                holder.ivStatusIcon.setImageResource(R.drawable.ic_processing_img);
                break; // optional
            case 3 :
                holder.ivStatusIcon.setImageResource(R.drawable.ic_shipp_img);
                break; // optional
            case 4 :
                holder.ivStatusIcon.setImageResource(R.drawable.ic_delivered_img);
                break; // optional
            case 5 :
                holder.ivStatusIcon.setImageResource(R.drawable.ic_cancellation_img);
                break; // optional
            case 6 :
                holder.ivStatusIcon.setImageResource(R.drawable.ic_not_recognize_img);
                break; // optional
            case 7 :
                holder.ivStatusIcon.setImageResource(R.drawable.ic_placed_img);
                break; // optional
            default : // Optional
                holder.ivStatusIcon.setImageResource(R.drawable.ic_not_recognize_img);
        }

        // Order Items
        String totalItem = "0";
        if (item.getCartItemCount() != null)
            try {
                int quantity = 0;
                for(int i=0;i<item.getShopRecord().get(0).getProductRecords().size();i++) {
                    quantity += item.getShopRecord().get(0).getProductRecords().get(i).getProductQuantity();
                }
                if (quantity == 1) {
                    totalItem = String.valueOf(item.getCartItemCount()) + " "
                            + "Product " /*+ " " + (quantity + " " + "Quantity")*/;
                } else {
                    totalItem = String.valueOf(item.getCartItemCount()) + " "
                            + "Product " /*+ " " + (quantity + " " + "Quantities")*/;
                }
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        holder.tvOrderItem.setText(totalItem);
        // Total Amount
        String totalAmount = "0";
        if (item.getFinalAmountAfterPromoCode() != null)
            totalAmount = item.getFinalAmountAfterPromoCode();
        holder.tvOrderTotalAmmount.setText(totalAmount);

        // Order Delivery Address
        String addressTxt = "No Location Available";
        String orderUsername = "";
        String orderPhone = "";
        if (item.getDeliveryAddress() != null){
            addressTxt = item.getDeliveryAddress().getStreetAddress() + ", "
                    + item.getDeliveryAddress().getCity() + ", "
                    + item.getDeliveryAddress().getState() + ", "
                    + item.getDeliveryAddress().getCountry()+", "
                    + item.getDeliveryAddress().getZipCode();
            orderUsername = item.getDeliveryAddress().getFullName();
            orderPhone = item.getDeliveryAddress().getPhone();
        }
        holder.tvOrderAddress.setText(addressTxt);
        holder.tvOrderUserName.setText(orderUsername);
        holder.tvOrderPhone.setText(orderPhone);

        // Payment Type
        if (item.getPaymentType() != null) {
            switch (item.getPaymentType()) {
                case 1:
                    holder.tvPaymentMethod.setText(R.string.text_paytm);
                    break;
                case 2:
                    holder.tvPaymentMethod.setText(R.string.text_pay_u_money);
                    break;
                case 3:
                    holder.tvPaymentMethod.setText(R.string.text_cash_on_delivery);
                    break;
                default:
                    holder.tvPaymentMethod.setText(R.string.pending_payment);
            }
        }

        /*String discounrtag = item.getPromoCodeTagLine();
        holder.tvDiscountTagLine.setText(discounrtag);
        String paymentbeforepromocode = "";
        if (item.getTotalCartPriceBeforePromoCode() != null)
            paymentbeforepromocode = item.getTotalCartPriceBeforePromoCode();
        String promocodeammount = "---";
        if (!item.getDiscountedPrice().equals(""))
            promocodeammount = item.getDiscountedPrice();
        holder.tvPaymentBeforePromoAmmount.setText(paymentbeforepromocode);
        holder.tvPromoCodeAmmount.setText(promocodeammount);*/
    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }

    public OrderHistory getItem(int position) {
        return orderList.get(position);
    }

    public void addItems(CopyOnWriteArrayList<OrderHistory> inventoryList) {
        this.orderList.clear();
        this.orderList.addAll(inventoryList);
        notifyDataSetChanged();
    }

    public void addMoreItems(CopyOnWriteArrayList<OrderHistory> inventoryList) {
        this.orderList.addAll(inventoryList);
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        OrderHistory order;
        @BindView(R.id.tv_order_date)
        TextView tvOrderDate;
        @BindView(R.id.tv_order_status)
        TextView tvOrderStatus;
        @BindView(R.id.tv_order_item)
        TextView tvOrderItem;
        @BindView(R.id.tv_order_id_number)
        TextView tvOrderIdNumber;
        @BindView(R.id.tv_order_address_user_name)
        TextView tvOrderUserName;
        @BindView(R.id.tv_order_address_user)
        TextView tvOrderAddress;
        @BindView(R.id.tv_order_address_number)
        TextView tvOrderPhone;
        @BindView(R.id.tv_payment_mehtod)
        TextView tvPaymentMethod;
        @BindView(R.id.tv_order_total_ammount)
        TextView tvOrderTotalAmmount;
        @BindView(R.id.img_status_icon)
        ImageView ivStatusIcon;
        /*@BindView(R.id.tv_payment_before_promo_ammount)
        TextView tvPaymentBeforePromoAmmount;
        @BindView(R.id.tv_promo_code_ammount)
        TextView tvPromoCodeAmmount;
        @BindView(R.id.tv_discount_tag_line)
        TextView tvDiscountTagLine;*/
        @BindView(R.id.date_view)
        View dateLayout;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindContent(OrderHistory item) {
            this.order = item;
        }

        @OnClick(R.id.btn_show_product)
        void onItemClick() {
            if (switchListener != null){
                List<FinalProductsRecords> shopList = new ArrayList<>();
                for (OrderHistory ordery:orderList) {
                    if(order.getOrderId().equals(ordery.getOrderId())){
                        shopList.addAll(ordery.getShopRecord());
                    }
                }
                switchListener.switchFragment(OrderHistoryDetailFragment.newInstance(shopList), true, true);
            }
        }

        @OnClick(R.id.status_btn)
        void onChangeStatus(View v){
            if(order.getOrderStatus()<4)
                onStatus(v, order, getAdapterPosition());
            else if(order.getOrderStatus()==4)
                Utils.showAlertDialog(context,"Delivery Alert","Your order has been delivered to your shipping address.");
            else if(order.getOrderStatus()==5 || order.getOrderStatus()==6)
                Utils.showAlertDialog(context,"Product Alert","Your order has been cancelled or declined.");
            else if(order.getOrderStatus()==7)
                Utils.showAlertDialog(context,"Product Alert","You requested for order return to seller");
        }
    }

    private void onStatus(View v, OrderHistory order, int position) {
        //creating a popup menu
        PopupMenu popup = new PopupMenu(context, v);
        //inflating menu from xml resource
        popup.inflate(R.menu.menu_order);
        //setMenuView(popup, order);
        //adding click listener
        popup.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.action_status:
                    cancelOrder(order.getOrderId(), position);
                    /*if (item.getTitle().equals("Cancel"))
                        cancelOrder(order.getOrderId(), position);
                    else if (item.getTitle().equals("Remove"))
                        removeOrder(order.getOrderId(), position);*/
                    break;
            }
            return false;
        });
        //displaying the popup
        popup.show();
    }

    private void setMenuView(PopupMenu menu, OrderHistory order) {
        MenuItem actionStatus = menu.getMenu().getItem(0);
        if (order.getOrderStatus() >= 5)
            actionStatus.setTitle("Cancel");
        else
            actionStatus.setTitle("Remove");
    }

    private void cancelOrder(int orderId, int position) {
        showProgress(true);
        ModelManager.modelManager().cancelPlacedOrder(orderId, (Constants.Status iStatus, GenricResponse<HashMap<String, Object>> genericResponse) -> {
            HashMap<String, Object> map = genericResponse.getObject();
            Integer isSuccessful = (Integer) map.get(kIsSuccessfullyCanceled);
            if (isSuccessful == 1) {
                orderList.get(position).setOrderStatus(5);
                notifyDataSetChanged();
            }
            showProgress(false);
        }, (Constants.Status iStatus, String message) -> {
            Log.e("Order Status", message);
            showProgress(false);
        });
    }

    private void removeOrder(int orderId, int position) {
        showProgress(true);
        ModelManager.modelManager().removePlacedOrder(orderId, (Constants.Status iStatus, GenricResponse<HashMap<String, Object>> genericResponse) -> {
            HashMap<String, Object> map = genericResponse.getObject();
            Integer isSuccessful = (Integer) map.get(kIsSuccessfullyRemoved);
            if (isSuccessful == 1) {
                orderList.remove(position);
                notifyDataSetChanged();
            }
            showProgress(false);
        }, (Constants.Status iStatus, String message) -> {
            Log.e("Order Status", message);
            Toaster.customToast(message);
            showProgress(false);
        });
    }


    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) progressDialog.cancel();
        }
    }
}
