package com.app.inn.peepsin.UI.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.Feeds;
import com.app.inn.peepsin.Models.ShoppingProduct;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.fragments.Feeds.ProductDetailFragment;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by dhruv on 14/9/17.
 */

public class CartProductsAdapter extends RecyclerView.Adapter<CartProductsAdapter.ViewHolder> {

    private List<ShoppingProduct> productList;
    private Context mContext;
    private static Switcher switcherListener;
    private ProgressDialog progressDialog;

    public CartProductsAdapter(Switcher switcher,Context context, List<ShoppingProduct> productShop){
        this.mContext = context;
        this.productList = productShop;
        switcherListener = switcher;
        progressDialog = Utils.generateProgressDialog(context, false);

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_cart_bottom_sheet_layout, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        ShoppingProduct product = productList.get(position);
        holder.bindContent(product);
        // Product Details
        holder.tvProductName.setText(product.getName());
        String price = mContext.getString(R.string.product_price)+"0.00/-";
        if(!product.getNetPrice().isEmpty()){
            price = mContext.getString(R.string.product_price)+"\t"+product.getNetPrice();
        }
        holder.tvPrice.setText(price);

        String totalPrice = mContext.getString(R.string.product_price)+"0.00/-";
        if(!product.getNetPrice().isEmpty()){
            totalPrice = mContext.getString(R.string.product_price)+"\t"+product.getPrice();
        }
        holder.tvTotalAmount.setText(totalPrice);

        String quantity = "0";
        if(product.getQuantity()!=0){
            quantity = String.valueOf(product.getQuantity());
        }
        holder.tvQuantity.setText(quantity);
        holder.tvDiscount.setText(product.getDiscountTagLine());

        if(!product.getCoverImageUrl().isEmpty())
            Picasso.with(mContext)
                    .load(product.getCoverImageUrl())
                    .fit()
                    .placeholder(R.drawable.img_product_placeholder)
                    .into(holder.ivProductPic);
        else
            holder.ivProductPic.setImageResource(R.drawable.img_product_placeholder);
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public void addItems(List<ShoppingProduct> list){
        productList.clear();
        productList.addAll(list);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ShoppingProduct item;
        @BindView(R.id.iv_product)
        ImageView ivProductPic;
        @BindView(R.id.tv_product_name)
        TextView tvProductName;
        @BindView(R.id.tv_quantity)
        TextView tvQuantity;
        @BindView(R.id.tv_total_amount)
        TextView tvTotalAmount;
        @BindView(R.id.tv_product_price)
        TextView tvPrice;
        @BindView(R.id.tv_discount)
        TextView tvDiscount;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        @OnClick(R.id.item_view)
        void onRowSelected() {
            getFeedDetails(item.getId());
        }

        private void getFeedDetails(int productId) {
            showProgress(true);
            ModelManager.modelManager().getProductDetails(productId,(Constants.Status iStatus, GenricResponse<Feeds> genericResponse) -> {
                showProgress(false);
                if(switcherListener!=null)
                    switcherListener.switchFragment(ProductDetailFragment.newInstance(productId, genericResponse.getObject(), switcherListener),true,true);
            }, (Constants.Status iStatus, String message) -> {
                Toaster.toast(message);
                showProgress(false);
            });
        }
        public void bindContent(ShoppingProduct product) {
            this.item = product;
        }
    }

    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) {
                progressDialog.cancel();
            }
        }
    }
}
