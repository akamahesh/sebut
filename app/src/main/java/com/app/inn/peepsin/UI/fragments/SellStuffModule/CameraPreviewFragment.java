package com.app.inn.peepsin.UI.fragments.SellStuffModule;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.CurrentUser;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.helpers.ItemOffsetDecoration;
import com.app.inn.peepsin.UI.helpers.Preview;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.zelory.compressor.Compressor;

import static com.app.inn.peepsin.Constants.Constants.kRecords;


/**
 * Created by akamahesh on 2/5/17.
 */

public class CameraPreviewFragment extends Fragment{

    private Context context;
    private CameraPreviewInteractionListener mListener;
    private int GALLARY_RESULT=101;

    private Preview preview;
    private Camera camera;
    private Calendar calendar;

    private PictureAdapter pictureAdapter;
    private ArrayList<String> imagePathList;
    private CurrentUser user;

    Camera.Parameters params;
    boolean isFlashOn;
    @BindView(R.id.progress_layout)
    View progressLayout;
    @BindView(R.id.recycler_view_images)
    RecyclerView recyclerView;
    @BindView(R.id.surface_view)
    SurfaceView surfaceView;
    @BindView(R.id.container)
    FrameLayout previewContainer;


    public interface CameraPreviewInteractionListener {
        void onUpload(List<String> imagePathList);
    }

    public static CameraPreviewFragment newInstance(ArrayList<String> imageList) {
        CameraPreviewFragment fragment = new CameraPreviewFragment();
        Bundle bundle = new Bundle();
        bundle.putStringArrayList(kRecords, imageList);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        if (context instanceof CameraPreviewInteractionListener) {
            mListener = (CameraPreviewInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement CameraPreviewInteractionListener");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        user = ModelManager.modelManager().getCurrentUser();
        if (bundle != null) {
            imagePathList = bundle.getStringArrayList(kRecords);
        } else {
            imagePathList = new ArrayList<>();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_camera_preview, container, false);
        ButterKnife.bind(this, view);
        calendar = Calendar.getInstance();
        preview = new Preview(getActivity(), surfaceView);
        previewContainer.addView(preview);
        preview.setKeepScreenOn(true);
        pictureAdapter = new PictureAdapter(getContext(), imagePathList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(pictureAdapter);
        recyclerView.addItemDecoration(new ItemOffsetDecoration(context, R.dimen.item_offset));
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        String message = "Hello "+user.getFullName()+" , Lets start with a nice cover image";
        Toaster.customToast(message);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @OnClick(R.id.iv_capture)
    void captureImage() {
        try {
            takeFocusedPicture();
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }
        progressLayout.setVisibility(View.VISIBLE);
    }


    @OnClick(R.id.iv_flash)
    void flashOn() {
        boolean hasFlash = context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
        if (hasFlash) {
            if (isFlashOn) {
                turnOffFlash();
            } else {
                turnOnFlash();
            }
        } else {
            Toaster.toast("Device do not have flash");
        }
    }

    @OnClick(R.id.iv_gallary)
    void getGallery() {
        if (isFlashOn) {
            turnOffFlash();
        }

        if(pictureAdapter.getItemCount()>6){
            Utils.showAlertDialog(getContext(), "Image count : " + pictureAdapter.getItemCount(), "Only Top 6 images can be added!");
            return;
        }
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_PICK);
        startActivityForResult(Intent.createChooser(intent,"Select Picture"), GALLARY_RESULT);
    }

    //Turning On flash
    private void turnOnFlash() {
        if (!isFlashOn) {
            if (camera == null || params == null) {
                return;
            }
            params = camera.getParameters();
            params.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
            camera.setParameters(params);
            isFlashOn = true;
        }
    }

    //Turning Off flash
    private void turnOffFlash() {
        if (isFlashOn) {
            if (camera == null || params == null) {
                return;
            }
            params = camera.getParameters();
            params.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
            camera.setParameters(params);
            isFlashOn = false;
        }
    }

    public void takeFocusedPicture() {
        camera.autoFocus(mAutoFocusCallback);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GALLARY_RESULT && resultCode == Activity.RESULT_OK) {
            if(data==null)
                return;
            Uri imageUri = data.getData();
            String path2 = Utils.getPathForMediaUri(getContext(),imageUri);
            if(path2!=null){
                addImageToList(path2);
            }else{
                Utils.showAlertDialog(getContext(),"Process Error!","Unable to find image! Please try again");
            }

        }
    }

    @OnClick(R.id.btn_cancel)
    void cancel() {
        getActivity().finish();
    }

    @OnClick(R.id.btn_upload)
    void upLoad() {
        if (imagePathList.isEmpty()) {
            Utils.showAlertDialog(getContext(), "Message Alert", "Upload at-least one image for cover!");
        } else {
            mListener.onUpload(imagePathList);
            getFragmentManager().popBackStack();
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        try {
            if (camera == null) {
                camera = Camera.open();
                camera.startPreview();
                params = camera.getParameters();
                camera.setErrorCallback((error, mcamera) -> {
                    camera.release();
                    camera = Camera.open();
                    Log.d("Camera died", "error camera");
                });
            } else {
                camera.stopPreview();
                camera.release();
                camera = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (camera != null) {
            setCameraDisplayOrientation((AppCompatActivity) context,
                    Camera.CameraInfo.CAMERA_FACING_BACK, camera);
            preview.setCamera(camera);
        }
    }

    @Override
    public void onPause() {
        if (camera != null) {
            camera.stopPreview();
            camera.release();
            camera = null;
        }
        super.onPause();
    }

    private void setCameraDisplayOrientation(Activity activity, int cameraId, Camera camera) {
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, info);
        int rotation = activity.getWindowManager().getDefaultDisplay()
                .getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360; // compensate the mirror
        } else { // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        camera.setDisplayOrientation(result);
    }


    Camera.AutoFocusCallback mAutoFocusCallback = new Camera.AutoFocusCallback() {
        @Override
        public void onAutoFocus(boolean success, Camera camera) {
            try {
                camera.takePicture(mShutterCallback, null, jpegCallback);
            } catch (Exception ignored) {
                ignored.printStackTrace();
            }
        }
    };

    Camera.ShutterCallback mShutterCallback = () -> {
    };

    Camera.PictureCallback jpegCallback = new Camera.PictureCallback() {
        @SuppressWarnings("deprecation")
        public void onPictureTaken(byte[] data, Camera camera) {
            FileOutputStream outStream;
            File imageFile;
            try {
                String fileName = Utils.generateTimeStamp() + ".png";
                imageFile = new File(context.getCacheDir(), fileName);
                if(!imageFile.createNewFile())
                    return;
                outStream = new FileOutputStream(imageFile);
                outStream.write(data);
                outStream.close();
                outStream.flush();
                String path = imageFile.getAbsolutePath();
                addImageToList(path);
            } catch (IOException e) {
                e.printStackTrace();
            }

            camera.startPreview();
            progressLayout.setVisibility(View.GONE);
        }
    };

    private void addImageToList(String filepath) {
        File originalFile = new File(filepath);
        Log.i("File Size ","Original : "+originalFile.length()/1024+"KB");
        try {
            File compressedFile = new Compressor(getContext()).compressToFile(originalFile);
            Log.i("File Size ","Compressed : "+compressedFile.length()/1024+"KB");
            if (pictureAdapter.getItemCount() < 6) {
                imagePathList.add(compressedFile.getAbsolutePath());
                pictureAdapter.notifyDataSetChanged();
                recyclerView.smoothScrollToPosition(pictureAdapter.getItemCount());
            } else {
                Utils.showAlertDialog(getContext(), "Image count : " + pictureAdapter.getItemCount(), "Only Top 6 images can be added!");
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    class PictureAdapter extends RecyclerView.Adapter<PictureAdapter.PictureHolder> {
        private Context context;
        private List<String> pictureList;

        PictureAdapter(Context context, List<String> paths) {
            this.context = context;
            this.pictureList = paths;
        }

        @Override
        public PictureHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_picture_layout, parent, false);
            return new PictureHolder(layoutView);
        }

        @Override
        public void onBindViewHolder(PictureHolder holder, int position) {
            String url = pictureList.get(position);
            holder.bindContent(url);
            if (position == 0)
                holder.cover.setVisibility(View.VISIBLE);
            else
                holder.cover.setVisibility(View.GONE);

            if(URLUtil.isValidUrl(url)){
                Picasso.with(context)
                        .load(url)
                        .resize(100,120)
                        .placeholder(R.drawable.img_product_placeholder)
                        .into(holder.ivProductImage);
            }else {
                File imageFile =   new File(url);
                Picasso.with(context)
                        .load(imageFile)
                        .resize(100,120)
                        .placeholder(R.drawable.img_product_placeholder)
                        .into(holder.ivProductImage);
            }

        }

        @Override
        public int getItemCount() {
            return pictureList.size();
        }

        class PictureHolder extends RecyclerView.ViewHolder {
            @BindView(R.id.iv_product_image)
            ImageView ivProductImage;
            @BindView(R.id.tv_cover)
            FrameLayout cover;
            private String imagePath;

            PictureHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            void bindContent(String url) {
                imagePath = url;
            }

            @OnClick(R.id.iv_discard)
            void discard() {
                pictureList.remove(imagePath);
                notifyDataSetChanged();
            }
        }


    }

}
