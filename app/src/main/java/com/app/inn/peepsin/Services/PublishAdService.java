package com.app.inn.peepsin.Services;

import android.app.Activity;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Managers.UploadManager.UploadManager;
import com.app.inn.peepsin.Models.CurrentUser;
import com.app.inn.peepsin.Models.SellingProduct;
import com.app.inn.peepsin.UI.helpers.Toaster;

import java.util.HashMap;
import java.util.List;

import static com.app.inn.peepsin.Constants.Constants.kProductImageUrlList;
import static com.app.inn.peepsin.Constants.Constants.kRecords;

/**
 * Created by akaMahesh on 12/7/17.
 * copyright to : Innverse Technologies
 */

public class PublishAdService extends IntentService {
    private String TAG = getClass().getSimpleName();
    private int result = Activity.RESULT_CANCELED;
    public static final String kProductUpdateNotification = "com.app.inn.peepsin.Services.receiver";
    private NotificationManager mNotifyManager;
    private Notification.Builder mBuilder;
    private Integer notificationId = 1;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public PublishAdService(String name) {
        super(name);
    }


    public PublishAdService() {
        super("PublishAdService");
    }

    // will be called asynchronously by Android
    @Override
    protected void onHandleIntent(Intent intent) {
        HashMap<String, Object> productMap = (HashMap<String, Object>) intent.getSerializableExtra(kRecords);
        List<String> imageUrlList = intent.getStringArrayListExtra(kProductImageUrlList);
        showNotification();
        UploadManager.uploadManager().upload(imageUrlList, (Constants.Status iStatus, GenricResponse<HashMap<String, Object>> genricResponse) -> {
            if (iStatus == Constants.Status.fail) {
                Toaster.toast("Failed to add Product! Please try again after sometime.");
                itemUploadednotification(false);
            } else {
                HashMap<String, Object> partialMap = genricResponse.getObject();
                productMap.putAll(partialMap);
                submitAd(productMap);
            }
        });
    }

    private void submitAd(HashMap<String, Object> productMap) {
        ModelManager.modelManager().addSellingProduct(productMap, (Constants.Status iStatus, GenricResponse<SellingProduct> genricResponse) -> {
            // When the loop is finished, updates the notification
            itemUploadednotification(true);
            publishResults(genricResponse.getObject());
            Toaster.toastRangeen("Congratulations! \n Your Ad published successfully!");
        }, (Constants.Status iStatus, String message) -> {
            Toaster.toastRangeen("Process Failed! \n "+message+"\n Please try adding product after sometime.");
            itemUploadednotification(false);
        });
    }

    private void editAd(HashMap<String, Object> productMap) {
        ModelManager.modelManager().editSellingProduct(productMap, (Constants.Status iStatus, GenricResponse<SellingProduct> genricResponse) -> {
            // When the loop is finished, updates the notification
            itemUploadednotification(true);
            publishResults(genricResponse.getObject());
            Toaster.toastRangeen("Congratulations! \n Your Ad updated successfully!");
        }, (Constants.Status iStatus, String message) -> {
            Toaster.toastRangeen("Process Failed! \n Please try updating product after sometime.");
            itemUploadednotification(false);
        });
    }

    private void itemUploadednotification(boolean success) {
        if (success) {
            mBuilder.setContentTitle("PeepsIn")
                    .setContentText("Product added successfully")
                    .setSmallIcon(android.R.drawable.stat_sys_upload_done)
                    .setProgress(0, 0, false)
                    .setTicker("Product publishing successfull.")
                    .setOngoing(false);

            mNotifyManager.notify(notificationId, mBuilder.build());
        } else {
            mBuilder.setContentTitle("PeepsIn")
                    .setContentText("Product submission failed Please try again!")
                    .setSmallIcon(android.R.drawable.stat_sys_warning)
                    .setProgress(0, 0, false)
                    .setTicker("Product publishing failed.")
                    .setOngoing(false);
            mNotifyManager.notify(notificationId, mBuilder.build());
        }

    }

    private void publishResults(SellingProduct object) {
        Intent intent = new Intent(kProductUpdateNotification);
        intent.putExtra(kRecords, object);
        sendBroadcast(intent);
        //Update of product Count
        CurrentUser currentUser = ModelManager.modelManager().getCurrentUser();
        currentUser.setSellingProductCount(currentUser.getSellingProductCount()+1);
    }

    void showNotification() {
        mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mBuilder = new Notification.Builder(this);
        mBuilder.setContentTitle("PeepsIn")
                .setContentText("Product publishing...")
                .setSmallIcon(android.R.drawable.stat_sys_upload)
                .setOngoing(true)
                .setTicker("Product publishing...")
                .setProgress(100, 10, true);
        mNotifyManager.notify(notificationId, mBuilder.build());
    }
}
