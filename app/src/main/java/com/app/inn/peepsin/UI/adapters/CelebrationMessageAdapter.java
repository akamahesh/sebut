package com.app.inn.peepsin.UI.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.CelebrationMessage;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.fragments.Profile.CelebrationMessageDetailsFragment;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by dhruv on 29/9/17.
 */

public class CelebrationMessageAdapter extends RecyclerView.Adapter<CelebrationMessageAdapter.MessageViewHolder>{
    private Context context;
    private List<CelebrationMessage> messageList;
    private boolean editEnabled;
    private Switcher switcherListener;

    public CelebrationMessageAdapter(Context context, List<CelebrationMessage> messageList, boolean editEnable, Switcher profileSwitcher) {
        this.context = context;
        this.messageList = messageList;
        switcherListener = profileSwitcher;
        editEnabled = editEnable;
    }

    @Override
    public MessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_message_item_layout, parent, false);
        return new MessageViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(MessageViewHolder holder, int position) {
        CelebrationMessage message = messageList.get(position);
        holder.bindContent(message);
        String title = message.getMessageTitle();
        String msg = message.getMessage();
        String senderName = message.getSenderName();
        String date = Utils.getTimeStampDate(message.getSchedulingTimeStamp());

        holder.tvTitle.setText(title);
        holder.tvMessage.setText(msg);
        holder.tvSenderName.setText(senderName);
        holder.tvDate.setText(date);

        if (message.getFavorite()) {
            holder.ivLike.setImageResource(R.drawable.ic_thumb_up_red_24dp);
            holder.tvLike.setTextColor(holder.redColor);
        } else {
            holder.ivLike.setImageResource(R.drawable.ic_thumb_up_grey_500_24dp);
            holder.tvLike.setTextColor(holder.greyColor);
        }

        String imageUrl = message.getEventImageUrl();
        if(!imageUrl.isEmpty())
            Picasso.with(context).load(imageUrl).fit()
                    .placeholder(R.drawable.img_event_banner).into(holder.ivEvent);
        else
            holder.ivEvent.setImageResource(R.drawable.img_event_banner);
    }

    @Override
    public int getItemCount() {
        return messageList.size();
    }

    public void addItems(List<CelebrationMessage> messageList){
        this.messageList.clear();
        this.messageList.addAll(messageList);
        notifyDataSetChanged();
    }

    public void addItem(CelebrationMessage message){
        this.messageList.clear();
        this.messageList.add(message);
        notifyDataSetChanged();
    }

    class MessageViewHolder extends RecyclerView.ViewHolder{
        private CelebrationMessage item;
        @BindView(R.id.tv_name) TextView tvTitle;
        @BindView(R.id.tv_message) TextView tvMessage;
        @BindView(R.id.tv_sender_name) TextView tvSenderName;
        @BindView(R.id.tv_date) TextView tvDate;
        @BindView(R.id.iv_event) ImageView ivEvent;
        @BindView(R.id.iv_event_like) ImageView ivLike;
        @BindView(R.id.tv_like) TextView tvLike;
        @BindColor(R.color.theme_color) int redColor;
        @BindColor(R.color.text_color_regular) int greyColor;

        MessageViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        void bindContent(CelebrationMessage message) {
            this.item= message;
        }

        @OnClick(R.id.item_view)
        void messageDetails(){
            if(editEnabled && switcherListener !=null){
                switcherListener.switchFragment(CelebrationMessageDetailsFragment.newInstance(item,switcherListener),true,true);
            }
        }

        @OnClick(R.id.tv_say_thanks)
        void sayThanks(){
            Toaster.kalaToast("Thanks "+item.getSenderName());
        }

        @OnClick(R.id.like_view)
        void eventLike(){
            Boolean isFavorite = item.getFavorite();
            if (isFavorite)
                onLike(0,getAdapterPosition());
            else
                onLike(1,getAdapterPosition());
        }

        void onLike(int isFav,int position){
            ModelManager.modelManager().setFavouriteMessage(item.getMessageId(),isFav, (Constants.Status iStatus) -> {
                messageList.get(position).setFavorite(isFav == 1);
                notifyItemChanged(position);
            }, (Constants.Status iStatus, String message) -> Toaster.toast(message));
        }
    }
}
