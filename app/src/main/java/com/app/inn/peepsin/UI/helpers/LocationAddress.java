package com.app.inn.peepsin.UI.helpers;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import static com.app.inn.peepsin.Constants.Constants.kAddress;
import static com.app.inn.peepsin.Constants.Constants.kCity;
import static com.app.inn.peepsin.Constants.Constants.kCountry;
import static com.app.inn.peepsin.Constants.Constants.kState;
import static com.app.inn.peepsin.Constants.Constants.kZipcode;

/**
 * Created by akamahesh on 15/5/17.
 */

public class LocationAddress {
    private static final String TAG = LocationAddress.class.getSimpleName();

    public static void getAddressFromLocation(final double latitude, final double longitude, final Context context, final Handler handler){
        Thread thread = new Thread(){

            @Override
            public void run() {
                Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                String result = null;
                String city = null;
                String zipCode = null;
                String state = null;
                String country = null;
                try {
                    List<Address> addressList = geocoder.getFromLocation(latitude,longitude,1);
                    if(addressList!=null && addressList.size()>0){
                        Address address = addressList.get(0);
                        StringBuilder sb= new StringBuilder();
                        for(int i= 0; i<address.getMaxAddressLineIndex();i++){
                            sb.append(address.getAddressLine(i)).append("\n");
                        }
                        country = address.getCountryName();
                        state  = address.getAdminArea();  //state
                        city  = address.getLocality();  //city
                        zipCode = address.getPostalCode();

                        sb.append(address.getLocality()).append("\n");
                        sb.append(address.getPostalCode()).append("\n");
                        sb.append(address.getCountryName());
                        result = sb.toString();

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    Message message = Message.obtain();
                    message.setTarget(handler);
                    if (result != null) {
                        message.what = 1;
                        Bundle bundle = new Bundle();
                        bundle.putString(kAddress, result);
                        bundle.putString(kCity,    city);
                        bundle.putString(kZipcode, zipCode);
                        bundle.putString(kState,  state);
                        bundle.putString(kCountry, country);
                        message.setData(bundle);
                    } else {
                        message.what = 1;
                        Bundle bundle = new Bundle();
                        result = "Unable to get address for this lat-long.";
                        bundle.putString(kAddress, result);
                        message.setData(bundle);
                    }
                    message.sendToTarget();
                }
            }
        };
        thread.start();
    }
}
