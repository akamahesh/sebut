package com.app.inn.peepsin.UI.fragments.Connections;

import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.app.inn.peepsin.Constants.Blocks.Block;
import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.DataBase.DataBaseHandler;
import com.app.inn.peepsin.Libraries.DispatchQueue.DispatchQueue;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.PhoneContact;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.activities.HomeActivity;
import com.app.inn.peepsin.UI.helpers.DividerItemRecyclerDecoration;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.helpers.Validations;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.Manifest.permission.READ_CONTACTS;

/**
 * Created by akamahesh on 1/5/17.
 */

public class PhoneFragment extends Fragment {
    private final String TAG = getClass().getSimpleName();

    private String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
    private String PROFILE_IMAGE = ContactsContract.Contacts.PHOTO_URI;
    private String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;
    private List<PhoneContact> phoneContactList;
    private ContactAdapter contactAdapter;

    @BindView(R.id.empty_view)
    View emptyView;
    @BindView(R.id.tv_title)
    TextView tvToolbarTitle;
    @BindView(R.id.recycler_view_phone)
    RecyclerView recyclerView;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.edt_search)
    EditText edtSearch;
    @BindView(R.id.view_invite)
    View inviteView;
    @BindView(R.id.btn_contacts)
    Button btnImportContacts;
    @BindView(R.id.btn_select_all) TextView tvSelectAll;
    @BindView(R.id.tv_empty_title) TextView tvEmptyTitle;
    @BindView(R.id.tv_empty_message) TextView tvEmptyMessage;
    @BindView(R.id.select_view)
    View selectView;
    @BindView(R.id.ib_checkbox)
    ImageView ivCheckBox;
    private final int REQUEST_PERMISSION_READ_CONTACTS = 102;
  //  AppBarLayout appbarLayout;

    public static Fragment newInstance() {
        return new PhoneFragment();
    }

    private DataBaseHandler dbHandler;
    private CharSequence filterString;
    private String mUserId;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dbHandler = new DataBaseHandler(getContext());
        mUserId = String.valueOf(ModelManager.modelManager().getCurrentUser().getUserId());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_phone, container, false);
        ButterKnife.bind(this, view);
       /* appbarLayout=((HomeActivity) getActivity()).getTabLayout();
        Utils.isfriendClicked=1;*/
        tvToolbarTitle.setText(getString(R.string.title_phone_contacts));
        tvEmptyTitle.setText(R.string.title_message_phone_contacts);
        tvEmptyMessage.setVisibility(View.GONE);
        phoneContactList = new ArrayList<>();
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.addItemDecoration(new DividerItemRecyclerDecoration(getContext(), R.drawable.canvas_recycler_divider));
        contactAdapter = new ContactAdapter(getContext(), phoneContactList);
        recyclerView.setAdapter(contactAdapter);

        swipeRefreshLayout.setEnabled(false);
        swipeRefreshLayout.setOnRefreshListener(onRefreshListener);

        addTextListener();
        edtSearch.setOnEditorActionListener(actionListener);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (ContextCompat.checkSelfPermission(getActivity(), READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            btnImportContacts.setVisibility(View.GONE);
            List<PhoneContact> contactList = dbHandler.getPhoneContactFromDatabase(mUserId, "0");
            List<PhoneContact> sentcontactList = dbHandler.getPhoneContactFromDatabase(mUserId, "1");
            if (contactList.isEmpty() & sentcontactList.isEmpty()) {
                loadData();
            }
            phoneContactList.addAll(contactList);
            contactAdapter.notifyDataSetChanged();
            checkEmptyState();

        } else
            btnImportContacts.setVisibility(View.VISIBLE);
    }

    SwipeRefreshLayout.OnRefreshListener onRefreshListener = this::importContacts;

    TextView.OnEditorActionListener actionListener = (TextView v, int actionId, KeyEvent event) -> {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            Utils.hideKeyboard(getContext());
        }
        return true;
    };

    private void addTextListener() {
        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                s = s.toString().toLowerCase();
                filterString = s.toString().toLowerCase();
                contactAdapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    @OnClick(R.id.view_invite)
    void invite() {
        CopyOnWriteArrayList<PhoneContact> contactList = new CopyOnWriteArrayList<>();
        for (PhoneContact contact : phoneContactList) {
            if (contact.isSelected())
                contactList.add(contact);
        }
        if (contactList.isEmpty()) {
            Toaster.toast("Please select any contact to invite ");
            return;
        }
        inviteContact(contactList);
    }

    @OnClick(R.id.select_view)
    void selectAll(){
        if(tvSelectAll.getText().toString().equals("Select All")){
            for(int i=0;i<phoneContactList.size();i++){
                phoneContactList.get(i).setSelected(true);
            }
            tvSelectAll.setText(getString(R.string.unselect_all));
            ivCheckBox.setImageDrawable(getResources().getDrawable(R.drawable.ic_invite_check));
        }else {
            for(int i=0;i<phoneContactList.size();i++){
                phoneContactList.get(i).setSelected(false);
            }
            tvSelectAll.setText(getString(R.string.select_all));
            ivCheckBox.setImageDrawable(getResources().getDrawable(R.drawable.ic_circle_thin));
        }
        contactAdapter.notifyDataSetChanged();
    }

    @OnClick(R.id.btn_back)
    void onBack() {
        Utils.hideKeyboard(getContext());
        getFragmentManager().popBackStack();
        /*appbarLayout.setVisibility(View.VISIBLE);
        Utils.isfriendClicked=0;*/
    }
    @Override
    public void onResume() {
        super.onResume();
       // appbarLayout.setVisibility(View.GONE);

    }

    @OnClick(R.id.btn_contacts)
    void importContacts() {
        if (ContextCompat.checkSelfPermission(getActivity(), READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            btnImportContacts.setVisibility(View.GONE);
            loadData();
        } else if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), READ_CONTACTS)) {
            // We've been denied once before. Explain why we need the permission, then ask again.
            Utils.showDialog(getContext(),
                    getString(R.string.contact_permission_required_text),
                    getString(R.string.ask_permission_text),
                    getString(R.string.discard_text),
                    (dialog, which) -> {
                        if (which == -1)
                            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_PERMISSION_READ_CONTACTS);
                        else
                            dialog.dismiss();
                    });
        } else {
            // We've never asked. Just do it.
            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_PERMISSION_READ_CONTACTS);
        }
    }

    private void inviteContact(CopyOnWriteArrayList<PhoneContact> contactList) {
        showProgress(true);
        ModelManager.modelManager().invitePhoneContact(dbHandler, contactList, (Constants.Status iStatus) -> {
            phoneContactList.removeAll(contactList);
            contactAdapter.notifyDataSetChanged();
            edtSearch.setText("");
            contactAdapter.getFilter().filter("");
            checkEmptyState();
            showProgress(false);
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Toaster.toast(message);
            checkEmptyState();
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PERMISSION_READ_CONTACTS && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            btnImportContacts.setVisibility(View.GONE);
            loadData();
        } else {
            // We were not granted permission this time, so don't try to show the contact picker
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void loadData() {
        showProgress(true);
        swipeRefreshLayout.setRefreshing(false);
        getPhoneContacts((Constants.Status iStatus, GenricResponse<List<PhoneContact>> genricResponse) -> {
            showProgress(false);
            phoneContactList.addAll(genricResponse.getObject());
            contactAdapter.notifyDataSetChanged();
            checkEmptyState();
            swipeRefreshLayout.setRefreshing(false);
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            checkEmptyState();
            Toaster.toast(message);
            swipeRefreshLayout.setRefreshing(false);
        });
    }


    public void getPhoneContacts(Block.Success<List<PhoneContact>> success, Block.Failure failure) {
        ModelManager.modelManager().getDispatchQueue().async(() -> {
            try {
                Cursor cursor = getContext().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
                if (cursor != null) {
                    cursor.moveToFirst();
                }
                List<PhoneContact> contactStore = new ArrayList<>();
                while (!(cursor != null && cursor.isAfterLast())) {
                    String name = cursor != null ? cursor.getString(cursor.getColumnIndex(DISPLAY_NAME)) : "";
                    String phone = cursor != null ? cursor.getString(cursor.getColumnIndex(NUMBER)) : "";
                    phone = Utils.getProperPhoneNumber(phone);
                    String image = cursor != null ? cursor.getString(cursor.getColumnIndex(PROFILE_IMAGE)) : "";

                    PhoneContact phoneContact = new PhoneContact(name, phone, image);  // 1 for phone
                    if (Validations.isValidPhone(phone) && !dbHandler.isPhoneUserExist(mUserId, phoneContact.getContactId())) {
                        Log.i(getClass().getSimpleName(), phoneContact.toString());
                        contactStore.add(phoneContact);
                        dbHandler.addPhoneContacts(mUserId, phoneContact.getName(), phoneContact.getContactId(), phoneContact.getImageURL(), "0");
                    }
                    if (cursor != null) {
                        cursor.moveToNext();
                    }
                }
                GenricResponse<List<PhoneContact>> genricResponse = new GenricResponse<>(contactStore);
                DispatchQueue.main(() -> success.iSuccess(Constants.Status.success, genricResponse));
            } catch (Exception e) {
                DispatchQueue.main(() -> failure.iFailure(Constants.Status.fail, e.getMessage()));
            }
        });
    }


    private void checkEmptyState() {
        if (contactAdapter.getItemCount() > 0) {
            emptyView.setVisibility(View.GONE);
            selectView.setVisibility(View.VISIBLE);
            swipeRefreshLayout.setEnabled(true);
        } else {
            selectView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        }
    }

    private void showProgress(boolean b) {
        if (b) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

    class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ContactHolder> {
        private List<PhoneContact> contactList;
        private List<PhoneContact> filterList;
        private Context context;
        private ColorGenerator generator = ColorGenerator.MATERIAL;

        ContactAdapter(Context context, List<PhoneContact> contactList) {
            this.contactList = contactList;
            this.filterList = contactList;
            this.context = context;
        }

        @Override
        public ContactAdapter.ContactHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_phone_layout, parent, false);
            return new ContactAdapter.ContactHolder(layoutView);
        }

        @Override
        public void onBindViewHolder(ContactAdapter.ContactHolder holder, int position) {
            PhoneContact contact = contactList.get(position);
            holder.bindContent(contact);
            String name = contact.getName();
            String phone = contact.getPhone();
            String profileURL = contact.getImageURL();

            holder.tvName.setText(name);
            holder.tvTitle.setText(phone);

            holder.tvStatus.setText(getString(R.string.invite_via_phone_text));
            holder.ivStatusIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_phone));

            if (contact.isSelected()) {
                holder.ibCheckBox.setImageDrawable(getResources().getDrawable(R.drawable.ic_invite_check));
            } else {
                holder.ibCheckBox.setImageDrawable(getResources().getDrawable(R.drawable.ic_circle_thin));
            }

            if (profileURL.isEmpty()) {
                holder.ivCircularUserImage.setVisibility(View.INVISIBLE);
                holder.ivUserImage.setVisibility(View.VISIBLE);
            } else {
                holder.ivCircularUserImage.setVisibility(View.VISIBLE);
                holder.ivUserImage.setVisibility(View.INVISIBLE);
            }

            TextDrawable drawable = TextDrawable.builder().buildRound(String.valueOf(name.toUpperCase().charAt(0)), generator.getRandomColor());

            if (!profileURL.isEmpty()) {
                Picasso.with(context)
                        .load(profileURL)
                        .placeholder(getResources().getDrawable(R.drawable.img_profile_placeholder))
                        .into(holder.ivCircularUserImage);
            } else {
                holder.ivUserImage.setImageDrawable(drawable);
            }
        }

        @Override
        public int getItemCount() {
            return contactList.size();
        }

        private Filter getFilter() {
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence charSequence) {

                    String charString = charSequence.toString();
                    if (charString.isEmpty()) {
                        contactList = filterList;
                    } else {
                        ArrayList<PhoneContact> filteredList = new ArrayList<>();
                        for (PhoneContact contacts : contactList) {
                            if (contacts.getName().toLowerCase().contains(charString) || contacts.getContactId().toLowerCase().contains(charString)) {
                                filteredList.add(contacts);
                            }
                        }
                        contactList = filteredList;
                    }
                    FilterResults filterResults = new FilterResults();
                    filterResults.values = contactList;
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                    contactList = (List<PhoneContact>) filterResults.values;
                    notifyDataSetChanged();
                    checkEmptyState();
                }
            };
        }

        class ContactHolder extends RecyclerView.ViewHolder {
            private PhoneContact contacts;
            @BindView(R.id.tv_name)
            TextView tvName;
            @BindView(R.id.tv_title)
            TextView tvTitle;
            @BindView(R.id.tv_status)
            TextView tvStatus;
            @BindView(R.id.iv_status_icon)
            ImageView ivStatusIcon;
            @BindView(R.id.iv_user_image)
            ImageView ivUserImage;
            @BindView(R.id.iv_user_image_circular)
            ImageView ivCircularUserImage;
            @BindView(R.id.ib_checkbox)
            ImageButton ibCheckBox;


            ContactHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            @OnClick(R.id.item_view)
            void onRowSelect() {
                if (contacts.isSelected()) {
                    contacts.setSelected(false);
                    ibCheckBox.setImageDrawable(getResources().getDrawable(R.drawable.ic_circle_thin));
                } else {
                    contacts.setSelected(true);
                    ibCheckBox.setImageDrawable(getResources().getDrawable(R.drawable.ic_invite_check));
                }
            }

            @OnClick(R.id.ib_checkbox)
            void onCheckbox() {
                if (contacts.isSelected()) {
                    contacts.setSelected(false);
                    ibCheckBox.setImageDrawable(getResources().getDrawable(R.drawable.ic_circle_thin));
                } else {
                    contacts.setSelected(true);
                    ibCheckBox.setImageDrawable(getResources().getDrawable(R.drawable.ic_invite_check));
                }
            }

            void bindContent(PhoneContact contacts) {
                this.contacts = contacts;
            }

        }
    }


}
