package com.app.inn.peepsin.UI.activities;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Libraries.Firebase.app.Config;
import com.app.inn.peepsin.Managers.APIManager.ReachabilityManager;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Managers.SocialManager.FacebookManager;
import com.app.inn.peepsin.Managers.SocialManager.GoogleManager;
import com.app.inn.peepsin.Managers.SocialManager.TwitterManager;
import com.app.inn.peepsin.Models.SocialUser;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.activities.AppPermissionActivity.Type;
import com.app.inn.peepsin.UI.dialogFragments.PolicyDialogFragment;
import com.app.inn.peepsin.UI.helpers.FragmentUtil;
import com.app.inn.peepsin.UI.helpers.SVG;
import com.app.inn.peepsin.UI.helpers.TickProgress;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.helpers.Validations;
import com.facebook.login.LoginManager;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.jaredrummler.android.widget.AnimatedSvgView;
import com.transitionseverywhere.ArcMotion;
import com.transitionseverywhere.ChangeBounds;
import com.transitionseverywhere.TransitionManager;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kAuthToken;
import static com.app.inn.peepsin.Constants.Constants.kDateOfBirth;
import static com.app.inn.peepsin.Constants.Constants.kEmail;
import static com.app.inn.peepsin.Constants.Constants.kFirstName;
import static com.app.inn.peepsin.Constants.Constants.kIsAlreadyRegistered;
import static com.app.inn.peepsin.Constants.Constants.kIsNewUser;
import static com.app.inn.peepsin.Constants.Constants.kJabberId;
import static com.app.inn.peepsin.Constants.Constants.kLastName;
import static com.app.inn.peepsin.Constants.Constants.kPassword;
import static com.app.inn.peepsin.Constants.Constants.kRegType;
import static com.app.inn.peepsin.Constants.Constants.kSocialId;

public class FrontActivity extends BaseActivity {

    @BindView(R.id.iv_facebook)
    ImageView ivFacebook;
    @BindView(R.id.iv_google)
    ImageView ivGoogle;
    @BindView(R.id.iv_twitter)
    ImageView ivTwitter;
    @BindView(R.id.tv_terms)
    TextView textViewTerms;
    @BindView(R.id.login_button)
    LoginButton facebookLoginButton;
    @BindView(R.id.social_layout)
    View socialLayout;
    @BindView(R.id.info_layout)
    View infoLayout;
    @BindView(R.id.root_view_group)
    ViewGroup supeRootViewGroup;
    @BindView(R.id.tv_welcome)
    TextView tvWelcome;
    @BindView(R.id.edt_email)
    EditText etContactID;
    @BindView(R.id.btn_login)
    Button btnLogin;

    @BindView(R.id.animated_svg_view)
    AnimatedSvgView animatedSvgView;
    @BindView(R.id.progress_bar)
    TickProgress tickProgress;

    private GoogleManager googleManager;
    private FacebookManager facebookManager;
    private TwitterManager twitterManager;
    private ProgressDialog progressDialog;
    private BroadcastReceiver mRegistrationBroadcastReceiver;

    private final int kPathAnimationDelay = 800;
    String firebaseToken = "";

    @NonNull
    public static Intent getIntent(Context context) {
        return new Intent(context, FrontActivity.class);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
        setContentView(R.layout.activity_front);
        ButterKnife.bind(this);
        clickableTermsSpan(textViewTerms);
        progressDialog = Utils.generateProgressDialog(this, false);
        animatedSvgView.setVisibility(View.VISIBLE);
        animatedSvgView.setOnStateChangeListener(onStateChangeListener);
        infoLayout.setVisibility(View.INVISIBLE);
        socialLayout.setVisibility(View.INVISIBLE);

        googleManager = new GoogleManager(this, googleManagerListener);
        facebookLoginButton.setReadPermissions("email");
        facebookManager = new FacebookManager(this, facebookManagerListener);
        twitterManager = new TwitterManager(this, twitterManagerListener);

        LoginManager.getInstance().registerCallback(facebookManager.getCallbackManager(),
                facebookManager.getFacebookCallback());
        etContactID.setOnEditorActionListener(onEditorActionListener);
        handleFirebase();
        displayFirebaseRegID();
        animatedSvgView.postDelayed(() -> setSvg(SVG.values()[0]), 500);

    }

    AnimatedSvgView.OnStateChangeListener onStateChangeListener = state -> {
        if (state == AnimatedSvgView.STATE_TRACE_STARTED) {

        } else if (state == AnimatedSvgView.STATE_FINISHED) {
            startPathAnimation();
            final Handler handler = new Handler();
            handler.postDelayed(this::animateInfo, kPathAnimationDelay);
        }
    };

    private void animateInfo() {
        tvWelcome.setVisibility(View.VISIBLE);
        infoLayout.setVisibility(View.VISIBLE);
        socialLayout.setVisibility(View.VISIBLE);
    }

    GoogleManager.GoogleManagerInterface googleManagerListener = new GoogleManager.GoogleManagerInterface() {
        @Override
        public void success(SocialUser socialUser) {
            verifySocialUser(socialUser, Constants.UserRegType.google.getValue());
        }

        @Override
        public void failure() {
            Utils.showAlertDialog(FrontActivity.this, "Error",
                    "Error Completing Google authentication. Please try again, or if the issue persists, sign in using your email and password");
        }
    };

    FacebookManager.FacebookManagerInterface facebookManagerListener = new FacebookManager.FacebookManagerInterface() {
        @Override
        public void success(SocialUser socialUser) {
            verifySocialUser(socialUser, Constants.UserRegType.facebook.getValue());
        }

        @Override
        public void failure(String s) {
            Utils.showAlertDialog(FrontActivity.this, "Error",
                    "Error Completing Facebook authentication. Please try again, or if the issue persists, sign in using your email and password");
        }
    };

    TwitterManager.TwitterManagerInterface twitterManagerListener = new TwitterManager.TwitterManagerInterface() {
        @Override
        public void twitterSuccess(SocialUser socialUser) {
            verifySocialUser(socialUser, Constants.UserRegType.twitter.getValue());
        }

        @Override
        public void twitterFailure(String exception) {
            Utils.showAlertDialog(FrontActivity.this, "Error",
                    "Error Completing Twitter authentication. Please try again, or if the issue persists, sign in using your email and password");
        }
    };

    EditText.OnEditorActionListener onEditorActionListener = (v, actionId, event) -> {
        if (actionId == EditorInfo.IME_ACTION_GO) {
            Utils.hideKeyboard(this);
            login();
            return true;
        }
        return false;
    };

    private void verifySocialUser(SocialUser user, int regType) {
        String email = user.getEmail();
        if (email.isEmpty()) {
            Utils.showAlertDialog(FrontActivity.this, getString(R.string.error),
                    getString(R.string.error_email_absent));
            return;
        }
        String password = Utils.generatePassword(email);
        HashMap<String, Object> socialMap = new HashMap<>();
        socialMap.put(kFirstName, user.getFirstName());
        socialMap.put(kLastName, user.getLastName());
        socialMap.put(kEmail, user.getEmail());
        socialMap.put(kDateOfBirth, user.getDob());
        socialMap.put(kJabberId, Utils.getJabberId(this));
        socialMap.put(kPassword, password);
        socialMap.put(kSocialId, user.getId());
        socialMap.put(kRegType, regType);
        showProgress(true);
        ModelManager.modelManager().userSocialLogin(socialMap,
                (Constants.Status iStatus, GenricResponse<Map<String, Object>> genricResponse) -> {
                    Map<String, Object> map = genricResponse.getObject();
                    int isNewUser = (int) map.get(kIsNewUser);
                    String authToken = (String) map.get(kAuthToken);
                    if (isNewUser == 1) {
                        executeNewUser(authToken, password, user.getEmail());
                    } else {
                        startActivity(UpdationActivity.getIntent(this, authToken, Type.SIGNIN));
                    }
                }, (Constants.Status iStatus, String message) -> {
                    Toaster.toast(message);
                    showProgress(false);
                });

    }

    private void startPathAnimation() {
        TransitionManager.beginDelayedTransition(supeRootViewGroup,
                new ChangeBounds().setPathMotion(new ArcMotion()).setDuration(kPathAnimationDelay));

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) animatedSvgView
                .getLayoutParams();
        params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        params.addRule(RelativeLayout.ALIGN_PARENT_START);

        int size = Utils.dpToPx(this, 80);

        params.height = size;
        params.width = size;
        animatedSvgView.requestLayout();
        animatedSvgView.setLayoutParams(params);
    }

    private void executeNewUser(String authToken, String password, String email) {
        showProgress(false);
        Utils.showDialog(this,
                "You are now successfully registered with PeepsIn. Please Login with " + password
                        + " password.", "Reset Password", "Login", (dialog, which) -> {
                    if (which == -1) {

                        startActivity(LogInActivity.getIntent(FrontActivity.this, email, 3));
                    } else {
                        startActivity(UpdationActivity.getIntent(this, authToken, Type.SIGNUP));
                    }
                });
    }

    private void setSvg(SVG svg) {
        animatedSvgView.setGlyphStrings(svg.glyphs);
        animatedSvgView.setFillColors(svg.colors);
        animatedSvgView.setViewportSize(svg.width, svg.height);
        animatedSvgView.setTraceResidueColor(svg.colors[0]);
        animatedSvgView.setTraceColors(svg.colors);
        animatedSvgView.rebuildGlyphData();
        animatedSvgView.start();
    }


    @Override
    protected void onResume() {
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        // NotificationUtils.clearNotifications(getApplicationContext());
    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
        showProgress(false);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        twitterManager.getTwitterAuthClient().onActivityResult(requestCode, resultCode, data);
        facebookManager.getCallbackManager().onActivityResult(requestCode, resultCode, data);
        if (requestCode == GoogleManager.RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            googleManager.handleSignInResult(result);
        }
    }

    @OnClick(R.id.tv_register)
    void register() {
        startActivity(LogInActivity.getIntent(FrontActivity.this, "", 2));
    }

    @OnClick(R.id.btn_login)
    void login() {
        etContactID.setError(null);
        String contactId = Utils.getProperText(etContactID);
        if (contactId.isEmpty()) {
            Toaster.kalaToast("Email/phone should not be empty");
        } else if (Validations.isValidEmail(contactId) || Validations.isValidPhone(contactId)) {
            checkUserExistance(contactId);
        } else {
            etContactID.setError("Please Provide valid credentials");
        }
    }

    private void checkUserExistance(String contactId) {
        if (!ReachabilityManager.getNetworkStatus()) {
            Utils.showAlertDialog(this, "Login Failed!",
                    "Sorry, Failed to reach PeepsIn servers. Please check your network or try again later.");
            return;
        }
        tickProgress.showProgress();
        btnLogin.setEnabled(false);
        etContactID.setEnabled(false);
        ModelManager.modelManager().checkRegistration(contactId, (Constants.Status iStatus, GenricResponse<HashMap<String, Object>> genericResponse) -> {
            tickProgress.showSuccess();
            btnLogin.setEnabled(true);
            etContactID.setEnabled(true);
            HashMap<String, Object> hashMap = genericResponse.getObject();
            Integer isAlreadyRegistered = (Integer) hashMap.get(kIsAlreadyRegistered);
            boolean isAlreadyRegister = isAlreadyRegistered == 1;
            if (isAlreadyRegister) {
                startActivity(LogInActivity.getIntent(FrontActivity.this, contactId, 1));
            } else {
                startActivity(LogInActivity.getIntent(FrontActivity.this, contactId, 2));
            }
        }, (Constants.Status iStatus, String message) -> {
            Log.v(getClass().getSimpleName(), message);
            Toaster.toast(message);
            tickProgress.showError();
            btnLogin.setEnabled(true);
            etContactID.setEnabled(true);
        });
    }

    @OnClick(R.id.iv_facebook)
    void facebookLogin() {
        if (!ReachabilityManager.getNetworkStatus()) {
            Utils.showAlertDialog(this, "Login Failed!",
                    "Sorry, Login Failed to reach Facebook servers. Please check your network or try again later.");
            return;
        }
        LoginManager.getInstance()
                .logInWithReadPermissions(this, Collections.singletonList("public_profile"));

    }

    @OnClick(R.id.iv_google)
    void googleLogin() {
        if (!ReachabilityManager.getNetworkStatus()) {
            Utils.showAlertDialog(this, "Login Failed!",
                    "Sorry, Login Failed to reach Google servers. Please check your network or try again later.");
            return;
        }
        googleManager.signIn();
    }

    @OnClick(R.id.iv_twitter)
    void twitterLogin() {
        if (!ReachabilityManager.getNetworkStatus()) {
            Utils.showAlertDialog(this, "Login Failed!",
                    "Sorry, Login Failed to reach Twitter servers. Please check your network or try again later.");
            return;
        }
        twitterManager.authorize();
    }

    ClickableSpan termsSpanListener = new ClickableSpan() {
        @Override
        public void onClick(View widget) {
            FragmentUtil.showDialog(getSupportFragmentManager(), PolicyDialogFragment.newInstance(2), false);
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setUnderlineText(false);
            ds.setColor(getResources().getColor(R.color.link_color));
        }
    };


    ClickableSpan privacySpanListener = new ClickableSpan() {
        @Override
        public void onClick(View widget) {
            FragmentUtil.showDialog(getSupportFragmentManager(), PolicyDialogFragment.newInstance(1), false);
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setUnderlineText(false);
            ds.setColor(getResources().getColor(R.color.link_color));
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        showProgress(false);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
    }

    private void handleFirebase() {
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);
                    displayFirebaseRegID();
                }
            }
        };
    }


    private void displayFirebaseRegID() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        firebaseToken = pref.getString("regId", null);
        Log.v("Firebase ==> ", "Firebase reg id: " + firebaseToken);
        if (TextUtils.isEmpty(firebaseToken)) {
            Log.e("Firebase ==> ", "Firebase Reg Id is not received yet!");
        }
    }


    /**
     * Click span for Terms and Conditions
     * String str= "By continuing you agree to the Privacy Policy and Terms of Service"
     *
     * @param mTextViewTerms textview Where clickable span will be applied
     */
    private void clickableTermsSpan(TextView mTextViewTerms) {
        SpannableStringBuilder ssb = new SpannableStringBuilder();
        String termsText = getString(R.string.terms_and_policies);
        ssb.append(termsText);
        int pStart = termsText.indexOf('P');
        int pEnd = pStart + 14;
        int tStart = termsText.indexOf('T');
        int tEnd = tStart + 16;

        ssb.setSpan(privacySpanListener, pStart, pEnd, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        ssb.setSpan(termsSpanListener, tStart, tEnd, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mTextViewTerms.setText(ssb);
        mTextViewTerms.setMovementMethod(LinkMovementMethod.getInstance());
    }

    public void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        }
    }

}
