package com.app.inn.peepsin.UI.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.app.inn.peepsin.Models.DeliveryType;
import com.app.inn.peepsin.R;

import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by dhruv on 17/8/17.
 */

public class DeliverySpinnerAdapter extends ArrayAdapter<DeliveryType> {

    private Context context;
    private CopyOnWriteArrayList<DeliveryType> values;

    public DeliverySpinnerAdapter(Context context, int textViewResourceId, CopyOnWriteArrayList<DeliveryType> values) {
        super(context, textViewResourceId, values);
        this.context = context;
        this.values = values;
    }

    public int getCount(){
        return values.size();
    }

    public DeliveryType getItem(int position){
        return values.get(position);
    }

    public long getItemId(int position){
        return position;
    }

    // This is for the "passive" state of the spinner
    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        LayoutInflater mInflater = LayoutInflater.from(context);
        convertView = mInflater.inflate(R.layout.business_spinner_item, parent ,false);

        TextView label = (TextView) convertView.findViewById(R.id.spinner_text);
        //label.setText(values.get(position).getName());

        return convertView;
    }

    // And here is when the "chooser" is popped up
    // Normally is the same view, but you can customize it if you want
    @Override
    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
        LayoutInflater mInflater = LayoutInflater.from(context);
        convertView = mInflater.inflate(android.R.layout.simple_spinner_dropdown_item, parent ,false);

        TextView label = (TextView) convertView.findViewById(android.R.id.text1);
        label.setText(values.get(position).getName());

        return label;
    }

}
