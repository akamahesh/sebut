package com.app.inn.peepsin.UI.adapters;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.OrderHistory;
import com.app.inn.peepsin.Models.ProductList;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import static com.app.inn.peepsin.Constants.Constants.kOrderStatus;

/**
 * Created by dhruv on 16/8/17.
 */

public class OldReceivedOrderAdapter extends BaseExpandableListAdapter {
    private Context context;
    private CopyOnWriteArrayList<OrderHistory> orderItemList;
    private ProgressDialog progressDialog;

    public OldReceivedOrderAdapter(Context context, CopyOnWriteArrayList<OrderHistory> orderItemList) {
        progressDialog = Utils.generateProgressDialog(context, true);
        this.context = context;
        this.orderItemList = orderItemList;
    }

    // Parent View Group
    @Override
    public Object getGroup(int listPosition) {
        return this.orderItemList.get(listPosition);
    }

    @Override
    public int getGroupCount() {
        return this.orderItemList.size();
    }

    @Override
    public long getGroupId(int listPosition) {
        return listPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        OrderHistory item = (OrderHistory)getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.row_received_order_layout, null);
        }
        TextView tvOrderDate = (TextView) convertView.findViewById(R.id.tv_order_date);
        TextView tvOrderStatus = (TextView) convertView.findViewById(R.id.tv_order_status);
        TextView tvOrderItem = (TextView) convertView.findViewById(R.id.tv_order_item);
        TextView tvOrderIdNumber = (TextView) convertView.findViewById(R.id.tv_order_id_number);
        TextView tvOrderAddressUserName = (TextView) convertView.findViewById(R.id.tv_order_address_user_name);
        TextView tvOrderAddressUser = (TextView) convertView.findViewById(R.id.tv_order_address_user);
        TextView tvOrderAddressNumber = (TextView) convertView.findViewById(R.id.tv_order_address_number);
        TextView tvPaymentMethod = (TextView) convertView.findViewById(R.id.tv_payment_mehtod);
        TextView tvOrderTotalAmmount = (TextView) convertView.findViewById(R.id.tv_order_total_ammount);
        TextView tvPaymentBeforePromoAmmount = (TextView) convertView.findViewById(R.id.tv_payment_before_promo_ammount);
        TextView tvPromoCodeAmmount = (TextView) convertView.findViewById(R.id.tv_promo_code_ammount);
        TextView tvDiscountTagLine = (TextView) convertView.findViewById(R.id.tv_discount_tag_line);
        Button btnProduct = (Button) convertView.findViewById(R.id.btn_show_product);
        ImageView statusBtn = (ImageView) convertView.findViewById(R.id.status_btn);

        tvOrderStatus.setText(Utils.getStatus(item.getOrderStatus()));
        if (item.getOrderStatus() == 4) {
            tvOrderStatus.setTextColor(Color.parseColor("#FFFF0000"));
        }else {
            tvOrderStatus.setTextColor(Color.parseColor("#30C430"));
        }
        String orderdate = item.getOrderDate();
        String orderidnumber = String.valueOf(item.getOrderId());
        String orderaddressusername = item.getDeliveryAddress().getFullName();
        String orderaddressnumber = item.getDeliveryAddress().getPhone();
        if (item.getPaymentType() != null) {
            switch (item.getPaymentType()) {
                case 1:
                    tvPaymentMethod.setText(R.string.text_paytm);
                    break;
                case 2:
                    tvPaymentMethod.setText(R.string.text_pay_u_money);
                    break;
                case 3:
                    tvPaymentMethod.setText(R.string.text_cash_on_delivery);
                    break;
                default:
                    tvPaymentMethod.setText(R.string.pending_payment);
            }
        }

        String ordertotalammount = "";
        if (item.getFinalAmountAfterPromoCode() != null)
            ordertotalammount = item.getFinalAmountAfterPromoCode();
        String paymentbeforepromocode = "";
        if (item.getTotalCartPriceBeforePromoCode() != null)
            paymentbeforepromocode = item.getTotalCartPriceBeforePromoCode();
        String promocodeammount = "---";
        if (!item.getDiscountedPrice().equals(""))
            promocodeammount = item.getDiscountedPrice();
        tvOrderDate.setText(orderdate);
        tvOrderIdNumber.setText(orderidnumber);
        tvOrderAddressUserName.setText(orderaddressusername);
        String totalcartitem = "";
        if (item.getCartItemCount() != null)
            totalcartitem = String.valueOf(item.getCartItemCount()) + " " + "item";

        tvOrderItem.setText(totalcartitem);
        String discounrtag = item.getPromoCodeTagLine();
        tvDiscountTagLine.setText(discounrtag);
        String addressTxt = "No Location Available";
        if (item.getDeliveryAddress() != null)
            addressTxt = item.getDeliveryAddress().getStreetAddress() + ", "
                    +item.getDeliveryAddress().getCity() + ", "
                    + item.getDeliveryAddress().getState() + ", "
                    + item.getDeliveryAddress().getCountry();

        tvOrderAddressUser.setText(addressTxt);
        tvOrderAddressNumber.setText(orderaddressnumber);
        tvOrderTotalAmmount.setText(ordertotalammount);
        tvPaymentBeforePromoAmmount.setText(paymentbeforepromocode);
        tvPromoCodeAmmount.setText(promocodeammount);
        if(isExpanded)
            btnProduct.setText(context.getString(R.string.hide_products));
        else
            btnProduct.setText(context.getString(R.string.show_products));

        statusBtn.setOnClickListener(v -> {
            if(item.getOrderStatus()==7)
                Utils.showAlertDialog(context,"Product Delivered","Your Product has been delivered.");
            else if(item.getOrderStatus()==4)
                Utils.showAlertDialog(context,"Order Cancelled","Order is cancelled by the customer.");
            else
                openBottomSheetMenu(item.getOrderId(),item.getOrderStatus(),groupPosition);
        });

        return convertView;
    }

    private void openBottomSheetMenu(int orderId,int orderStatus,int position) {
        LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.bottom_sheet_received_order, null);

        final Dialog mBottomSheetDialog = new Dialog(context, R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.getWindow().setBackgroundDrawable(context.getResources().getDrawable(R.drawable.canvas_dialog_background_inset));
        mBottomSheetDialog.show();

        switch (orderStatus){
            case 1:
                view.findViewById(R.id.view_out_delivery).setVisibility(View.GONE);
                view.findViewById(R.id.view_delivered).setVisibility(View.GONE);
                view.findViewById(R.id.view_returned).setVisibility(View.GONE);
                break;
            case 2:
                view.findViewById(R.id.view_processing).setVisibility(View.GONE);
                view.findViewById(R.id.view_delivered).setVisibility(View.GONE);
                view.findViewById(R.id.view_returned).setVisibility(View.GONE);
                view.findViewById(R.id.view_declined).setVisibility(View.GONE);
                break;
            case 3:
                view.findViewById(R.id.view_processing).setVisibility(View.GONE);
                view.findViewById(R.id.view_out_delivery).setVisibility(View.GONE);
                view.findViewById(R.id.view_declined).setVisibility(View.GONE);
                break;
        }

        view.findViewById(R.id.view_processing).setOnClickListener(v -> {
            changeOrderStatus(orderId, Constants.OrderStatus.inProgress.getValue(), position);
            mBottomSheetDialog.dismiss();
        });
        view.findViewById(R.id.view_out_delivery).setOnClickListener(v -> {
            changeOrderStatus(orderId, Constants.OrderStatus.outDelivery.getValue(), position);
            mBottomSheetDialog.dismiss();
        });
        view.findViewById(R.id.view_delivered).setOnClickListener(v -> {
            changeOrderStatus(orderId, Constants.OrderStatus.delivered.getValue(), position);
            mBottomSheetDialog.dismiss();
        });
        view.findViewById(R.id.tv_cancel).setOnClickListener(v -> {
            changeOrderStatus(orderId, Constants.OrderStatus.cancelled.getValue(), position);
            mBottomSheetDialog.dismiss();
        });
        view.findViewById(R.id.view_declined).setOnClickListener(v -> {
            changeOrderStatus(orderId, Constants.OrderStatus.declined.getValue(), position);
            mBottomSheetDialog.dismiss();
        });
        view.findViewById(R.id.view_returned).setOnClickListener(v -> {
            changeOrderStatus(orderId, Constants.OrderStatus.returned.getValue(), position);
            mBottomSheetDialog.dismiss();
        });
    }

    private void changeOrderStatus(int orderId, int orderStatus,int position) {
        showProgress(true);
        ModelManager.modelManager().changeOrderStatus(orderId,orderStatus ,(Constants.Status iStatus, GenricResponse<HashMap<String,Object>> genericResponse) -> {
            HashMap<String,Object> map = genericResponse.getObject();
            Integer orderStat = (Integer) map.get(kOrderStatus);
            orderItemList.get(position).setOrderStatus(orderStat);
            notifyDataSetChanged();
            showProgress(false);
        }, (Constants.Status iStatus, String message) -> {
            Log.e("Order Status",message);
            showProgress(false);
        });
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    // Child View Group
    @Override
    public Object getChild(int listPosition, int expandedListPosition) {
        return this.orderItemList.get(listPosition).getProductRecords().get(expandedListPosition);
    }

    @Override
    public long getChildId(int listPosition, int expandedListPosition) {
        return expandedListPosition;
    }

    @Override
    public int getChildrenCount(int listPosition) {
        return this.orderItemList.get(listPosition).getProductRecords().size();
    }

    @Override
    public View getChildView(int listPosition, final int expandedListPosition, boolean isLastChild, View childView, ViewGroup parent) {

        final ProductList product = (ProductList) getChild(listPosition, expandedListPosition);

        if (childView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            childView = layoutInflater.inflate(R.layout.row_new_child_order_history, null);
        }
        TextView tvProductName = (TextView) childView.findViewById(R.id.tv_product_name);
        TextView tvProductRupees = (TextView) childView.findViewById(R.id.tv_product_rupees);
        TextView tvUserTitle = (TextView) childView.findViewById(R.id.tv_user_title);
        ImageView ivProductImage = (ImageView) childView.findViewById(R.id.iv_product_image);
        tvUserTitle.setVisibility(View.GONE);
        if(product!=null)
        {
            String producturl=product.getCoverImageThumbnailURL();
            String price = context.getString(R.string.Rs)+product.getPrice();
            String productname = product.getProductName();
            //String shopName="From:"+ orderItemList.get(listPosition).getShopRecords().get(0).getshopName();

            Picasso.with(context)
                    .load(producturl)
                    .placeholder(R.drawable.img_product_placeholder)
                    .into(ivProductImage);
            tvProductRupees.setText(price);
            tvProductName.setText(productname);
            //tvUserTitle.setText(shopName);
        }

        return childView;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;
    }

    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) progressDialog.cancel();
        }
    }

}
