package com.app.inn.peepsin.UI.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.app.inn.peepsin.Models.Search;
import com.app.inn.peepsin.R;

import java.util.List;

public class SearchAdapter extends BaseAdapter {

    private Context mContext;
    private List<Search> mSearch;
    private boolean mIsFilterList;

    public SearchAdapter(Context context,List<Search> searches, boolean isFilterList) {
        this.mContext = context;
        mSearch = searches;
        mIsFilterList = isFilterList;
    }

    public void updateList(List<Search> filterList, boolean isFilterList) {
        mSearch = filterList;
        mIsFilterList = isFilterList;
        notifyDataSetChanged ();
    }

    public void clearList() {
        mSearch.clear();
        notifyDataSetChanged ();
    }

    @Override
    public int getCount() {
        return mSearch.size();
    }

    @Override
    public String getItem(int position) {
        return mSearch.get(position).getSearchString();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public Search getItemPos(int position){
        return mSearch.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder holder;
        if(v==null){
            holder = new ViewHolder();
            LayoutInflater mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = mLayoutInflater.inflate(R.layout.list_item_search, parent, false);
            holder.txtSearch = (TextView)v.findViewById(R.id.tv_string);
            v.setTag(holder);
        } else{
            holder = (ViewHolder) v.getTag();
        }

        holder.txtSearch.setText(mSearch.get(position).getSearchString());

        Drawable searchDrawable,recentDrawable;

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            searchDrawable = mContext.getResources().getDrawable(R.drawable.ic_magnify_grey600_24dp, null);
            recentDrawable = mContext.getResources().getDrawable(R.drawable.ic_backup_restore_grey600_24dp, null);

        } else {
            searchDrawable = mContext.getResources().getDrawable(R.drawable.ic_magnify_grey600_24dp);
            recentDrawable = mContext.getResources().getDrawable(R.drawable.ic_backup_restore_grey600_24dp);
        }
        if(mIsFilterList) {
            holder.txtSearch.setCompoundDrawablesWithIntrinsicBounds(searchDrawable, null, null, null);
        }else {
            holder.txtSearch.setCompoundDrawablesWithIntrinsicBounds(recentDrawable, null, null, null);

        }
        return v;
    }
    class ViewHolder{
        TextView txtSearch;
    }
}







