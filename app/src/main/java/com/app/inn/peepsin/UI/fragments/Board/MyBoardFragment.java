package com.app.inn.peepsin.UI.fragments.Board;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.Board;
import com.app.inn.peepsin.Models.Comment;
import com.app.inn.peepsin.Models.CurrentUser;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.activities.MyAddBoardActivity;
import com.app.inn.peepsin.UI.activities.HomeActivity;
import com.app.inn.peepsin.UI.activities.ShoppingCartActivity;
import com.app.inn.peepsin.UI.fragments.Feeds.CustomBottomSheetDialogFragment;
import com.app.inn.peepsin.UI.fragments.Profile.ShoppingCartFragment;
import com.app.inn.peepsin.UI.helpers.ItemOffsetDecoration;
import com.app.inn.peepsin.UI.helpers.MyBounceInterpolator;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;
import static com.app.inn.peepsin.Constants.Constants.SHARE_PRODUCT;
import static com.app.inn.peepsin.Constants.Constants.kIsLike;
import static com.app.inn.peepsin.Constants.Constants.kLikeCount;
import static com.app.inn.peepsin.Constants.Constants.kReload;

/**
 * Created by akamahesh on 2/5/17.
 */

public class MyBoardFragment extends Fragment {

    private String TAG = getTag();

    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.recycler_view_posts)
    RecyclerView mRecyclerView;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.fab)
    FloatingActionButton floatingActionButton;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.empty_view)
    View emptyView;
    @BindView(R.id.ib_back)
    ImageButton ibBack;
    @BindView(R.id.tv_cartCount)
    TextView tvCartCount;
    @BindView(R.id.appBarLayout)
    AppBarLayout appBar;

    private CurrentUser currentUser;
    private List<Board> mBoardList;
    private MyBoardAdapter mMyBoardAdapter;
    private LinearLayoutManager mLayoutManager;

    int mPage = 1;
    boolean isLoading = true;
    int pageSize;

    static Switcher switcherListener;
    ProgressDialog progressDialog;
    // AppBarLayout appbarLayout;

    public static Fragment newInstance(Switcher switcher) { //flag 1= from timeline and flag 2 = from profile
        switcherListener = switcher;
        return new MyBoardFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = Utils.generateProgressDialog(getContext(), false);
        mBoardList = new ArrayList<>();
        currentUser = ModelManager.modelManager().getCurrentUser();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_timeline, container, false);
        ButterKnife.bind(this, view);
        appBar.setVisibility(View.VISIBLE);
        tvTitle.setText("My Activity");
        ibBack.setVisibility(View.VISIBLE);

        mMyBoardAdapter = new MyBoardAdapter(getContext(), mBoardList);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mMyBoardAdapter);
        mRecyclerView.addItemDecoration(new ItemOffsetDecoration(getContext(), R.dimen.item_offset));
        mRecyclerView.addOnScrollListener(onScrollListener);
        //checkEmptyScreen();
        swipeRefreshLayout.setOnRefreshListener(onRefreshListener);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadMyBoards();
        // setUpItemTouchHelper();
    }

    @Override
    public void onResume() {
        super.onResume();
        setCartUpdate(currentUser.getShoppingCartCount());
        // appbarLayout.setVisibility(View.GONE);

    }

    public void setCartUpdate(int count) {
        if (count == 0)
            tvCartCount.setVisibility(View.GONE);
        else {
            tvCartCount.setVisibility(View.VISIBLE);
            tvCartCount.setText(String.valueOf(count));
        }
    }

    SwipeRefreshLayout.OnRefreshListener onRefreshListener = this::loadMyBoards;

    public void initialState() {
        mPage = 1;
        isLoading = true;
        pageSize = mBoardList.size();
    }

    RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            if (dy > 0) //check for scroll down
            {
                int visibleItemCount = mLayoutManager.getChildCount();
                int totalItemCount = mLayoutManager.getItemCount();
                int firstVisibleItemPosition = mLayoutManager.findFirstVisibleItemPosition();

                if (isLoading) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= pageSize) {
                        isLoading = false;
                        ++mPage;
                        //Do pagination.. i.e. fetch new data
                        loadMorePeepsBoards();
                    }
                }
            }
        }
    };

    /*public void setUpItemTouchHelper(){
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int swipedPosition = viewHolder.getAdapterPosition();
                int boardId = mBoardList.get(swipedPosition).getBoardId();
                Utils.showConfirmDialog(getContext(),"Are you sure you want to remove it?", (dialog, which) -> {
                    //yes click listener
                   // removeBoard(swipedPosition,boardId);
                }, (dialog, which) -> {
                    //no click listerner
                    mMyBoardAdapter.notifyDataSetChanged();
                },"Yes","No");
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };
        ItemTouchHelper mItemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        mItemTouchHelper.attachToRecyclerView(mRecyclerView);
    }*/

    @OnClick(R.id.ib_back)
    void onBack() {
        getFragmentManager().popBackStack();
        /*appbarLayout.setVisibility(View.VISIBLE);
        Utils.isprofileClicked=0;*/
    }

    @OnClick(R.id.fab)
    void onAddPost() {
        startActivityForResult(MyAddBoardActivity.getIntent(getContext(), 1), 1);
    }

    @OnClick(R.id.btn_cart)
    void onCart() {
        if (switcherListener != null)
            switcherListener.switchFragment(ShoppingCartFragment.newInstance(switcherListener), true, true);
        //startActivity(ShoppingCartActivity.getIntent(getContext()));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                if (data.getBooleanExtra(kReload, false)) {
                    loadMyBoards();
                }
            }
        } else if (requestCode == 2) {
            if (resultCode == RESULT_OK) {
                if (data.getBooleanExtra(kReload, false)) {
                    loadMyBoards();
                }
            }
        } else
            loadMyBoards();
    }





    private void loadMyBoards() {
        swipeRefreshLayout.setRefreshing(true);
        ModelManager.modelManager().getMyBoards(1, (Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<Board>> genericResponse) -> {
            mMyBoardAdapter.addItems(genericResponse.getObject());
            initialState();
            checkEmptyScreen();
            swipeRefreshLayout.setRefreshing(false);
        }, (Constants.Status iStatus, String message) -> {
            Log.e(TAG, message);
            swipeRefreshLayout.setRefreshing(false);
        });
    }

    private void loadMorePeepsBoards() {
        swipeRefreshLayout.setRefreshing(true);
        ModelManager.modelManager().getMyBoards(mPage, (Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<Board>> genericResponse) -> {
            mMyBoardAdapter.addMoreItems(genericResponse.getObject());
            swipeRefreshLayout.setRefreshing(false);
            isLoading = !genericResponse.getObject().isEmpty();
        }, (Constants.Status iStatus, String message) -> {
            Log.e(TAG, message);
            swipeRefreshLayout.setRefreshing(false);
        });
    }

    // Notify the list when user comment on board
    private void loadMyBoardsNotfiyChange() {
        ModelManager.modelManager().getMyBoards(1, (Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<Board>> genericResponse) -> {
            mMyBoardAdapter.addItems(genericResponse.getObject());
            initialState();
        }, (Constants.Status iStatus, String message) -> {
            Log.e(TAG, message);
        });
    }

    public void shareCount(int boardId) {
        CustomBottomSheetDialogFragment bottomSheetDialogFragment = CustomBottomSheetDialogFragment.newInstance(3, boardId); //1 for map and 2 for share
        bottomSheetDialogFragment.setTargetFragment(this, SHARE_PRODUCT);
        bottomSheetDialogFragment.show(((HomeActivity) getContext()).getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
    }


    private void checkEmptyScreen() {
        if (mMyBoardAdapter.getItemCount() > 0) {
            emptyView.setVisibility(View.GONE);
        } else emptyView.setVisibility(View.VISIBLE);
    }

    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) progressDialog.dismiss();
        }
    }


    class MyBoardAdapter extends RecyclerView.Adapter<MyBoardAdapter.ViewHolder> {
        List<Board> boardList;
        Context context;

        MyBoardAdapter(Context context, List<Board> boardList) {
            this.boardList = boardList;
            this.context = context;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View layoutView = LayoutInflater.from(context).inflate(R.layout.row_post_layout, viewGroup, false);
            return new ViewHolder(layoutView);
        }

        private void addItems(List<Board> object) {
            boardList.clear();
            boardList = object;
            notifyDataSetChanged();
        }

        private void addMoreItems(List<Board> object) {
            boardList.addAll(object);
            notifyDataSetChanged();
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int i) {
            Board board = boardList.get(i);
            holder.bindContent(board);

            String userName = board.getFullName();
            String description = board.getDescription();
            String userProfileURL = board.getProfilePicURL();
            String boardTime = board.getBoardTime();

            int commentCount = board.getCommentCount();
            int shareCount = board.getShareCount();
            String imageUrl = ModelManager.modelManager().getCurrentUser().getProfilePicURL();
            String commentCountText = String.valueOf(commentCount) + " Comments";
            String shareCountText = String.valueOf(shareCount) + " Share";
            String commentCountViewReply = "View " + String.valueOf(commentCount - 1) + " more replies...";

            int likeCount = board.getLikeCount();
            String likeCountText = String.valueOf(likeCount);

            holder.tvTime.setText(boardTime);
            holder.tvUserName.setText(userName);
            holder.tvPost.setText(description);
            holder.tvCommentCounts.setText(commentCountText);
            holder.tvShareCount.setText(shareCountText);
            holder.tvLikeCounts.setText(likeCountText);
            if (commentCount == 1) {
                holder.tvViewReply.setVisibility(View.GONE);
            } else {
                holder.tvViewReply.setVisibility(View.VISIBLE);
                holder.tvViewReply.setText(commentCountViewReply);
            }


            if (board.getLike()) {
                holder.tvLikeCount.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_like_activity, 0, 0, 0);
            } else {
                holder.tvLikeCount.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_unlike_activity, 0, 0, 0);
            }

            if (!userProfileURL.isEmpty()) {
                holder.ivUserText.setVisibility(View.INVISIBLE);
                holder.ivUserImage.setVisibility(View.VISIBLE);
            } else {
                holder.ivUserText.setVisibility(View.VISIBLE);
                holder.ivUserImage.setVisibility(View.INVISIBLE);
            }
            TextDrawable drawable = TextDrawable.builder().buildRound(String.valueOf(userName.toUpperCase().charAt(0)), ColorGenerator.MATERIAL.getRandomColor());

            if (!userProfileURL.isEmpty()) {
                Picasso.with(context)
                        .load(userProfileURL)
                        .resize(100, 100)
                        .into(holder.ivUserImage);
            } else holder.ivUserText.setImageDrawable(drawable);


            if (!imageUrl.isEmpty())
                Picasso.with(getContext())
                        .load(imageUrl)
                        .resize(100, 100)
                        .into(holder.ivUserImageComment);
            else
                holder.ivUserImageComment.setImageDrawable(getResources().getDrawable(R.drawable.img_profile_placeholder));




            holder.edtComment.setImeOptions(EditorInfo.IME_ACTION_DONE);
            holder.edtComment.setRawInputType(InputType.TYPE_CLASS_TEXT);

            //Comment Element
            Comment commentUser = board.getLastComment();
            String commentTime;
            String commentUserImageURL;
            String commentUserName;
            String comment;
            if (commentUser != null && commentUser.getCommentId() != null) {
                holder.viewComment.setVisibility(View.VISIBLE);
                commentTime = commentUser.getTime();
                commentUserImageURL = commentUser.getProfilePicURL();
                commentUserName = commentUser.getUserName();
                comment = commentUser.getComment();
                holder.tvCommentUserName.setText(commentUserName);
                holder.tvCommentTime.setText(commentTime);
                holder.tvCommentDescription.setText(comment);

                if (!commentUserImageURL.isEmpty()) {
                    holder.ivCommentUserText.setVisibility(View.INVISIBLE);
                    holder.ivCommentUserImage.setVisibility(View.VISIBLE);
                } else {
                    holder.ivCommentUserText.setVisibility(View.VISIBLE);
                    holder.ivCommentUserImage.setVisibility(View.INVISIBLE);
                }
                TextDrawable draw = TextDrawable.builder().buildRound(String.valueOf(commentUserName.toUpperCase().charAt(0)), ColorGenerator.MATERIAL.getRandomColor());

                if (!commentUserImageURL.isEmpty()) {
                    Picasso.with(context)
                            .load(commentUserImageURL)
                            .resize(100, 100)
                            .into(holder.ivCommentUserImage);
                } else holder.ivCommentUserText.setImageDrawable(draw);
            } else holder.viewComment.setVisibility(View.GONE);


            holder.edtComment.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                        String comment = holder.edtComment.getText().toString().trim();
                        Utils.hideKeyboard(context);
                        if (comment.isEmpty()) {
                            Toaster.toastRangeen("comment can not be empty");

                        }
                        holder.addComment(comment, board.getBoardId());
                    }
                    return false;
                }
            });
        }

        @Override
        public int getItemCount() {
            return boardList.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            private Board board;
            @BindView(R.id.view_comment)
            View viewComment;
            @BindView(R.id.iv_user_image)
            ImageView ivUserImage;
            @BindView(R.id.iv_user_text)
            ImageView ivUserText;
            @BindView(R.id.tv_user_name)
            TextView tvUserName;
            @BindView(R.id.tv_post)
            TextView tvPost;
            @BindView(R.id.tv_time)
            TextView tvTime;
            @BindView(R.id.tv_like)
            TextView tvLikeCount;
            @BindView(R.id.tv_share)
            TextView tvShare;
            @BindView(R.id.tv_comment_count)
            TextView tvCommentCount;

            //Comment Element
            @BindView(R.id.iv_comment_user_image)
            ImageView ivCommentUserImage;
            @BindView(R.id.iv_comment_user_text)
            ImageView ivCommentUserText;
            @BindView(R.id.tv_comment_user_name)
            TextView tvCommentUserName;
            @BindView(R.id.tv_comment_time)
            TextView tvCommentTime;
            @BindView(R.id.tv_comment)
            TextView tvCommentDescription;
            @BindView(R.id.frame_image)
            View vDelete;
            @BindView(R.id.tv_likeCount)
            TextView tvLikeCounts;
            @BindView(R.id.tv_shareCount)
            TextView tvShareCount;
            @BindView(R.id.tv_commentCount)
            TextView tvCommentCounts;
            @BindView(R.id.tv_view_reply)
            TextView tvViewReply;
            @BindView(R.id.edt_comment)
            EditText edtComment;
            @BindView(R.id.iv_user_image_comment)
            ImageView ivUserImageComment;


            public ViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
                vDelete.setVisibility(View.VISIBLE);
            }

            void bindContent(Board board) {
                this.board = board;
            }

            @OnClick(R.id.frame_image)
            void onDeleteComment() {
                Utils.showConfirmDialog(getContext(), "Are you sure you want to remove it?", (dialog, which) -> {
                    //yes click listener
                    removeBoard(board, getAdapterPosition(), board.getBoardId());
                }, (dialog, which) -> {
                    //no click listerner
                    notifyDataSetChanged();
                }, "Yes", "No");

            }

            @OnClick(R.id.tv_comment_count)
            void onCommentCount() {
                startActivityForResult(MyAddBoardActivity.getIntent(context, 2, board.getBoardId()), 2);
            }



            @OnClick(R.id.tv_view_reply)
            void onLeaveCommentShow() {
                startActivityForResult(MyAddBoardActivity.getIntent(context, 2, board.getBoardId()), 2);

            }




            private void addComment(String comment, Integer mBoardId) {
                ModelManager.modelManager().saveComment(mBoardId, comment, (Constants.Status iStatus, GenricResponse<Comment> genericResponse) -> {
                    CopyOnWriteArrayList<Comment> mCommentList = new CopyOnWriteArrayList<>();
                    mCommentList.add(genericResponse.getObject());
                    loadMyBoardsNotfiyChange();
                    edtComment.setText("");
                }, (Constants.Status iStatus, String message) -> {
                    Toaster.toast(message);
                });
            }

            @OnClick(R.id.tv_like)
            void onLike(TextView textView) {
                final Animation favAnim = AnimationUtils.loadAnimation(context, R.anim.bounce);
                MyBounceInterpolator favInterpolar = new MyBounceInterpolator(.2, 20);
                favAnim.setInterpolator(favInterpolar);
                textView.startAnimation(favAnim);
                likeBoard(board.getBoardId(), board.getLike(), getAdapterPosition());
            }

            void likeBoard(int boardId, Boolean like, int adapterPosition) {
                ModelManager.modelManager().likeDislikeBoard(boardId, like, (Constants.Status iStatus, GenricResponse<HashMap<String, Object>> genricResponse) -> {
                    HashMap<String, Object> hashMap = genricResponse.getObject();
                    Integer isLike = (Integer) hashMap.get(kIsLike);
                    Integer likeCount = (Integer) hashMap.get(kLikeCount);
                    if (isLike == 1) {
                        board.setLike(true);
                    } else {
                        board.setLike(false);
                    }
                    board.setLikeCount(likeCount);
                    notifyItemChanged(adapterPosition);

                }, (Constants.Status iStatus, String message) -> Toaster.toast(message));
            }

            @OnClick(R.id.tv_share)
            void onShare() {
                shareCount(board.getBoardId());
            }

        }

        private void removeBoard(Board board, int swipedPosition, int boardId) {
            showProgress(true);
            ModelManager.modelManager().removeBoard(boardId, (Constants.Status iStatus) -> {
                showProgress(false);
                boardList.remove(swipedPosition);
                notifyDataSetChanged();
                checkEmptyScreen();
            }, (Constants.Status iStatus, String message) -> {
                Toaster.toast(message);
                showProgress(false);
            });
        }

    }


}
