package com.app.inn.peepsin.UI.fragments.Profile;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.app.Activity.RESULT_OK;
import static com.app.inn.peepsin.Constants.Constants.PHOTO_RESULT;
import static com.app.inn.peepsin.Constants.Constants.kAuthToken;
import static com.app.inn.peepsin.Constants.Constants.kData;
import static com.app.inn.peepsin.Constants.Constants.kDateOfBirth;
import static com.app.inn.peepsin.Constants.Constants.kEmail;
import static com.app.inn.peepsin.Constants.Constants.kFirstName;
import static com.app.inn.peepsin.Constants.Constants.kGender;
import static com.app.inn.peepsin.Constants.Constants.kIsAlreadyRegistered;
import static com.app.inn.peepsin.Constants.Constants.kLastName;
import static com.app.inn.peepsin.Constants.Constants.kMessageInternalInconsistency;
import static com.app.inn.peepsin.Constants.Constants.kOTP;
import static com.app.inn.peepsin.Constants.Constants.kPhone;
import static com.app.inn.peepsin.Constants.Constants.kProfilePic;
import static com.app.inn.peepsin.Libraries.ImageEditor.ImageEditor.PIC_CAMERA;
import static com.app.inn.peepsin.Libraries.ImageEditor.ImageEditor.PIC_CROP;
import static com.app.inn.peepsin.Libraries.ImageEditor.ImageEditor.PIC_GALLARY;
import static com.app.inn.peepsin.UI.helpers.Utils.generateProgressDialog;
import static com.app.inn.peepsin.UI.helpers.Utils.getProperText;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.zelory.compressor.Compressor;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Libraries.ImageEditor.ImageController;
import com.app.inn.peepsin.Libraries.ImageEditor.ImageEditor;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.CurrentUser;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.helpers.AkaImageUtil;
import com.app.inn.peepsin.UI.helpers.DatePicker;
import com.app.inn.peepsin.UI.helpers.ImageIntentController;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.helpers.Validations;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import com.squareup.picasso.Picasso;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by akamahesh on 2/5/17.
 */

public class EditProfileFragment extends Fragment {

  private String TAG = getTag();
  @BindView(R.id.tv_title)
  TextView tvTitle;
  @BindView(R.id.iv_profile)
  ImageView ivProfile;
  @BindView(R.id.edt_full_name)
  EditText edtFullName;
  /*   @BindView(R.id.edt_last_name)
     EditText editTextLastName;*/
  @BindView(R.id.edt_email)
  EditText edtEmail;
  @BindView(R.id.edt_password)
  EditText edtPwd;
  @BindView(R.id.edt_phone)
  EditText edtPhone;
  @BindView(R.id.edt_dob)
  EditText edtDob;
  @BindView(R.id.edt_location)
  EditText edtLoc;
  @BindView(R.id.gender_type)
  RadioGroup genderGroup;
  @BindView(R.id.radio_female)
  RadioButton radioFemale;
  @BindView(R.id.radio_male)
  RadioButton radioMale;

  private String mSelectedImagePathProfile;
  private ProgressDialog progressDialog;
  private CurrentUser currentUser;
  private int genderType = -1;

  private final int REQUEST_PERMISSION_CAMERA_STORAGE = 101;

  private static Switcher profileSwitcherListener;
  // AppBarLayout appbarLayout;
  String addresss = "";

  public static Fragment newInstance(Switcher switcher) {
    profileSwitcherListener = switcher;
    return new EditProfileFragment();
  }

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    progressDialog = generateProgressDialog(getContext(), false);
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_edit_profile, container, false);
    ButterKnife.bind(this, view);
    currentUser = ModelManager.modelManager().getCurrentUser();
    tvTitle.setText(getString(R.string.title_edit_myprofile));
    edtDob.setKeyListener(null);
    edtPwd.setKeyListener(null);
    edtLoc.setKeyListener(null);
    if (currentUser.getContactIdType() != -1) {
      if (currentUser.getContactIdType() == 1) {
        edtPhone.setKeyListener(null);
      } else {
        edtEmail.setKeyListener(null);
      }
    } else {
      edtEmail.setKeyListener(null);
      Toaster.kalaToast("ContactIdType Issue(Old User)");
    }
    return view;
  }


  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    loadProfile();
  }

  @Override
  public void onResume() {
    super.onResume();
    if (Utils.isAddressUpdated == 1) {
      CurrentUser user = ModelManager.modelManager().getCurrentUser();
      String loc = "Change Address";
      if (user.getPrimaryAddress() != null && user.getPrimaryAddress().getAddressId() != 0) {
        loc = user.getPrimaryAddress().getCity() + ", "
            + user.getPrimaryAddress().getState() + ", "
            + user.getPrimaryAddress().getCountry();
      }
      edtLoc.setText(loc);
    }

    edtPhone.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {}

      @Override
      public void afterTextChanged(Editable start) {
        if (start.toString().matches(Utils.PHONE_PATTERN)) {
          edtPhone.setText("+91 " + start.toString());
          Selection.setSelection(edtPhone.getText(), edtPhone.getText().length());
        }
      }
    });

  }

  private void loadProfile() {
    edtFullName.setText(currentUser.getFullName());
    edtEmail.setText(currentUser.getEmail().getContactId());
    edtDob.setText(currentUser.getDateOfBirth());
    edtPhone.setText(currentUser.getPhone().getContactId());

    String loc = "Change Address";
    if (currentUser.getPrimaryAddress() != null
        && currentUser.getPrimaryAddress().getAddressId() != 0) {
      loc = currentUser.getPrimaryAddress().getCity() + ", "
          + currentUser.getPrimaryAddress().getState() + ", "
          + currentUser.getPrimaryAddress().getCountry();
    }
    edtLoc.setText(loc);

    if (!currentUser.getProfilePicURL().isEmpty()) {
      Picasso.with(getContext())
          .load(currentUser.getProfilePicURL())
          .placeholder(getResources().getDrawable(R.drawable.img_profile_placeholder))
          .fit()
          .into(ivProfile);
    } else if (currentUser.getGender() == 0) {
      ivProfile.setImageResource(R.drawable.female_placeholder);
    } else if (currentUser.getGender() == 1) {
      ivProfile.setImageResource(R.drawable.male_placeholder);
    }

    if (currentUser.getGender() != -1) {
      if (currentUser.getGender() == 0) {
        genderType = 0;
        radioFemale.setChecked(true);
      } else {
        genderType = 1;
        radioMale.setChecked(true);
      }
    }

    //Gender Type RadioGroup
    genderGroup.setOnCheckedChangeListener((group, checkedId) -> {
      RadioButton rb = (RadioButton) group.findViewById(checkedId);
      if (rb.getText().toString().equals("Female")) {
        genderType = 0;
      } else {
        genderType = 1;
      }
    });
  }


  @OnClick(R.id.btn_back)
  public void onBack() {
    Utils.hideKeyboard(getContext());
    getFragmentManager().popBackStack();
  }

  @OnClick(R.id.dob_btn)
  public void getDob() {
    Utils.hideKeyboard(getContext());
    showDatePicker();
  }

  @OnClick(R.id.pwd_btn)
  public void changePassword() {
    if (profileSwitcherListener != null) {
      profileSwitcherListener
              .switchFragment(VerificationFragment.newInstance(3, profileSwitcherListener), true, true);
    }
  }

  @OnClick(R.id.address_btn)
  void changeAddress() {
    if (profileSwitcherListener != null) {
      profileSwitcherListener
              .switchFragment(MyAddressFragment.newInstance(profileSwitcherListener, 1), true, true);
    }
  }

  @OnClick(R.id.iv_profile)
  public void pickPhoto() {
    Utils.hideKeyboard(getContext());
    checkPermission();
  }


  private void showDatePicker() {
    DatePicker.getInstance(getContext(), 1,date -> edtDob.setText(date)).showDatePickDialog();
  }

  @OnClick(R.id.btn_save)
  public void saveProfile() {
    if (!validateData()) {
      return;
    }
    HashMap<String, Object> userMap = getParameters();
    if (currentUser.getContactIdType() != -1) { // 1=phone-login and 2=email-login
      if (currentUser.getContactIdType() == 1) {
        if (!getProperText(edtEmail).isEmpty() &&
                !getProperText(edtEmail).equals(currentUser.getEmail().getContactId())) {
          checkRegistration(getProperText(edtEmail), userMap, 2);
        } else {
          editUserProfile(userMap);
        }
      } else {
        if (!getProperText(edtPhone).isEmpty() &&
                !getProperText(edtPhone).equals(currentUser.getPhone().getContactId())) {
          checkRegistration(getProperText(edtPhone), userMap, 1);
        } else {
          editUserProfile(userMap);
        }
      }
    } else {
      Toaster.kalaToast(kMessageInternalInconsistency);
      //editUserProfile(userMap);
    }
  }

  private void checkRegistration(String contactId, HashMap<String, Object> userMap, int type) {
    showProgress(true);
    ModelManager.modelManager().checkRegistration(contactId,
        (Constants.Status iStatus, GenricResponse<HashMap<String, Object>> genericResponse) -> {
          HashMap<String, Object> hashMap = genericResponse.getObject();
          Integer isAlreadyRegistered = (Integer) hashMap.get(kIsAlreadyRegistered);
          if (isAlreadyRegistered == 1) {
            showProgress(false);
            if(type==1)
              Utils.showAlertDialog(getContext(), "Alert", "Phone No is used by another User");
            else
              Utils.showAlertDialog(getContext(), "Alert", "Email Id is used by another User");
          } else {
            verifyContact(contactId, userMap, type);
          }
        }, (Constants.Status iStatus, String message) -> {
          showProgress(false);
          Log.v(getClass().getSimpleName(), message);
        });
  }

  //1 for phone and 2 for email
  private void verifyContact(String contactId, HashMap<String, Object> userMap, int type) {
    ModelManager.modelManager().contactVerification(contactId, (Constants.Status iStatus, GenricResponse<HashMap<String, Object>> genericResponse) -> {
          showProgress(false);
          HashMap<String, Object> hashMap = genericResponse.getObject();
          String OTP = (String) hashMap.get(kOTP);
          if (profileSwitcherListener != null) {
            profileSwitcherListener.switchFragment(ProfileOTPFragment.newInstance(type, -1, profileSwitcherListener, OTP, contactId, addresss, null, userMap), true, true);
          }
        }, (Constants.Status iStatus, String message) -> {
          Log.e(TAG, message);
          Toaster.toast(message);
          showProgress(false);
        });
  }

  private void editUserProfile(HashMap<String, Object> userMap) {
    showProgress(true);
    ModelManager.modelManager().editMyProfile(userMap, (Constants.Status iStatus) -> {
      Log.v(TAG, "Profile Edited Successfully");
      showProgress(false);
      if(profileSwitcherListener!=null)
        profileSwitcherListener.switchFragment(ProfileFragment.newInstance(profileSwitcherListener), false, true);
      //updateUserModel(ModelManager.modelManager().getCurrentUser().getAuthToken());
    }, (Constants.Status iStatus, String message) -> {
      showProgress(false);
      Toaster.toast(message);
    });
  }

/*  private void updateUserModel(String authToken) {
    ModelManager.modelManager().getMyProfile(authToken, (Constants.Status iStatus) -> {
      showProgress(false);
      profileSwitcherListener
          .switchFragment(ProfileFragment.newInstance(profileSwitcherListener), false, true);
    }, (Constants.Status iStatus, String message) -> {
      showProgress(false);
      Toaster.toast(message);
    });
  }*/

  private void showProgress(boolean b) {
    if (b) {
      progressDialog.show();
    } else {
      if (progressDialog != null) {
        progressDialog.dismiss();
      }
    }
  }

  private void checkPermission() {
    if (ContextCompat.checkSelfPermission(getActivity(), CAMERA) == PackageManager.PERMISSION_GRANTED
        && ContextCompat.checkSelfPermission(getActivity(), READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
      openBottomSheetBanner();
    } else if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), CAMERA)
        && ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), READ_EXTERNAL_STORAGE)) {
      // We've been denied once before. Explain why we need the permission, then ask again.
      Utils.showDialog(getContext(),
          "Camera & Gallary permissions are required to upload profile images!", "Ask Permission",
          "Discard", (dialog, which) -> {
            if (which == -1) {
              requestPermissions(new String[]{CAMERA, READ_EXTERNAL_STORAGE},
                  REQUEST_PERMISSION_CAMERA_STORAGE);
            } else {
              dialog.dismiss();
            }
          });
    } else {
      // We've never asked. Just do it.
      requestPermissions(new String[]{CAMERA, READ_EXTERNAL_STORAGE},
          REQUEST_PERMISSION_CAMERA_STORAGE);
    }
  }


  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                         @NonNull int[] grantResults) {
    if (requestCode == REQUEST_PERMISSION_CAMERA_STORAGE
        && grantResults[0] == PackageManager.PERMISSION_GRANTED
        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
      openBottomSheetBanner();
    } else {
      // We were not granted permission this time, so don't try to show the contact picker
      super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    switch (requestCode) {
      case PIC_GALLARY:
        if (resultCode == RESULT_OK && data != null) {
          Uri selectedImage = data.getData();
          cropImageBottomSheet(selectedImage);
        }
        break;
      case PIC_CAMERA:
        if (resultCode == RESULT_OK && data != null) {
          Bitmap bitmap = (Bitmap) data.getExtras().get(kData);
          Uri uri = ImageController.getImageUri(getContext(),bitmap);
          //Uri uri = data.getData();
          cropImageBottomSheet(uri);
        }
        break;
      case PIC_CROP:
        if (resultCode == RESULT_OK && data != null) {
          Uri uri = data.getData();
          if (uri != null) {
            try {
              File file = new File(uri.getPath());
              mSelectedImagePathProfile = file.getAbsolutePath();
              Picasso.with(getContext())
                  .load(file)
                  .into(ivProfile);
            }catch (Exception e){
              e.printStackTrace();
            }
          }
        }
        break;

    }
  }

  public void cropImageBottomSheet(Uri uri) {
    View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_crop_image, null);
    final Dialog mBottomSheetDialog = new Dialog(getContext(), R.style.MaterialDialogSheet);
    mBottomSheetDialog.setContentView(view);
    mBottomSheetDialog.setCancelable(true);
    mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
    mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
    mBottomSheetDialog.show();

    view.findViewById(R.id.original_view).setOnClickListener(v -> {
      if (uri != null) {
        try {
          File file = new File(Utils.getRealPathFromURI(getContext(),uri));
          mSelectedImagePathProfile = file.getAbsolutePath();
          Picasso.with(getContext())
                  .load(file)
                  .into(ivProfile);
        }catch (Exception e){
          e.printStackTrace();
        }
      }
      mBottomSheetDialog.dismiss();
    });
    view.findViewById( R.id.crop_view).setOnClickListener(v -> {
      File outputFile = ImageEditor.getInstance().cropImage(this, uri, PIC_CROP);
      Log.e(TAG, outputFile.getAbsolutePath());
      mBottomSheetDialog.dismiss();
    });
  }

  public void openBottomSheetBanner() {
    View view = getActivity().getLayoutInflater().inflate(R.layout.custom_bottonsheet_layout, null);
    final Dialog mBottomSheetDialog = new Dialog(getContext(), R.style.MaterialDialogSheet);
    mBottomSheetDialog.setContentView(view);
    mBottomSheetDialog.setCancelable(true);
    mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
    mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
    mBottomSheetDialog.show();

    view.findViewById(R.id.camera_view).setOnClickListener(v -> {
      Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
      startActivityForResult(intent, PIC_CAMERA);
      mBottomSheetDialog.dismiss();
    });

    view.findViewById(R.id.gallery_view).setOnClickListener(v -> {
      Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
      startActivityForResult(gallery, PIC_GALLARY);
      mBottomSheetDialog.dismiss();
    });

    view.findViewById(R.id.cancel_view).setOnClickListener(v -> mBottomSheetDialog.dismiss());
  }

  public HashMap<String, Object> getParameters() {
    HashMap<String, Object> parameters = new HashMap<>();
    parameters.put(kAuthToken, ModelManager.modelManager().getCurrentUser().getAuthToken());
    String name = Utils.getProperText(edtFullName);
    String[] names = name.split(" ");
    String firstName = Utils.getFirstName(names);
    String lastName = Utils.getLastName(names);
    parameters.put(kFirstName,firstName);
    parameters.put(kLastName,lastName);
    if (currentUser.getContactIdType() != -1) {
      if (currentUser.getContactIdType() == 1 && !getProperText(edtEmail).isEmpty()) {
        parameters.put(kEmail, getProperText(edtEmail));
      } else if(currentUser.getContactIdType() == 2 && !getProperText(edtPhone).isEmpty()){
        parameters.put(kPhone, getProperText(edtPhone));
      }
    }
    parameters.put(kDateOfBirth, getProperText(edtDob));
    parameters.put(kGender, genderType);
    if (mSelectedImagePathProfile != null) {
      parameters.put(kProfilePic, new File(mSelectedImagePathProfile));
    }
    return parameters;
  }

  private boolean validateData() {
    boolean isOk = true;
    EditText etValid;
    String testString;
    View requestFocus = null;

    etValid = edtFullName;
    testString = etValid.getText().toString().toLowerCase().trim();
    if (!Validations.isValidFirstName(testString)) {
      etValid.setError(getString(R.string.error_invalid_first_name));
      requestFocus = etValid;
      isOk = false;
    }
    etValid = edtPhone;
    testString = etValid.getText().toString().toLowerCase().trim();
    if (!testString.isEmpty()) {
      if (!Validations.isValidPhone(testString)) {
        etValid.setError(getString(R.string.error_invalid_phone));
        requestFocus = etValid;
        isOk = false;
      }
    }
    etValid = edtEmail;
    testString = etValid.getText().toString().toLowerCase().trim();
    if (!testString.isEmpty()) {
      if (!Validations.isValidEmail(testString)) {
        etValid.setError(getString(R.string.error_invalid_email));
        requestFocus = etValid;
        isOk = false;
      }
    }

    if (genderType == -1) {
      Toaster.customToast("Please select any gender");
      isOk = false;
    }

    if (requestFocus != null) {
      requestFocus.requestFocus();
    }
    return isOk;
  }



  @Override
  public void onDetach() {
    super.onDetach();
    profileSwitcherListener = null;
  }

}
