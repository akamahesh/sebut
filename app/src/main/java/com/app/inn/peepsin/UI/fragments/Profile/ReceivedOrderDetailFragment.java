package com.app.inn.peepsin.UI.fragments.Profile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.inn.peepsin.Models.ProductList;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.helpers.DividerItemRecyclerDecoration;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kProductList;

/**
 * Created by dhruv on 16/8/17.
 */

public class ReceivedOrderDetailFragment extends Fragment {

    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.rv_order_history)
    RecyclerView mRecyclerView;

    private List<ProductList> productList;

    public static Fragment newInstance(List<ProductList> productList) {
        Fragment fragment = new ReceivedOrderDetailFragment();
        Bundle bdl = new Bundle();
        bdl.putSerializable(kProductList, (Serializable) productList);
        fragment.setArguments(bdl);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            productList = ((List<ProductList>) bundle.getSerializable(kProductList));
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_order_history_detail, container, false);
        ButterKnife.bind(this, view);
        tvTitle.setText(R.string.my_order_history);

        ProductAdapter productAdapter = new ProductAdapter(productList);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(productAdapter);
        RecyclerView.ItemDecoration itemDecoration = new DividerItemRecyclerDecoration(getContext(), R.drawable.canvas_recycler_divider);
        mRecyclerView.addItemDecoration(itemDecoration);

        return view;
    }

    @OnClick(R.id.btn_back)
    void onBack() {
        getFragmentManager().popBackStack();
    }

    public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {

        private List<ProductList> orderList;

        private ProductAdapter(List<ProductList> ProductListList) {
            this.orderList = ProductListList;
        }

        @Override
        public ProductAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_new_child_order_history, parent, false);
            return new ProductAdapter.ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ProductAdapter.ViewHolder holder, int position) {
            ProductList product = orderList.get(position);
            holder.bindContent(product);

            String producturl = product.getCoverImageThumbnailURL();
            String price = getString(R.string.Rs) + product.getPrice();
            String productname = product.getProductName();
            //String shopName="From:"+shopRecord.getshopName();

            Picasso.with(getContext())
                    .load(producturl)
                    .placeholder(R.drawable.img_product_placeholder)
                    .into(holder.ivProductImage);
            holder.tvProductRupees.setText(price);
            holder.tvProductName.setText(productname);
            holder.tvProductQuantity.setText("Quantity: "+product.getProductQuantity());
            //holder.tvUserTitle.setVisibility(View.INVISIBLE);
            //holder.tvUserTitle.setText(shopName);
        }

        @Override
        public int getItemCount() {
            return orderList.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            ProductList product;
            @BindView(R.id.tv_product_name)
            TextView tvProductName;
            @BindView(R.id.tv_product_rupees)
            TextView tvProductRupees;
            @BindView(R.id.iv_product_image)
            ImageView ivProductImage;
            @BindView(R.id.tv_product_quantity)
            TextView tvProductQuantity;
            public ViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            public void bindContent(ProductList item) {
                this.product = item;
            }
        }
    }
}
