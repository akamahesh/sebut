package com.app.inn.peepsin.UI.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

/**
 * Created by mahesh on 2/3/17.
 */

//SectionPagerAdapter a common PagerAdapter for all ViewPagers
public class SectionPagerAdapter extends FragmentPagerAdapter {

    private List<Fragment> fragmentList;

    /**
     * @param fm            supportFragmentManager
     * @param fragmentList  list of Fragments to be displayed
     */
    public SectionPagerAdapter(FragmentManager fm, List<Fragment> fragmentList) {
        super(fm);
        this.fragmentList=fragmentList;
    }

    @Override
    public Fragment getItem(int position) {
        if(fragmentList==null){
            return null;
        }
        return fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }


}
