package com.app.inn.peepsin.UI.fragments;

import static com.app.inn.peepsin.Constants.Constants.kAddressId;
import static com.app.inn.peepsin.Constants.Constants.kContactId;
import static com.app.inn.peepsin.Constants.Constants.kData;
import static com.app.inn.peepsin.Constants.Constants.kOTP;
import static com.app.inn.peepsin.Constants.Constants.kType;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.activities.LogInActivity;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.chaos.view.PinView;
import java.util.HashMap;

/**
 * Created by akaMahesh on 2/5/17
 * contact : mckay1718@gmail.com
 */

public class AddressOTPFragment extends Fragment {

  @BindView(R.id.tv_title)
  TextView tvTitle;
  @BindView(R.id.tv_otp)
  TextView tvOTP;
  @BindView(R.id.btn_verify)
  Button btnVerify;
  @BindView(R.id.otp_view)
  PinView otpView;

  private String PIN;
  private int mType;
  private String mContactID;
  private ProgressDialog progressDialog;
  private HashMap<String, Object> addressMap;


  public static Fragment newInstance(String pin, String contactId,int type,
      HashMap<String, Object> userMap) { //1 for phone 2 for email
    Fragment fragment = new AddressOTPFragment();
    Bundle bundle = new Bundle();
    bundle.putString(kOTP, pin);
    bundle.putInt(kType,type);
    bundle.putString(kContactId, contactId);
    bundle.putSerializable(kData, userMap);
    fragment.setArguments(bundle);
    return fragment;
  }


  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    Bundle bundle = getArguments();
    if (bundle != null) {
      PIN = bundle.getString(kOTP);
      mType = bundle.getInt(kType);
      mContactID = bundle.getString(kContactId);
      addressMap = (HashMap<String, Object>) bundle.getSerializable(kData);
    }
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_address_otp, container, false);
    ButterKnife.bind(this, view);
    progressDialog = Utils.generateProgressDialog(getContext(), false);
    btnVerify.setText("Verify Contact");
    tvTitle.setText("Contact Verification");
    Toaster.toastRangeen(
        "OTP sent to " + mContactID + "\n" + "Please verify your contactId ");
    Toaster.toastOTP(PIN);
    return view;
  }

  @OnClick(R.id.btn_verify)
  void verify() {
    String otpString = otpView.getText().toString();
    if (otpString.equals(PIN)) {
      saveAddress(addressMap,mType);
      Utils.hideKeyboard(getContext());
    } else {
      otpView.setText("");
      Utils.showKeyboard(getContext(), otpView);
      Toaster.toast("Worng OTP");
    }
  }


  //type 1 = for new and 2 for edit
  private void saveAddress(HashMap<String, Object> addressMap,int type) {
    if(type==1){
      ModelManager.modelManager().addNewAddress(addressMap, (Constants.Status iStatus) -> {
        showProgress(false);
        getFragmentManager().popBackStack();
        getFragmentManager().popBackStack();
      }, (Constants.Status iStatus, String message) -> {
        showProgress(false);
        Toaster.toast(message);
      });
    }else if(type==2){
      //addressMap.put(kAddressId, address.getAddressId());
      ModelManager.modelManager().editAddress(addressMap, (Constants.Status iStatus) -> {
        getFragmentManager().popBackStack();
        getFragmentManager().popBackStack();
      }, (Constants.Status iStatus, String message) -> {
        showProgress(false);
        Toaster.toast(message);
      });
    }


  }


  @OnClick(R.id.tv_resend)
  void resend() {
    resetOTP(mContactID);
  }

  @OnClick(R.id.btn_back)
  void onBack() {
    getFragmentManager().popBackStack();
  }

  private void resetOTP(String contactID) {
    showProgress(true);
    ModelManager.modelManager().loginByOtp(contactID,
        (Constants.Status iStatus, GenricResponse<HashMap<String, Object>> genricResponse) -> {
          HashMap<String, Object> map = genricResponse.getObject();
          PIN = (String) map.get(kOTP);
          showProgress(false);
          Toaster.toastRangeen(
              "OTP sent to " + mContactID + "\n" + "Please verify your contactId " );
        }, (Constants.Status iStatus, String message) -> {
          Utils.showAlertDialog(getContext(), "Otp Failed!", message);
          showProgress(false);
        });

  }

  private void showProgress(boolean b) {
    if (b) {
      progressDialog.show();
    } else {
      if (progressDialog != null) {
        progressDialog.dismiss();
      }
    }
  }



}
