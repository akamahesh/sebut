package com.app.inn.peepsin.UI.fragments.Profile;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.app.inn.peepsin.Constants.Blocks.Block;
import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Libraries.DispatchQueue.DispatchQueue;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.Address;
import com.app.inn.peepsin.Models.CartShop;
import com.app.inn.peepsin.Models.CurrentUser;
import com.app.inn.peepsin.Models.ShoppingCart;
import com.app.inn.peepsin.Models.ShoppingProduct;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.activities.HomeActivity;
import com.app.inn.peepsin.UI.activities.TransactionDetailActivity;
import com.app.inn.peepsin.UI.adapters.CartProductAdapter;
import com.app.inn.peepsin.UI.adapters.CartProductsAdapter;
import com.app.inn.peepsin.UI.dialogFragments.AddressDialogFragment;
import com.app.inn.peepsin.UI.helpers.DividerItemRecyclerDecoration;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.ADDRESS_RESULT;
import static com.app.inn.peepsin.Constants.Constants.KSCREENHEIGHT;
import static com.app.inn.peepsin.Constants.Constants.KSCREENWIDTH;
import static com.app.inn.peepsin.Constants.Constants.kData;

/**
 * Created by dhruv on 21/8/17.
 */

public class ShoppingCartFragment extends Fragment {

    private String TAG = getClass().getName();
    @BindView(R.id.recycler_cart)
    RecyclerView recyclerCart;
    @BindView(R.id.empty_view)
    View emptyView;
    @BindView(R.id.bottom_layout)
    View paymentView;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    /*  @BindView(R.id.tv_cart_price)
      TextView tvCartPrice;*/
   /* @BindView(R.id.tv_total_product)
    TextView tvTotalProduct;*/
    @BindView(R.id.btn_continue)
    Button btnContinue;
    @BindView(R.id.btn_start_shop)
    Button btnStartShopping;

    @BindView(R.id.bottom_cart_details)
    View bottomSheetView;
    @BindView(R.id.iv_arrow)
    ImageView ivArrow;
    @BindView(R.id.recycler_view_products)
    RecyclerView rvProducts;

    BottomSheetBehavior bottomSheetBehavior;
    private static Switcher switcherListener;
    private ProgressDialog progressDialog;
    private TabLayout tabLayout;

    private CartAdapter cartAdapter;
    private CartProductsAdapter productsAdapter;

    private ShoppingCart cart;

    public static ShoppingCartFragment newInstance(Switcher switcher) {
        ShoppingCartFragment fragment = new ShoppingCartFragment();
        switcherListener = switcher;
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = Utils.generateProgressDialog(getContext(), true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_shopping_cart, container, false);
        ButterKnife.bind(this, view);
        tabLayout = ((HomeActivity) getActivity()).getTabLayout();
        tabLayout.setVisibility(View.GONE);
        tvTitle.setText(getString(R.string.title_cart));

        cartAdapter = new CartAdapter(new ArrayList<>());
        recyclerCart.setHasFixedSize(true);
        recyclerCart.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerCart.setAdapter(cartAdapter);
        productsAdapter = new CartProductsAdapter(switcherListener, getContext(), new ArrayList<>());
        rvProducts.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvProducts.setHasFixedSize(true);
        rvProducts.addItemDecoration(new DividerItemRecyclerDecoration(getContext(), R.drawable.canvas_recycler_divider));
        rvProducts.setAdapter(productsAdapter);

        // init the bottom sheet behavior
        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheetView);
        bottomSheetBehavior.setBottomSheetCallback(bottomSheetCallback);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadShoppingCart();
    }


    BottomSheetBehavior.BottomSheetCallback bottomSheetCallback = new BottomSheetBehavior.BottomSheetCallback() {
        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_EXPANDED) {
                ivArrow.setImageDrawable(getResources().getDrawable(R.drawable.ic_keyboard_arrow_down_grey_500_24dp));
            } else {
                ivArrow.setImageDrawable(getResources().getDrawable(R.drawable.ic_keyboard_arrow_up_grey_500_24dp));
            }
        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {
        }
    };

    @OnClick(R.id.ib_back)
    public void onBack() {
        getFragmentManager().popBackStack();
        tabLayout.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.btn_continue)
    public void btnClick() {
        showAddressDialog();
    }


    private void setUpView(String cartPrice, int totalCount) {
        String price = "Total " + getString(R.string.Rs) + " " + cartPrice + " ->";
        String count = getString(R.string.cart_total_product) + "\t" + totalCount;
        btnContinue.setText(price);
        //tvTotalProduct.setText(count);

        CurrentUser currentUser = ModelManager.modelManager().getCurrentUser();
        if (currentUser != null) {
            String userName = currentUser.getFirstName();
            String countitem = String.valueOf(currentUser.getShoppingCartCount());
            tvTitle.setText(userName + "'s Cart" + "(" + countitem + ")");
        } else {
            tvTitle.setText("Cart");
        }
    }

    public void loadShoppingCart() {
        showProgress(true);
        ModelManager.modelManager().getShoppingCart((Constants.Status iStatus, GenricResponse<ShoppingCart> genericResponse) -> {
            cart = genericResponse.getObject();
            cartAdapter.addItems(cart.getShopList());
            setUpView(cart.getTotalCartPrice(), cart.getCartItemCount());
            setUpList(cart.getShopList());
            showProgress(false);
            checkEmptyScreen();
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Log.e(TAG, message);
        });
    }

    public void cartUpdate() {
        showProgress(true);
        ModelManager.modelManager().getShoppingCart((Constants.Status iStatus, GenricResponse<ShoppingCart> genericResponse) -> {
            cart = genericResponse.getObject();
            setUpView(cart.getTotalCartPrice(), cart.getCartItemCount());
            setUpList(cart.getShopList());
            showProgress(false);
            checkEmptyScreen();
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Utils.showAlertDialog(getContext(), "Alert", message);
            Log.e(TAG, message);
        });
    }

    public void refreshNewData(GenricResponse<ShoppingCart> genericResponse) {
        showProgress(true);
        cart = genericResponse.getObject();
        cartAdapter.addItems(cart.getShopList());
        setUpView(cart.getTotalCartPrice(), cart.getCartItemCount());
        setUpList(cart.getShopList());
        showProgress(false);
        checkEmptyScreen();
    }

    public void setUpList(List<CartShop> list) {
        getProductList(list, (Constants.Status iStatus, GenricResponse<List<ShoppingProduct>> response) -> {
            productsAdapter.addItems(response.getObject());
        }, (Constants.Status iStatus, String message) -> Log.e(TAG, message));
    }

    public void getProductList(List<CartShop> list, Block.Success<List<ShoppingProduct>> success, Block.Failure failure) {
        ModelManager.modelManager().getDispatchQueue().async(() -> {
            try {
                List<ShoppingProduct> productList = new ArrayList<>();
                for (int i = 0; i < list.size(); i++) {
                    productList.addAll(list.get(i).getProductList());
                }
                GenricResponse<List<ShoppingProduct>> genricResponse = new GenricResponse<>(productList);
                DispatchQueue.main(() -> success.iSuccess(Constants.Status.success, genricResponse));
            } catch (Exception e) {
                DispatchQueue.main(() -> failure.iFailure(Constants.Status.fail, e.getMessage()));
            }
        });
    }



    private void checkEmptyScreen() {
        if (cartAdapter.getItemCount() > 0) {
            emptyView.setVisibility(View.GONE);
            paymentView.setVisibility(View.VISIBLE);
            bottomSheetView.setVisibility(View.VISIBLE);
        } else {
            emptyView.setVisibility(View.VISIBLE);
            paymentView.setVisibility(View.GONE);
            bottomSheetView.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.btn_start_shop)
    void onStartShop() {

        try {
            getFragmentManager().popBackStack();
            tabLayout.setVisibility(View.VISIBLE);
            ((HomeActivity) getActivity()).getViewPager().setCurrentItem(0);
        } catch (Exception e) {
            Toaster.toast(e.getMessage());
            e.printStackTrace();
            getFragmentManager().popBackStack();
        }
    }

    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) progressDialog.cancel();
        }
    }

    void showAddressDialog() {
        Address primaryAddress = ModelManager.modelManager().getCurrentUser().getPrimaryAddress();
        String streetAddress = "No Primary Address";
        if (primaryAddress != null && primaryAddress.getAddressId()!=0) {
            streetAddress = primaryAddress.getStreetAddress() + ", "
                    + primaryAddress.getCity() + ", "
                    + primaryAddress.getState() + ", "
                    + primaryAddress.getCountry();
        }

        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_change_address_prompt);
        TextView tvAddress = ButterKnife.findById(dialog, R.id.tv_address);
        tvAddress.setText(streetAddress);
        Button btnChange = ButterKnife.findById(dialog, R.id.btn_change);
        Button btnContinue = ButterKnife.findById(dialog, R.id.btn_continue);
        if(primaryAddress!=null && primaryAddress.getAddressId()!=0){
            btnChange.setVisibility(View.VISIBLE);
        }else{
            btnChange.setVisibility(View.GONE);
            btnContinue.setText("Add Delivery Address");
        }

        btnChange.setOnClickListener(v -> {
            dialog.dismiss();
            getAddress();
        });
        btnContinue.setOnClickListener(v -> {
            dialog.dismiss();
            if(primaryAddress!=null && primaryAddress.getAddressId()!=0)
                startTransaction(primaryAddress);
            else
                getAddress();

        });
        dialog.show();
    }

    void getAddress() {
        DialogFragment fragment = AddressDialogFragment.newInstance(switcherListener);
        Bundle bdl = new Bundle();
        bdl.putInt(KSCREENWIDTH, 0);
        bdl.putInt(KSCREENHEIGHT, 0);
        fragment.setArguments(bdl);
        fragment.setTargetFragment(this, ADDRESS_RESULT);
        fragment.show(getChildFragmentManager(), fragment.getClass().getSimpleName());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ADDRESS_RESULT && resultCode == Activity.RESULT_OK) {
            Address address = (Address) data.getSerializableExtra(kData);
            startTransaction(address);
        }
    }

    public void startTransaction(Address address){
        startActivity(TransactionDetailActivity.getIntent(getContext(), address));
        final Handler handler = new Handler();
        handler.postDelayed(() -> tabLayout.setVisibility(View.VISIBLE), 1000);
        getFragmentManager().popBackStack();
    }


    /*
    Shop Cart Adapter for shop details
     */
    public class CartAdapter extends RecyclerView.Adapter<CartAdapter.MyViewHolder> {

        private List<CartShop> shopList;

        private CartAdapter(List<CartShop> cartShop) {
            this.shopList = cartShop;
        }

        @Override
        public CartAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_shoping_cart_layout, parent, false);
            return new CartAdapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final CartAdapter.MyViewHolder holder, int position) {

            CartShop shop = shopList.get(position);
            holder.bindContent(shop);
            holder.tvShopName.setText(shop.getShopName());
            String address = "No Address Available";
            if (shop.getShopAddress() != null) {
                address = shop.getShopAddress().getCity() + ", "
                        + shop.getShopAddress().getState() + ", "
                        + shop.getShopAddress().getCountry();
            }
            holder.tvShopAddress.setText(address);
            holder.ivChat.setVisibility(View.GONE);
            holder.ivLocation.setVisibility(View.GONE);
            GradientDrawable gradientDrawable = new GradientDrawable();
            gradientDrawable.setShape(GradientDrawable.RECTANGLE);
            gradientDrawable.setColor(ColorGenerator.MATERIAL.getRandomColor());

            if (!shop.getShopBannerImageUrl().isEmpty())
                Picasso.with(getContext())
                        .load(shop.getShopBannerImageUrl())
                        .fit()
                        .placeholder(R.drawable.img_product_placeholder)
                        .into(holder.ivShopBanner);
            else
                holder.ivShopBanner.setImageDrawable(gradientDrawable);

            CartProductAdapter productAdapter = new CartProductAdapter(switcherListener, getContext(), shop.getProductList(), ShoppingCartFragment.this);
            holder.productRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
            holder.productRecycler.setAdapter(productAdapter);
        }

        public void addItems(List<CartShop> list) {
            shopList.clear();
            shopList.addAll(list);
            notifyDataSetChanged();
        }

        @Override
        public int getItemCount() {
            return shopList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            CartShop item;
            @BindView(R.id.product_recycler)
            RecyclerView productRecycler;
            @BindView(R.id.iv_shop_image)
            ImageView ivShopBanner;
            @BindView(R.id.tv_name)
            TextView tvShopName;
            /* @BindView(R.id.tv_owner_name)
             TextView tvOwnerName;*/
            @BindView(R.id.tv_address)
            TextView tvShopAddress;
            @BindView(R.id.iv_chat)
            ImageView ivChat;
            @BindView(R.id.iv_location)
            ImageView ivLocation;


            MyViewHolder(View view) {
                super(view);
                ButterKnife.bind(this, view);
            }

            public void bindContent(CartShop shop) {
                this.item = shop;
            }
        }

    }
}
