package com.app.inn.peepsin.UI.fragments.Feeds;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.inn.peepsin.Constants.Blocks.Block;
import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Libraries.DispatchQueue.DispatchQueue;
import com.app.inn.peepsin.Managers.BaseManager.PermissionManager;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.CurrentUser;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.activities.HomeActivity;
import com.app.inn.peepsin.UI.helpers.DirectionsJSONParser;
import com.app.inn.peepsin.UI.helpers.DistanceJSONParser;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.app.inn.peepsin.Constants.Constants.kImageUrl;
import static com.app.inn.peepsin.Constants.Constants.kLatitude;
import static com.app.inn.peepsin.Constants.Constants.kLongitude;
import static com.app.inn.peepsin.Constants.Constants.kMessageInternalInconsistency;
import static com.app.inn.peepsin.Constants.Constants.kOwnerName;

/**
 * Created by akamahesh on 5/5/17.
 */

public class MapFragment extends Fragment {

    String TAG = getClass().getSimpleName() + "--->";
    Activity context;

    @BindView(R.id.map_view)
    MapView mapView;
    private GoogleMap mMap;
    private PermissionManager permissionManager;

    private CurrentUser user;
    private String locationName;
    private String destinationUrl;
    private String latitude, longitude;

    LatLng origin;
    LatLng destination;
    BitmapDescriptor sellerIcon;
    BitmapDescriptor buyerIcon;

    public static Fragment newInstance(String latitude, String longitude, String name, String url) {
        Fragment fragment = new MapFragment();
        Bundle bundle = new Bundle();
        bundle.putString(kLatitude, latitude);
        bundle.putString(kLongitude, longitude);
        bundle.putString(kOwnerName, name);
        bundle.putString(kImageUrl, url);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = (HomeActivity) context;
        permissionManager = new PermissionManager((HomeActivity) context);
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        bundle = getArguments();
        if (bundle != null) {
            latitude = bundle.getString(kLatitude);
            longitude = bundle.getString(kLongitude);
            locationName = bundle.getString(kOwnerName);
            destinationUrl = bundle.getString(kImageUrl);
        }
        user = ModelManager.modelManager().getCurrentUser();

        //Defining lat and long for map
        getLatLong();
    }


    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View v = layoutInflater.inflate(R.layout.fragment_map, viewGroup, false);
        ButterKnife.bind(this, v);
        mapView.onCreate(bundle);
        // Gets to GoogleMap from the MapView and does initialization stuff
        mapView.getMapAsync(onMapReadyCallback);

        // map.getUiSettings().setMyLocationButtonEnabled(false);
        // map.setMyLocationEnabled(true);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            permissionManager.requestPermission(Manifest.permission.ACCESS_FINE_LOCATION, 1);
        }

        // Needs to call MapsInitializer before doing any CameraUpdateFactory calls
        try {
            MapsInitializer.initialize(this.getActivity());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return v;
    }

    private void getLatLong() {
        String lat = user.getPrimaryAddress().getLatitude().trim();
        String lon = user.getPrimaryAddress().getLongitude().trim();

        double userLat = Double.valueOf(lat);
        double userLong = Double.valueOf(lon);
        double sellerLat = Double.valueOf(latitude);
        double sellerLong = Double.valueOf(longitude);
        origin = new LatLng(userLat, userLong);
        destination = new LatLng(sellerLat, sellerLong);
    }


    OnMapReadyCallback onMapReadyCallback = new OnMapReadyCallback() {
        @Override
        public void onMapReady(GoogleMap googleMap) {
            mMap = googleMap;
            mMap.moveCamera(CameraUpdateFactory.newLatLng(origin));
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(origin, 10);
            mMap.animateCamera(cameraUpdate);
            mMap.getUiSettings().setZoomControlsEnabled(true);

            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                permissionManager.requestPermission(Manifest.permission.ACCESS_FINE_LOCATION, 1);
                return;
            }

            mMap.setMyLocationEnabled(true);

            //getting url to the google directions api
            String url = getDirectionUrl(origin, destination);
            Log.v(TAG, " : " + url);

            buyerIcon = BitmapDescriptorFactory.fromResource(R.drawable.ic_address);
            sellerIcon = BitmapDescriptorFactory.fromResource(R.drawable.ic_address);

            String sellerPic = destinationUrl;
            if (!sellerPic.isEmpty()) {
                getImage(sellerPic, (Constants.Status iStatus, GenricResponse<Bitmap> genricResponse) -> {
                    sellerIcon = BitmapDescriptorFactory.fromBitmap(genricResponse.getObject());
                }, (Constants.Status iStatus, String message) -> {
                    // sellerIcon = BitmapDescriptorFactory.fromResource(R.drawable.ic_address);
                    Toaster.toast(message);
                });
            } else
                sellerIcon = BitmapDescriptorFactory.fromResource(R.drawable.ic_address);
            String userPic = user.getProfilePicURL();
            if (!userPic.isEmpty()) {
                // setImageForUsers(userPic);
                getImage(userPic, (Constants.Status iStatus, GenricResponse<Bitmap> genricResponse) -> {
                    buyerIcon = BitmapDescriptorFactory.fromBitmap(genricResponse.getObject());
                }, (Constants.Status iStatus, String message) -> {
                    // buyerIcon = BitmapDescriptorFactory.fromResource(R.drawable.ic_address);
                    Toaster.toast(message);
                });
            } else
                buyerIcon = BitmapDescriptorFactory.fromResource(R.drawable.ic_address);

            //String sellerPic = product.getSeller().getProfilePicUrl();


            DownloadTask downloadTask = new DownloadTask();
            downloadTask.execute(url);
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    public void getImage(String imageUrl, Block.Success<Bitmap> success, Block.Failure failure) {
        ModelManager.modelManager().getDispatchQueue().async(() -> {
            try {
                Bitmap bitmap;
                URL imageURL = new URL(imageUrl);
                bitmap = BitmapFactory.decodeStream(imageURL.openConnection().getInputStream());
                Bitmap smallMarker = Bitmap.createScaledBitmap(bitmap, 50, 50, false);
                GenricResponse<Bitmap> genricResponse = new GenricResponse<>(smallMarker);
                DispatchQueue.main(() -> success.iSuccess(Constants.Status.success, genricResponse));
            } catch (Exception e) {
                DispatchQueue.main(() -> failure.iFailure(Constants.Status.fail, kMessageInternalInconsistency));
            }
        });
    }

    public void setImageForUsers(String imageUrl) {

        try {
            Bitmap bitmap;
            URL imageURL = new URL(imageUrl);
            bitmap = BitmapFactory.decodeStream(imageURL.openConnection().getInputStream());
            Bitmap smallMarker = Bitmap.createScaledBitmap(bitmap, 100, 100, false);
            buyerIcon = BitmapDescriptorFactory.fromBitmap(smallMarker);


        } catch (Exception e) {
        }
    }

    @SuppressLint("StaticFieldLeak")
    public class DownloadTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            String data = "";
            try {
                data = downloadUrl(strings[0]);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return data;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            ParserTask parserTask = new ParserTask();
            parserTask.execute(s);
            JSONObject jsonObject;

            try {
                jsonObject = new JSONObject(s);
                DistanceJSONParser parser = new DistanceJSONParser();
                List<Map<String, String>> routeInfo = parser.parse(jsonObject);
                Log.e(TAG, "path : " + routeInfo);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    @SuppressLint("StaticFieldLeak")
    public class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... strings) {
            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(strings[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> hashMaps) {
            super.onPostExecute(hashMaps);
            try {
                String distance = "";
                String duration = "";
                ArrayList points = new ArrayList();
                PolylineOptions lineOptions = new PolylineOptions();

                for (int i = 0; i < hashMaps.size(); i++) {
                    List<HashMap<String, String>> path = hashMaps.get(i);

                    for (int j = 0; j < path.size(); j++) {
                        HashMap point = path.get(j);
                        if (j == 0) {    // Get distance from the list
                            distance = (String) point.get("distance");
                            continue;
                        } else if (j == 1) { // Get duration from the list
                            duration = (String) point.get("duration");
                            continue;
                        }

                        double lat = Double.parseDouble((String) point.get("lat"));
                        double lng = Double.parseDouble((String) point.get("lng"));
                        LatLng position = new LatLng(lat, lng);

                        points.add(position);
                    }

                    lineOptions.addAll(points);
                    lineOptions.width(12);
                    lineOptions.color(Color.RED);
                    lineOptions.geodesic(true);
                }
                // Toaster.toastRangeen("Distance:"+distance + ", Duration:"+duration);


                // Drawing polyline in the Google Map for the i-th route
                mMap.clear();
                MarkerOptions markerOptions1 = new MarkerOptions()
                        .position(origin)
                        .icon(buyerIcon)
                        .title(distance + ", " + duration);
                //.title("Buyer: "+user.getFirstName());

                MarkerOptions markerOptions2 = new MarkerOptions();
                markerOptions2.position(destination)
                        .icon(sellerIcon)
                        .title(distance + ", " + duration);

                // .title("Seller: "+locationName);

                mMap.addMarker(markerOptions1);
                mMap.addMarker(markerOptions2);
                if (lineOptions.isGeodesic())
                    mMap.addPolyline(lineOptions);
            } catch (Exception e) {
                Toaster.toast(e.toString());
                e.printStackTrace();
            }
        }
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.connect();

            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuilder sb = new StringBuilder();

            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            if (iStream != null) {
                iStream.close();
            }
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return data;
    }

    private String getDirectionUrl(LatLng origin, LatLng destination) {
        //origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        //destination of route
        String str_dest = "destination=" + destination.latitude + "," + destination.longitude;

        //sensor enabled
        String sensor = "sensor=false";
        String mode = "mode-driving";

        //building the parameters to the webservice
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + mode;
        //output format
        String output = "json";

        //Building the url to the web service
        return "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;
    }

}
