package com.app.inn.peepsin.UI.fragments;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.Address;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.fragments.Profile.AddNewAddressFragment;
import com.app.inn.peepsin.UI.helpers.DividerItemRecyclerDecoration;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.AddressListener;
import com.app.inn.peepsin.UI.interfaces.Switcher;

import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by root on 10/7/17.
 */

public class AddressFragment extends Fragment {

    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.recycler_view_history)
    RecyclerView recyclerView;
    @BindView(R.id.empty_view)
    View emptyView;
    @BindView(R.id.tv_empty_message)
    TextView tvEmptyTextMessage;
    @BindView(R.id.tv_empty_title)
    TextView tvEmptyTitle;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;

    static AddressListener listener;
    ProgressDialog progressDialog;
    private AddressAdapter addressAdapter;
    private CopyOnWriteArrayList<Address> addressList;
    Paint p = new Paint();
    private static Switcher profileSwitcherListener;

    public static Fragment newInstance(AddressListener addressListener) {
        listener = addressListener;
        return new AddressFragment();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = Utils.generateProgressDialog(getContext(), false);
        addressList = new CopyOnWriteArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_address_fragment, container, false);
        ButterKnife.bind(this, view);
        tvTitle.setText(getString(R.string.my_addresses));

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DividerItemRecyclerDecoration(getActivity(), R.drawable.canvas_recycler_divider));
        addressAdapter = new AddressAdapter(addressList);
        recyclerView.setAdapter(addressAdapter);

        swipeToDismissTouchHelper.attachToRecyclerView(recyclerView);

        tvEmptyTitle.setText(getString(R.string.empty_no_address_available));
        tvEmptyTextMessage.setText(getString(R.string.empty_address_message));
        swipeRefreshLayout.setOnRefreshListener(onRefreshListener);
        return view;
    }

    SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            loadData();
        }
    };

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData();
    }

    ItemTouchHelper swipeToDismissTouchHelper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            int swipedPosition = viewHolder.getAdapterPosition();
            Address address = addressAdapter.getItem(swipedPosition);
            deleteAddress(address,address.getAddressId(),swipedPosition);
        }

        @Override
        public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
            Bitmap icon;
            if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
                View itemView = viewHolder.itemView;
                float height = (float) itemView.getBottom() - (float) itemView.getTop();
                float width = height / 3;

                p.setColor(Color.parseColor("#D32F2F"));
                RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(), (float) itemView.getRight(), (float) itemView.getBottom());
                c.drawRect(background, p);
                icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_close_white_24dp);
                RectF icon_dest = new RectF((float) itemView.getRight() - 2 * width, (float) itemView.getTop() + width, (float) itemView.getRight() - width, (float) itemView.getBottom() - width);
                c.drawBitmap(icon, null, icon_dest, p);

            }
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
        }
    });



    private void loadData(){
        showProgress(true);
        ModelManager.modelManager().getMyAddress((Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<Address>> genericList) -> {
            showProgress(false);
            swipeRefreshLayout.setRefreshing(false);
            addressAdapter.addAddress(genericList.getObject());
            checkEmptyScreen();
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Toaster.toast(message);
            checkEmptyScreen();
        });
    }

    private void deleteAddress(Address address,int addressId, int swipedPosition) {
        showProgress(true);
        ModelManager.modelManager().getDeleteAddress(addressId,(Constants.Status iStatus,GenricResponse<CopyOnWriteArrayList<Address>> genericList) -> {
            showProgress(false);
            addressAdapter.addAddress(genericList.getObject());
            checkEmptyScreen();
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Toaster.toast(message);
            checkEmptyScreen();
            addressAdapter.notifyDataSetChanged();
        });
    }

    @OnClick(R.id.btn_back)
    void onBack() {
        listener.onBack();
    }

    @OnClick(R.id.fab)
    void addAddress(){
        if(listener!=null)
            listener.switchFragment(AddNewAddressFragment.newInstance(1,null,null,listener),true,true);
    }

    private void checkEmptyScreen() {
        if(addressAdapter.getItemCount()>0){
            emptyView.setVisibility(View.GONE);
        }else {
            emptyView.setVisibility(View.VISIBLE);
        }
    }

    private void showProgress(boolean b) {
        if (b) {
            //progressDialog.show();
            swipeRefreshLayout.setRefreshing(true);
        } else {
            swipeRefreshLayout.setRefreshing(false);
            //if (progressDialog != null) progressDialog.cancel();
        }
    }

    class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.StuffHolder> {
        private  CopyOnWriteArrayList<Address> addressList;
        AddressAdapter(CopyOnWriteArrayList<Address> addresses){
            this.addressList=addresses;
        }

        @Override
        public AddressAdapter.StuffHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_my_address_list, parent, false);
            return new AddressAdapter.StuffHolder(layoutView);
        }

        @Override
        public void onBindViewHolder(AddressAdapter.StuffHolder holder, int position) {
            Address address = addressList.get(position);
            holder.bindContent(address);
            String addressLineOne = address.getStreetAddress();
            String addressLineTwo = address.getCity()+", "+address.getCountry()+", "+address.getZipCode();
            String phone = address.getPhone();
            holder.tvName.setText(address.getFullName());
            holder.tvAddressId.setText(addressLineOne);
            holder.tvCity.setText(addressLineTwo);
            holder.tvPhone.setText(phone);

            if (address.getPrimary()) {
                holder.ivPrimaryImage.setVisibility(View.VISIBLE);
            }else {
                holder.ivPrimaryImage.setVisibility(View.GONE);
            }
        }

        @Override
        public int getItemCount() {
            return addressList.size();
        }

        Address getItem(int index) {
            return addressList.get(index);
        }

        void addAddress(CopyOnWriteArrayList<Address> address){
            addressList.clear();
            addressList.addAll(address);
            notifyDataSetChanged();
        }

        class StuffHolder extends RecyclerView.ViewHolder {
            private Address address;
            @BindView(R.id.iv_primary_image)
            ImageView ivPrimaryImage;
            @BindView(R.id.tv_address_line_one) TextView tvAddressId;
            @BindView(R.id.tv_address_line_two) TextView tvCity;
            @BindView(R.id.tv_phone_no) TextView tvPhone;
            @BindView(R.id.tv_name) TextView tvName;

            StuffHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            void bindContent(Address address) {
                this.address = address;
            }
            @OnClick(R.id.item_view)
            void editAddress(){
                listener.address(address);
            }
        }
    }
}
