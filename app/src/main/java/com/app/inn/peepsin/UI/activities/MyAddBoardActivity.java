package com.app.inn.peepsin.UI.activities;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.app.inn.peepsin.BuildConfig;
import com.app.inn.peepsin.Managers.BaseManager.ExceptionHandler;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.fragments.Board.AddBoardFragment;
import com.app.inn.peepsin.UI.fragments.Board.CommentFragment;
import com.app.inn.peepsin.UI.fragments.Board.MyAddBoardFragment;
import com.app.inn.peepsin.UI.fragments.Board.MyCommentFragment;
import com.app.inn.peepsin.UI.helpers.FragmentUtil;

import butterknife.ButterKnife;

import static com.app.inn.peepsin.Constants.Constants.kBoardId;
import static com.app.inn.peepsin.Constants.Constants.kFlag;
import static com.app.inn.peepsin.Constants.Constants.kReload;

public class MyAddBoardActivity extends AppCompatActivity {
    private Integer FLAG;
    private Integer boardId;
    Fragment fragment;

    public static Intent getIntent(Context context,int flag){
        Intent intent = new Intent(context,MyAddBoardActivity.class);
        intent.putExtra(kFlag,flag);
        return intent;
    }

    public static Intent getIntent(Context context,int flag,int boardId){
        Intent intent = new Intent(context,MyAddBoardActivity.class);
        intent.putExtra(kFlag,flag);
        intent.putExtra(kBoardId,boardId);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
        setContentView(R.layout.fragment_root);
        ButterKnife.bind(this);

        if (!BuildConfig.DEBUG) {
            // do something for a debug build
            Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        }
        Intent intent = getIntent();
        if(intent!=null){
            FLAG = intent.getIntExtra(kFlag,0);
            boardId = intent.getIntExtra(kBoardId,0);
        }

        if(FLAG==1)
            FragmentUtil.changeFragment(getSupportFragmentManager(), MyAddBoardFragment.newInstance(),false,false);
        else if(FLAG==2)
            FragmentUtil.changeFragment(getSupportFragmentManager(), MyCommentFragment.newInstance(boardId),false,false);

    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
    }

    public void endisNear(){
        Intent data = new Intent();
        String text = "from add post....";
        data.putExtra(kFlag,text);
        data.putExtra(kReload,true);
        setResult(RESULT_OK, data);
    }

    @Override
    public void onBackPressed() {
        endisNear();
        super.onBackPressed();
    }
}
