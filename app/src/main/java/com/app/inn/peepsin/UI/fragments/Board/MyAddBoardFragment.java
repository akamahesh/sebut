package com.app.inn.peepsin.UI.fragments.Board;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.activities.AddBoardActivity;
import com.app.inn.peepsin.UI.activities.MyAddBoardActivity;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by akamahesh on 2/5/17.
 */

public class MyAddBoardFragment extends Fragment {

    @BindView(R.id.tv_title)
    TextView tvTitle;

    @BindView(R.id.iv_user_image)
    ImageView ivUserImage;

    @BindView(R.id.tv_user_name)
    TextView tvUserName;


    @BindView(R.id.et_description)
    EditText edtDescription;

   /* @BindView(R.id.tv_privacy)
    TextView tvPrivacy;*/

    ProgressDialog progressDialog;
    AlertDialog levelDialog;


    public static Fragment newInstance() {
        return new MyAddBoardFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = Utils.generateProgressDialog(getContext(), false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_share_board, container, false);
        ButterKnife.bind(this, view);
        tvTitle.setText("Write a Post");
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        String imageUrl = ModelManager.modelManager().getCurrentUser().getProfilePicURL();
        String userName = ModelManager.modelManager().getCurrentUser().getFullName();
        if(!userName.equals(""))
        tvUserName.setText(userName);
        if (!imageUrl.isEmpty())
            Picasso.with(getContext())
                    .load(imageUrl)
                    .resize(100, 100)
                    .into(ivUserImage);
        else
            ivUserImage.setImageDrawable(getResources().getDrawable(R.drawable.img_profile_placeholder));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        showProgress(false);
        progressDialog = null;
    }

    @OnClick(R.id.btn_back)
    void onBack() {
        Utils.hideKeyboard(getContext());
        MyAddBoardActivity addBoardActivity = (MyAddBoardActivity) getActivity();
        addBoardActivity.onBackPressed();
    }

    @OnClick(R.id.btn_post)
    void onAddBoard() {
        String description = edtDescription.getText().toString().trim();
        if (description.isEmpty()) {
            shareDialogOpen(getString(R.string.comment_not_empty_text));
            return;
        }
        showProgress(true);
        ModelManager.modelManager().addBoard(description, (Constants.Status iStatus) -> {
            showProgress(false);
            onBack();
        }, (Constants.Status iStatus, String message) -> {
            Toaster.toast(message);
            showProgress(false);
        });
    }

    //Show dialog
    private void shareDialogOpen(String title) {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_dialog_block);
        TextView tvTitle = ButterKnife.findById(dialog, R.id.tv_dialog);
        Button tvOk = ButterKnife.findById(dialog, R.id.btn_ok);
        tvTitle.setText(title);
        tvOk.setOnClickListener(v -> dialog.dismiss());
        dialog.show();
    }

  /*  @OnClick(R.id.tv_privacy)
    void onPrivacy() {
        showPrivacyDialog();
    }

    @OnClick(R.id.iv_camera)
    void onAddImage() {
        Toaster.toast("add a photo");
    }*/


    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) progressDialog.dismiss();
        }
    }


    /*void showPrivacyDialog() {
        final CharSequence[] items = {"Only Me", "1st Level", "2nd Level", "3rd Level", "Public"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), R.style.MyAlertDialogStyle);
        builder.setTitle("Select Connection Level");
        builder.setSingleChoiceItems(items, -1, (dialog, item) -> {
            switch (item) {
                case 0:
                    tvPrivacy.setText(items[item]);
                    break;
                case 1:
                    tvPrivacy.setText(items[item]);
                    break;
                case 2:
                    tvPrivacy.setText(items[item]);
                    break;
                case 3:
                    tvPrivacy.setText(items[item]);
                    break;
                case 4:
                    tvPrivacy.setText(items[item]);
                    break;

            }
            levelDialog.dismiss();
        });
        levelDialog = builder.create();
        levelDialog.show();
    }*/

}
