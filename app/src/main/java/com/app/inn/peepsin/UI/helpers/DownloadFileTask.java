package com.app.inn.peepsin.UI.helpers;

import android.util.Log;

import com.app.inn.peepsin.Constants.Blocks.Block;
import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Libraries.DispatchQueue.DispatchQueue;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by root on 31/5/17.
 */

public class DownloadFileTask {

    private static DownloadFileTask mInstance;

    public static DownloadFileTask getInstance() {
        if (mInstance == null) {
            mInstance = new DownloadFileTask();
        }
        return mInstance;
    }

    public void getDownloadedImageList(List<String> list, Block.Success<List<String>> success, Block.Failure failure) {
        String sdPath = "/sdcard/KutCamera/cache/images/";
        File videoDirectory = new File(sdPath);

        if (!videoDirectory.exists()) {
            videoDirectory.mkdirs();
        }
        ModelManager.modelManager().getDispatchQueue().async(() -> {
            try {
                List<String> pathList = new ArrayList<>();

                for(int i=0;i<list.size();i++){

                    String filepath=null;
                    URL url = new URL(list.get(i));
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setRequestMethod("POST");
                    urlConnection.setDoOutput(true);
                    urlConnection.connect();

                    Calendar c = Calendar.getInstance();
                    String filename= sdPath +c.getTime().getSeconds()+".jpg";
                    Log.i("Local filename:",""+filename);
                    File file = new File(filename);
                    if(file.createNewFile())
                    {
                        file.createNewFile();
                    }
                    FileOutputStream fileOutput = new FileOutputStream(file);
                    InputStream inputStream = new BufferedInputStream(url.openStream(), 4098);
                    //InputStream inputStream = urlConnection.getInputStream();
                    int totalSize = urlConnection.getContentLength();

                    int downloadedSize = 0;
                    byte[] buffer = new byte[4096];
                    int bufferLength;
                    while ( (bufferLength = inputStream.read(buffer)) > 0 )
                    {
                        fileOutput.write(buffer, 0, bufferLength);
                        downloadedSize += bufferLength;
                        Log.i("Progress:","downloadedSize:"+downloadedSize+"totalSize:"+ totalSize) ;
                    }
                    fileOutput.close();
                    if(downloadedSize==totalSize)
                        filepath=file.getAbsolutePath();

                    pathList.add(filepath);
                }

                GenricResponse<List<String>> genricResponse = new GenricResponse<>(pathList);
                DispatchQueue.main(() -> success.iSuccess(Constants.Status.success, genricResponse));
            } catch (Exception e) {
                DispatchQueue.main(() -> failure.iFailure(Constants.Status.fail, e.toString()));
            }
        });
    }

}
