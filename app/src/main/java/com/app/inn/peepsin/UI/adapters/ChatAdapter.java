package com.app.inn.peepsin.UI.adapters;

import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.BaseManager.DateManager;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.CurrentUser;
import com.app.inn.peepsin.Models.Message;
import com.app.inn.peepsin.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by akaMahesh on 11/5/17.
 * copyright to : Innverse Technologies
 */

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ChatViewHolder> {

    private List<Message> chatList;
    private ColorGenerator generator = ColorGenerator.MATERIAL;
    private String senderName;
    private String receiverName;
    public ChatAdapter(List<Message> chatList,String receiverName) {
        this.chatList = chatList;
        senderName = ModelManager.modelManager().getCurrentUser().getFirstName();
        this.receiverName = receiverName;
    }

    @Override
    public ChatViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = null;
        if(viewType==Constants.MessageDirection.receive.getValue()){
            layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_incoming_text, parent, false);
        }
        else if(viewType==Constants.MessageDirection.send.getValue()){
            layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_outgoing_text, parent, false);
        }
        return new ChatViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(ChatViewHolder holder, int position) {
        Message chat = chatList.get(position);
        int viewType = getItemViewType(position);
        String mgsTxt  =chat.getBody();
        String timing = chat.getTime();
        timing = DateManager.dispalyValue(timing);
        holder.tvMessage.setText(mgsTxt);
        holder.tvTime.setText(timing);
        String textD = "Buyer";
        if(viewType==Constants.MessageDirection.send.getValue())
            textD = senderName;
        else
            textD = (receiverName!=null)?receiverName:textD;

        TextDrawable drawable = TextDrawable.builder().buildRound(String.valueOf(textD.toUpperCase().charAt(0)), generator.getRandomColor());
        holder.ivUserImage.setImageDrawable(drawable);
    }

    @Override
    public int getItemCount() {
        return chatList.size();
    }

    public void addItem(Message chat){
        chatList.add(chat);
        notifyDataSetChanged();
    }

    public void addChats(List<Message> chats){
        chatList.addAll(chats);
        notifyDataSetChanged();
    }



    @Override
    public int getItemViewType(int position) {
        Message messageChat=chatList.get(position);
        String k= messageChat.getDirection();
        if (k.equals(Constants.MessageDirection.receive.getValue().toString())) {
            return 1;
        }else {
            return 2;
        }
    }
    class ChatViewHolder extends RecyclerView.ViewHolder{
        private Message chat;
        @BindDrawable(R.drawable.ic_schedule_grey_500_18dp) Drawable drawableSending;
        @BindDrawable(R.drawable.ic_check_grey_500_18dp) Drawable drawableSend;
        @BindDrawable(R.drawable.ic_done_all_grey_500_18dp) Drawable drawableSent;


        @BindView(R.id.tv_message) TextView tvMessage;
        @BindView(R.id.tv_time) TextView tvTime;
        @BindView(R.id.iv_user_image)
        ImageView ivUserImage;


        ChatViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        void bindContent(Message chat){
            this.chat = chat;
        }
    }
}

