package com.app.inn.peepsin.Managers.XMPPManager;

import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.app.inn.peepsin.Constants.Blocks.Block;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Libraries.DispatchQueue.DispatchQueue;
import com.app.inn.peepsin.Libraries.Firebase.utils.NotificationUtils;
import com.app.inn.peepsin.Managers.APIManager.ReachabilityManager;
import com.app.inn.peepsin.Managers.BaseManager.ApplicationManager;
import com.app.inn.peepsin.Managers.BaseManager.DateManager;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.ChatProduct;
import com.app.inn.peepsin.Models.Message;
import com.app.inn.peepsin.UI.activities.SplashActivity;

import org.jivesoftware.smack.AbstractXMPPConnection;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.SmackConfiguration;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.chat.Chat;
import org.jivesoftware.smack.chat.ChatManager;
import org.jivesoftware.smack.packet.DefaultExtensionElement;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.roster.Roster;
import org.jivesoftware.smack.roster.RosterEntry;
import org.jivesoftware.smack.roster.RosterListener;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smackx.iqregister.AccountManager;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import static com.app.inn.peepsin.Constants.Constants.kImageUrl;
import static com.app.inn.peepsin.Constants.Constants.kJabberId;
import static com.app.inn.peepsin.Constants.Constants.kMessage;
import static com.app.inn.peepsin.Constants.Constants.kMessageDirection;
import static com.app.inn.peepsin.Constants.Constants.kMessageId;
import static com.app.inn.peepsin.Constants.Constants.kMessageStatus;
import static com.app.inn.peepsin.Constants.Constants.kName;
import static com.app.inn.peepsin.Constants.Constants.kProductId;
import static com.app.inn.peepsin.Constants.Constants.kProductName;
import static com.app.inn.peepsin.Constants.Constants.kProductPrice;
import static com.app.inn.peepsin.Constants.Constants.kProductThumbnailURL;
import static com.app.inn.peepsin.Constants.Constants.kTimestamp;
import static com.app.inn.peepsin.Constants.Constants.kUserId;
import static com.app.inn.peepsin.Constants.Constants.kUserImageURL;
import static com.app.inn.peepsin.Constants.Constants.kUserName;
import static com.app.inn.peepsin.Managers.XMPPManager.Constant.kIncomingMessage;
import static com.app.inn.peepsin.Managers.XMPPManager.Constant.kQueueNameXMPP;
import static com.app.inn.peepsin.Managers.XMPPManager.Constant.kXMPPHost;
import static com.app.inn.peepsin.Managers.XMPPManager.Constant.kXMPPPassword;
import static com.app.inn.peepsin.Managers.XMPPManager.Constant.kXMPPPort;

/**
 * Created by akaMahesh on 30/6/17.
 * copyright to : Innverse Technologies
 */

public class RoosterManager implements ConnectionListener, ReachabilityManager.ConnectivityReceiverListener {
    private static String TAG = "Smack Rooster : ";


    //Static Properties
    private static final AtomicReference<RoosterManager> _ROOSTER = new AtomicReference<>();
    //Instance Properties
    private DispatchQueue mDispatchQueue = new DispatchQueue(kQueueNameXMPP, DispatchQueue.QoS.userInteractive);
    private AbstractXMPPConnection mConnectionManager = null;
    private ChatManager mChatManager = null;
    private String mUserName;

    /* A private Constructor prevents any other class from instantiating.*/
    private RoosterManager() {
        this.setupConnection();
        ApplicationManager.setConnectivityListener(this);
        this.connectUserToXMPPServer();
    }

    public static synchronized RoosterManager roost() {
        for (; ; ) {
            RoosterManager instance = _ROOSTER.get();
            if (instance != null) {
                return instance;
            }
            instance = new RoosterManager();
            if (_ROOSTER.compareAndSet(null, instance))
                return instance;
        }
    }

    public void initialize() {
        System.out.println("Rooster Manager has been initialized!");
    }


    private void setupConnection() {
        SmackConfiguration.DEBUG = true;
        String jabberId = ModelManager.modelManager().getCurrentUser().getJabberId();
        String parts[] = jabberId.split("@");
        mUserName = parts[0];
        // Create a connection to the jabber.org server on a specific port.
        XMPPTCPConnectionConfiguration _ConfigurationManager = XMPPTCPConnectionConfiguration.builder()
                .setUsernameAndPassword(mUserName, kXMPPPassword)
                .setHost(kXMPPHost)
                .setServiceName(kXMPPHost)
                .setSecurityMode(ConnectionConfiguration.SecurityMode.required)
                .setCustomSSLContext(RoosterHelper.getSSLContext())
                .setPort(kXMPPPort)
                .build();

        mConnectionManager = new XMPPTCPConnection(_ConfigurationManager);
        //All listeners will executed in com.queue.serial.xmpp serial queue.
        mConnectionManager.addConnectionListener(this);
        mChatManager = ChatManager.getInstanceFor(mConnectionManager);
    }


    int counter = 0;
    void connectUserToXMPPServer() {
        mDispatchQueue.async(() -> {
            try {
                if (!mConnectionManager.isConnected()) {
                    mConnectionManager.connect();
                }
                if(!mConnectionManager.isAuthenticated())
                    mConnectionManager.login();
            } catch (Exception e) {
                e.printStackTrace();
                if(counter++<4){
                    connectUserToXMPPServer();
                }
                //connectUserToXMPPServer();
            }
        });
    }

    public void disconnectFromXMPPServer() {
        mDispatchQueue.async(() -> {
            if (mConnectionManager != null)
                mConnectionManager.disconnect();
        });
    }

    @Override
    public void connected(XMPPConnection connection) {
        Log.v(TAG, "connected");
        try {
            registerAccount();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void authenticated(XMPPConnection connection, boolean resumed) {
        Log.i(TAG, "authenticated");
        joinAllChat();
        getPresence();
    }



    private void joinAllChat() {
        mChatManager.addChatListener(
                (chat, createdLocally) -> {
                    chat.addMessageListener((chat1, message) -> {
                        String messageToast = (message != null ? message.getBody() : "NULL");
                        Log.i(TAG, messageToast);
                        ChatProduct chatProduct = null;
                        if (message != null) {
                            chatProduct = parseXMPPMessage(message);
                        }

                        RoosterHelper.saveChatThread(chatProduct);
                        showNotificationMessage(chatProduct);
                        Intent messageIntent = new Intent(kIncomingMessage);
                        messageIntent.putExtra(kMessage, chatProduct);
                        LocalBroadcastManager.getInstance(ApplicationManager.getContext()).sendBroadcast(messageIntent);
                    });
                    Log.w("app", chat.toString());
                });
    }


    @Override
    public void connectionClosed() {
        Log.i(TAG, "connectionClosed");
       // connectUserToXMPPServer();
        setLastJoinedTime();
    }

    @Override
    public void connectionClosedOnError(Exception e) {
        setLastJoinedTime();
       /* if (ReachabilityManager.getNetworkStatus()) {
            connectUserToXMPPServer();
        }*/
    }

    @Override
    public void reconnectionSuccessful() {
        Log.i(TAG, "reconnectionSuccessful");
    }

    @Override
    public void reconnectingIn(int seconds) {
        Log.i(TAG, "reconnectingIn");
    }

    @Override
    public void reconnectionFailed(Exception e) {
        Log.i(TAG, "reconnectionFailed");
    }

    private void registerAccount() throws SmackException.NotConnectedException, XMPPException.XMPPErrorException, SmackException.NoResponseException {
        AccountManager accountManager = AccountManager.getInstance(mConnectionManager);
        accountManager.createAccount(mUserName, kXMPPPassword);
    }

    /**
     * Parse the XMPPMessage and discard if needed
     */
    private ChatProduct parseXMPPMessage(@NonNull org.jivesoftware.smack.packet.Message xmppMessage) {
        DefaultExtensionElement messageData = (DefaultExtensionElement) xmppMessage.getExtensions().get(0);
        HashMap<String, String> messageMap = new HashMap<>();
        ChatProduct chatProduct = new ChatProduct();
        //get the messagedata
        for (String key : messageData.getNames()) {
            messageMap.put(key, messageData.getValue(key));
        }
        messageMap.put(kMessageId, xmppMessage.getStanzaId());
        messageMap.put(kMessage, xmppMessage.getBody());
        messageMap.put(kMessageStatus, Constants.MessageStatus.sent.getValue().toString());
        messageMap.put(kMessageDirection, Constants.MessageDirection.receive.getValue().toString());

        chatProduct.setJabberId(messageMap.get(kJabberId));
        chatProduct.setUserId(messageMap.get(kUserId));
        chatProduct.setUserName(messageMap.get(kUserName));
        chatProduct.setUserImageURL(messageMap.get(kUserImageURL));
        chatProduct.setProductId(messageMap.get(kProductId));
        chatProduct.setProductName(messageMap.get(kProductName));
        chatProduct.setProductThumbnailURL(messageMap.get(kProductThumbnailURL));
        chatProduct.setPrice(messageMap.get(kProductPrice));

        Message message = new Message();
        message.setBody(xmppMessage.getBody());
        message.setId(xmppMessage.getStanzaId());
        message.setStatus(Constants.MessageStatus.sent.getValue().toString());
        message.setDirection(Constants.MessageDirection.receive.getValue().toString());
        message.setTime(messageMap.get(kTimestamp));
        chatProduct.getMessageList().add(message);
        return chatProduct;
    }

    /**
     * This method only call in Model Manager.
     * While sending message, we need to check
     * 1.   for network then
     * 2.   check MUC joined status using isJoinedMultiUserChat () if not joined then call joinMultiUserChat()
     * else call sendMessage()
     *
     * @return set status StatusSuccess if message sent otherwise StatusFail in background queue
     */
    public void sendMessage(ChatProduct chatProduct, Block.Status status) {
        mDispatchQueue.async(() -> {
            String jabberId         = chatProduct.getJabberId();
            String userId           = String.valueOf(chatProduct.getUserId());
            String userName         = ModelManager.modelManager().getCurrentUser().getFullName();
            String userImageURL     = chatProduct.getUserImageURL();
            String productId        = String.valueOf(chatProduct.getProductId());
            String productName      = chatProduct.getProductName();
            String productImageURL  = chatProduct.getProductThumbnailURL();
            String price            = chatProduct.getPrice();
            String chatOn           = chatProduct.getChatOn();
            String connectionLevel  = chatProduct.getConnectionLevel();
            Message lastMessage     = chatProduct.getLastMessage();

            org.jivesoftware.smack.packet.Message xmppMessage = new org.jivesoftware.smack.packet.Message(jabberId);
            XMPPMessageExtension messageExtension = new XMPPMessageExtension();
            messageExtension.setJabberId(ModelManager.modelManager().getCurrentUser().getJabberId());
            messageExtension.setUserId(userId);
            messageExtension.setUserName(userName);
            messageExtension.setUserImageUrl(userImageURL);
            messageExtension.setProductId(productId);
            messageExtension.setMessageType(Constants.MessageType.text.getValue().toString());
            messageExtension.setProductName(productName);
            messageExtension.setChatOn(chatOn);
            messageExtension.setConnectionLevel(connectionLevel);
            messageExtension.setProductThumbnailUrl(productImageURL);
            messageExtension.setPrice(price);
            messageExtension.setTimeStamp(String.valueOf(DateManager.getCurrentTimestamp()));
            xmppMessage.setBody(lastMessage.getBody());
            xmppMessage.addExtension(messageExtension);
            try {
                if (mConnectionManager.isAuthenticated()) {
                    Chat chat = mChatManager.createChat(jabberId);
                    chat.sendMessage(xmppMessage);
                    lastMessage.setStatus(Constants.MessageDirection.send.getValue().toString());

                    RoosterHelper.saveChatThread(chatProduct);
                    DispatchQueue.main(() -> status.iStatus(Constants.Status.success));
                } else {
                    connectUserToXMPPServer();
                    DispatchQueue.main(() -> status.iStatus(Constants.Status.fail));
                }
            } catch (SmackException.NotConnectedException e) {
                e.printStackTrace();
                DispatchQueue.main(() -> status.iStatus(Constants.Status.fail));
            }
        });
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        Log.i(TAG, "isConnected : " + isConnected);
        if (isConnected) {
            connectUserToXMPPServer();
        } else {
            //Todo whether disconnect or not.
        }
    }

    private void showNotificationMessage(ChatProduct chatProduct) {
        NotificationUtils notificationUtils = new NotificationUtils(ApplicationManager.getContext());
        String username = chatProduct.getUserName();
        String messageBody = chatProduct.getLastMessage().getBody();
        String time = chatProduct.getLastMessage().getTime();
        notificationUtils.showNotificationMessage(username, messageBody, time, SplashActivity.getIntent(ApplicationManager.getContext()));
    }

    // Playing notification sound
    private void playNotificationSound() {
        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(ApplicationManager.getContext(), notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setLastJoinedTime() {
    }

    public void getPresence() {
        Roster roster = Roster.getInstanceFor(mConnectionManager);
        Collection<RosterEntry> entries = roster.getEntries();
        for (RosterEntry entry : entries) {
            System.out.println(entry.getName() + "is online");
        }
        roster.addRosterListener(new RosterListener() {
            @Override
            public void entriesAdded(Collection<String> addresses) {
                Log.v(TAG + "entriesAdded", addresses.toString());
            }

            @Override
            public void entriesUpdated(Collection<String> addresses) {
                Log.v(TAG + "entriesUpdated", addresses.toString());
            }

            @Override
            public void entriesDeleted(Collection<String> addresses) {
                Log.v(TAG + "entriesDeleted", addresses.toString());
            }

            @Override
            public void presenceChanged(Presence presence) {
                Log.v(TAG + "presenceChanged", presence.toString());
            }
        });
    }


}
