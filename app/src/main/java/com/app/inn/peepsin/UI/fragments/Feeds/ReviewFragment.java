package com.app.inn.peepsin.UI.fragments.Feeds;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.ReviewModel;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.adapters.ReviewAdapter;
import com.app.inn.peepsin.UI.helpers.DividerItemRecyclerDecoration;

import java.io.Serializable;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kProductId;
import static com.app.inn.peepsin.Constants.Constants.kReviewList;

public class ReviewFragment extends Fragment {

    private String TAG = getTag();

    @BindView(R.id.recycler_view_feeds) RecyclerView recyclerView;
    @BindView(R.id.swipeRefreshLayout) SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.tv_title) TextView tvTitle;
    @BindView(R.id.empty_view) View emptyView;
    //@BindView(R.id.progress_bar) ProgressBar progressBar;

    private int mPage;
    private boolean loading;
    private int pageSize;

    private ReviewAdapter reviewAdapter;
    private List<ReviewModel> reviewList;
    private LinearLayoutManager mLayoutManager;

    private int productId;

    public static ReviewFragment newInstance(int productId,List<ReviewModel> list) {
        ReviewFragment fragment = new ReviewFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(kProductId, productId);
        bundle.putSerializable(kReviewList, (Serializable) list);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            productId = bundle.getInt(kProductId);
            reviewList = (List<ReviewModel>) bundle.getSerializable(kReviewList);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_review, container, false);
        ButterKnife.bind(this, view);
        tvTitle.setText(getString(R.string.title_review));

        reviewAdapter = new ReviewAdapter(getContext(), reviewList,pageSize);
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(reviewAdapter);
        recyclerView.addItemDecoration(new DividerItemRecyclerDecoration(getActivity(), R.drawable.canvas_recycler_divider));
        recyclerView.addOnScrollListener(onScrollListener);
        swipeRefreshLayout.setOnRefreshListener(onRefreshListener);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initialState();
        checkEmptyScreen();
    }

    public void initialState(){
        mPage=1;
        loading=true;
        pageSize= reviewList.size();
    }

    @OnClick(R.id.btn_back)
    void onBack(){
        getFragmentManager().popBackStack();
    }


    SwipeRefreshLayout.OnRefreshListener onRefreshListener = this::loadData;

    RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            if(dy > 0) //check for scroll down
            {
                int visibleItemCount = mLayoutManager.getChildCount();
                int totalItemCount = mLayoutManager.getItemCount();
                int firstVisibleItemPosition = mLayoutManager.findFirstVisibleItemPosition();

                if (loading)
                {
                    if ( (visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= pageSize) {
                        loading = false;
                        //Do pagination.. i.e. fetch new data
                        loadMoreData(++mPage);
                    }
                }
            }
        }
    };

    public void loadData() {
        ModelManager.modelManager().getRatingReviewList(1,productId ,(Constants.Status iStatus, GenricResponse<List<ReviewModel>> genericResponse) -> {
            reviewList.clear();
            reviewList.addAll(genericResponse.getObject());
            reviewAdapter.notifyDataSetChanged();
            checkEmptyScreen();
            initialState();
            swipeRefreshLayout.setRefreshing(false);
        }, (Constants.Status iStatus, String message) -> {
            Log.e(TAG,message);
            checkEmptyScreen();
            swipeRefreshLayout.setRefreshing(false);
        });
    }

    public void loadMoreData(int page) {
        ModelManager.modelManager().getRatingReviewList(page,productId, (Constants.Status iStatus, GenricResponse<List<ReviewModel>> genericResponse) -> {
            reviewList.addAll(genericResponse.getObject());
            reviewAdapter.notifyDataSetChanged();
            loading = !genericResponse.getObject().isEmpty();
        }, (Constants.Status iStatus, String message) -> Log.e(TAG,message));
    }

    private void checkEmptyScreen() {
        if (reviewAdapter.getItemCount() > 1) {
            emptyView.setVisibility(View.GONE);
        } else {
            emptyView.setVisibility(View.VISIBLE);
        }
    }

  /*  private void showProgress(boolean b) {
        if (b) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }*/

}
