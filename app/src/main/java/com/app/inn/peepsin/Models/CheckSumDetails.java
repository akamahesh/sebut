package com.app.inn.peepsin.Models;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by root on 18/5/17.
 */

public class CheckSumDetails extends BaseModel implements Serializable{

    private Integer orderId;
    private Integer custId;
    private Integer taxAmount;
    private String mid;
    private String industryId;
    private String channelId;
    private String website;
    private String callbackURL;
    private String checksumHash;

    public CheckSumDetails(JSONObject jsonResponse) {
        this.mid               = getValue(jsonResponse, kMID,              String.class);
        this.taxAmount                 = getValue(jsonResponse, kTXN_AMOUNT,                Integer.class);
        this.custId                 = getValue(jsonResponse, kCUST_ID,                Integer.class);
        this.orderId                 = getValue(jsonResponse, kORDER_ID,                Integer.class);
        this.industryId        = getValue(jsonResponse, kINDUSTRY_TYPE_ID,              String.class);
        this.channelId      = getValue(jsonResponse, kCHANNEL_ID,          String.class);
        this.website           = getValue(jsonResponse, kWEBSITE,          String.class);
        this.callbackURL      = getValue(jsonResponse, kCALLBACK_URL,          String.class);
        this.checksumHash           = getValue(jsonResponse, kCHECKSUMHASH,          String.class);


    }


    public String geMID() {
        return mid;
    }

    public String getIndustryId() {
        return industryId;
    }


    public Integer getOrderId() {
        return orderId;
    }

    public String getChannelId() {
        return channelId;
    }

    public Integer getCustId() {
        return custId;
    }

    public Integer getTaxAmount() {
        return taxAmount;
    }


    public String getWebsite() {
        return website;
    }

    public String getCallbackURL() {
        return callbackURL;
    }

    public String getChecksumHash() {
        return checksumHash;
    }


    public void setMID(String mid) {
        mid = mid;
    }

    public void setIndustryId(String industryId) {
        industryId = industryId;
    }

    public void setChannelId(String channelId) {
        channelId = channelId;
    }

    public void setWebsite(String website) {
        website = website;
    }

    public void setCallbackURL(String callbackURL) {
        callbackURL = callbackURL;
    }

    public void setChecksumHashL(String checksumHash) {
        checksumHash = checksumHash;
    }

    public void setOrderId(Integer orderId) {
        orderId = orderId;
    }
    public void setTaxAmount(Integer taxAmount) {
        taxAmount = taxAmount;
    }
    public void setCustId(Integer custId) {
        custId = custId;
    }



}
