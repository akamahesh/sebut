package com.app.inn.peepsin.Models;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by dhruv on 21/7/17.
 */

public class BankDetails extends BaseModel implements Serializable {

    private String accountHolderName;
    private String accountNumber;
    private String bankName;
    private String IFSCCode;
    private String accountType;

    public BankDetails(String name, String number, String bankName, String ifsc, String account) {
        this.accountHolderName = name;
        this.accountNumber     = number;
        this.bankName          = bankName;
        this.IFSCCode          = ifsc;
        this.accountType       = account;
    }

    public BankDetails(JSONObject jsonResponse) {
        this.accountHolderName = getValue(jsonResponse, kAccountHolderName, String.class);
        this.accountNumber     = getValue(jsonResponse, kAccountNumber,     String.class);
        this.bankName          = getValue(jsonResponse, kBankName,          String.class);
        this.IFSCCode          = getValue(jsonResponse, kIFSC,              String.class);
        this.accountType       = getValue(jsonResponse, kAccountType,       String.class);
    }

    public String getAccountHolderName() {
        return accountHolderName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public String getBankName() {
        return bankName;
    }

    public String getIFSCCode() {
        return IFSCCode;
    }

    public String getAccountType() {
        return accountType;
    }
}
