package com.app.inn.peepsin.Managers.XMPPManager;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.BaseManager.ApplicationManager;
import com.app.inn.peepsin.Managers.BaseManager.BaseManager;
import com.app.inn.peepsin.Models.BaseModel;
import com.app.inn.peepsin.Models.ChatProduct;
import com.app.inn.peepsin.Models.Message;
import com.app.inn.peepsin.Models.PeepsChat;
import com.google.gson.Gson;

import org.jivesoftware.smack.chat.Chat;

import java.lang.reflect.Type;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import static com.app.inn.peepsin.Constants.Constants.kChats;
import static com.app.inn.peepsin.Managers.XMPPManager.Constant.kXMPPProtocol;

/**
 * Created by akaMahesh on 30/6/17.
 * copyright to : Innverse Technologies
 */

public class RoosterHelper {
    private static String TAG = RoosterHelper.class.getSimpleName();
    public static String kChatPreferences = RoosterHelper.class.getSimpleName();
    /**
     * Instances of this class represent a secure socket protocol implementation which acts as a
     * factory for secure socket factories or SSLEngines. This class is initialized with an optional
     * set of key and trust managers and source of secure random bytes.
     * @return @{@link SSLContext}
     */
    public static SSLContext getSSLContext() {
        SSLContext sslContext = null;
        TrustManager[] trustAllCerts = trustAllCertificates();
        try {
            sslContext = SSLContext.getInstance(kXMPPProtocol);
            sslContext.init(null, trustAllCerts, new SecureRandom());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
        return sslContext;
    }

    /**
     * This is the base interface for JSSE trust managers.
     * method trust all posible certificates related to this server.
     * Its although not a right practice
     * //TODO Need to change
     * @return array of {@link TrustManager}
     */
    private synchronized static TrustManager[] trustAllCertificates() {
        TrustManager[] trustAllCerts = new TrustManager[0];
        try {
            trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        public X509Certificate[] getAcceptedIssuers() {
                            X509Certificate[] myTrustedAnchors = new X509Certificate[0];
                            Log.e(TAG, "getAcceptedIssuers");
                            return myTrustedAnchors;
                        }

                        @Override
                        public void checkClientTrusted(X509Certificate[] certs, String authType) {
                            Log.e(TAG, "checkClientTrusted");
                        }

                        @Override
                        public void checkServerTrusted(X509Certificate[] certs, String authType) {
                            Log.e(TAG, "checkServerTrusted");
                        }
                    }
            };
        } catch (Exception e) {
            e.printStackTrace();
        }
        return trustAllCerts;
    }


    synchronized static void saveChatThread(ChatProduct chatProduct) {
        boolean doesExist = false;
        try{
            String UNIQUE_ID = chatProduct.getUniqueId();
            PeepsChat peepsChat = getDataFromPreferences(kChats,PeepsChat.class);
            peepsChat = (peepsChat==null)?new PeepsChat():peepsChat;
            CopyOnWriteArrayList<ChatProduct> chatProductList = peepsChat.getChatProductList();
            if(chatProductList.isEmpty())
                chatProductList.add(chatProduct);
            else {
                for (ChatProduct chatPro: chatProductList) {
                    String uniqueId = chatPro.getUniqueId();
                    if(uniqueId.equals(UNIQUE_ID)){
                        doesExist = true;
                        chatPro.getMessageList().add(chatProduct.getLastMessage());
                    }
                }
                if(!doesExist)
                    chatProductList.add(chatProduct);
            }
            saveDataIntoPreferences(peepsChat,kChats);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public synchronized static CopyOnWriteArrayList<ChatProduct> getChatThreads(){
        PeepsChat peepsChat = getDataFromPreferences(kChats,PeepsChat.class);
        peepsChat = (peepsChat==null)?new PeepsChat():peepsChat;

        return peepsChat.getChatProductList();
    }


    public synchronized static <T> T getDataFromPreferences(String key, Type typeOfT)   {
        SharedPreferences sharedPreferences = ApplicationManager.getContext().getSharedPreferences(kChatPreferences, Context.MODE_PRIVATE);
        T dataObject = null;
        try {
            Gson gson = new Gson();
            String jsonString = sharedPreferences.getString(key, Constants.kEmptyString);
            dataObject = gson.fromJson(jsonString, typeOfT);
        }catch (Exception e)    {
            e.printStackTrace();
        }
        return dataObject;
    }

    public synchronized static void saveDataIntoPreferences(Object dataObject, String key)   {
        Gson gson = new Gson();
        String json = gson.toJson(dataObject);
        SharedPreferences sharedPreferences = ApplicationManager.getContext().getSharedPreferences(kChatPreferences, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.putString(key, json);
        prefsEditor.commit();
    }


}
