package com.app.inn.peepsin.Managers.UploadManager;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.app.inn.peepsin.Constants.Blocks.Block;
import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Libraries.DispatchQueue.DispatchQueue;
import com.app.inn.peepsin.UI.helpers.Utils;

import java.io.File;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import static com.app.inn.peepsin.Constants.Constants.kMessageInternalInconsistency;
import static com.app.inn.peepsin.Managers.BaseManager.ApplicationManager.getContext;

/**
 * Created by akaMahesh on 6/7/17.
 * copyright to : Innverse Technologies
 */

public class DownloadManager implements UploadHelper {

    private final static String TAG = DownloadManager.class.getSimpleName();
    private static DownloadManager _DownloadManager;
    private CopyOnWriteArrayList<HashMap<String, Object>> hashMapList = new CopyOnWriteArrayList<>();

    public static DownloadManager downloadManager() {
        if (_DownloadManager == null)
            _DownloadManager = new DownloadManager();
        return _DownloadManager;
    }

    private DownloadManager() {

    }


    public void download(List<String> imageUrl, Block.Completion<List<String>> completion) {
        DispatchQueue.global(DispatchQueue.QoS.userInteractive).async(() -> {
            CopyOnWriteArrayList<String> pathList = new CopyOnWriteArrayList<>();
            for (int index = 0; index < imageUrl.size(); index++) {
                String imageURL = imageUrl.get(index);
                File imageFile = getDownloadedImage(imageURL);
                String imageFilePath = (imageFile==null)?"":imageFile.getAbsolutePath();
                pathList.add(imageFilePath);
                if(imageUrl.size()==pathList.size()){
                    GenricResponse<List<String>> genricResponse = new GenricResponse<>(pathList);
                    completion.iCompletion(Constants.Status.success,genricResponse);
                }







                /*downloadImage(imageUrl.get(index), index, (Constants.Status iStatus, GenricResponse<HashMap<Integer,String>> genricResponse) -> {
                    ++counter;
                    HashMap<Integer,String> stringHashMap = genricResponse.getObject();
                    hashMaps.putAll(stringHashMap);
                    if(imageUrl.size()==counter){
                        for (Integer keys:hashMaps.keySet()) {
                            pathList.add(hashMaps.get(keys));
                        }
                        GenricResponse<List<String>> listGenricResponse = new GenricResponse<>(pathList);
                        completion.iCompletion(Constants.Status.success, listGenricResponse);
                    }

                }, (Constants.Status failure, String message) -> {
                    ++counter;
                    completion.iCompletion(Constants.Status.fail, null);
                });*/
            }
        });
    }

    private File getDownloadedImage(String imageURL) {
        File imageFile = null;
        try {
            Bitmap bitmap;
            URL imageurl = new URL(imageURL);
            bitmap = BitmapFactory.decodeStream(imageurl.openStream());
            String filename = Utils.generateTimeStamp() + ".JPEG";
            imageFile = Utils.getFile(bitmap, getContext(), filename);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return imageFile;
    }

    private static Map<String, Integer> sortByComparator(Map<String, Integer> unsortMap, final boolean order) {
        List<Map.Entry<String, Integer>> list = new LinkedList<>(unsortMap.entrySet());
        // Sorting the list based on values
        Collections.sort(list, (o1, o2) -> {
            if (order) {
                return o1.getValue().compareTo(o2.getValue());
            } else {
                return o2.getValue().compareTo(o1.getValue());
            }
        });

        // Maintaining insertion order with the help of LinkedList
        Map<String, Integer> sortedMap = new LinkedHashMap<>();
        for (Map.Entry<String, Integer> entry : list) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }
        return sortedMap;
    }


    private void downloadImage(String imageurl, Integer index, Block.Success<HashMap<Integer, String>> success, Block.Failure failure) {
        DispatchQueue.global(DispatchQueue.QoS.userInteractive).async(() -> {
            try {
                Bitmap bitmap;
                URL imageURL = new URL(imageurl);
                bitmap = BitmapFactory.decodeStream(imageURL.openStream());
                String filename = Utils.generateTimeStamp() + ".JPEG";
                File file = Utils.getFile(bitmap, getContext(), filename);
                String filePath = "";
                if (file != null)
                    filePath = file.getAbsolutePath();
                HashMap<Integer, String> hashMap = new HashMap<>();
                hashMap.put(index, filePath);

                GenricResponse<HashMap<Integer, String>> genricResponse = new GenricResponse<>(hashMap);
                success.iSuccess(Constants.Status.success, genricResponse);
            } catch (Exception e) {
                e.printStackTrace();
                failure.iFailure(Constants.Status.fail, kMessageInternalInconsistency);
            }
        });
    }


}
