package com.app.inn.peepsin.UI.fragments.SellStuffModule;

import static com.app.inn.peepsin.Constants.Constants.kProductId;
import static com.app.inn.peepsin.Constants.Constants.kProductList;
import static com.app.inn.peepsin.Constants.Constants.kProductStatus;
import static com.app.inn.peepsin.Constants.Constants.kShopId;
import static com.app.inn.peepsin.Constants.Constants.kType;

import android.app.ProgressDialog;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Constants.Constants.ProductStatus;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.SellingProduct;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.helpers.DividerItemRecyclerDecoration;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import javax.annotation.Nonnull;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by dhruv on 16/8/17.
 */

public class ModifySellingStuffFragment extends Fragment {

  private String TAG = getTag();
  @BindView(R.id.recycler_view_feeds)
  RecyclerView recyclerViewFeeds;
  @BindView(R.id.tv_title)
  TextView tvTitle;
  @BindView(R.id.empty_view)
  View emptyView;
  @BindView(R.id.tv_empty_title)
  TextView tvEmptyTitle;
  @BindView(R.id.tv_empty_message)
  TextView tvEmptyMessage;
  @BindView(R.id.submit_btn)
  Button submitBtn;

  private ProgressDialog progressDialog;
  private CopyOnWriteArrayList<SellingProduct> productList;
  private SellingStuffAdapter stuffAdapter;
  private Integer shopId;
  private Integer type;
  private Constants.ProductStatus productStatus = Constants.ProductStatus.active;


  public static ModifySellingStuffFragment newInstance(int shopId,
      CopyOnWriteArrayList<SellingProduct> sellingList, int productStatus) {
    ModifySellingStuffFragment fragment = new ModifySellingStuffFragment();
    Bundle bundle = new Bundle();
    bundle.putSerializable(kType, productStatus);
    bundle.putInt(kShopId, shopId);
    bundle.putSerializable(kProductList, sellingList);
    fragment.setArguments(bundle);
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    progressDialog = Utils.generateProgressDialog(getContext(), true);
    productList = new CopyOnWriteArrayList<>();
    Bundle bundle = getArguments();
    if (bundle != null) {
      type = bundle.getInt(kType);
      shopId = bundle.getInt(kShopId);
      productList = (CopyOnWriteArrayList<SellingProduct>) bundle.getSerializable(kProductList);
    }
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_modify_selling_stuff, container, false);
    ButterKnife.bind(this, view);
    tvEmptyTitle.setText(getString(R.string.empty_title_no_selling_stuff));
    tvEmptyMessage.setText(getString(R.string.empty_message_no_selling_stuff));
    if (type.equals(ProductStatus.draft.getValue())) {
      tvTitle.setText("Publish Products");
      submitBtn.setText(getString(R.string.text_publish));
      productStatus = Constants.ProductStatus.publish;
    } else if (type.equals(Constants.ProductStatus.active.getValue())) {
      tvTitle.setText("InActivate Products");
      submitBtn.setText("InActivate Products");
      productStatus = Constants.ProductStatus.inActive;
    } else if (type.equals(Constants.ProductStatus.inActive.getValue())) {
      tvTitle.setText("Activate Products");
      submitBtn.setText("Activate Products");
      productStatus = Constants.ProductStatus.active;
    }

    stuffAdapter = new SellingStuffAdapter(productList);
    recyclerViewFeeds.setHasFixedSize(true);
    recyclerViewFeeds.setLayoutManager(new LinearLayoutManager(getActivity()));
    recyclerViewFeeds.setAdapter(stuffAdapter);
    recyclerViewFeeds.addItemDecoration(
        new DividerItemRecyclerDecoration(getActivity(), R.drawable.canvas_recycler_divider));
    checkEmptyScreen();
    return view;
  }

  @OnClick(R.id.btn_back)
  void onBack() {
    getFragmentManager().popBackStack();
  }

  @OnClick(R.id.submit_btn)
  void onSubmit() {
    int size = 0;
    List<SellingProduct> selectedProducts = new ArrayList<>();
    JSONArray list = new JSONArray();
    for (SellingProduct sellingProduct : productList) {
      if (sellingProduct.isSelected()) {
        try {
          JSONObject obj = new JSONObject();
          obj.put(kProductId, sellingProduct.getId());
          obj.put(kProductStatus, 1);
          list.put(obj);
          selectedProducts.add(sellingProduct);
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    }
    if (selectedProducts.isEmpty()) {
      Toaster.kalaToast("Please Select Products to proceed");
      return;
    }
    setProductStatus(shopId, selectedProducts, productStatus);
  }

  void setProductStatus(int shopId, List<SellingProduct> sellingProducts,
      Constants.ProductStatus productStatus) {
    showProgress(true);
    ModelManager.modelManager()
        .setProductStatus(shopId, Utils.generateJsonModifyList(sellingProducts, productStatus),
            (Constants.Status iStatus, GenricResponse<HashMap<String, Object>> genricResponse) -> {
              HashMap<String, Object> hashMap = genricResponse.getObject();
              productList.removeAll(sellingProducts);
              stuffAdapter.notifyDataSetChanged();
              showProgress(false);
              checkEmptyScreen();
            }, (Constants.Status iStatus, String message) -> {
              showProgress(false);
              checkEmptyScreen();
              Log.e(getClass().getSimpleName(), message);
            });
  }


  private void checkEmptyScreen() {
    if (stuffAdapter.getItemCount() > 0) {
      emptyView.setVisibility(View.GONE);
    } else {
      emptyView.setVisibility(View.VISIBLE);
    }
  }

  private void showProgress(boolean b) {
    if (b) {
      progressDialog.show();
    } else {
      if (progressDialog != null) {
        progressDialog.cancel();
      }
    }
  }

  public class SellingStuffAdapter extends RecyclerView.Adapter<SellingStuffAdapter.StuffHolder> {

    private List<SellingProduct> feedList;

    private SellingStuffAdapter(List<SellingProduct> items) {
      this.feedList = items;
    }

    @Override
    public StuffHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      View layoutView = LayoutInflater.from(parent.getContext())
          .inflate(R.layout.row_stuff_selection_layout, parent, false);
      return new StuffHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(SellingStuffAdapter.StuffHolder holder, int position) {
      SellingProduct item = feedList.get(position);
      holder.bindContent(item);
      String name = item.getName();
      String shareCount = String.valueOf(item.getShareCount());
      String favCount = String.valueOf(item.getFavouriteCount());
      String price = getString(R.string.Rs) + " " + item.getPrice();
      String postingDate = getString(R.string.postedOn) + " " + item.getPostingDate();
      String thumbnailURL = item.getImageThumbUrl();
      String skui = "SKU No. " + item.getSKUNumber();
      String rating = String.valueOf(item.getRating());
      String discount = getString(R.string.no_offer);
      if (!item.getDiscountTagLine().isEmpty()) {
        discount = item.getDiscountTagLine();
      }
      String quantity =
          "(" + getString(R.string.in_stock) + String.valueOf(item.getQuantity()) + ")";

      if (item.isSelected()) {
        holder.ivSelection.setVisibility(View.VISIBLE);
      } else {
        holder.ivSelection.setVisibility(View.INVISIBLE);
      }

      holder.tvSKUI.setText(skui);
      holder.textViewItemName.setText(name);
      holder.textViewShareCount.setText(shareCount);
      holder.textViewFavCount.setText(favCount);
      holder.textViewPrice.setText(price);
      holder.tvDiscount.setText(discount);
      holder.tvQuantity.setText(quantity);
      holder.textViewTime.setText(postingDate);
      holder.tvStarRating.setText(rating);
      if (!thumbnailURL.isEmpty()) {
        Picasso.with(getContext())
            .load(thumbnailURL)
            .placeholder(R.drawable.img_product_placeholder)
            .into(holder.imageViewItem);
      }
      setProductStatus(holder,item.getProductStatus());
    }

    private void setProductStatus(StuffHolder holder, @Nonnull Integer productStatus) {
      switch (productStatus) {
        case 2:
          holder.tvStatus.setText("Active");
          holder.tvStatus.setBackground(holder.positiveFlag);
          break;
        case 3:
          holder.tvStatus.setText("Inactive");
          holder.tvStatus.setBackground(holder.negativeFlag);
          break;
        case 4:
          holder.tvStatus.setText("In-process");
          holder.tvStatus.setBackground(holder.positiveFlag);
          break;
        case 6:
          holder.tvStatus.setText("Draft");
          holder.tvStatus.setBackground(holder.positiveFlag);
          break;
        default:
          holder.tvStatus.setText("Un-identified");
          holder.tvStatus.setBackground(holder.negativeFlag);
      }
    }


    @Override
    public int getItemCount() {
      return feedList.size();
    }

    class StuffHolder extends RecyclerView.ViewHolder {

      SellingProduct item;
      @BindView(R.id.iv_item)
      ImageView imageViewItem;
      @BindView(R.id.tv_name)
      TextView textViewItemName;
      @BindView(R.id.tv_price)
      TextView textViewPrice;
      @BindView(R.id.tv_time)
      TextView textViewTime;
      @BindView(R.id.tv_offer)
      TextView tvDiscount;
      @BindView(R.id.tv_quantity)
      TextView tvQuantity;
      @BindView(R.id.tv_shared_count)
      TextView textViewShareCount;
      @BindView(R.id.tv_favorite_count)
      TextView textViewFavCount;
      @BindView(R.id.tv_text_star)
      TextView tvStarRating;
      @BindView(R.id.tv_skui)
      TextView tvSKUI;
      @BindView(R.id.iv_selection)
      ImageView ivSelection;
      @BindView(R.id.tv_status)
      TextView tvStatus;
      @BindDrawable(R.drawable.img_green_flag)
      Drawable positiveFlag;
      @BindDrawable(R.drawable.img_red_flag)
      Drawable negativeFlag;

      StuffHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
      }

      @OnClick(R.id.item_view)
      void onRowSelected(View view) {
        if (item.isSelected()) {
          item.setSelected(false);
          ivSelection.setVisibility(View.INVISIBLE);
        } else {
          item.setSelected(true);
          ivSelection.setVisibility(View.VISIBLE);
        }
      }

      private void bindContent(SellingProduct item) {
        this.item = item;
      }
    }
  }

}
