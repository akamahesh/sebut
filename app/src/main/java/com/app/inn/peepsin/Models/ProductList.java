package com.app.inn.peepsin.Models;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by akamahesh on 15/5/17.
 */

public class ProductList extends BaseModel implements Serializable {
    private Integer productId;
    private String productName;
    private String price;
    private String sellingPrice;
    private String netPrice;
    private String discountTagLine;
    private Integer productQuantity;
    private String productCategory;
    private String coverImageThumbnailURL;
    private String shopName;


    public ProductList(JSONObject jsonResponse) {
        this.productId                = getValue(jsonResponse, kProductId,              Integer.class);
        this.productName              = getValue(jsonResponse, kFProductName,           String.class);
        this.price                    = getValue(jsonResponse, kPrice,                  String.class);
        this.sellingPrice             = getValue(jsonResponse, kSellingPrice,           String.class);
        this.netPrice                 = getValue(jsonResponse, kNetPrice,               String.class);
        this.discountTagLine          = getValue(jsonResponse, kDiscountTagLine,        String.class);
        this.productQuantity          = getValue(jsonResponse, kProductQuantity,        Integer.class);
        this.productCategory          = getValue(jsonResponse, kProductCategory,        String.class);
        this.coverImageThumbnailURL   = getValue(jsonResponse, kCoverImageThumbnailURL, String.class);
        this.shopName                 = getValue(jsonResponse, kShopName,               String.class);
    }


    public Integer getProductId() {
        return productId;
    }

    public String getProductName() {
        return productName;
    }

    public String getPrice() {
        return price;
    }

    public String getSellingPrice() {
        return sellingPrice;
    }

    public String getNetPrice() {
        return netPrice;
    }

    public String getDiscountTagLine() {
        return discountTagLine;
    }

    public Integer getProductQuantity() {
        return productQuantity;
    }

    public String getProductCategory() {
        return productCategory;
    }

    public String getCoverImageThumbnailURL() {
        return coverImageThumbnailURL;
    }

  public String getShopName() {
    return shopName;
  }

  public void setShopName(String shopName) {
    this.shopName = shopName;
  }
}
