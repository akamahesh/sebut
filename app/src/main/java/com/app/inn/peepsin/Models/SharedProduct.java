package com.app.inn.peepsin.Models;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dhruv on 27/7/17.
 */

public class SharedProduct extends BaseModel implements Serializable {

    private Integer id;
    private Integer shopId;
    private String shopName;
    private String sharedBy;
    private String name;
    private String description;
    private String specification;
    private String shopBannerUrl;
    private String shopLogoUrl;
    private String profilePicUrl;
    private String imageThumbUrl;
    private String imageUrl;
    private Integer quantity;
    private List<String> imageUrlList;
    private List<String> imageThumbUrlList;
    private Boolean isFavourite;
    private String postingDate;
    private Integer categoryId;
    private String category;
    private String subcategory;
    private String price;
    private String sellingPrice;
    private String netPrice;
    private Address address;
    private Seller seller;
    private Integer totalRatingCount;
    private Integer rating;
    private String discountTagline;
    private String skuNumber;
    private Integer connectionLevel;

    public SharedProduct(JSONObject jsonResponse) {
        this.id                 = getValue(jsonResponse, kProductId,                Integer.class);
        this.shopId             = getValue(jsonResponse, kShopId,                   Integer.class);
        this.shopName           = getValue(jsonResponse, kShopName,                 String.class);
        this.sharedBy           = getValue(jsonResponse, kSharedBy,                 String.class);
        this.name               = getValue(jsonResponse, kName,                     String.class);
        this.description        = getValue(jsonResponse, kProductDesc,              String.class);
        this.specification      = getValue(jsonResponse, kSpecification,            String.class);
        this.imageThumbUrl      = getValue(jsonResponse, kProductThumbUrl,          String.class);
        this.imageUrl           = getValue(jsonResponse, kProductImageUrl,          String.class);
        try {
            this.imageUrlList       = handleImageList(getValue(jsonResponse, kProductImageUrlList,      JSONArray.class));
        }catch (ClassCastException e){
            e.printStackTrace();
        }
        try {
            this.imageThumbUrlList  = handleImageList(getValue(jsonResponse, kProductThumbUrlList,      JSONArray.class));
        }catch (ClassCastException e){
            e.printStackTrace();
        }
        this.isFavourite        = getValue(jsonResponse, kIsFavorite,               Boolean.class);
        this.postingDate        = getValue(jsonResponse, kPostingDate,              String.class);
        this.categoryId         = getValue(jsonResponse, kCategoryId,               Integer.class);
        this.category           = getValue(jsonResponse, kProductCategory,          String.class);
        this.subcategory        = getValue(jsonResponse, kProductSubCategory,       String.class);
        this.price              = getValue(jsonResponse, kProductPrice,             String.class);
        try {
            this.quantity           = getValue(jsonResponse, kQuantity ,     Integer.class);
        }catch (Exception e){
            Log.e("Error : ",e.getMessage());
        }
        this.sellingPrice       = getValue(jsonResponse, kProductSellPrice,         String.class);
        this.netPrice       = getValue(jsonResponse, kNetPrice,                 String.class);
        try { //to handle @ClassCastException when addressId is emptyString, not to be removed
            this.address        = new Address(getValue(jsonResponse, kProductAddress,   JSONObject.class));
        }catch (Exception e){
            Log.e("Error : ",e.getMessage());
        }
        try { //to handle @ClassCastException when sellerId is emptyString, not to be removed
            this.seller         = new Seller(getValue(jsonResponse, kSellerDetails,     JSONObject.class));
        }catch (Exception e){
            Log.e("Error : ",e.getMessage());
        }
        this.profilePicUrl      = getValue(jsonResponse, kProfilePicUrl,            String.class);
        this.shopBannerUrl      = getValue(jsonResponse, kShopBannerImageURl,       String.class);
        this.shopLogoUrl        = getValue(jsonResponse, kShopLogoImageURL,         String.class);
        this.discountTagline    = getValue(jsonResponse, kDiscountTagLine,          String.class);
        this.skuNumber          = getValue(jsonResponse, kSKUNumber,                String.class);
        this.totalRatingCount   = getValue(jsonResponse, kTotalRatingCount,         Integer.class);
        this.rating             = getValue(jsonResponse, kRating,                   Integer.class);
        this.connectionLevel    = getValue(jsonResponse, kConnectionLevel,          Integer.class);
    }

    private List<String> handleImageList(JSONArray jsonArray) {
        List<String> imageList= new ArrayList<>();
        for(int i = 0; i<jsonArray.length();i++){
            try {
                imageList.add(jsonArray.get(i).toString());

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return imageList;

    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCategory() {
        return category;
    }

    public Integer getShopId() {
        return shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public Integer getConnectionLevel() {
        return connectionLevel;
    }

    public String getSharedBy() {
        return sharedBy;
    }

    public String getDiscountTagline() {
        return discountTagline;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getSkuNumber() {
        return skuNumber;
    }

    public String getSubcategory() {
        return subcategory;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public Integer getTotalRatingCount() {
        return totalRatingCount;
    }

    public Integer getRating() {
        return rating;
    }

    public Address getAddress() {
        return address;
    }

    public Seller getSeller() {
        return seller;
    }

    public String getFullName(){
        return getSeller().getFirstName()+" "+getSeller().getLastName();
    }

    public String getDescription() {
        return description;
    }

    public String getSpecification() {
        return specification;
    }

    public String getImageThumbUrl() {
        return imageThumbUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public List<String> getImageUrlList() {
        return imageUrlList;
    }

    public List<String> getImageThumbUrlList() {
        return imageThumbUrlList;
    }

    public Boolean getFavourite() {
        return isFavourite;
    }

    public void setFavourite(Boolean favourite) {
        isFavourite = favourite;
    }

    public String getPostingDate() {
        return postingDate;
    }

    public String getPrice() {
        return price;
    }

    public String getSellingPrice() {
        return sellingPrice;
    }

    public String getNetPrice() {
        return netPrice;
    }

    public String getShopBannerUrl() {
        return shopBannerUrl;
    }

    public String getShopLogoUrl() {
        return shopLogoUrl;
    }

    public String getProfilePicUrl() {
        return profilePicUrl;
    }
}
