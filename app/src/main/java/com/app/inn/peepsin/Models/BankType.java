package com.app.inn.peepsin.Models;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by dhruv on 17/8/17.
 */

public class BankType extends BaseModel implements Serializable {

    private Integer id;
    private String name;

    public BankType(JSONObject jsonResponse) {
        this.id   = getValue(jsonResponse, kbankId, Integer.class);
        this.name = getValue(jsonResponse, kBankName, String.class);
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

}
