package com.app.inn.peepsin.UI.adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.app.inn.peepsin.Models.OrderStatus;
import com.app.inn.peepsin.Models.Orientation;
import com.app.inn.peepsin.Models.TimeLineModel;
import com.app.inn.peepsin.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ActivityOrderHistoryAdapter extends BaseExpandableListAdapter {


    RecyclerView mRecyclerView;
    TimeLineAdapter mTimeLineAdapter;
    List<TimeLineModel> mDataList;
    Orientation mOrientation;
    boolean mWithLinePadding;

    private Context context;
    private List<String> expandableListTitle;
    private HashMap<String, List<String>> expandableListDetail;

    public ActivityOrderHistoryAdapter(Context context, List<String> expandableListTitle,
                                       HashMap<String, List<String>> expandableListDetail) {
        this.context = context;
        this.expandableListTitle = expandableListTitle;
        this.expandableListDetail = expandableListDetail;
    }

    @Override
    public Object getChild(int listPosition, int expandedListPosition) {
        return this.expandableListDetail.get(this.expandableListTitle.get(listPosition))
                .get(expandedListPosition);
    }

    @Override
    public long getChildId(int listPosition, int expandedListPosition) {
        return expandedListPosition;
    }

    @Override
    public View getChildView(int listPosition, final int expandedListPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final String expandedListText = (String) getChild(listPosition, expandedListPosition);
        mDataList = new ArrayList<>();
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.order_history_expandable_layout, null);
        }
        TextView expandedListTextView = (TextView) convertView
                .findViewById(R.id.tv_workdetaill);
        expandedListTextView.setText(expandedListText);
        mRecyclerView = (RecyclerView) convertView.findViewById(R.id.recyclerView);
        mRecyclerView.setLayoutManager(getLinearLayoutManager());
        mRecyclerView.setHasFixedSize(true);

        initView();
        return convertView;
    }

    @Override
    public int getChildrenCount(int listPosition) {
        return this.expandableListDetail.get(this.expandableListTitle.get(listPosition))
                .size();
    }

    @Override
    public Object getGroup(int listPosition) {
        return this.expandableListTitle.get(listPosition);
    }

    @Override
    public int getGroupCount() {
        return this.expandableListTitle.size();
    }

    @Override
    public long getGroupId(int listPosition) {
        return listPosition;
    }

    @Override
    public View getGroupView(int listPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String listTitle = (String) getGroup(listPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.order_history_layout, null);
        }
        TextView listTitleTextView = (TextView) convertView
                .findViewById(R.id.tv_workdetail);
      //  listTitleTextView.setTypeface(null, Typeface.BOLD);
        listTitleTextView.setText(listTitle);
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    private void initView() {
        setDataListItems();
        mTimeLineAdapter = new TimeLineAdapter(mDataList, mOrientation, mWithLinePadding);
        mRecyclerView.setAdapter(mTimeLineAdapter);
    }

    private void setDataListItems(){
        mDataList.add(new TimeLineModel("Ordered",  OrderStatus.COMPLETED));
        mDataList.add(new TimeLineModel("Canceled",  OrderStatus.INACTIVE));
        mDataList.add(new TimeLineModel("Packaged",  OrderStatus.INACTIVE));
        mDataList.add(new TimeLineModel("Shipped",  OrderStatus.INACTIVE));

    }

    private LinearLayoutManager getLinearLayoutManager() {
        if (mOrientation == Orientation.HORIZONTAL) {
            return new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        } else {
            return new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        }
    }


    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;
    }
}