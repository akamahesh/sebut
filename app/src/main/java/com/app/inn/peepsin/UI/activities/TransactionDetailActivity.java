package com.app.inn.peepsin.UI.activities;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.BaseManager.ExceptionHandler;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.Address;
import com.app.inn.peepsin.Models.CheckSumDetails;
import com.app.inn.peepsin.Models.CurrentUser;
import com.app.inn.peepsin.Models.FinalBillSummary;
import com.app.inn.peepsin.Models.FinalProductsRecords;
import com.app.inn.peepsin.Models.PayUmoneyDetails;
import com.app.inn.peepsin.Models.ProductList;
import com.app.inn.peepsin.Models.ShoppingCart;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.adapters.NewTransactionDetailAdapter;
import com.app.inn.peepsin.UI.adapters.TransactionDetailAdapter;
import com.app.inn.peepsin.UI.helpers.DividerItemRecyclerDecoration;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;
import com.sasidhar.smaps.payumoney.MakePaymentActivity;
import com.sasidhar.smaps.payumoney.PayUMoney_Constants;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kAddress;
import static com.app.inn.peepsin.Constants.Constants.kAddressId;
import static com.app.inn.peepsin.Constants.Constants.kCity;
import static com.app.inn.peepsin.Constants.Constants.kCountry;
import static com.app.inn.peepsin.Constants.Constants.kEmail;
import static com.app.inn.peepsin.Constants.Constants.kFullName;
import static com.app.inn.peepsin.Constants.Constants.kIsPrimary;
import static com.app.inn.peepsin.Constants.Constants.kLatitude;
import static com.app.inn.peepsin.Constants.Constants.kLongitude;
import static com.app.inn.peepsin.Constants.Constants.kPhone;
import static com.app.inn.peepsin.Constants.Constants.kState;
import static com.app.inn.peepsin.Constants.Constants.kStreetAddress;
import static com.app.inn.peepsin.Constants.Constants.kZipcode;

public class TransactionDetailActivity extends AppCompatActivity {
    private String TAG = getClass().getName();

    @BindView(R.id.tv_title)
    TextView tvTitle;
   /* @BindView(R.id.expandableListView)
    ExpandableListView expandableListView;*/
    @BindView(R.id.tv_total_amount)
    TextView tvTotalAmount;
    @BindView(R.id.tv_order_id)
    TextView tvOrderId;
    @BindView(R.id.tv_price_after_code)
    TextView tvAfterAmount;
    @BindView(R.id.tv_price_before_code)
    TextView tvBeforeAmount;
    @BindView(R.id.tv_discount)
    TextView tvDiscount;
    @BindView(R.id.edt_promo_code)
    EditText edtPromoCode;
    @BindView(R.id.promo_code_tag)
    TextView tvPromoCodeTag;
    @BindView(R.id.payment_amount_layout)
    View amountLayout;
    @BindView(R.id.btn_proceed)
    Button btnProceed;
    @BindView(R.id.btn_cancel)
    Button btnCancel;
    @BindView(R.id.ib_back)
    ImageButton btnBack;
    @BindView(R.id.tv_del_address)
    TextView tvDelAddress;


    @BindView(R.id.recycler_transaction)
    RecyclerView rvTransaction;

    private Address address;
    private CurrentUser currentUser;
    private ProgressDialog progressDialog;

    private FinalBillSummary finalBillSummary;
    CheckSumDetails checkSumDetails;
    Integer orderId=0;
    Integer isValid=0;
    private HashMap<String, String> params = new HashMap<>();
    PayUmoneyDetails payUmoneyDetails;

    public static Intent getIntent(Context context, Address address) {
        Intent intent =  new Intent(context, TransactionDetailActivity.class);
        intent.putExtra(kAddress,address);
        return intent;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_detail);
        ButterKnife.bind(this);
        tvTitle.setText(getString(R.string.title_transaction));
        currentUser = ModelManager.modelManager().getCurrentUser();
        progressDialog = Utils.generateProgressDialog(this,false);
        //Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        address = (Address) getIntent().getSerializableExtra(kAddress);
        amountLayout.setVisibility(View.GONE);

        HashMap<String, Object> userMap = getParameters();
        System.out.println("Array Values...."+userMap.toString());
        loadBillSummary("", userMap);
        if(address!=null) {
            String addressText = "Delivery Address: " + address.getStreetAddress()+ address.getCity()
                  + address.getState()+ address.getCountry();
            tvDelAddress.setText(addressText);
        }
    }
    public HashMap<String,Object> getParameters() {
        HashMap<String,Object> parameters = new HashMap<>();
        Address selectedAddress = (address==null)?currentUser.getPrimaryAddress():address;
        parameters.put(kAddressId,      selectedAddress.getAddressId());
        parameters.put(kFullName,       selectedAddress.getFullName());
        parameters.put(kPhone,          selectedAddress.getPhone());
        parameters.put(kStreetAddress,  selectedAddress.getStreetAddress());
        parameters.put(kCity,           selectedAddress.getCity());
        parameters.put(kState,          selectedAddress.getState());
        parameters.put(kCountry,        selectedAddress.getCountry());
        parameters.put(kZipcode,        selectedAddress.getZipCode());
        parameters.put(kIsPrimary,      selectedAddress.getPrimary());
        parameters.put(kLatitude,       selectedAddress.getLatitude());
        parameters.put(kLongitude,      selectedAddress.getLongitude());
        parameters.put(kEmail,          currentUser.getEmail().getContactId());
        return parameters;
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadShoppingCart();
    }

    public void loadShoppingCart() {
        ModelManager.modelManager().getShoppingCart((Constants.Status iStatus, GenricResponse<ShoppingCart> genericResponse) -> {
            ShoppingCart cart = genericResponse.getObject();
        }, (Constants.Status iStatus, String message) -> {
            Log.e(TAG, message);
        });
    }

    public void loadBillSummary(String promoCode, HashMap<String,Object> addressMap) {
        showProgress(true);
        ModelManager.modelManager().getFinalBillSummary(promoCode,addressMap,(Constants.Status iStatus, GenricResponse<FinalBillSummary> genericResponse) -> {
            showProgress(false);
            finalBillSummary = genericResponse.getObject();
            setOrderPriceView(finalBillSummary);
            setSummaryList(finalBillSummary);
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Log.e(TAG,message);
        });
    }

    private void setOrderPriceView(FinalBillSummary summary){
        orderId = summary.getOrderId();
        String totalAmount  = getString(R.string.Rs)+summary.getFinalAmountAfterPromoCode();
        String afterAmount  = getString(R.string.Rs)+summary.getFinalAmountAfterPromoCode();
        String beforeAmount = getString(R.string.Rs)+summary.getTotalCartPriceBeforePromoCode();
        String discountPrice = getString(R.string.Rs)+"0/-";
        if(!summary.getDiscountedPrice().isEmpty())
            discountPrice = getString(R.string.Rs)+summary.getDiscountedPrice();
        tvOrderId.setText(String.valueOf(orderId));
        tvTotalAmount.setText(totalAmount);
        tvAfterAmount.setText(afterAmount);
        tvBeforeAmount.setText(beforeAmount);
        tvDiscount.setText(discountPrice);

        tvPromoCodeTag.setText(summary.getPromoCodeTagLine());
    }

    private void setSummaryList(FinalBillSummary finalBillSummary){
        CopyOnWriteArrayList<ProductList> productLists= new CopyOnWriteArrayList<>() ;
        if(finalBillSummary.getShopRecords()!=null){

            for(int i=0;i<finalBillSummary.getShopRecords().size();i++){

                FinalProductsRecords shop = finalBillSummary.getShopRecords().get(i);
                for(int j=0;j<shop.getProductRecords().size();j++) {
                    ProductList product = shop.getProductRecords().get(j);
                    productLists.add(product);
                }
            }
            NewTransactionDetailAdapter transactionDetailAdapter= new NewTransactionDetailAdapter(this,productLists);
           /* TransactionDetailAdapter expandableListAdapter = new TransactionDetailAdapter(this,finalBillSummary);
            expandableListView.setAdapter(expandableListAdapter);*/
            rvTransaction.setLayoutManager(new LinearLayoutManager(this));
            rvTransaction.setHasFixedSize(true);
            rvTransaction.addItemDecoration(new DividerItemRecyclerDecoration(this,R.drawable.canvas_recycler_divider));
            rvTransaction.setAdapter(transactionDetailAdapter);
        }
    }

    @OnClick(R.id.ib_back)
    public void onCancel(){
        ModelManager.modelManager().cancelOrderBeforePayment(orderId,(Constants.Status iStatus) -> {
            getCart();
        }, (Constants.Status iStatus, String message) -> Log.e(TAG, message));
        finish();
    }

    public void getCart(){
        ModelManager.modelManager().getShoppingCart((Constants.Status iiStatus, GenricResponse<ShoppingCart> genericResponse) -> {
        }, (Constants.Status iiStatus, String message) -> Log.e(TAG, message));
    }

    @OnClick(R.id.btn_proceed)
    void getFavorites() {
        openBottomSheetMenu();
    }

    @OnClick(R.id.btn_verify_code)
    void validateCode(){
        if(edtPromoCode.getText().toString().isEmpty()){
            Toaster.toast("Please enter the promocode");
        }else{
            validatePromoCode(edtPromoCode.getText().toString(), orderId);
        }
    }
    private void openBottomSheetMenu() {
        View view = TransactionDetailActivity.this.getLayoutInflater().inflate(R.layout.bottom_sheet_transcation_details, null);
        TextView tvPayTM = (TextView) view.findViewById(R.id.tv_paytm);
        TextView tvPayU = (TextView) view.findViewById(R.id.tv_pay_u_money);
        TextView tv_COD =(TextView)view.findViewById(R.id.tv_cah_on_delivery);

        final Dialog mBottomSheetDialog = new Dialog(TransactionDetailActivity.this, R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.getWindow().setBackgroundDrawable(TransactionDetailActivity.this.getResources().getDrawable(R.drawable.canvas_dialog_background_inset));
        mBottomSheetDialog.show();

        tvPayTM.setOnClickListener(v -> {
            setPaymentType(orderId,1);
            generateCheckSumForPaytm(orderId);
            mBottomSheetDialog.dismiss();
        });
        tv_COD.setOnClickListener(v -> {
            mBottomSheetDialog.dismiss();
            setPaymentType(orderId,3);
            Intent in= new Intent(TransactionDetailActivity.this, OrderSummaryActivity.class);
            in.putExtra("OrderId",orderId);
            startActivity(in);
        });
        tvPayU.setOnClickListener(v -> {
            setPaymentType(orderId,2);
            mBottomSheetDialog.dismiss();

            generateHashKey(orderId);
           /* Intent in = new Intent(TransactionDetailActivity.this,PayUMoneyActivity.class);
            in.putExtra("OrderId",orderId);
            startActivity(in);*/
        });

        view.findViewById(R.id.tv_cancel).setOnClickListener(v -> mBottomSheetDialog.dismiss());
    }


    public void onStartTransaction(CheckSumDetails checksum) {
        String MID= checksum.geMID();
        Integer Order_Id= checksum.getOrderId();
        Integer cust_Id= checksum.getCustId();
        String  industry_type= checksum.getIndustryId();
        String chanle_id= checksum.getChannelId();
        Integer txn_amount=checksum.getTaxAmount();
        String checksumhash= checksum.getChecksumHash();
        String finalCkecksum="";
        if(checksumhash.contains("\"")){
            finalCkecksum=checksumhash.replace("\"","");
        }else{
            finalCkecksum=checksumhash;
        }

        String callback_url= checksum.getCallbackURL();
        String finalcallbackUrl="";
        if(callback_url.contains("\"")){
            finalcallbackUrl= callback_url.replace("\"","");
        }else{
            finalcallbackUrl=callback_url;
        }
        String website=checksum.getWebsite();
        /* for staging paytm integration*/
       // PaytmPGService Service = PaytmPGService.getStagingService();

        /* for live paytm integration*/
        PaytmPGService Service = PaytmPGService.getProductionService();
        //Kindly create complete Map and checksum on your server side and then put it here in paramMap.
        Map<String, String> paramMap = new HashMap<>();
        paramMap.put("MID" , MID);
        paramMap.put("ORDER_ID" , String.valueOf(Order_Id));
        paramMap.put("CUST_ID" , String.valueOf(cust_Id));
        paramMap.put("INDUSTRY_TYPE_ID" , industry_type);
        paramMap.put("CHANNEL_ID" , chanle_id);
        paramMap.put("TXN_AMOUNT" , String.valueOf(txn_amount));
        paramMap.put("CHECKSUMHASH" , finalCkecksum);
        paramMap.put("CALLBACK_URL" ,finalcallbackUrl);
        paramMap.put("WEBSITE" ,website);
        PaytmOrder Order = new PaytmOrder(paramMap);

        Service.initialize(Order, null);

        Service.startPaymentTransaction(this, true, true,
                new PaytmPaymentTransactionCallback() {

                    @Override
                    public void someUIErrorOccurred(String inErrorMessage) {
                        Log.d("LOG", "Payment Transaction Error: " + inErrorMessage);
                        Toast.makeText(getApplicationContext(), "Payment Transaction response "+inErrorMessage, Toast.LENGTH_LONG).show();
                        // Some UI Error Occurred in Payment Gateway Activity.
                        // // This may be due to initialization of views in
                        // Payment Gateway Activity or may be due to //
                        // initialization of webview. // Error Message details
                        // the error occurred.
                    }

                    @Override
                    public void onTransactionResponse(Bundle inResponse) {
                        Log.d("LOG", "Payment Transaction : " + inResponse);
                        Toast.makeText(getApplicationContext(), "Payment Transaction response "+inResponse.toString(), Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void networkNotAvailable() {
                        // If network is not
                        // available, then this
                        // method gets called.
                    }

                    @Override
                    public void clientAuthenticationFailed(String inErrorMessage) {
                        // This method gets called if client authentication
                        // failed. // Failure may be due to following reasons //
                        // 1. Server error or downtime. // 2. Server unable to
                        // generate checksum or checksum response is not in
                        // proper format. // 3. Server failed to authenticate
                        // that client. That is value of payt_STATUS is 2. //
                        // Error Message describes the reason for failure.
                        Log.d("LOG", "Payment Transaction clientAuthenticationFailed : " + inErrorMessage);
                        Toast.makeText(getApplicationContext(), "Payment Transaction clientAuthenticationFailed "+inErrorMessage, Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onErrorLoadingWebPage(int iniErrorCode, String inErrorMessage, String inFailingUrl) {
                        Log.d("LOG", "Payment Transaction onErrorLoadingWebPage : " + inErrorMessage);
                        Toast.makeText(getApplicationContext(), "Payment Transaction onErrorLoadingWebPage "+inErrorMessage, Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onBackPressedCancelTransaction() {
                        Log.d("LOG", "Payment Transaction Cancelled");
                        Toast.makeText(getBaseContext(), "Payment Transaction Cancelled ", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onTransactionCancel(String inErrorMessage, Bundle inResponse) {
                        Log.d("LOG", "Payment Transaction Failed " + inErrorMessage);
                        Toast.makeText(getBaseContext(), "Payment Transaction Failed ", Toast.LENGTH_LONG).show();
                    }

                });
    }

    public void generateCheckSumForPaytm(Integer orderId) {
        ModelManager.modelManager().generateChecksum(orderId,(Constants.Status iStatus, GenricResponse<CheckSumDetails > genericResponse) -> {
            checkSumDetails=genericResponse.getObject();
            onStartTransaction(checkSumDetails);
        }, (Constants.Status iStatus, String message) -> Log.e("CheckSum",message));
    }

    public void setPaymentType(Integer orderId,Integer paymentType) {
        ModelManager.modelManager().setPaymentType(orderId,paymentType,(Constants.Status iStatus, GenricResponse<Integer > genericResponse) -> {

        }, (Constants.Status iStatus, String message) -> Log.e("CheckSum",message));
    }

    public void validatePromoCode(String promocode,Integer orderId) {
        ModelManager.modelManager().validatePromoCode(promocode,orderId,(Constants.Status iStatus, GenricResponse<Integer > genericResponse) -> {
            isValid=genericResponse.getObject();
            if(isValid==1){
                amountLayout.setVisibility(View.VISIBLE);
                tvPromoCodeTag.setVisibility(View.VISIBLE);
                loadBillSummary(edtPromoCode.getText().toString(), getParameters());
            }else
                Toaster.customToast("Promo Code is not valid");
        }, (Constants.Status iStatus, String message) -> {
            Toaster.customToast(message);
            Log.e("PromoCode",message);
        });
    }

    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) progressDialog.cancel();
        }
    }

    public void generateHashKey(Integer orderId) {

        ModelManager.modelManager().generateHashCode(orderId,(Constants.Status iStatus, GenricResponse<PayUmoneyDetails> genericResponse) -> {

            payUmoneyDetails=genericResponse.getObject();
            init(payUmoneyDetails);

        }, (Constants.Status iStatus, String message) -> {
            //Log.e(Tag,message);
        });
    }

    private double getAmount(String amountValue) {


        Double amount = 10.0;

        if (isDouble(amountValue)) {
            amount = Double.parseDouble(amountValue);
            return amount;
        } else {
            Toast.makeText(getApplicationContext(), "Paying Default Amount ₹10", Toast.LENGTH_LONG).show();
            return amount;
        }
    }

    private boolean isDouble(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private synchronized void init(PayUmoneyDetails hasdetail) {
        String key=hasdetail.getKey();
        String txnId= hasdetail.getTxnid();
        String amount=hasdetail.getAmount();
        String firstName= hasdetail.geFirstname();
        String email= hasdetail.getEmail();
        String phone= hasdetail.getPhone();
        String productinfo= hasdetail.getProductinfo();
        String surl= hasdetail.getSurl();
        String furl= hasdetail.getFurl();
        String salt=hasdetail.getSalt();
        double amountVal=getAmount(amount);
        params.put(PayUMoney_Constants.KEY, key);
        params.put(PayUMoney_Constants.TXN_ID, txnId);
        // params.put("merchantId", "5834982");
        params.put(PayUMoney_Constants.AMOUNT, String.valueOf(amountVal));
        params.put(PayUMoney_Constants.FIRST_NAME, firstName);
        params.put(PayUMoney_Constants.EMAIL, email);
        params.put(PayUMoney_Constants.PHONE, phone);
        params.put(PayUMoney_Constants.PRODUCT_INFO, productinfo);
        params.put(PayUMoney_Constants.SURL, surl);
        params.put(PayUMoney_Constants.FURL, furl);
        params.put(PayUMoney_Constants.UDF1, "");
        params.put(PayUMoney_Constants.UDF2, "");
        params.put(PayUMoney_Constants.UDF3, "");
        params.put(PayUMoney_Constants.UDF4, "");
        params.put(PayUMoney_Constants.UDF5, "");

        String hash = com.sasidhar.smaps.payumoney.Utils.generateHash(params, salt);
        params.put(PayUMoney_Constants.HASH,hash);

        // params.put("salt", "2vLL6QiUJm");
        params.put(PayUMoney_Constants.SERVICE_PROVIDER, "payu_paisa");

        Intent intent = new Intent(this, MakePaymentActivity.class);
        intent.putExtra(PayUMoney_Constants.ENVIRONMENT, PayUMoney_Constants.ENV_DEV);
        intent.putExtra(PayUMoney_Constants.PARAMS, params);

        startActivityForResult(intent, PayUMoney_Constants.PAYMENT_REQUEST);

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PayUMoney_Constants.PAYMENT_REQUEST) {
            if (resultCode == RESULT_OK) {
                Intent intent= new Intent(this,OrderHistoryActivity.class);
                startActivity(intent);

                Toast.makeText(this, "Payment Success,", Toast.LENGTH_SHORT).show();
            } else if (resultCode == RESULT_CANCELED) {
                /*Intent intent= new Intent(this,OrderHistoryActivity.class);
                startActivity(intent);*/
                finish();
                Toast.makeText(this, "Payment Failed | Cancelled.", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
