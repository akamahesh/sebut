package com.app.inn.peepsin.UI.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.OrderHistory;
import com.app.inn.peepsin.Models.ProductList;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.squareup.picasso.Picasso;
import java.util.HashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import static com.app.inn.peepsin.Constants.Constants.kIsSuccessfullyCanceled;

/**
 * Created by Harsh on 7/13/2017.
 */

public class NewOrderHistoryAdapter extends BaseExpandableListAdapter {
    private Context context;
    private CopyOnWriteArrayList<OrderHistory> historyItemList;

    public NewOrderHistoryAdapter(Context context, CopyOnWriteArrayList<OrderHistory> historyItemList) {
        this.context = context;
        this.historyItemList = historyItemList;
    }

    // Parent View Group
    @Override
    public Object getGroup(int listPosition) {
        return this.historyItemList.get(listPosition);
    }

    @Override
    public int getGroupCount() {
        return this.historyItemList.size();
    }

    @Override
    public long getGroupId(int listPosition) {
        return listPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        OrderHistory item = (OrderHistory)getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.row_new_order_history, null);
        }
        TextView tvOrderDate = (TextView) convertView.findViewById(R.id.tv_order_date);
        TextView tvOrderStatus = (TextView) convertView.findViewById(R.id.tv_order_status);
        TextView tvOrderItem = (TextView) convertView.findViewById(R.id.tv_order_item);
        TextView tvOrderIdNumber = (TextView) convertView.findViewById(R.id.tv_order_id_number);
        TextView tvOrderAddressUserName = (TextView) convertView.findViewById(R.id.tv_order_address_user_name);
        TextView tvOrderAddressUser = (TextView) convertView.findViewById(R.id.tv_order_address_user);
        TextView tvOrderAddressNumber = (TextView) convertView.findViewById(R.id.tv_order_address_number);
        TextView tvPaymentMethod = (TextView) convertView.findViewById(R.id.tv_payment_mehtod);
        TextView tvOrderTotalAmmount = (TextView) convertView.findViewById(R.id.tv_order_total_ammount);
        Button btnProduct = (Button) convertView.findViewById(R.id.btn_show_product) ;
        /*TextView tvPaymentBeforePromoAmmount = (TextView) convertView.findViewById(R.id.tv_payment_before_promo_ammount);
        TextView tvPromoCodeAmmount = (TextView) convertView.findViewById(R.id.tv_promo_code_ammount);
        TextView tvDiscountTagLine = (TextView) convertView.findViewById(R.id.tv_discount_tag_line);
        Button btnCancelled = (Button) convertView.findViewById(R.id.btn_cancelled);*/

        tvOrderStatus.setText(Utils.getStatus(item.getOrderStatus()));
        if (item.getOrderStatus() == 4) {
            tvOrderStatus.setTextColor(Color.parseColor("#FFFF0000"));
        }else {
            tvOrderStatus.setTextColor(Color.parseColor("#30C430"));
        }
        String orderdate = item.getOrderDate();
        String orderidnumber = String.valueOf(item.getOrderId());
        String orderaddressusername = item.getDeliveryAddress().getFullName();
        String orderaddressnumber = item.getDeliveryAddress().getPhone();
        if (item.getPaymentType() != null) {
            switch (item.getPaymentType()) {
                case 1:
                    tvPaymentMethod.setText(R.string.text_paytm);
                    break;
                case 2:
                    tvPaymentMethod.setText(R.string.text_pay_u_money);
                    break;
                case 3:
                    tvPaymentMethod.setText(R.string.text_cash_on_delivery);
                    break;
                default:
                    tvPaymentMethod.setText(R.string.pending_payment);
            }
        }

        tvOrderDate.setText(orderdate);
        tvOrderIdNumber.setText(orderidnumber);
        tvOrderAddressUserName.setText(orderaddressusername);
        tvOrderAddressNumber.setText(orderaddressnumber);
        String totalcartitem = "";
        if (item.getCartItemCount() != null)
            totalcartitem = String.valueOf(item.getCartItemCount()) + " " + "item";
        tvOrderItem.setText(totalcartitem);
        String addressTxt = "No Location Available";
        if (item.getDeliveryAddress() != null)
            addressTxt = item.getDeliveryAddress().getCity() + ", "
                    + item.getDeliveryAddress().getState() + ", "
                    + item.getDeliveryAddress().getCountry();

        tvOrderAddressUser.setText(addressTxt);
        String ordertotalammount = "";
        if (item.getFinalAmountAfterPromoCode() != null)
            ordertotalammount = item.getFinalAmountAfterPromoCode();
        tvOrderTotalAmmount.setText(ordertotalammount);

        /*String discounrtag = item.getPromoCodeTagLine();
        tvDiscountTagLine.setText(discounrtag);
        String paymentbeforepromocode = "";
        if (item.getTotalCartPriceBeforePromoCode() != null)
            paymentbeforepromocode = item.getTotalCartPriceBeforePromoCode();
        String promocodeammount = "---";
        if (!item.getDiscountedPrice().equals(""))
        promocodeammount = item.getDiscountedPrice();
        tvPaymentBeforePromoAmmount.setText(paymentbeforepromocode);
        tvPromoCodeAmmount.setText(promocodeammount);
        if (item.getOrderStatus() == 4)
            btnCancelled.setText(R.string.cancelled);
        else
            btnCancelled.setText("Cancel");
        btnCancelled.setOnClickListener(v -> {
            if (item.getOrderStatus() != 4)
                cancelOrder(item.getOrderId(), groupPosition, (Button) btnCancelled);
            else
                Utils.showAlertDialog(context, "", "Product is already Cancelled");

        });*/
        if(isExpanded) {
            btnProduct.setText("Hide Product");
        }else {
            btnProduct.setText("Show Product");

        }

        return convertView;
    }

    private void cancelOrder(int orderId, int position,Button btnCancelled) {
        ModelManager.modelManager().cancelPlacedOrder(orderId, (Constants.Status iStatus, GenricResponse<HashMap<String,Object>> genericResponse) -> {
            HashMap<String,Object> map = genericResponse.getObject();
            Integer issuccessed = (Integer) map.get(kIsSuccessfullyCanceled);
            if(issuccessed == 1) {
                btnCancelled.setText(R.string.cancelled);
                historyItemList.get(position).setOrderStatus(4);
            }
        }, (Constants.Status iStatus, String message) -> {
        });
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    // Child View Group
    @Override
    public Object getChild(int listPosition, int expandedListPosition) {
        return this.historyItemList.get(listPosition).getShopRecord().get(0).getProductRecords().get(expandedListPosition);
    }

    @Override
    public long getChildId(int listPosition, int expandedListPosition) {
        return expandedListPosition;
    }

    @Override
    public int getChildrenCount(int listPosition) {
        return this.historyItemList.get(listPosition).getShopRecord().get(0).getProductRecords().size();
    }

    @Override
    public View getChildView(int listPosition, final int expandedListPosition, boolean isLastChild, View childView, ViewGroup parent) {

        final ProductList product = (ProductList) getChild(listPosition, expandedListPosition);

        if (childView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            childView = layoutInflater.inflate(R.layout.row_new_child_order_history, null);
        }
        TextView tvProductName = (TextView) childView.findViewById(R.id.tv_product_name);
        TextView tvProductRupees = (TextView) childView.findViewById(R.id.tv_product_rupees);
        TextView tvUserTitle = (TextView) childView.findViewById(R.id.tv_user_title);
        ImageView ivProductImage = (ImageView) childView.findViewById(R.id.iv_product_image);
        if(product!=null)
        {
            String producturl=product.getCoverImageThumbnailURL();
            String price = context.getString(R.string.Rs)+product.getPrice();
            String productname = product.getProductName();
            String shopName="From:"+historyItemList.get(listPosition).getShopRecord().get(0).getshopName();

            Picasso.with(context)
                    .load(producturl)
                    .placeholder(R.drawable.img_product_placeholder)
                    .into(ivProductImage);
            tvProductRupees.setText(price);
            tvProductName.setText(productname);
            tvUserTitle.setText(shopName);
        }

        return childView;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;
    }

}