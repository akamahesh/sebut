package com.app.inn.peepsin.UI.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.CurrentUser;
import com.app.inn.peepsin.Models.Feeds;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.activities.HomeActivity;
import com.app.inn.peepsin.UI.fragments.Feeds.MapDialogFragment;
import com.app.inn.peepsin.UI.fragments.Feeds.ProductDetailFragment;
import com.app.inn.peepsin.UI.helpers.MyBounceInterpolator;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kImageUrl;
import static com.app.inn.peepsin.Constants.Constants.kLatitude;
import static com.app.inn.peepsin.Constants.Constants.kLongitude;
import static com.app.inn.peepsin.Constants.Constants.kOwnerName;

/**
 * Created by akaMahesh on 24/4/17
 * contact : mckay1718@gmail.com
 */

public class FeedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {

    private List<Feeds> filterList;
    private List<Feeds> feedList;
    private Context context;
    private static Switcher switcherListener;
    private ProgressDialog progressDialog;
    private CurrentUser user;
    private int pageSize;

    public FeedAdapter(Context context, List<Feeds> items, Switcher switcher) {
        switcherListener = switcher;
        user = ModelManager.modelManager().getCurrentUser();
        progressDialog = Utils.generateProgressDialog(context, false);
        feedList = items;
        filterList = items;
        this.context = context;
        this.pageSize = items.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == feedList.size()) {
            return 0;
        } else {
            return 1;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 1) {
            View layoutView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_product_layout, parent, false);
            return new FeedViewHolder(layoutView);
        } else {
            View layoutView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_feed_progress, parent, false);
            return new FeedProgressHolder(layoutView);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

        if (position == feedList.size()) {
            FeedProgressHolder view = (FeedProgressHolder) viewHolder;
            if(feedList.isEmpty()|| feedList.size()<10){
                view.progressBar.setVisibility(View.GONE);
                view.tvFeedCount.setVisibility(View.GONE);
            }
            else if (feedList.size() >= pageSize) {
                view.progressBar.setVisibility(View.VISIBLE);
                view.tvFeedCount.setVisibility(View.GONE);
                new Handler().postDelayed(() -> {
                    //Hide the refresh after 2sec
                    ((HomeActivity) context).runOnUiThread(() -> {
                        view.progressBar.setVisibility(View.GONE);
                        view.tvFeedCount.setVisibility(View.VISIBLE);
                        String countTxt = context.getString(R.string.feed_total_text) + feedList.size();
                        view.tvFeedCount.setText(countTxt);
                    });
                }, 1000);
            }

        } else {
            FeedViewHolder holder = (FeedViewHolder) viewHolder;
            Feeds item = feedList.get(position);
            holder.bindContent(item);
            String name = item.getName();
            String postingDate = item.getPostingDate();
            String sellerName = "";
            int connectionLevel = 0;
            if (item.getSeller() != null) {
                sellerName = item.getSeller().getFirstName() + " " + item.getSeller().getLastName();
                connectionLevel = item.getSeller().getConnectionLevel();
            }

            String thumbnailURL = item.getImageThumbUrl();
            String addressTxt = "No Location Available";
            if (item.getAddress() != null) {
                addressTxt =
                        item.getAddress().getCity() + ", " + item.getAddress().getState() + ", " + item
                                .getAddress().getCountry();
            }

            String price = item.getPrice();
            String sellingPrice = item.getSellingPrice();
            String discountTagline = item.getDiscountTagline();
            if (discountTagline.isEmpty())
                discountTagline = "No Offer Available Now!";
            holder.tvSellingPrice.setText(context.getString(R.string.Rs) + sellingPrice);
            holder.tvPrice.setText(context.getString(R.string.Rs) + price);
            holder.tvPrice.setPaintFlags(holder.tvPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

            price = price.replaceAll("/", "0").replaceAll("-", "0");
            sellingPrice = sellingPrice.replaceAll("/", "0").replaceAll("-", "0");

            try {
                Float pricefloat = Float.parseFloat(price);
                Float sellingfloat = Float.parseFloat(sellingPrice);
                if (pricefloat <= sellingfloat) {
                    holder.tvPrice.setVisibility(View.GONE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            holder.tvOffer.setText(discountTagline);
            holder.textViewItemName.setText(name);
            holder.textViewConnection.setText(Utils.getUserName(sellerName, connectionLevel));
            holder.textViewTime.setText(postingDate);
            if(item.getRating()==0){
                holder.tvStarRating.setVisibility(View.GONE);
            }
            else {
                holder.tvStarRating.setVisibility(View.VISIBLE);

                holder.tvStarRating.setText(String.valueOf(item.getRating()) + "");
            }
           // holder.tvStarRating.setText(String.valueOf(item.getRating()));

            if (item.getFavourite()) {
                holder.imageViewFav.setImageResource(R.drawable.ic_favorite_red_700_24dp);
            } else {
                holder.imageViewFav.setImageResource(R.drawable.ic_favorite_border_24dp);
            }

            if (!item.getImageUrl().isEmpty()) {
                Picasso.with(context)
                        .load(thumbnailURL)
                        .placeholder(R.drawable.img_product_placeholder)
                        .into(holder.imageViewItem);
            }
        }

    }


    @Override
    public int getItemCount() {
        return feedList.size() + 1;
    }

    public void addNewItems(CopyOnWriteArrayList<Feeds> list) {
        feedList.clear();
        feedList.addAll(list);
        notifyDataSetChanged();
    }

    public void addItems(List<Feeds> list) {
        feedList.clear();
        feedList.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    feedList = filterList;
                } else {
                    ArrayList<Feeds> filteredList = new ArrayList<>();
                    for (Feeds feeds : feedList) {
                        if (feeds.getName().toLowerCase().contains(charString)) {
                            filteredList.add(feeds);
                        }
                    }
                    feedList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = feedList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                feedList = (List<Feeds>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    class FeedProgressHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_feed_count)
        TextView tvFeedCount;
        @BindView(R.id.progress_bar)
        ProgressBar progressBar;

        FeedProgressHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class FeedViewHolder extends RecyclerView.ViewHolder {
        Feeds item;
        @BindView(R.id.tv_name)
        TextView textViewItemName;
        @BindView(R.id.tv_connection)
        TextView textViewConnection;
        @BindView(R.id.tv_price)
        TextView tvPrice;
        @BindView(R.id.tv_selling_price)
        TextView tvSellingPrice;
        @BindView(R.id.tv_offer)
        TextView tvOffer;

        @BindView(R.id.tv_time)
        TextView textViewTime;
        @BindView(R.id.iv_item)
        ImageView imageViewItem;
        @BindView(R.id.iv_fav)
        ImageView imageViewFav;
        @BindView(R.id.tv_star_rating)
        TextView tvStarRating;


        FeedViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.item_view)
        void onRowSelected() {
            getFeedDetails(item.getId());
        }

        private void getFeedDetails(int productId) {
            showProgress(true);
            ModelManager.modelManager().getProductDetails(productId, (Constants.Status iStatus, GenricResponse<Feeds> genericResponse) -> {
                showProgress(false);
                if (switcherListener != null)
                    switcherListener.switchFragment(ProductDetailFragment.newInstance(productId, genericResponse.getObject(), switcherListener), true, true);

            }, (Constants.Status iStatus, String message) -> {
                Toaster.toast(message);
                showProgress(false);
            });
        }

        @OnClick(R.id.iv_fav)
        void onMakeFavorite(ImageView imageView) {
            Boolean isFavorite = item.getFavourite();
            Animation favAnim = AnimationUtils.loadAnimation(context, R.anim.bounce);
            MyBounceInterpolator favInterpolar = new MyBounceInterpolator(.2, 20);
            favAnim.setInterpolator(favInterpolar);
            imageView.startAnimation(favAnim);
            if (isFavorite) {
                makeFavorite(0, item.getId(), getAdapterPosition());
            } else {
                makeFavorite(1, item.getId(), getAdapterPosition());
            }
        }

        @OnClick(R.id.iv_location)
        void getLocation() {
            try {
                String lat = user.getPrimaryAddress().getLatitude();
                String sLat = item.getAddress().getLatitude();
                String sLon = item.getAddress().getLongitude();
                if (!lat.isEmpty() && !sLat.isEmpty()) {
                    MapDialogFragment mapDialogFragment = MapDialogFragment.newInstance();
                    Bundle bdl = new Bundle();
                    bdl.putString(kLatitude, sLat);
                    bdl.putString(kLongitude, sLon);
                    bdl.putString(kOwnerName, item.getName());
                    bdl.putString(kImageUrl, item.getSeller().getProfilePicUrl());
                    mapDialogFragment.setArguments(bdl);
                    mapDialogFragment.setCancelable(true);
                    mapDialogFragment.show(((AppCompatActivity) context).getSupportFragmentManager(),
                            mapDialogFragment.getClass().getSimpleName());

                } else {
                    Toaster.toast("Sorry Lat Long UnAvailable");
                }
            } catch (Exception e) {
                Toaster.toast("Sorry Lat Long UnAvailable");
                e.printStackTrace();
            }
        }

        private void makeFavorite(Integer isFav, Integer productId, int position) {
            ModelManager.modelManager()
                    .setFavouriteProduct(productId, isFav, (Constants.Status iStatus) -> {
                        feedList.get(position).setFavourite(isFav == 1);
                        notifyItemChanged(position);
                    }, (Constants.Status iStatus, String message) -> {
                        Toaster.toast(message);
                    });
        }

        private void bindContent(Feeds item) {
            this.item = item;
        }
    }

    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) {
                progressDialog.cancel();
            }
        }
    }
}
