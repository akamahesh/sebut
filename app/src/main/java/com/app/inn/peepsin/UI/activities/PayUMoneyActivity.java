package com.app.inn.peepsin.UI.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;


import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.CheckSumDetails;
import com.app.inn.peepsin.Models.PayUmoneyDetails;
import com.app.inn.peepsin.R;
import com.sasidhar.smaps.payumoney.MakePaymentActivity;
import com.sasidhar.smaps.payumoney.PayUMoney_Constants;
import com.sasidhar.smaps.payumoney.Utils;

import java.util.HashMap;

import butterknife.OnClick;


public class PayUMoneyActivity extends AppCompatActivity {

    private HashMap<String, String> params = new HashMap<>();
    Integer orderId=0;
    PayUmoneyDetails payUmoneyDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payumoney);
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        Intent orderIntent = getIntent();
        orderId= orderIntent.getIntExtra("OrderId",0);
        generateHashKey(orderId);



    }

    private synchronized void init(PayUmoneyDetails hasdetail) {
        String key=hasdetail.getKey();
        String txnId= hasdetail.getTxnid();
        String amount=hasdetail.getAmount();
        String firstName= hasdetail.geFirstname();
        String email= hasdetail.getEmail();
        String phone= hasdetail.getPhone();
        String productinfo= hasdetail.getProductinfo();
        String surl= hasdetail.getSurl();
        String furl= hasdetail.getFurl();
        String salt=hasdetail.getSalt();
        double amountVal=getAmount(amount);
        params.put(PayUMoney_Constants.KEY, key);
        params.put(PayUMoney_Constants.TXN_ID, txnId);
       // params.put("merchantId", "5834982");
        params.put(PayUMoney_Constants.AMOUNT, String.valueOf(amountVal));
        params.put(PayUMoney_Constants.FIRST_NAME, firstName);
        params.put(PayUMoney_Constants.EMAIL, email);
        params.put(PayUMoney_Constants.PHONE, phone);
        params.put(PayUMoney_Constants.PRODUCT_INFO, productinfo);
        params.put(PayUMoney_Constants.SURL, surl);
        params.put(PayUMoney_Constants.FURL, furl);
        params.put(PayUMoney_Constants.UDF1, "");
        params.put(PayUMoney_Constants.UDF2, "");
        params.put(PayUMoney_Constants.UDF3, "");
        params.put(PayUMoney_Constants.UDF4, "");
        params.put(PayUMoney_Constants.UDF5, "");

       String hash = Utils.generateHash(params, salt);
        params.put(PayUMoney_Constants.HASH,hash);

        // params.put("salt", "2vLL6QiUJm");
       params.put(PayUMoney_Constants.SERVICE_PROVIDER, "payu_paisa");

        Intent intent = new Intent(this, MakePaymentActivity.class);
        intent.putExtra(PayUMoney_Constants.ENVIRONMENT, PayUMoney_Constants.ENV_DEV);
        intent.putExtra(PayUMoney_Constants.PARAMS, params);

        startActivityForResult(intent, PayUMoney_Constants.PAYMENT_REQUEST);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PayUMoney_Constants.PAYMENT_REQUEST) {
            if (resultCode == RESULT_OK) {
               Intent intent= new Intent(this,OrderHistoryActivity.class);
                startActivity(intent);

                Toast.makeText(this, "Payment Success,", Toast.LENGTH_SHORT).show();
            } else if (resultCode == RESULT_CANCELED) {
                startActivity(TransactionDetailActivity.getIntent(this,null));
                Toast.makeText(this, "Payment Failed | Cancelled.", Toast.LENGTH_SHORT).show();
            }
        }
    }


    public void generateHashKey(Integer orderId) {

        ModelManager.modelManager().generateHashCode(orderId,(Constants.Status iStatus, GenricResponse<PayUmoneyDetails > genericResponse) -> {

            payUmoneyDetails=genericResponse.getObject();
            init(payUmoneyDetails);

        }, (Constants.Status iStatus, String message) -> {
            //Log.e(Tag,message);
        });
    }

    private double getAmount(String amountValue) {


        Double amount = 10.0;

        if (isDouble(amountValue)) {
            amount = Double.parseDouble(amountValue);
            return amount;
        } else {
            Toast.makeText(getApplicationContext(), "Paying Default Amount ₹10", Toast.LENGTH_LONG).show();
            return amount;
        }
    }

    private boolean isDouble(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
