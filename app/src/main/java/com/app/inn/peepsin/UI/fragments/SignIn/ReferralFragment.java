package com.app.inn.peepsin.UI.fragments.SignIn;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ViewFlipper;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.activities.LogInActivity;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kAuthToken;
import static com.app.inn.peepsin.Constants.Constants.kIsReferralCodeValidated;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReferralFragment extends Fragment {

    @BindView(R.id.view_flipper)
    ViewFlipper viewFlipper;
    @BindView(R.id.edt_referral)
    EditText etReferral;
    private OnReferralFragmentInteractionListener mListener;
    private ProgressDialog progressDialog;


    private String TAG = getClass().getSimpleName();
    private String mAuthToken;

    public static ReferralFragment newInstance(String authToken) {
        Bundle args = new Bundle();
        args.putString(kAuthToken, authToken);
        ReferralFragment fragment = new ReferralFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public ReferralFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnReferralFragmentInteractionListener) {
            mListener = (OnReferralFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnReferralFragmentInteractionListener");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuthToken = getArguments().getString(kAuthToken);
        progressDialog = Utils.generateProgressDialog(getContext(), false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_referral, container, false);
        ((LogInActivity) getActivity()).hideToolbar();
        ButterKnife.bind(this, view);
        viewFlipper.setAnimation(AnimationUtils.loadAnimation(getContext(), android.R.anim.slide_in_left));
        viewFlipper.setInAnimation(AnimationUtils.loadAnimation(getContext(), android.R.anim.slide_in_left));
        viewFlipper.setOutAnimation(AnimationUtils.loadAnimation(getContext(), android.R.anim.slide_in_left));

        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @OnClick(R.id.btn_enter)
    void onEnter() {
        viewFlipper.showNext();
    }

    @OnClick(R.id.tv_no_referral)
    void onNoReferral() {
        mListener.onReferralDone(mAuthToken);
    }

    @OnClick(R.id.btn_validate)
    void onValidate() {
        etReferral.setError(null);
        String referralCode = Utils.getProperText(etReferral);
        if (referralCode.isEmpty()){
            etReferral.setError("Field should not be empty!");
            return;
        }

        showProgress(true);
        ModelManager.modelManager().validateReferralCode(referralCode, mAuthToken, (Constants.Status iStatus, GenricResponse<HashMap<String, Object>> genericResponse) -> {
            Log.d(TAG, "validateReferralCode : success");
            HashMap<String, Object> hashMap = genericResponse.getObject();
            int isValidate = (int) hashMap.get(kIsReferralCodeValidated);
            showProgress(false);
            if (isValidate == 0) {
                //not validated
                etReferral.setText("");
                viewFlipper.showPrevious();
            } else {
                mListener.onReferralDone(mAuthToken);
            }
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Toaster.toast(message);
            Log.e(TAG, "validateReferralCode : failure ");
        });
    }

    public void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else if (progressDialog != null) {
            progressDialog.cancel();
        }
    }

    public interface OnReferralFragmentInteractionListener {
        void onReferralDone(String mAuthToken);
    }

}
