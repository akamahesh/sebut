package com.app.inn.peepsin.UI.fragments.SellStuffModule;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.RatingModel;
import com.app.inn.peepsin.Models.ReviewModel;
import com.app.inn.peepsin.Models.SellingProduct;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.Services.UpdateAdService;
import com.app.inn.peepsin.UI.activities.EditProductActivity;
import com.app.inn.peepsin.UI.adapters.ReviewAdapter;
import com.app.inn.peepsin.UI.dialogFragments.DialogRatingReviewFragment;
import com.app.inn.peepsin.UI.dialogFragments.ZoomDialogFragment;
import com.app.inn.peepsin.UI.helpers.DividerItemRecyclerDecoration;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import at.blogc.android.views.ExpandableTextView;
import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.PRODUCT_RESULT;
import static com.app.inn.peepsin.Constants.Constants.REVIEW_RESULT;
import static com.app.inn.peepsin.Constants.Constants.kData;
import static com.app.inn.peepsin.Constants.Constants.kEmptyString;
import static com.app.inn.peepsin.Constants.Constants.kImageUrl;
import static com.app.inn.peepsin.Constants.Constants.kMyRating;
import static com.app.inn.peepsin.Constants.Constants.kMyReview;
import static com.app.inn.peepsin.Constants.Constants.kProductId;
import static com.app.inn.peepsin.Constants.Constants.kProductImageUrlList;
import static com.app.inn.peepsin.Constants.Constants.kRecords;
import static com.app.inn.peepsin.Services.PublishAdService.kProductUpdateNotification;

/**
 * Created by Harsh on 5/11/2017.
 */

public class SellingStuffDetailFragment extends Fragment {

    @BindDrawable(R.drawable.img_product_placeholder)
    Drawable placeholder;
    @BindView(R.id.edit)
    Button editBtn;
    @BindView(R.id.view_pager_images)
    ViewPager viewPager;
    @BindView(R.id.tabs)
    TabLayout tabLayout;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_product_name)
    TextView tvProductName;
    @BindView(R.id.tv_description_details)
    TextView tvDescription;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.tv_money)
    TextView tvPrice;
    @BindView(R.id.tv_text_star)
    TextView tvStarRating;
    @BindView(R.id.iv_offer_money)
    TextView tvSellingPrice;
    @BindView(R.id.tv_tax)
    TextView tvTax;
    @BindView(R.id.tv_total_tax)
    TextView tvTaxPrice;
    /*@BindView(R.id.tv_location)
    TextView tvLocation;*/
    @BindView(R.id.tv_shared_count)
    TextView tvShareCount;
    @BindView(R.id.tv_favorite_count)
    TextView tvLikeCount;
    @BindView(R.id.tv_main_category_name)
    TextView tvCategoryName;
    @BindView(R.id.tv_sub_category_name)
    TextView tvSubCategoryName;
    @BindView(R.id.iv_previous) ImageView ivPrevious;
    @BindView(R.id.iv_next) ImageView ivNext;
    @BindView(R.id.tv_rating)
    TextView tvRating;
    @BindView(R.id.tv_skui)
    TextView tvSKUI;
    @BindView(R.id.tv_offer)
    TextView tvOffer;
    @BindView(R.id.tv_ratings_points)
    TextView tvTotalRating;
    @BindView(R.id.progress_bar1)
    ProgressBar progressBar1;
    @BindView(R.id.progress_bar2)
    ProgressBar progressBar2;
    @BindView(R.id.progress_bar3)
    ProgressBar progressBar3;
    @BindView(R.id.progress_bar4)
    ProgressBar progressBar4;
    @BindView(R.id.progress_bar5)
    ProgressBar progressBar5;
    @BindView(R.id.recycler_view_review)
    RecyclerView reviewRecycler;
    @BindView(R.id.btn_show_all_review)
    Button btnShowReviews;
    @BindView(R.id.btn_rate_review)
    Button btnRateReview;
    @BindView(R.id.btn_discription) Button btnDescription;
    @BindView(R.id.btn_specification) Button btnSpecification;

    private int myRating = 0;
    private String myReview = "";
    private List<ReviewModel> reviewList;


    private SellingProduct product;
    private ProgressDialog progressDialog;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private List<String> imagePathList;
    private int productId = 0;
    @BindView(R.id.expandableTextView)
    ExpandableTextView expandableTextView;
    @BindView(R.id.button_toggle)
    Button buttonToggle;
    int isselected=1;

    private FragmentInteractionListener mListener;


    public interface FragmentInteractionListener {
        void onEditProgress();

        void onEditDone();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentInteractionListener) {
            mListener = (FragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement FragmentInteractionListener");
        }
    }

    @Override
    public void onSaveInstanceState( Bundle outState ) {
    }


    public static SellingStuffDetailFragment newInstance(SellingProduct item, Integer productId) {
        SellingStuffDetailFragment fragment = new SellingStuffDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(kRecords, item);
        bundle.putInt(kProductId, productId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = Utils.generateProgressDialog(getContext(), false);
        imagePathList = new ArrayList<>();
        Bundle bundle = getArguments();
        if (bundle != null) {
            product = (SellingProduct) bundle.getSerializable(kRecords);
            productId = bundle.getInt(kProductId);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_selling_stuff_product_detail, container, false);
        ButterKnife.bind(this, view);
        //logged in user should not be able to write review on his own product
        btnRateReview.setVisibility(View.GONE);
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUI(product);
        setupViewPager(product);
        getProductRating(productId);
        getRatingReviewList(productId);

        // downloadImages(imageURLs);

    }

    public void ratingViewUpdate(RatingModel rating) {
        if (rating.getRating().equals("")) {
            tvRating.setText("0.0");
        } else {
            tvRating.setText(rating.getRating());
        }
        Activity activity = getActivity();
        if (activity != null) {

            String totalRating = rating.getTotalRating() + " " + getString(R.string.ratings);
            tvTotalRating.setText(totalRating);

        }
        progressBar1.setProgress(rating.getFifthProgress());
        progressBar2.setProgress(rating.getForthProgress());
        progressBar3.setProgress(rating.getThirdProgress());
        progressBar4.setProgress(rating.getSecondProgress());
        progressBar5.setProgress(rating.getFirstProgress());
        myRating = rating.getMyRating();
        myReview = rating.getMyReview();
    }


    public void reviewListUpdate(List<ReviewModel> reviewList) {
        ReviewAdapter reviewAdapter = new ReviewAdapter(getContext(), reviewList,reviewList.size());
        reviewRecycler.setHasFixedSize(true);
        reviewRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        reviewRecycler.setAdapter(reviewAdapter);
        reviewRecycler.addItemDecoration(new DividerItemRecyclerDecoration(getActivity(), R.drawable.canvas_recycler_divider));
    }


    private void getProductRating(int productId) {
        ModelManager.modelManager().getProductRating(productId, (Constants.Status iStatus, GenricResponse<RatingModel> genericResponse) -> {
            RatingModel rating = genericResponse.getObject();
            ratingViewUpdate(rating);
        }, (Constants.Status iStatus, String message) -> Toaster.toast(message));
    }

    private void getRatingReviewList(int productId) {
        showProgress(true);
        ModelManager.modelManager().getRatingReviewList(1, productId, (Constants.Status iStatus, GenricResponse<List<ReviewModel>> genericResponse) -> {
            reviewList = genericResponse.getObject();

            if (reviewList.size() > 3) {
                List<ReviewModel> list = new ArrayList<>();
                for (int i = 0; i < 3; i++) {
                    list.add(reviewList.get(i));
                }
                reviewListUpdate(list);
                btnShowReviews.setVisibility(View.VISIBLE);
            } else {
                reviewListUpdate(reviewList);
                btnShowReviews.setVisibility(View.GONE);
            }
            showProgress(false);
        }, (Constants.Status iStatus, String message) -> {
            Toaster.toast(message);
            showProgress(false);
        });
    }


    private void setupUI(SellingProduct product) {
        String title = product.getName();
        String description = product.getDescription();
        String postedtime = getString(R.string.postedOn) + " " + product.getPostingDate();
        String price = getString(R.string.Rs) + " " + product.getPrice();
        String salePrice = getString(R.string.Rs) + " " + product.getSellingPrice();
        String shareCount = String.valueOf(product.getShareCount());
        String favCount = String.valueOf(product.getFavouriteCount());
        String starRating = String.valueOf(product.getRating());
        String tax = product.getTotalTax().replace(".00/-","%");
        float val  = Float.parseFloat(product.getBasePrice().replace("/-",""));
        float sell = Float.parseFloat(product.getSellingPrice().replace("/-",""));
        float totalTax = sell-val;
        String taxPrice =  getString(R.string.Rs)+String.format("%.2f", totalTax)+"/-";


        String address = "No Location Available";
        if (product.getAddress() != null) {
            String city = product.getAddress().getCity();
            String state = product.getAddress().getState();
            String country = product.getAddress().getCountry();
            address = city + ", " + state + ", " + country;
        }

        String category = product.getCategory();
        String subcategory = product.getSubcategory();
        String offer = product.getDiscountTagLine();
        String skui ="SKU No. "+ product.getSKUNumber();

        tvOffer.setText(offer);
        tvSKUI.setText(skui);
        tvTitle.setText(title);
        tvProductName.setText(title);
        getExpandableView(1);
        //tvDescription.setText(Html.fromHtml(description));
        tvTime.setText(postedtime);
        tvPrice.setText(price);
        tvSellingPrice.setText(salePrice);
        tvShareCount.setText(shareCount);
        tvLikeCount.setText(favCount);
       // tvLocation.setText(address);
        tvCategoryName.setText(category);
        tvSubCategoryName.setText(subcategory);
        tvStarRating.setText(starRating);
        tvTax.setText(tax);
        tvTaxPrice.setText(taxPrice);

    }


    @OnClick(R.id.btn_discription)
    public void description(){
        btnSpecification.setBackground(getResources().getDrawable(android.R.color.transparent));
        btnSpecification.setTextColor(Color.parseColor("#F8636A"));
        btnDescription.setBackgroundResource(R.drawable.canvas_left_rounded_corner_light_red_background);
        btnDescription.setTextColor(Color.parseColor("#FFFFFF"));
        isselected=1;
        getExpandableView(isselected);
       /* if(!product.getDescription().isEmpty())
            tvDescription.setText(Html.fromHtml(product.getDescription()));
        else tvDescription.setText(R.string.no_description_avilable);*/
    }

    @OnClick(R.id.btn_specification)

    public void specification(){
        isselected=2;
        btnDescription.setBackground(getResources().getDrawable(android.R.color.transparent));
        btnDescription.setTextColor(Color.parseColor("#F8636A"));
        btnSpecification.setBackgroundResource(R.drawable.canvas_right_rounded_corner_light_red_background);
        btnSpecification.setTextColor(Color.parseColor("#FFFFFF"));
        getExpandableView(isselected);
      /*  if(!product.getSpecification().isEmpty())
            tvDescription.setText(Html.fromHtml(product.getSpecification()));
        else tvDescription.setText(R.string.no_specification_avilable);*/
    }

    @OnClick(R.id.btn_back)
    void onBack() {
        getFragmentManager().popBackStack();
    }

    @OnClick(R.id.iv_previous)
    void onPrevious() {
        int current = viewPager.getCurrentItem();
        if (current > 0)
            viewPager.setCurrentItem(--current);
    }

    @OnClick(R.id.iv_next)
    void onNext() {
        int current = viewPager.getCurrentItem();
        if (current < mSectionsPagerAdapter.getCount())
            viewPager.setCurrentItem(++current);
    }

    @OnClick(R.id.edit)
    void editDetails() {
        if(product.getProductStatus().equals(4)){
          Toaster.toastRangeen("This product is under process so you can't edit.");
          return;
        }
        Intent intent = EditProductActivity.getIntent(getContext(), product);
        startActivityForResult(intent, PRODUCT_RESULT);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PRODUCT_RESULT && resultCode == Activity.RESULT_OK) {
            HashMap<String, Object> productMap = (HashMap<String, Object>) data.getSerializableExtra(kData);
            List<String> imagePathList = (List<String>) data.getSerializableExtra(kProductImageUrlList);
            updateAd(productMap, imagePathList);
        }
        if (requestCode == REVIEW_RESULT && resultCode == Activity.RESULT_OK) {
            getProductRating(productId);
            getRatingReviewList(productId);
        }
    }

    private void updateAd(HashMap<String, Object> productMap, List<String> imagePathList) {
        Intent intent = new Intent(getContext(), UpdateAdService.class);
        intent.putExtra(kRecords, productMap);
        intent.putExtra(kProductImageUrlList, (Serializable) imagePathList);
        getActivity().startService(intent);
    }


    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(receiver, new IntentFilter(kProductUpdateNotification));
    }


    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(receiver);
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            product = (SellingProduct) bundle.getSerializable(kRecords);
            if (product != null) {
                Log.d("onReceive", product.getName());
            }
            setupUI(product);
            setupViewPager(product);
        }
    };


    @OnClick(R.id.btn_rate_review)
    void onRateReview() {
        FragmentManager fm = getFragmentManager();
        DialogRatingReviewFragment dialogFragment = new DialogRatingReviewFragment();
        Bundle bdl = new Bundle();
        bdl.putInt(kProductId, productId);
        bdl.putInt(kMyRating, myRating);
        bdl.putString(kMyReview, myReview);
        dialogFragment.setArguments(bdl);
        dialogFragment.setCancelable(true);
        dialogFragment.setTargetFragment(this, REVIEW_RESULT);
        dialogFragment.show(fm, "Sample Fragment");
    }


    private void editAd(HashMap<String, Object> productMap) {
        mListener.onEditProgress();
        ModelManager.modelManager().editSellingProduct(productMap, (Constants.Status iStatus, GenricResponse<SellingProduct> genricResponse) -> {
            mListener.onEditDone();
            Toaster.toastRangeen("Changes Made successfully");
            mListener = null;
            /*SellingProduct sellingProduct = genricResponse.getObject();
            setupUI(sellingProduct);*/
            //getFragmentManager().popBackStack();
        }, (Constants.Status iStatus, String message) -> {
            mListener.onEditDone();
            Toaster.toast(message);
            mListener = null;
        });

    }


    public void setupViewPager(SellingProduct product) {
        //@otherImageThumnailURL for carousel images
        ArrayList<String> otherImageUrl = new ArrayList<>();
        otherImageUrl.add(product.getImageUrl());
        otherImageUrl.addAll(product.getImageUrlList());
        mSectionsPagerAdapter = new SectionsPagerAdapter(getChildFragmentManager());
        mSectionsPagerAdapter.addItems(otherImageUrl);
        viewPager.setAdapter(mSectionsPagerAdapter);
        viewPager.setOffscreenPageLimit(3);
        //@otherImageThumnailURL for tab images
        ArrayList<String> otherImageThumnailURL = new ArrayList<>();
        otherImageThumnailURL.add(product.getImageThumbUrl());
        otherImageThumnailURL.addAll(product.getImageThumbUrlList());
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons(otherImageThumnailURL);

        if(otherImageThumnailURL.size() == 1){
            ivPrevious.setVisibility(View.INVISIBLE);
            ivNext.setVisibility(View.INVISIBLE);
        }else {
            ivPrevious.setVisibility(View.VISIBLE);
            ivNext.setVisibility(View.VISIBLE);
        }
    }

    private void setupTabIcons(List<String> imageUrlList) {
        int i = 0;
        imageUrlList.remove("");
        for (String imageUrl : imageUrlList) {
            tabLayout.getTabAt(i).setCustomView(R.layout.tab_carousal_layout);
            ImageView imageView = (ImageView) tabLayout.getTabAt(i++).getCustomView().findViewById(R.id.iv_tab_image);
            Picasso.with(getActivity())
                    .load(imageUrl)
                    .error(placeholder)
                    .resize(30, 30)
                    .memoryPolicy(MemoryPolicy.NO_STORE)
                    .placeholder(placeholder)
                    .into(imageView);
        }
    }

    public static class PlaceholderFragment extends Fragment {
        String imageURL = kEmptyString;
        ArrayList<String> imageURLList;
        @BindView(R.id.iv_product_image)
        ImageView ivProductImage;

        public PlaceholderFragment() {
        }

        public static PlaceholderFragment newInstance(String imageurl, ArrayList<String> imageUrls) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putString(kImageUrl, imageurl);
            args.putStringArrayList(kProductImageUrlList, imageUrls);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            Bundle bundle = getArguments();
            if (bundle != null) {
                imageURL = bundle.getString(kImageUrl);
                imageURLList = bundle.getStringArrayList(kProductImageUrlList);
            }
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            ButterKnife.bind(this, rootView);


            if (!imageURL.isEmpty())
                Picasso.with(getContext())
                        .load(imageURL)
                        .fit()
                        .memoryPolicy(MemoryPolicy.NO_STORE)
                        .error(R.drawable.img_product_placeholder)
                        .placeholder(R.drawable.img_product_placeholder)
                        .into(ivProductImage);

            return rootView;
        }

        @Override
        public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);
        }


        @OnClick(R.id.container)
        void zoomImage() {
            if (!imageURLList.get(0).isEmpty()) {
                DialogFragment dialogFragment = ZoomDialogFragment.newInstance(imageURLList);
                showDialog(dialogFragment, false);
            }
        }

        void showDialog(DialogFragment dialogFragment, boolean saveInBackstack) {
            String backStateName = dialogFragment.getClass().getName();
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction ft = fragmentManager.beginTransaction();

            if (saveInBackstack) {
                ft.addToBackStack(backStateName);
            }
            dialogFragment.show(fragmentManager, backStateName);
        }

    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        ArrayList<String> otherImageUrlList = new ArrayList<>();

        SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return PlaceholderFragment.newInstance(otherImageUrlList.get(position), otherImageUrlList);
        }

        void addItems(ArrayList<String> imageUrl) {
            otherImageUrlList.addAll(imageUrl);
        }

        @Override
        public int getCount() {
            return otherImageUrlList.size();
        }
    }

    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) progressDialog.cancel();
        }
    }
    private void getExpandableView(int isselected) {
        int lineCount =0;
        // set animation duration via code, but preferable in your layout files by using the animation_duration attribute
        expandableTextView.setAnimationDuration(750L);
        //Typeface type = Typeface.createFromAsset(getAssets(),"fonts/SanFranciscoDisplay-Regular.otf");

        //expandableTextView.setTypeface(type);
        if(isselected==1) {
            if (!product.getDescription().isEmpty()) {
                expandableTextView.setText(Html.fromHtml(product.getDescription().replace("<br>", "<br />")));
            } else {
                expandableTextView.setText(R.string.no_description_avilable);
            }
        } else if(isselected==2){
            if (!product.getSpecification().isEmpty()) {
                expandableTextView.setText(Html.fromHtml(product.getSpecification().replace("<br>", "<br />")));
            } else {
                expandableTextView.setText(R.string.no_specification_avilable);
            }
        }
        Typeface font = Typeface.createFromAsset(getContext().getAssets(), "fonts/SanFranciscoText-Regular.otf");
        expandableTextView.setTypeface(font);
        // set interpolators for both expanding and collapsing animations
        expandableTextView.setInterpolator(new OvershootInterpolator());

        // or set them separately
        expandableTextView.setExpandInterpolator(new OvershootInterpolator());
        expandableTextView.setCollapseInterpolator(new OvershootInterpolator());

        expandableTextView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                try {

                    final int lineCount =   expandableTextView.getLineCount();
                    // Log.e("linecount",lineCount+"");
                    if(lineCount>4){
                        buttonToggle.setVisibility(View.VISIBLE);
                    }else{
                        buttonToggle.setVisibility(View.GONE);
                    }

                }catch (Exception e)
                {
                    e.printStackTrace();
                }

                return true;
            }
        });

        // toggle the ExpandableTextView
        buttonToggle.setOnClickListener(new View.OnClickListener() {
            @SuppressWarnings("ConstantConditions")
            @Override
            public void onClick(final View v) {
                expandableTextView.toggle();
                buttonToggle.setText(expandableTextView.isExpanded() ? R.string.collapse : R.string.expand);
            }
        });

        // but, you can also do the checks yourself
        buttonToggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                if (expandableTextView.isExpanded()) {
                    expandableTextView.collapse();
                    buttonToggle.setText(R.string.expand);
                } else {
                    expandableTextView.expand();
                    buttonToggle.setText(R.string.collapse);
                }
            }
        });

        // listen for expand / collapse events
        expandableTextView.setOnExpandListener(new  at.blogc.android.views.ExpandableTextView.OnExpandListener() {
            @Override
            public void onExpand(final  at.blogc.android.views.ExpandableTextView view) {
                // Log.d(TAG, "ExpandableTextView expanded");
            }

            @Override
            public void onCollapse(final  at.blogc.android.views.ExpandableTextView view) {
                //Log.d(TAG, "ExpandableTextView collapsed");
            }
        });

    }
}
