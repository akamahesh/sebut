package com.app.inn.peepsin.UI.fragments.Shop;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.Address;
import com.app.inn.peepsin.Models.CurrentUser;
import com.app.inn.peepsin.Models.Feeds;
import com.app.inn.peepsin.Models.MyShop;
import com.app.inn.peepsin.Models.Search;
import com.app.inn.peepsin.Models.ShopModel;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.activities.HomeActivity;
import com.app.inn.peepsin.UI.activities.ShoppingCartActivity;
import com.app.inn.peepsin.UI.adapters.FeedAdapter;
import com.app.inn.peepsin.UI.adapters.SearchAdapter;
import com.app.inn.peepsin.UI.adapters.ShopCategoryAdapter;
import com.app.inn.peepsin.UI.fragments.Profile.ShoppingCartFragment;
import com.app.inn.peepsin.UI.fragments.Search.SearchResultFragment;
import com.app.inn.peepsin.UI.helpers.DividerItemRecyclerDecoration;
import com.app.inn.peepsin.UI.helpers.SearchUtil;
import com.app.inn.peepsin.UI.helpers.SharedPreference;

import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import static com.app.inn.peepsin.Constants.Constants.kCategoryId;
import static com.app.inn.peepsin.Constants.Constants.kFeeds;
import static com.app.inn.peepsin.Constants.Constants.kProductTypeId;
import static com.app.inn.peepsin.Constants.Constants.kSearchString;

 /**
 * Created by dhruv on 21/7/17.
 */

public class CategoryShopFeedFragment extends Fragment implements ShopCategoryAdapter.ShopCategoryFilter {

    private String TAG = getTag();

    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.empty_view)
    View emptyView;
    @BindView(R.id.progress_bar)
    ProgressBar progress;
    @BindView(R.id.recycler_view_feeds)
    RecyclerView recyclerViewFeeds;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.recycler_view_shop_item)
    RecyclerView recyclerShopItem;

    @BindView(R.id.bottom_shop_details) View bottomSheetView;
    @BindView(R.id.iv_arrow) ImageView ivArrow;
    @BindView(R.id.tv_Shop_name) TextView tvShopName;
    @BindView(R.id.ratingbar)
    RatingBar ratingBar;
    @BindView(R.id.tv_user_name) TextView tvOwner;
    //@BindView(R.id.tv_selling_count) TextView tvSellingCount;
    //@BindView(R.id.tv_favorite_count) TextView tvFavouriteCount;
    @BindView(R.id.iv_shop_image) ImageView ivBanner;
    @BindView(R.id.iv_logo_image) ImageView ivLogo;
    @BindView(R.id.tv_email) TextView tvEmail;
    @BindView(R.id.tv_phone) TextView tvPhone;
    @BindView(R.id.tv_location) TextView tvLocation;
    @BindView(R.id.tv_joined_shop_date) TextView tvJoined;
    @BindView(R.id.fab) FloatingActionButton floatingActionButton;
    @BindView(R.id.tv_cartCount)
    TextView tvCartCount;

    private static Switcher switcherListener;
    private ProgressDialog progressDialog;
    private int mPage;
    private boolean loading;
    private int pageSize;

    private FeedAdapter feedAdapter;
    private LinearLayoutManager mLayoutManager;
    private List<Feeds> feedsList;
    private List<ShopModel> shopCategoryList;

    private SearchAdapter searchAdapter;
    private List<Search> searchStored;
    private ListView listSearch;
    private TextView tvEmptySearch;
    BottomSheetBehavior bottomSheetBehavior;

    private int categoryId;
    private ShopCategoryAdapter shopCategoryAdapter;
    // AppBarLayout appbar;

    public static Fragment newInstance(Switcher switcher, int categoryId) {
        Fragment fragment = new CategoryShopFeedFragment();
        switcherListener = switcher;
        Bundle bundle = new Bundle();
        bundle.putInt(kCategoryId, categoryId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = Utils.generateProgressDialog(getContext(), true);
        feedsList = new ArrayList<>();
        shopCategoryList = new ArrayList<>();
        Bundle bundle = getArguments();
        if (bundle != null) {
            categoryId = bundle.getInt(kCategoryId);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_category_feed, container, false);
        ButterKnife.bind(this, view);
       // appbar=((HomeActivity) getActivity()).getTabLayout();
       // appbar.setVisibility(View.GONE);
        tvTitle.setText(getString(R.string.title_feeds));

        //Rating star color
        LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(Color.parseColor("#FAD368"), PorterDuff.Mode.SRC_ATOP);

        // init the bottom sheet behavior
        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheetView);
        bottomSheetBehavior.setBottomSheetCallback(bottomSheetCallback);

        // Feed Adapter and feed List
        feedAdapter = new FeedAdapter(getContext(), feedsList, switcherListener);
        recyclerViewFeeds.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerViewFeeds.setLayoutManager(mLayoutManager);
        recyclerViewFeeds.setAdapter(feedAdapter);
        recyclerViewFeeds.addItemDecoration(new DividerItemRecyclerDecoration(getContext(), R.drawable.canvas_recycler_divider));
        recyclerViewFeeds.addOnScrollListener(onScrollListener);

        // Inventory Adapter and Inventory List
        shopCategoryAdapter = new ShopCategoryAdapter(shopCategoryList, getContext(), this);
        LinearLayoutManager HorizontalLayout = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerShopItem.setLayoutManager(HorizontalLayout);
        recyclerShopItem.setAdapter(shopCategoryAdapter);
        swipeRefreshLayout.setOnRefreshListener(onRefreshListener);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadShopList();
    }

    @OnClick(R.id.btn_back)
    void onBack() {
        getFragmentManager().popBackStack();
        //Utils.isfeedsClicked=0;
     //   appbar.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.btn_cart)
    void onCart() {
        if(switcherListener!=null)
            switcherListener.switchFragment(ShoppingCartFragment.newInstance(switcherListener),true,true);
        //startActivity(ShoppingCartActivity.getIntent(getContext()));
    }

    @Override
    public void onResume() {
        super.onResume();
        CurrentUser currentUser = ModelManager.modelManager().getCurrentUser();
        setCartUpdate(currentUser.getShoppingCartCount());
        //appbar.setVisibility(View.GONE);

    }

    public void setCartUpdate(int count) {
        if (count == 0)
            tvCartCount.setVisibility(View.GONE);
        else {
            tvCartCount.setVisibility(View.VISIBLE);
            tvCartCount.setText(String.valueOf(count));
        }
    }

    public void initialState() {
        mPage = 1;
        loading = true;
        pageSize = shopCategoryList.size();
    }

    SwipeRefreshLayout.OnRefreshListener onRefreshListener = this::loadShopList;

    BottomSheetBehavior.BottomSheetCallback bottomSheetCallback = new BottomSheetBehavior.BottomSheetCallback() {
        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if(newState == BottomSheetBehavior.STATE_EXPANDED){
                ivArrow.setImageDrawable(getResources().getDrawable(R.drawable.ic_keyboard_arrow_down_grey_500_24dp));
            }else {
                ivArrow.setImageDrawable(getResources().getDrawable(R.drawable.ic_keyboard_arrow_up_grey_500_24dp));
            }
            // this part hides the button immediately and waits bottom sheet to collapse
            if (BottomSheetBehavior.STATE_DRAGGING == newState) {
                floatingActionButton.animate().scaleX(0).scaleY(0).setDuration(300).start();
            } else if (BottomSheetBehavior.STATE_COLLAPSED == newState) {
                floatingActionButton.animate().scaleX(1).scaleY(1).setDuration(300).start();
            }
        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {
        }
    };

    RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            if (dy > 0) //check for scroll down
            {
                int visibleItemCount = mLayoutManager.getChildCount();
                int totalItemCount = mLayoutManager.getItemCount();
                int firstVisibleItemPosition = mLayoutManager.findFirstVisibleItemPosition();

                if (loading) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= pageSize) {
                        loading = false;
                        //Do pagination.. i.e. fetch new data
                        //loadMoreFeeds(++mPage);
                    }
                }
            }
        }
    };

    // Refresh list on Swipe
    public void loadShopList() {
        showProgress(true);
        HashMap<String ,Object> map = new HashMap<>();
        map.put(kProductTypeId,categoryId);
        ModelManager.modelManager().getShopList(map,1, (Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<ShopModel>> genericResponse) -> {
            CopyOnWriteArrayList<ShopModel> shopCategories = genericResponse.getObject();
            if (!shopCategories.isEmpty()) {
                shopCategoryList = shopCategories;
                shopCategoryList.get(0).setSelected(true);
                shopCategoryAdapter.addItems(shopCategories);
                loadFeeds(shopCategoryList.get(0).getId());
                loadShopData(shopCategoryList.get(0).getId());
                initialState();
            } else checkEmptyScreen();
            showProgress(false);
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            checkEmptyScreen();
            Log.e(TAG, message);
        });
    }

    public void loadFeeds(int shopId) {
        ModelManager.modelManager().getProductFeeds(shopId, categoryId,0, (Constants.Status iStatus, GenricResponse<HashMap<String, Object>> genericResponse) -> {
            HashMap<String, Object> map = genericResponse.getObject();
            CopyOnWriteArrayList<Feeds> list = (CopyOnWriteArrayList<Feeds>) map.get(kFeeds);
            feedsList = list;
            feedAdapter.addNewItems(list);
            checkEmptyScreen();
            initialState();
            showProgress(false);
        }, (Constants.Status iStatus, String message) -> {
            Log.e(TAG, message);
            checkEmptyScreen();
            showProgress(false);
        });
    }

     private void loadShopData(Integer shopId){
         ModelManager.modelManager().getShopDetails(shopId, (Constants.Status iStatus, GenricResponse<MyShop> genricResponse) -> {
             setupShop(genricResponse.getObject());
         }, (Constants.Status iStatus, String message) -> Log.e(TAG,message));
     }

    /*public void loadMoreFeeds(int page) {
        ModelManager.modelManager().getProductFeeds(shopId, categoryId,(Constants.Status iStatus, GenricResponse<HashMap<String,Object>> genericResponse) -> {
            HashMap<String,Object> map = genericResponse.getObject();
            CopyOnWriteArrayList<Feeds> list = (CopyOnWriteArrayList<Feeds>) map.get(kFeeds);
            feedAdapter.addItems(list);
            loading=true;
        }, (Constants.Status iStatus, String message) -> Log.e(TAG,message));
    }*/

    @Override
    public void Categoryfilter(Integer shopId, ShopModel shopModel) {
        showProgress(true);
        loadFeeds(shopId);
        loadShopData(shopId);
    }

    private void checkEmptyScreen() {
        if (feedAdapter.getItemCount() > 1) {
            recyclerShopItem.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
        } else {
            recyclerShopItem.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.search_view)
    public void onSearch() {
        loadToolBarSearch();
    }

    public void loadToolBarSearch() {
        searchStored = SharedPreference.getSearchList(getContext(), SearchUtil.PREFS_NAME, SearchUtil.KEY_SEARCH);
        View view = getActivity().getLayoutInflater().inflate(R.layout.view_toolbar_search, null);
        EditText edtToolSearch = (EditText) view.findViewById(R.id.edt_tool_search);
        ImageView ivToolMic = (ImageView) view.findViewById(R.id.img_tool_mic);
        ImageView ivToolBack = (ImageView) view.findViewById(R.id.img_tool_back);
        listSearch = (ListView) view.findViewById(R.id.list_search);
        tvEmptySearch = (TextView) view.findViewById(R.id.txt_empty);

        SearchUtil.setListViewHeightBasedOnChildren(listSearch);
        edtToolSearch.setHint("Search to shop");

        Dialog toolbarSearchDialog = new Dialog(getContext(), R.style.MaterialSearch);
        toolbarSearchDialog.setContentView(view);
        toolbarSearchDialog.setCancelable(false);
        toolbarSearchDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        toolbarSearchDialog.getWindow().setGravity(Gravity.BOTTOM);
        toolbarSearchDialog.show();

        toolbarSearchDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        if (searchStored == null) {
            searchStored = new ArrayList<>();
        }
        searchAdapter = new SearchAdapter(getContext(), searchStored, false);
        listSearch.setAdapter(searchAdapter);

        listSearch.setOnItemClickListener((adapterView, view13, position, l) -> {
            toolbarSearchDialog.dismiss();

            Search item = searchAdapter.getItemPos(position);
            if (searchStored.contains(item)) {
                SharedPreference.removeSearch(getContext(), item, SearchUtil.PREFS_NAME, SearchUtil.KEY_SEARCH);
            }
            SharedPreference.addSearch(getContext(), item, SearchUtil.PREFS_NAME, SearchUtil.KEY_SEARCH);

            if (switcherListener != null)
                switcherListener.switchFragment(SearchResultFragment.newInstance(item.getCategoryId(), switcherListener), true, false);
        });

        edtToolSearch.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                String str = edtToolSearch.getText().toString();
                searchProduct(str);
                return true;
            }
            return false;
        });

        edtToolSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (searchAdapter.getCount() > 0) {
                    searchAdapter.clearList();
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String str = s.toString().toLowerCase();
                char[] ch = str.toCharArray();
                for (char aCh : ch) {
                    if (aCh == ' ') {
                        String withoutSpaceStr = str.replaceAll("\\s+", "");
                        searchProduct(withoutSpaceStr);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        ivToolBack.setOnClickListener(view1 -> toolbarSearchDialog.dismiss());

        ivToolMic.setOnClickListener(view12 -> edtToolSearch.setText(""));
    }

    public void searchProduct(String searchStr) {
        HashMap<String, Object> searchmap = new HashMap<>();
        searchmap.put(kSearchString, searchStr);
        ModelManager.modelManager().getSearch(searchmap, (Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<Search>> genericList) -> {
            searchAdapter.updateList(genericList.getObject(), true);
            checkEmptySearch();
        }, (Constants.Status iStatus, String message) -> {
            Toaster.toast(message);
            checkEmptySearch();
        });
    }

    private void checkEmptySearch() {
        if (searchAdapter.getCount() > 0) {
            listSearch.setVisibility(View.VISIBLE);
            tvEmptySearch.setVisibility(View.GONE);
        } else {
            listSearch.setVisibility(View.GONE);
            tvEmptySearch.setVisibility(View.VISIBLE);
            tvEmptySearch.setText(getString(R.string.empty_message_search));
        }
    }

    private void showProgress(boolean b) {
        if (b) {
            swipeRefreshLayout.setRefreshing(true);
            //progressDialog.show();
        } else {
            swipeRefreshLayout.setRefreshing(false);
            //if (progressDialog != null) progressDialog.cancel();
        }
    }

    public void setupShop(MyShop upShop) {
        String shopName = upShop.getName();
        String owner = upShop.getOwnerName();
        Integer rating = upShop.getRating();
        String ownerImageUrl = (upShop.getBannerUrl()==null)?"":upShop.getBannerUrl();
        String ownerLogoUrl = (upShop.getBannerUrl()==null)?"":upShop.getBannerUrl();
        String favouriteCount = upShop.getFavoriteCount().toString();
        String shopCount = upShop.getProductCount().toString();
        String email = upShop.getEmail();
        String phone =(upShop.getPhone().isEmpty())?"No Phone Available":upShop.getPhone();

        Address address= upShop.getAddress();
        String addressText = "Address not Available.";
        if(address!=null){
            String street = "";
            if(address.getStreetAddress() != "") {
                street = address.getStreetAddress() + ", ";
            }
            addressText = street+address.getCity()+" ,"+address.getState()+" ,"+address.getCountry();
        }

        String joinedTxt = "Joined PeepsIn on " + upShop.getJoinedDate();
        tvJoined.setText(joinedTxt);
        tvLocation.setText(addressText);
        tvShopName.setText(shopName);
        tvOwner.setText(owner);
        ratingBar.setRating(Float.valueOf(rating));
        tvEmail.setText(email);
        tvPhone.setText(phone);

        if(!ownerImageUrl.isEmpty()){
            Picasso.with(getContext())
                    .load(ownerImageUrl)
                    .resize(400,300)
                    .placeholder(R.drawable.placeholder_300)
                    .into(ivBanner);
        }
        if (!ownerLogoUrl.isEmpty()) {
            Picasso.with(getContext())
                    .load(ownerLogoUrl)
                    .resize(80,80)
                    .placeholder(R.drawable.img_shop_logo)
                    .into(ivLogo);
        }
        //tvFavouriteCount.setText(favouriteCount);
        //tvSellingCount.setText(shopCount);
    }


    @Override
    public void onDetach() {
        super.onDetach();
        switcherListener = null;
    }

}
