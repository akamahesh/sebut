package com.app.inn.peepsin.Models;

import java.io.Serializable;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by akaMahesh on 29/6/17.
 * copyright to : Innverse Technologies
 */

public class ChatProduct implements Serializable {

  private String userId;           //sender User Id
  private String productId;        //productId
  private String jabberId;          //sender jabberID
  private String userName;          //sender name
  private String userImageURL;      //user sender image url
  private String productName;
  private String productThumbnailURL;
  private String connectionLevel;
  private String chatOn;  //1 for user 2 for product
  private String timeStamp;
  private String price;
  private Integer unreadChatCount = 0;

  private CopyOnWriteArrayList<Message> messageList;

  public ChatProduct() {
    messageList = new CopyOnWriteArrayList<>();
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public String getJabberId() {
    return jabberId;
  }

  public void setJabberId(String jabberId) {
    this.jabberId = jabberId;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getUserImageURL() {
    return userImageURL;
  }

  public void setUserImageURL(String userImageURL) {
    this.userImageURL = userImageURL;
  }

  public String getProductName() {
    return (productName==null)?"":productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  public String getProductThumbnailURL() {
    return (productThumbnailURL==null)?"":productThumbnailURL;
  }

  public void setProductThumbnailURL(String productThumbnailURL) {
    this.productThumbnailURL = productThumbnailURL;
  }


  public String getTimeStamp() {
    return timeStamp;
  }

  public void setTimeStamp(String timeStamp) {
    this.timeStamp = timeStamp;
  }

  public String getPrice() {
    return (price==null)?"":price;
  }

  public void setPrice(String price) {
    this.price = price;
  }


  public CopyOnWriteArrayList<Message> getMessageList() {
    return messageList;
  }

  public void setMessageList(CopyOnWriteArrayList<Message> messageList) {
    this.messageList = messageList;
  }

  public Message getLastMessage() {
    int size = messageList.size();
    Message lastM = null;
    if (size > 0) {
      lastM = messageList.get(size - 1);
    }
    return lastM;
  }

  public String getUniqueId() {
    String pID = (productId == null) ? "" : productId;
    return pID + jabberId;
  }

  public Integer getUnreadChatCount() {
    return unreadChatCount;
  }

  public void setUnreadChatCount(Integer unreadChatCount) {
    this.unreadChatCount = unreadChatCount;
  }

  public void increaseUnreadChatCount() {
    ++unreadChatCount;
  }

  public String getConnectionLevel() {
    return connectionLevel;
  }

  public void setConnectionLevel(String connectionLevel) {
    this.connectionLevel = connectionLevel;
  }

  public String getChatOn() {
    return chatOn;
  }

  public void setChatOn(String chatOn) {
    this.chatOn = chatOn;
  }
}
