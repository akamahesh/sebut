package com.app.inn.peepsin.UI.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.inn.peepsin.Models.ProductType;
import com.app.inn.peepsin.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by dhruv on 5/9/17.
 */

public class ProductTypeAdapter extends RecyclerView.Adapter<ProductTypeAdapter.ViewHolder> {

    private Context context;
    private List<ProductType> inventoryList;
    private List<ProductType> selectedList;

    public ProductTypeAdapter(Context context, List<ProductType> inventoryList) {
        this.context = context;
        this.inventoryList = inventoryList;
        this.selectedList = new ArrayList<>();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_product_grid_base, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ProductType type = inventoryList.get(position);
        holder.bindContent(type);
        String name = type.getName();
        holder.tvItemName.setText(name);
        if(type.isSelected()){
            holder.checkBox.setChecked(true);
            selectedList.add(type);
        }

        String imageURL = type.getUrl();
        if (!imageURL.isEmpty())
            Picasso.with(context)
                    .load(imageURL)
                    .placeholder(R.drawable.img_product_placeholder)
                    .into(holder.ivItemImage);
        else
            holder.ivItemImage.setImageDrawable(
                    context.getResources().getDrawable(R.drawable.img_product_placeholder));
    }

    public List<ProductType> getProductList(){
        return selectedList;
    }

    @Override
    public int getItemCount() {
        return inventoryList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ProductType itemType;
        @BindView(R.id.iv_item_image)
        ImageView ivItemImage;
        @BindView(R.id.tv_item_name)
        TextView tvItemName;
        @BindView(R.id.checkBox_btn)
        CheckBox checkBox;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindContent(ProductType shop) {
            this.itemType = shop;
        }

        @OnClick(R.id.checkBox_btn)
        void onItemClick() {
            if(itemType.isSelected()){
                itemType.setSelected(false);
                checkBox.setChecked(false);
                selectedList.remove(itemType);
            }else {
                itemType.setSelected(true);
                checkBox.setChecked(true);
                selectedList.add(itemType);
            }
        }
    }
}
