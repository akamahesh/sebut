package com.app.inn.peepsin.UI.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.app.inn.peepsin.BuildConfig;
import com.app.inn.peepsin.Managers.BaseManager.ExceptionHandler;
import com.app.inn.peepsin.Models.SellingProduct;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.fragments.SellStuffModule.CameraPreviewFragment;
import com.app.inn.peepsin.UI.fragments.SellStuffModule.SellingProductEditFragment;
import com.app.inn.peepsin.UI.helpers.FragmentUtil;
import com.app.inn.peepsin.UI.helpers.Utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static com.app.inn.peepsin.Constants.Constants.kData;
import static com.app.inn.peepsin.Constants.Constants.kProduct;
import static com.app.inn.peepsin.Constants.Constants.kProductImageUrlList;

/**
 * Created by root on 25/5/17.
 */

public class EditProductActivity extends AppCompatActivity implements
        CameraPreviewFragment.CameraPreviewInteractionListener,
        SellingProductEditFragment.EditProductInteractionListener{

    private SellingProduct product;
    private final int REQUEST_PERMISSION_CAMERA_STORAGE = 101;
    private ArrayList<String> imagePATHList = new ArrayList<>();

    public static Intent getIntent(Context context, SellingProduct product){
        Intent intent =  new Intent(context,EditProductActivity.class);
        intent.putExtra(kProduct,product);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
        setContentView(R.layout.fragment_root);

        if (!BuildConfig.DEBUG) {
            // do something for a debug build
            Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        }
        product = (SellingProduct) getIntent().getSerializableExtra(kProduct);
        FragmentUtil.changeFragment(getSupportFragmentManager(), SellingProductEditFragment.getInstance(product),true,false);
        imagePATHList.add(product.getImageUrl());
        imagePATHList.addAll(product.getImageUrlList());
    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
    }

    @Override
    public void onUpload(List<String> imagePathList) {
        Log.v("TAG","pop");
        imagePATHList = (ArrayList<String>) imagePathList;
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onCamera() {
        Utils.hideKeyboard(this);
        checkPermission();
    }

    @Override
    public void submitAd(HashMap<String, Object> productMap) {
        Intent intent = getIntent();
        intent.putExtra(kData, productMap);
        intent.putExtra(kProductImageUrlList, (Serializable) imagePATHList);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    @SuppressLint("NewApi")
    private void checkPermission() {
        if (ContextCompat.checkSelfPermission(this, CAMERA) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            FragmentUtil.addFragment(getSupportFragmentManager(),CameraPreviewFragment.newInstance(imagePATHList),true,false);
        } else if (ActivityCompat.shouldShowRequestPermissionRationale(this, CAMERA) && ActivityCompat.shouldShowRequestPermissionRationale(this, READ_EXTERNAL_STORAGE)) {
            // We've been denied once before. Explain why we need the permission, then ask again.
            Utils.showDialog(this, "Camera & Gallary permissions are required to upload profile images!", "Ask Permission", "Discard", (dialog, which) -> {
                if (which == -1)
                    requestPermissions(new String[]{CAMERA, READ_EXTERNAL_STORAGE}, REQUEST_PERMISSION_CAMERA_STORAGE);
                else
                    dialog.dismiss();
            });
        } else {
            // We've never asked. Just do it.
            requestPermissions(new String[]{CAMERA, READ_EXTERNAL_STORAGE}, REQUEST_PERMISSION_CAMERA_STORAGE);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_PERMISSION_CAMERA_STORAGE && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            FragmentUtil.addFragment(getSupportFragmentManager(),CameraPreviewFragment.newInstance(imagePATHList),true,false);
        } else {
            // We were not granted permission this time, so don't try to show the contact picker
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
