package com.app.inn.peepsin.UI.fragments.Connections;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.CurrentUser;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.activities.ShoppingCartActivity;
import com.app.inn.peepsin.UI.fragments.Profile.ShoppingCartFragment;
import com.app.inn.peepsin.UI.helpers.CustomViewPager;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.CART_UPDATE;
import static com.app.inn.peepsin.Constants.Constants.kData;
import static com.app.inn.peepsin.Constants.Constants.kRecords;

public class ConnectionFragment extends Fragment implements TabLayout.OnTabSelectedListener{

    private static Switcher switcherListener;
    private CurrentUser currentUser;
    @BindView(R.id.tv_title) TextView toolbarTitle;
    @BindView(R.id.view_pager_connection) CustomViewPager viewPager;
    @BindView(R.id.tabs_connection) TabLayout tabLayout;
    @BindColor(R.color.navigation_active_tab) int activeColor;
    @BindColor(R.color.navigation_inactive_tab) int inactiveColor;
    @BindColor(R.color.tab_request_color) int requestColor;
    @BindColor(R.color.tab_invitation_color) int invitationColor;
    @BindView(R.id.tv_cartCount) TextView tvCartCount;

    @BindView(R.id.toolbar_layout)
    Toolbar toolbar;

    //SlidingRootNav slidingRootNav;
    private int[] activeImageResId = {
            R.drawable.ic_connections,
            R.drawable.ic_request_active,
            R.drawable.ic_eye_active};

    private int[] inactiveImageResId = {
            R.drawable.ic_connections_inactive,
            R.drawable.ic_requests_inactive,
            R.drawable.ic_eye_inactive};

    public ConnectionFragment() {
    }

    public static ConnectionFragment newInstance(Switcher switcher) {
        ConnectionFragment fragment = new ConnectionFragment();
        switcherListener = switcher;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_connection, container, false);
        ButterKnife.bind(this,view);
        //appBarLayout=((HomeActivity) getActivity()).getTabLayout();
      // btnSidemenu.setVisibility(View.VISIBLE);
        //btnSidemenu.setBackgroundResource(R.drawable.img_sidemenu);
        toolbarTitle.setText(getString(R.string.title_connections));
        viewPager.setPagingEnabled(false);
        viewPager.setOffscreenPageLimit(1);
        setupViewPager(viewPager);

        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons(tabLayout);
        tabLayout.addOnTabSelectedListener(this);
        //CustomSideMenu.setCustomSideMenu(getActivity(),toolbar, viewPager,savedInstanceState);
       // setSideMenu( savedInstanceState);
        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(outState==null)
            return;
        outState.putString(kRecords, "records saved");

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewPager.setCurrentItem(0);
        currentUser = ModelManager.modelManager().getCurrentUser();
        setCartUpdate(currentUser.getShoppingCartCount());
    }

    public void setCartUpdate(int count){
        if(count==0)
            tvCartCount.setVisibility(View.GONE);
        else{
            tvCartCount.setVisibility(View.VISIBLE);
            tvCartCount.setText(String.valueOf(count));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        // register cart update receiver
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mBroadcastReceiver,
                new IntentFilter(CART_UPDATE));
    }


    BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction().equals(CART_UPDATE)){
                setCartUpdate(intent.getIntExtra(kData,0));
            }
        }
    };

    @Override
    public void onDetach() {
        super.onDetach();
        switcherListener=null;
    }

    @OnClick(R.id.btn_cart)
    void onCart(){
        if(switcherListener!=null)
            switcherListener.switchFragment(ShoppingCartFragment.newInstance(switcherListener),true,true);
        //startActivity(ShoppingCartActivity.getIntent(getContext()));
    }

    public void setupViewPager(ViewPager viewPager) {
        SectionsPagerAdapter adapter = new SectionsPagerAdapter(getChildFragmentManager());
        adapter.addFragment(MyConnectionFragment.newInstance(switcherListener,2));
        adapter.addFragment(RequestsFragment.newInstance(switcherListener,2));
        adapter.addFragment(InvitationsFragment.newInstance(switcherListener,2));
        viewPager.setAdapter(adapter);
    }

    public void setupTabIcons(TabLayout upTabIcons) {
        tabLayout.getTabAt(0).setCustomView(R.layout.tab_my_connections);
        tabLayout.getTabAt(1).setCustomView(R.layout.tab_requests);
        tabLayout.getTabAt(2).setCustomView(R.layout.tab_invitations);
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        int position = tab.getPosition();
        Utils.hideKeyboard(getContext());
        View view = tabLayout.getTabAt(position).getCustomView();
        TextView tabText = (TextView) view.findViewById(R.id.tv_tab);
        if(position == 1){
            tabText.setTextColor(requestColor);
            tabLayout.setSelectedTabIndicatorColor(requestColor);
        }else if(position ==2){
            tabText.setTextColor(invitationColor);
            tabLayout.setSelectedTabIndicatorColor(invitationColor);
        }else {
            tabText.setTextColor(activeColor);
            tabLayout.setSelectedTabIndicatorColor(activeColor);
        }
        ImageView tabImage = (ImageView) view.findViewById(R.id.iv_tab);
        tabImage.setImageResource(activeImageResId[position]);
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
        int position = tab.getPosition();
        View view = tabLayout.getTabAt(position).getCustomView();
        TextView tabText = (TextView) view.findViewById(R.id.tv_tab);
        tabText.setTextColor(inactiveColor);
        ImageView tabImage = (ImageView) view.findViewById(R.id.iv_tab);
        tabImage.setImageResource(inactiveImageResId[position]);
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }


    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();

        SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFragment(Fragment fragment) {
            mFragmentList.add(fragment);
        }
    }
  /*  public void setSideMenu(Bundle savedInstanceState){
    //slidingRootNav=slidingRootNav.getLayout().

        slidingRootNav=  new SlidingRootNavBuilder(getActivity())
                .withMenuOpened(false)
                .withToolbarMenuToggle(toolbar)
                .withMenuLayout(R.layout.custom_sidemenu)
                .inject();
        btnSidemenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slidingRootNav.getLayout();
                if(slidingRootNav.isMenuHidden()) {
                    slidingRootNav.openMenu();
                    CustomSideMenu.setCustomSideMenu(getActivity(), slidingRootNav,  mainviewPager);
                }else {
                    slidingRootNav.closeMenu();
                }

            }
        });
    }*/
}
