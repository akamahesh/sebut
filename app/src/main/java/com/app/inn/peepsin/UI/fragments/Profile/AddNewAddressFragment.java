package com.app.inn.peepsin.UI.fragments.Profile;

import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.APIManager.APIManager;
import com.app.inn.peepsin.Managers.APIManager.APIRequestHelper;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.Address;
import com.app.inn.peepsin.Models.CurrentUser;
import com.app.inn.peepsin.Models.GeoAddress;
import com.app.inn.peepsin.Models.SocialLink;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.Services.GPSTracker;
import com.app.inn.peepsin.UI.fragments.AddressOTPFragment;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.helpers.Validations;
import com.app.inn.peepsin.UI.interfaces.AddressListener;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static com.app.inn.peepsin.Constants.Constants.kAddressId;
import static com.app.inn.peepsin.Constants.Constants.kAuthToken;
import static com.app.inn.peepsin.Constants.Constants.kCity;
import static com.app.inn.peepsin.Constants.Constants.kCountry;
import static com.app.inn.peepsin.Constants.Constants.kFullName;
import static com.app.inn.peepsin.Constants.Constants.kIsPrimary;
import static com.app.inn.peepsin.Constants.Constants.kLatitude;
import static com.app.inn.peepsin.Constants.Constants.kLongitude;
import static com.app.inn.peepsin.Constants.Constants.kOTP;
import static com.app.inn.peepsin.Constants.Constants.kPhone;
import static com.app.inn.peepsin.Constants.Constants.kRecords;
import static com.app.inn.peepsin.Constants.Constants.kResults;
import static com.app.inn.peepsin.Constants.Constants.kState;
import static com.app.inn.peepsin.Constants.Constants.kStreetAddress;
import static com.app.inn.peepsin.Constants.Constants.kType;
import static com.app.inn.peepsin.Constants.Constants.kZipcode;
import static com.app.inn.peepsin.Managers.BaseManager.PermissionManager.LOCATION_REQUEST_CODE;

/**
 * Created by Harsh on 5/10/2017.
 */

public class AddNewAddressFragment extends Fragment {
    private final String TAG = getClass().getSimpleName() + ">>>";
    String apikey;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.et_user_name)
    EditText etUserName;
    @BindView(R.id.et_phone_number)
    EditText etPhoneNumber;
    @BindView(R.id.et_street_address)
    EditText etStreetAddress;
    @BindView(R.id.et_city)
    AutoCompleteTextView etCity;
    @BindView(R.id.et_state)
    AutoCompleteTextView etState;
    @BindView(R.id.et_country)
    AutoCompleteTextView etCountry;
    @BindView(R.id.et_zip_code)
    EditText etZipCode;
    @BindView(R.id.checkBox_btn)
    CheckBox checkBoxPrimary;

    private ProgressDialog progressDialog;
    private CurrentUser user;
    String currentPhone;

    double latitude = 0.0;
    double longitude = 0.0;

    private int TYPE;
    private Address address;
    GPSTracker gpsTracker;
    private final static int TYPE_NEW = 1;  //for new address
    private final static int TYPE_EDIT = 2;    // for edit address
    private static Switcher profileSwitcherListener;
    private static AddressListener addressListener;


    public static AddNewAddressFragment newInstance(int type, Address address, Switcher switcher, AddressListener listener) {
        AddNewAddressFragment fragment = new AddNewAddressFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(kType, type);
        bundle.putSerializable(kRecords, address);
        profileSwitcherListener = switcher;
        addressListener = listener;
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = Utils.generateProgressDialog(getContext(), false);
        Bundle bundle = getArguments();
        if (bundle != null) {
            TYPE = bundle.getInt(kType, 0);
            address = (Address) bundle.getSerializable(kRecords);
        }
        apikey = getString(R.string.google_api_key);
        user = ModelManager.modelManager().getCurrentUser();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_add_new_address, container, false);
        ButterKnife.bind(this, view);

        List<String> indiancities = Arrays.asList(getResources().getStringArray(R.array.city_indian_array));
        ArrayAdapter<String> cityAdapter = new ArrayAdapter<>(getContext(), R.layout.fragment_skill_list_dialog_item, indiancities);
        etCity.setAdapter(cityAdapter);

        List<String> stateList = Arrays.asList(getResources().getStringArray(R.array.states_indian_array));
        ArrayAdapter<String> stateAdapter = new ArrayAdapter<>(getContext(), R.layout.fragment_skill_list_dialog_item, stateList);
        etState.setAdapter(stateAdapter);

        List<String> countryList = Arrays.asList(getResources().getStringArray(R.array.countries_array));
        ArrayAdapter<String> countryAdapter = new ArrayAdapter<>(getContext(), R.layout.fragment_skill_list_dialog_item, countryList);
        etCountry.setAdapter(countryAdapter);

        if (TYPE == TYPE_EDIT) {
            tvTitle.setText(getString(R.string.title_edit_address));
            setupAddress(address);
        } else if (TYPE == TYPE_NEW) {
            tvTitle.setText(getString(R.string.title_add_new_address));
            etUserName.setText(user.getFullName());
            if (!user.getPhone().getContactId().isEmpty())
                etPhoneNumber.setText(user.getPhone().getContactId());
        }

        etPhoneNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable start) {
                if (start.toString().matches(Utils.PHONE_PATTERN)) {
                    etPhoneNumber.setText("+91 " + start.toString());
                    Selection.setSelection(etPhoneNumber.getText(), etPhoneNumber.getText().length());
                }
            }
        });
        return view;
    }


    public void setupAddress(Address upAddress) {
        etUserName.setText(upAddress.getFullName());
        etCity.setText(upAddress.getCity());
        etCountry.setText(upAddress.getCountry());
        etPhoneNumber.setText(upAddress.getPhone());
        currentPhone = upAddress.getPhone();
        etState.setText(upAddress.getState());
        etStreetAddress.setText(upAddress.getStreetAddress());
        etZipCode.setText(upAddress.getZipCode());
        boolean isPrimary = upAddress.getPrimary();
        checkBoxPrimary.setChecked(isPrimary);
        latitude = Double.parseDouble(upAddress.getLatitude());
        longitude = Double.parseDouble(upAddress.getLongitude());

    }

    @OnClick(R.id.btn_save_address)
    void addAddress() {
        Utils.hideKeyboard(getContext());
        if (isValidAddress()) {
            if (TYPE == TYPE_EDIT) {
                makeAddress(2);
            } else if (TYPE == TYPE_NEW) {
                makeAddress(1);
            }
        }
    }

    public boolean isValidAddress() {
        boolean isOk = true;
        EditText etValid;
        String testString;
        View requestFocus = null;

        etValid = etUserName;
        testString = Utils.getProperText(etValid);
        if (testString.isEmpty()) {
            etValid.setError(getString(R.string.error_cannot_be_empty));
            requestFocus = etValid;
            isOk = false;
        }

        etValid = etPhoneNumber;
        testString = Utils.getProperText(etValid);
        if (testString.isEmpty()) {
            etValid.setError(getString(R.string.error_cannot_be_empty));
            requestFocus = etValid;
            isOk = false;
        } else if (!Validations.isValidPhone(testString)) {
            etValid.setError(getString(R.string.error_invalid_phone));
            requestFocus = etValid;
            isOk = false;
        }

        etValid = etStreetAddress;
        testString = Utils.getProperText(etValid);
        if (testString.isEmpty()) {
            etValid.setError(getString(R.string.error_cannot_be_empty));
            requestFocus = etValid;
            isOk = false;
        }

        etValid = etCity;
        testString = Utils.getProperText(etValid);
        if (testString.isEmpty()) {
            etValid.setError(getString(R.string.error_cannot_be_empty));
            requestFocus = etValid;
            isOk = false;
        }
        etValid = etState;
        testString = Utils.getProperText(etValid);
        if (testString.isEmpty()) {
            etValid.setError(getString(R.string.error_cannot_be_empty));
            requestFocus = etValid;
            isOk = false;
        }

        etValid = etCountry;
        testString = Utils.getProperText(etValid);
        if (testString.isEmpty()) {
            etValid.setError(getString(R.string.error_cannot_be_empty));
            requestFocus = etValid;
            isOk = false;
        }

        etValid = etZipCode;
        testString = Utils.getProperText(etValid);
        if (testString.isEmpty()) {
            etValid.setError(getString(R.string.error_cannot_be_empty));
            requestFocus = etValid;
            isOk = false;
        }
        if (requestFocus != null) requestFocus.requestFocus();
        return isOk;
    }

    //1 for new 2 for edit
    private void makeAddress(int type) {
        showProgress(true);
        //for edit address should always be primary
        boolean isPrimary;
        isPrimary = type == 2 || checkBoxPrimary.isChecked();

        String username = Utils.getProperText(etUserName);
        String phone = Utils.getProperText(etPhoneNumber);
        String streetaddress = Utils.getProperText(etStreetAddress);
        String city = Utils.getProperText(etCity);
        String state = Utils.getProperText(etState);
        String country = Utils.getProperText(etCountry);
        String zipcode = Utils.getProperText(etZipCode);

        String addressTXT =
                ((city.isEmpty()) ? "" : city + "+") +
                        ((state.isEmpty()) ? "" : state + "+") +
                        ((country.isEmpty()) ? "" : country + "+") +
                        ((zipcode.isEmpty()) ? "" : zipcode + "+").replaceAll(" ", "+");

        Integer addressId = (address == null) ? 0 : address.getAddressId();
        HashMap<String, Object> addressMap = new HashMap<>();
        addressMap.put(kAuthToken, ModelManager.modelManager().getCurrentUser().getAuthToken());
        addressMap.put(kFullName, username);
        addressMap.put(kPhone, phone);
        addressMap.put(kStreetAddress, streetaddress);
        addressMap.put(kCity, city);
        addressMap.put(kState, state);
        addressMap.put(kCountry, country);
        addressMap.put(kZipcode, zipcode);
        addressMap.put(kIsPrimary, isPrimary);
        addressMap.put(kAddressId, addressId);
        requestCordinates(addressTXT, addressMap, type);
/*
        if(type==2) {
            if (!getProperText(etPhoneNumber).equals(currentPhone))
                verifyContact(phone, address, addressMap,type);
            else
                requestCordinates(address, addressMap, type);
        }else if(type==1){
            verifyContact(phone, address, addressMap,type);
            } else*/

    }

    @OnClick(R.id.btn_back)
    void onBack() {
        getFragmentManager().popBackStack();
    }

    @OnClick(R.id.iv_auto_add_address)
    public void autoAddAddress() {
        if (ContextCompat.checkSelfPermission(getActivity(), ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(getActivity(), ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            getLocation();
        } else if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), ACCESS_FINE_LOCATION) && ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), ACCESS_COARSE_LOCATION)) {
            // We've been denied once before. Explain why we need the permission, then ask again.
            Utils.showDialog(getContext(),
                    "Location Permission Required",
                    "Ask Permission",
                    "Discard",
                    (dialog, which) -> {
                        if (which == -1)
                            requestPermissions(new String[]{ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION}, LOCATION_REQUEST_CODE);
                        else
                            dialog.dismiss();
                    });
        } else {
            // We've never asked. Just do it.
            requestPermissions(new String[]{ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION}, LOCATION_REQUEST_CODE);
        }


    }

    public void requestAddress(double latitude, double longitude) {
        String latlngText = latitude + "," + longitude;
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(APIManager.kGoogleMapsBaseURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIRequestHelper service = retrofit.create(APIRequestHelper.class);
        showProgress(true);
        service.getLocation(latlngText, apikey).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                try {
                    showProgress(false);
                    JsonObject jsonObject = response.body();
                    JSONObject jsonResponse = new JSONObject(jsonObject.toString());
                    Log.i(TAG, jsonResponse.toString(4));
                    JSONArray jsonResult = jsonResponse.getJSONArray(kResults);
                    GeoAddress geoAddress = new GeoAddress(jsonResult.getJSONObject(0));
                    HashMap<String, Object> addressMap = Utils.handlePlacesJSON(geoAddress);
                    setupAddress(addressMap);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.i(TAG, t.toString());
                showProgress(false);
            }
        });
    }


    public void requestCordinates(String address, HashMap<String, Object> addressMap, int type) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(APIManager.kGoogleMapsBaseURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIRequestHelper service = retrofit.create(APIRequestHelper.class);
        showProgress(true);
        service.getCordinates(address, apikey).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                try {
                    showProgress(false);
                    JsonObject jsonObject = response.body();
                    JSONObject jsonResponse = new JSONObject(jsonObject.toString());
                    JSONArray jsonResult = jsonResponse.getJSONArray(kResults);
                    GeoAddress geoAddress = new GeoAddress(jsonResult.getJSONObject(0));
                    latitude = geoAddress.getLocation().getLat();
                    longitude = geoAddress.getLocation().getLng();
                    Log.i(TAG, "latitude : " + latitude);
                    Log.i(TAG, "longitude : " + longitude);
                    addressMap.put(kLatitude, latitude);
                    addressMap.put(kLongitude, longitude);
                    SocialLink socialLink = ModelManager.modelManager().getCurrentUser().getPhone();
                    String phoneNumber = socialLink.getContactId();
                    String givenPhone = (String) addressMap.get(kPhone);
                    if (givenPhone.equals(phoneNumber) && socialLink.getVerified()) {
                        saveAddress(addressMap, type);
                    }/*else if(addressListener!=null)
                        saveAddress(addressMap,type);*/ else
                        verifyPhoneContact(addressMap, type);
                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlertDialog(getContext(), "Error Processing Address", "Please Provide a valid address that can be located on map.");
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.i(TAG, t.toString());
                showProgress(false);
            }
        });
    }

    private void verifyPhoneContact(HashMap<String, Object> addressMap, int type) {
        String phoneNumber = (String) addressMap.get(kPhone);
        showProgress(true);
        ModelManager.modelManager().loginByOtp(phoneNumber,
                (Constants.Status iStatus, GenricResponse<HashMap<String, Object>> genricResponse) -> {
                    showProgress(false);
                    HashMap<String, Object> hashMap = genricResponse.getObject();
                    String OTP = (String) hashMap.get(kOTP);
                    if (profileSwitcherListener != null)
                        profileSwitcherListener.switchFragment(AddressOTPFragment.newInstance(OTP, phoneNumber, type, addressMap), true, true);
                    else if (addressListener != null)
                        addressListener.switchFragment(AddressOTPFragment.newInstance(OTP, phoneNumber, type, addressMap), true, true);
                    else onBack();
                }, (Constants.Status iStatus, String message) -> {
                    Utils.showAlertDialog(getContext(), "Otp Failed!", message);
                });
    }

    //type 1 = for new and 2 for edit
    private void saveAddress(HashMap<String, Object> addressMap, int type) {
        if (type == 1) {
            ModelManager.modelManager().addNewAddress(addressMap, (Constants.Status iStatus) -> {
                showProgress(false);
                onBack();
            }, (Constants.Status iStatus, String message) -> {
                showProgress(false);
                Toaster.toast(message);
            });
        } else if (type == 2) {
            //addressMap.put(kAddressId, address.getAddressId());
            ModelManager.modelManager().editAddress(addressMap, (Constants.Status iStatus) -> {
                onBack();
            }, (Constants.Status iStatus, String message) -> {
                showProgress(false);
                Toaster.toast(message);
            });
        }
    }


    public void getLocation() {
        // check if GPS enabled
        gpsTracker = new GPSTracker(getContext());
        if (gpsTracker.canGetLocation()) {
            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();
            requestAddress(latitude, longitude);
        } else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gpsTracker.showSettingsAlert();
        }
    }

    private void setupAddress(HashMap<String, Object> addressMap) {
        CurrentUser currentUser = ModelManager.modelManager().getCurrentUser();
        String name = currentUser.getFullName();
        SocialLink phone = currentUser.getPhone();
        String phoneNumber = "";
        if (phone != null)
            phoneNumber = phone.getContactId();
        String streetAddress = (String) addressMap.get(kStreetAddress);
        String city = (String) addressMap.get(kCity);
        String state = (String) addressMap.get(kState);
        String country = (String) addressMap.get(kCountry);
        String zipcode = (String) addressMap.get(kZipcode);

        etStreetAddress.setText(streetAddress);
        etCity.setText(city);
        etState.setText(state);
        etCountry.setText(country);
        etZipCode.setText(zipcode);
        latitude = (double) addressMap.get(kLatitude);
        longitude = (double) addressMap.get(kLongitude);

        etUserName.setText(name);
        etPhoneNumber.setText(phoneNumber);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (gpsTracker != null)
            gpsTracker.stopUsingGPS();
    }


    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) progressDialog.dismiss();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == LOCATION_REQUEST_CODE && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            getLocation();
        } else {
            // We were not granted permission this time, so don't try to show the contact picker
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
