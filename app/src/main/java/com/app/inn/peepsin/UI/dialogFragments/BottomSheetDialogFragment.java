package com.app.inn.peepsin.UI.dialogFragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.inn.peepsin.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by akamahesh on 5/5/17.
 */

public class BottomSheetDialogFragment extends DialogFragment {

    public static DialogFragment newInstance(){
        return new BottomSheetDialogFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_frament_bottom_sheet, container, false);
        ButterKnife.bind(this,view);
        return view;
    }



}
