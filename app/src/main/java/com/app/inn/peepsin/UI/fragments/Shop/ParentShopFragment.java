package com.app.inn.peepsin.UI.fragments.Shop;

import static com.app.inn.peepsin.Constants.Constants.CART_UPDATE;
import static com.app.inn.peepsin.Constants.Constants.kData;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.CurrentUser;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.fragments.Feeds.SharedProductListFragment;
import com.app.inn.peepsin.UI.fragments.Profile.ShoppingCartFragment;
import com.app.inn.peepsin.UI.helpers.FragmentUtil;
import com.app.inn.peepsin.UI.interfaces.Switcher;

/**
 * Created by Harsh on 7/20/2017.
 */

public class ParentShopFragment extends Fragment {

  static Switcher switcherListener;
  @BindView(R.id.tv_title)
  TextView tvTitle;
  @BindView(R.id.tv_cartCount)
  TextView tvCartCount;
  @BindView(R.id.appBarLayout)
  AppBarLayout appbar;
  @BindView(R.id.toolbar)
  Toolbar toolbar;
  @BindView(R.id.btn_shop)
  Button btnShop;
  @BindView(R.id.btn_categories)
  Button btnCategories;
  @BindView(R.id.btn_shared_product)
  Button btnSharedProduct;


  public static Fragment newInstance(Switcher switcher) {
    switcherListener = switcher;
    return new ParentShopFragment();
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    return inflater.inflate(R.layout.parent_shop_fragment_root, container, false);
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    ButterKnife.bind(this, view);
    tvTitle.setText(getString(R.string.title_shop));
    //appBarLayout=((HomeActivity) getActivity()).getTabLayout();

    CurrentUser currentUser = ModelManager.modelManager().getCurrentUser();
    setCartUpdate(currentUser.getShoppingCartCount());
    FragmentUtil
        .changeFragment(getChildFragmentManager(), ShopsFragment.newInstance(switcherListener,"",""), true,
            false);
  }

  @OnClick(R.id.btn_shop)
  void openShop() {
    btnShop.setBackground(this.getResources().getDrawable(R.color.ButtonShopCategoryColor));
    btnShop.setTextColor(this.getResources().getColor(R.color.text_color_white));
    btnCategories.setBackground(null);
    btnSharedProduct.setBackground(null);
    btnSharedProduct.setTextColor(this.getResources().getColor(R.color.ButtonShopCategoryColor));
    btnCategories.setTextColor(this.getResources().getColor(R.color.ButtonShopCategoryColor));
    FragmentUtil.changeFragment(getChildFragmentManager(), ShopsFragment.newInstance(switcherListener,"",""), true,
            false);
  }

  @OnClick(R.id.btn_categories)
  void openCategories() {
    btnCategories.setBackground(this.getResources().getDrawable(R.color.ButtonShopCategoryColor));
    btnCategories.setTextColor(this.getResources().getColor(R.color.text_color_white));
    btnShop.setBackground(null);
    btnSharedProduct.setBackground(null);
    btnSharedProduct.setTextColor(this.getResources().getColor(R.color.ButtonShopCategoryColor));
    btnShop.setTextColor(this.getResources().getColor(R.color.ButtonShopCategoryColor));
    FragmentUtil
        .changeFragment(getChildFragmentManager(), CategoriesFragment.newInstance(switcherListener),
            true, false);
  }

  @OnClick(R.id.btn_shared_product)
  void openSharedProduct() {
    btnSharedProduct
        .setBackground(this.getResources().getDrawable(R.color.ButtonShopCategoryColor));
    btnSharedProduct.setTextColor(this.getResources().getColor(R.color.text_color_white));
    btnShop.setBackground(null);
    btnCategories.setBackground(null);
    btnCategories.setTextColor(this.getResources().getColor(R.color.ButtonShopCategoryColor));
    btnShop.setTextColor(this.getResources().getColor(R.color.ButtonShopCategoryColor));
    FragmentUtil.changeFragment(getChildFragmentManager(),
        SharedProductListFragment.newInstance(switcherListener), true, false);

  }

  @Override
  public void onResume() {
    super.onResume();
    // register cart update receiver
    LocalBroadcastManager.getInstance(getContext()).registerReceiver(mBroadcastReceiver, new IntentFilter(CART_UPDATE));
  }

  BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      if (intent.getAction().equals(CART_UPDATE)) {
        setCartUpdate(intent.getIntExtra(kData, 0));
      }
    }
  };



  public void setCartUpdate(int count) {
    if (count == 0) {
      tvCartCount.setVisibility(View.GONE);
    } else {
      tvCartCount.setVisibility(View.VISIBLE);
      tvCartCount.setText(String.valueOf(count));
    }
  }

  @OnClick(R.id.btn_cart)
  void onCart() {
    if(switcherListener!=null)
      switcherListener.switchFragment(ShoppingCartFragment.newInstance(switcherListener),true,true);
    //startActivity(ShoppingCartActivity.getIntent(getContext()));
  }
/*
  @Override
  public void onPause() {
    super.onPause();
    LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mBroadcastReceiver);
  }*/

  @Override
  public void onDetach() {
    super.onDetach();
    switcherListener = null;
  }
}
