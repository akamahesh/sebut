package com.app.inn.peepsin.UI.fragments.SignIn;

import static com.app.inn.peepsin.Constants.Constants.kContactId;
import static com.app.inn.peepsin.Constants.Constants.kData;
import static com.app.inn.peepsin.Constants.Constants.kOTP;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.activities.LogInActivity;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.chaos.view.PinView;
import java.util.HashMap;

/**
 * Created by akaMahesh on 2/5/17
 * contact : mckay1718@gmail.com
 */

public class VerifyOTPFragment extends Fragment {

  @BindView(R.id.tv_otp)
  TextView tvOTP;
  @BindView(R.id.btn_verify)
  Button btnVerify;
  @BindView(R.id.otp_view)
  PinView otpView;

  private String PIN;
  private String mContactID;
  private ProgressDialog progressDialog;
  private HashMap<String, Object> userMap;
  private OnOTPFragmentInteractionListener mListener;


  public static Fragment newInstance(String pin, String contactId,
      HashMap<String, Object> userMap) { //1 for phone 2 for email
    Fragment fragment = new VerifyOTPFragment();
    Bundle bundle = new Bundle();
    bundle.putString(kOTP, pin);
    bundle.putString(kContactId, contactId);
    bundle.putSerializable(kData, userMap);
    fragment.setArguments(bundle);
    return fragment;
  }


  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    if (context instanceof OnOTPFragmentInteractionListener) {
      mListener = (OnOTPFragmentInteractionListener) context;
    } else {
      throw new RuntimeException(context.toString()
          + " must implement OnOTPFragmentInteractionListener");
    }
  }

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    Bundle bundle = getArguments();
    if (bundle != null) {
      PIN = bundle.getString(kOTP);
      mContactID = bundle.getString(kContactId);
      userMap = (HashMap<String, Object>) bundle.getSerializable(kData);
    }

  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_otp, container, false);
    ButterKnife.bind(this, view);
    ((LogInActivity)getActivity()).setTitle("Verify Contact");
    progressDialog = Utils.generateProgressDialog(getContext(), false);
    btnVerify.setText("Verify Contact");
    Toaster.toastRangeen("OTP sent to " + mContactID + "\n" + " Please enter OTP to complete SignUp process" );
    Toaster.toastOTP(PIN);
    return view;
  }

  @OnClick(R.id.btn_verify)
  void verify() {
    String otpString = otpView.getText().toString();
    if (otpString.equals(PIN)) {
      mListener.onSignUpVerified(userMap);
      Utils.hideKeyboard(getContext());
    } else {
      otpView.setText("");
      Utils.showKeyboard(getContext(), otpView);
      Toaster.toast("Worng OTP");
    }
  }

  @OnClick(R.id.tv_resend)
  void resend() {
    resetOTP(mContactID);
  }

  private void resetOTP(String contactID) {
    showProgress(true);
    ModelManager.modelManager().loginByOtp(contactID,
        (Constants.Status iStatus, GenricResponse<HashMap<String, Object>> genricResponse) -> {
          HashMap<String, Object> map = genricResponse.getObject();
          PIN = (String) map.get(kOTP);
          showProgress(false);
          Toaster.toastRangeen("OTP sent to " + mContactID + "\n" + " Please enter OTP to complete SignUp process");
          Toaster.toastOTP(PIN);
        }, (Constants.Status iStatus, String message) -> {
          Utils.showAlertDialog(getContext(), "Otp Failed! ", message);
          showProgress(false);
        });

  }

  private void showProgress(boolean b) {
    if (b) {
      progressDialog.show();
    } else {
      if (progressDialog != null) {
        progressDialog.dismiss();
      }
    }
  }

  public interface OnOTPFragmentInteractionListener {
    void onSignUpVerified(HashMap<String, Object> userMap);
  }


}
