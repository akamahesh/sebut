package com.app.inn.peepsin.UI.activities;


import static com.app.inn.peepsin.Constants.Constants.kAPNSToken;
import static com.app.inn.peepsin.Constants.Constants.kAuthToken;
import static com.app.inn.peepsin.Constants.Constants.kDeviceType;
import static com.app.inn.peepsin.Constants.Constants.kLatitude;
import static com.app.inn.peepsin.Constants.Constants.kLongitude;
import static com.app.inn.peepsin.Constants.Constants.kType;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Libraries.Firebase.app.Config;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.CurrentUser;
import com.app.inn.peepsin.Models.OrderStatusList;
import com.app.inn.peepsin.Models.ProductType;
import com.app.inn.peepsin.Models.SharedProduct;
import com.app.inn.peepsin.Models.ShopModel;
import com.app.inn.peepsin.Models.ShoppingCart;
import com.app.inn.peepsin.Models.UserLocation;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.activities.AppPermissionActivity.Type;
import java.util.HashMap;
import java.util.concurrent.CopyOnWriteArrayList;


public class UpdationActivity extends AppCompatActivity {

  private Integer current = 1;
  private String firebaseToken;
  private AppPermissionActivity.Type mType;
  private String mAuthToken;
  public String TAG = getClass().getSimpleName();

  @BindView(R.id.progress_bar)
  ProgressBar progressBar;

  public static Intent getIntent(Context context,String authToken,AppPermissionActivity.Type type) {
    Intent intent = new Intent(context,UpdationActivity.class);
    intent.putExtra(kAuthToken,authToken);
    intent.putExtra(kType,type);
    return intent;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
    setContentView(R.layout.fragment_ideal);
    ButterKnife.bind(this);
    Intent intent = getIntent();
    if(intent!=null){
      mAuthToken = intent.getStringExtra(kAuthToken);
      mType = (Type) intent.getSerializableExtra(kType);
    }
    setFireBaseToken();
    getUserProfile();
  }

  @Override
  protected void onPause() {
    super.onPause();
    overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
  }

  // 1 for signIn, 2 for signUp
  public void getUserProfile() {
    ModelManager.modelManager().getMyProfile(mAuthToken, (Constants.Status iStatus) -> {
      getLocation();
    }, (Constants.Status iStatus, String message) -> {
      getLocation();
      Log.e(TAG, message);
    });
  }

  public void getLocation(){
    ModelManager.modelManager().getUserLocation((Constants.Status iStatus, GenricResponse<UserLocation> genericResponse) -> {
      if(ModelManager.modelManager().getGenericAuthToken().isEmpty())
        getGenericAuthToken();
      else
        update();
    }, (Constants.Status iStatus, String message) -> {
      if(ModelManager.modelManager().getGenericAuthToken().isEmpty())
        getGenericAuthToken();
      else
        update();
      Log.e(TAG, message);
    });
  }

  public void getGenericAuthToken(){
    ModelManager.modelManager().getGenericAuthToken((Constants.Status iStatus, GenricResponse<String> genricResponse) -> {
      update();
    }, (Constants.Status iStatus, String message) -> {
      update();
      Log.e(TAG, message);
    });
  }


  private void update() {
    CurrentUser currentUser = ModelManager.modelManager().getCurrentUser();
    UserLocation userLocation = currentUser.getUserLocation();
    String lat = "28.5355";
    String lng = "77.3910";
    if(userLocation!=null){
      lat = userLocation.getLatitude();
      lng = userLocation.getLongitude();
    }

    // 1. register device
    {
      HashMap<String, Object> hashMap = new HashMap<>();
      hashMap.put(kAuthToken, mAuthToken);
      hashMap.put(kDeviceType, Constants.DeviceType.android.getValue());
      hashMap.put(kAPNSToken, firebaseToken);
      ModelManager.modelManager().registerDevice(hashMap, (Constants.Status iStatus) -> {
        Log.v("register Device", " " + iStatus);
        think(current++);
      }, (Constants.Status iStatus, String message) -> {
        Log.e(getClass().getSimpleName(), message);
        think(current++);
      });
    }

    // 2. update cart count
    {
      ModelManager.modelManager().getShoppingCart((Constants.Status iStatus, GenricResponse<ShoppingCart> genericResponse) -> {
            Log.v("get cart number", " " + genericResponse.getObject().getCartItemCount());
            think(current++);
          }, (Constants.Status iStatus, String message) ->{
            Log.e(TAG, message);
            think(current++);
          });
    }


    // 3. Top shop List
    {
      ModelManager.modelManager().getFeatureShopList(1,  lat,lng ,
          (Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<ShopModel>> genericResponse) -> {
            think(current++);
          }, (Constants.Status iStatus, String message) -> {
            think(current++);
            Log.e(TAG, message);
          });
    }

    // 4. Top shop List
    {
      HashMap<String ,Object> map = new HashMap<>();
      map.put(kLatitude,lat);
      map.put(kLongitude,lng);
      ModelManager.modelManager().getShopList(map,1,
          (Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<ShopModel>> genericResponse) -> {
            think(current++);
          }, (Constants.Status iStatus, String message) -> {
            think(current++);
            Log.e(TAG, message);
          });
    }

    // 5. Shared Product List
    {
      ModelManager.modelManager().getSharedProductList(1,
          (Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<SharedProduct>> genericResponse) -> {
            think(current++);
          }, (Constants.Status iStatus, String message) -> {
            think(current++);
            Log.e(TAG, message);
          });
    }

    // 6. Favorite shop List
    {
      ModelManager.modelManager().getFavouriteShopList((Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<ShopModel>> genericResponse) -> {
            think(current++);
          },(Constants.Status iStatus, String message) -> {
            think(current++);
            Log.e(TAG, message);
          });
    }

    // 7. Product Type List
    {
      ModelManager.modelManager().getProductTypeList(
          (Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<ProductType>> genricResponse) -> {
            think(current++);
          }, (Constants.Status iStatus, String message) -> {
            think(current++);
            Log.e(TAG, message);
          });
    }

    //8. tax list
    {
      ModelManager.modelManager().getTaxInfo((Constants.Status iStatus) -> {
        think(current++);
      }, (Constants.Status iStatus, String message) -> {
        Log.e(TAG, message);
        think(current++);
      });
    }

    //9. order status list
    {
      ModelManager.modelManager().getOrderStatusList(
              (Constants.Status iStatus,GenricResponse<CopyOnWriteArrayList<OrderStatusList>> genricResponse) -> {
        think(current++);
      }, (Constants.Status iStatus, String message) -> {
        Log.e(TAG, message);
        think(current++);
      });
    }

  }

  private void setFireBaseToken() {
    SharedPreferences pref = getSharedPreferences(Config.SHARED_PREF, 0);
    firebaseToken = pref.getString("regId", null);
  }


  private void think(int i) {
    if (i == 9) {
      if(mType.equals(Type.SIGNIN))
        startActivity(HomeActivity.getIntent(this));
      else
        startActivity(AppPermissionActivity.getIntent(this,mType));
      finish();
    }else
      System.out.println("count "+current);

  }

}
