package com.app.inn.peepsin.UI.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.app.inn.peepsin.Models.SubCategory;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.interfaces.Switcher;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Harsh on 7/20/2017.
 */

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.MyViewHolder> {

    Context mContext;
    private List<SubCategory> subCategoryList;
    static Switcher switcherListener;


    public CategoriesAdapter(Context mContext, List<SubCategory> albumList, Switcher switcher) {
        this.mContext = mContext;
        this.subCategoryList = albumList;
        switcherListener = switcher;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.categories_checkbox_layout, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        SubCategory subCategory = subCategoryList.get(position);
        holder.bindContent(subCategory);
        holder.tvSubCategoryText.setText(subCategory.getName());

    }

    public void addItems(List<SubCategory> list) {
        subCategoryList.clear();
        subCategoryList.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return subCategoryList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        SubCategory item;

        @BindView(R.id.tv_sub_category_text)
        TextView tvSubCategoryText;
        @BindView(R.id.check_box)
        CheckBox checkBox;

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bindContent(SubCategory shop) {
            this.item = shop;
        }
    }
}

