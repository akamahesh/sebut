package com.app.inn.peepsin.Models;

/**
 *
 */
public enum OrderStatus {

    COMPLETED,
    ACTIVE,
    INACTIVE

}
