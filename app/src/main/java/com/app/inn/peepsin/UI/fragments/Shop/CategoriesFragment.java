package com.app.inn.peepsin.UI.fragments.Shop;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.ShopCatSubCategory;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.activities.BaseActivity;
import com.app.inn.peepsin.UI.helpers.GridSpacingItemDecoration;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Harsh on 7/20/2017.
 */

public class CategoriesFragment extends Fragment {

  static Switcher switchListener;
  @BindView(R.id.recycler_view_inventory)
  RecyclerView recyclerViewInventory;
  @BindView(R.id.progress_bar)
  ProgressBar progressBar;
  @BindView(R.id.empty_view)
  View emptyView;
  @BindView(R.id.swipeRefreshLayout)
  SwipeRefreshLayout swipeRefreshLayout;
  private List<ShopCatSubCategory> inventoryList;
  private InventoryAdapter inventoryAdapter;
  //private List<ShopCatSubCategory> newinventoryList;

  public static Fragment newInstance(Switcher switcher) {
    switchListener = switcher;
    return new CategoriesFragment();
  }

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    inventoryList = new ArrayList<>();
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_category, container, false);
    ButterKnife.bind(this, view);

    // Inventory Adapter and Inventory List
    inventoryAdapter = new InventoryAdapter(inventoryList);
    RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(),
        BaseActivity.getSpan());
    recyclerViewInventory.setLayoutManager(mLayoutManager);
    recyclerViewInventory.addItemDecoration(
        new GridSpacingItemDecoration(BaseActivity.getSpan(), Utils.dpToPx(getContext(), 10),
            true));
    recyclerViewInventory.setItemAnimator(new DefaultItemAnimator());
    recyclerViewInventory.setAdapter(inventoryAdapter);
    checkEmptyState();

    swipeRefreshLayout.setOnRefreshListener(onRefreshListener);
    return view;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    loadShopCategoryList();
  }

  @Override
  public void onDetach() {
    super.onDetach();
    switchListener = null;
  }

  SwipeRefreshLayout.OnRefreshListener onRefreshListener = this::refreshShopCategory;

  // Refresh list on Swipe
  private void refreshShopCategory() {
    ModelManager.modelManager().getCatSubCategoryList(
        (Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<ShopCatSubCategory>> genricResponse) -> {
          inventoryList = genricResponse.getObject();
          inventoryAdapter.addItems(inventoryList);
          checkEmptyState();
          swipeRefreshLayout.setRefreshing(false);
        }, (Constants.Status iStatus, String message) -> {
          Toaster.toast(message);
          swipeRefreshLayout.setRefreshing(false);
        });

  }


  private void loadShopCategoryList() {
    ModelManager.modelManager().getCatSubCategoryList(
        (Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<ShopCatSubCategory>> genricResponse) -> {
          inventoryList = genricResponse.getObject();
          inventoryAdapter.addItems(inventoryList);
          checkEmptyState();
        }, (Constants.Status iStatus, String message) -> {
          Toaster.toast(message);
        });
  }


  private void showProgress(boolean b) {
    if (b) {
      progressBar.setVisibility(View.VISIBLE);
    } else {
      progressBar.setVisibility(View.GONE);
    }
  }

  private void checkEmptyState() {
    if (inventoryAdapter.getItemCount() > 0) {
      emptyView.setVisibility(View.GONE);
    } else {
      emptyView.setVisibility(View.VISIBLE);
    }
  }

  public class InventoryAdapter extends RecyclerView.Adapter<InventoryAdapter.ViewHolder> {

    private List<ShopCatSubCategory> inventoryList;

    private InventoryAdapter(List<ShopCatSubCategory> inventoryList) {
      this.inventoryList = inventoryList;
    }

    @Override
    public InventoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      View v = LayoutInflater.from(parent.getContext())
          .inflate(R.layout.row_inventrory_grid_base, parent, false);
      return new InventoryAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(InventoryAdapter.ViewHolder holder, int position) {
      ShopCatSubCategory shop = inventoryList.get(position);
      holder.bindContent(shop);
      String name = shop.getName();
      String imageURL = shop.getImageUrl();

      if (!imageURL.isEmpty()) {
        Picasso.with(getContext())
            .load(imageURL)
            .placeholder(R.drawable.img_product_placeholder)
            .into(holder.ivItemImage);
      } else {
        holder.ivItemImage.setImageDrawable(
            getContext().getResources().getDrawable(R.drawable.img_product_placeholder));
      }

      holder.tvItemName.setText(name);
    }

    @Override
    public int getItemCount() {
      return inventoryList.size();
    }

    public void addItems(List<ShopCatSubCategory> inventoryList) {
      this.inventoryList.clear();
      this.inventoryList.addAll(inventoryList);
      notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

      ShopCatSubCategory shopCategory;
      @BindView(R.id.iv_item_image)
      ImageView ivItemImage;
      @BindView(R.id.tv_item_name)
      TextView tvItemName;

      public ViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
      }

      public void bindContent(ShopCatSubCategory shop) {
        this.shopCategory = shop;
      }

      @OnClick(R.id.item_view)
      void onInventory() {
        if (switchListener != null) {
          switchListener.switchFragment(
              CategoryShopFeedFragment.newInstance(switchListener, shopCategory.getId()), true,
              true);
        }
      }
    }
  }
}

