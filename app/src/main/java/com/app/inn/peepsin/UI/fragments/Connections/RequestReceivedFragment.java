package com.app.inn.peepsin.UI.fragments.Connections;


import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.Connection;
import com.app.inn.peepsin.Models.Requests;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.fragments.Profile.UserProfileFragment;
import com.app.inn.peepsin.UI.helpers.DividerItemRecyclerDecoration;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kUserId;

/**
 * Created by akamahesh on 2/5/17.
 */

public class RequestReceivedFragment extends Fragment{
    private List<Requests> requestsList ;
    private RequestAdapter requestAdapter;
    private Paint p = new Paint();


    @BindView(R.id.recycler_view_requests) RecyclerView recyclerView;
    @BindView(R.id.empty_view) View emptyView;
    @BindView(R.id.tv_title) TextView tvToolbarTitle;
    @BindView(R.id.tv_empty_title) TextView tvEmptyViewText;
    @BindView(R.id.edt_search)
    EditText edtSearch;

    @BindView(R.id.progress_bar) ProgressBar progressBar;
    ProgressDialog progressDialog ;
    private String filterString;
    //AppBarLayout appbarLayout;
    public static Switcher switcherListener;

    public static Fragment newInstance(Switcher switcher,int tabselected){
        //return new RequestReceivedFragment();
        switcherListener = switcher;
        RequestReceivedFragment fragment = new RequestReceivedFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("tabSelected",tabselected);

        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestsList = new ArrayList<>();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_requests, container, false);
        ButterKnife.bind(this,view);
        /*appbarLayout=((HomeActivity) getActivity()).getTabLayout();
        Utils.isfriendClicked=1;*/
        tvToolbarTitle.setText(getString(R.string.title_requests_received));
        tvEmptyViewText.setText(getString(R.string.empty_title_no_request_received));
        progressDialog = Utils.generateProgressDialog(getContext(),true);

        requestAdapter = new RequestAdapter(requestsList);
        recyclerView.setAdapter(requestAdapter);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.addItemDecoration(new DividerItemRecyclerDecoration(getContext(), R.drawable.canvas_recycler_divider));

        swipeToDismissTouchHelper.attachToRecyclerView(recyclerView);
        edtSearch.setOnEditorActionListener(actionListener);
        edtSearch.addTextChangedListener(searchTextWatcher);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData();
    }

    TextView.OnEditorActionListener actionListener = (v, actionId, event) -> {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            Utils.hideKeyboard(getContext());
        }
        return true;
    };

    TextWatcher searchTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
            charSequence = charSequence.toString().toLowerCase();
            filterString = charSequence.toString().toLowerCase();
            requestAdapter.getFilter().filter(charSequence);
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };


    @OnClick(R.id.btn_back)
    void onBack(){
        getFragmentManager().popBackStack();
        /*appbarLayout.setVisibility(View.VISIBLE);
        Utils.isfriendClicked=0;*/
    }
    @Override
    public void onResume() {
        super.onResume();
        //appbarLayout.setVisibility(View.GONE);

    }
    public void loadData() {
        showProgress(true);
        ModelManager.modelManager().getRequestReceived((Constants.Status iStatus,GenricResponse<CopyOnWriteArrayList<Requests>> genricResponse) -> {
            requestAdapter.addItems(genricResponse.getObject());
            showProgress(false);
            checkEmptyScreen();
        }, (Constants.Status iStatus, String message) -> {
            Toaster.toast(message);
            showProgress(false);
            checkEmptyScreen();
        });
    }

    private void showProgressBar(boolean b) {
        if(b){
            progressBar.setVisibility(View.VISIBLE);
        }else{
            progressBar.setVisibility(View.GONE);
        }
    }

    private void checkEmptyScreen() {
        if(requestAdapter.getItemCount()>0){
            emptyView.setVisibility(View.GONE);
        }else {
            emptyView.setVisibility(View.VISIBLE);
        }
    }

    ItemTouchHelper swipeToDismissTouchHelper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            return false;
        }


        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            int swipedPosition = viewHolder.getAdapterPosition();
            Requests requests = requestAdapter.getItem(swipedPosition);
            if (direction == ItemTouchHelper.LEFT) {
                rejectRequest(requests,requests.getUserId(), swipedPosition);
            } else {
                acceptRequest(requests,requests.getUserId(), swipedPosition);
            }
        }

        @Override
        public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
            Bitmap icon;
            TextView tv;
            if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
                View itemView = viewHolder.itemView;
                float height = (float) itemView.getBottom() - (float) itemView.getTop();
                float width = height / 3;
                if (dX > 0) {
                    p.setColor(Color.parseColor("#388E3C"));
                    RectF background = new RectF((float) itemView.getLeft(), (float) itemView.getTop(), dX, (float) itemView.getBottom());
                    c.drawRect(background, p);
                    icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_check_white);
                    RectF icon_dest = new RectF((float) itemView.getLeft() + width, (float) itemView.getTop() + width, (float) itemView.getLeft() + 2 * width, (float) itemView.getBottom() - width);
                    c.drawBitmap(icon, null, icon_dest, p);
                } else {
                    p.setColor(Color.parseColor("#D32F2F"));
                    RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(), (float) itemView.getRight(), (float) itemView.getBottom());
                    c.drawRect(background, p);
                    icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_close_white_24dp);
                    RectF icon_dest = new RectF((float) itemView.getRight() - 2 * width, (float) itemView.getTop() + width, (float) itemView.getRight() - width, (float) itemView.getBottom() - width);
                    c.drawBitmap(icon, null, icon_dest, p);
                }
            }
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
        }
    });


    private void acceptRequest(Requests request,int userId, int swipedPosition) {
        showProgress(true);
        ModelManager.modelManager().requestReceivedAccept(userId, (Constants.Status iStatus) -> {
            showProgress(false);
            requestsList.remove(request);
            requestAdapter.notifyDataSetChanged();
            requestAdapter.getFilter().filter("");
            edtSearch.setText("");
            //requestAdapter.remove(swipedPosition);
            checkEmptyScreen();
        }, (Constants.Status iStatus, String message) -> {
            Toaster.toast(message);
            showProgress(false);
            requestAdapter.notifyDataSetChanged();
            checkEmptyScreen();
        });
    }

    private void rejectRequest(Requests request,int userId, int swipedPosition) {
        showProgress(true);
        ModelManager.modelManager().requestReceivedReject(userId, (Constants.Status iStatus) -> {
            showProgress(false);
            requestsList.remove(request);
            requestAdapter.notifyDataSetChanged();
            requestAdapter.getFilter().filter("");
            edtSearch.setText("");
            //requestAdapter.remove(swipedPosition);
            checkEmptyScreen();
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Toaster.toast(message);
            checkEmptyScreen();
            requestAdapter.notifyDataSetChanged();
        });
    }


    private void showProgress(boolean b) {
        if(b){
            progressDialog.show();
        }else {
            if(progressDialog!=null) progressDialog.cancel();
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        switcherListener = null;
    }




    class RequestAdapter extends RecyclerView.Adapter<RequestAdapter.MyViewHolder> {
        private List<Requests> requestList;
        private List<Requests> filterList;
        private ColorGenerator generator = ColorGenerator.MATERIAL;

        RequestAdapter(List<Requests> userList) {
            this.requestList = userList;
            this.filterList = userList;
        }

        @Override
        public RequestAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(getContext()).inflate(R.layout.row_request_layout, parent, false);
            return new RequestAdapter.MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(RequestAdapter.MyViewHolder holder, int position) {
            Requests request = requestList.get(position);
            holder.bindContent(request);
            int type= request.getMode();
            String name= request.getFullName();
            String contact = request.getContactId();
            String profileURL = request.getProfilePicURL();

            holder.tvName.setText(name);
            holder.tvContact.setText(contact);
            if (type == 1) {
                holder.tvStatus.setText(getString(R.string.sent_via_email_text));
                holder.ivStatusIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_email));
            } else if (type == 2) {
                holder.tvStatus.setText(getString(R.string.sent_via_phone_text));
                holder.ivStatusIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_phone));
            } else if (type == 3) {
                holder.tvStatus.setText(getString(R.string.sent_via_peeps_text));
                holder.ivStatusIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_eye_active));
            }else if (type == 4) {
                holder.tvStatus.setText(getString(R.string.sent_via_facebook_text));
                holder.ivStatusIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_facebook_verified));
            }


            if(profileURL.isEmpty()){
                holder.ivCircularUserImage.setVisibility(View.INVISIBLE);
                holder.ivUserImage.setVisibility(View.VISIBLE);
            }else {
                holder.ivCircularUserImage.setVisibility(View.VISIBLE);
                holder.ivUserImage.setVisibility(View.INVISIBLE);
            }

            TextDrawable drawable = TextDrawable.builder().buildRound(String.valueOf(name.toUpperCase().charAt(0)), generator.getRandomColor());

            if (!profileURL.isEmpty()) {
                Picasso.with(getContext())
                        .load(request.getProfilePicURL())
                        .resize(100,100)
                        .placeholder(R.drawable.img_profile_placeholder)
                        .into(holder.ivCircularUserImage);
            } else {
                holder.ivUserImage.setImageDrawable(drawable);
            }

        }

        Requests getItem(int index) {
            return requestList.get(index);
        }

        @Override
        public int getItemCount() {
            return requestList.size();
        }

        private Filter getFilter() {
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence charSequence) {

                    String charString = charSequence.toString();
                    if (charString.isEmpty()) {
                        requestList = filterList;
                    } else {
                        ArrayList<Requests> filteredList = new ArrayList<>();
                        for (Requests contacts : requestList) {
                            if (contacts.getFirstName().toLowerCase().contains(charString) || contacts.getContactId().toLowerCase().contains(charString)) {
                                filteredList.add(contacts);
                            }
                        }
                        requestList = filteredList;
                    }
                    FilterResults filterResults = new FilterResults();
                    filterResults.values = requestList;
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                    requestList = (List<Requests>) filterResults.values;
                    notifyDataSetChanged();
                    checkEmptyScreen();
                }
            };
        }

        public void addItems(List<Requests> requests) {
            requestList.clear();
            requestList.addAll(requests);
            notifyDataSetChanged();
        }

        public void remove(int swipedPosition) {
            requestList.remove(swipedPosition);
            notifyDataSetChanged();
        }


        class MyViewHolder extends RecyclerView.ViewHolder {
            private Requests requests;
            @BindView(R.id.iv_user_image)
            ImageView ivUserImage;
            @BindView(R.id.iv_user_image_circular) ImageView ivCircularUserImage;
            @BindView(R.id.iv_status_icon) ImageView ivStatusIcon;
            @BindView(R.id.tv_name) TextView tvName;
            @BindView(R.id.tv_contact) TextView tvContact;
            @BindView(R.id.tv_status) TextView tvStatus;

            MyViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this,itemView);
            }
            void bindContent(Requests requests){
                this.requests = requests;
            }

            @OnClick(R.id.item_view)
            void onItemView(){
                if(requests.getProfileVisibility()==-1)
                    Toaster.toastRangeen("User not registered");
                else if(requests.getProfileVisibility()==0)
                    Toaster.toastRangeen("You can’t see  profile");
                else if(requests.getProfileVisibility()==1)
                    getUserProfile(requests.getUserId());
            }

            void getUserProfile(int userId) {
                showProgress(true);
                HashMap<String, Object> parameters = new HashMap<>();
                parameters.put(kUserId,userId);
                ModelManager.modelManager().getUserProfile(parameters,(Constants.Status iStatus, GenricResponse<Connection> genricResponse) -> {
                    showProgress(false);
                    Connection connection = genricResponse.getObject();
                    if(connection.getProfileVisibility()!=0){
                        if(switcherListener!=null)
                            switcherListener.switchFragment(UserProfileFragment.newInstance(switcherListener,connection),true,true);
                    }else
                        Utils.showAlertDialog(getContext(),"Message","User Profile permission is denied");

                }, (Constants.Status iStatus, String message) -> {
                    Log.e("User Profile",message);
                    Toaster.toast(message);
                    showProgress(false);
                });
            }


        }
    }

}
