package com.app.inn.peepsin.UI.fragments.SignIn;


import static com.app.inn.peepsin.Constants.Constants.kAuthToken;
import static com.app.inn.peepsin.Constants.Constants.kEmail;
import static com.app.inn.peepsin.Constants.Constants.kOTP;
import static com.app.inn.peepsin.Constants.Constants.kPassword;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.activities.LogInActivity;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.helpers.Validations;
import java.util.HashMap;

/**
 * Created by akaMahesh on 2/5/17
 * contact : mckay1718@gmail.com
 */

public class SignInFragment extends Fragment {

  @BindView(R.id.edt_email)
  EditText editTextEmail;
  @BindView(R.id.edt_password)
  EditText editTextPwd;
  String mEmail;

  ProgressDialog progressDialog;
  private static OnSignInFragmentInteractionListener mListener;

  public static Fragment newInstance(String email) {
    Fragment fragment = new SignInFragment();
    Bundle bundle = new Bundle();
    bundle.putString(kEmail, email);
    fragment.setArguments(bundle);
    return fragment;
  }


  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    if (context instanceof OnSignInFragmentInteractionListener) {
      mListener = (OnSignInFragmentInteractionListener) context;
    } else {
      throw new RuntimeException(context.toString()
          + " must implement OnSignInFragmentInteractionListener");
    }
  }



  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mEmail = getArguments().getString(kEmail);
    mEmail = (mEmail == null) ? "" : mEmail;
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_sign_in, container, false);
    ButterKnife.bind(this, view);
    progressDialog = Utils.generateProgressDialog(getActivity(), true);
    editTextEmail.setKeyListener(null);
    editTextPwd.setOnEditorActionListener(onEditorActionListener);

    editTextPwd.setTransformationMethod(new PasswordTransformationMethod());
    editTextPwd.setTypeface(Typeface.DEFAULT);
    ((LogInActivity)getActivity()).setTitle("Sign In");
    editTextPwd.requestFocus();
    editTextEmail.setText(mEmail);
    return view;
  }

  @Override
  public void onDetach() {
    super.onDetach();
    mListener = null;
  }

  @OnClick(R.id.btn_login)
  public void login() {
    if (!validate()) {
      return;
    }
    if(mListener!=null)
      mListener.onSignIn(getLoginMap());
  }

  @OnClick(R.id.tv_forget_password)
  public void forgetPassword() {
    if(mListener!=null)
      mListener.moveToForgetPassword();
  }

  @OnClick(R.id.tv_login_with_otp)
  public void onLoginWithOTP() {
    editTextPwd.clearFocus();
    Utils.hideKeyboard(getContext());
    String contactID = Utils.getProperText(editTextEmail);
    ModelManager.modelManager().loginByOtp(contactID, (Constants.Status iStatus, GenricResponse<HashMap<String, Object>> genricResponse) -> {
          showProgress(false);
          HashMap<String, Object> hashMap = genricResponse.getObject();
          String authToken = (String) hashMap.get(kAuthToken);
          String OTP = (String) hashMap.get(kOTP);
          int type = 1;
          if (Validations.isValidEmail(contactID)) {
            type = 2;
          }
          if(mListener!=null)
            mListener.moveToOTPScreen(type, OTP, contactID, authToken);


        }, (Constants.Status iStatus, String message) -> {
          Utils.showAlertDialog(getContext(), "Otp Failed!", message);
        });

  }


  EditText.OnEditorActionListener onEditorActionListener = (v, actionId, event) -> {
    if (actionId == EditorInfo.IME_ACTION_GO) {
      Utils.hideKeyboard(getContext());
      login();
      return true;
    }
    return false;
  };


  public void showProgress(boolean b) {
    if (b) {
      progressDialog.show();
    } else if (progressDialog != null) {
      progressDialog.cancel();
    }
  }

  private boolean validate() {
    boolean isOk = true;
    EditText etValid;
    String testString;
    View requestFocus = null;

    etValid = editTextEmail;
    testString = etValid.getText().toString().toLowerCase().trim();
    if (testString.isEmpty()) {
      etValid.setError(getString(R.string.error_cannot_be_empty));
      requestFocus = etValid;
      isOk = false;
    } else if (!(Validations.isValidEmail(testString) || Validations.isValidPhone(testString))) {
      etValid.setError(getString(R.string.error_invalid_credential));
      requestFocus = etValid;
      isOk = false;
    }

    etValid = editTextPwd;
    testString = etValid.getText().toString().toLowerCase().trim();
    if (testString.isEmpty()) {
      etValid.setError(getString(R.string.error_cannot_be_empty));
      requestFocus = etValid;
      isOk = false;
    } else if (!Validations.isValidPassword(testString)) {
      etValid.setError(getString(R.string.error_invalid_password));
      requestFocus = etValid;
      isOk = false;
    }

    if (requestFocus != null) {
      requestFocus.requestFocus();
    }
    return isOk;
  }


  public HashMap<String, Object> getLoginMap() {
    HashMap<String, Object> loginMap = new HashMap<>();
    loginMap.put(kEmail, Utils.getProperText(editTextEmail));
    loginMap.put(kPassword, Utils.getProperText(editTextPwd));
    return loginMap;
  }

  public interface OnSignInFragmentInteractionListener {

    void onSignIn(HashMap<String, Object> loginMap);

    void moveToForgetPassword();

    void moveToOTPScreen(int type, String otp, String contactID, String authtoken);

    void onBack();
  }


}
