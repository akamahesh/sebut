package com.app.inn.peepsin.UI.fragments.Shop;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Blocks.Block;
import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Libraries.DispatchQueue.DispatchQueue;
import com.app.inn.peepsin.Managers.APIManager.ReachabilityManager;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.CurrentUser;
import com.app.inn.peepsin.Models.ProductType;
import com.app.inn.peepsin.Models.SharedProduct;
import com.app.inn.peepsin.Models.ShopModel;
import com.app.inn.peepsin.Models.UserLocation;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.Services.GPSTracker;
import com.app.inn.peepsin.UI.activities.LocationChangeActivity;
import com.app.inn.peepsin.UI.adapters.FavShopAdapter;
import com.app.inn.peepsin.UI.adapters.RecommendedAdapter;
import com.app.inn.peepsin.UI.adapters.ShopNearAdapter;
import com.app.inn.peepsin.UI.adapters.ShoppingCategoryAdapter;
import com.app.inn.peepsin.UI.adapters.TopShopAdapter;
import com.app.inn.peepsin.UI.fragments.Profile.ShoppingCartFragment;
import com.app.inn.peepsin.UI.helpers.ItemOffsetDecoration;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import com.lsjwzh.widget.recyclerviewpager.LoopRecyclerViewPager;
import com.lsjwzh.widget.recyclerviewpager.RecyclerViewPager;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;
import static android.view.View.GONE;
import static com.app.inn.peepsin.Constants.Constants.CART_UPDATE;
import static com.app.inn.peepsin.Constants.Constants.REQUEST_LOCATION;
import static com.app.inn.peepsin.Constants.Constants.kData;
import static com.app.inn.peepsin.Constants.Constants.kLatitude;
import static com.app.inn.peepsin.Constants.Constants.kLongitude;
import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by dhruv on 30/8/17.
 */

public class ShoppingFragment extends Fragment {

    private String TAG = getTag();
    private static Switcher switcherListener;
    @BindView(R.id.tv_location)
    TextView tvLocation;
    @BindView(R.id.tv_cartCount)
    TextView tvCartCount;
    @BindView(R.id.empty_view)
    View emptyView;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.shop_near_tag)
    TextView tvShopTag;
    @BindView(R.id.top_rated_tag)
    TextView topRatedTag;
    @BindView(R.id.favorite_tag)
    ImageView favoriteTag;
    @BindView(R.id.recommended_tag)
    ImageView recommendedTag;
    @BindView(R.id.empty_top_shop)
    View emptyShopView;
    @BindView(R.id.recycler_view_inventory)
    RecyclerView rvCategory;
    @BindView(R.id.recycler_view_shop)
    RecyclerView rvShop;
    @BindView(R.id.recycler_view_recommended)
    RecyclerView rvRecommended;
    @BindView(R.id.recycler_view_favourite)
    RecyclerView rvFavourite;
    @BindView(R.id.btn_show_more)
    Button btnMoreShop;
    @BindView(R.id.layoutDots)
    LinearLayout dotsLayout;

    @BindView(R.id.view_top_shops)
    View viewTopRated;
    @BindView(R.id.viewpager)
    LoopRecyclerViewPager rvTopShops;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;


    private int count = 0;

    private Dialog dialog = null;
    private String location = "No Location";
    private String latitude = "";
    private String longitude = "";

    private List<ShopModel> featureShopList;

    private ShoppingCategoryAdapter categoryAdapter;
    private List<ProductType> categoryList;

    private RecommendedAdapter productAdapter;
    private List<SharedProduct> productList;

    private FavShopAdapter favAdapter;
    private List<ShopModel> favShopList;

    private List<ShopModel> shopList;
    private ShopNearAdapter shopAdapter;
    private CurrentUser currentUser;

    public static Fragment newInstance(Switcher switcher) {
        switcherListener = switcher;
        return new ShoppingFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        currentUser = ModelManager.modelManager().getCurrentUser();
        UserLocation userLocation = currentUser.getUserLocation();
        featureShopList = currentUser.getFeatureShopList(5);
        categoryList = currentUser.getProductTypeList();
        productList = currentUser.getRecommendedList();
        favShopList = currentUser.getFavShopList();
        shopList = currentUser.getShopNearList(5);

        setAddressData(userLocation);
        if (userLocation != null) {
            getLocation(userLocation.getCity());
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_shopping_base_layout, container, false);
        ButterKnife.bind(this, view);
        Utils.hideKeyboard(getContext());

        // Category Adapter and Category List
        categoryAdapter = new ShoppingCategoryAdapter(getContext(), categoryList, switcherListener);
        rvCategory.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        setRVDefaults(rvCategory);
        rvCategory.addItemDecoration(new ItemOffsetDecoration(getContext(), R.dimen.item_offset));
        rvCategory.setAdapter(categoryAdapter);
        checkEmptyState();

        // Favourite Adapter and Favourite Shop List
        favAdapter = new FavShopAdapter(getContext(), favShopList, switcherListener, this);
        rvFavourite.setLayoutManager(
                new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        setRVDefaults(rvFavourite);
        rvFavourite.addItemDecoration(new ItemOffsetDecoration(getContext(), R.dimen.item_offset));
        rvFavourite.setAdapter(favAdapter);
        checkFavEmpty();

        // Recommended Adapter and Recommended List
        productAdapter = new RecommendedAdapter(getContext(), productList, switcherListener);
        rvRecommended.setLayoutManager(
                new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        setRVDefaults(rvRecommended);
        rvRecommended.setAdapter(productAdapter);
        rvRecommended.addItemDecoration(new ItemOffsetDecoration(getContext(), R.dimen.item_offset));
        checkRecEmpty();

        // Shop Near Adapter and Shop Near List
        shopAdapter = new ShopNearAdapter(getContext(), shopList, switcherListener, this);
        GridLayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 1);
        rvShop.setLayoutManager(new LinearLayoutManager(getContext()));
        rvShop.setNestedScrollingEnabled(false);
        rvShop.addItemDecoration(new ItemOffsetDecoration(getContext(), R.dimen.item_offset));
        rvShop.setAdapter(shopAdapter);
        initialState();
        // swipe to refresh the new list
        swipeRefreshLayout.setOnRefreshListener(onRefreshListener);
        swipeRefreshLayout.setProgressViewOffset(false, 0,200);

        return view;
    }


    private void setRVDefaults(RecyclerView rv) {
        rv.setHasFixedSize(true);
        rv.setNestedScrollingEnabled(false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setCartUpdate(currentUser.getShoppingCartCount());
        tvLocation.setText(location);
        if (featureShopList.isEmpty()) {
            loadTopShopList();
        } else {
            loadTopShopView();
        }
        checkLoadMoreButtonState();

    }


    SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            if (!ReachabilityManager.getNetworkStatus()) {
                Utils.showAlertDialog(getContext(), "Refresh Failed!", "Failed to reach PeepsIn servers. Please check your network or try again later.");
                swipeRefreshLayout.setRefreshing(false);
                return;
            }
            loadFavShopList();
            loadShopList();
            loadTopShopList();
            loadTopShopView();

        }
    };

    void checkLoadMoreButtonState() {
        if (shopAdapter.getItemCount() < 5) {
            btnMoreShop.setVisibility(GONE);
        } else {
            btnMoreShop.setVisibility(View.VISIBLE);
        }
    }

    public void setAddressData(UserLocation address) {
        if (address != null && !address.getCity().isEmpty()) {
            location = address.getCity() + ", " + address.getState() + ", " + address.getCountry();
            latitude = address.getLatitude();
            longitude = address.getLongitude();
        }
    }

    public void initialState() {
        int pageSize = shopList.size();
        if (pageSize == 0) {
            tvShopTag.setText(getString(R.string.no_shop_near_you));
            btnMoreShop.setVisibility(GONE);
            /*if(!latitude.isEmpty()&& !longitude.isEmpty())
                loadShopList();*/
        } else {
            tvShopTag.setText(getString(R.string.shop_near_you));
            if (shopList.size() > 4) {
                btnMoreShop.setVisibility(View.VISIBLE);
            }
        }
    }

    private void addBottomDots(int currentPage,int size) throws Exception {
        TextView[] dots = new TextView[size];
        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(getContext());
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(getResources().getColor(R.color.hint_color_regular));
            dotsLayout.addView(dots[i]);
        }
        if (dots.length > 0) {
            dots[currentPage].setTextColor(getResources().getColor(R.color.link_color));
        }
    }

    RecyclerViewPager.OnPageChangedListener recyclerChangeListener = new RecyclerViewPager.OnPageChangedListener() {
        @Override
        public void OnPageChanged(int i, int i1) {
            try {
                addBottomDots(rvTopShops.getActualCurrentPosition(),featureShopList.size());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };


    private void loadTopShopList() {
        ModelManager.modelManager().getFeatureShopList(1, latitude, longitude, (Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<ShopModel>> genericResponse) -> {
            if(genericResponse.getObject().size()>0){
                featureShopList.clear();
                CurrentUser currentUser = ModelManager.modelManager().getCurrentUser();
                featureShopList.addAll(currentUser.getFeatureShopList(5));
                loadTopShopView();
            }
        }, (Constants.Status iStatus, String message) -> {
            Log.e(TAG, message);
        });
    }

    private void loadTopShopView() {
        topRatedTag.setVisibility(View.VISIBLE);
        viewTopRated.setVisibility(View.VISIBLE);
        emptyShopView.setVisibility(GONE);

        LinearLayoutManager layout = new LinearLayoutManager(getContext(),
                LinearLayoutManager.HORIZONTAL, false);
        rvTopShops.setTriggerOffset(0.15f);
        rvTopShops.setFlingFactor(0.25f);
        rvTopShops.setLayoutManager(layout);
        rvTopShops.setAdapter(new TopShopAdapter(getContext(), featureShopList, switcherListener));
        rvTopShops.setHasFixedSize(true);
        rvTopShops.addOnPageChangedListener(recyclerChangeListener);

        try {
            addBottomDots(0,featureShopList.size());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void loadFavShopList() {
        ModelManager.modelManager().getFavouriteShopList((Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<ShopModel>> genericResponse) -> {
            favShopList.clear();
            favShopList.addAll(genericResponse.getObject());
            favAdapter.notifyDataSetChanged();
            checkFavEmpty();
        }, (Constants.Status iStatus, String message) -> {
            Log.e(TAG, message);
            checkFavEmpty();
        });
    }


    public void loadShopList() {
        HashMap<String, Object> map = new HashMap<>();
        map.put(kLatitude, latitude);
        map.put(kLongitude, longitude);
        ModelManager.modelManager().getShopList(map, 1, (Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<ShopModel>> genericResponse) -> {
            CurrentUser currentUser = ModelManager.modelManager().getCurrentUser();
            shopAdapter.addItems(currentUser.getShopNearList(5));
            checkLoadMoreButtonState();
            initialState();
            swipeRefreshLayout.setRefreshing(false);
        }, (Constants.Status iStatus, String message) -> {
            Log.e(TAG, message);
        });
    }

    public void makeshopFavorite(ProgressBar bar, ImageView view, Integer isFav, Integer shopId, int position) {
        showemptyProgress(bar, view, true);
        ModelManager.modelManager().setFavouriteShop(shopId, isFav, (Constants.Status iStatus) -> {
            shopList.get(position).setFavorite(isFav == 1);
            shopAdapter.notifyItemChanged(position);
            loadFavShopList();
            showemptyProgress(bar, view, false);
        }, (Constants.Status iStatus, String message) -> Toaster.toast(message));
    }


    @OnClick(R.id.btn_show_more)
    void seeAllShops() {
        if (switcherListener != null)
            switcherListener.switchFragment(ShopsFragment.newInstance(switcherListener, latitude, longitude), true, true);
    }

    @Override
    public void onResume() {
        super.onResume();
        // register cart update receiver
        LocalBroadcastManager.getInstance(getContext())
                .registerReceiver(mBroadcastReceiver, new IntentFilter(CART_UPDATE));
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getContext())
                .unregisterReceiver(mBroadcastReceiver);
    }

    BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(CART_UPDATE)) {
                setCartUpdate(intent.getIntExtra(kData, 0));
            }
        }
    };

    public void setCartUpdate(int count) {
        if (count == 0) {
            tvCartCount.setVisibility(GONE);
        } else {
            tvCartCount.setVisibility(View.VISIBLE);
            tvCartCount.setText(String.valueOf(count));
        }
    }

    @OnClick(R.id.btn_cart)
    void onCart() {
        if (switcherListener != null) {
            switcherListener.switchFragment(ShoppingCartFragment.newInstance(switcherListener), true, true);
        }
    }

    @OnClick(R.id.tv_location)
    void locationChange() {
        if (dialog == null) {
            SpannableStringBuilder builder = new SpannableStringBuilder();
            SpannableString str1 = new SpannableString("It seems like you are not in ");
            builder.append(str1);

            SpannableString strloc = new SpannableString(location);
            strloc.setSpan(new ForegroundColorSpan(Color.parseColor("#29b6f6")), 0, strloc.length(), 0);
            builder.append(strloc);

            SpannableString str2 = new SpannableString(" . Do you want to change your location");
            builder.append(str2);

            dialog = new Dialog(getContext(), R.style.PauseDialog);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.dialog_change_location_prompt);
            TextView tvAddress = ButterKnife.findById(dialog, R.id.tv_address);
            tvAddress.setText(builder, TextView.BufferType.SPANNABLE);
            ButterKnife.findById(dialog, R.id.btn_change).setOnClickListener(v -> {
                Intent intent = new Intent(getActivity(), LocationChangeActivity.class);
                startActivityForResult(intent, REQUEST_LOCATION);
                dialog.dismiss();
            });
            ButterKnife.findById(dialog, R.id.btn_continue).setOnClickListener(v -> dialog.dismiss());
            dialog.show();
        } else if (!dialog.isShowing()) {
            dialog = null;
            locationChange();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_LOCATION) {
            if (resultCode == RESULT_OK) {
                // Get the user's selected place from the Intent.
                String locationName = data.getStringExtra("name");
                tvLocation.setText(locationName);
                latitude = data.getStringExtra("lat");
                longitude = data.getStringExtra("lng");
                Log.i(TAG,
                        "Place Selected: " + locationName + "\n" + "LatLong: " + latitude + "," + longitude);
                loadTopShopList();
                loadShopList();
            }
        }
    }

    public void getLocation(String city) {
        GPSTracker gps = new GPSTracker(getContext());
        // check if GPS enabled
        if (gps.canGetLocation()) {
            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();
            getAddress(city, latitude, longitude);
        } else {
            Toaster.kalaToast("Please enable GPS to get near by Shops");
        }
    }

    public void getAddress(String city, double latitude, double longitude) {
        getFullAddress(latitude, longitude,
                (Constants.Status iStatus, GenricResponse<String> genericResponse) -> {
                    if (!genericResponse.getObject().equals(city)) {
                        locationChange();
                    }
                }, (Constants.Status iStatus, String message) -> {
                    Toaster.kalaToast("Sorry unable to get current location.");
                });
    }

    public void getFullAddress(double latitude, double longitude, Block.Success<String> success, Block.Failure failure) {
        ModelManager.modelManager().getDispatchQueue().async(() -> {
            try {
                String city = "";
                Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                List<Address> listAddresses = geocoder.getFromLocation(latitude, longitude, 1);
                if (null != listAddresses && listAddresses.size() > 0) {
                    city = listAddresses.get(0).getLocality();
                }
                city = (city == null) ? "" : city;
                GenricResponse<String> response = new GenricResponse<>(city);
                DispatchQueue.main(() -> success.iSuccess(Constants.Status.success, response));
            } catch (Exception e) {
                DispatchQueue.main(() -> failure.iFailure(Constants.Status.fail, e.getMessage()));
            }
        });
    }

    private void showProgress(boolean b) {
        if (b) {
            progressBar.setVisibility(View.VISIBLE);
            //swipeRefreshLayout.setRefreshing(true);
            //progressDialog.show();
        } else {
            progressBar.setVisibility(GONE);
            //swipeRefreshLayout.setRefreshing(false);
            //if (progressDialog != null) progressDialog.cancel();
        }
    }


    private void checkEmptyState() {
        if (categoryAdapter.getItemCount() > 0) {
            emptyView.setVisibility(GONE);
        } else {
            emptyView.setVisibility(View.VISIBLE);
        }
    }

    public void checkFavEmpty() {
        if (favAdapter.getItemCount() > 0) {
            favoriteTag.setVisibility(View.VISIBLE);
        } else {
            favoriteTag.setVisibility(GONE);
        }
    }

    private void checkRecEmpty() {
        if (productAdapter.getItemCount() > 0) {
            recommendedTag.setVisibility(View.VISIBLE);
        } else {
            recommendedTag.setVisibility(GONE);
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        switcherListener = null;
    }

    private void showemptyProgress(ProgressBar bar, ImageView view, boolean b) {
        if (b) {
            view.setVisibility(View.INVISIBLE);
            bar.setVisibility(View.VISIBLE);
            bar.getIndeterminateDrawable().setColorFilter(Color.RED, PorterDuff.Mode.MULTIPLY);
            //progressDialog.show();
        } else {
            bar.setVisibility(GONE);
            view.setVisibility(View.VISIBLE);

            //if (progressDialog != null) progressDialog.cancel();
        }
    }

    public void refreshShopList(Integer id) {
        for (ShopModel shop : shopList) {
            if (shop.getId().equals(id)) {
                shop.setFavorite(false);
            }
        }
        shopAdapter.notifyDataSetChanged();
    }
}

