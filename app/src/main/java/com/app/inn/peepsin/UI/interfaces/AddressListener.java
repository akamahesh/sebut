package com.app.inn.peepsin.UI.interfaces;

import android.support.v4.app.Fragment;

import com.app.inn.peepsin.Models.Address;

/**
 * Created by root on 10/7/17.
 */

public interface AddressListener {
    void switchFragment(Fragment fragment, boolean s, boolean a);
    void address(Address address);
    void onBack();
}
