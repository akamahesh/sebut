package com.app.inn.peepsin.Models;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by dhruv on 29/9/17.
 */

public class CelebrationMessage extends BaseModel implements Serializable {

    private int messageId;
    private String messageTitle;
    private String message;
    private long schedulingTimeStamp;
    private String eventImageUrl;
    private String senderImageUrl;
    private String senderName;
    private String jabberId;
    private Boolean isFavorite;

    public CelebrationMessage(JSONObject jsonResponse){
        this.messageId = getValue(jsonResponse,kMSGId, Integer.class);
        this.messageTitle = getValue(jsonResponse,kMessageTitle, String.class);
        this.message = getValue(jsonResponse,kMessage, String.class);
        this.schedulingTimeStamp = getValue(jsonResponse,kSchedulingTimestamp, Long.class);
        this.eventImageUrl = getValue(jsonResponse,kEventCelebrationimageURl, String.class);
        this.senderImageUrl = getValue(jsonResponse,kSenderImageUrl, String.class);
        this.senderName = getValue(jsonResponse,kSenderName, String.class);
        this.jabberId   = getValue(jsonResponse, kJabberId,  String.class);
        this.isFavorite = getValue(jsonResponse,kIsFavorite, Boolean.class);
    }

    public int getMessageId() {
        return messageId;
    }

    public String getMessageTitle() {
        return messageTitle;
    }

    public String getMessage() {
        return message;
    }

    public long getSchedulingTimeStamp() {
        return schedulingTimeStamp;
    }

    public String getEventImageUrl() {
        return eventImageUrl;
    }

    public String getSenderImageUrl() {
        return senderImageUrl;
    }

    public String getSenderName() {
        return senderName;
    }

    public String getJabberId() {
        return jabberId;
    }

    public Boolean getFavorite() {
        return isFavorite;
    }

    public void setFavorite(Boolean favorite) {
        isFavorite = favorite;
    }
}
