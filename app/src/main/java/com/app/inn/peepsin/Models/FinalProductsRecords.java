package com.app.inn.peepsin.Models;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by root on 18/5/17.
 */

public class FinalProductsRecords extends BaseModel implements Serializable{

    private Integer shopId;
    private String shopName;
    private String shopBannerImageURL;
    private String ownerName;
    private Integer rating;
    private Integer connectionLevel;
    private String totalAmountWithoutDiscount;
    private String netPaybaleAmount;
    private String invoiceURL;
    private CopyOnWriteArrayList<ProductList> productList;
    private ShopAddress shopAddress;
    private Integer shopCount;
    //private String checksumHash;


    public FinalProductsRecords(JSONObject records) {
        this.shopCount          = records.length();
        this.shopId             = getValue(records,kShopId,             Integer.class);
        this.shopName           = getValue(records,kshopName,           String.class);
        this.shopBannerImageURL = getValue(records,kShopBannerImageURL, String.class);
        this.ownerName          = getValue(records,kOwnerName,          String.class);
        this.rating             = getValue(records,kRating,             Integer.class);
        this.connectionLevel    = getValue(records,kConnectionLevel,    Integer.class);
        this.totalAmountWithoutDiscount = getValue(records,kTotalAmountWithoutDiscount,String.class);
        this.netPaybaleAmount   = getValue(records,kNetPaybaleAmount,    String.class);
        try {
            this.invoiceURL             = getValue(records, kInvoiceURL,     String.class);
        }catch (Exception e){
            Log.e("Error : ",e.getMessage());
        }
        try {
            this.shopAddress            = new ShopAddress(getValue(records, kShopAddress,   JSONObject.class));
        }catch (Exception e){
            Log.e("Error : ",e.getMessage());
        }
        try { //to handle @ClassCastException when addressId is emptyString, not to be removed
            this.productList            = handleShopList(getValue(records, kProductList,   JSONObject.class));
        }catch (Exception e){
            Log.e("Error : ",e.getMessage());
        }
    }

    public Integer getShopId() {
        return shopId;
    }

    public ShopAddress getShopAddress() {
        return shopAddress;
    }

    public Integer getShopCount() {
        return shopCount;
    }

    public Integer getRating() {
        return rating;
    }

    public String getshopName() {
        return shopName;
    }

    public String getshopBannerImageURL() {
        return shopBannerImageURL;
    }

    public String getownerName() {
        return ownerName;
    }

    public Integer getconnectionLevel() {
        return connectionLevel;
    }

    public String gettotalAmountWithoutDiscount() {
        return totalAmountWithoutDiscount;
    }

    public String getnetPaybaleAmount() {
        return netPaybaleAmount;
    }

    public String getInvoiceURL() {
        return invoiceURL;
    }

    private CopyOnWriteArrayList<ProductList> handleShopList(JSONArray jsonArray) throws JSONException {
        CopyOnWriteArrayList<ProductList> billShopList = new CopyOnWriteArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            billShopList.add(new ProductList(jsonArray.getJSONObject(i)));
        }

        return billShopList;
    }

    public CopyOnWriteArrayList<ProductList> getProductRecords() {
        return productList;
    }



}
