package com.app.inn.peepsin.Libraries.Firebase.service;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.app.inn.peepsin.Libraries.Firebase.app.Config;


/**
 * Created by mahesh on 23/3/17.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = MyFirebaseInstanceIDService.class.getSimpleName();

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken= FirebaseInstanceId.getInstance().getToken();
        Log.v(TAG, "sendRegistrationToServer: " + refreshedToken);

        //saving reg id to shared preference
        storeRegistrationIdPref(refreshedToken);

        //Notify UI that registraiton has completed , so the progress indicator can be hiddern
        Intent registrationCompleteIntent = new Intent(Config.REGISTRATION_COMPLETE);
        registrationCompleteIntent.putExtra("token",refreshedToken);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationCompleteIntent);

    }



    private void storeRegistrationIdPref(String token) {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("regId", token);
        editor.commit();
    }

}
