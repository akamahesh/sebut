package com.app.inn.peepsin.UI.fragments.Shop;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.inn.peepsin.R;
import com.app.inn.peepsin.Services.PublishAdService;
import com.app.inn.peepsin.UI.activities.AddProductActivity;
import com.app.inn.peepsin.UI.activities.HomeActivity;
import com.app.inn.peepsin.UI.fragments.SellStuffModule.CameraPreviewFragment;
import com.app.inn.peepsin.UI.helpers.FragmentUtil;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static com.app.inn.peepsin.Constants.Constants.PRODUCT_RESULT;
import static com.app.inn.peepsin.Constants.Constants.kData;
import static com.app.inn.peepsin.Constants.Constants.kProductImageUrlList;
import static com.app.inn.peepsin.Constants.Constants.kRecords;
import static com.app.inn.peepsin.Constants.Constants.kType;

/**
 * Created by dhruv on 18/8/17.
 */

public class ProductIntermediateFragment extends Fragment {

    private static Switcher switchListener;
    private String TAG = getClass().getName();

    @BindView(R.id.iv_stroller)
    ImageView ivStroller;
    @BindView(R.id.vg_stroller)
    ViewGroup vgStroller;
    @BindView(R.id.btn_yes)
    Button yesBtn;
    @BindView(R.id.empty_skill_view)
    TextView tvHead;
    @BindView(R.id.tv_empty_message)
    TextView tvMessage;

    private final int REQUEST_PERMISSION_CAMERA_STORAGE = 101;

    public static Fragment newInstance(Switcher switcher) {
        switchListener = switcher;
        return new ProductIntermediateFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_shop_intermediate, container, false);
        ButterKnife.bind(this, view);
        Utils.hideKeyboard(getContext());
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        String tag = TAG;
        tvHead.setText(getString(R.string.want_to_add_product_to_shop));
        tvMessage.setText(getString(R.string.message_add_product));
        yesBtn.setText(getString(R.string.want_to_add_product));

        ivStroller.setVisibility(View.VISIBLE);
   /* final Handler handler = new Handler();
    handler.postDelayed(() -> {
      TransitionManager.beginDelayedTransition(vgStroller, new Slide(Gravity.START));
      ivStroller.setVisibility(View.VISIBLE);
    }, 1000);*/

    }

    @OnClick(R.id.btn_yes)
    void onPositive() {
        checkPermission();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PRODUCT_RESULT && resultCode == Activity.RESULT_OK) {
            HashMap<String, Object> productMap = (HashMap<String, Object>) data.getSerializableExtra(kData);
            List<String> imagePathList = (List<String>) data.getSerializableExtra(kProductImageUrlList);
            publishAd(productMap, imagePathList);
        }
    }

    @OnClick(R.id.btn_no)
    void onNegative() {
        startActivity(HomeActivity.getIntent(getContext()));
    }

    private void publishAd(HashMap<String, Object> productMap, List<String> imagePathList) {
        Intent intent = new Intent(getContext(), PublishAdService.class);
        // add infos for the service which file to download and where to store
        intent.putExtra(kRecords, productMap);
        intent.putExtra(kProductImageUrlList, (Serializable) imagePathList);
        getActivity().startService(intent);
        startActivity(HomeActivity.getIntent(getContext()));
    }

    private void checkPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(), CAMERA) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(getActivity(), READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            productAdd();
        } else if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), CAMERA) && ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), READ_EXTERNAL_STORAGE)) {
            // We've been denied once before. Explain why we need the permission, then ask again.
            Utils.showDialog(getContext(), "Camera & Gallary permissions are required to upload profile images!", "Ask Permission", "Discard", (dialog, which) -> {
                if (which == -1)
                    requestPermissions(new String[]{CAMERA, READ_EXTERNAL_STORAGE}, REQUEST_PERMISSION_CAMERA_STORAGE);
                else
                    dialog.dismiss();
            });
        } else {
            // We've never asked. Just do it.
            requestPermissions(new String[]{CAMERA, READ_EXTERNAL_STORAGE}, REQUEST_PERMISSION_CAMERA_STORAGE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        try {
            if (requestCode == REQUEST_PERMISSION_CAMERA_STORAGE && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                productAdd();
            } else {
                // We were not granted permission this time, so don't try to show the contact picker
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }catch (Exception e){
            e.printStackTrace();
            Toaster.toast(e.getMessage());
        }

    }

    void productAdd(){
       startActivityForResult(AddProductActivity.getIntent(getContext()), PRODUCT_RESULT);
    }
}
