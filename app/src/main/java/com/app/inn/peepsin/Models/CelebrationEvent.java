package com.app.inn.peepsin.Models;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by dhruv on 25/9/17.
 */

public class CelebrationEvent extends BaseModel implements Serializable {

    private int userId;
    private int eventId;
    private int eventType;
    private String eventTypeName;
    private String eventTitle;
    private long schedulingTimeStamp;
    private String timeZone;
    private int notifyMeDuration;
    private String imageUrl;
    private String message;
    private int repetitionInterval;
    private transient JSONArray celebrationMode;

    public CelebrationEvent(int userId,int eventId,int eventType,String eventTypeName,String eventTitle
            ,long schedulingTimeStamp,String timeZone,String imageUrl,String message,int notifyMeDuration, int repetitionInterval){
        this.userId        = userId;
        this.eventId       = eventId;
        this.eventType     = eventType;
        this.eventTypeName = eventTypeName;
        this.eventTitle    = eventTitle;
        this.schedulingTimeStamp = schedulingTimeStamp;
        this.timeZone = timeZone;
        this.imageUrl = imageUrl;
        this.message  = message;
        this.notifyMeDuration   = notifyMeDuration;
        this.repetitionInterval = repetitionInterval;
    }

    public CelebrationEvent(JSONObject jsonResponse){
        this.userId = getValue(jsonResponse,kUserId,  Integer.class);
        this.eventId = getValue(jsonResponse,kEventId,  Integer.class);
        this.eventType = getValue(jsonResponse,kEventType, Integer.class);
        this.eventTypeName = getValue(jsonResponse,kEventTypeName, String.class);
        this.eventTitle = getValue(jsonResponse, kEventTitle, String.class);
        this.schedulingTimeStamp = getValue(jsonResponse, kSchedulingTimestamp, Long.class);
        this.timeZone = getValue(jsonResponse,kTimeZone, String.class);
        this.imageUrl = getValue(jsonResponse,kImageURL, String.class);
        this.message = getValue(jsonResponse,kMessage, String.class);
        this.notifyMeDuration = getValue(jsonResponse,kNotifyMeDuration, Integer.class);
        this.repetitionInterval = getValue(jsonResponse, kRepetitionInterval, Integer.class);
        try {
            this.celebrationMode = getValue(jsonResponse, kCelebrationMode, JSONArray.class);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public int getUserId() {
        return userId;
    }

    public int getEventId() {
        return eventId;
    }

    public int getEventType() {
        return eventType;
    }

    public String getEventTypeName() {
        return eventTypeName;
    }

    public String getEventTitle() {
        return eventTitle;
    }

    public Long getSchedulingTimeStamp() {
        return schedulingTimeStamp;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public int getNotifyMeDuration() {
        return notifyMeDuration;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getMessage() {
        return message;
    }

    public int getRepetitionInterval() {
        return repetitionInterval;
    }

    public JSONArray getCelebrationMode() {
        return celebrationMode;
    }
}
