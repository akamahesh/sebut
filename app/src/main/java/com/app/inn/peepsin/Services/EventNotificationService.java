package com.app.inn.peepsin.Services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.app.inn.peepsin.DataBase.DataBaseHandler;
import com.app.inn.peepsin.Managers.EventManager.EventBroadcastManager;
import com.app.inn.peepsin.Models.CelebrationEvent;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.app.inn.peepsin.Constants.Constants.kData;
import static com.app.inn.peepsin.Constants.Constants.kEventMap;
import static com.app.inn.peepsin.Managers.BaseManager.ApplicationManager.getContext;

public class EventNotificationService extends Service {
    private static final String TAG=EventNotificationService.class.getSimpleName();
    final static String ACTION = "NotifyServiceReceiver";
    private boolean isRunning  = false;
    private boolean isStopped  = false;
    private List<CelebrationEvent> events;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.i(TAG, "Service onBind");
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        isRunning=true;
        DataBaseHandler dbHandler = new DataBaseHandler(getContext());
        events = dbHandler.getEventsFromDatabase();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG,"Service onStartCommand");
        try{
            if (intent.getStringExtra("RQS").equals(ACTION)){
                stopSelf();
                isStopped = true;
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        if(isRunning){
            if(events.size()==0){
                stopSelf();
                isStopped = true;
            }else {
                for(int i=0;i<events.size();i++){
                    notifyEvent(events.get(i));
                }
            }
        }

        return Service.START_STICKY;
    }

    public void notifyEvent(CelebrationEvent event){
        AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
        Intent intent = new Intent(getApplicationContext(), EventBroadcastManager.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(kEventMap,event);
        intent.putExtra(kData,bundle);
        intent.setAction("event data");
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(),0,intent,PendingIntent.FLAG_UPDATE_CURRENT);

        Calendar calendar = Calendar.getInstance();
        Date date = null;
        switch (event.getNotifyMeDuration()){
            case 1:// notify before 15 min
                date = new Date(event.getSchedulingTimeStamp()-(15*60*1000));
                break;
            case 2:// notify before 1 Hr
                date = new Date(event.getSchedulingTimeStamp()-(60*60*1000));
                break;
            case 3:// notify before 1 day
                date = new Date(event.getSchedulingTimeStamp()-(24*60*60*1000));
                break;
        }
        calendar.setTime(date);

        switch (event.getRepetitionInterval()){
            case 1: am.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
                break;
            case 2: am.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
                break;
            case 3:
                int days = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
                am.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY*days, pendingIntent);
                break;
            case 4:
                if(calendar.getActualMaximum(Calendar.DAY_OF_YEAR) > 365)
                    am.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY*365, pendingIntent);
                else
                    am.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY*366, pendingIntent);
                break;
        }

    }

    public void manageNotification(){
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        Intent myIntent = new Intent(getApplicationContext(), EventNotificationService.class);
        PendingIntent pendingIntent = PendingIntent.getService(getApplicationContext(), 0, myIntent, 0);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.add(Calendar.SECOND, 5);
        alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Log.i(TAG, "Service onLowMemory");
        stopSelf();
        manageNotification();
    }

    @Override
    public void onDestroy() {
        isRunning = false;
        Log.i(TAG, "Service onDestroy");
        if(isStopped){
            stopSelf();
        }
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        isRunning = false;
        Log.i(TAG, "Service onTaskRemoved");
        if(isStopped){
            stopService(rootIntent);
        }
    }
}
