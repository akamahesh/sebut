package com.app.inn.peepsin.Models;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Harsh on 8/22/2017.
 */

public class ShopCatSubCategory extends BaseModel implements Serializable {

    private Integer id;
    private String name;
    private String imageUrl;
    private boolean isSelected=false;
    private CopyOnWriteArrayList<SubCategory> subCategories;


    public ShopCatSubCategory(JSONObject jsonResponse) {
        this.id = getValue(jsonResponse, kCategoryId, Integer.class);
        this.name = getValue(jsonResponse, kCategory, String.class);
        this.imageUrl = getValue(jsonResponse, kCategoryImageURL, String.class);
        try {
            this.subCategories = handleCategoryRecords(getValue(jsonResponse, ksubCategoryList, JSONObject.class));
        } catch (Exception e) {
            Log.e("Error : ", e.getMessage());
        }
    }
    private CopyOnWriteArrayList<SubCategory> handleCategoryRecords(JSONArray jsonArray) throws JSONException {
        CopyOnWriteArrayList<SubCategory> subCategoriesList = new CopyOnWriteArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            subCategoriesList.add(new SubCategory(jsonArray.getJSONObject(i)));
        }
        return subCategoriesList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public CopyOnWriteArrayList<SubCategory> getSubCategories() {
        return subCategories;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
