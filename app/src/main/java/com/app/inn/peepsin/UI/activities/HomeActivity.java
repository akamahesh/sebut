
package com.app.inn.peepsin.UI.activities;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Constants.Constants.DeviceType;
import com.app.inn.peepsin.Libraries.Firebase.app.Config;
import com.app.inn.peepsin.Managers.APIManager.ReachabilityManager;
import com.app.inn.peepsin.Managers.BaseManager.ApplicationManager;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Managers.XMPPManager.Constant;
import com.app.inn.peepsin.Managers.XMPPManager.RoosterManager;
import com.app.inn.peepsin.Models.ChatProduct;
import com.app.inn.peepsin.Models.CheckLatestBuild;
import com.app.inn.peepsin.Models.CurrentUser;
import com.app.inn.peepsin.Models.ShoppingCart;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.fragments.Base.RootBoardFragment;
import com.app.inn.peepsin.UI.fragments.Base.RootConnectionFragment;
import com.app.inn.peepsin.UI.fragments.Base.RootMessageFragment;
import com.app.inn.peepsin.UI.fragments.Base.RootProfileFragment;
import com.app.inn.peepsin.UI.fragments.Base.RootShopFragment;
import com.app.inn.peepsin.UI.fragments.SellStuffModule.SellingStuffDetailFragment;
import com.app.inn.peepsin.UI.helpers.CustomViewPager;
import com.app.inn.peepsin.UI.helpers.ProgressUtil;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.google.firebase.messaging.FirebaseMessaging;
import com.special.ResideMenu.ResideMenu;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import static com.app.inn.peepsin.Constants.Constants.PROGRESS_UPDATE;
import static com.app.inn.peepsin.Constants.Constants.kBody;
import static com.app.inn.peepsin.Constants.Constants.kBuildNumber;
import static com.app.inn.peepsin.Constants.Constants.kCurrentVersion;
import static com.app.inn.peepsin.Constants.Constants.kDeviceType;
import static com.app.inn.peepsin.Constants.Constants.kMessage;
import static com.app.inn.peepsin.Constants.Constants.kTitle;

public class HomeActivity extends BaseActivity implements
        TabLayout.OnTabSelectedListener,
        SellingStuffDetailFragment.FragmentInteractionListener,
        ReachabilityManager.ConnectivityReceiverListener {

    private String TAG = getClass().getName();

    private BroadcastReceiver mRegistrationBroadcastReceiver;
    /**
     * Indicate that app will be closed on next back press
     */
    private boolean isAppReadyToFinish = false;

    @BindView(R.id.view_pager)
    CustomViewPager viewPager;
    @BindView(R.id.progress_overlay)
    View progressOverlay;
    @BindView(R.id.buttonBack)
    Button sidebtn;
    @BindView(R.id.overlay_connectivity)
    View overlayConnectivity;

    SectionsPagerAdapter sectionsPagerAdapter;

    ResideMenu resideMenu;
    @BindView(R.id.appBarLayout)
    AppBarLayout appBarLayout;
    @BindView(R.id.btn_edit)
    ImageButton editMenu;

    @BindView(R.id.tabs)
    TabLayout tabLayout;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.toolbar)
    View view;
    @BindView(R.id.tv_cartCount)
    TextView tvCartCount;

    @BindColor(R.color.navigation_active_tab)
    int activeColor;
    @BindColor(R.color.navigation_inactive_tab)
    int inActiveColor;

    private int[] activeImageResId = {
            R.drawable.ic_home,
            R.drawable.ic_connections,
            R.drawable.ic_dashboard_active_24dp,
            R.drawable.ic_messages,
            R.drawable.ic_account};

    private int[] inactiveImageResId = {
            R.drawable.ic_home_inactive,
            R.drawable.ic_connections_inactive,
            R.drawable.ic_dashboard_grey_24dp,
            R.drawable.ic_messages_inactive,
            R.drawable.ic_account_inactive};

    public static Intent getIntent(Context context) {
        return new Intent(context, HomeActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        CurrentUser currentUser = ModelManager.modelManager().getCurrentUser();
        int size = Utils.getObjectSize(currentUser);
        handleFirebase();
        displayFirebaseRegID();
        //Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));


        viewPager.setPagingEnabled(false);
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons(tabLayout);
        tabLayout.addOnTabSelectedListener(this);

        RoosterManager.roost().initialize();

        tv_title.setText(getString(R.string.title_shop));
        editMenu.setVisibility(View.GONE);
        /*CurrentUser currentUser = ModelManager.modelManager().getCurrentUser();
        setCartUpdate(currentUser.getShoppingCartCount());*/
        updateCart();

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {
            }

            @Override
            public void onPageSelected(int i) {
                if (i == 4) {
                    Intent registrationCompleteIntent = new Intent(PROGRESS_UPDATE);
                    LocalBroadcastManager.getInstance(ApplicationManager.getContext())
                            .sendBroadcast(registrationCompleteIntent);
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {
            }
        });

        final Handler handler = new Handler();
        handler.postDelayed(() -> {
            getLatestBuildInfo();
        }, 6000);

    }

    // Showing the status in Snackbar
    private void showSnack(boolean isConnected) {
        if (!isConnected) {
            ProgressUtil.animateView(overlayConnectivity, View.VISIBLE, 1, 200);
        } else {
            ProgressUtil.animateView(overlayConnectivity, View.GONE, 0, 200);
        }
    }

    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        if (!ModelManager.modelManager().getCurrentUser().getAccountActivated()) {
            Toaster.toastRangeen("Activate your account to get visible in PeepsIn");
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
    }

    private void setupTabIcons(TabLayout tabLayout) {
        tabLayout.getTabAt(0).setCustomView(R.layout.tab_feeds_layout);
        tabLayout.getTabAt(1).setCustomView(R.layout.tab_connection_layout);
        tabLayout.getTabAt(2).setCustomView(R.layout.tab_search_layout);
        tabLayout.getTabAt(3).setCustomView(R.layout.tab_chat_layout);
        tabLayout.getTabAt(4).setCustomView(R.layout.tab_profile_layout);
    }


    public void setupViewPager(ViewPager viewPager) {
        sectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        sectionsPagerAdapter.addFragment(RootShopFragment.newInstance());
        sectionsPagerAdapter.addFragment(RootConnectionFragment.newInstance());
        sectionsPagerAdapter.addFragment(RootBoardFragment.newInstance());
        sectionsPagerAdapter.addFragment(RootMessageFragment.newInstance());
        sectionsPagerAdapter.addFragment(RootProfileFragment.newInstance());
        viewPager.setAdapter(sectionsPagerAdapter);
        viewPager.setOffscreenPageLimit(5);
    }


    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        int position = tab.getPosition();
        View view = tabLayout.getTabAt(position).getCustomView();
        TextView tvTab = (TextView) view.findViewById(R.id.tv_tab);
        ImageView ivTab = (ImageView) view.findViewById(R.id.iv_tab);
        ivTab.setImageResource(activeImageResId[position]);
        //tvTab.setTextColor(ContextCompat.getColor(this, R.color.navigation_active_tab));
        tvTab.setTextColor(activeColor);
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
        int position = tab.getPosition();
        View view = tabLayout.getTabAt(position).getCustomView();
        TextView tvTab = (TextView) view.findViewById(R.id.tv_tab);
        ImageView ivTab = (ImageView) view.findViewById(R.id.iv_tab);
        ivTab.setImageResource(inactiveImageResId[position]);
        tvTab.setTextColor(inActiveColor);
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
        Log.d(TAG, "onTabReselected: ");
    }


    @Override
    public void onEditProgress() {
        ProgressUtil.animateView(progressOverlay, View.VISIBLE, 1, 200);
    }

    @Override
    public void onEditDone() {
        ProgressUtil.animateView(progressOverlay, View.GONE, 0, 200);
    }

    public TabLayout getTabLayout() {
        return (TabLayout) findViewById(R.id.tabs);
    }

    public ViewPager getViewPager() {
        return viewPager;
    }

    public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();

        SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @Override
        public int getItemPosition(Object object) {
            return super.getItemPosition(object);
        }

        void addFragment(Fragment fragment) {
            mFragmentList.add(fragment);
        }
    }

    @Override
    public void onBackPressed() {

        if (getSupportFragmentManager().getBackStackEntryCount() > 0 || isAppReadyToFinish) {
            finishAffinity();
        } else {
            // BackStack is empty. For closing the app user have to tap the back button two times in two seconds.
            Toaster.toast(getString(R.string.another_click_for_leaving_app));
            isAppReadyToFinish = true;
            new Handler().postDelayed(() -> isAppReadyToFinish = false, 2000);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // register connection status listener
        ApplicationManager.setConnectivityListener(this);

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));

        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Constant.kIncomingMessage));

        // clear the notification area when the app is opened
        // NotificationUtils.clearNotifications(getApplicationContext());
    }

    private void handleFirebase() {
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);
                    displayFirebaseRegID();
                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    String body = intent.getStringExtra(kBody);
                    String title = intent.getStringExtra(kTitle);
                    Toaster.toastRangeen(title + " :\n " + body);

                } else if (intent.getAction().equals(Constant.kIncomingMessage)) {
                    ChatProduct chatProduct = (ChatProduct) intent.getSerializableExtra(kMessage);
                    String name = chatProduct.getUserName();
                    String message = chatProduct.getLastMessage().getBody();
                    Toaster.toastRangeen(name + "\n " + message);
                }
            }
        };
    }

    private void displayFirebaseRegID() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);
        Log.e("Firebase ==> ", "Firebase reg id: " + regId);

        if (TextUtils.isEmpty(regId)) {
            Log.e("Firebase ==> ", "Firebase Reg Id is not received yet!");
        }
    }

    public void setCartUpdate(int count) {
        if (count == 0) {
            tvCartCount.setVisibility(View.GONE);
        } else {
            tvCartCount.setVisibility(View.VISIBLE);
            tvCartCount.setText(String.valueOf(count));
        }
    }


    private void updateCart() {
        ModelManager.modelManager().getShoppingCart(
                (Constants.Status iStatus, GenricResponse<ShoppingCart> genericResponse) -> {
                    setCartUpdate(genericResponse.getObject().getCartItemCount());
                    Log.v("get cart number", " " + genericResponse.getObject().getCartItemCount());
                }, (Constants.Status iStatus, String message) -> {
                    Log.e(getClass().getSimpleName(), message);
                });
    }

    private void getLatestBuildInfo() {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(kCurrentVersion, Utils.getApplicationVersionNumber(this));
        parameters.put(kBuildNumber, Utils.getApplicationVersionCode(this));
        parameters.put(kDeviceType, DeviceType.android.getValue());
        ModelManager.modelManager().getLatestBuildInfo(parameters, (Constants.Status iStatus, GenricResponse<CheckLatestBuild> genericResponse) -> {
            try {
                CheckLatestBuild checkLatestBuild = genericResponse.getObject();
                Integer versionCode = Utils.getApplicationVersionCode(this);
                if (versionCode < Integer.parseInt(checkLatestBuild.getBuildNumber())) {
                    showUpgradeDialog(checkLatestBuild);
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toaster.toast(e.getMessage());
            }

            Log.v(TAG, "checkLatestBuild");
        }, (Constants.Status iStatus, String message) -> {
            Log.e(getClass().getSimpleName(), message);
        });
    }


    private void showUpgradeDialog(CheckLatestBuild build) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_upgrade_view);
        Button btnUpgrade = ButterKnife.findById(dialog, R.id.btn_upgrade);
        TextView btnSkip = ButterKnife.findById(dialog, R.id.tv_skip);
        TextView tvSummary = ButterKnife.findById(dialog, R.id.tv_update_summary);
        TextView tvVersion = ButterKnife.findById(dialog, R.id.tv_version);
        tvSummary.setText(build.getVersionDescription());
        String versionNumber = "PeepsIn " + build.getVersionNumber();
        tvVersion.setText(versionNumber);
        if (build.getMandatoryUpdate()) {
            btnSkip.setVisibility(View.INVISIBLE);
            dialog.setCancelable(false);
        } else {
            btnSkip.setVisibility(View.VISIBLE);
            dialog.setCancelable(true);
        }

        btnUpgrade.setOnClickListener(v -> {
            Uri appUri = Uri.parse("market://details?id=com.app.inn.peepsin");
            try {
                Intent myIntent = new Intent(Intent.ACTION_VIEW, appUri);
                startActivity(myIntent);
            } catch (Exception e) {
                e.printStackTrace();
            }
            dialog.dismiss();
        });
        btnSkip.setOnClickListener(v -> {
            dialog.dismiss();
        });
        dialog.show();
    }

    @OnClick(R.id.btn_cart)
    void onCart() {
        startActivity(ShoppingCartActivity.getIntent(HomeActivity.this));
    }

}