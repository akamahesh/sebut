package com.app.inn.peepsin.UI.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.Address;
import com.app.inn.peepsin.Models.CurrentUser;
import com.app.inn.peepsin.Models.ShopModel;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.fragments.Feeds.MapDialogFragment;
import com.app.inn.peepsin.UI.fragments.Shop.NewShopDetailsFragment;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kImageUrl;
import static com.app.inn.peepsin.Constants.Constants.kLatitude;
import static com.app.inn.peepsin.Constants.Constants.kLongitude;
import static com.app.inn.peepsin.Constants.Constants.kOwnerName;

/**
 * Created by dhruv on 25/9/17.
 */

public class TopShopAdapter extends RecyclerView.Adapter<TopShopAdapter.MyViewHolder> {

    private Context mContext;
    private List<ShopModel> shopList;
    private static Switcher switcherListener;

    public TopShopAdapter(Context mContext, List<ShopModel> albumList, Switcher switcher) {
        this.mContext = mContext;
        this.shopList = albumList;
        switcherListener = switcher;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_top_shop_layout, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        ShopModel item = shopList.get(position);
        holder.bindContent(item);
        String shopName = item.getName();
        holder.tvTitle.setText(Utils.getFirstLetterInUpperCase(shopName));
        Address address = item.getAddress();
        String loc = "No Location Available";
        if(address!=null)
            loc = address.getCity()+","+address.getState()+","+address.getCountry();
        holder.tvAddress.setText(loc);

        String bannerUrl = item.getBannerUrl();
        TextDrawable drawable = TextDrawable.builder().buildRoundRect(String.valueOf(shopName.toUpperCase().charAt(0)), ColorGenerator.MATERIAL.getRandomColor(),5);
        if(!bannerUrl.isEmpty())
            Picasso.with(mContext)
                    .load(bannerUrl)
                    .fit()
                    .placeholder(drawable)
                    .error(drawable)
                    .into(holder.ivBanner);
        else
            holder.ivBanner.setImageDrawable(drawable);

        String logoUrl = item.getLogoUrl();
        if(!logoUrl.isEmpty())
            Picasso.with(mContext)
                    .load(logoUrl)
                    .fit()
                    .placeholder(R.drawable.img_shop_logo)
                    .error(R.drawable.img_shop_logo)
                    .into(holder.ivLogo);
        else
            holder.ivLogo.setImageResource(R.drawable.img_shop_logo);
    }

    public void addItems(List<ShopModel> list) {
        shopList.clear();
        shopList.addAll(list);
        notifyDataSetChanged();
    }

    public void addMoreItems(List<ShopModel> list) {
        shopList.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return shopList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ShopModel item;
        @BindView(R.id.iv_banner_image)
        ImageView ivBanner;
        @BindView(R.id.iv_shop_logo)
        ImageView ivLogo;
        @BindView(R.id.tv_title)
        TextView tvTitle;
        @BindView(R.id.tv_address)
        TextView tvAddress;

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        @OnClick(R.id.card_view)
        void OnItemClick() {
            if (switcherListener != null) {
                switcherListener.switchFragment(
                        NewShopDetailsFragment.newInstance(switcherListener,item.getId()), true, true);
            }
        }

        @OnClick(R.id.iv_location)
        void getLocation() {
            try {
                CurrentUser user = ModelManager.modelManager().getCurrentUser();
                String lat = user.getPrimaryAddress().getLatitude();
                String sLat = item.getAddress().getLatitude();
                String sLon = item.getAddress().getLongitude();
                if (!lat.isEmpty() && !sLat.isEmpty()) {
                    MapDialogFragment mapDialogFragment = MapDialogFragment.newInstance();
                    Bundle bdl = new Bundle();
                    bdl.putString(kLatitude, sLat);
                    bdl.putString(kLongitude, sLon);
                    bdl.putString(kOwnerName, item.getName());
                    bdl.putString(kImageUrl, item.getBannerUrl());
                    mapDialogFragment.setArguments(bdl);
                    mapDialogFragment.setCancelable(true);
                    mapDialogFragment.show(((AppCompatActivity) mContext).getSupportFragmentManager(),
                            mapDialogFragment.getClass().getSimpleName());
                } else {
                    Toaster.toast("Sorry Lat Long UnAvailable");
                }
            } catch (Exception e) {
                Toaster.toast("Sorry Lat Long UnAvailable");
                e.printStackTrace();
            }
        }

        public void bindContent(ShopModel shop) {
            this.item = shop;
        }
    }
}
