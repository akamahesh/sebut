package com.app.inn.peepsin.DataBase;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.app.inn.peepsin.Models.FacebookContact;
import com.app.inn.peepsin.Models.GoogleContact;
import com.app.inn.peepsin.Models.PhoneContact;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.app.inn.peepsin.Constants.Constants.kFacebookCommanIdKey;
import static com.app.inn.peepsin.Constants.Constants.kFacebookCommanNameKey;
import static com.app.inn.peepsin.Constants.Constants.kGoogleContactsEmail;
import static com.app.inn.peepsin.Constants.Constants.kGoogleContactsName;
import static com.app.inn.peepsin.Constants.Constants.kGoogleContactsPhoto;

public class ModifiedDataBaseHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "UserInformationDB";
    // Contacts table name
    private static final String TABLE_CONTACTS = "userContacts";
    // Contacts Table Columns names
    private static final String KEY_NAME = "name";
    private static final String KEY_ID = "id";
    private static final String KEY_IMAGE = "image";
    private static final String KEY_ISINVITED = "isinvited";
    private static final String KEY_USERID = "userid";
    private static final String KEY_CONTACT_TYPE = "contacttype";
    private static final String KEY_SOCIAL_ID = "socialid";
    private static final String KEY_CONTACT_ID = "contactid";


    public ModifiedDataBaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_CONTACT_TABLE =
                "CREATE TABLE " + TABLE_CONTACTS
                        + "("
                        + KEY_ID + " INTEGER PRIMARY KEY,"
                        + KEY_USERID + " TEXT,"
                        + KEY_NAME + " TEXT,"
                        + KEY_CONTACT_ID + " TEXT,"
                        + KEY_CONTACT_TYPE + "TEXT,"
                        + KEY_IMAGE + " TEXT,"
                        + KEY_ISINVITED + " TEXT "
                        + ")";

        db.execSQL(CREATE_CONTACT_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACTS);

        // Create tables again
        onCreate(db);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
    }

    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */



    public void addContacts(String userId, String name, String contactId,String contactType, String imageUrl, String isInvited) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_USERID, userId);
        values.put(KEY_NAME, name);
        values.put(KEY_CONTACT_ID, contactId);
        values.put(KEY_CONTACT_TYPE, contactType);
        values.put(KEY_IMAGE, imageUrl);
        values.put(KEY_ISINVITED, isInvited);
        db.insert(TABLE_CONTACTS, null, values);
        db.close(); // Closing database connection
    }




    public void updateContacts(String userId, String contactId, String contactType, String isinvited) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ISINVITED, isinvited);

        if (!contactId.isEmpty()) {
            db.update(TABLE_CONTACTS, values, KEY_USERID+"=" + userId + " AND "+KEY_CONTACT_ID+" = '"+ contactId+"' AND "+KEY_CONTACT_TYPE+" = "+contactType, null);
        }
        db.close(); // Closing database connection
    }



    @SuppressLint("Recycle")
    public boolean isContactExist(String userId, String contactId,String contactType) {
        Cursor cursor = null;
        SQLiteDatabase db = this.getReadableDatabase();
        if (!contactId.isEmpty()) {
            cursor = db.query(TABLE_CONTACTS, new String[]{KEY_USERID, KEY_CONTACT_ID,KEY_CONTACT_TYPE},
                    KEY_USERID + "=? AND " + KEY_CONTACT_ID + "=? AND "+KEY_CONTACT_TYPE+"=? ",
                    new String[]{userId, contactId,contactType},
                    null, null, null, null);
        }
        //row exists
        return cursor != null && cursor.moveToFirst();
    }

    @SuppressLint("Recycle")
    public List<GoogleContact> getContactFromDatabase(String userId, String isinvited,String contactType) {
        Cursor c;
        List<GoogleContact> googleContactList = new ArrayList<>();
        try {
            SQLiteDatabase newDB = getReadableDatabase();
            c = newDB.rawQuery("Select id,userid,name,contactid,contacttype,image,isinvited From " + ModifiedDataBaseHandler.TABLE_CONTACTS + " where userId = " + userId + " AND  isinvited = " + isinvited+ " AND  contacttype = " + contactType, null);
            if (c != null) {
                System.out.println("Values::::" + c.getCount());
                if (c.moveToFirst()) {
                    do {
                        String name = c.getString(c.getColumnIndex(KEY_NAME));
                        String contactId = c.getString(c.getColumnIndex(KEY_CONTACT_ID));
                        String imageURL = c.getString(c.getColumnIndex(KEY_IMAGE));

                        HashMap<String, Object> googleContactMap = new HashMap<>();
                        googleContactMap.put(kGoogleContactsName, name);
                        googleContactMap.put(kGoogleContactsEmail, contactId);
                        googleContactMap.put(kGoogleContactsPhoto, imageURL);

                        googleContactList.add(new GoogleContact(googleContactMap));
                    }
                    while (c.moveToNext());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return googleContactList;
    }



    @SuppressLint("Recycle")
    public List<FacebookContact> getFacebookContactFromDatabase(String userId, String isinvited) {
        Cursor c;
        List<FacebookContact> facebookContactList = new ArrayList<>();
        try {
            SQLiteDatabase newDB = getReadableDatabase();
            c = newDB.rawQuery("Select id,userid,name,socialid,isinvited From " + ModifiedDataBaseHandler.TABLE_CONTACTS + " where userId = " + userId + " AND  isinvited = " + isinvited, null);
            if (c != null) {
                System.out.println("Values::::" + c.getCount());
                if (c.moveToFirst()) {
                    do {
                        String name = c.getString(c.getColumnIndex(KEY_NAME));
                        String socialID = c.getString(c.getColumnIndex(KEY_SOCIAL_ID));

                        HashMap<String, Object> facebookContactMap = new HashMap<>();
                        facebookContactMap.put(kFacebookCommanNameKey, name);
                        facebookContactMap.put(kFacebookCommanIdKey, socialID);

                        facebookContactList.add(new FacebookContact(facebookContactMap));
                    }
                    while (c.moveToNext());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return facebookContactList;
    }
    /*                            + KEY_USERID + " TEXT,"
                + KEY_NAME + " TEXT,"
                + KEY_IMAGE + " TEXT,"
                + KEY_CONTACT_ID + " TEXT,"
                + KEY_ISINVITED + " TEXT "*/
    @SuppressLint("Recycle")
    public List<PhoneContact> getPhoneContactFromDatabase(String userId, String isinvited) {
        Cursor c;
        List<PhoneContact> phoneContactList = new ArrayList<>();
        try {
            SQLiteDatabase newDB = getReadableDatabase();
            c = newDB.rawQuery("Select id,userid,name,contactid,image,isinvited From " + ModifiedDataBaseHandler.TABLE_CONTACTS + " where userId = " + userId + " AND  isinvited = " + isinvited, null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        String name = c.getString(c.getColumnIndex(KEY_NAME));
                        String contactId = c.getString(c.getColumnIndex(KEY_CONTACT_ID));
                        String imageURL = c.getString(c.getColumnIndex(KEY_IMAGE));
                        phoneContactList.add(new PhoneContact(name,contactId,imageURL));
                    }
                    while (c.moveToNext());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return phoneContactList;
    }

}
