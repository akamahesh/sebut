package com.app.inn.peepsin.UI.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.inn.peepsin.Models.Address;
import com.app.inn.peepsin.Models.FinalBillSummary;
import com.app.inn.peepsin.Models.FinalProductsRecords;
import com.app.inn.peepsin.Models.ProductList;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.helpers.RoundedCornersTransformation;
import com.squareup.picasso.Picasso;

import java.util.concurrent.CopyOnWriteArrayList;

public class TransactionDetailAdapter extends BaseExpandableListAdapter {

    private Context context;
    private CopyOnWriteArrayList<FinalProductsRecords> shopList;
    private FinalBillSummary finalBillSummary;

    public TransactionDetailAdapter(Context context, FinalBillSummary finalBillSummary) {
        this.context = context;
        this.finalBillSummary = finalBillSummary;
        this.shopList = finalBillSummary.getShopRecords();
    }

    // Parent View Group
    @Override
    public Object getGroup(int listPosition) {
        return this.shopList.get(listPosition);
    }

    @Override
    public int getGroupCount() {
        return this.shopList.size();
    }

    @Override
    public long getGroupId(int listPosition) {
        return listPosition;
    }

    @Override
    public View getGroupView(int listPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        FinalProductsRecords shop = (FinalProductsRecords) getGroup(listPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.transaction_detail_parent_layout, null);
        }

        ImageView ivShopBanner = (ImageView) convertView.findViewById(R.id.iv_shop_banner);
        TextView tvShopName = (TextView) convertView.findViewById(R.id.tv_shop_name);
        TextView tvTotalProduct = (TextView) convertView.findViewById(R.id.tv_total_product);
        TextView tvShippedAddress = (TextView) convertView.findViewById(R.id.tv_shipped_address);

        tvShopName.setText(shop.getshopName());
        tvTotalProduct.setText(String.valueOf(shop.getProductRecords().size()));
        Picasso.with(context)
                .load(shop.getshopBannerImageURL())
                .transform(new RoundedCornersTransformation(5, 0))
                .placeholder(R.drawable.img_product_placeholder)
                .into(ivShopBanner);
        try {
            Address address = finalBillSummary.getDeliveryAddress();
            String location = address.getStreetAddress()+","+address.getCity() + "\n" + address.getState() + "\n" + address.getZipCode();
            tvShippedAddress.setText(location);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    // Child View Group
    @Override
    public Object getChild(int listPosition, int expandedListPosition) {
        return this.shopList.get(listPosition).getProductRecords().get(expandedListPosition);
    }

    @Override
    public long getChildId(int listPosition, int expandedListPosition) {
        return expandedListPosition;
    }

    @Override
    public int getChildrenCount(int listPosition) {
        return this.shopList.get(listPosition).getProductRecords().size();
    }

    @Override
    public View getChildView(int listPosition, final int expandedListPosition, boolean isLastChild, View childView, ViewGroup parent) {

        final ProductList product = (ProductList) getChild(listPosition, expandedListPosition);
        if (childView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            childView = layoutInflater.inflate(R.layout.transaction_detail_child_layout, null);
        }

        ImageView ivProduct = (ImageView) childView.findViewById(R.id.iv_product);
        TextView tvProductName = (TextView) childView.findViewById(R.id.tv_product_name);
        TextView tvQuantity = (TextView) childView.findViewById(R.id.tv_quantity);
        TextView tvPrice = (TextView) childView.findViewById(R.id.tv_product_price);

        tvProductName.setText(product.getProductName());
        tvQuantity.setText(String.valueOf(product.getProductQuantity()));
        String price = context.getString(R.string.Rs) + product.getPrice();
        tvPrice.setText(price);
        Picasso.with(context)
                .load(product.getCoverImageThumbnailURL())
                .placeholder(R.drawable.img_product_placeholder)
                .into(ivProduct);

        return childView;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return false;
    }

}
