package com.app.inn.peepsin.Models;

import static com.app.inn.peepsin.Constants.Constants.kEmptyString;

/**
 * Created by mahesh on 30/5/17.
 */

public class PhoneContact {
    private String name;
    private String contactId;
    private String imageURL;
    private boolean isSelected;

    public PhoneContact(String name, String contactId, String imageURL) {
        this.name = name;
        this.contactId = contactId;
        this.imageURL = imageURL;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return contactId;
    }

    public void setPhone(String phone) {
        this.contactId = phone;
    }

    public String getImageURL() {
        return (imageURL==null)?kEmptyString:imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }


    @Override
    public String toString() {
        return "Phone Contact : {" +"\n"+
                " contactId : " +contactId+"\n"+
                " name : " +name+"\n"+
                " imageURL :" +imageURL+"\n"+
                "}";
    }
}
