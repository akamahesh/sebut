package com.app.inn.peepsin.UI.fragments.Profile;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.APIManager.APIManager;
import com.app.inn.peepsin.Managers.APIManager.APIRequestHelper;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.Address;
import com.app.inn.peepsin.Models.GeoAddress;
import com.app.inn.peepsin.Models.Shop;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.fragments.Shop.ProductIntermediateFragment;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import com.chaos.view.PinView;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.app.inn.peepsin.Constants.Constants.kAddress;
import static com.app.inn.peepsin.Constants.Constants.kAddressId;
import static com.app.inn.peepsin.Constants.Constants.kAuthToken;
import static com.app.inn.peepsin.Constants.Constants.kBankDetails;
import static com.app.inn.peepsin.Constants.Constants.kContactId;
import static com.app.inn.peepsin.Constants.Constants.kCurrentUser;
import static com.app.inn.peepsin.Constants.Constants.kIsVerified;
import static com.app.inn.peepsin.Constants.Constants.kLatitude;
import static com.app.inn.peepsin.Constants.Constants.kLongitude;
import static com.app.inn.peepsin.Constants.Constants.kOTP;
import static com.app.inn.peepsin.Constants.Constants.kRecords;
import static com.app.inn.peepsin.Constants.Constants.kResults;
import static com.app.inn.peepsin.Constants.Constants.kShopType;
import static com.app.inn.peepsin.Constants.Constants.kType;
import static com.app.inn.peepsin.Constants.Constants.kZipcode;

/**
 * Created by akamahesh on 2/5/17.
 */

public class ProfileOTPFragment extends Fragment {

    private final static String TAG = ProfileFragment.class.getSimpleName();
    static Switcher profileSwitcherListener;

    @BindView(R.id.tv_otp)
    TextView tvOTP;
    @BindView(R.id.btn_verify)
    Button btnVerify;
    @BindView(R.id.otp_view)
    PinView otpView;
    @BindView(R.id.tv_title)
    TextView tvTitle;

    int TYPE = 1;
    int shopCreate = 1;
    private static final int PHONE = 1;
    private static final int EMAIL = 2;
    private static final int PASSWORD = 3;
    private String ContactId;
    private String PIN;

    double latitude = 0.0;
    double longitude = 0.0;


    private ProgressDialog progressDialog;
    private HashMap<String, Object> userMap;
    String addresss;
    String apikey;
    private Address address;

    public static Fragment newInstance(int type, int shopCreate, Switcher switcherListener, String pin, String contactId, String addresss , Address address, HashMap<String, Object> hashMap) { //1 for phone 2 for email
        ProfileOTPFragment fragment = new ProfileOTPFragment();
        profileSwitcherListener = switcherListener;
        Bundle bundle = new Bundle();
        bundle.putInt(kType, type);
        bundle.putInt(kShopType, shopCreate);
        bundle.putString(kOTP, pin);
        bundle.putString(kContactId, contactId);
        bundle.putString(kAddress,addresss);
        bundle.putSerializable(kCurrentUser, hashMap);
        bundle.putSerializable(kRecords, address);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = Utils.generateProgressDialog(getContext(), false);
        Bundle bundle = getArguments();
        apikey = getString(R.string.google_api_key);
        if (bundle != null) {
            TYPE = bundle.getInt(kType);
            shopCreate = bundle.getInt(kShopType);
            PIN = bundle.getString(kOTP);
            ContactId = bundle.getString(kContactId);
            addresss=bundle.getString(kAddress);
            address = (Address) bundle.getSerializable(kRecords);
            userMap = (HashMap<String, Object>) bundle.getSerializable(kCurrentUser);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile_otp, container, false);
        ButterKnife.bind(this, view);
        if (TYPE == PHONE) {
           // tvTitle.setText(getString(R.string.title_phone_verification));
            btnVerify.setText("Verify Phone");
        } else if (TYPE == EMAIL) {
           // tvTitle.setText(getString(R.string.title_email_verificaition));
            btnVerify.setText("Verify Email");
        } else if (TYPE == PASSWORD) {
           // tvTitle.setText(getString(R.string.title_forget_password));
            btnVerify.setText("Reset Password");
        }
        Toaster.toastOTP(PIN);
        tvTitle.setText("OTP Verification");

        return view;
    }

    @OnClick(R.id.btn_back)
    void onBack() {
        Utils.hideKeyboard(getContext());
        getFragmentManager().popBackStack();
    }

    @OnClick(R.id.btn_verify)
    void verify() {
        String otpString = otpView.getText().toString();
        if (otpString.equals(PIN)) {
            if (TYPE != PASSWORD)
                verifyOTP(ContactId, otpString);
        } else {
            otpView.setText("");
            Utils.showKeyboard(getContext(), otpView);
            Toaster.toast("Worng OTP");
        }
    }

    private void verifyOTP(String contactId, String OTP) {
        String authToken = ModelManager.modelManager().getCurrentUser().getAuthToken();
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(kAuthToken, authToken);
        parameters.put(kContactId, contactId);
        parameters.put(kOTP, OTP);
        showProgress(true);
        ModelManager.modelManager().otpVerification(parameters, (Constants.Status iStatus, GenricResponse<HashMap<String, Object>> genericResponse) -> {
            HashMap<String, Object> hashMap = genericResponse.getObject();
            Integer isVerified = (Integer) hashMap.get(kIsVerified);
            if (isVerified.equals(1)) {
                if (userMap.isEmpty()){
                    showProgress(false);
                    Toaster.toastRangeen("Your credential has been verified.");
                    onBack();
                }
                else {
                    if (userMap.containsKey(kBankDetails)){
                        createShop(userMap);
                    } else if(userMap.containsKey(kZipcode))
                        requestCordinates(addresss,userMap,2);
                        else{
                        editUserProfile(userMap);
                    }
                    }
            } else {
                showProgress(false);
                Utils.showAlertDialog(getContext(), "Process Error", "Sorry , Your credential were not verified!");
                onBack();
            }
            otpView.setText("");
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Log.e(TAG, message);
            Toaster.toast(message);
        });
    }

    private void editUserProfile(HashMap<String, Object> userMap) {
        ModelManager.modelManager().editMyProfile(userMap, (Constants.Status iStatus) -> {
            showProgress(false);
            if(profileSwitcherListener!=null)
                profileSwitcherListener.switchFragment(ProfileFragment.newInstance(profileSwitcherListener),false,true);
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Log.e(TAG, message);
            Toaster.toast(message);
        });
    }


    public void requestCordinates(String addresss, HashMap<String, Object> userMap, int type) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(APIManager.kGoogleMapsBaseURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIRequestHelper service = retrofit.create(APIRequestHelper.class);
        showProgress(true);
        service.getCordinates(addresss, apikey).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                try {
                    showProgress(false);
                    JsonObject jsonObject = response.body();
                    JSONObject jsonResponse = new JSONObject(jsonObject.toString());
                    JSONArray jsonResult = jsonResponse.getJSONArray(kResults);
                    GeoAddress geoAddress = new GeoAddress(jsonResult.getJSONObject(0));
                    latitude = geoAddress.getLocation().getLat();
                    longitude = geoAddress.getLocation().getLng();
                    Log.i(TAG, "latitude : " + latitude);
                    Log.i(TAG, "longitude : " + longitude);
                    userMap.put(kLatitude,latitude);
                    userMap.put(kLongitude,longitude);
                    saveAddress(userMap,type);
                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlertDialog(getContext(),"Error Processing Address","Please Provide a valid address that can be located on map.");
                }
            }



            //type 1 = for new and 2 for edit
            private void saveAddress(HashMap<String, Object> addressMap,int type) {
                if(type==1){
                    ModelManager.modelManager().addNewAddress(addressMap, (Constants.Status iStatus) -> {
                        showProgress(false);
                        onBack();
                        onBack();
                    }, (Constants.Status iStatus, String message) -> {
                        showProgress(false);
                        Toaster.toast(message);
                    });
                }else if(type==2){
                   addressMap.put(kAddressId, address.getAddressId());
                    ModelManager.modelManager().editAddress(addressMap, (Constants.Status iStatus) -> {
                        showProgress(false);
                        onBack();
                        onBack();
                    }, (Constants.Status iStatus, String message) -> {
                        showProgress(false);
                        Toaster.toast(message);
                    });
                }
            }


            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.i(TAG, t.toString());
                showProgress(false);
            }
        });
    }


    private void createShop(HashMap<String, Object> map) {
        showProgress(true);
        ModelManager.modelManager().createNewShop(map, (Constants.Status iStatus, GenricResponse<Shop> genricResponse) -> {
            showProgress(false);
            if (shopCreate == 2)
                profileSwitcherListener.switchFragment(ProfileFragment.newInstance(profileSwitcherListener), false, true);
            else
                profileSwitcherListener.switchFragment(ProductIntermediateFragment.newInstance(profileSwitcherListener), true, true);
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Toaster.toast(message);
        });
    }

    @OnClick(R.id.tv_resend)
    void resend() {
        resetOTP(ContactId);
    }

    private void resetOTP(String email) {
        showProgress(true);
        ModelManager.modelManager().contactVerification(email, (Constants.Status iStatus, GenricResponse<HashMap<String, Object>> genericResponse) -> {
            HashMap<String, Object> map = genericResponse.getObject();
            PIN = (String) map.get(kOTP);
            showProgress(false);
        }, (Constants.Status iStatus, String message) -> {
            Toaster.toast(message);
            showProgress(false);
        });
    }

    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) progressDialog.dismiss();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        profileSwitcherListener = null;
    }

}
