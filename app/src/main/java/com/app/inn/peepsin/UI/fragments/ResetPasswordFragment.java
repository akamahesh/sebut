package com.app.inn.peepsin.UI.fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.helpers.Validations;
import com.app.inn.peepsin.UI.interfaces.Switcher;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kContactId;
import static com.app.inn.peepsin.Constants.Constants.kOTP;
import static com.app.inn.peepsin.Constants.Constants.kPassword;

/**
 * Created by akamahesh on 2/5/17.
 */

public class ResetPasswordFragment extends Fragment{

    @BindView(R.id.edt_new_password) EditText edtNewPassword;
    @BindView(R.id.edt_confirm_password) EditText edtConfirmPassword;
    @BindView(R.id.tv_title) TextView tvTitle;
    private static Switcher profileSwitcherListener;

    private static String contactId;
    private static String password;
    private static String OTP;
    private ProgressDialog progressDialog;

    public static Fragment newInstance(String contactId, String password, String OTP, Switcher switcher){
        Fragment fragment = new ResetPasswordFragment();
        profileSwitcherListener = switcher;
        Bundle bundle = new Bundle();
        bundle.putString(kContactId,contactId);
        bundle.putString(kPassword,password);
        bundle.putString(kOTP,OTP);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if(bundle!=null){
            contactId = bundle.getString(kContactId);
            password = bundle.getString(kPassword);
            OTP = bundle.getString(kOTP);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_reset_password, container, false);
        ButterKnife.bind(this,view);
        progressDialog = Utils.generateProgressDialog(getContext(),false);
        tvTitle.setText(getString(R.string.reset_password_text));
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @OnClick(R.id.btn_reset)
    void onResetPassword(){
        String newPassword = edtNewPassword.getText().toString().trim();
        String confirmPassword = edtConfirmPassword.getText().toString().trim();

        if(!Validations.isValidPassword(newPassword)){
            edtNewPassword.setError("Please Enter a valid Password!");
            return;
        }

        if(newPassword.equals(confirmPassword))
            resetPassword(newPassword,contactId,OTP);
        else
            edtConfirmPassword.setError("Password Don't match");

    }

    private void resetPassword(String password,String contactId,String otp) {
        HashMap<String,Object> dataMap = new HashMap<>();
        dataMap.put(kContactId,contactId);
        dataMap.put(kPassword,password);
        dataMap.put(kOTP,otp);
        showProgress(true);
        ModelManager.modelManager().resetPassword(dataMap,(Constants.Status iStatus) -> {
            showProgress(false);
            Utils.hideKeyboard(getContext());
            Utils.showAlertDialog(getContext(),"Success!","Password Successfully changed.");
            getFragmentManager().popBackStack();
            getFragmentManager().popBackStack();
        }, (Constants.Status iStatus, String message) -> {
            Toaster.toast(message);
            showProgress(false);
        });


    }

    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) progressDialog.cancel();
        }
    }


    @OnClick(R.id.btn_back)
    void onBack(){
        getFragmentManager().popBackStack();
    }

}
