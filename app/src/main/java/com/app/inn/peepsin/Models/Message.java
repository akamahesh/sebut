package com.app.inn.peepsin.Models;

import java.io.Serializable;
/**
 * Created by akaMahesh on 2/5/17.
 * copyright to : Innverse Technologies
 */

public class Message implements Serializable {
    private String id;
    private String body;
    private String direction;
    private String status;
    private String time;

    public Message() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
