package com.app.inn.peepsin.UI.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.inn.peepsin.Models.ProductList;
import com.app.inn.peepsin.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.app.inn.peepsin.Managers.BaseManager.ApplicationManager.getContext;


/**
 * Created by Harsh on 9/18/2017.
 */

public class OrderHistoryDetailShowProductAdapter extends RecyclerView.Adapter<OrderHistoryDetailShowProductAdapter.ViewHolder> {

    private List<ProductList> orderList;
    private Context mContext;

    public OrderHistoryDetailShowProductAdapter(Context context, List<ProductList> ProductListList) {
        this.mContext = context;
        this.orderList = ProductListList;
    }


    @Override
    public OrderHistoryDetailShowProductAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_new_child_order_history, parent, false);
        return new OrderHistoryDetailShowProductAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(OrderHistoryDetailShowProductAdapter.ViewHolder holder, int position) {
        ProductList product = orderList.get(position);
        holder.bindContent(product);
        String producturl = product.getCoverImageThumbnailURL();
        String price = mContext.getString(R.string.Rs) + product.getPrice();
        String productname = product.getProductName();
        String productQuantity = "Quantity:" + " " + product.getProductQuantity();

        Picasso.with(getContext())
                .load(producturl)
                .placeholder(R.drawable.img_product_placeholder)
                .into(holder.ivProductImage);
        holder.tvProductName.setText(productname);
        holder.tvProductQuantity.setText(productQuantity);
        holder.tvProductRupees.setText(price);

       /* String shopName =shopRecord.getshopName();
            String shopaddress="";
            if (shopRecord.getShopAddress() != null) {
                 shopaddress = shopRecord.getShopAddress().getStreetAddress() + ", "
                        + shopRecord.getShopAddress().getCity() + ", "
                        + shopRecord.getShopAddress().getState() + ", "
                        + shopRecord.getShopAddress().getCountry();

            }

           if (position == 0)
                holder.shopNameView.setVisibility(View.GONE);
            else {
                if (!shopName.equals(orderList.get(position - 1).getShopName()))
                    holder.shopNameView.setVisibility(View.VISIBLE);
                else
                    holder.shopNameView.setVisibility(View.GONE);
            }
*/

    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ProductList product;
        @BindView(R.id.iv_product_image)
        ImageView ivProductImage;
        @BindView(R.id.tv_product_name)
        TextView tvProductName;
        @BindView(R.id.tv_product_rupees)
        TextView tvProductRupees;
        @BindView(R.id.tv_product_quantity)
        TextView tvProductQuantity;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindContent(ProductList item) {
            this.product = item;
        }
    }
}

