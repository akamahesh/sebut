package com.app.inn.peepsin.UI.fragments.Profile;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.DataBase.DataBaseHandler;
import com.app.inn.peepsin.Libraries.ImageEditor.ImageController;
import com.app.inn.peepsin.Libraries.ImageEditor.ImageEditor;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.Address;
import com.app.inn.peepsin.Models.CelebrationEvent;
import com.app.inn.peepsin.Models.EventImages;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.Services.EventNotificationService;
import com.app.inn.peepsin.UI.helpers.AkaImageUtil;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.zelory.compressor.Compressor;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.app.Activity.RESULT_OK;
import static com.app.inn.peepsin.Constants.Constants.ADDRESS_RESULT;
import static com.app.inn.peepsin.Constants.Constants.PHOTO_RESULT;
import static com.app.inn.peepsin.Constants.Constants.kCelebrationMode;
import static com.app.inn.peepsin.Constants.Constants.kData;
import static com.app.inn.peepsin.Constants.Constants.kEvent;
import static com.app.inn.peepsin.Constants.Constants.kEventMap;
import static com.app.inn.peepsin.Constants.Constants.kEventTitle;
import static com.app.inn.peepsin.Constants.Constants.kEventType;
import static com.app.inn.peepsin.Constants.Constants.kImage;
import static com.app.inn.peepsin.Constants.Constants.kMessage;
import static com.app.inn.peepsin.Constants.Constants.kNotifyMeDuration;
import static com.app.inn.peepsin.Constants.Constants.kRepetitionInterval;
import static com.app.inn.peepsin.Libraries.ImageEditor.ImageEditor.PIC_CAMERA;
import static com.app.inn.peepsin.Libraries.ImageEditor.ImageEditor.PIC_CROP;
import static com.app.inn.peepsin.Libraries.ImageEditor.ImageEditor.PIC_GALLARY;

/**
 * Created by dhruv on 27/9/17.
 */

public class AddEventDetailsFragment extends Fragment{
    private final String TAG = getClass().getSimpleName() + ">>>";

    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.ivEventImage)
    ImageView ivEventImage;
    @BindView(R.id.edt_message)
    EditText edtMessage;
    @BindView(R.id.event_notify_before)
    RadioGroup notifyGroup;
    @BindView(R.id.event_repetition)
    RadioGroup repetitionGroup;

    @BindView(R.id.event_text)
    CheckBox checkBoxText;
    @BindView(R.id.event_email)
    CheckBox checkBoxEmail;
    @BindView(R.id.event_notify)
    CheckBox checkBoxNotify;
    @BindView(R.id.event_chat)
    CheckBox checkBoxChat;

    private ProgressDialog progressDialog;
    private HashMap<String,Object> eventMap;
    private CelebrationEvent event;

    private String mImagePath = "";
    private final int REQUEST_PERMISSION_CAMERA_STORAGE = 101;

    private int notifyDuration;
    private int repetitionInterval;
    private List<Integer> celebrationMode;
    private List<CheckBox> notifyCheckBoxes;
    private DataBaseHandler dbHandler;

    public static Fragment newInstance(HashMap<String,Object> map,CelebrationEvent event) {
        AddEventDetailsFragment fragment = new AddEventDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(kEventMap,map);
        bundle.putSerializable(kEvent,event);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = Utils.generateProgressDialog(getContext(), false);
        celebrationMode = new ArrayList<>();
        notifyCheckBoxes = new ArrayList<>();
        dbHandler = new DataBaseHandler(getContext());
        Bundle bdl = getArguments();
        if(bdl!=null){
            eventMap = (HashMap<String, Object>) bdl.getSerializable(kEventMap);
            event = (CelebrationEvent) bdl.getSerializable(kEvent);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_event, container, false);
        ButterKnife.bind(this, view);
        tvTitle.setText(eventMap.get(kEventTitle).toString());
        notifyCheckBoxes.add(checkBoxText);
        notifyCheckBoxes.add(checkBoxEmail);
        notifyCheckBoxes.add(checkBoxNotify);
        notifyCheckBoxes.add(checkBoxChat);

        //Notify Type RadioGroup
        notifyGroup.setOnCheckedChangeListener((group, checkedId) -> {
            RadioButton rb = (RadioButton) group.findViewById(checkedId);
            if (rb.getText().toString().equals(getString(R.string.event_15min)))
                notifyDuration = 1;
            else if(rb.getText().toString().equals(getString(R.string.event_1hr)))
                notifyDuration = 2;
            else
                notifyDuration = 3;
        });

        //Repetition Type RadioGroup
        repetitionGroup.setOnCheckedChangeListener((group, checkedId) -> {
            RadioButton rb = (RadioButton) group.findViewById(checkedId);
            if (rb.getText().toString().equals(getString(R.string.event_oneTime)))
                repetitionInterval = 1;
            else if(rb.getText().toString().equals(getString(R.string.event_everyDay)))
                repetitionInterval = 2;
            else if(rb.getText().toString().equals(getString(R.string.event_everyMonth)))
                repetitionInterval = 3;
            else
                repetitionInterval = 4;
        });

        checkBoxText.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if(isChecked)
                celebrationMode.add(1);
            else
                celebrationMode.remove(Integer.valueOf(1));
        });

        checkBoxEmail.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if(isChecked)
                celebrationMode.add(2);
            else
                celebrationMode.remove(Integer.valueOf(2));
        });

        checkBoxNotify.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if(isChecked)
                celebrationMode.add(3);
            else
                celebrationMode.remove(Integer.valueOf(3));
        });

        checkBoxChat.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if(isChecked)
                celebrationMode.add(4);
            else
                celebrationMode.remove(Integer.valueOf(4));
        });

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(event!=null)
            setEventInfo(event);
        else
            getCelebrationImages(eventMap);
    }

    private void setEventInfo(CelebrationEvent event){
        edtMessage.setText(event.getMessage());
        JSONArray celebration = event.getCelebrationMode();

        String imageUrl = event.getImageUrl();
        if(!imageUrl.isEmpty()){
            Picasso.with(getContext()).load(imageUrl).fit()
                    .placeholder(R.drawable.placeholder_300).into(ivEventImage);
            getImagePath(imageUrl);
        }

        for(int i=0;i<notifyGroup.getChildCount();i++){
            if(i==event.getNotifyMeDuration()-1)
                ((RadioButton)notifyGroup.getChildAt(i)).setChecked(true);
        }

        for(int i=0;i<repetitionGroup.getChildCount();i++){
            if(i==event.getRepetitionInterval()-1)
                ((RadioButton)repetitionGroup.getChildAt(i)).setChecked(true);
        }

        for(int i=0;i<celebration.length();i++){
            for(int j=0;j<notifyCheckBoxes.size();j++){
                try {
                    if(celebration.getString(i).equals(notifyCheckBoxes.get(j).getTag())) {
                        notifyCheckBoxes.get(j).setChecked(true);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @OnClick(R.id.btn_back)
    void onBack() {
        getFragmentManager().popBackStack();
    }

    @OnClick(R.id.ivEventImage)
    public void pickPhotoCreate() {
        Utils.hideKeyboard(getContext());
        checkPermission();
    }

    @OnClick(R.id.continue_btn)
    void onNext(){
        if (!validateData())
            return;
        HashMap<String, Object> hashMap = getParameters();
        if(event!=null)
            editCelebrationEvent(hashMap);
        else
            addCelebrationEvent(hashMap);
    }

    private boolean validateData() {
        boolean isOk = true;

        if (edtMessage.getText().toString().isEmpty()) {
            edtMessage.setError("Event Name is empty");
            edtMessage.requestFocus();
            isOk = false;
        }
        else if(notifyGroup.getCheckedRadioButtonId()==-1){
            Toaster.kalaToast("Please select notify time");
            isOk = false;
        }
        else if(repetitionGroup.getCheckedRadioButtonId()==-1){
            Toaster.kalaToast("Please select repetition interval");
            isOk = false;
        }
        else if(celebrationMode.size()==0){
            Toaster.kalaToast("Please select any notify type");
            isOk = false;
        }
        else if(mImagePath.isEmpty()){
            Toaster.kalaToast("Please select event banner image");
            isOk = false;
        }

        return isOk;
    }

    public HashMap<String, Object> getParameters() {
        eventMap.put(kImage,new File(mImagePath));
        eventMap.put(kMessage, Utils.getProperText(edtMessage));
        eventMap.put(kNotifyMeDuration, notifyDuration);
        eventMap.put(kRepetitionInterval,repetitionInterval);
        eventMap.put(kCelebrationMode,celebrationMode);
        return eventMap;
    }

    private void addCelebrationEvent(HashMap<String, Object> eventMap){
        showProgress(true);
        ModelManager.modelManager().addCelebrationEvent(dbHandler,eventMap, (Constants.Status iStatus, GenricResponse<CelebrationEvent> genricResponse) -> {
            showProgress(false);
            createNotificationService();
            Utils.isEventBack=1;
            if(!progressDialog.isShowing())
                onBack();
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Log.e(TAG,message);
            Toaster.toast(message);
        });
    }

    private void editCelebrationEvent(HashMap<String, Object> eventMap){
        showProgress(true);
        ModelManager.modelManager().editCelebrationEvent(dbHandler,eventMap, (Constants.Status iStatus, GenricResponse<CelebrationEvent> genricResponse) -> {
            showProgress(false);
            createNotificationService();
            Utils.isEventBack=1;
            if(!progressDialog.isShowing())
                onBack();
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Log.e(TAG,message);
            Toaster.toast(message);
        });
    }

    public void createNotificationService(){
        Intent intent = new Intent(getContext(), EventNotificationService.class);
        intent.putExtra("RQS", "active");
        getContext().startService(intent);
    }

    private void getImagePath(String imageUrl){
        Utils.getDownloadedPath(imageUrl,(Constants.Status iStatus, GenricResponse<String> genricResponse) -> {
            mImagePath = genricResponse.getObject();
        }, (Constants.Status iStatus, String message) -> Log.e(TAG,message));
    }

    private void getCelebrationImages(HashMap<String, Object> eventMap){
        showProgress(true);
        ModelManager.modelManager().getCelebrationEventImages( (Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<EventImages>> genricResponse) -> {
            showProgress(false);
            CopyOnWriteArrayList<EventImages> images = genricResponse.getObject();
            for(int i =0;i<images.size();i++){
                if(eventMap.get(kEventType).equals(images.get(i).getEventType())){
                    Picasso.with(getContext()).load(images.get(i).getImageUrl()).fit()
                            .placeholder(R.drawable.placeholder_300).into(ivEventImage);
                    getImagePath(images.get(i).getImageUrl());
                }
            }
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Log.e(TAG,message);
            Toaster.toast(message);
        });
    }

    private void checkPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(), CAMERA) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(getActivity(), READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            openBottomSheetBanner();
        } else if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), CAMERA)
                && ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), READ_EXTERNAL_STORAGE)) {
            // We've been denied once before. Explain why we need the permission, then ask again.
            Utils.showDialog(getContext(),
                    "Camera & Gallary permissions are required to upload profile images!", "Ask Permission",
                    "Discard", (dialog, which) -> {
                        if (which == -1) {
                            requestPermissions(new String[]{CAMERA, READ_EXTERNAL_STORAGE},
                                    REQUEST_PERMISSION_CAMERA_STORAGE);
                        } else {
                            dialog.dismiss();
                        }
                    });
        } else {
            // We've never asked. Just do it.
            requestPermissions(new String[]{CAMERA, READ_EXTERNAL_STORAGE},
                    REQUEST_PERMISSION_CAMERA_STORAGE);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_PERMISSION_CAMERA_STORAGE
                && grantResults[0] == PackageManager.PERMISSION_GRANTED
                && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            openBottomSheetBanner();
        } else {
            // We were not granted permission this time, so don't try to show the contact picker
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case PIC_GALLARY:
                if (resultCode == RESULT_OK && data != null) {
                    Uri selectedImage = data.getData();
                    cropImageBottomSheet(selectedImage);
                }
                break;
            case PIC_CAMERA:
                if (resultCode == RESULT_OK && data != null) {
                    Bitmap bitmap = (Bitmap) data.getExtras().get(kData);
                    Uri uri = ImageController.getImageUri(getContext(),bitmap);
                    cropImageBottomSheet(uri);
                }
                break;
            case PIC_CROP:
                if (resultCode == RESULT_OK && data != null) {
                    Uri uri = data.getData();
                    if (uri != null) {
                        try {
                            File file = new File(uri.getPath());
                            mImagePath = file.getAbsolutePath();
                            Picasso.with(getContext())
                                    .load(file)
                                    .into(ivEventImage);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
                break;

        }
    }

    public void cropImageBottomSheet(Uri uri) {
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_crop_image, null);
        final Dialog mBottomSheetDialog = new Dialog(getContext(), R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.show();

        view.findViewById(R.id.original_view).setOnClickListener(v -> {
            if (uri != null) {
                try {
                    File file = new File(Utils.getRealPathFromURI(getContext(),uri));
                    mImagePath = file.getAbsolutePath();
                    Picasso.with(getContext())
                            .load(file)
                            .into(ivEventImage);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
            mBottomSheetDialog.dismiss();
        });
        view.findViewById( R.id.crop_view).setOnClickListener(v -> {
            File outputFile = ImageEditor.getInstance().cropImage(this, uri, PIC_CROP);
            Log.e(TAG, outputFile.getAbsolutePath());
            mBottomSheetDialog.dismiss();
        });
    }

    public void openBottomSheetBanner() {
        View view = getActivity().getLayoutInflater().inflate(R.layout.custom_bottonsheet_layout, null);
        final Dialog mBottomSheetDialog = new Dialog(getContext(), R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.show();

        view.findViewById(R.id.camera_view).setOnClickListener(v -> {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, PIC_CAMERA);
            mBottomSheetDialog.dismiss();
        });

        view.findViewById(R.id.gallery_view).setOnClickListener(v -> {
            Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(gallery, PIC_GALLARY);
            mBottomSheetDialog.dismiss();
        });

        view.findViewById(R.id.cancel_view).setOnClickListener(v -> mBottomSheetDialog.dismiss());
    }


    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) progressDialog.dismiss();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Utils.isEventBack=0;
    }
}
