package com.app.inn.peepsin.Models;

import android.util.Log;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by dhruv on 17/8/17.
 */

public class ProductType extends BaseModel implements Serializable {

    private Integer id;
    private String name;
    private String url;
    private boolean isSelected = false;

    public ProductType(JSONObject jsonResponse) {
        this.id   = getValue(jsonResponse, kProductTypeId, Integer.class);
        try {
            this.name = getValue(jsonResponse, kProductTypeName, String.class);
        }catch (Exception e){
            Log.e(getClass().getSimpleName(), e.getMessage() );
        }
        try {
            this.url  = getValue(jsonResponse, kProductTypeImageURL,String.class);
        }catch (Exception e){
            Log.e(getClass().getSimpleName(), e.getMessage() );
        }
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        this.isSelected = selected;
    }
}
