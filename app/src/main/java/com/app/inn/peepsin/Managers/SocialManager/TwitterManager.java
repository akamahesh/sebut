package com.app.inn.peepsin.Managers.SocialManager;

/**
 * Created by mahesh on 19/4/17.
 */

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.app.inn.peepsin.Models.SocialUser;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.models.User;

import io.fabric.sdk.android.Fabric;
import retrofit2.Call;

/**
 * TwitterId used                   : Kuldeep.appsstreet@gmail.com
 * Consumer Key (API Key) 		    : IpaT7ZM6LHjlXBCDAqJeqGzkb
 * Consumer Secret (API Secret)     : 2xAUKouO4ayuKwxj2NZMnP2UQYK8KyvhRLgUpNyrdlS9dbYvdn
 */

public class TwitterManager {

    private TwitterAuthClient client;
    private Context context;
    private TwitterManagerInterface twitterManagerListener;

    public TwitterManager(Context context, TwitterManagerInterface twitterManagerListener) {
        this.context = context;
        this.twitterManagerListener = twitterManagerListener;
        TwitterAuthConfig authConfig = new TwitterAuthConfig(SocialConstants.TWITTER_API_KEY, SocialConstants.TWITTER_API_SECRET);
        Fabric.with(context, new Twitter(authConfig));
        client = new TwitterAuthClient();
    }


    private Callback<TwitterSession> twitterSessionCallback = new Callback<TwitterSession>() {
        @Override
        public void success(Result<TwitterSession> result) {
            login(result);
        }

        @Override
        public void failure(TwitterException exception) {
            Log.e("Twitter",exception.toString());
            twitterManagerListener.twitterFailure(exception.getMessage());
        }
    };


    public void authorize() {
        client.authorize((Activity) context, getTwitterSessionCallback());
    }

    private Callback<TwitterSession> getTwitterSessionCallback() {
        return twitterSessionCallback;
    }

    public TwitterAuthClient getTwitterAuthClient() {
        return client;
    }


    //The login function accepting the result object
    public void login(Result<TwitterSession> result) {

        final TwitterSession session = result.data;
        client.requestEmail(session, new Callback<String>() {
            @Override
            public void success(Result<String> result) {
                String email = result.data;
                Call<User> call = Twitter.getApiClient(session).getAccountService().verifyCredentials(Boolean.TRUE, Boolean.TRUE);
                call.enqueue(new Callback<User>() {
                    @Override
                    public void success(Result<User> userResult) {
                        User user = userResult.data;
                        SocialUser socialUser = new SocialUser(user, email);
                        twitterManagerListener.twitterSuccess(socialUser);
                    }

                    @Override
                    public void failure(TwitterException exception) {
                        Log.e("Twitter",exception.toString());
                        twitterManagerListener.twitterFailure(exception.getMessage());
                    }
                });
            }

            @Override
            public void failure(TwitterException exception) {
                Log.e("Twitter",exception.toString());
                twitterManagerListener.twitterFailure(exception.toString());
            }
        });
    }

    public interface TwitterManagerInterface {
        void twitterSuccess(SocialUser user);
        void twitterFailure(String exception);
    }

}
