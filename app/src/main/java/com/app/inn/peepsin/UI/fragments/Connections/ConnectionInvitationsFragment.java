package com.app.inn.peepsin.UI.fragments.Connections;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Telephony;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.Connection;
import com.app.inn.peepsin.Models.People;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.fragments.Profile.UserProfileFragment;
import com.app.inn.peepsin.UI.helpers.DividerItemRecyclerDecoration;
import com.app.inn.peepsin.UI.helpers.ItemOffsetDecoration;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kUserId;

/**
 * Created by mahesh on 6/4/17.
 */

public class ConnectionInvitationsFragment extends Fragment {
    private static Switcher switcherListener;
    private ProgressDialog progressDialog;
    AppBarLayout appBarLayout;

    private PeopleAdapter peopleAdapter;

    private List<People> userList;
    @BindView(R.id.recycler_view_people)
    RecyclerView recyclerViewPeople;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.empty_view)
    View emptyView;

    public static Fragment newInstance(Switcher switcherInterface, int tabselected) {
        switcherListener = switcherInterface;
        ConnectionInvitationsFragment fragment = new ConnectionInvitationsFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("tabSelected", tabselected);

        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userList = new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_invitations, container, false);
        ButterKnife.bind(this, view);
        progressDialog = Utils.generateProgressDialog(getContext(), true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.HORIZONTAL, false);
        recyclerViewPeople.setLayoutManager(layoutManager);
   /* recyclerViewPeople.addItemDecoration(
        new DividerItemRecyclerDecoration(getActivity(), R.drawable.canvas_recycler_divider));*/
        peopleAdapter = new PeopleAdapter(userList);
        recyclerViewPeople.setAdapter(peopleAdapter);
        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(getContext(), R.dimen.item_offset);
        recyclerViewPeople.addItemDecoration(itemDecoration);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData();
    }

    @OnClick(R.id.tv_seeall)
    void people() {
        switcherListener
                .switchFragment(OnlineFirendsFragment.newInstance(switcherListener, 0), true, true);
    }

    @OnClick(R.id.iv_google)
    void google() {
        switcherListener.switchFragment(GoogleFragment.newInstance(), true, true);
    }

    @OnClick(R.id.iv_facebook)
    void facebook() {
        switcherListener.switchFragment(FacebookFragment.newInstance(), true, true);
    }

    @OnClick(R.id.iv_phone)
    void phone() {
        switcherListener.switchFragment(PhoneFragment.newInstance(), true, true);
    }


    @OnClick(R.id.message_view)
    void onMessage() {
        getInviteLink(1);
    }

    @OnClick(R.id.gmail_view)
    void onMail() {
        getInviteLink(2);
    }

    @OnClick(R.id.more_view)
    void more() {
        getInviteLink(3);
    }

    /* type 1: phone, type 2:email , type 3:messenger */
    public void getInviteLink(int type) {
        showProgress(true);
        ModelManager.modelManager()
                .getInviteLink((Constants.Status iStatus, GenricResponse<String> genericResponse) -> {
                    showProgress(false);
                    String inviteLink = genericResponse.getObject();
                    if (type == 1) {
                        sendInviteLinkSMS(inviteLink);
                    } else if (type == 2) {
                        sendInviteLinkEmail(inviteLink);
                    } else {
                        shareIntent(inviteLink);
                    }
                    //sendLinkMessenger(inviteLink);
                }, (Constants.Status iStatus, String message) -> {
                    showProgress(false);
                    Toaster.toast(message);
                });
    }

    void sendInviteLinkEmail(String url) {
        String subject = "Invitation for PeepsIn";
        String message =
                "I Just downloaded Peeps chat Application on my Android Phone.Get it now from" + "\n\n"
                        + url + "\n\n\n" + "Send from my Android Phone.";

        Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
        emailIntent.setType("text/plain");
        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
        emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, message);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        try {
            startActivity(emailIntent);
        } catch (Exception ex) {
            Toaster.kalaToast("You don't have Gmail App");
        }

        /*Intent email = new Intent(Intent.ACTION_SEND);
        email.putExtra(Intent.EXTRA_EMAIL, new String[]{to});
        email.putExtra(Intent.EXTRA_SUBJECT, subject);
        email.putExtra(Intent.EXTRA_TEXT, message);
        //need this to prompts email client only
        email.setType("message/rfc822");
        startActivity(Intent.createChooser(email, "Choose an Email client :"));*/
    }

    void sendLinkMessenger(String url) {
        boolean installed = appInstalledOrNot("com.facebook.orca");
        if (installed) {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT,
                    "Check out Peepsin app for your Android.Download it today from" + " " + url);
            sendIntent.setType("text/plain");

            sendIntent.setPackage("com.facebook.orca");
            startActivity(sendIntent);
        } else {
            Toaster.toast("App is not currently installed on your phone");
        }
    }


    public void loadData() {
        if(peopleAdapter.getItemCount()<=0)
            progressBar.setVisibility(View.VISIBLE);
        String lat="";
        String lng="";
        try {
            lat = ModelManager.modelManager().getCurrentUser().getUserLocation().getLatitude();
            lng = ModelManager.modelManager().getCurrentUser().getUserLocation().getLongitude();
        } catch (Exception e){
            e.printStackTrace();
        }
        ModelManager.modelManager().getFriendSuggestions(1,lat, lng,
                (Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<People>> genericResponse) -> {
                    progressBar.setVisibility(View.GONE);
                    userList.clear();
                    userList.addAll(genericResponse.getObject());
                    peopleAdapter.notifyDataSetChanged();
                    checkEmptyView();
                }, (Constants.Status iStatus, String message) -> {
                    Log.e(getClass().getSimpleName(), message);
                    progressBar.setVisibility(View.GONE);
                    checkEmptyView();
                    Toaster.toast(message);
                });
    }

    void shareIntent(String url) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT,
                "Check out Peepsin app for your Android.Download it today from" + " " + url);
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    void sendInviteLinkSMS(String url) {
        Intent sendIntent;
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            String defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(getActivity());

            sendIntent = new Intent(Intent.ACTION_SEND);
            sendIntent.setType("text/plain");
            sendIntent.putExtra(Intent.EXTRA_TEXT,
                    "Check out Peepsin app for your Android.Download it today from" + " " + url);

            if (defaultSmsPackageName != null) {
                sendIntent.setPackage(defaultSmsPackageName);
                startActivity(sendIntent);
            } else {
                Toaster.toast("Device does not have SMS functionality");
            }
        } else {
            PackageManager pm = getContext().getPackageManager();
            if (pm.hasSystemFeature(PackageManager.FEATURE_TELEPHONY)) {
                sendIntent = new Intent(Intent.ACTION_VIEW);
                sendIntent.putExtra("sms_body",
                        "Check out Peepsin app for your Android.Download it today from" + " " + url);
                sendIntent.setData(Uri.parse("smsto:" + Uri.encode("")));
                sendIntent.setType("vnd.android-dir/mms-sms");
                startActivity(sendIntent);
            } else {
                Toaster.toast("Device does not have SMS functionality");
            }
        }
    }

    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = getActivity().getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return false;
    }

    private void checkEmptyView(){
        if(peopleAdapter.getItemCount()==0)
            emptyView.setVisibility(View.VISIBLE);
        else
            emptyView.setVisibility(View.GONE);
    }

    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) {
                progressDialog.cancel();
            }
        }
    }

    class PeopleAdapter extends RecyclerView.Adapter<PeopleAdapter.ViewHolder> {

        List<People> users;
        private List<People> filterList;


        PeopleAdapter(List<People> userList) {
            users = userList;
            this.filterList = userList;
        }

        @Override
        public PeopleAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_card_people_you_may_know_layout, parent, false);
            return new PeopleAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(PeopleAdapter.ViewHolder holder, int position) {
            People people = users.get(position);
            holder.bindContent(people);
            String name = people.getFullName();
            String rating = Integer.toString(people.getRating());
            String profileURL = people.getProfilePicURL();
            String suggestionTag = people.getSuggestionTag();

            if (position == 9) {
                holder.pardaView.setVisibility(View.VISIBLE);
                holder.pardaView.setBackgroundColor(Color.parseColor("#ffffff"));
            } else {
                holder.pardaView.setVisibility(View.GONE);

                holder.tvName.setText(name);
                holder.tvStatus.setText(suggestionTag);
     /* if(people.isRequestSent()){
        holder.btnAddFriend.setText("Request Sent");
        holder.btnAddFriend.setTextColor(getResources().getColor(R.color.activeState));
        holder.btnAddFriend.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,getResources().getDrawable(R.drawable.ic_check_circle_green_500_24dp),null);
      } else{
        holder.btnAddFriend.setTextColor(getResources().getColor(R.color.text_color_regular));
        holder.btnAddFriend.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,getResources().getDrawable(R.drawable.ic_person_add_grey_500_18dp),null);
        holder.btnAddFriend.setText("Add Friend");
      }
*/


                if (profileURL.isEmpty()) {
                    holder.ivUserImageCircular.setVisibility(View.INVISIBLE);
                    holder.ivUserImage.setVisibility(View.VISIBLE);
                } else {
                    holder.ivUserImageCircular.setVisibility(View.VISIBLE);
                    holder.ivUserImage.setVisibility(View.INVISIBLE);
                }

                TextDrawable drawable = TextDrawable.builder()
                        .buildRound(String.valueOf(name.toUpperCase().charAt(0)),
                                ColorGenerator.MATERIAL.getRandomColor());

                if (!profileURL.isEmpty()) {
                    Picasso.with(getContext())
                            .load(profileURL)
                            .placeholder(R.drawable.male_placeholder)
                            .resize(100, 100)
                            .into(holder.ivUserImageCircular);
                } else {
                    holder.ivUserImage.setImageDrawable(drawable);
                }
            }
        }



      /*  @Override
        public int getItemCount() {
            return users.size();
        }*/


    @Override
    public int getItemCount() {
      int count = users.size();
      if(count==0||users.size()<=10){
        return users.size();
      }else return 10;
    }


        private Filter getFilter() {
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence charSequence) {

                    String charString = charSequence.toString();
                    if (charString.isEmpty()) {
                        users = filterList;
                    } else {
                        ArrayList<People> filteredList = new ArrayList<>();
                        for (People contacts : users) {
                            if (contacts.getFirstName().toLowerCase().contains(charString)) {
                                filteredList.add(contacts);
                            }
                        }
                        users = filteredList;
                    }
                    FilterResults filterResults = new FilterResults();
                    filterResults.values = users;
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                    users = (List<People>) filterResults.values;
                    notifyDataSetChanged();
                    //checkEmptyScreen();
                }
            };
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            People people;
            @BindView(R.id.tv_status)
            TextView tvStatus;
            @BindView(R.id.btn_add_friend)
            Button btnAddFriend;
            @BindView(R.id.tv_name)
            TextView tvName;
            @BindView(R.id.iv_user_image_circular)
            ImageView ivUserImageCircular;
            @BindView(R.id.iv_user_image)
            ImageView ivUserImage;
            @BindView(R.id.view_parda)
            View pardaView;

            public ViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            @OnClick(R.id.view_parda)
            void onPeopleYouMayKnow(){
                if (switcherListener != null) {
                    switcherListener.switchFragment(PeopleYouMayKnowFragment.newInstance(switcherListener, 0), true, true);
                }
            }

            @OnClick(R.id.btn_add_friend)
            void onAddFriend(){
                sendFriendRequest(people);
            }

            private void sendFriendRequest(People people){
                HashMap<String,Object> parameters = new HashMap<>();
                parameters.put(kUserId, people.getUserId());
                showProgress(true);
                ModelManager.modelManager().sendFriendRequest(parameters,(Constants.Status iStatus) -> {
                    users.remove(people);
                    filterList.remove(people);
                    notifyDataSetChanged();
                    getFilter().filter("");
        /*  edtSearch.setText("");
          checkEmptyScreen();*/
                    showProgress(false);
                }, (Constants.Status iStatus, String message) -> {
                    showProgress(false);
                    //  checkEmptyScreen();
                    Toaster.toast(message);
                });
            }

            @OnClick(R.id.item_view)
            void onItem(){
                if(getAdapterPosition()==getItemCount()&&switcherListener!=null){
                    switcherListener
                            .switchFragment(PeopleYouMayKnowFragment.newInstance(switcherListener, 0), true, true);
                }else {
                    getUserProfile(people.getUserId());
                }
            }
    /*  @OnClick(R.id.btn_add_friend)
      void onAddFriend(){
        if(people.isRequestSent()){
          Toaster.toastRangeen("Request already sent");
          return;
        }
        sendFriendRequest(people);
      }*/

            void getUserProfile(int userId) {
                showProgress(true);
                HashMap<String, Object> parameters = new HashMap<>();
                parameters.put(kUserId,userId);
                ModelManager.modelManager().getUserProfile(parameters,(Constants.Status iStatus, GenricResponse<Connection> genricResponse) -> {
                    showProgress(false);
                    Connection connection = genricResponse.getObject();
                    if(connection.getProfileVisibility()!=0){
                        if(switcherListener!=null)
                            switcherListener.switchFragment(
                                    UserProfileFragment.newInstance(switcherListener,connection),true,true);
                    }else
                        Utils.showAlertDialog(getContext(),"Message","User Profile permission is denied");

                }, (Constants.Status iStatus, String message) -> {
                    Log.e("User Profile",message);
                    Toaster.toast(message);
                    showProgress(false);
                });
            }

     /* private void sendFriendRequest(People people){
        HashMap<String,Object> parameters = new HashMap<>();
        parameters.put(kUserId, people.getUserId());
        showProgress(true);
        ModelManager.modelManager().sendFriendRequest(parameters,(Constants.Status iStatus) -> {
          people.setRequestSent(true);
          notifyItemChanged(getAdapterPosition());
          showProgress(false);
        }, (Constants.Status iStatus, String message) -> {
          showProgress(false);
          Toaster.toast(message);
        });
      }*/


            void bindContent(People people) {
                this.people = people;
            }
        }

    }
}

