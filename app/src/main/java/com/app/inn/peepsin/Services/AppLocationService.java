package com.app.inn.peepsin.Services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Created by akamahesh on 15/5/17.
 */

public class AppLocationService extends Service implements LocationListener {
    private static final String TAG = AppLocationService.class.getSimpleName();
    protected LocationManager locationManager;
    Location location;
    private static final long MIN_DISTANCE_FOR_UPDATE = 10;
    private static final long MIN_TIME_FOR_UPDATE = 1000 * 60 * 2;


    public AppLocationService() {
    }

    public AppLocationService(Context context) {
        locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
    }

    public Location getLocation(String provider) {
        if (locationManager.isProviderEnabled(provider)) {
            locationManager.requestLocationUpdates(provider, MIN_TIME_FOR_UPDATE, MIN_DISTANCE_FOR_UPDATE, this);

            if(locationManager!=null){
                location = locationManager.getLastKnownLocation(provider);
                return location;
            }
        }
        return null;
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public void onLocationChanged(Location location) {
        Log.v(TAG,"onLocationChanged :  "+location.toString());
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.v(TAG,"onStatusChanged :  "+provider);
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.v(TAG,"onProviderDisabled :  "+provider);
    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.v(TAG,"onProviderDisabled :  "+provider);
    }
}
