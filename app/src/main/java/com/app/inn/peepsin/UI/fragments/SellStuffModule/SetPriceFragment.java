package com.app.inn.peepsin.UI.fragments.SellStuffModule;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Selection;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.squareup.picasso.Picasso;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnEditorAction;

import static com.app.inn.peepsin.Constants.Constants.kImageUrl;


/**
 * Created by akamahesh on 2/5/17.
 */

public class SetPriceFragment extends Fragment{

    @BindView(R.id.tv_message)TextView tvMessage;
    @BindView(R.id.tv_title)TextView tvTitle;
    @BindView(R.id.edt_price) EditText edtPrice;
    @BindView(R.id.iv_product_image) ImageView ivProduct;

    private Context context;
    private FragmentInteractionListener mListener;
    private String imagePath;
    private int price = 0;

    public interface FragmentInteractionListener{
        void priceNext(int price);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        if (context instanceof FragmentInteractionListener) {
            mListener = (FragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement FragmentInteractionListener");
        }
    }

    public static Fragment newInstance(String path){
        Fragment fragment =  new SetPriceFragment();
        Bundle bdl = new Bundle();
        bdl.putString(kImageUrl,path);
        fragment.setArguments(bdl);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if(bundle!= null){
            imagePath = bundle.getString(kImageUrl);
        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_set_price, container, false);
        ButterKnife.bind(this,view);
        tvTitle.setText(getString(R.string.set_price));
        File imgFile = new  File(imagePath);
        if(imgFile.exists()){
            Picasso.with(context)
                    .load(imgFile)
                    .fit()
                    .into(ivProduct);
        }
        edtPrice.setFilters(new InputFilter[] {new InputFilter.LengthFilter(8)});

        //setEditText();
        return view;
    }

    private void setEditText() {
        Selection.setSelection(edtPrice.getText(), edtPrice.getText().length());
        edtPrice.setText(getString(R.string.Rs));
        edtPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().contains(getString(R.string.Rs))) {
                    edtPrice.setText(getString(R.string.Rs));
                    Selection.setSelection(edtPrice.getText(), edtPrice.getText().length());
                }
            }
        });
    }

    @OnEditorAction(R.id.edt_price)
    boolean submit(){
        next();
        return true;
    }

    @OnClick(R.id.btn_next)
    void next(){
        if(!edtPrice.getText().toString().isEmpty()) {
            price = Integer.parseInt(edtPrice.getText().toString());
            /*String money = edtPrice.getText().toString().substring(1);
            if(!money.isEmpty())
                price = Integer.parseInt(money);*/
        }
        Utils.hideKeyboard(getContext());
        mListener.priceNext(price);
    }

    @OnClick(R.id.btn_cancel)
    void cancel(){
        Utils.hideKeyboard(getContext());
        getActivity().finish();
    }



    @Override
    public void onDetach() {
        super.onDetach();
        mListener=null;
    }
}
