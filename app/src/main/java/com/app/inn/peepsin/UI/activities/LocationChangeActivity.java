package com.app.inn.peepsin.UI.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Blocks.Block;
import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Libraries.DispatchQueue.DispatchQueue;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.PhoneContact;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.Services.GPSTracker;
import com.app.inn.peepsin.UI.adapters.PlaceAutocompleteAdapter;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.helpers.Validations;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static com.app.inn.peepsin.Constants.Constants.kAuthToken;
import static com.app.inn.peepsin.Constants.Constants.kCity;
import static com.app.inn.peepsin.Constants.Constants.kCountry;
import static com.app.inn.peepsin.Constants.Constants.kLatitude;
import static com.app.inn.peepsin.Constants.Constants.kLongitude;
import static com.app.inn.peepsin.Constants.Constants.kState;
import static com.app.inn.peepsin.Constants.Constants.kStreetAddress;
import static com.app.inn.peepsin.Constants.Constants.kZipcode;

/**
 * Created by dhruv on 13/9/17.
 */

public class LocationChangeActivity extends BaseActivity implements PlaceAutocompleteAdapter.PlaceAutoCompleteInterface, GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks{

    private final int REQUEST_PERMISSION_LOCATION = 102;
    @BindView(R.id.list_search)
    RecyclerView mRecyclerView;
    @BindView(R.id.search_et)
    EditText mSearchEdittext;
    @BindView(R.id.clear)
    ImageView mClear;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.progressBar)
    ProgressBar progressGps;

    private Dialog dialog;
    private ProgressDialog progressDialog;
    private GoogleApiClient mGoogleApiClient;

    private PlaceAutocompleteAdapter mAdapter;
    private static final LatLngBounds BOUNDS_INDIA = new LatLngBounds(
            new LatLng(-0, 0), new LatLng(0, 0));

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
        setContentView(R.layout.activity_location_change_layout);
        ButterKnife.bind(this);
        progressDialog = Utils.generateProgressDialog(this, true);
        tvTitle.setText(getString(R.string.change_location_title));

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 0, this)
                .addApi(Places.GEO_DATA_API)
                .build();
        initViews();
    }

    @Override
    public void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
    }

    private void initViews(){
        mAdapter = new PlaceAutocompleteAdapter(this, R.layout.row_placesearch,
                mGoogleApiClient, BOUNDS_INDIA, null);
        mRecyclerView.setHasFixedSize(false);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(mAdapter);
        // adding text change listener to search
        mSearchEdittext.addTextChangedListener(textWatcher);
    }

    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (count > 0) {
                mClear.setVisibility(View.VISIBLE);
                if (mAdapter != null) {
                    mRecyclerView.setAdapter(mAdapter);
                }
            } else {
                mClear.setVisibility(View.GONE);
            }
            if (!s.toString().equals("") && mGoogleApiClient.isConnected()) {
                mAdapter.getFilter().filter(s.toString());
            } else if (!mGoogleApiClient.isConnected()) {
                Log.e("", "NOT CONNECTED");
            }
        }

        @Override
        public void afterTextChanged(Editable s) {}
    };

    @OnClick(R.id.clear)
    public void onClear(View v) {
        if(v == mClear){
            mSearchEdittext.setText("");
            if(mAdapter!=null){
                mAdapter.clearList();
            }
        }
    }

    @OnClick(R.id.btn_back)
    public void onBack(){
        finish();
    }

    @Override
    public void onConnected(Bundle bundle) {
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    @Override
    public void onPlaceClick(PlaceAutocompleteAdapter.PlaceAutocomplete mResultList, int position) {
        if(mResultList!=null){
            try {
                final String placeId = String.valueOf(mResultList.placeId);
                /*
                  Issue a request to the Places Geo Data API to retrieve a Place object with additional details about the place.
                */
                PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi.getPlaceById(mGoogleApiClient, placeId);
                placeResult.setResultCallback(places -> {
                    if(places.getCount()==1){
                        //Do the things here on Click.....
                        String location = places.get(0).getAddress().toString();
                        double latitude = places.get(0).getLatLng().latitude;
                        double longitude = places.get(0).getLatLng().longitude;
                        getAddress(location,latitude,longitude);
                    }else {
                        Toaster.kalaToast("Unable to get location coordinates");
                    }
                });
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @SuppressLint("NewApi")
    @OnClick(R.id.btn_grant_access)
    void onGrantAccess() {
        if (ContextCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            getLocation();

        } else if (ActivityCompat.shouldShowRequestPermissionRationale(this, ACCESS_FINE_LOCATION)
                        && ActivityCompat.shouldShowRequestPermissionRationale(this, ACCESS_COARSE_LOCATION)) {
            // We've been denied once before. Explain why we need the permission, then ask again.
            Utils.showDialog(this, getString(R.string.location_permission_required_text),
                    getString(R.string.ask_permission_text),
                    getString(R.string.discard_text),
                    (dialog, which) -> {
                        if (which == -1) {
                            requestPermissions(new String[]{ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION},
                                    REQUEST_PERMISSION_LOCATION);
                        } else {
                            dialog.dismiss();
                        }
                    });
        } else {
            // We've never asked. Just do it.
            requestPermissions(new String[]{ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION},
                    REQUEST_PERMISSION_LOCATION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_PERMISSION_LOCATION
                && grantResults[0] == PackageManager.PERMISSION_GRANTED
                && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            getLocation();
        } else {
            // We were not granted permission this time, so don't try to show the contact picker
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void getLocation() {
        showProgressBar(true);
        GPSTracker gps = new GPSTracker(this);
        // check if GPS enabled
        if (gps.canGetLocation()) {
            showProgressBar(false);
            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();
            getAddress("",latitude,longitude);
        } else {
            showProgressBar(false);
            Utils.showAlertDialog(this, "Error!",
                    "Sorry unable to get current location. Make sure your GPS is ON!");
        }
    }

    public void getAddress(String location, double latitude, double longitude){
        showProgress(true);
        getFullAddress(location,latitude,longitude,(Constants.Status iStatus) -> {
            showProgress(false);
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Toaster.kalaToast("Unable to Get Location. Check Connection");
        });
    }

    public void getFullAddress(String location, double latitude, double longitude,Block.Status success, Block.Failure failure) {
        ModelManager.modelManager().getDispatchQueue().async(() -> {
            try {
                String address = "Change Position";
                Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                List<Address> listAddresses = geocoder.getFromLocation(latitude, longitude, 1);
                if(null!=listAddresses&&listAddresses.size()>0){
                    address = listAddresses.get(0).getLocality()+", "+listAddresses.get(0).getAdminArea()+", "+listAddresses.get(0).getCountryName();
                }
                generateHashMap(listAddresses.get(0).getAddressLine(0)
                        ,listAddresses.get(0).getLocality()
                        ,listAddresses.get(0).getAdminArea()
                        ,listAddresses.get(0).getCountryName()
                        ,listAddresses.get(0).getPostalCode()
                        ,String.valueOf(latitude),String.valueOf(longitude));
                if(location.isEmpty())
                    changeLocationResult(address,latitude,longitude);
                else
                    changeLocationResult(location,latitude,longitude);
                DispatchQueue.main(() -> success.iStatus(Constants.Status.success));
            } catch (Exception e) {
                DispatchQueue.main(() -> failure.iFailure(Constants.Status.fail, e.getMessage()));
            }
        });
    }

    private void changeLocationResult(String location, double lat, double lon){
        Intent data = new Intent();
        data.putExtra("name",location);
        data.putExtra("lat",String.valueOf(lat));
        data.putExtra("lng",String.valueOf(lon));
        setResult(LocationChangeActivity.RESULT_OK, data);
        onBack();
    }

    private void generateHashMap(String streetAddress, String city, String state, String country, String pincode, String lat,String lng){
        HashMap<String,Object> hashMap = new HashMap<>();
        hashMap.put(kStreetAddress, streetAddress);
        hashMap.put(kCity, city);
        hashMap.put(kState, state);
        hashMap.put(kCountry, country);
        hashMap.put(kZipcode, pincode);
        hashMap.put(kLatitude, lat);
        hashMap.put(kLongitude, lng);
        updateLocation(hashMap);
    }

    private void updateLocation(HashMap<String, Object> hashMap) {
        String authToken = ModelManager.modelManager().getCurrentUser().getAuthToken();
        hashMap.put(kAuthToken, authToken);
        ModelManager.modelManager().updateUserLocation(hashMap, (Constants.Status iStatus) -> {
        }, (Constants.Status iStatus, String message) ->{
            Toaster.kalaToast(message);
            //Utils.showAlertDialog(this, "Alert", message);
        });
    }

    private void showProgress(boolean b){
        if (b) {
            progressGps.setVisibility(View.VISIBLE);
            /*dialog = new Dialog(LocationChangeActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.dialog_loading_address);
            dialog.show();*/
        } else {
            progressGps.setVisibility(View.GONE);
            //if (dialog != null) dialog.dismiss();
        }
    }

    private void showProgressBar(boolean b) {
        if (b) {
            progressGps.setVisibility(View.VISIBLE);
        } else {
            progressGps.setVisibility(View.GONE);
        }
    }

}
