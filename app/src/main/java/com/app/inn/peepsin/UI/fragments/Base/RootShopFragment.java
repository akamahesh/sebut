package com.app.inn.peepsin.UI.fragments.Base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.fragments.Shop.ShoppingFragment;
import com.app.inn.peepsin.UI.helpers.FragmentUtil;
import com.app.inn.peepsin.UI.interfaces.Switcher;

/**
 * Created by mahesh on 25/4/17.
 */

public class RootShopFragment extends Fragment {

  public static RootShopFragment newInstance() {
    return new RootShopFragment();
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    return inflater.inflate(R.layout.fragment_root, container, false);
  }


  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    FragmentUtil.changeFragment(getChildFragmentManager(), ShoppingFragment.newInstance(switcher), true,
            false);
    //FragmentUtil.changeFragment(getChildFragmentManager(), ParentShopFragment.newInstance(switcher),true,false);
  }

  Switcher switcher = (fragment, saveInBackStack, animate) ->{
    FragmentUtil.addFragment(getChildFragmentManager(), fragment, saveInBackStack, animate);
  };

  @Override
  public void onDetach() {
    super.onDetach();
    switcher = null;
  }

}
