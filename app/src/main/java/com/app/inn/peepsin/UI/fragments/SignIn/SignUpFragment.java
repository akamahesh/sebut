package com.app.inn.peepsin.UI.fragments.SignIn;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.method.PasswordTransformationMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.APIManager.ReachabilityManager;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.activities.LogInActivity;
import com.app.inn.peepsin.UI.dialogFragments.PolicyDialogFragment;
import com.app.inn.peepsin.UI.helpers.FragmentUtil;
import com.app.inn.peepsin.UI.helpers.TickProgress;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.helpers.Validations;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kAuthToken;
import static com.app.inn.peepsin.Constants.Constants.kEmail;
import static com.app.inn.peepsin.Constants.Constants.kFirstName;
import static com.app.inn.peepsin.Constants.Constants.kIsAlreadyRegistered;
import static com.app.inn.peepsin.Constants.Constants.kJabberId;
import static com.app.inn.peepsin.Constants.Constants.kOTP;
import static com.app.inn.peepsin.Constants.Constants.kPassword;
import static com.app.inn.peepsin.Constants.Constants.kPhone;
import static com.app.inn.peepsin.Constants.Constants.kReferralCode;
import static com.app.inn.peepsin.Constants.Constants.kRegType;

/**
 * Created by akaMahesh on 2/5/17
 * contact : mckay1718@gmail.com
 */

public class SignUpFragment extends Fragment {

    @BindView(R.id.btn_signup)
    Button btnSignup;
    @BindView(R.id.tv_terms)
    TextView textViewTerms;
    @BindView(R.id.ticky_progress)
    TickProgress tickProgress;
    @BindView(R.id.btn_promo_code)
    Button btnPromocode;
    @BindView(R.id.til_promo)
    TextInputLayout tilPromo;
    @BindView(R.id.edt_first_name)
    EditText editTextFirstName;
    @BindView(R.id.edt_email)
    EditText editTextEmail;
    @BindView(R.id.edt_password)
    EditText editTextPwd;
    @BindView(R.id.edt_promo)
    EditText etPromo;
    private ProgressDialog progressDialog;
    private static OnSignUpFragmentInteractionListener mListener;
    private String mEmail = "";

    public static Fragment newInstance(String email) {
        Fragment fragment = new SignUpFragment();
        Bundle bundle = new Bundle();
        bundle.putString(kEmail, email);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnSignUpFragmentInteractionListener) {
            mListener = (OnSignUpFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnSignUpFragmentInteractionListener");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mEmail = getArguments().getString(kEmail);
        progressDialog = Utils.generateProgressDialog(getContext(), false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sign_up, container, false);
        ButterKnife.bind(this, view);
        clickableTermsSpan(textViewTerms);
        ((LogInActivity) getActivity()).setTitle("Sign Up");
        editTextEmail.setText(mEmail);
        editTextEmail.setSelection(mEmail.length());
        editTextPwd.setTransformationMethod(new PasswordTransformationMethod());
        editTextPwd.setTypeface(Typeface.DEFAULT);
        //hide #edtpromocode at first and make #tvPromocode visible
        tilPromo.setVisibility(View.INVISIBLE);
        btnPromocode.setVisibility(View.VISIBLE);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        editTextEmail.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                String contactId = Utils.getProperText(editTextEmail);
                if(contactId.isEmpty())
                    return;
                checkUserExistance(Utils.getProperText(editTextEmail));
            }
        });
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @OnClick(R.id.btn_promo_code)
    void onPromoCode() {
        btnPromocode.setVisibility(View.INVISIBLE);
        tilPromo.setVisibility(View.VISIBLE);
        etPromo.requestFocus();
    }

    @OnClick(R.id.btn_signup)
    public void onSignup() {
        Utils.hideKeyboard(getContext());
        String contactID = Utils.getProperText(editTextEmail);
        if (!ReachabilityManager.getNetworkStatus()) {
            Utils.showAlertDialog(getContext(), "Signup Failed!",
                    "Sorry, Failed to reach PeepsIn servers. Please check your network or try again later.");
            return;
        }
        if (!validateData()) {
            return;
        }
        if (!isEligible) {
            showLoginAlert(contactID);
            return;
        }

        HashMap<String, Object> userMap = getParameters();

        showProgress(true);
        ModelManager.modelManager().loginByOtp(contactID, (Constants.Status iStatus, GenricResponse<HashMap<String, Object>> genricResponse) -> {
            showProgress(false);
            HashMap<String, Object> hashMap = genricResponse.getObject();
            String authToken = (String) hashMap.get(kAuthToken);
            String OTP = (String) hashMap.get(kOTP);
            int type = 1;
            if (Validations.isValidEmail(contactID)) {
                type = 2;
            }
            if (mListener != null)
                mListener.verifyUser(OTP, contactID, userMap);
        }, (Constants.Status iStatus, String message) -> {
            Utils.showAlertDialog(getContext(), "Otp Failed!", message);
        });

    }

    private void showLoginAlert(String contactID) {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_login_view);
        Button btnChange = ButterKnife.findById(dialog, R.id.btn_action);
        btnChange.setOnClickListener(v -> {
            dialog.dismiss();
            mListener.moveToSignIn(contactID);
        });
        dialog.show();
    }

    @OnClick(R.id.tv_login)
    public void onLogin() {
        if (mListener != null)
            mListener.onBack();
    }


    ClickableSpan termsSpanListener = new ClickableSpan() {
        @Override
        public void onClick(View widget) {
            FragmentUtil.showDialog(getChildFragmentManager(), PolicyDialogFragment.newInstance(2), false);
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setUnderlineText(false);
            ds.setColor(getResources().getColor(R.color.link_color));
        }
    };

    ClickableSpan privacySpanListener = new ClickableSpan() {
        @Override
        public void onClick(View widget) {
            FragmentUtil.showDialog(getChildFragmentManager(), PolicyDialogFragment.newInstance(1), false);
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setUnderlineText(false);
            ds.setColor(getResources().getColor(R.color.link_color));
        }
    };

    /**
     * Click span for Terms and Conditions
     * String str= "By continuing you agree to the Privacy Policy and Terms of Service"
     *
     * @param mTextViewTerms textview Where clickable span will be applied
     */
    private void clickableTermsSpan(TextView mTextViewTerms) {
        SpannableStringBuilder ssb = new SpannableStringBuilder();
        String termsText = getString(R.string.terms_and_policies);
        ssb.append(termsText);
        int pStart = termsText.indexOf('P');
        int pEnd = pStart + 14;
        int tStart = termsText.indexOf('T');
        int tEnd = tStart + 16;

        ssb.setSpan(privacySpanListener, pStart, pEnd, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        ssb.setSpan(termsSpanListener, tStart, tEnd, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mTextViewTerms.setText(ssb);
        mTextViewTerms.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private boolean validateData() {
        boolean isOk = true;
        EditText etValid;
        String testString;
        View requestFocus = null;

        etValid = editTextFirstName;
        testString = etValid.getText().toString().toLowerCase().trim();
        if (!Validations.isValidFirstName(testString)) {
            if (testString.isEmpty()) {
                etValid.setError(getString(R.string.error_cannot_be_empty));
            } else {
                etValid.setError(getString(R.string.error_invalid_first_name));
            }
            requestFocus = etValid;
            isOk = false;
        }

        etValid = editTextEmail;
        testString = etValid.getText().toString().toLowerCase().trim();
        if (Validations.isValidPhone(testString) || Validations.isValidEmail(testString)) {

        } else {
            if (testString.isEmpty()) {
                etValid.setError(getString(R.string.error_cannot_be_empty));
            } else {
                etValid.setError("Invalid email/phone ");
            }
            requestFocus = etValid;
            isOk = false;
        }

        etValid = editTextPwd;
        testString = etValid.getText().toString().toLowerCase().trim();
        if (!Validations.isValidPassword(testString)) {
            if (testString.isEmpty()) {
                etValid.setError(getString(R.string.error_cannot_be_empty));
            } else {
                etValid.setError(getString(R.string.error_invalid_password));
            }
            requestFocus = etValid;
            isOk = false;
        }

        if (requestFocus != null) {
            requestFocus.requestFocus();
        }
        return isOk;
    }


    public HashMap<String, Object> getParameters() {
        HashMap<String, Object> parameters = new HashMap<>();
        String contact = Utils.getProperText(editTextEmail);
        String referralCode = Utils.getProperText(etPromo);
        if (Validations.isValidEmail(contact))
            parameters.put(kEmail, Utils.getProperText(editTextEmail));
        else
            parameters.put(kPhone, Utils.getProperText(editTextEmail));
        parameters.put(kFirstName, Utils.getProperText(editTextFirstName));
        parameters.put(kJabberId, Utils.getJabberId(getContext()));
        parameters.put(kPassword, Utils.getProperText(editTextPwd));
        parameters.put(kRegType, 1);
        parameters.put(kReferralCode, referralCode);
        return parameters;
    }


    boolean isEligible = false;
    private void checkUserExistance(String contactId) {
        tickProgress.showProgress();
        editTextEmail.setError(null);
        ModelManager.modelManager().checkRegistration(contactId, (Constants.Status iStatus, GenricResponse<HashMap<String, Object>> genericResponse) -> {
            HashMap<String, Object> hashMap = genericResponse.getObject();
            Integer isAlreadyRegistered = (Integer) hashMap.get(kIsAlreadyRegistered);
            boolean isAlreadyRegister = isAlreadyRegistered == 1;
            if (isAlreadyRegister) {
                isEligible = false;
                tickProgress.showError();
            } else {
                isEligible = true;
                tickProgress.showSuccess();
                editTextEmail.setError(null);
            }
        }, (Constants.Status iStatus, String message) -> {
            Toaster.kalaToast(message);
            tickProgress.showError();
        });
    }

    public interface OnSignUpFragmentInteractionListener {

        void onSignUp(HashMap<String, Object> userMap);

        void moveToSignIn(String contactID);

        void onBack();

        void verifyUser(String otp, String contactID,
                        HashMap<String, Object> userMap);
    }

    public void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else if (progressDialog != null) {
            progressDialog.cancel();
        }
    }

}
