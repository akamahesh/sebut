package com.app.inn.peepsin.Models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by root on 5/7/17.
 */

public class CartShop extends BaseModel implements Serializable{

    private int shopId;
    private String shopName;
    private String shopBannerImageUrl;
    private Address shopAddress;
    private String ownerName;
    private Integer rating;
    private Integer connectionLevel;
    private CopyOnWriteArrayList<ShoppingProduct> productList = new CopyOnWriteArrayList<>();

    public CartShop(JSONObject jsonResponse){
        this.shopId = getValue(jsonResponse, kShopId, Integer.class);
        this.shopName = getValue(jsonResponse,kShopName,String.class);
        this.shopBannerImageUrl = getValue(jsonResponse, kShopBannerImageURl,String.class);
        this.ownerName = getValue(jsonResponse,kOwnerName,String.class);
        this.rating = getValue(jsonResponse,kRating,Integer.class);
        this.connectionLevel = getValue(jsonResponse,kConnectionLevel,Integer.class);
        try {
            this.shopAddress = new Address(getValue(jsonResponse,kShopAddress,JSONObject.class));
        }catch (Exception e){
            e.printStackTrace();
        }
        try {
            this.productList = handleProductList(getValue(jsonResponse,kProductList,JSONArray.class));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private CopyOnWriteArrayList<ShoppingProduct> handleProductList(JSONArray jsonArray) throws JSONException {
        CopyOnWriteArrayList<ShoppingProduct> cartProductList = new CopyOnWriteArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            cartProductList.add(new ShoppingProduct(jsonArray.getJSONObject(i)));
        }
        return cartProductList;
    }


    public int getShopId() {
        return shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public String getShopBannerImageUrl() {
        return shopBannerImageUrl;
    }

    public Address getShopAddress() {
        return shopAddress;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public Integer getRating() {
        return rating;
    }

    public Integer getConnectionLevel() {
        return connectionLevel;
    }

    public CopyOnWriteArrayList<ShoppingProduct> getProductList() {
        return productList;
    }

}
