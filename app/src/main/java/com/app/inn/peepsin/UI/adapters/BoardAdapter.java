package com.app.inn.peepsin.UI.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatDialog;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.Board;
import com.app.inn.peepsin.Models.Comment;
import com.app.inn.peepsin.Models.Connection;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.activities.HomeActivity;
import com.app.inn.peepsin.UI.fragments.Board.PeepsBoardFragment;
import com.app.inn.peepsin.UI.fragments.Feeds.CustomBottomSheetDialogFragment;
import com.app.inn.peepsin.UI.helpers.MyBounceInterpolator;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kCommentCount;
import static com.app.inn.peepsin.Constants.Constants.kIsLike;
import static com.app.inn.peepsin.Constants.Constants.kLikeCount;
import static com.app.inn.peepsin.Constants.Constants.kRecords;
import static com.app.inn.peepsin.Constants.Constants.kTotalRecords;

/**
 * Created by akaMahesh on 7/6/17.
 * copyright to : Innverse Technologies
 */

public class BoardAdapter extends RecyclerView.Adapter<BoardAdapter.ViewHolder> {

    private List<Board> boardList;
    private Context context;
    private static Switcher switcherListener;
    private PeepsBoardFragment fragment;
    private Integer mTotalRecords;
    private BoardAdapterInterface mListener;


    public BoardAdapter(Context context, List<Board> boardList, Switcher switcher, BoardAdapterInterface boardAdapterInterface, PeepsBoardFragment frag) {
        this.context = context;
        this.boardList = boardList;
        this.fragment = frag;
        switcherListener = switcher;
        mListener = boardAdapterInterface;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View layoutView = LayoutInflater.from(context).inflate(R.layout.row_post_board_layout, viewGroup, false);
        return new ViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int i) {

        Board board = boardList.get(i);
        holder.bindContent(board);

        String userName = board.getFullName();
        String description = board.getDescription();
        String userProfileURL = board.getProfilePicURL();
        String boardTime = board.getBoardTime();

        int commentCount = board.getCommentCount();
        int shareCount = board.getShareCount();
        String commentCountText = String.valueOf(commentCount) + " Comments";
        String imageUrl = ModelManager.modelManager().getCurrentUser().getProfilePicURL();
        String commentCountViewReply = "View " + String.valueOf(commentCount - 1) + " more replies...";
        String shareCountText = String.valueOf(shareCount) + " Share";

        int likeCount = board.getLikeCount();
        String likeCountText = String.valueOf(likeCount);

        holder.tvTime.setText(boardTime);
        holder.tvUserName.setText(userName);
        holder.tvPost.setText(description);
        holder.tvCommentCounts.setText(commentCountText);

        holder.tvShareCount.setText(shareCountText);
        holder.tvLikeCounts.setText(likeCountText);

        if(commentCount == 1) {
           holder.tvViewReply.setVisibility(View.GONE);
        }else {
            holder.tvViewReply.setVisibility(View.VISIBLE);
            holder.tvViewReply.setText(commentCountViewReply);
        }

        if (board.getLike()) {
            holder.tvLikeCount.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_like_activity, 0, 0, 0);
        } else {
            holder.tvLikeCount.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_unlike_activity, 0, 0, 0);
        }

        if (!userProfileURL.isEmpty()) {
            holder.ivUserText.setVisibility(View.INVISIBLE);
            holder.ivUserImage.setVisibility(View.VISIBLE);
        } else {
            holder.ivUserText.setVisibility(View.VISIBLE);
            holder.ivUserImage.setVisibility(View.INVISIBLE);
        }
        TextDrawable drawable = TextDrawable.builder().buildRound(String.valueOf(userName.toUpperCase().charAt(0)), ColorGenerator.MATERIAL.getRandomColor());

        if (!userProfileURL.isEmpty()) {
            Picasso.with(context)
                    .load(userProfileURL)
                    .resize(100, 100)
                    .into(holder.ivUserImage);
        } else holder.ivUserText.setImageDrawable(drawable);


        //Comment Element
        Comment commentUser = board.getLastComment();
        String commentTime;
        String commentUserImageURL;
        String commentUserName;
        String comment;
        if (commentUser != null && commentUser.getCommentId() != null) {
            holder.viewComment.setVisibility(View.VISIBLE);
            commentTime = commentUser.getTime();
            commentUserImageURL = commentUser.getProfilePicURL();
            commentUserName = commentUser.getUserName();
            comment = commentUser.getComment();
            holder.tvCommentUserName.setText(commentUserName);
            holder.tvCommentTime.setText(commentTime);
            holder.tvCommentDescription.setText(comment);
            holder.tvCommentDescription.setMovementMethod(new ScrollingMovementMethod());

            if (!commentUserImageURL.isEmpty()) {
                holder.ivCommentUserText.setVisibility(View.INVISIBLE);
                holder.ivCommentUserImage.setVisibility(View.VISIBLE);
            } else {
                holder.ivCommentUserText.setVisibility(View.VISIBLE);
                holder.ivCommentUserImage.setVisibility(View.INVISIBLE);
            }
            TextDrawable draw = TextDrawable.builder().buildRound(String.valueOf(commentUserName.toUpperCase().charAt(0)), ColorGenerator.MATERIAL.getRandomColor());

            if (!commentUserImageURL.isEmpty()) {
                Picasso.with(context)
                        .load(commentUserImageURL)
                        .resize(100, 100)
                        .into(holder.ivCommentUserImage);
            } else holder.ivCommentUserText.setImageDrawable(draw);

        } else holder.viewComment.setVisibility(View.GONE);

        if (!imageUrl.isEmpty())
            Picasso.with(context)
                    .load(imageUrl)
                    .resize(100, 100)
                    .into(holder.ivUserImageComment);
        else
            holder.ivUserImageComment.setImageDrawable(context.getResources().getDrawable(R.drawable.img_profile_placeholder));



        holder.edtComment.setImeOptions(EditorInfo.IME_ACTION_DONE);
        holder.edtComment.setRawInputType(InputType.TYPE_CLASS_TEXT);


        holder.edtComment.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    String comment = holder.edtComment.getText().toString().trim();
                    Utils.hideKeyboard(context);
                    if (comment.isEmpty()) {
                        Toaster.toastRangeen("comment can not be empty");

                    }
                   holder.addComment(comment, board.getBoardId());
                }
                return false;
            }
        });

    }

    @Override
    public int getItemCount() {
        return boardList.size();
    }

    public void addItems(List<Board> object) {
        boardList.clear();
        boardList = object;
        notifyDataSetChanged();
    }

    public void addMoreItems(List<Board> object) {
        boardList.addAll(object);
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private Board board;
        @BindView(R.id.view_comment)
        View viewComment;
        @BindView(R.id.iv_user_image)
        ImageView ivUserImage;
        @BindView(R.id.iv_user_text)
        ImageView ivUserText;
        @BindView(R.id.tv_user_name)
        TextView tvUserName;
        @BindView(R.id.tv_post)
        TextView tvPost;
        @BindView(R.id.tv_time)
        TextView tvTime;
        @BindView(R.id.tv_like)
        TextView tvLikeCount;
        @BindView(R.id.tv_likeCount)
        TextView tvLikeCounts;
        @BindView(R.id.tv_share)
        TextView tvShare;
        @BindView(R.id.tv_shareCount)
        TextView tvShareCount;
        @BindView(R.id.tv_comment_count)
        TextView tvCommentCount;
        @BindView(R.id.tv_commentCount)
        TextView tvCommentCounts;
        @BindView(R.id.edt_comment)
        EditText edtComment;

        //Comment Element

        @BindView(R.id.iv_comment_user_image)
        ImageView ivCommentUserImage;
        @BindView(R.id.iv_comment_user_text)
        ImageView ivCommentUserText;
        @BindView(R.id.tv_comment_user_name)
        TextView tvCommentUserName;
        @BindView(R.id.tv_comment_time)
        TextView tvCommentTime;
        @BindView(R.id.tv_comment)
        TextView tvCommentDescription;
        @BindView(R.id.tv_view_reply)
        TextView tvViewReply;
        @BindView(R.id.iv_user_image_comment)
        ImageView ivUserImageComment;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bindContent(Board board) {
            this.board = board;
        }

        @OnClick(R.id.tv_view_reply)
        void onLeaveCommentShow() {
            mListener.onLeaveComment(board.getBoardId());

        }

       /* @OnClick(R.id.btn_comment)
        void onLeaveComment() {
            String comment = edtComment.getText().toString().trim();
            Utils.hideKeyboard(context);
            if (comment.isEmpty()) {
                Toaster.toastRangeen("comment can not be empty");
                return;
            }
            addComment(comment, board.getBoardId());
        }*/


        //Send the comment
        private void addComment(String comment, Integer mBoardId) {
            ModelManager.modelManager().saveComment(mBoardId, comment, (Constants.Status iStatus, GenricResponse<Comment> genericResponse) -> {
                CopyOnWriteArrayList<Comment> mCommentList = new CopyOnWriteArrayList<>();
                mCommentList.add(genericResponse.getObject());
                fragment.loadDataNotifyChange();
                edtComment.setText("");
            }, (Constants.Status iStatus, String message) -> {
                //Toaster.toast(message);
            });
        }

        @OnClick(R.id.tv_comment_count)
        void onComment() {
            mListener.onLeaveComment(board.getBoardId());

        }

        @OnClick(R.id.tv_like)
        void onLike(TextView textView) {
            final Animation favAnim = AnimationUtils.loadAnimation(context, R.anim.bounce);
            MyBounceInterpolator favInterpolar = new MyBounceInterpolator(.2, 20);
            favAnim.setInterpolator(favInterpolar);
            textView.startAnimation(favAnim);
            likeBoard(board.getBoardId(), board.getLike(), getAdapterPosition());
        }

        void likeBoard(int boardId, Boolean like, int adapterPosition) {
            ModelManager.modelManager().likeDislikeBoard(boardId, like, (Constants.Status iStatus, GenricResponse<HashMap<String, Object>> genricResponse) -> {
                HashMap<String, Object> hashMap = genricResponse.getObject();
                Integer isLike = (Integer) hashMap.get(kIsLike);
                Integer likeCount = (Integer) hashMap.get(kLikeCount);
                if (isLike == 1) {
                    board.setLike(true);
                } else {
                    board.setLike(false);
                }
                board.setLikeCount(likeCount);
                notifyItemChanged(adapterPosition);

            }, (Constants.Status iStatus, String message) -> Toaster.toast(message));
        }

        @OnClick(R.id.tv_share)
        void onShare() {
            fragment.shareCount(board.getBoardId());
        }
    }


    public interface BoardAdapterInterface {
        void onLeaveComment(Integer boardId);
    }
}
