package com.app.inn.peepsin.Models;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by root on 18/5/17.
 */

public class OrderHistory extends BaseModel implements Serializable {

    private Integer orderId;
    private Integer paymentType;
    private String expectedDeliveryDate;
    private String orderDate;
    private Integer orderStatus;
    private String promoCode;
    private Boolean isValid;
    private Address deliveryAddress;
    private String promoCodeTagLine;
    private String totalCartPriceBeforePromoCode;
    private String finalAmountAfterPromoCode;
    private String discountedPrice;
    private Integer totalItemCount;
    private Integer cartItemCount;
    private Integer productCount;
    private String invoiceURL;
    private CopyOnWriteArrayList<FinalProductsRecords> shopRecords;
    private CopyOnWriteArrayList<FinalProductsRecords> shopRecord;
    private CopyOnWriteArrayList<ProductList> productRecords;

    public OrderHistory(JSONObject records) {
        this.orderId        = getValue(records,       kOrderId,               Integer.class);
        this.paymentType    = getValue(records,       kPaymentType,           Integer.class);
        this.expectedDeliveryDate = getValue(records, kExpectedDeliveryDate,  String.class);
        this.orderDate      = getValue(records,       kOrderDate,             String.class);
        this.orderStatus    = getValue(records,       kOrderStatus,           Integer.class);

        this.promoCode          = getValue(records,   kPromoCode,             String.class);
        this.isValid            = getValue(records,   kIsValid,               Boolean.class);
        this.promoCodeTagLine   = getValue(records,   kPromoCodeTagLine,      String.class);
        this.totalCartPriceBeforePromoCode  = getValue(records, kTotalCartPriceBeforePromoCode, String.class);
        this.finalAmountAfterPromoCode      = getValue(records, kFinalAmountAfterPromoCode, String.class);
        this.discountedPrice    = getValue(records,   kDiscountedPrice,       String.class);
        try{
            this.totalItemCount = getValue(records,   kTotalItemCount,        Integer.class);
            this.cartItemCount  = getValue(records,   kCartItemCount,         Integer.class);
            this.productCount   = getValue(records,   kTotalProductCount,     Integer.class);
        }catch (Exception e){
            e.printStackTrace();
        }
        try { //to handle @ClassCastException when addressId is emptyString, not to be removed
            this.deliveryAddress = new Address(getValue(records, kDeliveryAddress, JSONObject.class));
        } catch (Exception e) {
            Log.e("Error : ", e.getMessage());
        }
        try {
            this.shopRecords = handleShopRecords(getValue(records, kRecords, JSONObject.class));
        } catch (Exception e) {
            Log.e("Error : ", e.getMessage());
        }
        try {
            this.shopRecord = handleShopRecords(getValue(records, kRecord, JSONObject.class));
        } catch (Exception e) {
            Log.e("Error : ", e.getMessage());
        }
        try {
            this.productRecords = handleProductRecords(getValue(records, kProductList, JSONObject.class));
        } catch (Exception e) {
            Log.e("Error : ", e.getMessage());
        }
        try {
            this.invoiceURL     = getValue(records, kInvoiceURL,     String.class);
        }catch (Exception e){
            Log.e("Error : ",e.getMessage());
        }

    }

    private CopyOnWriteArrayList<FinalProductsRecords> handleShopRecords(JSONArray jsonArray) throws JSONException {
        CopyOnWriteArrayList<FinalProductsRecords> orderHistoryList = new CopyOnWriteArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            orderHistoryList.add(new FinalProductsRecords(jsonArray.getJSONObject(i)));
        }
        return orderHistoryList;
    }

    private CopyOnWriteArrayList<ProductList> handleProductRecords(JSONArray jsonArray) throws JSONException {
        CopyOnWriteArrayList<ProductList> orderProductList = new CopyOnWriteArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            orderProductList.add(new ProductList(jsonArray.getJSONObject(i)));
        }
        return orderProductList;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public Integer getPaymentType() {
        return paymentType;
    }

    public String getExpectedDeliveryDate() {
        return expectedDeliveryDate;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public Boolean getValid() {

        return isValid;
    }

    public Address getDeliveryAddress() {
        return deliveryAddress;
    }

    public String getPromoCodeTagLine() {
        return promoCodeTagLine;
    }

    public String getTotalCartPriceBeforePromoCode() {
        return totalCartPriceBeforePromoCode;
    }

    public String getFinalAmountAfterPromoCode() {
        return finalAmountAfterPromoCode;
    }

    public String getDiscountedPrice() {
        return discountedPrice;
    }

    public Integer getCartItemCount() {
        return cartItemCount;
    }

    public Integer getProductCount() {
        return productCount;
    }

    public Integer getTotalItemCount() {
        return totalItemCount;
    }

    public String getInvoiceURL() {
        return invoiceURL;
    }

    public CopyOnWriteArrayList<ProductList> getProductRecords() {
        return productRecords;
    }

    public CopyOnWriteArrayList<FinalProductsRecords> getShopRecords() {
        return shopRecords;
    }

    public CopyOnWriteArrayList<FinalProductsRecords> getShopRecord() {
        return shopRecord;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }
}
