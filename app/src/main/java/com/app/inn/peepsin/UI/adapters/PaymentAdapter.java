package com.app.inn.peepsin.UI.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.app.inn.peepsin.Models.PaymentModel;
import com.app.inn.peepsin.R;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by root on 7/4/2017.
 */

public class PaymentAdapter extends RecyclerView.Adapter<PaymentAdapter.FeedInventoryViewHolder> {

    private List<PaymentModel> list;
    Context context;


    public PaymentAdapter(List<PaymentModel> list, Context context) {
        this.list = list;
        this.context = context;
    }


    @Override
    public FeedInventoryViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.payment_adapter, viewGroup, false);
        return new FeedInventoryViewHolder(v);

    }

    @Override
    public void onBindViewHolder(FeedInventoryViewHolder holder, int position) {

     //   String imageurl=list.get(position).getInventroryImageUrl();
        holder.tvItemName.setText(list.get(position).getName());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class FeedInventoryViewHolder extends RecyclerView.ViewHolder{



        @BindView(R.id.tv_workdetail)
        TextView tvItemName;

        FeedInventoryViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

    }
}
