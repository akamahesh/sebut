package com.app.inn.peepsin.UI.fragments.Permissions;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.app.inn.peepsin.Constants.Blocks.Block;
import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.DataBase.DataBaseHandler;
import com.app.inn.peepsin.Libraries.DispatchQueue.DispatchQueue;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.PhoneContact;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.helpers.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.Manifest.permission.READ_CONTACTS;
import static com.app.inn.peepsin.Constants.Constants.kMessageInternalInconsistency;


public class ContactPermissionFragment extends Fragment {

    private static final String TAG = ContactPermissionFragment.class.getSimpleName();
    @BindView(R.id.btn_grant_access)
    Button btnGrantAccess;
    private OnFragmentInteractionListener mListener;
    private final int REQUEST_PERMISSION_READ_CONTACTS = 102;
    private String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
    private String PROFILE_IMAGE = ContactsContract.Contacts.PHOTO_URI;
    private String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;
    private String mUserId;
    private ProgressDialog progressDialog;

    private DataBaseHandler dbHandler;

    public ContactPermissionFragment() {
        // Required empty public constructor
    }

    public static ContactPermissionFragment newInstance() {
        return new ContactPermissionFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dbHandler = new DataBaseHandler(getContext());
        //mUserId = String.valueOf(ModelManager.modelManager().getCurrentUser().getUserId());
        mUserId = String.valueOf(45);
        progressDialog = Utils.generateProgressDialog(getContext(),false);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contact_permission, container, false);
        ButterKnife.bind(this, view);
        if (ContextCompat.checkSelfPermission(getActivity(), READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            btnGrantAccess.setText(getString(R.string.permission_granted));
            btnGrantAccess.setEnabled(false);
        }
        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @OnClick(R.id.btn_grant_access)
    void onGrantAccess(Button button) {
        if (ContextCompat.checkSelfPermission(getActivity(), READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            mListener.onSkip(2);
        } else if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), READ_CONTACTS)) {
            // We've been denied once before. Explain why we need the permission, then ask again.
            Utils.showDialog(getContext(),
                    getString(R.string.contact_permission_required_text),
                    getString(R.string.ask_permission_text),
                    getString(R.string.discard_text),
                    (dialog, which) -> {
                        if (which == -1)
                            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_PERMISSION_READ_CONTACTS);
                        else
                            dialog.dismiss();
                    });
        } else {
            // We've never asked. Just do it.
            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_PERMISSION_READ_CONTACTS);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PERMISSION_READ_CONTACTS && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            loadData();
        } else {
            // We were not granted permission this time, so don't try to show the contact picker
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void loadData() {
        showProgress(true);
        getPhoneContacts((Constants.Status iStatus, GenricResponse<List<PhoneContact>> genricResponse) -> {
            showProgress(false);
            mListener.onSkip(2);
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
        });
    }

    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) progressDialog.dismiss();
        }
    }

    public void getPhoneContacts(Block.Success<List<PhoneContact>> success, Block.Failure failure) {
        ModelManager.modelManager().getDispatchQueue().async(() -> {
            try {
                Cursor cursor = getContext().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
                if (cursor != null) {
                    cursor.moveToFirst();
                }
                List<PhoneContact> contactStore = new ArrayList<>();
                while (!(cursor != null && cursor.isAfterLast())) {
                    String name = cursor != null ? cursor.getString(cursor.getColumnIndex(DISPLAY_NAME)) : null;
                    String phone = cursor != null ? cursor.getString(cursor.getColumnIndex(NUMBER)) : null;
                    String image = cursor != null ? cursor.getString(cursor.getColumnIndex(PROFILE_IMAGE)) : null;

                    PhoneContact phoneContact = new PhoneContact(name, phone, image);  // 1 for phone
                    contactStore.add(phoneContact);
                    if (!dbHandler.isPhoneUserExist(mUserId, phoneContact.getContactId())) {
                        Log.d(TAG,phoneContact.toString());
                        dbHandler.addPhoneContacts(mUserId, phoneContact.getName(), phoneContact.getContactId(), phoneContact.getImageURL(), "0");
                    }
                    if (cursor != null) {
                        cursor.moveToNext();
                    }
                }
                GenricResponse<List<PhoneContact>> genricResponse = new GenricResponse<>(contactStore);
                DispatchQueue.main(() -> success.iSuccess(Constants.Status.success, genricResponse));
            } catch (Exception e) {
                DispatchQueue.main(() -> failure.iFailure(Constants.Status.fail, kMessageInternalInconsistency));
            }
        });
    }


    public interface OnFragmentInteractionListener {
        void onSkip(int code);
    }
}
