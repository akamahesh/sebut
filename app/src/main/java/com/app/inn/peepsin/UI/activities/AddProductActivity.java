package com.app.inn.peepsin.UI.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;

import com.app.inn.peepsin.BuildConfig;
import com.app.inn.peepsin.Managers.BaseManager.ExceptionHandler;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.fragments.Profile.EditProfileFragment;
import com.app.inn.peepsin.UI.fragments.SellStuffModule.CameraPreviewFragment;
import com.app.inn.peepsin.UI.fragments.SellStuffModule.SellingProductAddFragment;
import com.app.inn.peepsin.UI.fragments.SellStuffModule.SetPriceFragment;
import com.app.inn.peepsin.UI.fragments.SellStuffModule.SetTitleFragment;
import com.app.inn.peepsin.UI.helpers.AkaImageUtil;
import com.app.inn.peepsin.UI.helpers.FragmentUtil;
import com.app.inn.peepsin.UI.helpers.Utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static com.app.inn.peepsin.Constants.Constants.kData;
import static com.app.inn.peepsin.Constants.Constants.kProductImageUrlList;
import static com.app.inn.peepsin.Constants.Constants.kType;

public class AddProductActivity extends AppCompatActivity implements
        CameraPreviewFragment.CameraPreviewInteractionListener,
        SetPriceFragment.FragmentInteractionListener,
        SetTitleFragment.FragmentInteractionListener,
        SellingProductAddFragment.SellingProductInteractionListener{

    private List<String> imagePathList;
    private int productPrice;

    public static Intent getIntent(Context context) {// for intermediate and home activity
        return new Intent(context, AddProductActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
        setContentView(R.layout.fragment_root);
        FragmentUtil.changeFragment(getSupportFragmentManager(), CameraPreviewFragment.newInstance(new ArrayList<>()), false, false);
    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
    }
    @Override
    protected void onResume() {
        super.onResume();
        if(Utils.isprofileClicked==1) {
            setBarlayotVisibility();
        }
    }
    @Override
    public void onUpload(List<String> pathList) {
        imagePathList = pathList;
        FragmentUtil.changeFragment(getSupportFragmentManager(), SetPriceFragment.newInstance(imagePathList.get(0)), false, true);
    }

    @Override
    public void priceNext(int price) {
        productPrice = price;
        FragmentUtil.changeFragment(getSupportFragmentManager(), SetTitleFragment.newInstance(imagePathList.get(0)), false, true);
    }

    @Override
    public void productNext(String productName) {
        FragmentUtil.changeFragment(getSupportFragmentManager(), SellingProductAddFragment.newInstance(imagePathList, productPrice, productName), false, true);
    }

    @Override
    public void submitAd(HashMap<String, Object> hashMap) {
        Intent intent = getIntent();
        intent.putExtra(kData, hashMap);
        intent.putExtra(kProductImageUrlList, (Serializable) imagePathList);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }


    @Override
    public void onBackPressed() {
        int count = getSupportFragmentManager().getBackStackEntryCount();
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }

    }
    public void setBarlayotVisibility(){
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.activity_home,null);
        AppBarLayout appBarLayout = (AppBarLayout) view.findViewById(R.id.appBarLayout);
        // LinearLayout bar = (LinearLayout) ll_ActivityA.findViewById(R.id.actionbarhome);
        appBarLayout.setVisibility(View.GONE);

    }

}
