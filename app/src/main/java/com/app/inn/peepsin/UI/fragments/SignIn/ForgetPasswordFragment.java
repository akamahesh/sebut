package com.app.inn.peepsin.UI.fragments.SignIn;


import static com.app.inn.peepsin.Constants.Constants.kOTP;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.activities.LogInActivity;
import com.app.inn.peepsin.UI.dialogFragments.PolicyDialogFragment;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.helpers.Validations;
import java.util.HashMap;

/**
 * Created by akamahesh on 2/5/17.
 */

public class ForgetPasswordFragment extends Fragment {

  @BindView(R.id.tv_title)
  TextView textViewTitle;
  @BindView(R.id.tv_terms)
  TextView textViewTerms;
  @BindView(R.id.edt_email)
  EditText editTextEmail;
  private ProgressDialog progressDialog;

  private static OnForgetPasswordFragmentInteractionListener mListener;

  public static Fragment newInstance() {
    return new ForgetPasswordFragment();
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    if (context instanceof OnForgetPasswordFragmentInteractionListener) {
      mListener = (OnForgetPasswordFragmentInteractionListener) context;
    } else {
      throw new RuntimeException(context.toString()
          + " must implement OnForgetPasswordFragmentInteractionListener");
    }
  }

  @Override
  public void onDetach() {
    super.onDetach();
    mListener = null;
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.activity_forget_password, container, false);
    ButterKnife.bind(this, view);

    ((LogInActivity) getActivity()).setTitle(getString(R.string.title_forget_password));
    textViewTitle.setText(getString(R.string.title_forget_password));
    progressDialog = Utils.generateProgressDialog(getContext(), false);
    editTextEmail.setOnEditorActionListener(onEditorActionListener);
    return view;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    textViewTitle.setText(getString(R.string.title_forget_password));
    clickableTermsSpan(textViewTerms);
  }

  @OnClick(R.id.btn_forget_pwd)
  public void forgetPassword() {
    if (!validate()) {
      return;
    }
    getOtp(Utils.getProperText(editTextEmail));
  }


  @OnClick(R.id.btn_back)
  public void onBack() {
    if (mListener != null) {
      mListener.onBack();
    }
  }

  @OnClick(R.id.tv_login)
  public void loginHere() {
    if (mListener != null) {
      mListener.moveToSignIn("");
    }
  }


  ClickableSpan termsSpanListener = new ClickableSpan() {
    @Override
    public void onClick(View widget) {
      showDialog(PolicyDialogFragment.newInstance(2), false);
    }

    @Override
    public void updateDrawState(TextPaint ds) {
      super.updateDrawState(ds);
      ds.setUnderlineText(false);
      ds.setColor(getResources().getColor(R.color.link_color));
    }
  };


  ClickableSpan privacySpanListener = new ClickableSpan() {
    @Override
    public void onClick(View widget) {
      showDialog(PolicyDialogFragment.newInstance(1), false);
    }

    @Override
    public void updateDrawState(TextPaint ds) {
      super.updateDrawState(ds);
      ds.setUnderlineText(false);
      ds.setColor(getResources().getColor(R.color.link_color));
    }
  };


  EditText.OnEditorActionListener onEditorActionListener = (v, actionId, event) -> {
    if (actionId == EditorInfo.IME_ACTION_GO) {
      Utils.hideKeyboard(getContext());
      forgetPassword();
      return true;
    }
    return false;
  };

  private void getOtp(String email) {
    showProgress(true);
    ModelManager.modelManager().getForgotPasswordOTP(email,
        (Constants.Status iStatus, GenricResponse<HashMap<String, Object>> genericResponse) -> {
          HashMap<String, Object> map = genericResponse.getObject();
          String OTP = (String) map.get(kOTP);
          if (mListener != null) {
            mListener.onForgetPassword(OTP, email);
          }
          showProgress(false);
        }, (Constants.Status iStatus, String message) -> {
          Utils.showAlertDialog(getContext(), "Alert", message);
          showProgress(false);
        });
  }

  /**
   * Click span for Terms and Conditions
   * String str= "By continuing you agree to the Privacy Policy and Terms of Service"
   *
   * @param mTextViewTerms textview Where clickable span will be applied
   */
  private void clickableTermsSpan(TextView mTextViewTerms) {
    SpannableStringBuilder ssb = new SpannableStringBuilder();
    String termsText = getString(R.string.terms_and_policies);
    ssb.append(termsText);
    int pStart = termsText.indexOf('P');
    int pEnd = pStart + 14;
    int tStart = termsText.indexOf('T');
    int tEnd = tStart + 16;

    ssb.setSpan(privacySpanListener, pStart, pEnd, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    ssb.setSpan(termsSpanListener, tStart, tEnd, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    mTextViewTerms.setText(ssb);
    mTextViewTerms.setMovementMethod(LinkMovementMethod.getInstance());
  }


  void showDialog(DialogFragment dialogFragment, boolean saveInBackstack) {
    String backStateName = dialogFragment.getClass().getName();
    FragmentManager fragmentManager = getChildFragmentManager();
    FragmentTransaction ft = fragmentManager.beginTransaction();

    if (saveInBackstack) {
      ft.addToBackStack(backStateName);
    } else {

    }
    dialogFragment.show(fragmentManager, backStateName);
  }

  private boolean validate() {
    boolean isOk = true;
    EditText etValid;
    String testString;
    View requestFocus = null;

    etValid = editTextEmail;
    testString = etValid.getText().toString().toLowerCase().trim();
    if (testString.isEmpty()) {
      etValid.setError(getString(R.string.error_cannot_be_empty));
      requestFocus = etValid;
      isOk = false;
    } else if (!Validations.isValidEmail(testString)) {
      etValid.setError(getString(R.string.error_invalid_email));
      requestFocus = etValid;
      isOk = false;
    }

    if (requestFocus != null) {
      requestFocus.requestFocus();
    }
    return isOk;

  }

  private void showProgress(boolean b) {
    if (b) {
      progressDialog.show();
    } else {
      if (progressDialog != null) {
        progressDialog.cancel();
      }
    }
  }


  public interface OnForgetPasswordFragmentInteractionListener {

    void onForgetPassword(String properText, String email);

    void moveToSignIn(String contactId);

    void onBack();
  }

}
