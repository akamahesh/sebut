package com.app.inn.peepsin.UI.dialogFragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.app.inn.peepsin.Models.SubCategory;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.helpers.DividerItemRecyclerDecoration;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnEditorAction;

import static com.app.inn.peepsin.Constants.Constants.KSCREENHEIGHT;
import static com.app.inn.peepsin.Constants.Constants.KSCREENWIDTH;
import static com.app.inn.peepsin.Constants.Constants.kProductSubCategory;

/**
 * Created by Harsh on 5/15/2017.
 */

public class ProductSubCategoryDialogFragment extends android.support.v4.app.DialogFragment{
    @BindView(R.id.edt_search)
    EditText edtSearch;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.recycler_view_history)
    RecyclerView recyclerView;

    private List<SubCategory> subCategoryList = new ArrayList<>();
    private SubCategoryAdapter subCategoryAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.MY_DIALOG);
        Bundle mArgs = getArguments();
        int pHeight = mArgs.getInt(KSCREENHEIGHT);
        int pWidth  = mArgs.getInt(KSCREENWIDTH);
        subCategoryList = (List<SubCategory>) mArgs.getSerializable(kProductSubCategory);
        Dialog d = getDialog();
        if (d!=null){
            d.getWindow().setLayout(pWidth-100, pHeight-100);
            d.getWindow().getAttributes().windowAnimations = R.style.MaterialDialogSheetAnimation;
            Drawable drawable = getResources().getDrawable(R.drawable.canvas_dialog_bg);
            d.getWindow().setBackgroundDrawable(drawable);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product_category_layout, container, false);
        ButterKnife.bind(this, view);
        tvTitle.setText(getString(R.string.subProductCategory));

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DividerItemRecyclerDecoration(getActivity(), R.drawable.canvas_recycler_divider));

        subCategoryAdapter = new SubCategoryAdapter(subCategoryList);
        recyclerView.setAdapter(subCategoryAdapter);

        addTextListener();
        return view;
    }

    @OnClick(R.id.btn_back)
    void onBack() {
        dismiss();
    }

    @OnEditorAction(R.id.edt_search)
    protected boolean onSearch(int actionId) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(edtSearch.getWindowToken(), 0);
            return true;
        }
        return false;
    }

    private void addTextListener() {
        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                s = s.toString().toLowerCase();


                final List<SubCategory> filteredList = new ArrayList<>();
                for (int i = 0; i < subCategoryList.size(); i++) {
                    final String text = subCategoryList.get(i).getName().toLowerCase().trim();
                    if (text.contains(s)) {
                        filteredList.add(subCategoryList.get(i));
                    }
                }
                subCategoryAdapter = new SubCategoryAdapter(filteredList);
                recyclerView.setAdapter(subCategoryAdapter);
                subCategoryAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable s){}
        });
    }


    class SubCategoryAdapter extends RecyclerView.Adapter<SubCategoryAdapter.SubCategoryHolder> {

        List<SubCategory> myItemList;

        SubCategoryAdapter(List<SubCategory> itemsList) {
            this.myItemList = itemsList;
        }

        @Override
        public SubCategoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(getContext()).inflate(R.layout.category_row_layout, parent, false);
            return new SubCategoryHolder(view);
        }

        @Override
        public void onBindViewHolder(SubCategoryHolder holder, int position) {
            SubCategory item = myItemList.get(position);
            holder.bindContent(item);
            String itemName = item.getName();
            holder.tvName.setText(itemName);
        }


        @Override
        public int getItemCount() {
            return myItemList.size();
        }

        class SubCategoryHolder extends RecyclerView.ViewHolder {

            private SubCategory item;
            @BindView(R.id.tv_item_name)
            TextView tvName;

            SubCategoryHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            void bindContent(SubCategory item) {
                this.item = item;
            }

            @OnClick(R.id.row_item)
            void itemSelected() {
                Intent in = getActivity().getIntent().putExtra("data",item.getName());
                in.putExtra("id",item.getId());
                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK,in);
                dismiss();
            }
        }
    }
}
