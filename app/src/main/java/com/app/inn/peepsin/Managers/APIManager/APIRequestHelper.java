package com.app.inn.peepsin.Managers.APIManager;

import com.google.gson.JsonObject;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface APIRequestHelper {
    //User will register through userRegistration API.
    String kUserSystemRegistration      = "userdetail/userSystemRegistration";

    //User can login with into app with SSO
    String kUserSystemLogin             = "userdetail/userSystemLogin";

    //As user login through social (F, G, T), userSocialLogin will call.
    String kUserSocialLogin             = "userdetail/userSocialLogin";

    //API will call to register a device for the user
    String kRegisterDevice              = "userdetail/registerDevice";

    //A PI will return the registered device details so that app can check the authentication of device.
    String kGetRegisteredDevice         = "userdetail/getRegisteredDevice";

    // API add the new address for the user. User can be multiple address.
    String kAddNewAddress               = "userdetail/addNewAddress";

    // API will use to edit the existing address.
    String kEditAddress                 = "userdetail/editAddress";

    // API will return an array of all addresses for that user.
    String kGetUserAddresses            = "userdetail/getAddress";

    // API will return an array of all addresses for that user
    String kDeleteAddress            = "userdetail/deleteAddress";

    // API will return the user’s basic details.
    String kGetMyProfileDetails         = "userdetail/getMyProfileDetails";

    // API will return the user’s basic details.
    String kEditMyProfileDetails        = "userdetail/editMyProfileDetails";

    // API will return the otp for contactId
    String kGetForgotPasswordOTP        = "userdetail/getForgotPasswordOTP";

    // API will resetPassword
    String kResetPassword               = "userdetail/resetPassword";

    // Api Will Return the privacy policy Description
    String kGetPrivacyPolicy            = "userdetail/getPrivacyPolicy";

    // Api Will Return the Terms and Policy Description
    String kGetTermsOfUse               = "userdetail/getTermsOfUse";

    // Api Will Return the Product Feeds
    String kGetProductFeeds             = "userdetail/getProductFeeds";

    // Api Will Return the Product Feeds Details
    String kGetProductDetails           = "userdetail/getProductDetails";

    // Api Will Return the Favourite Product Feeds
    String kGetFavProductFeeds          = "userdetail/getFavoriteProductList";

    // Api Will Return to set favourite and un-favourite product
    String kSetFavouriteProduct         = "userdetail/setFavoriteProduct";

    // Api Will Return to share Product Details with Connections
    String kShareProductConnection      = "userdetail/shareProduct";

    // Api Will Return to share Product List by Connections
    String kGetMySharedProduct          = "userdetail/getMySharedProduct";

    // Api Will Return to share Product List with Connections
    String kGetShareProductRecommended  = "userdetail/getShareProduct";

    // Api Will Return to get selling Product list
    String kGetSellingProducts          = "userdetail/getSellingProductList";

    // Api Will Return a product feed that is added in selling Product List
    String kAddSellingProduct           = "userdetail/addSellingProduct";

    // Api Will Return edited product feed that was added in selling Product List
    String kEditSellingProduct          = "userdetail/editSellingProduct";

    // Api Will Return the list of product category to the user
    String kGetProductCategory          = "userdetail/getProductCategoryList";

    // Api Will Return the list of product sub-category to the user
    String kGetProductSubCategory       = "userdetail/getProductSubcategoryList";

    // Api will return user's connection
    String kGetUserConnections          = "userdetail/getUserConnections";

    // Api will return user's connection
    String kGetUserProfileDetails          = "userdetail/getUserProfileDetails";

    // Api will return user's connection
    String kGetRequestReceived          = "userdetail/getRequestReceived";

    // Api will return user's connection
    String kAcceptConnectionRequest     = "userdetail/acceptConnectionRequest";

    // Api will return user's connection
    String kRejectConnectionRequest     = "userdetail/rejectConnectionRequest";

    // Api will return user's connection
    String kGetRequestSent              = "userdetail/getRequestSent";

    // Api will return user's connection
    String kCancelConnectionRequest = "userdetail/cancelConnectionRequest";

    // Api will return user's connection
    String kGetFriendSuggestions = "userdetail/getFriendSuggestions";

    // Api will return user's connection
    String kSendConnectionRequest = "userdetail/sendConnectionRequest";

    // Api will return user's connection
    String kRemoveConnection = "userdetail/removeConnection";

    // Api will return user's connection
    String kInviteContact = "userdetail/inviteContact";

    // API will use to send invitation or connection request to his/her mutual
    String kInviteFacebookMutualFriends = "userdetail/inviteFacebookMutualFriends";

    // API will use to send invitation or connection request to his/her mutual
    String kContactVerification = "userdetail/contactVerification";

    //Api for Account Deactivate
    String kDeactivateAccount = "userdetail/deactivateAccount";

    // API to get logged in user otp
    String kOTPVerification = "userdetail/otpVerification";

    // API to validate current password
    String kValidateCurrentPassword = "userdetail/validateCurrentPassword";

    //Api for Account Reactivate
    String kReactivateAccount = "userdetail/reactivateAccount";

    //API for get Privacy Settings
    String kGetMySettings = "userdetail/getMySettings";

    //API for edit privacy and settings
    String kEditMySettings = "userdetail/editMySettings";

    //API for Get Blocked User List
    String kGetBlockedUserList = "userdetail/getBlockedUserList";

    //API for Logout
    String kLogout = "userdetail/logout";

    //API for Block Unblock User
    String kBlockUnblockUser = "userdetail/blockUnblockUser";

    //API for Search Product,
    String kSearch ="userdetail/search";

    //Api to get my linked social accounts
    String kLinkSocialAccount = "userdetail/linkSocialAccount";

    String kGetSearchProduct="userdetail/getSearchProduct";

    //API will return boards of peeps In Board
    String kGetPeepsInBoardList = "userdetail/getPeepsInBoardList";

    //API will return boards of current user as well as the boards share with the user
    String kGetMyBoardList = "userdetail/getMyBoardList";

    //API will return the list of comments
    String kGetComments = "userdetail/getComments";

    // API will used to add comment to particular board
    String kSaveComments = "userdetail/saveComments";

    //API will used to like dislike particular board
    String kLikeDislikeBoard = "userdetail/likeDislikeBoard";

    //API will used to share the product with his friends(connections).
    String kShareBoard = "userdetail/shareBoard";

    //API will used when used add board and server will cerate a notification to all users about that board
    String kAddBoard = "userdetail/addBoard";

    // API will used delete board
    String kRemoveBoard = "userdetail/removeBoard";

    //API will used to add comment to particular board
    String kEditComments = "userdetail/editComments";

    //Api to remove comment from board
    String kRemoveComment = "userdetail/removeComment";

    //Api to get product rating and review
    String kGetProductRating = "userdetail/productRatingReview";

    //Api to get rating and review list
    String kGetRatingReviewList = "userdetail/getRatingReviewList";

    //Api to add and edit rating review
    String kAddEditReview = "userdetail/saveEditRatingReview";

    //Api to create a shop
    String kCreateShop = "userdetail/createShop";

    //Api to get the product type in shop
    String kGetProductTypeList = "userdetail/getProductTypeList";

    //Api to get the delivery type in shop
    String kGetDeliveryTime = "userdetail/getDeliveryTime";

    //Api to get the bank listing in shop
    String kGetBankList = "userdetail/getBankList";

    //Api to edit a shop
    String kEditShop = "userdetail/editShop";

    //Api to get shop list for the logged in user
    String kGetShopList = "userdetail/getShopList";

    //Api to get shop details for particular shopId
    String kGetShopDetails = "userdetail/getShopDetails";

    //Api to get my shop details for particular shopId
    String kGetMyShopDetails = "userdetail/getMyShopDetails";

    //Api to get shop category list for logged in user
    String kGetShopCategoryList = "userdetail/getShopCategoryList";

    //Api to get shop rating and review
    String kGetShopRatingReview = "userdetail/shopRatingReview";

    //Api to set the product status by the user in shop details
    String kSetProductStatus = "userdetail/setProductStatus";

    //Api to remove a product for particular productId
    String kDeleteProduct = "userdetail/deleteProduct";

    //Api to activate shop after verification by the logged in user
    String kActivateMyShop = "userdetail/activateShop";

    //Api to delete shop created by the logged in user
    String kDeleteMyShop = "userdetail/deleteMyShop";

    //Api to set Favorite/UnFavourite a shop
    String kSetFavouriteShop = "userdetail/setFavoriteUnfavoriteShop";

    //API will used to add the upload the media (image, audio, video) to AWS server.
    String kUploadMediaToServer = "userdetail/uploadMediaToServer";

    //Api to generate CheckSum
    String kGenerateCheckSum="userdetail/generateChecksum";

    //Api to get Favourite Shop List
    String kGetFavoriteShopList = "userdetail/getFavoriteShopList";

    //Api to get shopping cart details
    String kGetFinalBillSummary="userdetail/getFinalBillSummary";

    //Api to get Favourite Shop List
    String kGetShoppingCart = "userdetail/getShoppingCart";


    //Api to Online search friends
    String kOnlineSearchFriends = "userdetail/onlineSearchFriend";

    //Api to add product to shopping cart
    String kAddShoppingCart = "userdetail/addToShoppingCart";

    //Api to remove product from shopping cart
    String kRemoveShoppingCart = "userdetail/removeFromShoppingCart";

    //Api to get and validate promocode
    String kGetValidatePromoCode = "userdetail/validatePromoCode";

    //Api to cancel the placed order
    String kCancelPlacedOrder = "userdetail/cancelPlacedOrder";

    //Api to remove the placed order
    String kRemovePlacedOrder = "userdetail/removeClosedOrder";

    //Api to get order status for order placed
    String kGetOrderCurrentStatus = "userdetail/getOrderCurrentStatus";

    //Api to get order status list for dynamic saving
    String kGetOrderStatus = "userdetail/getOrderStatus";

    //Api to get order status for order placed
    String kGetOrderSummaryAfterPayment = "userdetail/getOrderSummaryAfterPayment";

    //Api to get order status for order placed
    String kGetOrderHistory= "userdetail/getOrderHistory";

    //Api to get order status for order placed
    String kGetRecievedOrder= "userdetail/getMyReceiveOrder";

    //Api to change the order status to received order
    String kChangeOrderStatus = "userdetail/changeOrderStatus";

    //Api to generate CheckSum
    String kGenerateHashCode="userdetail/generateHashCode";

    //Api to get Payment type
    String kSetPaymentType="userdetail/setPaymentType";

    //Api to check Contact Id Registration
    String kCheckRegistration="userdetail/checkContactIdRegistration";

    //Api to login user with otp
    String kLoginByOtp="userdetail/loginByOTP";

    //Api to check the user authentication
    String kAuthenticateUser="userdetail/authenticateUser";

    //Api to validate the user credentials for registration
    String kValidateUserCredentials="userdetail/validateUserCredentials";

    //Api to register the User to app
    String kRegisterUser="userdetail/registerUser";

    //Api to update the location of user for app
    String kGetUserLocation="userdetail/getUserLocation";

    //Api to update the location of user for app
    String kUpdateUserLocation="userdetail/updateUserLocation";

    //API will used to invite social users.
    String kInviteLink="userdetail/inviteLink";

    // Api Will Return the seller agreement Description
    String kGetSellerAgreement = "userdetail/sellerAgreement";

    // Api Will Return the seller agreement Description
    String kReceiveMessageHistory = "userdetail/receiveMessageHistory";

    // Api Will Return the seller agreement Description
    String kSendMessageToOpponent = "userdetail/sendMessageToOpponent";

    // Api Will used to manage product quantity in cart
    String kManageCartQuantity = "userdetail/manageCartQuantity";

    // Api Will used to cancel the order before payment
    String kCancelOrderBeforePayment = "userdetail/cancelOrderBeforePayment";

    //Api will used to the get cat and sub category list.
    String kGetCatSubCategoryList   ="userdetail/getCatSubCategoryList";

    //Api will used to edit the product inventory of user.
    String kEditProductInventory   ="userdetail/editProductInventory";

    //Api will used to the get cat and sub category list.
    String kGetTaxList   ="userdetail/getTaxList";

    //Api will used to validate the referral code for registered user.
    String kValidateReferralCode   ="userdetail/validateReferralCode";

    //Api to get shop list for the logged in user
    String kGetFeaturedShopList = "userdetail/getFeaturedShopList";

    //Api to get shop list for the logged in user
    String kGetShopProducts = "userdetail/getShopProducts";

    //Api to get shop list for the logged in user
    String kGetShopCategoryListAtSellerEnd = "userdetail/getShopCategoryListAtSellerEnd";

    //Api to add celebration event for friend by the logged in user
    String kAddCelebrationEvent = "userdetail/addCelebrationEvent";

    //Api to edit celebration event for friend by the logged in user
    String kEditCelebrationEvent = "userdetail/editCelebrationEvent";

    //Api to delete celebration event for friend by the logged in user
    String kDeleteCelebrationEvent = "userdetail/deleteCelebrationEvent";

    //Api to get celebration event list for friend by the logged in user
    String kGetCelebrationEventList = "userdetail/getCelebrationEventList";

    //Api to get celebration event details of friend by the logged in user
    String kGetCelebrationEventDetails = "userdetail/getCelebrationEventDetails";

    //Api to get celebration event images of friend by the logged in user
    String kgetCelebrationEventImages = "userdetail/getCelebrationEventDefaultImages";

    //Api to get app version regarding the updation of app
    String kGetLatestBuildInfo = "userdetail/getLatestBuildInfo";

    //Api to get message list for the celebrations by the logged in user
    String kGetCelebrationMessageList = "userdetail/getCelebrationMessageList";

    //Api to get message details for the celebration by the logged in user
    String kGetCelebrationMessageDetails = "userdetail/getCelebrationMessageDetails";

    //Api to delete the message celebration by the logged in user
    String kDeleteCelebrationMessage = "userdetail/deleteCelebrationMessage";

    //Api to set favorite a celebration Message
    String kSetFavoriteMessage = "userdetail/setFavoriteMessage";

    //Api to get generic authToken for the particular user
    String kGetGenericAuthToken = "userdetail/getGenericAuthToken";

    /**
     * set api request with api key and corresponding parameters
     * @param APIKey  key of the url
     * @param details details contains request body parameters
     * @param files   if include file will be sent in multipart
     * @return JsonObject ie. response
     */
    @Multipart
    @POST()
    Call<JsonObject> APIRequestWithFile(
            @Url String APIKey,
            @PartMap Map<String, RequestBody> details,
            @Part List<MultipartBody.Part> files
    );


    @POST()
    Call<JsonObject> APIRequestRaw(
            @Url String APIKey,
            @Body RequestBody params
    );

    @GET("maps/api/geocode/json")
    Call<JsonObject> getLocation(
            @Query("latlng") String latlng,
            @Query("key") String key
    );

    @GET("maps/api/place/autocomplete/json")
    Call<JsonObject> getPlaces(
            @Query("input") String input,
            @Query("key") String key
    );

    @GET("maps/api/place/details/json")
    Call<JsonObject> getPlacesByID(
            @Query("placeid") String placeID,
            @Query("key") String key
    );

    @GET("maps/api/geocode/json")
    Call<JsonObject> getCordinates(
            @Query("address") String address,
            @Query("key") String key
    );


}


