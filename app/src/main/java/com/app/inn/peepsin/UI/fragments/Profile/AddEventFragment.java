package com.app.inn.peepsin.UI.fragments.Profile;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Blocks.Block;
import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Libraries.DispatchQueue.DispatchQueue;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.CelebrationEvent;
import com.app.inn.peepsin.Models.CurrentUser;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.dialogFragments.TimeZoneListDialogFragment;
import com.app.inn.peepsin.UI.helpers.DatePicker;
import com.app.inn.peepsin.UI.helpers.TimePicker;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kAuthToken;
import static com.app.inn.peepsin.Constants.Constants.kEventId;
import static com.app.inn.peepsin.Constants.Constants.kEventMap;
import static com.app.inn.peepsin.Constants.Constants.kEventTitle;
import static com.app.inn.peepsin.Constants.Constants.kEventType;
import static com.app.inn.peepsin.Constants.Constants.kEventTypeName;
import static com.app.inn.peepsin.Constants.Constants.kName;
import static com.app.inn.peepsin.Constants.Constants.kSchedulingTimestamp;
import static com.app.inn.peepsin.Constants.Constants.kTimeZone;
import static com.app.inn.peepsin.Constants.Constants.kType;
import static com.app.inn.peepsin.Constants.Constants.kUserId;

/**
 * Created by dhruv on 26/9/17.
 */

public class AddEventFragment extends Fragment
        implements TimeZoneListDialogFragment.ItemClickListener{
    private final String TAG = getClass().getSimpleName() + ">>>";

    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.edt_event)
    EditText edtEvent;
    @BindView(R.id.edt_date)
    EditText edtDate;
    @BindView(R.id.edt_time)
    EditText edtTime;
    @BindView(R.id.tv_timezone)
    TextView tvTimeZone;
    @BindView(R.id.event_type_group)
    RadioGroup eventGroup;

    private static Switcher switcherListener;
    private ProgressDialog progressDialog;
    private CurrentUser user;
    private CelebrationEvent event;
    private int userId;
    private int type;
    private int eventType;
    private String eventTypeName;

    public static Fragment newInstance(int userId, CelebrationEvent event, int type, Switcher switcher) {
        switcherListener = switcher;
        AddEventFragment fragment = new AddEventFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(kEventMap,event);
        bundle.putInt(kUserId, userId);
        bundle.putInt(kType,type);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = Utils.generateProgressDialog(getContext(), false);
        user = ModelManager.modelManager().getCurrentUser();
        Bundle bdl = getArguments();
        if(bdl!=null){
            type = bdl.getInt(kType);
            userId = bdl.getInt(kUserId);
            event = (CelebrationEvent)bdl.getSerializable(kEventMap);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_event_details, container, false);
        ButterKnife.bind(this, view);
        edtDate.setKeyListener(null);
        edtTime.setKeyListener(null);
        if(Utils.isEventBack==1)
            onBack();
        if(type==1){
            tvTitle.setText(getString(R.string.event_create_title));
            Calendar cal = Calendar.getInstance();
            TimeZone tz = cal.getTimeZone();
            tvTimeZone.setText(displayTimeZone(tz));
        }else{
            tvTitle.setText(getString(R.string.event_edit));
            setEventInfo(event);
        }

        //Event Type RadioGroup
        eventGroup.setOnCheckedChangeListener((group, checkedId) -> {
            RadioButton rb = (RadioButton) group.findViewById(checkedId);
            eventTypeName = rb.getText().toString();
            if (eventTypeName.equals(getString(R.string.event_birthday)))
                eventType = 1;
            else if(eventTypeName.equals(getString(R.string.event_anniversary)))
                eventType = 2;
            else
                eventType = 3;
        });

        return view;
    }

    private void setEventInfo(CelebrationEvent event){
        edtEvent.setText(event.getEventTitle());
        edtDate.setText(Utils.getTimeStampDate(event.getSchedulingTimeStamp()));
        edtTime.setText(Utils.getTimeStampTime(event.getSchedulingTimeStamp()));
        tvTimeZone.setText(event.getTimeZone());
        eventTypeName = event.getEventTypeName();

        for(int i=0;i<eventGroup.getChildCount();i++){
            if(i==event.getEventType()-1)
                ((RadioButton)eventGroup.getChildAt(i)).setChecked(true);
        }
    }

    @OnClick(R.id.btn_back)
    void onBack() {
        getFragmentManager().popBackStack();
    }

    @OnClick(R.id.continue_btn)
    void onNext(){
        if (!validateData())
            return;
        HashMap<String, Object> hashMap = getParameters();
        if(switcherListener!=null){
            switcherListener.switchFragment(AddEventDetailsFragment.newInstance(hashMap,event),true,true);
        }
    }

    @OnClick(R.id.date_btn)
    void startDate(){
        Utils.hideKeyboard(getContext());
        showDatePicker();
    }

    private void showDatePicker() {
        DatePicker.getInstance(getContext(),2, date -> setDateTime(date,1)).showDatePickDialog();
    }

    @OnClick(R.id.time_btn)
    void startTime(){
        Utils.hideKeyboard(getContext());
        showTimePicker();
    }

    private void showTimePicker(){
        TimePicker.getInstance(getContext(), time -> setDateTime(time,2)).showTimePickDialog();
    }

    private void setDateTime(String value,int type){ // 1=date and 2=time
        if(type==1)
            edtDate.setText(value);
        else if(type==2)
            edtTime.setText(value);

        if(!edtDate.getText().toString().isEmpty()&&!edtTime.getText().toString().isEmpty()){
            String str_date = Utils.getProperText(edtDate)+" "+Utils.getProperText(edtTime);
            if(new Date().compareTo(Utils.scheduleDate(str_date))>=0){
                Toaster.kalaToast("Cannot set time to current or before");
                edtTime.setText("");
            }
        }
    }

    @Override
    public void onItemClicked(String name) {
        tvTimeZone.setText(name);
    }

    @OnClick(R.id.timezone_btn)
    void getTimeZone(){
        showProgress(true);
        getTimeZoneList((Constants.Status iStatus,GenricResponse<List<HashMap<String,Object>>> genericResponse) -> {
            TimeZoneListDialogFragment dialogFragment = TimeZoneListDialogFragment
                    .newInstance(genericResponse.getObject());
            dialogFragment.show(getChildFragmentManager(), "TAG");
            showProgress(false);
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Log.e(TAG,message);
        });
    }

    public void getTimeZoneList(Block.Success<List<HashMap<String,Object>>> success, Block.Failure failure) {
        ModelManager.modelManager().getDispatchQueue().async(() -> {
            try {
                List<HashMap<String,Object>> list = new ArrayList<>();
                String[] ids = TimeZone.getAvailableIDs();
                for (String id : ids) {
                    HashMap<String,Object> timezoneMap = new HashMap<>();
                    timezoneMap.put(kTimeZone,displayTimeZone(TimeZone.getTimeZone(id)));
                    timezoneMap.put(kName,TimeZone.getTimeZone(id).getID());
                    list.add(timezoneMap);
                }
                GenricResponse<List<HashMap<String,Object>>> genericList = new GenricResponse<>(list);
                DispatchQueue.main(() -> success.iSuccess(Constants.Status.success,genericList));
            } catch (Exception e) {
                DispatchQueue.main(() -> failure.iFailure(Constants.Status.fail, e.getMessage()));
            }
        });
    }

    private String displayTimeZone(TimeZone tz) {
        long hours = TimeUnit.MILLISECONDS.toHours(tz.getRawOffset());
        long minutes = TimeUnit.MILLISECONDS.toMinutes(tz.getRawOffset())
                - TimeUnit.HOURS.toMinutes(hours);
        minutes = Math.abs(minutes); // avoid -4:-30 issue
        String result;
        if (hours > 0) {
            result = String.format("(UTC+%d:%02d) %s", hours, minutes, tz.getID());
        } else {
            result = String.format("(UTC%d:%02d) %s", hours, minutes, tz.getID());
        }
        return result;
    }

    private boolean validateData() {
        boolean isOk = true;

        if (edtEvent.getText().toString().isEmpty()) {
            edtEvent.setError("Event Name is empty");
            edtEvent.requestFocus();
            isOk = false;
        }
        else if (edtTime.getText().toString().isEmpty()) {
            Toaster.kalaToast("Time cannot be empty");
            isOk = false;
        }
        else if (edtDate.getText().toString().isEmpty()) {
            Toaster.kalaToast("Date cannot be empty");
            isOk = false;
        }
        else if(eventGroup.getCheckedRadioButtonId()==-1){
            Toaster.kalaToast("Please select event type");
            isOk = false;
        }
        return isOk;
    }


    public HashMap<String, Object> getParameters() {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(kAuthToken, user.getAuthToken());
        parameters.put(kUserId,userId);
        parameters.put(kEventTitle,Utils.getProperText(edtEvent));
        parameters.put(kEventType,eventType);
        parameters.put(kEventTypeName,eventTypeName);
        parameters.put(kTimeZone,tvTimeZone.getText().toString());
        String str_date = Utils.getProperText(edtDate)+" "+Utils.getProperText(edtTime);
        parameters.put(kSchedulingTimestamp, Utils.scheduleTimeStamp(str_date));
        if(type==2)
            parameters.put(kEventId,event.getEventId());
        return parameters;
    }

    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        }
    }

}
