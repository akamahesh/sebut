package com.app.inn.peepsin.UI.fragments.Settings;

import android.app.ActivityManager;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.BaseModel;
import com.app.inn.peepsin.Models.CurrentUser;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.fragments.Profile.BlockedUserFragment;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import com.squareup.picasso.Picasso;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.content.Context.ACTIVITY_SERVICE;
import static com.app.inn.peepsin.Constants.Constants.kCurrentUser;
import static com.app.inn.peepsin.Constants.Constants.kEmailVisibility;
import static com.app.inn.peepsin.Constants.Constants.kOtherProductVisibility;
import static com.app.inn.peepsin.Constants.Constants.kPhoneVisibility;
import static com.app.inn.peepsin.Constants.Constants.kPrimaryLocationVisibility;
import static com.app.inn.peepsin.Constants.Constants.kProfileVisibility;
import static com.app.inn.peepsin.Constants.Constants.kSeeMyProducts;

/**
 * Created by Harsh on 5/9/2017.
 */

public class PrivacyAndSettingsFragment extends Fragment implements SeekBar.OnSeekBarChangeListener {

    @BindView(R.id.tv_title) TextView tvTitle;
    @BindView(R.id.iv_user_image) ImageView ivUserImage;
    @BindView(R.id.tv_name) TextView tvName;
    @BindView(R.id.v_shopSetting) View vShopSetting;
    @BindView(R.id.tv_deactive_account) TextView tvDeactiveAccount;
    @BindView(R.id.tv_aboutPrivacy) TextView tvPrivacy;
    @BindView(R.id.seek_bar_email) SeekBar seekBarEmail;
    @BindView(R.id.seek_bar_contact) SeekBar seekBarContact;
    @BindView(R.id.seek_bar_location) SeekBar seekBarLocation;
    @BindView(R.id.seek_bar_profile_visibility) SeekBar seekBarProfile;
    @BindView(R.id.seek_bar_me) SeekBar seekBarMyProduct;
    @BindView(R.id.seek_bar_product_level) SeekBar seekBarOtherProduct;

    int colorRed ;
    int colorYellow;
    int colorBlue;
    int colorGreen;

    private Dialog dialog=null;
    private CurrentUser currentUser;
    private ProgressDialog progressDialog;
    private HashMap<String, Object> privacyMap;
    private static Switcher profileSwitcherListener;

    //AppBarLayout appBarLayout;

    public static PrivacyAndSettingsFragment newInstance(Switcher switcher) {
        PrivacyAndSettingsFragment fragment = new PrivacyAndSettingsFragment();
        profileSwitcherListener=switcher;
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        privacyMap = new HashMap<>();
        currentUser = ModelManager.modelManager().getCurrentUser();
        progressDialog = Utils.generateProgressDialog(getContext(), true);
        colorRed = Color.parseColor("#F09D9A");
        colorYellow = Color.parseColor("#9B7F00");
        colorBlue = Color.parseColor("#00959C");
        colorGreen = Color.parseColor("#6BCBA7");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_privacy_and_settings, container, false);
        ButterKnife.bind(this, view);
       /* appBarLayout=((HomeActivity) getActivity()).getTabLayout();
        Utils.isprofileClicked=1;*/
        tvTitle.setText(getString(R.string.title_privacy_and_setting));
        if(currentUser.getShopId()>0){
            vShopSetting.setVisibility(View.VISIBLE);
        } else vShopSetting.setVisibility(View.GONE);

        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setUpSeekBar();
        setupProfile();
        loadData();
        changeAccountSetting();
    }

    private void setupProfile() {
        String name = currentUser.getFullName();
        tvName.setText(name);
        String privacy = name+ "'s" + " Privacy";
        tvPrivacy.setText(privacy);
        String imageURL = currentUser.getProfilePicURL();
        if (!imageURL.isEmpty()) {
            Picasso.with(getContext()).load(imageURL).resize(100,100).into(ivUserImage);
        }
    }

    public void loadData() {
        showProgress(true);
        ModelManager.modelManager().getPrivacySettings((Constants.Status iStatus, GenricResponse<HashMap<String, Object>> genricResponse) -> {
            showProgress(false);
            privacyMap = genricResponse.getObject();
            setProgress(privacyMap);
        }, (Constants.Status iStatus, String message) -> {
            Toaster.toast(message);
            showProgress(false);
        });
    }


    private void setUpSeekBar() {
        seekBarEmail.getProgressDrawable().setColorFilter(colorRed, PorterDuff.Mode.SRC_IN);
        seekBarEmail.setThumb(getResources().getDrawable(R.drawable.ic_marker_red));
        seekBarEmail.setOnSeekBarChangeListener(this);
        seekBarEmail.setTag(1);

        seekBarContact.getProgressDrawable().setColorFilter(colorRed, PorterDuff.Mode.SRC_IN);
        seekBarContact.setThumb(getResources().getDrawable(R.drawable.ic_marker_red));
        seekBarContact.setOnSeekBarChangeListener(this);
        seekBarContact.setTag(2);

        seekBarLocation.getProgressDrawable().setColorFilter(colorRed, PorterDuff.Mode.SRC_IN);
        seekBarLocation.setThumb(getResources().getDrawable(R.drawable.ic_marker_red));
        seekBarLocation.setOnSeekBarChangeListener(this);
        seekBarLocation.setTag(3);

        seekBarProfile.getProgressDrawable().setColorFilter(colorRed, PorterDuff.Mode.SRC_IN);
        seekBarProfile.setThumb(getResources().getDrawable(R.drawable.ic_marker_red));
        seekBarProfile.setOnSeekBarChangeListener(this);
        seekBarProfile.setTag(5);

        seekBarMyProduct.getProgressDrawable().setColorFilter(colorRed, PorterDuff.Mode.SRC_IN);
        seekBarMyProduct.setThumb(getResources().getDrawable(R.drawable.ic_marker_red));
        seekBarMyProduct.setOnSeekBarChangeListener(this);
        seekBarMyProduct.setTag(6);

        seekBarOtherProduct.getProgressDrawable().setColorFilter(colorRed, PorterDuff.Mode.SRC_IN);
        seekBarOtherProduct.setThumb(getResources().getDrawable(R.drawable.ic_marker_red));
        seekBarOtherProduct.setOnSeekBarChangeListener(this);
        seekBarOtherProduct.setTag(7);
    }

    public void setProgress(HashMap<String, Object> hashMap) {
        setSeekBar(seekBarEmail,       (int) hashMap.get(kEmailVisibility));
        setSeekBar(seekBarLocation,    (int) hashMap.get(kPrimaryLocationVisibility));
        setSeekBar(seekBarContact,     (int) hashMap.get(kPhoneVisibility));
        setSeekBar(seekBarProfile,     (int) hashMap.get(kProfileVisibility));
        setSeekBar(seekBarMyProduct,   (int) hashMap.get(kSeeMyProducts));
        setSeekBar(seekBarOtherProduct,(int) hashMap.get(kOtherProductVisibility));
    }

    private void setSeekBar(SeekBar seekBar, int status) {
        if(seekBar==seekBarMyProduct||seekBar==seekBarOtherProduct){
            if (status == 0) {
                seekBar.setProgress(0);
            } else if (status == 2) {
                seekBar.setProgress(15);
                seekBar.getProgressDrawable().setColorFilter(colorRed, PorterDuff.Mode.SRC_IN);
                seekBar.setThumb(getResources().getDrawable(R.drawable.ic_marker_red));
            } else if (status == 3) {
                seekBar.setProgress(50);
                seekBar.getProgressDrawable().setColorFilter(colorYellow ,PorterDuff.Mode.SRC_IN);
                seekBar.setThumb(getResources().getDrawable(R.drawable.ic_marker_yellow));
            } else if (status == 4) {
                seekBar.setProgress(90);
                seekBar.getProgressDrawable().setColorFilter(colorBlue, PorterDuff.Mode.SRC_IN);
                seekBar.setThumb(getResources().getDrawable(R.drawable.ic_marker_blue));
            }else if (status == 5) {
                seekBar.setProgress(130);
                seekBar.getProgressDrawable().setColorFilter(colorGreen, PorterDuff.Mode.SRC_IN);
                seekBar.setThumb(getResources().getDrawable(R.drawable.ic_marker_green));
            }
        }else {
            if (status == 0) {
                seekBar.setProgress(0);
            } else if (status == 1) {
                seekBar.setProgress(15);
                seekBar.getProgressDrawable().setColorFilter(colorRed, PorterDuff.Mode.SRC_IN);
                seekBar.setThumb(getResources().getDrawable(R.drawable.ic_marker_red));
            } else if (status == 2) {
                seekBar.setProgress(50);
                seekBar.getProgressDrawable().setColorFilter(colorYellow ,PorterDuff.Mode.SRC_IN);
                seekBar.setThumb(getResources().getDrawable(R.drawable.ic_marker_yellow));
            } else if (status == 3) {
                seekBar.setProgress(90);
                seekBar.getProgressDrawable().setColorFilter(colorBlue, PorterDuff.Mode.SRC_IN);
                seekBar.setThumb(getResources().getDrawable(R.drawable.ic_marker_blue));
            } else if (status == 4) {
                seekBar.setProgress(130);
                seekBar.getProgressDrawable().setColorFilter(colorGreen, PorterDuff.Mode.SRC_IN);
                seekBar.setThumb(getResources().getDrawable(R.drawable.ic_marker_green));
            }else if (status == 5) {
                seekBar.setProgress(130);
                seekBar.getProgressDrawable().setColorFilter(colorGreen, PorterDuff.Mode.SRC_IN);
                seekBar.setThumb(getResources().getDrawable(R.drawable.ic_marker_green));
            }
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        int progress = seekBar.getProgress();
        switch ((int) seekBar.getTag()) {
            case 1:
                privacyMap.put(kEmailVisibility, setLogicalSeekbar(seekBarEmail, progress));
                editPrivacy(privacyMap);
                break;
            case 2:
                privacyMap.put(kPhoneVisibility, setLogicalSeekbar(seekBarContact, progress));
                editPrivacy(privacyMap);
                break;
            case 3:
                privacyMap.put(kPrimaryLocationVisibility, setLogicalSeekbar(seekBarLocation, progress));
                editPrivacy(privacyMap);
                break;
            case 5:
                privacyMap.put(kProfileVisibility, setLogicalSeekbar(seekBarProfile, progress));
                editPrivacy(privacyMap);
                break;
            case 6:
                privacyMap.put(kSeeMyProducts, setLogicalSeekbar(seekBarMyProduct, progress));
                editPrivacy(privacyMap);
                break;
            case 7:
                privacyMap.put(kOtherProductVisibility, setLogicalSeekbar(seekBarOtherProduct, progress));
                editPrivacy(privacyMap);
                break;
            default:
                Toaster.toast("No default value");
        }
    }

    int setLogicalSeekbar(SeekBar seekBar, int value) {
        int pointer = 0;
        if(seekBar==seekBarMyProduct||seekBar == seekBarOtherProduct){
            if (value <= 30) {
                seekBar.setProgress(15);
                seekBar.getProgressDrawable().setColorFilter(colorRed, PorterDuff.Mode.SRC_IN);
                seekBar.setThumb(getResources().getDrawable(R.drawable.ic_marker_red));
                pointer = 2;
            } else if (value >= 30 && value <= 70) {
                seekBar.setProgress(50);
                seekBar.getProgressDrawable().setColorFilter(colorYellow, PorterDuff.Mode.SRC_IN);
                seekBar.setThumb(getResources().getDrawable(R.drawable.ic_marker_yellow));
                pointer = 3;
            } else if (value >= 70 && value <= 110) {
                seekBar.setProgress(90);
                seekBar.getProgressDrawable().setColorFilter(colorBlue, PorterDuff.Mode.SRC_IN);
                seekBar.setThumb(getResources().getDrawable(R.drawable.ic_marker_blue));
                pointer = 4;
            } else if (value >= 110) {
                seekBar.setProgress(130);
                seekBar.getProgressDrawable().setColorFilter(colorGreen, PorterDuff.Mode.SRC_IN);
                seekBar.setThumb(getResources().getDrawable(R.drawable.ic_marker_green));
                pointer = 5;
            }
        }else {
            if (value <= 30) {
                seekBar.setProgress(15);
                seekBar.getProgressDrawable().setColorFilter(colorRed, PorterDuff.Mode.SRC_IN);
                seekBar.setThumb(getResources().getDrawable(R.drawable.ic_marker_red));
                pointer = 1;
            } else if (value >= 30 && value <= 70) {
                seekBar.setProgress(50);
                seekBar.getProgressDrawable().setColorFilter(colorYellow, PorterDuff.Mode.SRC_IN);
                seekBar.setThumb(getResources().getDrawable(R.drawable.ic_marker_yellow));
                pointer = 2;
            } else if (value >= 70 && value <= 110) {
                seekBar.setProgress(90);
                seekBar.getProgressDrawable().setColorFilter(colorBlue, PorterDuff.Mode.SRC_IN);
                seekBar.setThumb(getResources().getDrawable(R.drawable.ic_marker_blue));
                pointer = 3;
            } else if (value >= 110) {
                seekBar.setProgress(130);
                seekBar.getProgressDrawable().setColorFilter(colorGreen, PorterDuff.Mode.SRC_IN);
                seekBar.setThumb(getResources().getDrawable(R.drawable.ic_marker_green));
                pointer = 5;
            }
        }

        return pointer;
    }


    private void clearContent() {
        if (Build.VERSION_CODES.KITKAT <= Build.VERSION.SDK_INT&&false) {
            ((ActivityManager)getContext().getSystemService(ACTIVITY_SERVICE))
                    .clearApplicationUserData(); // note: it has a return value!
        }else {
            SharedPreferences preferences = getActivity().getSharedPreferences(BaseModel.kAppPreferences, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.remove(kCurrentUser);
            editor.clear();
            editor.apply();
            //to make it sure that current user be null in shared prefs
            {
                ModelManager.modelManager().setCurrentUser(null);
            }

        }
    }

    public void editPrivacy(HashMap<String, Object> privacyMap) {
        ModelManager.modelManager().editPrivacySetting(privacyMap, (Constants.Status iStatus, GenricResponse<HashMap<String, Object>> genricResponse) -> {
            HashMap<String, Object> hashMap = genricResponse.getObject();
            try {setProgress(hashMap);
            }catch (IllegalStateException e){e.printStackTrace();}
        }, (Constants.Status iStatus, String message) -> {
            Toaster.toast(message);
            progressDialog.dismiss();
        });
    }

    @OnClick(R.id.view_blocked_users)
    void blockedUsers() {
        if (profileSwitcherListener != null)
            profileSwitcherListener.switchFragment(BlockedUserFragment.newInstance(), true, true);
    }

    @OnClick(R.id.view_Deactivate)
    void onDeactivate() {
        dialogActivateDeactivate();
    }


    @OnClick(R.id.btn_back)
    void onBack() {
        getFragmentManager().popBackStack();
       /* Utils.isprofileClicked=0;
        appBarLayout.setVisibility(View.VISIBLE);*/
    }
    @Override
    public void onResume() {
        super.onResume();
        //appBarLayout.setVisibility(View.GONE);
    }
    private void dialogActivateDeactivate() {
        if (dialog==null) {
            Boolean isAccountActivated = ModelManager.modelManager().getCurrentUser().getAccountActivated();
            dialog = new Dialog(getActivity());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.dailog_activate_deactivate);
            dialog.getWindow().setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM);
            TextView tv_head = (TextView) dialog.findViewById(R.id.tvMessage);
            Button ok_btn = (Button) dialog.findViewById(R.id.btn_ok);
            Button cancel_btn = (Button) dialog.findViewById(R.id.btn_cancel);
            if(isAccountActivated)
                tv_head.setText(getString(R.string.deactivate_description));
            else
                tv_head.setText(getString(R.string.activate_description));
            ok_btn.setOnClickListener(view -> {
                if(isAccountActivated){
                    deactivateAccount();
                }else {
                    reactivateAccount();
                }
                dialog.dismiss();
            });
            cancel_btn.setOnClickListener(v -> dialog.dismiss());
            dialog.show();
        }else {
            dialog=null;
            dialogActivateDeactivate();

        }
    }

    public void changeAccountSetting(){
        if(ModelManager.modelManager().getCurrentUser().getAccountActivated())
            tvDeactiveAccount.setText(getString(R.string.deactivate_account));
        else
            tvDeactiveAccount.setText(getString(R.string.reactivate_account));
    }


    public void deactivateAccount() {
        showProgress(true);
        ModelManager.modelManager().getDeactivateAccount((Constants.Status iStatus) -> {
            changeAccountSetting();
            Toaster.toastRangeen("Activate your account to get visible in PeepsIn");
            showProgress(false);
        }, (Constants.Status iStatus, String message) -> {
            Toaster.toast(message);
            showProgress(false);
        });
    }

    public void reactivateAccount() {
        showProgress(true);
        ModelManager.modelManager().getReactivateAccount((Constants.Status iStatus) -> {
            changeAccountSetting();
            Toaster.toastRangeen("Your account is now visible in PeepsIn");
            showProgress(false);
        }, (Constants.Status iStatus, String message) -> {
            Toaster.toast(message);
            showProgress(false);
        });
    }



    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) progressDialog.dismiss();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        profileSwitcherListener = null;
    }

}
