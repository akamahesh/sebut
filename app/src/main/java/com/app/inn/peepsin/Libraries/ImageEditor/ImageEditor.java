package com.app.inn.peepsin.Libraries.ImageEditor;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.Toast;
import com.app.inn.peepsin.Managers.BaseManager.DateManager;
import com.app.inn.peepsin.UI.fragments.Profile.EditProfileFragment;
import com.app.inn.peepsin.UI.helpers.Toaster;
import java.io.File;

/**
 * Created by akaMahesh on 9/1/17
 * contact : mckay1718@gmail.com
 */

public class ImageEditor {
    private static ImageEditor _ImageEditor;
    public static final int PIC_CROP = 151;
    public static final int PIC_CAMERA = 152;
    public static final int PIC_GALLARY = 153;

    public static ImageEditor getInstance(){
        if(_ImageEditor==null){
            _ImageEditor =new ImageEditor();
        }
        return _ImageEditor;
    }

    public File cropImage(Fragment fragment, Uri picUri, int code){
        String timeStamp = DateManager.getCurrentTimestamp().toString()+".png";
        File imageFile =new File(fragment.getContext().getExternalCacheDir(),timeStamp);
        Intent intent= cropImage(picUri, imageFile);
        if(intent!=null){
            fragment.startActivityForResult(intent, code);
        }else {
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Toaster.toast(errorMessage);
        }
        return imageFile;
    }




    private Intent cropImage( Uri picUri, File outPutFile){
        int aspectX         = 0;
        int aspectY         = 0;
        int spotlightX      = 100;
        int spotlighty      = 100;
        Intent cropIntent = null;
        try {
            cropIntent = new Intent("com.android.camera.action.CROP");
            cropIntent.setDataAndType(picUri, "image/*");
            // set crop properties
            cropIntent.putExtra("crop", "true");
            cropIntent.putExtra("aspectX",aspectX);
            cropIntent.putExtra("aspectY", aspectY);
            cropIntent.putExtra("scaleUpIfNeeded", true);
            cropIntent.putExtra("spotlightX", spotlightX);
            cropIntent.putExtra("spotlightY", spotlighty);
            cropIntent.putExtra("scale", true);

            //Create output file here
            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(outPutFile));
        }
        catch (Exception e) {
            e.printStackTrace();
            cropIntent = null;
        }
        return cropIntent;
    }




}
