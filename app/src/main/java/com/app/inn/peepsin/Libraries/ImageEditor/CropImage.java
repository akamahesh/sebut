package com.app.inn.peepsin.Libraries.ImageEditor;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;

public class CropImage {

    public static void getCroppedImage(Activity activity, Uri picUri,int code) {
        int aspectX         = 0;
        int aspectY         = 0;
        int spotlightX      = 100;
        int spotlighty      = 100;
        try {
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            cropIntent.setDataAndType(picUri, "image/*");
            // set crop properties
            cropIntent.putExtra("crop", "true");
            cropIntent.putExtra("aspectX",aspectX);
            cropIntent.putExtra("aspectY", aspectY);
            cropIntent.putExtra("scaleUpIfNeeded", true);
            cropIntent.putExtra("spotlightX", spotlightX);
            cropIntent.putExtra("spotlightY", spotlighty);
            cropIntent.putExtra("scale", true);

            // retrieve data on return
            cropIntent.putExtra("return-data", true);
            // start the activity - we handle returning in onActivityResult
            activity.startActivityForResult(cropIntent, code);
        }

        // respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException anfe) {
            // display an error message
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Toast.makeText(activity, errorMessage, Toast.LENGTH_SHORT).show();
        }
    }
}
