package com.app.inn.peepsin.Models;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by root on 18/5/17.
 */

public class SellingProduct  extends BaseModel implements Serializable {

    private Integer id;
    private Integer shopId;
    private String name;
    private String description;
    private String imageThumbUrl;
    private String imageUrl;
    private List<String> imageUrlList;
    private List<String> imageThumbUrlList;
    private Boolean isFavourite;
    private String postingDate;
    private Integer categoryId;
    private String productCategory;
    private Integer subcategoryId;
    private Integer rating;
    private String productSubCategory;
    private String price;
    private String sellingPrice;
    private Address address;
    private Integer favouriteCount;
    private Integer shareCount;
    private String discountTagLine;
    private Integer quantity;
    private Integer productStatus;
    private String SKUNumber;
    private String  specification;
    private String reasonOfFailure;
    private Integer productType;
    private CopyOnWriteArrayList<TAX> taxList;
    private String basePrice;
    private String netPrice;
    private String totalTax;
    private boolean isSelected = false;

    public SellingProduct(JSONObject jsonResponse) {
        this.id                 = getValue(jsonResponse, kProductId,                Integer.class);
        this.shopId             = getValue(jsonResponse, kShopId,                   Integer.class);
        this.name               = getValue(jsonResponse, kName,                     String.class);
        this.description        = getValue(jsonResponse, kProductDesc,              String.class);
        this.imageThumbUrl      = getValue(jsonResponse, kProductThumbUrl,          String.class);
        this.imageUrl           = getValue(jsonResponse, kProductImageUrl,          String.class);
        this.imageUrlList       = handleImageList(getValue(jsonResponse, kProductImageUrlList,      JSONArray.class));
        this.imageThumbUrlList  = handleImageList(getValue(jsonResponse, kProductThumbUrlList,      JSONArray.class));
        this.isFavourite        = getValue(jsonResponse, kIsFavorite,               Boolean.class);
        this.postingDate        = getValue(jsonResponse, kPostingDate,              String.class);
        this.productCategory    = getValue(jsonResponse, kProductCategory,          String.class);
        try{
            this.categoryId         = getValue(jsonResponse, kCategoryId ,          Integer.class);
        }catch (Exception e){
            e.printStackTrace();
        }
        try{
            this.subcategoryId      = getValue(jsonResponse, kProductSubCategoryId,     Integer.class);
        }catch (Exception e){
            e.printStackTrace();
        }
        try{
            this.productSubCategory = getValue(jsonResponse, kProductSubCategory,       String.class);
        }catch (Exception e){
            e.printStackTrace();
        }
        this.rating             = getValue(jsonResponse, kRating,                   Integer.class);
        this.price              = getValue(jsonResponse, kProductPrice,             String.class);
        this.sellingPrice       = getValue(jsonResponse, kProductSellPrice,         String.class);
        this.favouriteCount     = getValue(jsonResponse, kFavouriteCount,           Integer.class);
        this.shareCount         = getValue(jsonResponse, kShareCount,               Integer.class);
        this.discountTagLine    = getValue(jsonResponse, kDiscountTagLine,          String.class);
        this.quantity           = getValue(jsonResponse, kQuantity,                 Integer.class);
        try {
            this.productStatus      = getValue(jsonResponse, kProductStatus,            Integer.class);
        }catch (Exception e){
            e.printStackTrace();
            this.productStatus = 1;
        }
        this.SKUNumber          = getValue(jsonResponse, kSKUNumber,                String.class);
        if(jsonResponse.has(kSpecification)) {
            this.specification  = getValue(jsonResponse, kSpecification,            String.class);
        }

        try { //to handle @ClassCastException when addressId is emptyString, not to be removed
            this.address            = new Address(getValue(jsonResponse, kProductAddress,   JSONObject.class));
        }catch (Exception e){
            Log.e("Error : ",e.getMessage());
        }
        try {
            this.reasonOfFailure = getValue(jsonResponse, kReasonOfFailure,         String.class);
            this.productType     = getValue(jsonResponse, kProductType,             Integer.class);
        }catch (Exception e){
            e.printStackTrace();
        }
        try {
            this.taxList         = handleTaxList(getValue(jsonResponse, kTaxList,    JSONArray.class));
        }catch (Exception e){
            e.printStackTrace();
        }
        this.basePrice    = getValue(jsonResponse, kProductBasicPrice,    String.class);
        this.netPrice     = getValue(jsonResponse, kNetPrice,             String.class);
        this.totalTax     = getValue(jsonResponse, kTotalTax,             String.class);
    }

    public void setDiscountTagLine(String discountTagLine) {
        this.discountTagLine = discountTagLine;
    }


    public void setProductStatus(Integer productStatus) {
        this.productStatus = productStatus;
    }

    public void setSKUNumber(String SKUNumber) {
        this.SKUNumber = SKUNumber;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public Integer getSubcategoryId() {
        return subcategoryId;
    }

    public String getDiscountTagLine() {
        return discountTagLine;
    }

    public Integer getQuantity() {
        return quantity;
    }

    private List<String> handleImageList(JSONArray jsonArray) {
        List<String> imageList= new ArrayList<>();
        for(int i = 0; i<jsonArray.length();i++){
            try {
                imageList.add(jsonArray.get(i).toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return imageList;

    }

    private CopyOnWriteArrayList<TAX> handleTaxList(JSONArray jsonArray) throws JSONException {
        CopyOnWriteArrayList<TAX> taxList = new CopyOnWriteArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            taxList.add(new TAX(jsonArray.getJSONObject(i)));
        }
        return taxList;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getImageThumbUrl() {
        return imageThumbUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public List<String> getImageUrlList() {
        return imageUrlList;
    }

    public List<String> getImageThumbUrlList() {
        return imageThumbUrlList;
    }

    public Boolean isFavourite() {
        return isFavourite;
    }

    public void setFavourite(Boolean favourite) {
        isFavourite = favourite;
    }

    public String getPostingDate() {
        return postingDate;
    }

    public String getCategory() {
        return productCategory;
    }

    public String getSubcategory() {
        return productSubCategory;
    }

    public String getPrice() {
        return price;
    }

    public String getSellingPrice() {
        return sellingPrice;
    }

    public Address getAddress() {
        return address;
    }

    public Integer getFavouriteCount() {
        return favouriteCount;
    }

    public Integer getShareCount() {
        return shareCount;
    }

    public Integer getShopId() {
        return shopId;
    }

    public Integer getProductStatus() {
        return productStatus;
    }

    public String getSKUNumber() {
        return SKUNumber;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public String getReasonOfFailure() {
        return reasonOfFailure;
    }

    public Integer getProductType() {
        return productType;
    }

    public CopyOnWriteArrayList<TAX> getTaxList() {
        return taxList;
    }

    public String getBasePrice() {
        return basePrice;
    }

    public String getNetPrice() {
        return netPrice;
    }

    public String getTotalTax() {
        return totalTax;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        this.isSelected = selected;
    }
}
