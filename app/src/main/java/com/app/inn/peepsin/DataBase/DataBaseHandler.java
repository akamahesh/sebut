package com.app.inn.peepsin.DataBase;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.app.inn.peepsin.Models.CelebrationEvent;
import com.app.inn.peepsin.Models.FacebookContact;
import com.app.inn.peepsin.Models.GoogleContact;
import com.app.inn.peepsin.Models.MyConnections;
import com.app.inn.peepsin.Models.PhoneContact;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.measite.minidns.record.SRV;

import static com.app.inn.peepsin.Constants.Constants.kFacebookCommanIdKey;
import static com.app.inn.peepsin.Constants.Constants.kFacebookCommanNameKey;
import static com.app.inn.peepsin.Constants.Constants.kGoogleContactsEmail;
import static com.app.inn.peepsin.Constants.Constants.kGoogleContactsName;
import static com.app.inn.peepsin.Constants.Constants.kGoogleContactsPhoto;

public class DataBaseHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String TAG = DataBaseHandler.class.getSimpleName();

    // Database Name
    private static final String DATABASE_NAME = "UserInformationDB";
    // Contacts table name
    private static final String TABLE_PHONE_CONTACTS = "userPhoneContacts";
    private static final String TABLE_FB_CONTACTS = "userFBContacts";
    private static final String TABLE_GOOGLE_CONTACTS = "userGoogleContacts";
    private static final String TABLE_CONNECTIONS = "Connections";
    private static final String TABLE_EVENTS = "Events";
    // Contacts Table Columns names
    private static final String KEY_NAME = "name";
    private static final String KEY_ID = "id";
    private static final String KEY_IMAGE = "image";
    private static final String KEY_ISINVITED = "isinvited";
    private static final String KEY_USERID = "userid";
    private static final String KEY_SOCIAL_ID = "socialid";
    private static final String KEY_CONTACT_ID = "contactid";

    //Connections Table Columns names
    private static final String KEY_FIRST_NAME = "firstName";
    private static final String KEY_LAST_NAME = "lastName";
    private static final String KEY_PROFILEPIC_URL = "profilePicURL";
    private static final String KEY_SELLINGPRODUCT_COUNT = "sellingProductCount";
    private static final String KEY_FRIENDS_COUNT = "friendsCount";
    private static final String KEY_ISUSERBLOCKED = "isUserBlocked";
    private static final String KEY_CONNECTION_LEVEL = "connectionLevel";
    private static final String KEY_DATEOFBIRTH = "dateOfBirth";
    private static final String KEY_DATEJOINED = "dateJoined";
    private static final String KEY_PRIMARYADDRESS = "primaryAddress";
    private static final String KEY_SHOPNAME = "shopName";
    private static final String KEY_RATING = "rating";
    private static final String KEY_SHOPID = "shopId";
    private static final String KEY_LOGGEDIN_USER = "loggedInUserId";

    //Event Table Columns names
    private static final String KEY_USER_ID = "userId";
    private static final String KEY_EVENT_ID = "eventId";
    private static final String KEY_EVENT_TYPE = "eventType";
    private static final String KEY_EVENT_TYPE_NAME = "eventTypeName";
    private static final String KEY_EVENT_TITLE = "eventTitle";
    private static final String KEY_SCHEDULING_TIMESTAMP = "schedulingTimestamp";
    private static final String KEY_TIMEZONE = "timeZone";
    private static final String KEY_IMAGE_URL = "imageURL";
    private static final String KEY_MESSAGE = "message";
    private static final String KEY_NOTIFY_DURATION = "notifyMeDuration";
    private static final String KEY_REPETITION_INTERVAL = "repetitionInterval";


    public DataBaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_TABLE_PHONE_CONTACTS =
                "CREATE TABLE " + TABLE_PHONE_CONTACTS
                        + "("
                        + KEY_ID + " INTEGER PRIMARY KEY,"
                        + KEY_USERID + " TEXT,"
                        + KEY_NAME + " TEXT,"
                        + KEY_CONTACT_ID + " TEXT,"
                        + KEY_IMAGE + " TEXT,"
                        + KEY_ISINVITED + " TEXT "
                        + ")";

        String CREATE_TABLE_FB_CONTACTS =
                "CREATE TABLE " + TABLE_FB_CONTACTS
                        + "("
                        + KEY_ID + " INTEGER PRIMARY KEY,"
                        + KEY_USERID + " TEXT,"
                        + KEY_NAME + " TEXT,"
                        + KEY_SOCIAL_ID + " TEXT,"
                        + KEY_ISINVITED + " TEXT "
                        + ")";

        String CREATE_TABLE_GOOGLE_CONTACTS =
                "CREATE TABLE " + TABLE_GOOGLE_CONTACTS
                        + "("
                        + KEY_ID + " INTEGER PRIMARY KEY,"
                        + KEY_USERID + " TEXT,"
                        + KEY_NAME + " TEXT,"
                        + KEY_CONTACT_ID + " TEXT,"
                        + KEY_IMAGE + " TEXT,"
                        + KEY_ISINVITED + " TEXT "
                        + ")";


        String CREATE_TABLE_CONNECTIONS = "CREATE TABLE " + TABLE_CONNECTIONS
                + "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_LOGGEDIN_USER + " TEXT,"
                + KEY_USERID + " TEXT,"
                + KEY_FIRST_NAME + " TEXT,"
                + KEY_LAST_NAME + " TEXT,"
                + KEY_PROFILEPIC_URL + " TEXT,"
                + KEY_SELLINGPRODUCT_COUNT + " TEXT,"
                + KEY_FRIENDS_COUNT + " TEXT,"
                + KEY_ISUSERBLOCKED + " TEXT,"
                + KEY_CONNECTION_LEVEL + " TEXT,"
                + KEY_DATEOFBIRTH + " TEXT,"
                + KEY_DATEJOINED + " TEXT,"
                + KEY_PRIMARYADDRESS + " TEXT,"
                + KEY_SHOPNAME + " TEXT,"
                + KEY_RATING + " TEXT,"
                + KEY_SHOPID + " TEXT "
                + ")";

        String CREATE_TABLE_EVENTS = "CREATE TABLE " + TABLE_EVENTS
                + "("
                + KEY_USER_ID + " INTEGER,"
                + KEY_EVENT_ID + " INTEGER PRIMARY KEY,"
                + KEY_EVENT_TYPE + " TEXT,"
                + KEY_EVENT_TYPE_NAME + " TEXT,"
                + KEY_EVENT_TITLE + " TEXT,"
                + KEY_SCHEDULING_TIMESTAMP + " TEXT,"
                + KEY_TIMEZONE + " TEXT,"
                + KEY_IMAGE_URL + " TEXT,"
                + KEY_MESSAGE + " TEXT,"
                + KEY_NOTIFY_DURATION + " TEXT,"
                + KEY_REPETITION_INTERVAL + " TEXT "
                + ")";


        db.execSQL(CREATE_TABLE_PHONE_CONTACTS);
        db.execSQL(CREATE_TABLE_FB_CONTACTS);
        db.execSQL(CREATE_TABLE_GOOGLE_CONTACTS);
        db.execSQL(CREATE_TABLE_CONNECTIONS);
        db.execSQL(CREATE_TABLE_EVENTS);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PHONE_CONTACTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FB_CONTACTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_GOOGLE_CONTACTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONNECTIONS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_EVENTS);

        // Create tables again
        onCreate(db);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
    }

    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */

    //Add the Values in events in table
    public void addEvents(CelebrationEvent event)throws SQLException  {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_USER_ID, event.getUserId());
        values.put(KEY_EVENT_ID, event.getEventId());
        values.put(KEY_EVENT_TYPE, String.valueOf(event.getEventType()));
        values.put(KEY_EVENT_TYPE_NAME, event.getEventTypeName());
        values.put(KEY_EVENT_TITLE, event.getEventTitle());
        values.put(KEY_SCHEDULING_TIMESTAMP, String.valueOf(event.getSchedulingTimeStamp()));
        values.put(KEY_TIMEZONE, event.getTimeZone());
        values.put(KEY_IMAGE_URL, event.getImageUrl());
        values.put(KEY_MESSAGE, event.getMessage());
        values.put(KEY_NOTIFY_DURATION, String.valueOf(event.getNotifyMeDuration()));
        values.put(KEY_REPETITION_INTERVAL,String.valueOf( event.getRepetitionInterval()));
        db.insert(TABLE_EVENTS, null, values);
        db.close(); // Closing database connection
    }


    //Add the Values in connections in table
    public void addConnections(String loggedInUserId,String userId, String firstName, String lastName, String profilePicURL, String sellingProductCount,
    String friendsCount,String isUserBlocked,String connectionLevel,
    String dateOfBirth,String dateJoined,String primaryAddress,String shopName,String rating,String shopId)throws SQLException  {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_LOGGEDIN_USER, loggedInUserId);
        values.put(KEY_USERID, userId);
        values.put(KEY_FIRST_NAME, firstName);
        values.put(KEY_LAST_NAME, lastName);
        values.put(KEY_PROFILEPIC_URL, profilePicURL);
        values.put(KEY_SELLINGPRODUCT_COUNT, sellingProductCount);
        values.put(KEY_FRIENDS_COUNT, friendsCount);
        values.put(KEY_ISUSERBLOCKED, isUserBlocked);
        values.put(KEY_CONNECTION_LEVEL, connectionLevel);
        values.put(KEY_DATEOFBIRTH, dateOfBirth);
        values.put(KEY_DATEJOINED, dateJoined);
        values.put(KEY_PRIMARYADDRESS, primaryAddress);
        values.put(KEY_SHOPNAME, shopName);
        values.put(KEY_RATING, rating);
        values.put(KEY_SHOPID, shopId);
        db.insert(TABLE_CONNECTIONS, null, values);
        db.close(); // Closing database connection
    }


    //Add the Values in Phone contacts in table
    public void addPhoneContacts(String userId, String name, String contactId, String imageURL, String isInvited)throws SQLException  {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_USERID, userId);
        values.put(KEY_NAME, name);
        values.put(KEY_CONTACT_ID, contactId);
        values.put(KEY_IMAGE, imageURL);
        values.put(KEY_ISINVITED, isInvited);
        db.insert(TABLE_PHONE_CONTACTS, null, values);
        db.close(); // Closing database connection
    }

    //Add the values of facebook contacts in table
    public void addFacebookContacts(String userId, String name, String socialID, String isInvited)throws SQLException {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_USERID, userId);
        values.put(KEY_NAME, name);
        values.put(KEY_SOCIAL_ID, socialID);
        values.put(KEY_ISINVITED, isInvited);
        db.insert(TABLE_FB_CONTACTS, null, values);
        db.close(); // Closing database connection
    }

    //Add the values of google contacts in table
    public void addGoogleContacts(String userId, String name, String contactId, String imageUrl, String isInvited)throws SQLException  {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_USERID, userId);
        values.put(KEY_NAME, name);
        values.put(KEY_CONTACT_ID, contactId);
        values.put(KEY_IMAGE, imageUrl);
        values.put(KEY_ISINVITED, isInvited);
        db.insert(TABLE_GOOGLE_CONTACTS, null, values);
        db.close(); // Closing database connection
    }



    // method to update the table value on the basis of key values
    public void updatePhoneContacts(String userId, String contactId, String invited) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ISINVITED, invited);
        if (!contactId.isEmpty())
            db.update(TABLE_PHONE_CONTACTS, values, "userId =" + userId + " AND  contactid"+" = '" + contactId+"'", null);
        db.close(); // Closing database connection
    }

    // method to update the table value on the basis of key values
    public void updateFacebookContacts(String userId, String socialID, String invited) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ISINVITED, invited);
        if (!socialID.isEmpty()) {
            db.update(TABLE_FB_CONTACTS, values, "userId =" + userId + " AND  socialid = '" + socialID+"'", null);
        }
        db.close(); // Closing database connection
    }

    // method to update the row of table
    public void updateGoogleContacts(String userId, String contactId, String isinvited) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ISINVITED, isinvited);

        if (!contactId.isEmpty()) {
            db.update(TABLE_GOOGLE_CONTACTS, values, "userId =" + userId + " AND  contactid"+" = '" + contactId+"'", null);
        }
        db.close(); // Closing database connection
    }

    //method to update the row of the table
    public void updateConnections(String userId,String loggedInUser,String isBlocked) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ISUSERBLOCKED, isBlocked);

        db.update(TABLE_CONNECTIONS, values, "userId =" + userId + " AND  loggedInUserId"+" = '" + loggedInUser+"'", null);

        db.close(); // Closing database connection
    }

    //method to update the row of the event table
    public void updateEvents(CelebrationEvent event)throws SQLException  {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_USER_ID, event.getUserId());
        values.put(KEY_EVENT_ID, event.getEventId());
        values.put(KEY_EVENT_TYPE, String.valueOf(event.getEventType()));
        values.put(KEY_EVENT_TYPE_NAME, event.getEventTypeName());
        values.put(KEY_EVENT_TITLE, event.getEventTitle());
        values.put(KEY_SCHEDULING_TIMESTAMP, String.valueOf(event.getSchedulingTimeStamp()));
        values.put(KEY_TIMEZONE, event.getTimeZone());
        values.put(KEY_IMAGE_URL, event.getImageUrl());
        values.put(KEY_MESSAGE, event.getMessage());
        values.put(KEY_NOTIFY_DURATION, String.valueOf(event.getNotifyMeDuration()));
        values.put(KEY_REPETITION_INTERVAL,String.valueOf( event.getRepetitionInterval()));
        db.update(TABLE_CONNECTIONS, values, "eventId =" + event.getEventId()+"'", null);
        db.close(); // Closing database connection
    }


    //method to check the user exists on the basis of phone number
    @SuppressLint("Recycle")
    public boolean isEventExist(String userId, String eventId) {

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_EVENTS, new String[]{KEY_USER_ID, KEY_EVENT_ID},
                KEY_USER_ID + "=? AND " + KEY_EVENT_ID + "=?",
                new String[]{userId, eventId},
                null, null, null, null);
        //row exists
        return cursor != null && cursor.moveToFirst();
    }

    //method to check the user exists on the basis of phone number
    @SuppressLint("Recycle")
    public boolean isPhoneUserExist(String userId, String contactID) {

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_PHONE_CONTACTS, new String[]{KEY_USERID, KEY_CONTACT_ID},
                KEY_USERID + "=? AND " + KEY_CONTACT_ID + "=?",
                new String[]{userId, contactID},
                null, null, null, null);
        //row exists
        return cursor != null && cursor.moveToFirst();
    }

    //method to check the user exists on the basis of social id(facebook)
    @SuppressLint("Recycle")
    public boolean isFbUserExist(String userId, String socialId) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_FB_CONTACTS, new String[]{KEY_USERID, KEY_SOCIAL_ID},
                KEY_USERID + "=? AND " + KEY_SOCIAL_ID + "=?",
                new String[]{userId, socialId},
                null, null, null, null);

        //row exists
        return cursor != null && cursor.moveToFirst();
    }

    //method to check the user exists on the basis of social id(Google)
    @SuppressLint("Recycle")
    public boolean isGoogleUserExist(String userId, String contactId) {
        Cursor cursor = null;
        SQLiteDatabase db = this.getReadableDatabase();
        if (!contactId.isEmpty()) {
            cursor = db.query(TABLE_GOOGLE_CONTACTS, new String[]{KEY_USERID, KEY_CONTACT_ID},
                    KEY_USERID + "=? AND " + KEY_CONTACT_ID + "=?",
                    new String[]{userId, contactId},
                    null, null, null, null);
        }
        //row exists
        return cursor != null && cursor.moveToFirst();
    }
    //method to check the user is already user's friends
    @SuppressLint("Recycle")
    public boolean isConnectionUserExist(String loggedInuserId, String userId) {
        Cursor cursor = null;
        SQLiteDatabase db = this.getReadableDatabase();
        if (!userId.isEmpty()) {
            cursor = db.query(TABLE_CONNECTIONS, new String[]{KEY_USERID, KEY_LOGGEDIN_USER},
                    KEY_USERID + "=? AND " + KEY_LOGGEDIN_USER + "=?",
                    new String[]{userId, loggedInuserId},
                    null, null, null, null);
        }
        //row exists
        return cursor != null && cursor.moveToFirst();
    }

    public void deleteFBContacts(String userId, String contactId, String email) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "DELETE FROM " + TABLE_FB_CONTACTS + " WHERE " + KEY_USERID + " = '" + userId + "'";
        db.execSQL(query);
        db.close();

    }
    public void deleteMyConnection(String userId, String loggedInUser) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_CONNECTIONS + " WHERE " + KEY_LOGGEDIN_USER + " = "+loggedInUser+" AND " +KEY_USERID+ " = "+userId);
        db.close();
    }

    public void deleteEvent(int userId, int eventId) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_EVENTS + " WHERE " + KEY_USER_ID + " = "+userId+" AND " +KEY_EVENT_ID+ " = "+eventId);
        db.close();
    }

    //method to get Google contacts from database.
    @SuppressLint("Recycle")
    public List<GoogleContact> getGoogleContactFromDatabase(String userId, String isinvited) {
        Cursor c;
        List<GoogleContact> googleContactList = new ArrayList<>();
        try {
            SQLiteDatabase newDB = getReadableDatabase();
            c = newDB.rawQuery("Select id,userid,name,contactid,image,isinvited From " + DataBaseHandler.TABLE_GOOGLE_CONTACTS + " where userId = " + userId + " AND  isinvited = " + isinvited, null);
            if (c != null) {
                System.out.println("Values::::" + c.getCount());
                if (c.moveToFirst()) {
                    do {
                        String name = c.getString(c.getColumnIndex(KEY_NAME));
                        String contactId = c.getString(c.getColumnIndex(KEY_CONTACT_ID));
                        String imageURL = c.getString(c.getColumnIndex(KEY_IMAGE));

                        HashMap<String, Object> googleContactMap = new HashMap<>();
                        googleContactMap.put(kGoogleContactsName, name);
                        googleContactMap.put(kGoogleContactsEmail, contactId);
                        googleContactMap.put(kGoogleContactsPhoto, imageURL);

                        googleContactList.add(new GoogleContact(googleContactMap));
                    }
                    while (c.moveToNext());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return googleContactList;
    }


    //method to get Facebook contacts from database.
    @SuppressLint("Recycle")
    public List<FacebookContact> getFacebookContactFromDatabase(String userId, String isinvited) {
        Cursor c;
        List<FacebookContact> facebookContactList = new ArrayList<>();
        try {
            SQLiteDatabase newDB = getReadableDatabase();
            c = newDB.rawQuery("Select id,userid,name,socialid,isinvited From " + DataBaseHandler.TABLE_FB_CONTACTS + " where userId = " + userId + " AND  isinvited = " + isinvited, null);
            if (c != null) {
                System.out.println("Values::::" + c.getCount());
                if (c.moveToFirst()) {
                    do {
                        String name = c.getString(c.getColumnIndex(KEY_NAME));
                        String socialID = c.getString(c.getColumnIndex(KEY_SOCIAL_ID));

                        HashMap<String, Object> facebookContactMap = new HashMap<>();
                        facebookContactMap.put(kFacebookCommanNameKey, name);
                        facebookContactMap.put(kFacebookCommanIdKey, socialID);

                        facebookContactList.add(new FacebookContact(facebookContactMap));
                    }
                    while (c.moveToNext());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return facebookContactList;
    }

    //method to get Phone contacts from database.
    @SuppressLint("Recycle")
    public List<PhoneContact> getPhoneContactFromDatabase(String userId, String isinvited) {
        Cursor c;
        List<PhoneContact> phoneContactList = new ArrayList<>();
        try {
            SQLiteDatabase newDB = getReadableDatabase();
            c = newDB.rawQuery("Select id,userid,name,contactid,image,isinvited From " + DataBaseHandler.TABLE_PHONE_CONTACTS + " where userId = " + userId + " AND  isinvited = " + isinvited, null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        String name = c.getString(c.getColumnIndex(KEY_NAME));
                        String contactId = c.getString(c.getColumnIndex(KEY_CONTACT_ID));
                        String imageURL = c.getString(c.getColumnIndex(KEY_IMAGE));
                        phoneContactList.add(new PhoneContact(name,contactId,imageURL));
                    }
                    while (c.moveToNext());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return phoneContactList;
    }

    //method to get user's celebration events from database.
    @SuppressLint("Recycle")
    public List<CelebrationEvent> getEventsFromDatabase() {
        Cursor c;
        List<CelebrationEvent> events = new ArrayList<>();
        try {
            SQLiteDatabase newDB = getReadableDatabase();
            String selectQuery = "SELECT * FROM " + DataBaseHandler.TABLE_EVENTS;
            c = newDB.rawQuery(selectQuery, null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        int userId  = c.getInt(c.getColumnIndex(KEY_USER_ID));
                        int eventId = c.getInt(c.getColumnIndex(KEY_EVENT_ID));
                        String eventType = c.getString(c.getColumnIndex(KEY_EVENT_TYPE));
                        String eventTypeName = c.getString(c.getColumnIndex(KEY_EVENT_TYPE_NAME));
                        String eventTitle = c.getString(c.getColumnIndex(KEY_EVENT_TITLE));
                        String timeStamp = c.getString(c.getColumnIndex(KEY_SCHEDULING_TIMESTAMP));
                        String timeZone = c.getString(c.getColumnIndex(KEY_TIMEZONE));
                        String image = c.getString(c.getColumnIndex(KEY_IMAGE_URL));
                        String message = c.getString(c.getColumnIndex(KEY_MESSAGE));
                        String notifyDuration = c.getString(c.getColumnIndex(KEY_NOTIFY_DURATION));
                        String repetitionInterval = c.getString(c.getColumnIndex(KEY_REPETITION_INTERVAL));
                        events.add(new CelebrationEvent(userId,eventId,Integer.valueOf(eventType),eventTypeName,eventTitle,
                                Long.parseLong(timeStamp),timeZone,image,message,Integer.valueOf(notifyDuration),Integer.valueOf(repetitionInterval)));
                    }
                    while (c.moveToNext());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return events;
    }

    //method to get user's connections contacts from database.
    @SuppressLint("Recycle")
    public List<MyConnections> getConnectionsFromDatabase(String userId,String blockeduser) {
        Cursor c;
        List<MyConnections> myConnections = new ArrayList<>();
        try {
            SQLiteDatabase newDB = getReadableDatabase();
            c = newDB.rawQuery("Select id,userid,firstName,lastName,profilePicURL,isUserBlocked,rating,connectionLevel,shopName,primaryAddress From " + DataBaseHandler.TABLE_CONNECTIONS + " where loggedInUserId = " + userId+ " AND "+KEY_ISUSERBLOCKED+" = " +blockeduser, null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        String userid = c.getString(c.getColumnIndex(KEY_USERID));
                        String fname = c.getString(c.getColumnIndex(KEY_FIRST_NAME));
                        String lastname = c.getString(c.getColumnIndex(KEY_LAST_NAME));
                        String imageURL = c.getString(c.getColumnIndex(KEY_PROFILEPIC_URL));
                        String isBlocked = c.getString(c.getColumnIndex(KEY_ISUSERBLOCKED));
                        String rating = c.getString(c.getColumnIndex(KEY_RATING));
                        String connectionLevel = c.getString(c.getColumnIndex(KEY_CONNECTION_LEVEL));
                        String shopname = c.getString(c.getColumnIndex(KEY_SHOPNAME));
                        String address = c.getString(c.getColumnIndex(KEY_PRIMARYADDRESS));
                        myConnections.add(new MyConnections(Integer.parseInt(userid),fname,lastname,imageURL,isBlocked,Integer.parseInt(rating), Integer.parseInt(connectionLevel),shopname,address));
                    }
                    while (c.moveToNext());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return myConnections;
    }
}
