package com.app.inn.peepsin.Models;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by root on 4/7/17.
 */

public class ShopCategory extends BaseModel implements Serializable {

    private Integer id;
    private String name;
    private String imageUrl;
    private boolean isSelected=false;

    public ShopCategory(JSONObject jsonResponse) {
        this.id = getValue(jsonResponse, kCategoryId, Integer.class);
        this.name = getValue(jsonResponse, kCategory, String.class);
        this.imageUrl = getValue(jsonResponse, kCategoryImageURL, String.class);
    }

    public Integer getId() {
        return id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getName() {
        return name;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
