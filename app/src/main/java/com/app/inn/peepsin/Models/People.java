package com.app.inn.peepsin.Models;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by akamahesh on 15/5/17.
 */

public class People extends BaseModel implements Serializable {
    final String TAG = getClass().getSimpleName();
    private Integer userId;
    private String firstName;
    private String lastName;
    private String dateOfBirth;
    private String joinedDate;
    private String profilePicURL;
    private String shopName;
    private Integer rating;
    private Integer sellingProductCount;
    private Integer friendsCount;
    private List<SocialLink> userSocialLinks;
    private Address primaryAddress;
    private Integer connectionLevel;
    private String suggestionTag;
    private Integer userConnectionStatus;
    private boolean isSelected = false;


    public People(JSONObject jsonResponse) {
        this.userId                 = getValue(jsonResponse, kUserId,               Integer.class);
        this.firstName              = getValue(jsonResponse, kFirstName,            String.class);
        this.lastName               = getValue(jsonResponse, kLastName,             String.class);
        this.profilePicURL          = getValue(jsonResponse, kProfilePicUrl,        String.class);
        this.suggestionTag          = getValue(jsonResponse, kSuggestionTag,        String.class);
        this.shopName               = getValue(jsonResponse, kShopName,             String.class);
        this.rating                 = getValue(jsonResponse, kRating,               Integer.class);
        this.userConnectionStatus   = getValue(jsonResponse, kUserConnectionStatus, Integer.class);
        this.dateOfBirth            = getValue(jsonResponse, kDateOfBirth,          String.class);
        this.joinedDate             = getValue(jsonResponse, kJoinedDate,           String.class);
        this.sellingProductCount    = getValue(jsonResponse, kSellingProductCount,  Integer.class);
        this.friendsCount           = getValue(jsonResponse, kFriendsCount,         Integer.class);
        this.connectionLevel        = getValue(jsonResponse, kConnectionLevel,      Integer.class);

        try {
            this.primaryAddress = new Address(getValue(jsonResponse, kPrimaryAddress, JSONObject.class));
        } catch (Exception e) {
            Log.e(TAG,e.getMessage() + "on primary address");
        }

        try {
            this.userSocialLinks = handleSocialLinks(getValue(jsonResponse, kUserSocialLinks, JSONArray.class));
        } catch (Exception e) {
            Log.e(TAG,e.getMessage() + "on userSocialLinks");
        }

    }

    private List<SocialLink> handleSocialLinks(JSONArray jsonArray) {
        List<SocialLink> userSocialLinks = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                userSocialLinks.add(new SocialLink(jsonArray.getJSONObject(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return userSocialLinks;
    }


    public String getTAG() {
        return TAG;
    }

    public Integer getUserId() {
        return userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public String getJoinedDate() {
        return joinedDate;
    }

    public String getShopName() {
        return shopName;
    }

    public Integer getRating() {
        return rating;
    }

    public String getProfilePicURL() {
        return profilePicURL;
    }

    public Integer getSellingProductCount() {
        return sellingProductCount;
    }

    public Integer getFriendsCount() {
        return friendsCount;
    }

    public List<SocialLink> getUserSocialLinks() {
        return userSocialLinks;
    }

    public Address getPrimaryAddress() {
        return primaryAddress;
    }

    public Integer getConnectionLevel() {
        return connectionLevel;
    }

    public void setConnectionLevel(Integer connectionLevel) {
        this.connectionLevel = connectionLevel;
    }

    public String getSuggestionTag() {
        return suggestionTag;
    }

    public Integer getUserConnectionStatus() {
        return userConnectionStatus;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getFullName() {
        return firstName+" "+lastName;
    }

}
