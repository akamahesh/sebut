package com.app.inn.peepsin.UI.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Constants.Constants.ProductStatus;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.Feeds;
import com.app.inn.peepsin.Models.MyShop;
import com.app.inn.peepsin.Models.SellingProduct;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.fragments.Feeds.ProductDetailFragment;
import com.app.inn.peepsin.UI.fragments.SellStuffModule.SellingStuffDetailFragment;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Nonnull;

import butterknife.BindColor;
import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kProductStatus;

/**
 * Created by root on 30/5/17.
 */

public class SellingStuffAdapter extends RecyclerView.Adapter<SellingStuffAdapter.StuffHolder> {

    private Context context;
    private List<SellingProduct> feedList;
    private int sellType;
    private MyShop shop;
    private static Switcher switcherListener;
    private ProgressDialog progressDialog;

    public SellingStuffAdapter(Context context, List<SellingProduct> items, Switcher switcher,
                               int type, MyShop shop) {
        switcherListener = switcher;
        this.context = context;
        this.sellType = type;
        this.shop = (shop != null) ? shop : ModelManager.modelManager().getCurrentUser().getMyShop();
        feedList = items;
        progressDialog = Utils.generateProgressDialog(context, false);
    }

    @Override
    public StuffHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_selling_stuff_layout, parent, false);
        return new StuffHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(StuffHolder holder, int position) {
        SellingProduct item = feedList.get(position);
        holder.bindContent(item);
        String name = item.getName();
        Integer quantity = item.getQuantity();
        String shareCount = String.valueOf(item.getShareCount());
        String favCount = String.valueOf(item.getFavouriteCount());
        String price = item.getPrice();
        String offerPrice = item.getSellingPrice();
        String postingDate = /*context.getString(R.string.postedOn) + " " + */item.getPostingDate();
        String thumbnailURL = item.getImageThumbUrl();
        String rating = String.valueOf(item.getRating());
        String skui = "SKU No. " + item.getSKUNumber();
        String discount = context.getString(R.string.no_offer);
        if (!item.getDiscountTagLine().isEmpty()) {
            discount = item.getDiscountTagLine();
        }
        String quantityText = (quantity > 0) ? "In Stock" : "Out of Stock";
        holder.tvQuantity.setText(quantityText);
        if (quantity > 0) {
            holder.tvQuantity.setTextColor(holder.inStockColor);
        } else {
            holder.tvQuantity.setTextColor(holder.outOfStockColor);
        }

        holder.tvOfferPrice.setText(context.getString(R.string.Rs) + offerPrice);
        holder.textViewPrice.setText(context.getString(R.string.Rs) + price);
        holder.textViewPrice.setPaintFlags(holder.textViewPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        price = price.replaceAll("/", "").replaceAll("-", "");
        offerPrice = offerPrice.replaceAll("/", "").replaceAll("-", "");

        try {
            Float pricefloat = Float.parseFloat(price);
            Float sellingfloat = Float.parseFloat(offerPrice);
            if (pricefloat <= sellingfloat) {
                holder.textViewPrice.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        holder.tvSKUI.setText(skui);
        holder.textViewItemName.setText(name);
        holder.textViewShareCount.setText(shareCount);
        holder.textViewFavCount.setText(favCount);
        holder.textViewPrice.setText(price);
        holder.tvDiscount.setText(discount);
        holder.textViewTime.setText(postingDate);
        holder.tvStarRating.setText(rating);
        if (!thumbnailURL.isEmpty()) {
            Picasso.with(context)
                    .load(thumbnailURL)
                    .placeholder(R.drawable.img_product_placeholder)
                    .error(R.drawable.img_product_placeholder)
                    .into(holder.imageViewItem);
        }

        setProductStatus(holder, item.getProductStatus());

    }

    private void setProductStatus(StuffHolder holder, @Nonnull Integer productStatus) {
        //(2 Active, 3 InActive, 4 waiting for approval, 6 Draft)
        holder.btnPublish.setVisibility(View.VISIBLE);
        holder.btnDelete.setVisibility(View.VISIBLE);
        switch (productStatus) {
            case 2:
                holder.tvStatus.setText("Active");
                holder.tvStatus.setBackground(holder.positiveFlag);
                holder.btnPublish.setVisibility(View.GONE);
                holder.btnDelete.setText("Inactive");
                holder.btnDelete.setBackgroundColor(holder.negativeColor);
                holder.btnDelete.setTag(ProductStatus.inActive);
                break;
            case 3:
                holder.tvStatus.setText("Inactive");
                holder.tvStatus.setBackground(holder.negativeFlag);
                holder.btnPublish.setText("Active");
                holder.btnPublish.setBackgroundColor(holder.positiveColor);
                holder.btnPublish.setTag(ProductStatus.active);
                holder.btnDelete.setText("Delete");
                holder.btnDelete.setBackgroundColor(holder.negativeColor);
                holder.btnDelete.setTag(ProductStatus.delete);
                break;
            case 4:
                holder.tvStatus.setText("In-process");
                holder.tvStatus.setBackground(holder.positiveFlag);
                holder.btnPublish.setVisibility(View.GONE);
                holder.btnDelete.setVisibility(View.GONE);
                break;
            case 6:
                holder.tvStatus.setText("Draft");
                holder.tvStatus.setBackground(holder.positiveFlag);
                holder.btnPublish.setText("Publish");
                holder.btnPublish.setBackgroundColor(holder.positiveColor);
                holder.btnPublish.setTag(ProductStatus.publish);
                holder.btnDelete.setVisibility(View.GONE);
                break;
            default:
                holder.tvStatus.setText("Un-identified");
                holder.tvStatus.setBackground(holder.negativeFlag);
                holder.btnPublish.setVisibility(View.GONE);
                holder.btnDelete.setVisibility(View.GONE);

        }
    }


    public SellingProduct getItem(int index) {
        return feedList.get(index);
    }

    public void addNewItems(List<SellingProduct> list) {
        feedList.clear();
        feedList.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return feedList.size();
    }

    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) {
                progressDialog.cancel();
            }
        }
    }


    class StuffHolder extends RecyclerView.ViewHolder {

        SellingProduct item;
        @BindView(R.id.iv_item)
        ImageView imageViewItem;
        @BindView(R.id.tv_name)
        TextView textViewItemName;
        @BindView(R.id.tv_price)
        TextView textViewPrice;
        @BindView(R.id.tv_offer_price)
        TextView tvOfferPrice;
        @BindView(R.id.tv_time)
        TextView textViewTime;
        @BindView(R.id.tv_offer)
        TextView tvDiscount;
        @BindView(R.id.tv_quantity)
        TextView tvQuantity;
        @BindView(R.id.tv_skui)
        TextView tvSKUI;
        @BindView(R.id.tv_shared_count)
        TextView textViewShareCount;
        @BindView(R.id.tv_favorite_count)
        TextView textViewFavCount;
        @BindView(R.id.tv_text_star)
        TextView tvStarRating;
        @BindView(R.id.tv_status)
        TextView tvStatus;
        @BindView(R.id.btn_delete)
        Button btnDelete;
        @BindView(R.id.btn_publish)
        Button btnPublish;

        @BindDrawable(R.drawable.img_green_flag)
        Drawable positiveFlag;
        @BindDrawable(R.drawable.img_red_flag)
        Drawable negativeFlag;
        @BindColor(R.color.text_color_white)
        int whiteTextColor;
        @BindColor(R.color.activeState)
        int positiveColor;
        @BindColor(R.color.action_color)
        int negativeColor;
        @BindColor(R.color.color_instock)
        int inStockColor;
        @BindColor(R.color.color_out_of_stock)
        int outOfStockColor;

        StuffHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.item_view)
        void onRowSelected() {
            if (sellType == 1) {
                getFeedDetails(item.getId());
            } else {
                if (switcherListener != null) {
                    switcherListener
                            .switchFragment(SellingStuffDetailFragment.newInstance(item, item.getId()), true,
                                    true);
                }
            }
        }

        @OnClick(R.id.btn_delete)
        void onDelete() {
            ProductStatus ps = (ProductStatus) btnDelete.getTag();
            setProductStatus(shop.getId(), item.getId(), ps, getAdapterPosition());
        }

        @OnClick(R.id.btn_publish)
        void onPublish() {
            ProductStatus ps = (ProductStatus) btnPublish.getTag();
            setProductStatus(shop.getId(), item.getId(), ps, getAdapterPosition());
        }


        private void bindContent(SellingProduct item) {
            this.item = item;
        }

        private void getFeedDetails(int productId) {
            showProgress(true);
            ModelManager.modelManager().getProductDetails(productId, (Constants.Status iStatus, GenricResponse<Feeds> genericResponse) -> {
                showProgress(false);
                if (switcherListener != null) {
                    switcherListener.switchFragment(ProductDetailFragment.newInstance(productId, genericResponse.getObject(), switcherListener), true, true);
                }
            }, (Constants.Status iStatus, String message) -> {
                Toaster.toast(message);
                showProgress(false);
            });
        }

        void setProductStatus(int shopId, int productId, Constants.ProductStatus productStatus, int position) {
            showProgress(true);
            ModelManager.modelManager().setProductStatus(shopId, Utils.generateJsonModifyList(productId, productStatus.getValue()), (Constants.Status iStatus, GenricResponse<HashMap<String, Object>> genricResponse) -> {
                HashMap<String, Object> hashMap = genricResponse.getObject();
                Integer status = (Integer) hashMap.get(kProductStatus);
                feedList.get(position).setProductStatus(status);
                notifyItemChanged(position);
                showProgress(false);
            }, (Constants.Status iStatus, String message) -> {
                showProgress(false);
                notifyDataSetChanged();
                Log.e(getClass().getSimpleName(), message);
            });
        }
    }
}
