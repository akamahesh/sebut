package com.app.inn.peepsin.Models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by root on 4/7/17.
 */


public class ShopModel extends BaseModel implements Serializable {
    private Integer id;
    private String name;
    private Integer type;
    private String bannerUrl;
    private String logoUrl;
    private Integer rating;
    private Address address;
    private Boolean isFavorite;
    private Integer favoriteCount;
    private Integer productCount;
    private String ownerName;
    private String email;
    private String phone;
    private String joinedDate;
    private Integer connectionLevel;
    private String profileUrl;
    private boolean isSelected= false;
    private CopyOnWriteArrayList<ProductType> productStore = new CopyOnWriteArrayList<>();
    private CopyOnWriteArrayList<ShopCategory> categoryStore = new CopyOnWriteArrayList<>();

    public ShopModel(JSONObject jsonResponse){
        this.id        = getValue(jsonResponse, kShopId,                   Integer.class);
        this.name      = getValue(jsonResponse, kShopName,                 String.class);
        this.type      = getValue(jsonResponse, kShopType,                 Integer.class);
        this.bannerUrl = getValue(jsonResponse, kShopBannerImageURl,       String.class);
        this.logoUrl   = getValue(jsonResponse, kShopLogoImageURL,         String.class);
        this.rating    = getValue(jsonResponse, kRating,                   Integer.class);
        try {
            this.address   = new Address(getValue(jsonResponse,kShopAddress, JSONObject.class));
        }catch (Exception e){
            e.printStackTrace();
        }
        this.isFavorite         = getValue(jsonResponse,kIsFavorite,        Boolean.class);
        this.favoriteCount      = getValue(jsonResponse,kFavoriteCount,     Integer.class);
        this.productCount       = getValue(jsonResponse,kProductCount,      Integer.class);
        this.ownerName          = getValue(jsonResponse,kOwnerName,         String.class);
        this.email              = getValue(jsonResponse,kEmail,             String.class);
        this.phone              = getValue(jsonResponse,kPhone,             String.class);
        this.joinedDate         = getValue(jsonResponse,kJoinedDate,        String.class);
        this.connectionLevel    = getValue(jsonResponse,kConnectionLevel,   Integer.class);
        try {
            this.profileUrl         = getValue(jsonResponse,kProfilePicUrl,     String.class);
        }catch (Exception e){
            e.printStackTrace();
        }
        try {
            this.productStore       = handleProductListStore(getValue(jsonResponse , kProductTypeStore ,    JSONArray.class));
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            this.categoryStore      = handleCategoryListStore(getValue(jsonResponse , kCategoryStore ,    JSONArray.class));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private CopyOnWriteArrayList<ShopCategory> handleCategoryListStore(JSONArray jsonArray) throws JSONException {
        CopyOnWriteArrayList<ShopCategory> shopCategories = new CopyOnWriteArrayList<>();
        for(int i = 0;i<jsonArray.length();i++){
            ShopCategory shopCategory = new ShopCategory(jsonArray.getJSONObject(i));
            shopCategories.add(shopCategory);
        }
        return shopCategories;
    }

    private CopyOnWriteArrayList<ProductType> handleProductListStore(JSONArray jsonArray) throws JSONException {
        CopyOnWriteArrayList<ProductType> shopProducts = new CopyOnWriteArrayList<>();
        for(int i = 0;i<jsonArray.length();i++){
            ProductType shopProduct = new ProductType(jsonArray.getJSONObject(i));
            shopProducts.add(shopProduct);
        }
        return shopProducts;
    }


    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getType() {
        return type;
    }

    public String getBannerUrl() {
        return bannerUrl;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public Integer getRating() {
        return rating;
    }

    public Address getAddress() {
        return address;
    }

    public Boolean getFavorite() {
        return isFavorite;
    }

    public Integer getFavoriteCount() {
        return favoriteCount;
    }

    public void setFavorite(Boolean favorite) {
        isFavorite = favorite;
    }

    public Integer getProductCount() {
        return productCount;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getJoinedDate() {
        return joinedDate;
    }

    public Integer getConnectionLevel() {
        return connectionLevel;
    }

    public CopyOnWriteArrayList<ShopCategory> getCategoryStore() {
        return categoryStore;
    }

    public CopyOnWriteArrayList<ProductType> getProductStore() {
        return productStore;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}