package com.app.inn.peepsin.UI.fragments.Shop;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.activities.HomeActivity;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import com.transitionseverywhere.Slide;
import com.transitionseverywhere.TransitionManager;

/**
 * Created by root on 4/7/17.
 */

public class ShopIntermediateFragment extends Fragment {

  private static Switcher switchListener;

  @BindView(R.id.iv_stroller)
  ImageView ivStroller;
  @BindView(R.id.vg_stroller)
  ViewGroup vgStroller;

  public static Fragment newInstance(Switcher switcher) {
    switchListener = switcher;
    return new ShopIntermediateFragment();
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_shop_intermediate, container, false);
    ButterKnife.bind(this, view);
    return view;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    ivStroller.setVisibility(View.VISIBLE);
  /*  final Handler handler = new Handler();
    handler.postDelayed(() -> {
      TransitionManager.beginDelayedTransition(vgStroller, new Slide(Gravity.START));
      ivStroller.setVisibility(View.VISIBLE);
    }, 1000);*/

  }


  @OnClick(R.id.btn_yes)
  void onPositive() {
    if (switchListener != null) {
      switchListener.switchFragment(CreateShopFragment.newInstance(CreateShopFragment.Type.CREATE_SHOP, null, switchListener), true, true);
    }
  }

  @OnClick(R.id.btn_no)
  void onNegative() {
    startActivity(HomeActivity.getIntent(getContext()));
  }
}
