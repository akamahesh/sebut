package com.app.inn.peepsin.Models;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by root on 18/5/17.
 */

public class PayUmoneyDetails extends BaseModel implements Serializable{


    private String firstname;
    private String key;
    private String merchantId;
    private String amount;
    private String email;
    private String phone;
    private String productinfo;
    private String surl;
    private String furl;
    private String txnid;
    private String environment;
    private String salt;
    private String hash;

    public PayUmoneyDetails(JSONObject jsonResponse) {
        this.firstname           = getValue(jsonResponse, kPFirstName,              String.class);
        this.key                 = getValue(jsonResponse, kKey,                String.class);
        this.merchantId          = getValue(jsonResponse, kMerchantId,                String.class);
        this.amount              = getValue(jsonResponse, kAmount,                String.class);
        this.email               = getValue(jsonResponse, kEmail,              String.class);
        this.phone               = getValue(jsonResponse, kPhone,          String.class);
        this.productinfo         = getValue(jsonResponse, kProductinfo,          String.class);
        this.surl                = getValue(jsonResponse, kSurl,          String.class);
        this.furl                = getValue(jsonResponse, kFurl,          String.class);
        this.txnid               = getValue(jsonResponse, kTxnid,          String.class);
        this.environment         = getValue(jsonResponse, kEnvironment,          String.class);
        this.salt                = getValue(jsonResponse, kSalt,          String.class);
        this.hash                = getValue(jsonResponse, kHash,          String.class);


    }


    public String geFirstname() {
        return firstname;
    }

    public String getKey() {
        return key;
    }


    public String getMerchantId() {
        return merchantId;
    }

    public String getAmount() {
        return amount;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }


    public String getProductinfo() {
        return productinfo;
    }

    public String getSurl() {
        return surl;
    }

    public String getFurl() {
        return furl;
    }

    public String getTxnid() {
        return txnid;
    }


    public String getEnvironment() {
        return environment;
    }

    public String getSalt() {
        return salt;
    }

    public String getHash() {
        return hash;
    }


    public void setFirstname(String firstname) {
        firstname = firstname;
    }

    public void setKey(String key) {
        key = key;
    }

    public void setMerchantId(String merchantId) {
        merchantId = merchantId;
    }

    public void setAmount(String amount) {
        amount = amount;
    }

    public void setEmail(String email) {
        email = email;
    }

    public void setPhone(String phone) {
        phone = phone;
    }

    public void setProductinfo(String productinfo) {
        productinfo = productinfo;
    }
    public void setSurl(String  surl) {
        surl = surl;
    }
    public void setfurl(String furl) {
        furl = furl;
    }

    public void setTxnid(String  txnid) {
        txnid = txnid;
    }
    public void setEnvironment(String environment) {
        environment = environment;
    }
    public void setSalt(String  salt) {
        salt = salt;
    }
    public void setHash(String hash) {
        hash = hash;
    }



}
