package com.app.inn.peepsin.Models;

import com.app.inn.peepsin.Constants.Constants;

import java.util.Map;

/**
 * Created by mahesh on 30/5/17.
 */

public class FacebookContact {
    private String id;
    private String name;
    private boolean isRequestSent;
    public FacebookContact(Map<String, Object> dataMap) {
        this.id = (String) dataMap.get(Constants.kFacebookCommanIdKey);
        this.name = (String) dataMap.get(Constants.kFacebookCommanNameKey);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isRequestSent() {
        return isRequestSent;
    }

    public void setRequestSent(boolean requestSent) {
        isRequestSent = requestSent;
    }


    @Override
    public String toString() {
        return "Facebook Contact : {" +"\n"+
                " socialId : " +id+"\n"+
                " name : " +name+"\n"+
                "}";
    }
}
