package com.app.inn.peepsin.UI.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.app.inn.peepsin.Models.ShopModel;
import com.app.inn.peepsin.R;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by dhruv on 21/7/17.
 */

public class ShopCategoryAdapter extends RecyclerView.Adapter<ShopCategoryAdapter.ShopCategoryViewHolder> {

    private List<ShopModel> inventoryList;
    private ColorGenerator generator = ColorGenerator.MATERIAL;
    Context context;
    private ShopCategoryFilter feedsCategoryListener;


    public interface ShopCategoryFilter {
        void Categoryfilter(Integer id,ShopModel shop);
    }


    public ShopCategoryAdapter(List<ShopModel> list, Context context,ShopCategoryFilter feedsCategoryListener) {
        this.inventoryList = list;
        this.context = context;
        this.feedsCategoryListener = feedsCategoryListener;
    }

    @Override
    public ShopCategoryViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_feeds_inventrory_base, viewGroup, false);
        return new ShopCategoryViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ShopCategoryViewHolder holder, int position) {
        ShopModel shop = inventoryList.get(position);
        String name = shop.getName();
        holder.bindContent(shop);

        TextDrawable drawable = TextDrawable.builder().buildRound(String.valueOf(name.toUpperCase().charAt(0)), generator.getRandomColor());

        if (!shop.getBannerUrl().isEmpty()) {
            Picasso.with(context)
                    .load(shop.getBannerUrl())
                    .placeholder(R.drawable.img_product_placeholder)
                    .into(holder.ivItemImage);
        } else {
            holder.ivItemImage.setImageDrawable(drawable);
        }

        if(shop.isSelected()){
            holder.ivItemImage.setBackgroundResource(R.drawable.canvas_circular_image_background);
            holder.tvItemName.setTextColor(context.getResources().getColor(R.color.favorite_highlight_color));

        }else {
            holder.tvItemName.setTextColor(context.getResources().getColor(R.color.text_color_lite));
            holder.ivItemImage.setBackgroundResource(R.color.window_background);
        }


        holder.tvItemName.setText(inventoryList.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return inventoryList.size();
    }

    public void addItems(List<ShopModel> list){
        inventoryList.clear();
        inventoryList.addAll(list);
        notifyDataSetChanged();
    }

    public void addItem(CopyOnWriteArrayList<ShopModel> list) {
        inventoryList.addAll(list);
        inventoryList.clear();
        notifyDataSetChanged();
    }

    class ShopCategoryViewHolder extends RecyclerView.ViewHolder{
        ShopModel item;

        @BindView(R.id.iv_item_image)
        ImageView ivItemImage;
        @BindView(R.id.tv_item_name)
        TextView tvItemName;

        ShopCategoryViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        @OnClick(R.id.item_view)
        void onShopCategory(){
            for (ShopModel shopCategory:inventoryList) {
                shopCategory.setSelected(false);
            }
            item.setSelected(true);
            notifyDataSetChanged();
            feedsCategoryListener.Categoryfilter(item.getId(),item);
        }

        public void bindContent(ShopModel shop) {
            this.item = shop;
        }
    }
}
