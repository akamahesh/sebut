package com.app.inn.peepsin.Models;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by root on 18/5/17.
 */

public class Seller extends BaseModel implements Serializable{
    private Integer id;
    private String profilePicUrl;
    private String firstName;
    private String lastName;
    private String jabberId;
    private Integer connectionLevel;

    Seller(JSONObject jsonResponse) {
        this.id                    = getValue(jsonResponse, kUserId,            Integer.class);
        this.profilePicUrl         = getValue(jsonResponse, kProfilePicUrl,     String.class);
        this.firstName             = getValue(jsonResponse, kFirstName,         String.class);
        this.lastName              = getValue(jsonResponse, kLastName,          String.class);
        this.jabberId              = getValue(jsonResponse, kJabberId,          String.class);
        this.connectionLevel       = getValue(jsonResponse, kConnectionLevel,   Integer.class);
    }

    public Integer getId() {
        return id;
    }

    public String getProfilePicUrl() {
        return profilePicUrl;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getFullName() {
        return firstName+" "+lastName;
    }

    public String getLastName() {
        return lastName;
    }

    public Integer getConnectionLevel() {
        return connectionLevel;
    }

    public String getJabberId() {
        return jabberId;
    }

    public void setJabberId(String jabberId) {
        this.jabberId = jabberId;
    }
}
