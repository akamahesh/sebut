package com.app.inn.peepsin.UI.fragments.Profile;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.inn.peepsin.Models.FinalProductsRecords;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.adapters.OrderHistoryDetailShowProductAdapter;
import com.app.inn.peepsin.UI.helpers.DividerItemRecyclerDecoration;
import com.app.inn.peepsin.UI.helpers.Utils;

import java.io.Serializable;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kShop;

/**
 * Created by dhruv on 4/8/17.
 */

public class OrderHistoryDetailFragment extends Fragment {

    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.rv_order_history)
    RecyclerView mRecyclerView;
 /*  @BindView(R.id.shop_name_view)
    View shopNameView;
   @BindView(R.id.tv_shop_name)
    TextView tvShopName;
   @BindView(R.id.tv_shop_address)
    TextView tvShopAddress;*/

    private ProgressDialog progressDialog;
    private List<FinalProductsRecords> shopList;


    public static Fragment newInstance(List<FinalProductsRecords> shopList) {
        Fragment fragment = new OrderHistoryDetailFragment();
        Bundle bdl = new Bundle();
        bdl.putSerializable(kShop, (Serializable) shopList);
        fragment.setArguments(bdl);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = Utils.generateProgressDialog(getContext(), true);
        Bundle bundle = getArguments();
        if (bundle != null) {
            shopList =   (List<FinalProductsRecords>) bundle.getSerializable(kShop);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_order_history_detail, container, false);
        ButterKnife.bind(this, view);
        tvTitle.setText(R.string.products);


     /*   String shopname=shopList.getgetshopName();
        String shopaddress="";
        if (shopRecord.getShopAddress() != null) {
            shopaddress = shopRecord.getShopAddress().getStreetAddress() + ", "
                    + shopRecord.getShopAddress().getCity() + ", "
                    + shopRecord.getShopAddress().getState() + ", "
                    + shopRecord.getShopAddress().getCountry();

        }
       tvShopName.setText(shopName);
       tvShopAddress.setText(shopaddress);*/

        ProductAdapter productAdapter = new ProductAdapter(shopList);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(productAdapter);
        RecyclerView.ItemDecoration itemDecoration = new DividerItemRecyclerDecoration(getContext(), R.drawable.canvas_recycler_divider);
        mRecyclerView.addItemDecoration(itemDecoration);

        return view;
    }

    @OnClick(R.id.btn_back)
    void onBack() {
        getFragmentManager().popBackStack();
    }

    public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {

        private List<FinalProductsRecords> orderList;

        private ProductAdapter(List<FinalProductsRecords> ProductListList) {
            this.orderList = ProductListList;
        }

        @Override
        public ProductAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_shop_name_address_view, parent, false);
            return new ProductAdapter.ViewHolder(v);
        }



        @Override
        public void onBindViewHolder(ProductAdapter.ViewHolder holder, int position) {
            FinalProductsRecords product = orderList.get(position);
            holder.bindContent(product);

            String shopName =product.getshopName();
            String shopaddress="";
            if (product.getShopAddress() != null) {
                 shopaddress = product.getShopAddress().getStreetAddress() + ", "
                        + product.getShopAddress().getCity() + ", "
                        + product.getShopAddress().getState() + ", "
                        + product.getShopAddress().getCountry();

            }
            holder.tvShopName.setText(shopName);
            holder.tvShopAddress.setText(shopaddress);

            OrderHistoryDetailShowProductAdapter orderHistoryDetailShowProductAdapter = new OrderHistoryDetailShowProductAdapter( getContext(), shopList.get(position).getProductRecords());
            holder.rvProductList.setLayoutManager(new LinearLayoutManager(getContext()));
            holder.rvProductList.setAdapter(orderHistoryDetailShowProductAdapter);


          /*  if (position == 0)
                holder.shopNameView.setVisibility(View.GONE);
            else {
                if (!shopName.equals(orderList.get(position - 1).getShopName()))
                    holder.shopNameView.setVisibility(View.VISIBLE);
                else
                    holder.shopNameView.setVisibility(View.GONE);
            }*/

        }

        @Override
        public int getItemCount() {
            return orderList.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            FinalProductsRecords product;
            @BindView(R.id.rv_product_list)
            RecyclerView rvProductList;
            @BindView(R.id.tv_shop_name)
            TextView tvShopName;
            @BindView(R.id.tv_shop_address)
            TextView tvShopAddress;

            public ViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            public void bindContent(FinalProductsRecords item) {
                this.product = item;
            }
        }
    }


}
