package com.app.inn.peepsin.UI.fragments.Profile;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Libraries.Firebase.app.Config;
import com.app.inn.peepsin.Libraries.ImageEditor.ImageController;
import com.app.inn.peepsin.Libraries.ImageEditor.ImageEditor;
import com.app.inn.peepsin.Managers.APIManager.ReachabilityManager;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Managers.SocialManager.FacebookManager;
import com.app.inn.peepsin.Managers.SocialManager.GoogleManager;
import com.app.inn.peepsin.Managers.XMPPManager.RoosterHelper;
import com.app.inn.peepsin.Managers.XMPPManager.RoosterManager;
import com.app.inn.peepsin.Models.Address;
import com.app.inn.peepsin.Models.BaseModel;
import com.app.inn.peepsin.Models.CelebrationMessage;
import com.app.inn.peepsin.Models.CurrentUser;
import com.app.inn.peepsin.Models.MyShop;
import com.app.inn.peepsin.Models.OrderHistory;
import com.app.inn.peepsin.Models.SocialLink;
import com.app.inn.peepsin.Models.SocialUser;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.Views.ColorArcProgressBar;
import com.app.inn.peepsin.UI.activities.FrontActivity;
import com.app.inn.peepsin.UI.adapters.CelebrationMessageAdapter;
import com.app.inn.peepsin.UI.fragments.Board.MyBoardFragment;
import com.app.inn.peepsin.UI.fragments.Connections.ConnectionProfileFragment;
import com.app.inn.peepsin.UI.fragments.Feeds.FavoriteFeedsFragment;
import com.app.inn.peepsin.UI.fragments.SellStuffModule.SellingStuffFragment;
import com.app.inn.peepsin.UI.fragments.Settings.PrivacyAndSettingsFragment;
import com.app.inn.peepsin.UI.fragments.Shop.BeforeMyShopFragment;
import com.app.inn.peepsin.UI.fragments.Shop.CreateShopFragment;
import com.app.inn.peepsin.UI.fragments.Shop.FavoriteShopsFragment;
import com.app.inn.peepsin.UI.helpers.DividerItemRecyclerDecoration;
import com.app.inn.peepsin.UI.helpers.MyBounceInterpolator;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Typewriter;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import com.facebook.AccessToken;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.app.Activity.RESULT_OK;
import static com.app.inn.peepsin.Constants.Constants.CART_UPDATE;
import static com.app.inn.peepsin.Constants.Constants.PROGRESS_UPDATE;
import static com.app.inn.peepsin.Constants.Constants.kAuthToken;
import static com.app.inn.peepsin.Constants.Constants.kChats;
import static com.app.inn.peepsin.Constants.Constants.kContactId;
import static com.app.inn.peepsin.Constants.Constants.kCurrentUser;
import static com.app.inn.peepsin.Constants.Constants.kData;
import static com.app.inn.peepsin.Constants.Constants.kDateOfBirth;
import static com.app.inn.peepsin.Constants.Constants.kEmail;
import static com.app.inn.peepsin.Constants.Constants.kFirstName;
import static com.app.inn.peepsin.Constants.Constants.kGender;
import static com.app.inn.peepsin.Constants.Constants.kLastName;
import static com.app.inn.peepsin.Constants.Constants.kOTP;
import static com.app.inn.peepsin.Constants.Constants.kPhone;
import static com.app.inn.peepsin.Constants.Constants.kProfilePic;
import static com.app.inn.peepsin.Constants.Constants.kRecords;
import static com.app.inn.peepsin.Constants.Constants.kSocialId;
import static com.app.inn.peepsin.Constants.Constants.kType;
import static com.app.inn.peepsin.Libraries.ImageEditor.ImageEditor.PIC_CAMERA;
import static com.app.inn.peepsin.Libraries.ImageEditor.ImageEditor.PIC_CROP;
import static com.app.inn.peepsin.Libraries.ImageEditor.ImageEditor.PIC_GALLARY;
import static com.app.inn.peepsin.UI.fragments.Shop.CreateShopFragment.Type.FROM_PROFILE;


public class ProfileFragment extends Fragment implements
        GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks {
    private String TAG = getTag();
    @BindColor(R.color.navigation_inactive_tab)
    int colorInactive;

    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_profile_image)
    ImageView ivProfileImage;

    @BindView(R.id.iv_phone_verification)
    ImageView ivPhoneVerification;
    @BindView(R.id.iv_email_verification)
    ImageView ivEmailVerification;
    @BindView(R.id.iv_facebook_verification)
    ImageView ivFacebookVerification;
    @BindView(R.id.iv_google_verification)
    ImageView ivGoogleVerification;

    /*@BindView(R.id.tv_total_friends)
    TextView tvFriends;*/
    @BindView(R.id.tv_user_name)
    Typewriter tvUserName;
    @BindView(R.id.iv_my_shop)
    ImageView ivMyShop;
    @BindView(R.id.tv_my_shop)
    TextView tvMyShop;
    @BindView(R.id.tv_shop_status)
    TextView tvShopStatus;
    @BindView(R.id.tv_about)
    TextView tvAbout;
    @BindView(R.id.tv_status)
    TextView tvStatus;
    @BindView(R.id.tv_productCount)
    TextView tvProductCount;
    @BindView(R.id.tv_favoriteShopCount)
    TextView tvFavoriteShopCount;
    @BindView(R.id.tv_favoriteFeedCount)
    TextView tvFavoriteFeedCount;
    @BindView(R.id.tv_myPeepBoardCount)
    TextView tvMyPeepBoardCount;
    @BindView(R.id.tv_myCircleCount)
    TextView tvMyCircleCount;
    @BindView(R.id.friend_count_view)
    View rlUserLevel;
    @BindView(R.id.tv_count)
    TextView tvUserFriendsLevel;
    @BindView(R.id.tv_joined)
    TextView tvJoined;
    @BindView(R.id.tv_name)
    TextView tvFullName;
    @BindView(R.id.view_email)
    View viewEmail;
    @BindView(R.id.tv_email)
    TextView tvEmail;
    @BindView(R.id.tv_phone)
    TextView tvPhone;
    @BindView(R.id.tv_dob)
    TextView tvDOB;
    @BindView(R.id.view_contact)
    View viewPhone;
    @BindView(R.id.view_dob)
    View viewDOB;
    @BindView(R.id.view_gender)
    View viewGender;
    @BindView(R.id.tv_gender)
    TextView tvGender;
    @BindView(R.id.colorProgressBar)
    ColorArcProgressBar colorProgressBar;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.tv_location)
    TextView tvLocation;
    @BindView(R.id.toolbarProfile)
    Toolbar toolbar;
    @BindView(R.id.tv_cartCount)
    TextView tvCartCount;
    @BindView(R.id.tvReffeal)
    TextView tvReffeal;
    @BindView(R.id.view_product)
    View productView;
    @BindView(R.id.appBarLayout)
    AppBarLayout appBarLayout;

    @BindView(R.id.recycler_messages)
    RecyclerView recyclerView;
    @BindView(R.id.tv_message_status)
    TextView tvViewAll;
    @BindView(R.id.tv_message_event)
    TextView tvEmptyMessage;

    @BindView(R.id.btn_edit)
    ImageButton editMenu;

    @BindView(R.id.view_order_history)
    View vOrderHistory;

    @BindView(R.id.view_celebration)
    View viewCelebration;

    private FacebookManager facebookManager;
    @BindView(R.id.facebook_login_button)
    LoginButton loginButton;
    private LoginManager loginManager;

    private static final int RC_SIGN_IN = 9001;
    private GoogleApiClient mGoogleApiClient;

    private ProgressDialog progressDialog;
    private static Switcher profileSwitcherListener;
    private CurrentUser currentUser;

    private final int REQUEST_PERMISSION_CAMERA_STORAGE = 101;
    private String mSelectedImagePathProfile;

    private CelebrationMessageAdapter messageAdapter;
    private CopyOnWriteArrayList<CelebrationMessage> messages;
    String addresss = "";
    // AppBarLayout appBar;

    public ProfileFragment() {
        // Required empty public constructor
    }

    public static ProfileFragment newInstance(Switcher switcher) {
        ProfileFragment fragment = new ProfileFragment();
        profileSwitcherListener = switcher;
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        scopePermission();
        currentUser = ModelManager.modelManager().getCurrentUser();
        messages = new CopyOnWriteArrayList<>();
        facebookManager = new FacebookManager(getContext(), facebookManagerListener);
        progressDialog = Utils.generateProgressDialog(getContext(), true);
    }

    public void scopePermission() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .addOnConnectionFailedListener(this)
                .addConnectionCallbacks(this)
                .build();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view);
        loginButton.setFragment(this);
        //setupToolbar();
        //setHasOptionsMenu(true);
        messageAdapter = new CelebrationMessageAdapter(getContext(), new ArrayList<>(), true, profileSwitcherListener);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(messageAdapter);
        recyclerView.addItemDecoration(new DividerItemRecyclerDecoration(getActivity(), R.drawable.canvas_recycler_divider));
        checkEmptyView();
        checkEvents();
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Utils.hideKeyboard(getContext());
        currentUser = ModelManager.modelManager().getCurrentUser();
        updateUserData(currentUser);
        updateUserProgress();
        setCartUpdate(currentUser.getShoppingCartCount());
        loadMessages();
    }

    private Integer calculateRank(CurrentUser user) {
        Integer counter = 0;
        Address address = user.getPrimaryAddress();

        if (!user.getEmail().getContactId().equals("")) {
            counter += 2;
        }
        if (!user.getPhone().getContactId().equals("")) {
            counter += 2;
        }
        if (!user.getDateOfBirth().equals("")) {
            counter += 2;
        }
        if (address != null && !address.getCity().isEmpty()) {
            counter += 2;
        }
        if (!user.getProfilePicURL().equals("")) {
            counter += 2;
        }
        return (counter * 10);
    }


    @OnClick(R.id.btn_edit)
    void onClick() {
        //creating a popup menu
        PopupMenu popup = new PopupMenu(getContext(), editMenu);
        //inflating menu from xml resource
        popup.inflate(R.menu.menu_my_profile);
        //adding click listener
        popup.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.action_setting:
                    if (profileSwitcherListener != null) {
                        profileSwitcherListener.switchFragment(PrivacyAndSettingsFragment.newInstance(profileSwitcherListener), true, true);
                    }
                    break;
            }
            return false;
        });
        //displaying the popup
        popup.show();
    }

    public void loadOrderHistory() {
        showProgress(true);
        ModelManager.modelManager().getOrderHistory(1,
                (Constants.Status iStatus, GenricResponse<HashMap<String, Object>> genericResponse) -> {
                    HashMap<String, Object> map = genericResponse.getObject();
                    CopyOnWriteArrayList<OrderHistory> orderList = (CopyOnWriteArrayList<OrderHistory>) map.get(kRecords);
                    if (profileSwitcherListener != null) {
                        profileSwitcherListener.switchFragment(OrderHistoryFragment.newInstance(profileSwitcherListener, orderList), true, true);
                    }
                    showProgress(false);
                }, (Constants.Status iStatus, String message) -> {
                    Log.e(TAG, message);
                    showProgress(false);
                });
    }

    private void loadMessages() {
        ModelManager.modelManager().getCelebrationMessageList((Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<CelebrationMessage>> genricResponse) -> {
            messages = genricResponse.getObject();
            if (messages.size() != 0) {
                viewCelebration.setVisibility(View.VISIBLE);
                messageAdapter.addItem(messages.get(0));
                checkEmptyView();
                checkEvents();
            }else{
                viewCelebration.setVisibility(View.GONE);
            }

        }, (Constants.Status iStatus, String message) -> {
            checkEmptyView();
            checkEvents();
            showProgress(false);
        });
    }

    private void checkEvents() {
        if (messages.size() > 1) {
            tvViewAll.setVisibility(View.VISIBLE);
        } else {
            tvViewAll.setVisibility(View.GONE);
        }
    }

    private void checkEmptyView() {
        if (messageAdapter.getItemCount() > 0) {
            tvEmptyMessage.setVisibility(View.GONE);
        } else {
            tvEmptyMessage.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().invalidateOptionsMenu();

        // register cart update receiver
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mBroadcastReceiver,
                new IntentFilter(CART_UPDATE));

        // register profile update receiver
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mBroadcastReceiver,
                new IntentFilter(PROGRESS_UPDATE));
      /*  if(Utils.isprofileClicked==0) {
            appBar.setVisibility(View.VISIBLE);
        }else{
            appBar.setVisibility(View.GONE);

        }*/
    }


    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mBroadcastReceiver);
    }

    public void setCartUpdate(int count) {
        if (count == 0) {
            tvCartCount.setVisibility(View.GONE);
        } else {
            tvCartCount.setVisibility(View.VISIBLE);
            tvCartCount.setText(String.valueOf(count));
        }
    }

    BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(CART_UPDATE)) {
                setCartUpdate(intent.getIntExtra(kData, 0));
            } else if (intent.getAction().equals(PROGRESS_UPDATE)) {
                updateUserProgress();
                loadMessages();
            }
        }
    };

    private void updateUserData(CurrentUser currentUser) {
        try {
            String aboutText = getString(R.string.about_);
            String joinedText = getString(R.string.member_since) + " " + currentUser.getJoinedDate();

            if (!currentUser.getEmail().getContactId().isEmpty()) {
                String emailText = currentUser.getEmail().getContactId();
                tvEmail.setText(emailText);
            } else {
                viewEmail.setVisibility(View.GONE);
            }


            if (!currentUser.getPhone().getContactId().isEmpty()) {
                String phoneText = "Contact on " + currentUser.getPhone().getContactId();
                tvPhone.setText(phoneText);
            } else {
                viewPhone.setVisibility(View.GONE);
            }

            if (!currentUser.getDateOfBirth().isEmpty()) {
                String birthText = "Birthday " + currentUser.getDateOfBirth();
                tvDOB.setText(birthText);
            } else {
                viewDOB.setVisibility(View.GONE);

            }

            if (currentUser.getGender() != -1) {
                String gender;
                if (currentUser.getGender() == 0) {
                    gender = "Gender as Female";
                } else {
                    gender = "Gender as Male";
                }
                tvGender.setText(gender);
            } else {
                viewGender.setVisibility(View.GONE);
            }
            String addressText = "Add Primary Address";
            if (currentUser.getPrimaryAddress() != null && currentUser.getPrimaryAddress().getAddressId() != 0)
                addressText = "Address: " /*+ currentUser.getPrimaryAddress().getStreetAddress() + ", "*/ + currentUser.getPrimaryAddress().getCity()
                        + ", " + currentUser.getPrimaryAddress().getState() + ", " + currentUser.getPrimaryAddress().getCountry();
            String imageUrl = currentUser.getProfilePicURL();
            tvFullName.setText(currentUser.getFullName());
            tvAbout.setText(aboutText);
            tvJoined.setText(joinedText);
            tvLocation.setText(addressText);

            if (!imageUrl.isEmpty()) {
                Picasso.with(getContext())
                        .load(imageUrl)
                        .resize(200, 200)
                        .placeholder(R.drawable.img_profile_placeholder)
                        .into(ivProfileImage);
            } else if (currentUser.getGender() == 0) {
                ivProfileImage.setImageResource(R.drawable.female_placeholder);
            } else if (currentUser.getGender() == 1) {
                ivProfileImage.setImageResource(R.drawable.male_placeholder);
            }

            if (currentUser.getShopId() == -1) {
                ivMyShop.setImageDrawable(getResources().getDrawable(R.drawable.ic_seller));
                tvMyShop.setText(getString(R.string.become_a_seller));
                productView.setVisibility(View.GONE);
                tvShopStatus.setVisibility(View.GONE);
            } else {
                ivMyShop.setImageDrawable(getResources().getDrawable(R.drawable.img_my_shop));
                tvMyShop.setText(getString(R.string.my_shop_title));
                productView.setVisibility(View.VISIBLE);
                tvShopStatus.setVisibility(View.VISIBLE);
            }

            if (!currentUser.getReferralCode().isEmpty()) {
                setReferalCode();
            }

            if (currentUser.getFriendsCount() > 0) {
                Integer circle =
                        currentUser.getFriendsCount() + currentUser.getFriendsCountSecondLevel() + currentUser
                                .getFriendsCountThirdLevel();
                String friends =
                        "Circle - " + circle + " (" + "Direct - " + currentUser.getFriendsCount() + ","
                                + " 2nd Level " + currentUser.getFriendsCountSecondLevel()
                                + "," + " 3rd Level " + currentUser.getFriendsCountThirdLevel() + " )";
                tvUserFriendsLevel.setText(friends);
            } else {
                //rlUserLevel.setVisibility(View.GONE);
                tvUserFriendsLevel.setText(R.string.no_friends_in_your_circle);
            }

            // updateUserModel(currentUser.getAuthToken());
            checkVerification(currentUser);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void setReferalCode() {
        SpannableStringBuilder builder = new SpannableStringBuilder();
        SpannableString strIInd = new SpannableString(getString(R.string.refer_and_earn));
        strIInd.setSpan(new ForegroundColorSpan(Color.parseColor("#454848")), 0, strIInd.length(), 0);
        builder.append(strIInd);

        SpannableString strIIndLevel = new SpannableString(currentUser.getReferralCode());
        strIIndLevel
                .setSpan(new ForegroundColorSpan(Color.parseColor("#DC9369")), 0, strIIndLevel.length(), 0);
        builder.append(strIIndLevel);
        tvReffeal.setText(builder, TextView.BufferType.SPANNABLE);
    }

    @OnClick(R.id.tv_message_status)
    void addEvent() {
        if (profileSwitcherListener != null) {
            profileSwitcherListener.switchFragment(CelebrationMessageFragment.newInstance(profileSwitcherListener), true, true);
        }
    }

    @OnClick(R.id.iv_profile_image)
    public void PickPhoto() {
        Utils.hideKeyboard(getContext());
        checkPermission();
    }

    public void updateUserProgress() {
        if (isAdded()) {
            CurrentUser user = ModelManager.modelManager().getCurrentUser();
            tvUserName.setCharacterDelay(150);
            tvUserName.animateText(user.getFullName());

            //change odf shop status
            if (user.getShopStatus() == 5 || user.getShopStatus() == 6)
                tvShopStatus.setTextColor(getResources().getColor(android.R.color.holo_green_light));
            else
                tvShopStatus.setTextColor(getResources().getColor(android.R.color.holo_red_light));
            tvShopStatus.setText(Utils.getShopStatus(user.getShopStatus()));

            // status tag update
            int percentage = calculateRank(user);
            String value = String.valueOf(percentage) + "%";
            tvStatus.setText(value);
            tvStatus.setVisibility(View.VISIBLE);
            moveViewAnimation(tvStatus);

            // progress Bar update
            colorProgressBar.setCurrentValues(percentage);
            colorProgressBar.setAnimation(0, (percentage * 320 / 100), 3000);

            // Count to be changed on tab click
            if (user.getSellingProductCount() > 0) {
                tvProductCount.setVisibility(View.VISIBLE);
                tvProductCount.setText(String.valueOf(user.getSellingProductCount()));
            } else {
                tvProductCount.setVisibility(View.INVISIBLE);
            }
            if (user.getFavoriteShopCount() > 0) {
                tvFavoriteShopCount.setVisibility(View.VISIBLE);
                tvFavoriteShopCount.setText(String.valueOf(user.getFavoriteShopCount()));
            } else {
                tvFavoriteShopCount.setVisibility(View.INVISIBLE);
            }
            if (user.getFavoriteProductCount() > 0) {
                tvFavoriteFeedCount.setVisibility(View.VISIBLE);
                tvFavoriteFeedCount.setText(String.valueOf(user.getFavoriteProductCount()));
            } else {
                tvFavoriteFeedCount.setVisibility(View.INVISIBLE);
            }
            if (user.getFriendsCount() > 0) {
                tvMyCircleCount.setVisibility(View.VISIBLE);
                tvMyCircleCount.setText(String.valueOf(user.getFriendsCount()));
            } else {
                tvMyCircleCount.setVisibility(View.INVISIBLE);
            }
            if (user.getPeepsBoardCount() > 0) {
                tvMyPeepBoardCount.setVisibility(View.VISIBLE);
                tvMyPeepBoardCount.setText(String.valueOf(user.getPeepsBoardCount()));
            } else {
                tvMyPeepBoardCount.setVisibility(View.INVISIBLE);
            }
        }

        /*ObjectAnimator animation = ObjectAnimator.ofInt (progressBar, "progress", 0, percentage); // see this max value coming back here, we animale towards that value
        animation.setDuration (5000); //in milliseconds
        animation.setInterpolator (new AccelerateInterpolator());
        animation.start();*/
    }

    private void moveViewAnimation(final View view) {
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);

        int originalPos[] = new int[2];
        view.getLocationOnScreen(originalPos);

        int xDelta = view.getWidth() / 6;
        //int xDelta = (dm.widthPixels - view.getMeasuredWidth() - originalPos[0])/2;
        //int yDelta = (dm.heightPixels - view.getMeasuredHeight() - originalPos[1]) / 2;

        // profile image bounce
        Animation favAnim = AnimationUtils.loadAnimation(getContext(), R.anim.pic_bounce);
        MyBounceInterpolator favInterpolar = new MyBounceInterpolator(0.5, 3);
        favAnim.setInterpolator(favInterpolar);
        ivProfileImage.startAnimation(favAnim);

        AnimationSet animationSet = new AnimationSet(true);
        Animation animation1 = new TranslateAnimation(dm.widthPixels, -xDelta, 0, 0);
        animation1.setDuration(1000);

        Animation animation2 = new AlphaAnimation(1.0f, 0.0f);
        animation2.setDuration(1000);
        animation2.setStartOffset(3000);
        animation2.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });

        animationSet.addAnimation(animation1);
        animationSet.addAnimation(animation2);
        animationSet.setFillAfter(true);
        view.startAnimation(animationSet);
    }


    private void checkVerification(CurrentUser currentUser) {
        SocialLink email = currentUser.getEmail();
        boolean isEmailVerified = email.getVerified();
        SocialLink phone = currentUser.getPhone();
        boolean isPhoneVerified = phone.getVerified();
        boolean isFacebookVerified = false;
        for (SocialLink socialLink : currentUser.getUserSocialLinks()) {
            if (socialLink.getType() == 2) {
                isFacebookVerified = !socialLink.getSocialId().isEmpty();
            }
        }

        boolean isGoogleVerified = false;
        for (SocialLink socialLink : currentUser.getUserSocialLinks()) {
            if (socialLink.getType() == 3) {
                isGoogleVerified = !socialLink.getSocialId().isEmpty();
            }
        }

        if (isEmailVerified) {
            ivEmailVerification.clearColorFilter();
            ivEmailVerification.setImageResource(R.drawable.ic_email_verified);
        } else {
            ivEmailVerification.setColorFilter(colorInactive);
        }

        if (isPhoneVerified) {
            ivPhoneVerification.clearColorFilter();
            ivPhoneVerification.setImageResource(R.drawable.ic_phone_verified);
        } else {
            ivPhoneVerification.setColorFilter(colorInactive);
        }

        if (isFacebookVerified) {
            ivFacebookVerification.clearColorFilter();
            ivFacebookVerification.setImageResource(R.drawable.ic_facebook_verified);
        } else {
            ivFacebookVerification.setColorFilter(colorInactive);
        }

        if (isGoogleVerified) {
            ivGoogleVerification.clearColorFilter();
            ivGoogleVerification.setImageResource(R.drawable.ic_google_verified);
        } else {
            ivGoogleVerification.setColorFilter(colorInactive);
        }
    }

    private void linkSocialAccount(String userEmailId, String socialId, Integer type) {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(kContactId, userEmailId);
        parameters.put(kType, type);
        parameters.put(kSocialId, socialId);
        showProgress(true);
        ModelManager.modelManager().linkSocialAccount(parameters, (Constants.Status iStatus) -> {
            showProgress(false);
            currentUser = ModelManager.modelManager().getCurrentUser();
            checkVerification(currentUser);
        }, (Constants.Status iStatus, String message) -> {
            Log.e(TAG, message);
            showProgress(false);
        });
    }

    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) {
                progressDialog.cancel();
            }
        }
    }

    @OnClick(R.id.iv_phone_verification)
    void onVerifyPhone() {
        String phone = currentUser.getPhone().getContactId();
        boolean isPhoneVerified = currentUser.getPhone().getVerified();
        if (phone.isEmpty()) {
            Utils.showAlertDialog(getContext(), "Error!",
                    "Please Edit your profile with a phone Number first!");
            return;
        }

        if (isPhoneVerified) {
            Utils.showAlertDialog(getContext(), "Verified!",
                    "Your account is already verified with Phone.");
        } else {
            verifyContact(phone, 1);
        }
    }

    @OnClick(R.id.iv_email_verification)
    void onVerifyEmail() {
        String email = currentUser.getEmail().getContactId();
        boolean isEmailVerified = currentUser.getEmail().getVerified();
        if (email.isEmpty()) {
            Utils.showAlertDialog(getContext(), "Error!",
                    "Please Edit your profile with an Email first!");
            return;
        }

        if (isEmailVerified) {
            Utils.showAlertDialog(getContext(), "Verified!",
                    "Your account is already verified with Email.");
        } else {
            verifyContact(email, 2);
        }
    }

    //1 for phone and 2 for email
    private void verifyContact(String contactId, int type) {
        showProgress(true);
        ModelManager.modelManager().contactVerification(contactId, (Constants.Status iStatus, GenricResponse<HashMap<String, Object>> genericResponse) -> {
            showProgress(false);
            HashMap<String, Object> hashMap = genericResponse.getObject();
            String OTP = (String) hashMap.get(kOTP);
            if (profileSwitcherListener != null) {
                profileSwitcherListener.switchFragment(ProfileOTPFragment.newInstance(type, -1, profileSwitcherListener, OTP, contactId, addresss, null, new HashMap<>()), true, true);
            }
        }, (Constants.Status iStatus, String message) -> {
            Log.e(TAG, message);
            showProgress(false);
        });
    }


    @OnClick(R.id.iv_facebook_verification)
    void onVerifyFacebook() {
        boolean isFacebookVerified = !currentUser.getUserSocialLinks().get(0).getSocialId().isEmpty();
        if (isFacebookVerified) {
            Utils.showAlertDialog(getContext(), "Verified!",
                    "Your account is already verified with Facebook.");
        } else {
            verifyFacebook();
        }
    }

    @OnClick(R.id.iv_google_verification)
    void onVerifyGoogle() {
        boolean isGoogleVerified = !currentUser.getUserSocialLinks().get(1).getSocialId().isEmpty();
        if (isGoogleVerified) {
            Utils.showAlertDialog(getContext(), "Verified!",
                    "Your account is already verified with Google.");
        } else {
            verifyGoogle();
        }
    }

    private void verifyFacebook() {
        if (!ReachabilityManager.getNetworkStatus()) {
            Utils.showAlertDialog(getContext(), "Login Failed!",
                    "Sorry, Login Failed to reach Google servers. Please check your network or try again later.");
            return;
        }
        loginManager = LoginManager.getInstance();
        loginManager.logInWithReadPermissions(this, Collections.singletonList("email"));
        loginManager.registerCallback(facebookManager.getCallbackManager(),
                facebookManager.getFacebookCallback());
    }

    private void verifyGoogle() {
        if (!ReachabilityManager.getNetworkStatus()) {
            Utils.showAlertDialog(getContext(), "Login Failed!",
                    "Sorry, Login Failed to reach Google servers. Please check your network or try again later.");
            return;
        }
        mGoogleApiClient.connect();
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }


    private void shopDialog() {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_no_shop_view);
        View view = ButterKnife.findById(dialog, R.id.item);
        view.setOnClickListener(v -> {
            dialog.dismiss();
        });
        Button btnChange = ButterKnife.findById(dialog, R.id.btn_create);
        btnChange.setOnClickListener(v -> {
            dialog.dismiss();
            if (profileSwitcherListener != null) {
                profileSwitcherListener.switchFragment(CreateShopFragment.newInstance(FROM_PROFILE, null, profileSwitcherListener), true, true);
            }
        });
        dialog.show();
    }


    @OnClick(R.id.view_order_history)
    void orderHistory() {
        loadOrderHistory();
    }


    @OnClick(R.id.btn_cart)
    void onCart() {
        if (profileSwitcherListener != null) {
            profileSwitcherListener
                    .switchFragment(ShoppingCartFragment.newInstance(profileSwitcherListener), true, true);
        }
        //startActivity(ShoppingCartActivity.getIntent(getContext()));
    }

    @OnClick(R.id.view_my_shop)
    void getMyShop() {
        Integer shopId = currentUser.getShopId();
        if (shopId == -1) {
            shopDialog();
        } else {
            if (profileSwitcherListener != null) {
                profileSwitcherListener
                        .switchFragment(BeforeMyShopFragment.newInstance(profileSwitcherListener, shopId), true,
                                true);
            }
        }
    }

    @OnClick(R.id.view_favorites_shop)
    void getFavoritesShop() {
        if (profileSwitcherListener != null) {
            profileSwitcherListener
                    .switchFragment(FavoriteShopsFragment.newInstance(profileSwitcherListener), true, true);
        }
    }


    @OnClick(R.id.view_iv_favorites_feed)
    void getFavoritesFeed() {
        if (profileSwitcherListener != null) {
            profileSwitcherListener
                    .switchFragment(FavoriteFeedsFragment.newInstance(profileSwitcherListener), true, true);
        }
    }


    @OnClick(R.id.iv_edit)
    void editProfile() {
        if (profileSwitcherListener != null) {
            profileSwitcherListener
                    .switchFragment(EditProfileFragment.newInstance(profileSwitcherListener), true, true);
        }
    }

    @OnClick(R.id.view_refer_and_earn)
    void refferalEran() {
        if (profileSwitcherListener != null) {
            profileSwitcherListener
                    .switchFragment(RefferalCodeFragment.newInstance(profileSwitcherListener), true, true);
        }
    }

    @OnClick(R.id.view_product)
    void onSellingProduct() {
        Integer shopId = currentUser.getShopId();
        if (shopId <= 0) {
            shopDialog();
        } else if (currentUser.getMyShop() == null) {
            showProgress(true);
            ModelManager.modelManager().getMyShopDetails(shopId, (Constants.Status iStatus, GenricResponse<MyShop> genricResponse) -> {
                showProgress(false);
                if (profileSwitcherListener != null) {
                    profileSwitcherListener.switchFragment(
                            SellingStuffFragment.newInstance(profileSwitcherListener, currentUser.getShopId(),
                                    currentUser.getUserId()), true, true);
                }
            }, (Constants.Status iStatus, String message) -> {
                showProgress(false);
                Toaster.toast(message);
            });
        } else {
            if (profileSwitcherListener != null) {
                profileSwitcherListener.switchFragment(SellingStuffFragment
                                .newInstance(profileSwitcherListener, currentUser.getShopId(), currentUser.getUserId()),
                        true, true);
            }
        }
    }


    @OnClick(R.id.view_My_circle)
    void onMyCircle() {
        if (profileSwitcherListener != null) {
            profileSwitcherListener
                    .switchFragment(ConnectionProfileFragment.newInstance(profileSwitcherListener), true,
                            true);
        }
    }


    @OnClick(R.id.view_my_connection)
    void myConnection() {
        if (profileSwitcherListener != null) {
            profileSwitcherListener
                    .switchFragment(MyBoardFragment.newInstance(profileSwitcherListener), true, true);
        }
    }

    FacebookManager.FacebookManagerInterface facebookManagerListener = new FacebookManager.FacebookManagerInterface() {
        @Override
        public void success(SocialUser socialUser) {
            if (currentUser.getEmail().getContactId().equals(socialUser.getEmail())) {
                String socialId = socialUser.getId();
                String socialEmailId = socialUser.getEmail();
                linkSocialAccount(socialEmailId, socialId, Constants.UserRegType.facebook.getValue());
            } else {
                Utils.showAlertDialog(getContext(), "Error",
                        "Please login facebook with " + currentUser.getEmail().getContactId());
                AccessToken.setCurrentAccessToken(null);
                Profile.setCurrentProfile(null);
                loginManager.logOut();
            }
        }

        @Override
        public void failure(String s) {
            Utils.showAlertDialog(getContext(), "Verification Failed",
                    "Error Completing verification. Please try again later.");
        }
    };


    public void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            GoogleSignInAccount account = result.getSignInAccount();
            assert account != null;
            if (currentUser.getEmail().getContactId().equals(account.getEmail())) {
                String socialId = account.getId();
                String socialEmailId = account.getEmail();
                linkSocialAccount(socialEmailId, socialId, Constants.UserRegType.google.getValue());
            } else {
                Utils.showAlertDialog(getContext(), "Error",
                        "Please login google with " + currentUser.getEmail().getContactId());
                signOut();
                mGoogleApiClient.disconnect();
            }
        } else {
            Utils.showAlertDialog(getContext(), "Verification failed",
                    "Error Completing Verification. Please try later");
        }
    }

    public void signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient)
                .setResultCallback(status -> Log.e(TAG, "Result: " + status));
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.v(getTag(), "onConnected");
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.v(getTag(), "onConnectionSuspended");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.v(getTag(), "onConnectionFailed");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        profileSwitcherListener = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        getActivity().getMenuInflater().inflate(R.menu.menu_my_profile, menu);
        //inflater.inflate(R.menu.menu_my_profile, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_setting:
                if (profileSwitcherListener != null) {
                    profileSwitcherListener
                            .switchFragment(PrivacyAndSettingsFragment.newInstance(profileSwitcherListener), true,
                                    true);
                }
                break;
        }
        return true;
    }


    @OnClick(R.id.view_logout)
    void onLogout() {
        showProgress(true);
        SharedPreferences pref = getActivity().getSharedPreferences(Config.SHARED_PREF, 0);
        String firebaseToken = pref.getString("regId", null);

        ModelManager.modelManager().getLogout(firebaseToken, (Constants.Status iStatus) -> {
            showProgress(false);
            clearContent();
            Intent intent = FrontActivity.getIntent(getContext());
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            getActivity().finish();
        }, (Constants.Status iStatus, String message) -> {
            Toaster.toast(message);
            showProgress(false);
        });
    }

    private void clearContent() {

        SharedPreferences preferences = getActivity()
                .getSharedPreferences(BaseModel.kAppPreferences, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(kCurrentUser);
        editor.commit();
        SharedPreferences chatpreferences = getActivity()
                .getSharedPreferences(RoosterHelper.kChatPreferences, Context.MODE_PRIVATE);
        SharedPreferences.Editor chateditor = chatpreferences.edit();
        chateditor.remove(kChats);
        chateditor.commit();
        RoosterManager.roost().disconnectFromXMPPServer();
        {
            ModelManager.modelManager().setCurrentUser(null);
        }
    }

    private void checkPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(), CAMERA) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(getActivity(), READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            openBottomSheetBanner();
        } else if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), CAMERA) && ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), READ_EXTERNAL_STORAGE)) {
            // We've been denied once before. Explain why we need the permission, then ask again.
            Utils.showDialog(getContext(), "Camera & Gallary permissions are required to upload profile images!", "Ask Permission", "Discard", (dialog, which) -> {
                if (which == -1)
                    requestPermissions(new String[]{CAMERA, READ_EXTERNAL_STORAGE}, REQUEST_PERMISSION_CAMERA_STORAGE);
                else
                    dialog.dismiss();
            });
        } else {
            // We've never asked. Just do it.
            requestPermissions(new String[]{CAMERA, READ_EXTERNAL_STORAGE}, REQUEST_PERMISSION_CAMERA_STORAGE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_PERMISSION_CAMERA_STORAGE && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            openBottomSheetBanner();
        } else {
            // We were not granted permission this time, so don't try to show the contact picker
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        facebookManager.getCallbackManager().onActivityResult(requestCode, resultCode, data);
        if (requestCode == GoogleManager.RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        } else if (requestCode == PIC_GALLARY) {
            if (resultCode == RESULT_OK && data != null) {
                Uri selectedImage = data.getData();
                cropImageBottomSheet(selectedImage);
            }
        } else if (requestCode == PIC_CAMERA) {
            if (resultCode == RESULT_OK && data != null) {
                Bitmap bitmap = (Bitmap) data.getExtras().get(kData);
                Uri uri = ImageController.getImageUri(getContext(), bitmap);
                cropImageBottomSheet(uri);
            }
        } else if (requestCode == PIC_CROP) {
            if (resultCode == RESULT_OK && data != null) {
                Uri uri = data.getData();
                if (uri != null) {
                    try {
                        File file = new File(uri.getPath());
                        mSelectedImagePathProfile = file.getAbsolutePath();
                        Picasso.with(getContext())
                                .load(file)
                                .into(ivProfileImage);
                        updateProfile();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public void cropImageBottomSheet(Uri uri) {
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_crop_image, null);
        final Dialog mBottomSheetDialog = new Dialog(getContext(), R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.show();

        view.findViewById(R.id.original_view).setOnClickListener(v -> {
            if (uri != null) {
                try {
                    File file = new File(Utils.getRealPathFromURI(getContext(), uri));
                    mSelectedImagePathProfile = file.getAbsolutePath();
                    Picasso.with(getContext())
                            .load(file)
                            .into(ivProfileImage);
                    updateProfile();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            mBottomSheetDialog.dismiss();
        });
        view.findViewById(R.id.crop_view).setOnClickListener(v -> {
            File outputFile = ImageEditor.getInstance().cropImage(this, uri, PIC_CROP);
            Log.e(TAG, outputFile.getAbsolutePath());
            mBottomSheetDialog.dismiss();
        });
    }

    public void openBottomSheetBanner() {
        View view = getActivity().getLayoutInflater().inflate(R.layout.custom_bottonsheet_layout, null);
        final Dialog mBottomSheetDialog = new Dialog(getContext(), R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.show();

        view.findViewById(R.id.camera_view).setOnClickListener(v -> {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, PIC_CAMERA);
            mBottomSheetDialog.dismiss();
        });

        view.findViewById(R.id.gallery_view).setOnClickListener(v -> {
            Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(gallery, PIC_GALLARY);
            mBottomSheetDialog.dismiss();
        });

        view.findViewById(R.id.cancel_view).setOnClickListener(v -> mBottomSheetDialog.dismiss());
    }

    public void updateProfile() {
        HashMap<String, Object> userMap = getParameters();
        editUserProfile(userMap);
    }

    public HashMap<String, Object> getParameters() {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(kAuthToken, ModelManager.modelManager().getCurrentUser().getAuthToken());
        parameters.put(kFirstName, ModelManager.modelManager().getCurrentUser().getFirstName());
        parameters.put(kLastName, ModelManager.modelManager().getCurrentUser().getLastName());
        if (currentUser.getContactIdType() != -1) {
            if (currentUser.getContactIdType() == 1)
                parameters.put(kEmail, ModelManager.modelManager().getCurrentUser().getEmail().getContactId());
            else
                parameters.put(kPhone, ModelManager.modelManager().getCurrentUser().getPhone().getContactId());
        } else
            parameters.put(kPhone, ModelManager.modelManager().getCurrentUser().getPhone().getContactId());
        parameters.put(kDateOfBirth, ModelManager.modelManager().getCurrentUser().getDateOfBirth());
        parameters.put(kGender, ModelManager.modelManager().getCurrentUser().getGender());
        if (mSelectedImagePathProfile != null) {
            parameters.put(kProfilePic, new File(mSelectedImagePathProfile));
        }
        return parameters;
    }

    private void editUserProfile(HashMap<String, Object> userMap) {
        showProgress(true);
        ModelManager.modelManager().editMyProfile(userMap, (Constants.Status iStatus) -> {
            Log.v(TAG, "Profile Edited Successfully");
            showProgress(false);
            updateUserProgress();
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Toaster.toast(message);
        });
    }

}

