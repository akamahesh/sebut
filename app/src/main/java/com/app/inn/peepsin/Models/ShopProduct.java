package com.app.inn.peepsin.Models;


import android.util.Log;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by akaMahesh on 4/9/17
 * contact : mckay1718@gmail.com
 */

public class ShopProduct extends BaseModel implements Serializable {

  static final String TAG = "ShopProduct";
  private Integer productId;
  private String name;
  private String description;
  private String coverImageThumbnailURL;
  private String coverImageURL;
  private List<String> otherImageURLList = null;
  private List<String> otherImageThumbnailURLList = null;
  private Boolean isFavorite;
  private String postingDate;
  private String discountTagLine;
  private Integer quantity;
  private String price;
  private String sellingPrice;
  private String netPrice;
  private Integer rating;
  private Integer totalRatingCount;
  private String sKUNumber;
  private String specification;
  private Integer productTypeId;
  private Integer categoryId;
  private String productCategory;
  private String productSubcategory;

  public ShopProduct(JSONObject jsonResponse) {
    this.productId                  = getValue(jsonResponse, kProductId,          Integer.class);
    this.name                       = getValue(jsonResponse, kName,               String.class);
    this.description                = getValue(jsonResponse, kDescription,        String.class);
    this.coverImageThumbnailURL     = getValue(jsonResponse, kCoverImageThumbnailURL,        String.class);
    this.coverImageURL              = getValue(jsonResponse, kCoverImageUrl,                 String.class);
    this.otherImageURLList          = handleUrlList(getValue(jsonResponse, kOtherImageURLList,          JSONArray.class));
    this.otherImageThumbnailURLList = handleUrlList(getValue(jsonResponse, kOtherImageThumbnailURLList, JSONArray.class));
    this.isFavorite                 = getValue(jsonResponse, kIsFavorite,             Boolean.class);
    this.postingDate                = getValue(jsonResponse, kPostingDate,            String.class);
    this.discountTagLine            = getValue(jsonResponse, kDiscountTagLine,        String.class);
    this.quantity                   = getValue(jsonResponse, kQuantity,               String.class);
    this.price                      = getValue(jsonResponse, kPrice,                  String.class);
    this.sellingPrice               = getValue(jsonResponse, kSellingPrice,           String.class);
    this.netPrice                   = getValue(jsonResponse, kNetPrice,               String.class);
    try {
      this.rating                     = getValue(jsonResponse, kRating,                 Integer.class);
    }catch (Exception e){
      Log.e(getClass().getSimpleName(),e.getMessage());
    }
    this.totalRatingCount           = getValue(jsonResponse, kTotalRatingCount,       Integer.class);
    this.sKUNumber                  = getValue(jsonResponse, kSKUNumber,              String.class);
    this.specification              = getValue(jsonResponse, kSpecification,          String.class);
    try {
      this.productTypeId              = getValue(jsonResponse, kProductTypeId,          Integer.class);
    }catch (Exception e){
      Log.e(TAG, "ShopProduct: "+e.getMessage() );
    }

    this.categoryId                 = getValue(jsonResponse, kCategoryId,             Integer.class);
    this.productCategory            = getValue(jsonResponse, kProductCategory,        String.class);
    this.productSubcategory         = getValue(jsonResponse, kProductSubCategory,     String.class);

  }

  private List<String> handleUrlList(JSONArray jsonArray) {
    List<String> imageUrlList = new CopyOnWriteArrayList<>();
    for(int i = 0;i<jsonArray.length();i++){
      try {
        String url = jsonArray.getString(i);
        imageUrlList.add(url);
      } catch (JSONException e) {
        e.printStackTrace();
      }
    }
    return imageUrlList;
  }

  public Integer getProductId() {
    return productId;
  }

  public String getName() {
    return name;
  }

  public String getDescription() {
    return description;
  }

  public String getCoverImageThumbnailURL() {
    return coverImageThumbnailURL;
  }

  public String getCoverImageURL() {
    return coverImageURL;
  }

  public List<String> getOtherImageURLList() {
    return otherImageURLList;
  }

  public List<String> getOtherImageThumbnailURLList() {
    return otherImageThumbnailURLList;
  }

  public Boolean getFavorite() {
    return isFavorite;
  }

  public void setFavorite(Boolean favorite) {
    isFavorite = favorite;
  }

  public String getPostingDate() {
    return postingDate;
  }

  public String getDiscountTagLine() {
    return discountTagLine;
  }

  public Integer getQuantity() {
    return quantity;
  }

  public String getPrice() {
    return price;
  }

  public String getSellingPrice() {
    return sellingPrice;
  }

  public String getNetPrice() {
    return netPrice;
  }

  public Integer getRating() {
    return rating;
  }

  public Integer getTotalRatingCount() {
    return totalRatingCount;
  }

  public String getsKUNumber() {
    return sKUNumber;
  }

  public String getSpecification() {
    return specification;
  }

  public Integer getProductTypeId() {
    return productTypeId;
  }

  public Integer getCategoryId() {
    return categoryId;
  }

  public String getProductCategory() {
    return productCategory;
  }

  public String getProductSubcategory() {
    return productSubcategory;
  }

}
