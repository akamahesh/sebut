package com.app.inn.peepsin.UI.fragments.Connections;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.DataBase.DataBaseHandler;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.Address;
import com.app.inn.peepsin.Models.Connection;
import com.app.inn.peepsin.Models.MyConnections;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.fragments.Profile.UserProfileFragment;
import com.app.inn.peepsin.UI.helpers.DividerItemRecyclerDecoration;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kCurrentUser;
import static com.app.inn.peepsin.Constants.Constants.kUserId;

/**
 * Created by mahesh on 6/4/17.
 */

public class ProfileMyConnectionFragment extends Fragment {
    private final String TAG = getClass().getSimpleName();
    private List<Connection> connectionList;
    private ConnectionAdapter connectionAdapter;

    @BindView(R.id.empty_view)
    View emptyView;
    @BindView(R.id.recycler_view_myconnection)
    RecyclerView recyclerView;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.edt_search)
    EditText edtSearch;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    ProgressDialog progressDialog;
    private String filterString;

    public static Switcher switcherListener;
    private String mUserId;
    private List<MyConnections> myconnectionList;
    DataBaseHandler dbHandler;


    public static Fragment newInstance(Switcher switcher, int tabselected) {
        switcherListener = switcher;
        ProfileMyConnectionFragment fragment = new ProfileMyConnectionFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("tabSelected", tabselected);

        fragment.setArguments(bundle);
        return fragment;
    }

    private BroadcastReceiver mRegistrationBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(kCurrentUser)) {
                connectionList = ModelManager.modelManager().getCurrentUser().getFilteredConnectionList();
                connectionAdapter.addUsers(myconnectionList);
                checkEmptyScreen();
                loadData();
            }
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = Utils.generateProgressDialog(getContext(), false);
        mUserId = String.valueOf(ModelManager.modelManager().getCurrentUser().getUserId());

        connectionList = new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_myconnection, container, false);
        ButterKnife.bind(this, view);
        dbHandler = new DataBaseHandler(getContext());
        myconnectionList=new ArrayList<>();

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DividerItemRecyclerDecoration(getActivity(), R.drawable.canvas_recycler_divider));
        connectionAdapter = new ConnectionAdapter(myconnectionList);
        recyclerView.setAdapter(connectionAdapter);
        swipeToDismissTouchHelper.attachToRecyclerView(recyclerView);
        swipeRefreshLayout.setOnRefreshListener(onRefreshListener);

        edtSearch.setOnEditorActionListener(actionListener);
        edtSearch.addTextChangedListener(searchTextWatcher);
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        connectionList = ModelManager.modelManager().getCurrentUser().getFilteredConnectionList();

        loadData();
        List<MyConnections> contactList = dbHandler.getConnectionsFromDatabase(mUserId,"0");
        /*if (contactList.isEmpty() ) {
            loadData();
        }*/

        myconnectionList.addAll(contactList);
        //connectionAdapter.addUsers(myconnectionList);
        connectionAdapter.notifyDataSetChanged();

        checkEmptyScreen();
        // loadData();

    }

    TextView.OnEditorActionListener actionListener = (v, actionId, event) -> {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            Utils.hideKeyboard(getContext());
        }
        return true;
    };

    TextWatcher searchTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
            charSequence = charSequence.toString().toLowerCase();
            filterString = charSequence.toString().toLowerCase();
            connectionAdapter.getFilter().filter(charSequence);
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };
    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(kCurrentUser));
    }


    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mRegistrationBroadcastReceiver);
    }

    SwipeRefreshLayout.OnRefreshListener onRefreshListener = this::loadData;

    ItemTouchHelper swipeToDismissTouchHelper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
        private Paint p = new Paint();

        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            int swipedPosition = viewHolder.getAdapterPosition();
            MyConnections connection = myconnectionList.get(swipedPosition);
            removeConnection(connection, connection.getUserId(), swipedPosition);
        }

        @Override
        public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
            Bitmap icon;
            if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
                View itemView = viewHolder.itemView;
                float height = (float) itemView.getBottom() - (float) itemView.getTop();
                float width = height / 3;
                p.setColor(Color.parseColor("#D32F2F"));
                RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(), (float) itemView.getRight(), (float) itemView.getBottom());
                c.drawRect(background, p);
                icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_close_white_24dp);
                RectF icon_dest = new RectF((float) itemView.getRight() - 2 * width, (float) itemView.getTop() + width, (float) itemView.getRight() - width, (float) itemView.getBottom() - width);
                c.drawBitmap(icon, null, icon_dest, p);
            }
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
        }
    });

    private void removeConnection(MyConnections connection, int connectionId, int swipedPosition) {
        showProgress(true);
        ModelManager.modelManager().removeUserConnection(dbHandler,connectionId, (Constants.Status iStatus) -> {
            showProgress(false);
            connectionList = ModelManager.modelManager().getCurrentUser().getFilteredConnectionList();
            List<MyConnections> contactList = dbHandler.getConnectionsFromDatabase(mUserId,"0");
            myconnectionList.clear();
            myconnectionList.addAll(contactList);
            //connectionAdapter.addUsers(myconnectionList);
            connectionAdapter.notifyDataSetChanged();

            edtSearch.setText("");
            connectionAdapter.getFilter().filter("");
            checkEmptyScreen();
        }, (Constants.Status iStatus, String message) -> {
            connectionAdapter.notifyDataSetChanged();
            showProgress(false);
            Toaster.toast(message);
            checkEmptyScreen();
        });
    }

    public void loadData() {
        ModelManager.modelManager().getMyConnections(dbHandler,(Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<Connection>> genricResponse) -> {
            myconnectionList.clear();
            List<MyConnections> contactList = dbHandler.getConnectionsFromDatabase(mUserId,"0");
            myconnectionList.addAll(contactList);
            //connectionAdapter.addUsers(myconnectionList);
            connectionAdapter.notifyDataSetChanged();
            checkEmptyScreen();
            swipeRefreshLayout.setRefreshing(false);
        }, (Constants.Status iStatus, String message) -> {
            Log.e(TAG, message);
            checkEmptyScreen();
            swipeRefreshLayout.setRefreshing(false);
        });
    }


    private void checkEmptyScreen() {
        if (connectionAdapter.getItemCount() > 0) {
            emptyView.setVisibility(View.GONE);
        } else {
            emptyView.setVisibility(View.VISIBLE);
        }
    }

    public void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) progressDialog.dismiss();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        switcherListener = null;
    }

    public class ConnectionAdapter extends RecyclerView.Adapter<ConnectionAdapter.MyConnectionViewHolder> {

        private List<MyConnections> userList;
        private List<MyConnections> filterList;

        private ConnectionAdapter(List<MyConnections> userList) {
            this.userList = userList;
            this.filterList = userList;
        }

        @Override
        public ConnectionAdapter.MyConnectionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_connection_item, parent, false);
            return new ConnectionAdapter.MyConnectionViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ConnectionAdapter.MyConnectionViewHolder holder, int position) {
            MyConnections user = userList.get(position);
            holder.bindContent(user);
            String name = user.getFName()+" "+user.getLName();
            Integer connectionLevel = user.getConnectionLevel();
            holder.tvName.setText(name);
            String rating = Integer.toString(user.getRating());


            if (!user.getShopName().equals("")) {
                holder.vLayout.setVisibility(View.VISIBLE);
                holder.tvProfile.setText(/*"Shop: " + */user.getShopName());
            }
            else {
                holder.vLayout.setVisibility(View.INVISIBLE);}

            holder.tvShopRating.setText(rating);


            String addressTxt = "No Location Available";
            /*Address address = user.getPrimaryAddress();
            if (address != null) {
                String street = "";
                if(address.getStreetAddress() != "") {
                    street =address.getStreetAddress() + ", ";
                }
                String city = "";
                String state = "";
                String country = "";
                if(address.getCity() != ""){
                    city = address.getCity()+ ", ";}
                if(address.getCountry() != ""){
                    state = address.getState()+ ", ";}
                if(address.getCountry() != "") {
                    country = address.getCountry();}
                addressTxt = street+city + state + country;
            }*/
            holder.tvAddress.setText(user.getAddress());

            String profileURL = user.getImageURL();
            if (profileURL.isEmpty()) {
                holder.ivCircularUserImage.setVisibility(View.INVISIBLE);
                holder.ivUserImage.setVisibility(View.VISIBLE);
            } else {
                holder.ivCircularUserImage.setVisibility(View.VISIBLE);
                holder.ivUserImage.setVisibility(View.INVISIBLE);
            }
            TextDrawable drawable = TextDrawable.builder().buildRound(String.valueOf(name.toUpperCase().charAt(0)), ColorGenerator.MATERIAL.getRandomColor());

            if (!profileURL.isEmpty())
                Picasso.with(getContext())
                        .load(profileURL)
                        .placeholder(R.drawable.img_profile_placeholder)
                        .into(holder.ivCircularUserImage);
            else
                holder.ivUserImage.setImageDrawable(drawable);


            if (connectionLevel == 1) {
                holder.tvConnectionLevel.setText("1st");
            } else if (connectionLevel == 2) {
                holder.tvConnectionLevel.setText("2nd");
            } else if (connectionLevel == 3) {
                holder.tvConnectionLevel.setText("3rd");
            } else {
                holder.tvConnectionLevel.setVisibility(View.INVISIBLE);
            }
        }

        @Override
        public int getItemCount() {
            return userList.size();
        }

        private Filter getFilter() {
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence charSequence) {

                    String charString = charSequence.toString();
                    if (charString.isEmpty()) {
                        userList = filterList;
                    } else {
                        ArrayList<MyConnections> filteredList = new ArrayList<>();
                        for (MyConnections contacts : userList) {
                            if (contacts.getFName().toLowerCase().contains(charString)) {
                                filteredList.add(contacts);
                            }
                        }
                        userList = filteredList;
                    }
                    FilterResults filterResults = new FilterResults();
                    filterResults.values = userList;
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                    userList = (List<MyConnections>) filterResults.values;
                    notifyDataSetChanged();
                    checkEmptyScreen();
                }
            };
        }

        public MyConnections getItem(int swipedPosition) {
            return userList.get(swipedPosition);
        }

        private void addUsers(List<MyConnections> object) {
            userList.clear();
            userList.addAll(object);
            notifyDataSetChanged();
        }

        public void removeUser(int position) {
            userList.remove(position);
            notifyDataSetChanged();
        }

        class MyConnectionViewHolder extends RecyclerView.ViewHolder {
            private MyConnections connection;
            @BindView(R.id.tv_name)
            TextView tvName;
            @BindView(R.id.tv_profile)
            TextView tvProfile;
            @BindView(R.id.tv_address)
            TextView tvAddress;
            @BindView(R.id.ll_layout)
            View vLayout;
            @BindView(R.id.tv_level)
            TextView tvConnectionLevel;
            @BindView(R.id.tv_shop_rating)
            TextView tvShopRating;
            @BindView(R.id.iv_user_image)
            ImageView ivUserImage;
            @BindView(R.id.iv_user_image_circular)
            ImageView ivCircularUserImage;

            MyConnectionViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            void bindContent(MyConnections connection) {
                this.connection = connection;
            }

            @OnClick(R.id.item_view)
            void onItemView() {
                getUserProfile(connection.getUserId());
            }

            void getUserProfile(int userId) {
                showProgress(true);
                HashMap<String, Object> parameters = new HashMap<>();
                parameters.put(kUserId, userId);
                ModelManager.modelManager().getUserProfile(parameters, (Constants.Status iStatus, GenricResponse<Connection> genricResponse) -> {
                    showProgress(false);
                    Connection connection = genricResponse.getObject();
                    if (connection.getProfileVisibility()!=0) {
                        if (switcherListener != null)
                            switcherListener.switchFragment(UserProfileFragment.newInstance(switcherListener, connection), true, true);
                    } else
                        Utils.showAlertDialog(getContext(), "Message", "User Profile permission is denied");

                }, (Constants.Status iStatus, String message) -> {
                    Log.e("User Profile", message);
                    Toaster.toast(message);
                    showProgress(false);
                });
            }
        }




    }




}
