package com.app.inn.peepsin.Models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class TransactionDetailModel {
    String imageurl;

    CopyOnWriteArrayList<FinalProductsRecords> productList;
    CopyOnWriteArrayList<ProductList> product;

    public static HashMap<String, List<String>> getData() {
        HashMap<String, List<String>> expandableListDetail = new HashMap<String, List<String>>();

        List<String> bigBazzer = new ArrayList<String>();
        bigBazzer.add("Android");
        bigBazzer.add("iPhone");
        bigBazzer.add("Windows");


        List<String> shoppersShop = new ArrayList<String>();
        shoppersShop.add("Windows");
        shoppersShop.add("Android");
        shoppersShop.add("iPhone");


        List<String> lifeStyle = new ArrayList<String>();
        lifeStyle.add("iPhone");
        lifeStyle.add("Windows");
        lifeStyle.add("Android");



        expandableListDetail.put("Big Bazzer", bigBazzer);
        expandableListDetail.put("Shoppers Shop", shoppersShop);
        expandableListDetail.put("LifeStyle", lifeStyle);
        expandableListDetail.put("Globus", lifeStyle);

        return expandableListDetail;
    }


    public static HashMap<String, List<String>> getLisData(FinalBillSummary finalBillSummary) {
        HashMap<String, List<String>> expandableListDetail = new HashMap<String, List<String>>();
        String productName="";
        HashMap<String,Object> parameters = new HashMap<>();
        CopyOnWriteArrayList<FinalProductsRecords> productList;
        CopyOnWriteArrayList<ProductList> product;
        productList =finalBillSummary.getShopRecords();
        List<String> bigBazzer = new ArrayList<String>();
        List<String> bigBazzerImage = new ArrayList<String>();
        List<Integer> bigBazzerCount = new ArrayList<Integer>();
        List<String> price = new ArrayList<String>();
        List<String> shoppersShop = new ArrayList<String>();
        for(int i=0;i<productList.size();i++){
            productName= productList.get(i).getshopName();
            // String city=finalBillSummary.getAddress().getCity()+", "+finalBillSummary.getAddress().getCountry();
            System.out.println("Product NAme"+productName);
            // System.out.println("city NAme"+city);
            product=productList.get(i).getProductRecords();

            for(int j=0;j<product.size();j++) {
                String productn = product.get(j).getProductName();
                String image = product.get(j).getCoverImageThumbnailURL();
                Integer quantitity = product.get(j).getProductQuantity();
                String cost=product.get(j).getPrice();

                bigBazzer.add(productn);
                bigBazzerImage.add(image);
                bigBazzerCount.add(quantitity);
                price.add(cost);
                System.out.println("Product NAme" + productn);
                System.out.println("Product image" + image);
            }
        }




        expandableListDetail.put(productName, bigBazzer);
        expandableListDetail.put("Shoppers Shop", shoppersShop);
       // expandableListDetail.put("LifeStyle", lifeStyle);
       // expandableListDetail.put("Globus", lifeStyle);

        return expandableListDetail;
    }

    public void getProductList(FinalBillSummary finalBillSummary){
        productList =finalBillSummary.getShopRecords();

        for(int i=0;i<productList.size();i++){
            String productName= productList.get(i).getshopName();
            // String city=finalBillSummary.getAddress().getCity()+", "+finalBillSummary.getAddress().getCountry();
            System.out.println("Product NAme"+productName);
            // System.out.println("city NAme"+city);
            product=productList.get(i).getProductRecords();

            for(int j=0;j<product.size();j++) {
                String productn = product.get(j).getProductName();
                String image = product.get(j).getCoverImageThumbnailURL();
                System.out.println("Product NAme" + productn);
                System.out.println("Product image" + image);
            }
        }

    }

    public String getImageurl(){
        return imageurl;
    }
    public void setImageurl(String imageurl){
        this.imageurl=imageurl;

    }
}
