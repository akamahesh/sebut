package com.app.inn.peepsin.UI.fragments.Base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.fragments.Board.PeepsBoardFragment;
import com.app.inn.peepsin.UI.helpers.FragmentUtil;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;

/**
 * Created by mahesh on 25/4/17.
 */

public class RootBoardFragment extends Fragment {

    public static RootBoardFragment newInstance() {
        return new RootBoardFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_root, container, false);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        FragmentUtil.changeFragment(getChildFragmentManager(), PeepsBoardFragment.newInstance(switcher),true,false);
    }

    Switcher switcher = (fragment, saveInBackstack, animate) -> {
        Utils.hideKeyboard(getContext());
        FragmentUtil.changeFragment(getChildFragmentManager(),fragment,saveInBackstack,animate);
    };




    @Override
    public void onDetach() {
        super.onDetach();
        Log.v("Lifecycle :  ","onDetach"+getClass().getName());
        switcher = null;
    }

}
