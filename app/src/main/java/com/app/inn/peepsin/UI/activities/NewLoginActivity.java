package com.app.inn.peepsin.UI.activities;

import static android.view.View.VISIBLE;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.method.KeyListener;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.app.inn.peepsin.Constants.Constants.MessageType;
import com.app.inn.peepsin.R;
import com.transitionseverywhere.ArcMotion;
import com.transitionseverywhere.ChangeBounds;
import com.transitionseverywhere.Fade;
import com.transitionseverywhere.TransitionManager;

public class NewLoginActivity extends AppCompatActivity {

  private final int kPathAnimationDelay = 800;
  @BindView(R.id.et_fullname)
  EditText etFullName;
  @BindView(R.id.et_contactid)
  EditText etContactId;
  @BindView(R.id.view_group_transition)
  ViewGroup vgTransition;
  @BindView(R.id.btn_continue)
  Button btnContinue;

  private KeyListener mContactKeyListener;
  private KeyListener mFullnameKeyListener;
  private ActionType actionType;

  private static enum ActionType{
    CONTACT_ID,
    FULL_NAME,
    PASSWORD,
    OTP
  }
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_new_login);
    ButterKnife.bind(this);
    mContactKeyListener = etContactId.getKeyListener();
    mFullnameKeyListener = etFullName.getKeyListener();
    setActionType(ActionType.CONTACT_ID);
  }

  @OnClick(R.id.btn_continue)
  void onContinue(){
    if(getActionType().equals(ActionType.CONTACT_ID)){
      TransitionManager.beginDelayedTransition(vgTransition,
          new ChangeBounds().setPathMotion(new ArcMotion()).setDuration(kPathAnimationDelay));

      RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) etContactId
          .getLayoutParams();
      params.removeRule(RelativeLayout.CENTER_IN_PARENT);
      setMargins(etContactId,60,20,20,20);
      etContactId.setKeyListener(null);
      etContactId.setLayoutParams(params);

      setActionType(ActionType.FULL_NAME);
      changeVisibilityWithAnimation(vgTransition,etFullName,VISIBLE);
    }else if(getActionType().equals(ActionType.FULL_NAME)){
      TransitionManager.beginDelayedTransition(vgTransition,
          new ChangeBounds().setPathMotion(new ArcMotion()).setDuration(kPathAnimationDelay));

      RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) etFullName
          .getLayoutParams();
      params.removeRule(RelativeLayout.CENTER_IN_PARENT);
      setMargins(etFullName,100,100,0,0);
      etFullName.setKeyListener(null);
      etFullName.setLayoutParams(params);

      setActionType(ActionType.PASSWORD);
    }



  }

  @OnClick(R.id.et_contactid)
  void onTvContactId(){
    TransitionManager.beginDelayedTransition(vgTransition,
        new ChangeBounds().setPathMotion(new ArcMotion()).setDuration(kPathAnimationDelay));

    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) etContactId
        .getLayoutParams();
    params.addRule(RelativeLayout.CENTER_IN_PARENT);
    setMargins(etContactId,20,20,20,20);
    etContactId.setLayoutParams(params);
    etContactId.setKeyListener(mContactKeyListener);

    setActionType(ActionType.CONTACT_ID);

  }

  @OnClick(R.id.et_fullname)
  void onFullName(){
    TransitionManager.beginDelayedTransition(vgTransition,
        new ChangeBounds().setPathMotion(new ArcMotion()).setDuration(kPathAnimationDelay));

    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) etFullName
        .getLayoutParams();
    params.addRule(RelativeLayout.CENTER_IN_PARENT);
    setMargins(etFullName,20,20,20,20);
    etFullName.setLayoutParams(params);
    etFullName.setKeyListener(mFullnameKeyListener);
    setActionType(ActionType.FULL_NAME);
  }


  public static void setMargins (View v, int l, int t, int r, int b) {
    if (v.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
      ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
      p.setMargins(l, t, r, b);
      v.requestLayout();
    }
  }

  public ActionType getActionType() {
    return actionType;
  }

  public void setActionType(ActionType actionType) {
    this.actionType = actionType;
  }

  private void changeVisibilityWithAnimation(ViewGroup viewGroup, View view, int visible) {
    if (view.getVisibility() != visible) {
      TransitionManager.beginDelayedTransition(viewGroup, new Fade());
      view.setVisibility(visible);

    }
  }
}
