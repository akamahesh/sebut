package com.app.inn.peepsin.UI.fragments.Shop;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Selection;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Libraries.ImageEditor.ImageController;
import com.app.inn.peepsin.Libraries.ImageEditor.ImageEditor;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.Address;
import com.app.inn.peepsin.Models.BankType;
import com.app.inn.peepsin.Models.CurrentUser;
import com.app.inn.peepsin.Models.DeliveryType;
import com.app.inn.peepsin.Models.MyShop;
import com.app.inn.peepsin.Models.ProductType;
import com.app.inn.peepsin.Models.Shop;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.Views.SeekBarHint;
import com.app.inn.peepsin.UI.activities.BaseActivity;
import com.app.inn.peepsin.UI.activities.FrontActivity;
import com.app.inn.peepsin.UI.activities.LogInActivity;
import com.app.inn.peepsin.UI.adapters.BankSpinnerAdapter;
import com.app.inn.peepsin.UI.adapters.DeliverySpinnerAdapter;
import com.app.inn.peepsin.UI.adapters.ProductSpinnerAdapter;
import com.app.inn.peepsin.UI.adapters.ProductTypeAdapter;
import com.app.inn.peepsin.UI.dialogFragments.AddressDialogFragment;
import com.app.inn.peepsin.UI.dialogFragments.PolicyDialogFragment;
import com.app.inn.peepsin.UI.fragments.Profile.ShopOTPFragment;
import com.app.inn.peepsin.UI.helpers.AkaImageUtil;
import com.app.inn.peepsin.UI.helpers.GridSpacingItemDecoration;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.helpers.Validations;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import com.squareup.picasso.Picasso;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.zelory.compressor.Compressor;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.app.Activity.RESULT_OK;
import static com.app.inn.peepsin.Constants.Constants.ADDRESS_RESULT;
import static com.app.inn.peepsin.Constants.Constants.KSCREENHEIGHT;
import static com.app.inn.peepsin.Constants.Constants.KSCREENWIDTH;
import static com.app.inn.peepsin.Constants.Constants.PHOTO_RESULT;
import static com.app.inn.peepsin.Constants.Constants.kAccountHolderName;
import static com.app.inn.peepsin.Constants.Constants.kAccountNumber;
import static com.app.inn.peepsin.Constants.Constants.kAccountType;
import static com.app.inn.peepsin.Constants.Constants.kAddressId;
import static com.app.inn.peepsin.Constants.Constants.kBankDetails;
import static com.app.inn.peepsin.Constants.Constants.kBankName;
import static com.app.inn.peepsin.Constants.Constants.kBusinessType;
import static com.app.inn.peepsin.Constants.Constants.kCompanyCinNumber;
import static com.app.inn.peepsin.Constants.Constants.kCompanyRegisteredAddress;
import static com.app.inn.peepsin.Constants.Constants.kCompanyRegistrationDetail;
import static com.app.inn.peepsin.Constants.Constants.kData;
import static com.app.inn.peepsin.Constants.Constants.kDeliveryTime;
import static com.app.inn.peepsin.Constants.Constants.kEmail;
import static com.app.inn.peepsin.Constants.Constants.kGST;
import static com.app.inn.peepsin.Constants.Constants.kIFSC;
import static com.app.inn.peepsin.Constants.Constants.kIsAlreadyRegistered;
import static com.app.inn.peepsin.Constants.Constants.kKycInfo;
import static com.app.inn.peepsin.Constants.Constants.kOTP;
import static com.app.inn.peepsin.Constants.Constants.kOwnerFirstName;
import static com.app.inn.peepsin.Constants.Constants.kOwnerLastName;
import static com.app.inn.peepsin.Constants.Constants.kPAN;
import static com.app.inn.peepsin.Constants.Constants.kPhone;
import static com.app.inn.peepsin.Constants.Constants.kProductTypeId;
import static com.app.inn.peepsin.Constants.Constants.kShopBannerImage;
import static com.app.inn.peepsin.Constants.Constants.kShopDetails;
import static com.app.inn.peepsin.Constants.Constants.kShopId;
import static com.app.inn.peepsin.Constants.Constants.kShopLogoImage;
import static com.app.inn.peepsin.Constants.Constants.kShopName;
import static com.app.inn.peepsin.Constants.Constants.kShopType;
import static com.app.inn.peepsin.Constants.Constants.kTAN;
import static com.app.inn.peepsin.Constants.Constants.kType;
import static com.app.inn.peepsin.Constants.Constants.kVisibilityLevel;
import static com.app.inn.peepsin.Constants.Constants.kVisibilityRange;
import static com.app.inn.peepsin.Libraries.ImageEditor.ImageEditor.PIC_CAMERA;
import static com.app.inn.peepsin.Libraries.ImageEditor.ImageEditor.PIC_CROP;
import static com.app.inn.peepsin.Libraries.ImageEditor.ImageEditor.PIC_GALLARY;
import static com.app.inn.peepsin.UI.fragments.Shop.CreateShopFragment.Type.EDIT_SHOP;
import static com.app.inn.peepsin.UI.helpers.Utils.generateJsonArrayFromProductList;
import static com.app.inn.peepsin.UI.helpers.Utils.getBusinessType;
import static com.app.inn.peepsin.UI.helpers.Utils.getProperText;

/**
 * Created by root on 3/7/17.
 */

public class CreateShopFragment extends Fragment implements AkaImageUtil.ImageUtilListener {

    private String TAG = getTag();
    Integer screenHeight = 0, screenWidth = 0;
    private static Switcher profileSwitcherListener;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.ivCreateImage)
    ImageView ivProfile;
    @BindView(R.id.ivLogoImage)
    ImageView ivLogo;
    @BindView(R.id.tvName)
    TextView tvShopName;
    @BindView(R.id.edt_shop)
    EditText edtShop;
    @BindView(R.id.edt_owner)
    EditText edtOwner;
    @BindView(R.id.email_view)
    LinearLayout emailView;
    @BindView(R.id.edt_email)
    EditText edtEmail;
    @BindView(R.id.phone_view)
    LinearLayout phoneView;
    @BindView(R.id.edt_phone)
    EditText edtPhone;
    @BindView(R.id.edt_location)
    EditText edtLocation;
    @BindView(R.id.btn_continue)
    Button btnContinue;

    @BindView(R.id.shop_visibility_view)
    View shopRangeView;
    @BindView(R.id.shop_level_view)
    View shopLevelView;
    @BindView(R.id.seek_bar_shop)
    SeekBar shopLevelSeekBar;
    @BindView(R.id.shop_range_bar)
    DiscreteSeekBar shopRangeSeekBar;
    @BindView(R.id.shop_range_seekbar)
    SeekBarHint shopSeekBar;
    @BindView(R.id.shop_type)
    RadioGroup shopGroup;
    @BindView(R.id.radio_public)
    RadioButton publicBtn;
    @BindView(R.id.radio_private)
    RadioButton privateBtn;

    @BindView(R.id.spinner_business_type)
    Spinner spinnerBusiness;
    @BindView(R.id.spinner_delivery_type)
    Spinner spinnerDelivery;
    @BindView(R.id.spinner_product_type)
    Spinner spinnerProduct;
    @BindView(R.id.spinner_bank_name)
    Spinner spinnerBankName;
    @BindView(R.id.recycler_view_product)
    RecyclerView rvProduct;

    @BindView(R.id.edt_pan)
    EditText edtPAN;
    @BindView(R.id.edt_tin)
    EditText edtTAN;
    @BindView(R.id.edt_gst)
    EditText edtGST;
    @BindView(R.id.edt_cin)
    EditText edtCIN;
    @BindView(R.id.edt_registered_address)
    EditText edtCompanyAddress;
    @BindView(R.id.edt_account_name)
    EditText edtAccountName;
    @BindView(R.id.edt_account_number)
    EditText edtAccountNumber;
    @BindView(R.id.edt_bank_name)
    EditText edtBankName;
    @BindView(R.id.edt_IFSC)
    EditText edtIFSC;
    @BindView(R.id.edt_product_type)
    EditText edtProductType;
    @BindView(R.id.edt_delivery)
    EditText edtDeliveryType;
    @BindView(R.id.account_type)
    RadioGroup accountGroup;
    @BindView(R.id.radio_saving)
    RadioButton savingBtn;
    @BindView(R.id.radio_current)
    RadioButton currentBtn;
    @BindView(R.id.tv_seller_agreement)
    TextView tvSellerAgreement;

    int colorRed;
    int colorYellow;
    int colorBlue;
    int colorGreen;

    private ProgressDialog progressDialog;
    private AkaImageUtil akaImageUtil;
    private String mImagePath = "";
    private String mLogoPath = "";
    private Integer imageType = 0;
    private final int REQUEST_PERMISSION_CAMERA_STORAGE = 101;

    private int shopType = 1;
    private int shopVisibilityRange = 5;
    private int shopVisibilityLevel = 1;

    private CurrentUser user;
    private MyShop shop;
    private Address address = null;
    String addresss = "";
    private ProductTypeAdapter adapter;
    private ProductSpinnerAdapter productAdapter;
    private DeliverySpinnerAdapter deliveryAdapter;
    private BankSpinnerAdapter bankAdapter;
    private CopyOnWriteArrayList<ProductType> productTypeList;
    int productTypeId = 1;
    int deliveryTypeId = 1;
    private String accountType = "Saving";
    private String contactType = "";
    private Type mType;

    // 1 for Edit , 0 for Create // 2 for profile create
    public enum Type {
        FROM_PROFILE, EDIT_SHOP, CREATE_SHOP
    }

    public static Fragment newInstance(Type type, MyShop shop, Switcher profileSwitcher) {
        profileSwitcherListener = profileSwitcher;
        Fragment fragment = new CreateShopFragment();
        Bundle bdl = new Bundle();
        bdl.putSerializable(kType, type);
        bdl.putSerializable(kShopDetails, shop);
        fragment.setArguments(bdl);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        user = ModelManager.modelManager().getCurrentUser();
        progressDialog = Utils.generateProgressDialog(getContext(), true);
        akaImageUtil = new AkaImageUtil(CreateShopFragment.this);
        screenHeight = (int) BaseActivity.getHeight();
        screenWidth = (int) BaseActivity.getWidth();
        colorRed = Color.parseColor("#F09D9A");
        colorYellow = Color.parseColor("#9B7F00");
        colorBlue = Color.parseColor("#00959C");
        colorGreen = Color.parseColor("#6BCBA7");
        Bundle bundle = getArguments();
        if (bundle != null) {
            mType = (Type) bundle.getSerializable(kType);
            shop = (MyShop) bundle.getSerializable(kShopDetails);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_shop, container, false);
        ButterKnife.bind(this, view);
        edtLocation.setKeyListener(null);
        edtBankName.setKeyListener(null);
        edtProductType.setKeyListener(null);
        edtDeliveryType.setKeyListener(null);
        if (mType.equals(EDIT_SHOP)) {
            tvTitle.setText(getString(R.string.edit_shop_title));
            btnContinue.setText(getString(R.string.save_text));
        } else {
            tvTitle.setText(getString(R.string.create_shop_title));
        }

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        String email = user.getEmail().getContactId();
        String phone = user.getPhone().getContactId();
        if (!phone.isEmpty() && email.isEmpty()) {
            contactType = phone;
            edtPhone.setText(phone);
            phoneView.setVisibility(View.GONE);
        } else if (!email.isEmpty() && phone.isEmpty()) {
            contactType = email;
            edtEmail.setText(email);
            emailView.setVisibility(View.GONE);
        } else if (!phone.isEmpty() && !email.isEmpty()) {
            edtPhone.setText(phone);
            phoneView.setVisibility(View.GONE);
            edtEmail.setText(email);
            emailView.setVisibility(View.GONE);
        }

        checkShopVisibilityView();
        loadSpinnerData();
        editTextListener();
        seekBarListener();
        radioGroupListener();
        clickableTermsSpan(tvSellerAgreement);

        if (mType.equals(EDIT_SHOP)) {
            setUpShop(shop);
        }
    }

    private void loadSpinnerData() {
        // Business Type Spinner Data
        List<String> typeList = Arrays.asList(getResources().getStringArray(R.array.business_type));
        ArrayAdapter<String> businessAdapter = new ArrayAdapter<>(getContext(),
                R.layout.business_spinner_item, typeList);
        businessAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerBusiness.setAdapter(businessAdapter);

        // Product Type Spinner Data
        ModelManager.modelManager().getProductTypeList(
                (Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<ProductType>> genricResponse) -> {
                    productTypeList = genricResponse.getObject();
                    if(getActivity() != null) {
                        productAdapter = new ProductSpinnerAdapter(getContext(),
                                R.layout.business_spinner_item, productTypeList);
                        spinnerProduct.setAdapter(productAdapter);
                    }
/*
            if(type==1)
                spinnerProduct.setSelection(shop.getProductTypeStore().get(0).getId()-1);
*/
                    setProductRecycler();
                }, (Constants.Status iStatus, String message) -> Log.e(TAG, message));

        spinnerProduct.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                productTypeId = productAdapter.getItem(position).getId();
                edtProductType.setText(productAdapter.getItem(position).getName());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
            }
        });

        // Delivery Time Spinner Data
        ModelManager.modelManager().getDeliveryTimeList(
                (Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<DeliveryType>> genricResponse) -> {
                    if(getContext() != null) {
                        deliveryAdapter = new DeliverySpinnerAdapter(getContext(),
                                R.layout.business_spinner_item, genricResponse.getObject());
                        spinnerDelivery.setAdapter(deliveryAdapter);
                    }
                    if (mType.equals(EDIT_SHOP)) {
                        spinnerDelivery.setSelection(shop.getDeliveryType().getId() - 1);
                    }
                }, (Constants.Status iStatus, String message) -> Log.e(TAG, message));

        spinnerDelivery.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                deliveryTypeId = deliveryAdapter.getItem(position).getId();
                edtDeliveryType.setText(deliveryAdapter.getItem(position).getName());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
            }
        });

        // Bank List Spinner Data
        ModelManager.modelManager().getBankList((Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<BankType>> genricResponse) -> {
            CopyOnWriteArrayList<BankType> bankNameList = genricResponse.getObject();
            if(getContext() != null) {
                bankAdapter = new BankSpinnerAdapter(getContext(),
                        R.layout.business_spinner_item, bankNameList);
                spinnerBankName.setAdapter(bankAdapter);
            }
            if (mType.equals(EDIT_SHOP)) {
                for (int i = 0; i < bankNameList.size(); i++) {
                    if (bankNameList.get(i).getName().equals(shop.getBankDetails().getBankName())) {
                        int spinnerPosition = bankAdapter.getPosition(bankNameList.get(i));
                        spinnerBankName.setSelection(spinnerPosition);
                    }
                }
            }
        }, (Constants.Status iStatus, String message) -> Log.e(TAG, message));

        spinnerBankName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                edtBankName.setText(bankAdapter.getItem(position).getName());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
                if (shop != null) {
                    edtBankName.setText(shop.getBankDetails().getBankName());
                }
            }
        });

    }

    private void setProductRecycler() {
        if(getContext() != null) {
            adapter = new ProductTypeAdapter(getContext(), productTypeList);
            rvProduct.setLayoutManager(new GridLayoutManager(getActivity(), 2));
            rvProduct.setHasFixedSize(false);
            rvProduct.setNestedScrollingEnabled(false);
            rvProduct.addItemDecoration(new GridSpacingItemDecoration(2, Utils.dpToPx(getContext(), 0), true));
            rvProduct.setItemAnimator(new DefaultItemAnimator());
            rvProduct.setAdapter(adapter);
        }
        if (mType.equals(EDIT_SHOP)) {
            List<ProductType> typeList = shop.getProductTypeStore();
            for (int i = 0; i < typeList.size(); i++) {
                for (int j = 0; j < productTypeList.size(); j++) {
                    if (typeList.get(i).getName().equals(productTypeList.get(j).getName())) {
                        productTypeList.get(j).setSelected(true);
                    }
                }
            }
            adapter.notifyDataSetChanged();
        }
    }

    private void editTextListener() {
        // Shop Name Edit Text Listener
        edtShop.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                tvShopName.setText(s);
            }
        });
        // Phone No Edit Text Listener
        edtPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable start) {
                if (start.toString().matches(Utils.PHONE_PATTERN)) {
                    edtPhone.setText("+91 " + start.toString());
                    Selection.setSelection(edtPhone.getText(), edtPhone.getText().length());
                }
            }
        });
    }

    private void seekBarListener() {
        shopRangeSeekBar.setOnProgressChangeListener(new DiscreteSeekBar.OnProgressChangeListener() {
            @Override
            public void onProgressChanged(DiscreteSeekBar discreteSeekBar, int progress, boolean b) {
                shopVisibilityRange = progress;
            }

            @Override
            public void onStartTrackingTouch(DiscreteSeekBar discreteSeekBar) {
            }

            @Override
            public void onStopTrackingTouch(DiscreteSeekBar discreteSeekBar) {
            }
        });
        shopSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                /*int val = (progress * (seekBar.getWidth() - 2 * seekBar.getThumbOffset())) / seekBar.getMax();
                tvShopProgress.setText(String.valueOf(progress+5));
                tvShopProgress.setX(seekBar.getX() + val + seekBar.getThumbOffset());*/
                shopSeekBar.setProgressText(String.valueOf(progress + 5));
                shopVisibilityRange = progress + 5;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        shopLevelSeekBar.getProgressDrawable().setColorFilter(colorRed, PorterDuff.Mode.SRC_IN);
        shopLevelSeekBar.setThumb(getResources().getDrawable(R.drawable.ic_marker_red));
        shopLevelSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int progress = seekBar.getProgress();
                shopVisibilityLevel = setLogicalSeekBar(seekBar, progress);
            }
        });
    }

    public void radioGroupListener() {
        //Account Type RadioGroup
        accountGroup.setOnCheckedChangeListener((group, checkedId) -> {
            RadioButton rb = (RadioButton) group.findViewById(checkedId);
            accountType = rb.getText().toString();
        });

        //Shop Type RadioGroup
        shopGroup.setOnCheckedChangeListener((group, checkedId) -> {
            checkShopVisibilityView();
            RadioButton rb = (RadioButton) group.findViewById(checkedId);
            if (rb.getText().toString().equals(getString(R.string.text_public))) {
                shopType = 2;
            } else {
                shopType = 1;
            }
        });
    }

    private void checkShopVisibilityView() {
        if (publicBtn.isChecked()) {
            shopRangeView.setVisibility(View.VISIBLE);
            shopLevelView.setVisibility(View.GONE);
        } else {
            shopRangeView.setVisibility(View.GONE);
            shopLevelView.setVisibility(View.VISIBLE);
        }
    }

    public void setUpShop(MyShop shop) {
        if (shop != null) {
            String name = shop.getName();
            edtShop.setText(name);
            tvShopName.setText(name);
            String ownerName = shop.getOwnerFirstName() + " " + shop.getOwnerLastName();
            edtOwner.setText(ownerName);
            String location = "";
            if (shop.getAddress() != null) {
                address = shop.getAddress();
                location = shop.getAddress().getStreetAddress() + " , "
                        + shop.getAddress().getCity() + " , "
                        + shop.getAddress().getState() + " , "
                        + shop.getAddress().getCountry();
            }
            edtLocation.setText(location);
            edtEmail.setText(shop.getEmail());
            edtPhone.setText(shop.getPhone());

            if (!shop.getBannerUrl().isEmpty()) {
                Picasso.with(getContext())
                        .load(shop.getBannerUrl())
                        .into(ivProfile);
            }

            if (!shop.getLogoUrl().isEmpty()) {
                Picasso.with(getContext())
                        .load(shop.getLogoUrl())
                        .into(ivLogo);
            }

            switch (shop.getShopType()) {
                case 1:
                    privateBtn.setChecked(true);
                    break;
                case 2:
                    publicBtn.setChecked(true);
                    break;
            }
            shopRangeSeekBar.setProgress(shop.getVisibilityRange());
            //shopSeekBar.setProgress(shop.getVisibilityRange()-5);
            shopVisibilityRange = shop.getVisibilityRange();
            setSeekBar(shopLevelSeekBar, shop.getVisibilityLevel());

            spinnerBusiness.setSelection(shop.getBusinessType() - 1);

            edtPAN.setText(shop.getKycInfo().getPAN());
            edtTAN.setText(shop.getKycInfo().getTAN());
            edtGST.setText(shop.getKycInfo().getGST());

            edtCIN.setText(shop.getCompanyDetails().getCompanyCINNumber());
            edtCompanyAddress.setText(shop.getCompanyDetails().getCompanyAddress());

            edtAccountName.setText(shop.getBankDetails().getAccountHolderName());
            edtAccountNumber.setText(shop.getBankDetails().getAccountNumber());
            edtBankName.setText(shop.getBankDetails().getBankName());
            edtIFSC.setText(shop.getBankDetails().getIFSCCode());
            switch (shop.getBankDetails().getAccountType()) {
                case "Saving":
                    savingBtn.setChecked(true);
                    break;
                case "Current":
                    currentBtn.setChecked(true);
                    break;
            }
        }
    }

    private void setSeekBar(SeekBar seekBar, int status) {
        if (status == 0) {
            seekBar.setProgress(0);
            seekBar.getProgressDrawable().setColorFilter(colorRed, PorterDuff.Mode.SRC_IN);
            seekBar.setThumb(getResources().getDrawable(R.drawable.ic_marker_red));
        } else if (status == 1) {
            seekBar.setProgress(30);
            seekBar.getProgressDrawable().setColorFilter(colorYellow, PorterDuff.Mode.SRC_IN);
            seekBar.setThumb(getResources().getDrawable(R.drawable.ic_marker_yellow));
        } else if (status == 2) {
            seekBar.setProgress(70);
            seekBar.getProgressDrawable().setColorFilter(colorBlue, PorterDuff.Mode.SRC_IN);
            seekBar.setThumb(getResources().getDrawable(R.drawable.ic_marker_blue));
        } else if (status == 3) {
            seekBar.setProgress(120);
            seekBar.getProgressDrawable().setColorFilter(colorGreen, PorterDuff.Mode.SRC_IN);
            seekBar.setThumb(getResources().getDrawable(R.drawable.ic_marker_green));
        }
    }

    private int setLogicalSeekBar(SeekBar seekBar, int value) {
        int pointer = 0;
        if (value <= 10) {
            seekBar.setProgress(0);
            seekBar.getProgressDrawable().setColorFilter(colorRed, PorterDuff.Mode.SRC_IN);
            seekBar.setThumb(getResources().getDrawable(R.drawable.ic_marker_red));
            pointer = 0;
        } else if (value >= 10 && value <= 70) {
            seekBar.setProgress(30);
            seekBar.getProgressDrawable().setColorFilter(colorYellow, PorterDuff.Mode.SRC_IN);
            seekBar.setThumb(getResources().getDrawable(R.drawable.ic_marker_yellow));
            pointer = 1;
        } else if (value >= 70 && value <= 110) {
            seekBar.setProgress(70);
            seekBar.getProgressDrawable().setColorFilter(colorBlue, PorterDuff.Mode.SRC_IN);
            seekBar.setThumb(getResources().getDrawable(R.drawable.ic_marker_blue));
            pointer = 2;
        } else if (value >= 90) {
            seekBar.setProgress(120);
            seekBar.getProgressDrawable().setColorFilter(colorGreen, PorterDuff.Mode.SRC_IN);
            seekBar.setThumb(getResources().getDrawable(R.drawable.ic_marker_green));
            pointer = 3;
        }
        return pointer;
    }

    @OnClick(R.id.btn_back)
    public void onBack() {
        getFragmentManager().popBackStack();
        Utils.hideKeyboard(getContext());
    }

    @OnClick(R.id.ivCreateImage)
    public void pickPhotoCreate() {
        Utils.hideKeyboard(getContext());
        imageType = 1;
        checkPermission();
    }

    @OnClick(R.id.ivLogoImage)
    public void pickLogoCreate() {
        Utils.hideKeyboard(getContext());
        imageType = 2;
        checkPermission();
    }

    @OnClick(R.id.address_btn)
    void onAddAddress() {
        DialogFragment fragment = AddressDialogFragment.newInstance(profileSwitcherListener);
        Bundle bdl = new Bundle();
        bdl.putInt(KSCREENWIDTH, screenWidth);
        bdl.putInt(KSCREENHEIGHT, screenHeight);
        fragment.setArguments(bdl);
        fragment.setTargetFragment(this, ADDRESS_RESULT);
        fragment.show(getChildFragmentManager(), "Dialog Fragment");
    }

    @OnClick(R.id.btn_continue)
    public void onContinue() {
        if (validateData()) {
            if (user.getContactIdType() == 1) {
                if (getProperText(edtEmail).isEmpty()) {
                    edtEmail.setError("Email Id is must");
                    edtEmail.requestFocus();
                } else if (!getProperText(edtEmail).isEmpty() && !Validations.isValidEmail(getProperText(edtEmail))) {
                    edtEmail.setError("Email Id is invalid");
                    edtEmail.requestFocus();
                } else {
                    continueType();
                }
            } else if (user.getContactIdType() == 2) {
                if (getProperText(edtPhone).isEmpty()) {
                    edtPhone.setError("Phone Number is must");
                    edtPhone.requestFocus();
                } else if (!getProperText(edtPhone).isEmpty() && !Validations
                        .isValidPhone(getProperText(edtPhone))) {
                    edtPhone.setError("Phone Number is invalid");
                    edtPhone.requestFocus();
                } else {
                    continueType();
                }
            } else {
                continueType();
            }
        }
    }

    private void continueType() {
        HashMap<String, Object> shopMap = getShopMap(user);

        if (!mType.equals(EDIT_SHOP)) {
            if (contactType.isEmpty()) {
                createShop(shopMap);
            } else if (contactType.contains("+91")) {
                checkUserRegistration(Constants.ContactIDType.EMAIL, getProperText(edtEmail), shopMap);
            } else {
                checkUserRegistration(Constants.ContactIDType.PHONE, getProperText(edtPhone), shopMap);

            }
        } else {
            shopMap.put(kShopId, shop.getId());
            editShop(shopMap);
        }
    }

    private void checkUserRegistration(Constants.ContactIDType contactIDType, String contactId, HashMap<String, Object> shopMap) {
        showProgress(true);
        ModelManager.modelManager().checkRegistration(contactId, (Constants.Status iStatus, GenricResponse<HashMap<String, Object>> genericResponse) -> {
            HashMap<String, Object> hashMap = genericResponse.getObject();
            Integer isAlreadyRegistered = (Integer) hashMap.get(kIsAlreadyRegistered);
            boolean isAlreadyRegister = isAlreadyRegistered == 1;
            if (isAlreadyRegister) {
                showProgress(false);
                String contactxt = (contactIDType.equals(Constants.ContactIDType.EMAIL))?"Email":"Phone";
                Utils.showAlertDialog(getContext(), contactxt+" Already Registered!", "Please try with another "+contactxt);
            } else {
                verifyContact(contactIDType,contactId,shopMap);
            }
        }, (Constants.Status iStatus, String message) -> {
            Log.v(getClass().getSimpleName(), message);
            Toaster.toast(message);
            showProgress(false);
        });
    }


    private boolean validateData() {
        boolean isOk = true;
        if (getProperText(edtShop).isEmpty()) {
            edtShop.setError("Shop Name is must");
            edtShop.requestFocus();
            isOk = false;
        } else if (address == null || getProperText(edtLocation).isEmpty()) {
            Toaster.customToast("Shop address is mandatory while creating shop");
            isOk = false;
        } else if (!getProperText(edtPAN).isEmpty() && !Validations.isValidPAN(getProperText(edtPAN))) {
            edtPAN.setError("PAN is invalid");
            edtPAN.requestFocus();
            isOk = false;
        } else if (getProperText(edtPAN).isEmpty()) {
            edtPAN.setError("PAN is invalid");
            edtPAN.requestFocus();
            isOk = false;
        } else if (getProperText(edtTAN).isEmpty()) {
            edtTAN.setError("TAN is invalid");
            edtTAN.requestFocus();
            isOk = false;
        } else if (getProperText(edtGST).isEmpty()) {
            edtGST.setError("GST is invalid");
            edtGST.requestFocus();
            isOk = false;
        } else if (!getProperText(edtGST).isEmpty()
                && !Validations.isAlphaNumeric(getProperText(edtGST))
                && getProperText(edtGST).length() < 15) {
            edtGST.setError("GST is invalid");
            edtGST.requestFocus();
            isOk = false;
        } else if (getProperText(edtCIN).isEmpty()) {
            edtCIN.setError("CIN is invalid");
            edtCIN.requestFocus();
            isOk = false;
        } else if (!getProperText(edtCIN).isEmpty()
                && !Validations.isAlphaNumeric(getProperText(edtCIN))
                && getProperText(edtCIN).length() < 21) {
            edtCIN.setError("CIN is invalid");
            edtCIN.requestFocus();
            isOk = false;
        } else if (getProperText(edtCompanyAddress).isEmpty()) {
            edtCompanyAddress.setError("Company Address is must");
            edtCompanyAddress.requestFocus();
            isOk = false;
        } else if (getProperText(edtAccountName).isEmpty()) {
            edtAccountName.setError("Account Name is must");
            edtAccountName.requestFocus();
            isOk = false;
        } else if (getProperText(edtAccountNumber).isEmpty()) {
            edtAccountNumber.setError("Account Number is must");
            edtAccountNumber.requestFocus();
            isOk = false;
        } else if (getProperText(edtBankName).isEmpty()) {
            edtBankName.setError("Bank Name is must");
            edtBankName.requestFocus();
            isOk = false;
        } else if (!getProperText(edtIFSC).isEmpty() && !Validations.isValidIFSC(getProperText(edtIFSC))) {
            edtIFSC.setError("IFSC Code is invalid");
            edtIFSC.requestFocus();
            isOk = false;
        } else if (getProperText(edtIFSC).isEmpty()) {
            edtIFSC.setError("IFSC Code is must");
            edtIFSC.requestFocus();
            isOk = false;
        } else if (adapter.getProductList().isEmpty()) {
            Toaster.customToast("Please select any product type");
            isOk = false;
        }
        return isOk;

    }

    private HashMap<String, Object> getShopMap(CurrentUser user) {
        String ownerFirstName = user.getFirstName();
        String ownerLastName = user.getLastName();
        HashMap<String, Object> kycMap = new HashMap<>();
        kycMap.put(kPAN, getProperText(edtPAN));
        kycMap.put(kTAN, getProperText(edtTAN));
        kycMap.put(kGST, getProperText(edtGST));
        HashMap<String, Object> companyMap = new HashMap<>();
        companyMap.put(kCompanyCinNumber, getProperText(edtCIN));
        companyMap.put(kCompanyRegisteredAddress, getProperText(edtCompanyAddress));
        HashMap<String, Object> bankMap = new HashMap<>();
        bankMap.put(kAccountHolderName, getProperText(edtAccountName));
        bankMap.put(kAccountNumber, getProperText(edtAccountNumber));
        bankMap.put(kBankName, getProperText(edtBankName));
        bankMap.put(kIFSC, getProperText(edtIFSC));
        bankMap.put(kAccountType, accountType);

        HashMap<String, Object> shopMap = new HashMap<>();
        shopMap.put(kOwnerFirstName, ownerFirstName);
        shopMap.put(kOwnerLastName, ownerLastName);
        shopMap.put(kShopType, shopType);
        shopMap.put(kVisibilityRange, shopVisibilityRange);
        shopMap.put(kVisibilityLevel, shopVisibilityLevel);
        shopMap.put(kShopName, getProperText(edtShop));
        shopMap.put(kEmail, getProperText(edtEmail));
        shopMap.put(kPhone, getProperText(edtPhone));
        shopMap.put(kBusinessType, getBusinessType(spinnerBusiness.getSelectedItem().toString()));
        shopMap.put(kProductTypeId, generateJsonArrayFromProductList(adapter.getProductList()));
        shopMap.put(kDeliveryTime, deliveryTypeId);
        shopMap.put(kKycInfo, kycMap);
        shopMap.put(kBankDetails, bankMap);
        shopMap.put(kCompanyRegistrationDetail, companyMap);
        shopMap.put(kPAN, getProperText(edtPAN));
        shopMap.put(kTAN, getProperText(edtTAN));
        shopMap.put(kGST, getProperText(edtGST));
        shopMap.put(kAccountHolderName, getProperText(edtAccountName));
        shopMap.put(kAccountNumber, getProperText(edtAccountNumber));
        shopMap.put(kBankName, getProperText(edtBankName));
        shopMap.put(kIFSC, getProperText(edtIFSC));
        shopMap.put(kAccountType, accountType);
        shopMap.put(kCompanyCinNumber, getProperText(edtCIN));
        shopMap.put(kCompanyRegisteredAddress, getProperText(edtCompanyAddress));
        shopMap.put(kAddressId, address.getAddressId());
        if (!mImagePath.isEmpty()) {
            shopMap.put(kShopBannerImage, new File(mImagePath));
        }
        if (!mLogoPath.isEmpty()) {
            shopMap.put(kShopLogoImage, new File(mLogoPath));
        }

        return shopMap;
    }


    //1 for phone and 2 for email
    private void verifyContact(Constants.ContactIDType contactIDType, String contactId, HashMap<String, Object> userMap) {
        ModelManager.modelManager().contactVerification(contactId, (Constants.Status iStatus, GenricResponse<HashMap<String, Object>> genericResponse) -> {
            showProgress(false);
            HashMap<String, Object> hashMap = genericResponse.getObject();
            String OTP = (String) hashMap.get(kOTP);
            if (profileSwitcherListener != null) {
                profileSwitcherListener.switchFragment(ShopOTPFragment.newInstance(contactIDType, mType, profileSwitcherListener, OTP, contactId, userMap), false, true);
            }
        }, (Constants.Status iStatus, String message) -> {
            Log.e(TAG, message);
            Toaster.toast(message);
            showProgress(false);
        });
    }

    private void createShop(HashMap<String, Object> map) {
        showProgress(true);
        ModelManager.modelManager().createNewShop(map, (Constants.Status iStatus, GenricResponse<Shop> genricResponse) -> {
            showProgress(false);
            onBack();
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Toaster.toast(message);
        });
    }

    private void editShop(HashMap<String, Object> map) {
        showProgress(true);
        ModelManager.modelManager().editShop(map, (Constants.Status iStatus, GenricResponse<Shop> genricResponse) -> {
            showProgress(false);
            onBack();
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Toaster.toast(message);
        });
    }

    private void checkPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(), CAMERA) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(getActivity(), READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            //akaImageUtil.pickPhoto();
            openBottomSheetBanner();
        } else if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), CAMERA)
                && ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), READ_EXTERNAL_STORAGE)) {
            // We've been denied once before. Explain why we need the permission, then ask again.
            Utils.showDialog(getContext(),
                    "Camera & Gallary permissions are required to upload profile images!", "Ask Permission",
                    "Discard", (dialog, which) -> {
                        if (which == -1) {
                            requestPermissions(new String[]{CAMERA, READ_EXTERNAL_STORAGE},
                                    REQUEST_PERMISSION_CAMERA_STORAGE);
                        } else {
                            dialog.dismiss();
                        }
                    });
        } else {
            // We've never asked. Just do it.
            requestPermissions(new String[]{CAMERA, READ_EXTERNAL_STORAGE},
                    REQUEST_PERMISSION_CAMERA_STORAGE);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_PERMISSION_CAMERA_STORAGE
                && grantResults[0] == PackageManager.PERMISSION_GRANTED
                && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            //akaImageUtil.pickPhoto();
            openBottomSheetBanner();
        } else {
            // We were not granted permission this time, so don't try to show the contact picker
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case PHOTO_RESULT:
                if (resultCode == RESULT_OK && data != null) {
                    akaImageUtil.onActivityResult(requestCode, resultCode, data);
                }
                break;
            case ADDRESS_RESULT:
                if (resultCode == RESULT_OK && data != null) {
                    address = (Address) data.getSerializableExtra("data");
                    edtLocation.setText(address.getStreetAddress().concat(",")
                            .concat(address.getCity()).concat(",")
                            .concat(address.getState()).concat(",")
                            .concat(address.getCountry()));
                }
                break;
            case PIC_GALLARY:
                if (resultCode == RESULT_OK && data != null) {
                    Uri selectedImage = data.getData();
                    cropImageBottomSheet(selectedImage);
                }
                break;
            case PIC_CAMERA:
                if (resultCode == RESULT_OK && data != null) {
                    Bitmap bitmap = (Bitmap) data.getExtras().get(kData);
                    Uri uri = ImageController.getImageUri(getContext(), bitmap);
                    cropImageBottomSheet(uri);
                }
                break;
            case PIC_CROP:
                if (resultCode == RESULT_OK && data != null) {
                    Uri uri = data.getData();
                    if (uri != null) {
                        try {
                            File file = new File(uri.getPath());
                            if (imageType == 1) {
                                mImagePath = file.getAbsolutePath();
                                Picasso.with(getContext())
                                        .load(file)
                                        .into(ivProfile);
                            } else {
                                mLogoPath = file.getAbsolutePath();
                                Picasso.with(getContext())
                                        .load(file)
                                        .into(ivLogo);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;

        }
    }

    public void cropImageBottomSheet(Uri uri) {
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_crop_image, null);
        final Dialog mBottomSheetDialog = new Dialog(getContext(), R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.show();

        view.findViewById(R.id.original_view).setOnClickListener(v -> {
            if (uri != null) {
                try {
                    File file = new File(Utils.getRealPathFromURI(getContext(), uri));
                    if (imageType == 1) {
                        mImagePath = file.getAbsolutePath();
                        Picasso.with(getContext())
                                .load(file)
                                .into(ivProfile);
                    } else {
                        mLogoPath = file.getAbsolutePath();
                        Picasso.with(getContext())
                                .load(file)
                                .into(ivLogo);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            mBottomSheetDialog.dismiss();
        });
        view.findViewById(R.id.crop_view).setOnClickListener(v -> {
            File outputFile = ImageEditor.getInstance().cropImage(this, uri, PIC_CROP);
            Log.e(TAG, outputFile.getAbsolutePath());
            mBottomSheetDialog.dismiss();
        });
    }

    public void openBottomSheetBanner() {
        View view = getActivity().getLayoutInflater().inflate(R.layout.custom_bottonsheet_layout, null);
        final Dialog mBottomSheetDialog = new Dialog(getContext(), R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.show();

        view.findViewById(R.id.camera_view).setOnClickListener(v -> {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, PIC_CAMERA);
            mBottomSheetDialog.dismiss();
        });

        view.findViewById(R.id.gallery_view).setOnClickListener(v -> {
            Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(gallery, PIC_GALLARY);
            mBottomSheetDialog.dismiss();
        });

        view.findViewById(R.id.cancel_view).setOnClickListener(v -> mBottomSheetDialog.dismiss());
    }


    @Override
    public void onImageLoadFailed(int requestCode) {
        Toaster.toast("Failed to load image, Please try again!");
    }

    @Override
    public void onImageLoadSuccess(Bitmap bitmap, Uri imageUri) {
        if (imageType == 1) {
            ivProfile.setImageBitmap(bitmap);
            try {
                //means its a camera image
                String path = imageUri.getPath();
                File originalFile = new File(path);
                Log.v("File size : ", "original " + originalFile.length());
                File compressedFile = new Compressor(getContext()).compressToFile(originalFile);
                Log.v("File size : ", "compressed " + compressedFile.length());
                mImagePath = compressedFile.getAbsolutePath();
            } catch (Exception e) {
                //means its a gallary image
                String path = Utils.getPathForMediaUri(getContext(), imageUri);
                try {
                    File originalFile = new File(path);
                    File compressedFile = new Compressor(getContext()).compressToFile(originalFile);
                    mImagePath = compressedFile.getAbsolutePath();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        } else if (imageType == 2) {
            ivLogo.setImageBitmap(bitmap);
            try {
                //means its a camera image
                String path = imageUri.getPath();
                File originalFile = new File(path);
                Log.v("File size : ", "original " + originalFile.length());
                File compressedFile = new Compressor(getContext()).compressToFile(originalFile);
                Log.v("File size : ", "compressed " + compressedFile.length());
                mLogoPath = compressedFile.getAbsolutePath();
            } catch (Exception e) {
                //means its a gallary image
                String path = Utils.getPathForMediaUri(getContext(), imageUri);
                try {
                    File originalFile = new File(path);
                    File compressedFile = new Compressor(getContext()).compressToFile(originalFile);
                    mLogoPath = compressedFile.getAbsolutePath();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        }

    }

    ClickableSpan sellerSapnListener = new ClickableSpan() {
        @Override
        public void onClick(View widget) {
            showDialog(PolicyDialogFragment.newInstance(3), false);
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setUnderlineText(false);
            ds.setColor(getResources().getColor(R.color.link_color));
        }
    };


    private void clickableTermsSpan(TextView mTextViewTerms) {
        SpannableStringBuilder ssb = new SpannableStringBuilder();
        String termsText = getString(R.string.create_seller_agreement);
        ssb.append(termsText);
        int pStart = termsText.indexOf('T');
        int pEnd = pStart + 18;

        ssb.setSpan(sellerSapnListener, pStart, pEnd, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mTextViewTerms.setText(ssb);
        mTextViewTerms.setMovementMethod(LinkMovementMethod.getInstance());
    }

    /**
     * takes a dialog frament and boolean and shows it
     */
    void showDialog(DialogFragment dialogFragment, boolean saveInBackstack) {
        String backStateName = dialogFragment.getClass().getName();
        FragmentManager fragmentManager = getChildFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();

        if (saveInBackstack) {
            ft.addToBackStack(backStateName);
        }
        dialogFragment.show(fragmentManager, backStateName);
    }

    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) {
                progressDialog.cancel();
            }
        }
    }
}
