package com.app.inn.peepsin.Models;

import android.util.Log;

import org.json.JSONObject;

/**
 * Created by akamahesh on 27/4/17.
 */

public class Requests extends BaseModel{

    private Integer userId;
    private String  firstName;
    private String  lastName;
    private String  contactId;
    private String  profilePicURL;
    private Integer profileVisibility;
    private Integer mode;

    public Requests(JSONObject jsonResponse){
        this.userId              = getValue(jsonResponse, kUserId,              Integer.class);
        try {
            this.contactId           = getValue(jsonResponse, kContactId,           String.class);
        }catch (Exception e){
            Log.e(getClass().getSimpleName(),e.getMessage());
            contactId = kEmptyString;
        }

        this.profileVisibility   = getValue(jsonResponse, kProfileVisibility,   Integer.class);
        this.mode                = getValue(jsonResponse, kRequestSentMode,     Integer.class);
        this.firstName           = getValue(jsonResponse, kFirstName,           String.class);
        this.lastName            = getValue(jsonResponse, kLastName,            String.class);
        this.profilePicURL       = getValue(jsonResponse, kProfilePicUrl,       String.class);
    }


    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getFullName(){
        return firstName+" "+lastName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public String getProfilePicURL() {
        return profilePicURL;
    }

    public void setProfilePicURL(String profilePicURL) {
        this.profilePicURL = profilePicURL;
    }


    public Integer getProfileVisibility() {
        return profileVisibility;
    }

    public void setProfileVisibility(Integer profileVisibility) {
        this.profileVisibility = profileVisibility;
    }

    public Integer getMode() {
        return mode;
    }

    public void setMode(Integer mode) {
        this.mode = mode;
    }
}
