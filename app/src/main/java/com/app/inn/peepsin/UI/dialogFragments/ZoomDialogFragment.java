package com.app.inn.peepsin.UI.dialogFragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.helpers.FixedViewPager;
import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kImageUrl;
import static com.app.inn.peepsin.Constants.Constants.kRecords;

/**
 * Created by akamahesh on 3/5/17.
 */

public class ZoomDialogFragment extends DialogFragment{
    @BindView(R.id.view_pager_images)
    FixedViewPager viewPager;
    private ArrayList<String> imageurls;

    public static DialogFragment newInstance(ArrayList<String> urls) {
        DialogFragment fragment = new ZoomDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putStringArrayList(kRecords,urls);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if(bundle!=null)
            imageurls = bundle.getStringArrayList(kRecords);
        setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Black_NoTitleBar);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_frament_zoom, container, false);
        ButterKnife.bind(this,view);
        viewPager.setAdapter(new SectionsPagerAdapter(getChildFragmentManager()));
        return view;
    }

    @OnClick(R.id.iv_close)
    void dismissDialog(){
        dismiss();
    }


    public static class PlaceholderFragment extends Fragment {

        @BindView(R.id.iv_product_image)
        PhotoView ivProductImage;
        String imageURL;

        public PlaceholderFragment() {
        }

        public static PlaceholderFragment newInstance(String imageurl) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putString(kImageUrl, imageurl);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            Bundle bundle = getArguments();
            if(bundle!=null)
                imageURL = bundle.getString(kImageUrl);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_zoom, container, false);
            ButterKnife.bind(this, rootView);

            Picasso.with(getContext())
                    .load(imageURL)
                    .placeholder(getResources().getDrawable(R.drawable.img_product_placeholder))
                    .error(getResources().getDrawable(R.drawable.img_product_placeholder))
                    .into(ivProductImage);

            return rootView;
        }
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return PlaceholderFragment.newInstance(imageurls.get(position));
        }

        @Override
        public int getCount() {
            return imageurls.size();
        }
    }


}
