package com.app.inn.peepsin.UI.fragments.Permissions;


import static com.app.inn.peepsin.Constants.Constants.kAuthToken;
import static com.app.inn.peepsin.Constants.Constants.kDescription;
import static com.app.inn.peepsin.Constants.Constants.kId;
import static com.app.inn.peepsin.Constants.Constants.kLatitude;
import static com.app.inn.peepsin.Constants.Constants.kLongitude;
import static com.app.inn.peepsin.Constants.Constants.kResults;
import static com.app.inn.peepsin.Constants.Constants.kStatus;
import static com.app.inn.peepsin.Constants.Constants.kStreetAddress;
import static com.app.inn.peepsin.Constants.Constants.kType;
import static com.facebook.login.widget.ProfilePictureView.TAG;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.APIManager.APIManager;
import com.app.inn.peepsin.Managers.APIManager.APIRequestHelper;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.CurrentUser;
import com.app.inn.peepsin.Models.GeoAddress;
import com.app.inn.peepsin.Models.ShopModel;
import com.app.inn.peepsin.Models.UserLocation;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.activities.AppPermissionActivity;
import com.app.inn.peepsin.UI.activities.HomeActivity;
import com.app.inn.peepsin.UI.activities.IntermediateActivity;
import com.app.inn.peepsin.UI.helpers.DividerItemRecyclerDecoration;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.google.gson.JsonObject;
import com.transitionseverywhere.ArcMotion;
import com.transitionseverywhere.ChangeBounds;
import com.transitionseverywhere.TransitionManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ManualAddressFragment extends Fragment {

  AppPermissionActivity.Type type;
  private ProgressDialog progressDialog;
  @BindView(R.id.btn_use_address)
  Button btnUseAddress;
  @BindView(R.id.et_address)
  EditText etAddress;
  @BindView(R.id.et_street_address)
  EditText etStreetAddress;

  @BindView(R.id.tv_location)
  TextView tvLocation;

  @BindView(R.id.recycler_view_address)
  RecyclerView recyclerView;
  @BindView(R.id.root_view_group)
  ViewGroup viewGroup;

  List<HashMap<String, String>> hashMaps;
  private AddressAdapter addressAdapter;

  public static Fragment newInstance(AppPermissionActivity.Type type) {
    Fragment fragment = new ManualAddressFragment();
    Bundle bundle = new Bundle();
    bundle.putSerializable(kType, type);
    fragment.setArguments(bundle);
    return fragment;
  }

  public ManualAddressFragment() {
    // Required empty public constructor
  }


  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    type = (AppPermissionActivity.Type) getArguments().getSerializable(kType);
    progressDialog = Utils.generateProgressDialog(getContext(), false);
    hashMaps = new ArrayList<>();
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_manual_address, container, false);
    ButterKnife.bind(this, view);
    addressAdapter = new AddressAdapter(hashMaps);
    recyclerView.setHasFixedSize(true);
    LayoutManager layoutManager = new LinearLayoutManager(getContext());
    recyclerView.setLayoutManager(layoutManager);
    recyclerView.setAdapter(addressAdapter);
    RecyclerView.ItemDecoration itemDecoration = new DividerItemRecyclerDecoration(getContext(),
        R.drawable.canvas_recycler_divider);
    //recyclerView.addItemDecoration(new ItemOffsetDecoration(getContext(), R.dimen.item_offset));
    recyclerView.addItemDecoration(itemDecoration);
    recyclerView.setAdapter(addressAdapter);
    etAddress.addTextChangedListener(textWatcher);
    btnUseAddress.setTag(1);
    return view;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    Utils.showKeyboard(getContext(), etAddress);
  }

  TextWatcher textWatcher = new TextWatcher() {
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
      String input = s.toString().trim();
      if (input.length() > 14) {
        return;
      }
      if (!input.isEmpty()) {
        requestplaces(input);
      }
    }
  };

  private void updateLocation(HashMap<String, Object> hashMap) {
    String streetAddress = Utils.getProperText(etStreetAddress);
    hashMap.put(kStreetAddress, streetAddress);
    CurrentUser currentUser = ModelManager.modelManager().getCurrentUser();
    String authToken = currentUser.getAuthToken();
    hashMap.put(kAuthToken, authToken);
    showProgress(true);
    ModelManager.modelManager().updateUserLocation(hashMap, (Constants.Status iStatus) -> {
      UserLocation userLocation = ModelManager.modelManager().getCurrentUser().getUserLocation();
      String lat = "28.5355";
      String lng = "77.3910";
      if(userLocation!=null){
        lat = userLocation.getLatitude();
        lng = userLocation.getLongitude();
      }
      getFeaturedShops(lat,lng);
    }, (Constants.Status iStatus, String message) -> {
      Utils.showAlertDialog(getContext(), "Alert", message);
      showProgress(false);
    });


  }

  private void getFeaturedShops(String lat,String lng){
    ModelManager.modelManager().getFeatureShopList(1,  lat,lng ,
            (Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<ShopModel>> genericResponse) -> {
              getShopList(lat,lng);
            }, (Constants.Status iStatus, String message) -> {
              getShopList(lat,lng);
              Log.e(TAG, message);
            });
  }

  public void getShopList(String lat, String lng){
    HashMap<String ,Object> map = new HashMap<>();
    map.put(kLatitude,lat);
    map.put(kLongitude,lng);
    ModelManager.modelManager().getShopList(map,1,
            (Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<ShopModel>> genericResponse) -> {
              showProgress(false);
              startActivity(IntermediateActivity.getIntent(getContext()));
            }, (Constants.Status iStatus, String message) -> {
              showProgress(false);
              startActivity(IntermediateActivity.getIntent(getContext()));
              Log.e(TAG, message);
            });
  }

  @OnClick(R.id.btn_use_address)
  void onUseAddress() {
    if (btnUseAddress.getTag().equals(1)) {
      //first time
      startPathAnimation();
    } else {
      String streetAddress = Utils.getProperText(etStreetAddress);
      if (streetAddress.isEmpty()) {
        Toaster.kalaToast("Enter valid Street Address");
        return;
      }
      requestAddress(streetAddress);
    }
  }

  @OnClick(R.id.tv_location)
  void onLocation() {
    resetUI();
  }

  private void resetUI() {
    btnUseAddress.setTag(1);
    etStreetAddress.setVisibility(View.INVISIBLE);
    TransitionManager.beginDelayedTransition(viewGroup,
        new ChangeBounds().setPathMotion(new ArcMotion()).setDuration(800));

    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) tvLocation
        .getLayoutParams();
    params.addRule(RelativeLayout.ABOVE, R.id.btn_use_address);
    params.removeRule(RelativeLayout.BELOW);
    params.removeRule(RelativeLayout.CENTER_HORIZONTAL);
    tvLocation.setLayoutParams(params);
    tvLocation.requestLayout();
    etAddress.setVisibility(View.VISIBLE);
    etAddress.requestFocus();
    etStreetAddress.setText("");
    tvLocation.setVisibility(View.INVISIBLE);
  }

  public void requestAddress(String placeID) {
    Retrofit retrofit = new Retrofit.Builder()
        .baseUrl(APIManager.kGoogleMapsBaseURL)
        .addConverterFactory(GsonConverterFactory.create())
        .build();

    APIRequestHelper service = retrofit.create(APIRequestHelper.class);
    showProgress(true);

    Utils.hideKeyboard(getContext());
    service.getCordinates(placeID, "AIzaSyA6kQor07K5xtlcb0PjiuPkLJomPobRxeI")
        .enqueue(new Callback<JsonObject>() {
          @Override
          public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
            try {
              JsonObject jsonObject = response.body();
              JSONObject jsonResponse = new JSONObject(jsonObject.toString());
              JSONArray jsonResult = jsonResponse.getJSONArray(kResults);
              GeoAddress geoAddress = new GeoAddress(jsonResult.getJSONObject(0));
              HashMap<String, Object> addressMap = Utils.handlePlacesJSON(geoAddress);
              updateLocation(addressMap);
            } catch (JSONException e) {
              e.printStackTrace();
              Toaster.kalaToast("Enter valid Street Address");
              showProgress(false);
            }
          }

          @Override
          public void onFailure(Call<JsonObject> call, Throwable t) {
            Log.i(TAG, t.toString());
            showProgress(false);
          }
        });
  }

  private void showProgress(boolean b) {
    if (b) {
      progressDialog.show();
    } else {
      if (progressDialog != null) {
        progressDialog.dismiss();
      }
    }
  }

  private void startPathAnimation() {
    btnUseAddress.setTag(2);
    etAddress.setVisibility(View.INVISIBLE);
    etStreetAddress.setVisibility(View.VISIBLE);
    etStreetAddress.requestFocus();
    tvLocation.setVisibility(View.VISIBLE);
    tvLocation.setText(Utils.getProperText(etAddress));
    TransitionManager.beginDelayedTransition(viewGroup,
        new ChangeBounds().setPathMotion(new ArcMotion()).setDuration(800));

    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) tvLocation
        .getLayoutParams();
    params.removeRule(RelativeLayout.ABOVE);
    params.addRule(RelativeLayout.BELOW, R.id.text);
    params.addRule(RelativeLayout.CENTER_HORIZONTAL);
    tvLocation.setLayoutParams(params);
    tvLocation.requestLayout();
  }

  //https://maps.googleapis.com/maps/api/place/details/json?placeid=ChIJ9xeBIK_6DDkRPW6lI9ppQZs&key=AIzaSyA6kQor07K5xtlcb0PjiuPkLJomPobRxeI
  public void requestplaces(String input) {
    Retrofit retrofit = new Retrofit.Builder()
        .baseUrl(APIManager.kGoogleMapsBaseURL)
        .addConverterFactory(GsonConverterFactory.create())
        .build();

    APIRequestHelper service = retrofit.create(APIRequestHelper.class);
    service.getPlaces(input, getString(R.string.google_api_key))
        .enqueue(new Callback<JsonObject>() {
          @Override
          public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
            try {
              List<HashMap<String, String>> maps = new ArrayList<>();
              JsonObject jsonObject = response.body();
              JSONObject jsonResponse = new JSONObject(jsonObject.toString());
              String status = jsonResponse.getString(kStatus);
              if (status.equals("OK")) {
                JSONArray jsonArray = jsonResponse.getJSONArray("predictions");
                hashMaps.clear();

                for (int i = 0; i < jsonArray.length(); i++) {
                  JSONObject jO = jsonArray.getJSONObject(i);
                  HashMap<String, String> addressmap = new HashMap<>();
                  String description = jO.getString(kDescription);
                  String id = jO.getString("place_id");
                  addressmap.put(kDescription, description);
                  addressmap.put(kId, id);
                  maps.add(addressmap);
                }
                hashMaps.addAll(maps);
                addressAdapter.notifyDataSetChanged();
                Log.i(TAG, jsonResponse.toString(4) + " " + hashMaps.size());
              }
            } catch (JSONException e) {
              e.printStackTrace();
            }
          }

          @Override
          public void onFailure(Call<JsonObject> call, Throwable t) {
            Log.i(TAG, t.toString());
          }
        });
  }


  class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.ViewHolder> {

    List<HashMap<String, String>> list;

    AddressAdapter(List<HashMap<String, String>> list) {
      this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      View layoutView = LayoutInflater.from(parent.getContext())
          .inflate(R.layout.row_address_layout, parent, false);
      return new ViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
      HashMap<String, String> address = list.get(position);
      holder.bindView(address);
      String id = address.get(kId);
      String addressText = address.get(kDescription);
      holder.tvAddress.setTag(id);
      holder.tvAddress.setText(addressText);
    }

    @Override
    public int getItemCount() {
      return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

      HashMap<String, String> map;
      @BindView(R.id.tv_address)
      TextView tvAddress;

      public ViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
      }

      @OnClick(R.id.tv_address)
      void onAddress() {
        String address = map.get(kDescription);
        String id = map.get(kId);
        etAddress.setText("");
        etAddress.setText(address);
        etAddress.setTag(id);
        etAddress.setSelection(etAddress.getText().length());
        hashMaps.clear();
        notifyDataSetChanged();
      }

      void bindView(HashMap<String, String> addressMap) {
        this.map = addressMap;
      }
    }
  }

}
