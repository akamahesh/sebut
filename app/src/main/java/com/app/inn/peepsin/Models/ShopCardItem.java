package com.app.inn.peepsin.Models;

/**
 * Created by dhruv on 30/8/17.
 */

public class ShopCardItem {

    private String mTitle;
    private String mAddress;

    public ShopCardItem(String title, String address) {
        mAddress = address;
        mTitle = title;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getAddress() {
        return mAddress;
    }

}
