package com.app.inn.peepsin.UI.fragments.Connections;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.activities.HomeActivity;
import com.app.inn.peepsin.UI.helpers.CustomViewPager;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kRecords;

public class ConnectionProfileFragment extends Fragment implements TabLayout.OnTabSelectedListener{

    private static Switcher switcherListener;
    @BindView(R.id.tv_title) TextView toolbarTitle;
    @BindView(R.id.view_pager_connection) CustomViewPager viewPager;
    @BindView(R.id.tabs_connection) TabLayout tabLayout;
    @BindColor(R.color.navigation_active_tab) int activeColor;
    @BindColor(R.color.navigation_inactive_tab) int inactiveColor;
    @BindColor(R.color.tab_request_color) int requestColor;
    @BindColor(R.color.tab_invitation_color) int invitationColor;
    @BindView(R.id.ib_back)
    ImageButton ibBack;
    @BindView(R.id.btn_cart)
    ImageButton btnCart;
    @BindView(R.id.tv_cartCount)
    TextView tvCartCount;
    AppBarLayout appbarLayout;
    @BindView(R.id.toolbar_layout) View toolbar_layout;


    private int[] activeImageResId = {
            R.drawable.ic_connections,
            R.drawable.ic_request_active,
            R.drawable.ic_eye_active};

    private int[] inactiveImageResId = {
            R.drawable.ic_connections_inactive,
            R.drawable.ic_requests_inactive,
            R.drawable.ic_eye_inactive};

    private int [] tabTitle = {
            R.string.tab_my_connections,
            R.string.tab_requests,
            R.string.tab_invitations};

    public ConnectionProfileFragment() {
    }


    public static ConnectionProfileFragment newInstance(Switcher switcher) {
        ConnectionProfileFragment fragment = new ConnectionProfileFragment();
        switcherListener = switcher;
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_connection, container, false);
        ButterKnife.bind(this,view);
        toolbar_layout.setVisibility(View.VISIBLE);
       // appbarLayout=((HomeActivity) getActivity()).getTabLayout();
        //Utils.isprofileClicked=1;
        toolbarTitle.setText(getString(R.string.title_connections));
        ibBack.setVisibility(View.VISIBLE);
        btnCart.setVisibility(View.GONE);
        tvCartCount.setVisibility(View.GONE);
        viewPager.setPagingEnabled(false);
        viewPager.setOffscreenPageLimit(1);
        setupViewPager(viewPager);

        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons(tabLayout);
        tabLayout.addOnTabSelectedListener(this);

        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(outState==null)
            return;
        outState.putString(kRecords, "records saved");

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState==null)
            return;
        String status = savedInstanceState.getString(kRecords);
        Toaster.toast(status);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewPager.setCurrentItem(0);
    }


    @Override
    public void onDetach() {
        super.onDetach();
        switcherListener=null;
    }

    public void setupViewPager(ViewPager viewPager) {
        SectionsPagerAdapter adapter = new SectionsPagerAdapter(getChildFragmentManager());
        adapter.addFragment(ProfileMyConnectionFragment.newInstance(switcherListener,4));
        adapter.addFragment(ConnectionProfileRequestsFragment.newInstance(switcherListener,4));
        adapter.addFragment(ConnectionInvitationsFragment.newInstance(switcherListener,4));
        viewPager.setAdapter(adapter);
    }

    public void setupTabIcons(TabLayout upTabIcons) {
        tabLayout.getTabAt(0).setCustomView(R.layout.tab_my_connections);
        tabLayout.getTabAt(1).setCustomView(R.layout.tab_requests);
        tabLayout.getTabAt(2).setCustomView(R.layout.tab_invitations);
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        int position = tab.getPosition();
        Utils.hideKeyboard(getContext());
        View view = tabLayout.getTabAt(position).getCustomView();
        TextView tabText = (TextView) view.findViewById(R.id.tv_tab);
        if(position == 1){
            tabText.setTextColor(requestColor);
            tabLayout.setSelectedTabIndicatorColor(requestColor);
        }else if(position ==2){
            tabText.setTextColor(invitationColor);
            tabLayout.setSelectedTabIndicatorColor(invitationColor);
        }else {
            tabText.setTextColor(activeColor);
            tabLayout.setSelectedTabIndicatorColor(activeColor);
        }
        ImageView tabImage = (ImageView) view.findViewById(R.id.iv_tab);
        tabImage.setImageResource(activeImageResId[position]);
    }
    @OnClick(R.id.ib_back)
    void onBack() {
        getFragmentManager().popBackStack();
       // appbarLayout.setVisibility(View.VISIBLE);
       // Utils.isprofileClicked=0;
    }
    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
        int position = tab.getPosition();
        View view = tabLayout.getTabAt(position).getCustomView();
        TextView tabText = (TextView) view.findViewById(R.id.tv_tab);
        tabText.setTextColor(inactiveColor);
        ImageView tabImage = (ImageView) view.findViewById(R.id.iv_tab);
        tabImage.setImageResource(inactiveImageResId[position]);
    }
    @Override
    public void onResume() {
        super.onResume();
//        appbarLayout.setVisibility(View.GONE);

    }
    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }


    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();

        SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFragment(Fragment fragment) {
            mFragmentList.add(fragment);
        }
    }

}
