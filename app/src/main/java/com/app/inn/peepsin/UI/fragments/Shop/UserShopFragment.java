package com.app.inn.peepsin.UI.fragments.Shop;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.MyShop;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.fragments.SellStuffModule.UserSellingStuffFragment;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kShopId;
import static com.app.inn.peepsin.Constants.Constants.kType;
import static com.app.inn.peepsin.Constants.Constants.kUserId;
import static com.app.inn.peepsin.Constants.Constants.kUserName;

/**
 * Created by dhruv on 20/7/17.
 */

public class UserShopFragment extends Fragment {

    @BindView(R.id.tv_title)
    TextView textViewTitle;
    @BindView(R.id.iv_shop_image)
    ImageView ivShopImage;
    @BindView(R.id.iv_logo_image)
    ImageView ivLogoImage;
    @BindView(R.id.tv_shop_name) TextView tvShopName;
    @BindView(R.id.rating_score_bar)
    RatingBar ratingScoreBar;
    @BindView(R.id.tv_name) TextView tvName;
    @BindView(R.id.tv_email) TextView tvEmail;
    @BindView(R.id.tv_phone) TextView tvPhone;
    @BindView(R.id.tv_joined) TextView tvJoined;
    @BindView(R.id.tv_location) TextView tvLocation;
    @BindView(R.id.tv_product_count) TextView tvProductCount;
    @BindView(R.id.view_selling_product)
    LinearLayout productView;

    private static Switcher profileSwitcherListener;
    private Integer userId;
    private String userName;
    private Integer shopId;
    private MyShop myShop;
    private ProgressDialog progressDialog;

    public static Fragment newInstance(Switcher switcher, int shopId,int userId,String username){
        Fragment fragment = new UserShopFragment();
        Bundle bdl = new Bundle();
        bdl.putInt(kUserId,userId);
        bdl.putInt(kShopId,shopId);
        bdl.putString(kUserName,username);
        fragment.setArguments(bdl);
        profileSwitcherListener = switcher;
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        progressDialog = Utils.generateProgressDialog(getContext(),false);
        if (bundle != null) {
            userId = bundle.getInt(kUserId);
            shopId = bundle.getInt(kShopId);
            userName= bundle.getString(kUserName);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_user_shop, container, false);
        ButterKnife.bind(this,view);
        textViewTitle.setText(getString(R.string.user_shop_title));

        //Rating star color
        LayerDrawable stars = (LayerDrawable) ratingScoreBar.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(Color.parseColor("#FAD368"), PorterDuff.Mode.SRC_ATOP);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadShopData(shopId);
    }

    private void loadShopData(Integer shopId){
        showProgress(true);
        ModelManager.modelManager().getShopDetails(shopId, (Constants.Status iStatus, GenricResponse<MyShop> genricResponse) -> {
            showProgress(false);
            myShop = genricResponse.getObject();
            updateData(myShop);
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Toaster.toast(message);
        });
    }


    private void updateData(MyShop shop){
        tvShopName.setText(shop.getName());
        tvName.setText(shop.getOwnerName());
        tvEmail.setText(shop.getEmail());
        String phone = "No Contact Available";
        if(!shop.getPhone().isEmpty())
            phone = shop.getPhone();
        tvPhone.setText(phone);
        tvJoined.setText(shop.getJoinedDate());
        tvProductCount.setText(String.valueOf(shop.getProductCount()));
        ratingScoreBar.setRating(Float.valueOf(shop.getRating()));

        String address = "No Address Available";
        if(shop.getAddress()!=null)
            address = shop.getAddress().getCity()+" , "+shop.getAddress().getState()+" , "+shop.getAddress().getCountry();
        tvLocation.setText(address);

        String bannerUrl = shop.getBannerUrl();
        if (!bannerUrl.isEmpty()) {
            Picasso.with(getContext())
                    .load(bannerUrl)
                    .placeholder(R.drawable.img_product_placeholder)
                    .into(ivShopImage);
        }
        else
            ivShopImage.setImageResource(R.drawable.img_product_placeholder);

        String logoUrl = shop.getLogoUrl();
        if (!logoUrl.isEmpty()) {
            Picasso.with(getContext())
                    .load(logoUrl)
                    .resize(80,80)
                    .placeholder(R.drawable.img_shop_logo)
                    .into(ivLogoImage);
        }
        else
            ivLogoImage.setImageResource(R.drawable.img_shop_logo);
    }

    @OnClick(R.id.view_selling_product)
    void onSellingProduct(){
        if (profileSwitcherListener != null)
            profileSwitcherListener.switchFragment(UserSellingStuffFragment.newInstance(profileSwitcherListener,userId,userName,myShop), true, true);

    }

    @OnClick(R.id.btn_back)
    public void onBackPress(){
        getFragmentManager().popBackStack();
    }

    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) progressDialog.cancel();
        }
    }

}
