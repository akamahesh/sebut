package com.app.inn.peepsin.Models;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by dhruv on 17/8/17.
 */

public class CompanyDetails extends BaseModel implements Serializable {

    private String companyCINNumber;
    private String companyAddress;

    public CompanyDetails(JSONObject jsonResponse){
        this.companyCINNumber = getValue(jsonResponse, kCompanyCinNumber, String.class);
        this.companyAddress   = getValue(jsonResponse, kCompanyRegisteredAddress, String.class);
    }

    public String getCompanyCINNumber() {
        return companyCINNumber;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }
}
