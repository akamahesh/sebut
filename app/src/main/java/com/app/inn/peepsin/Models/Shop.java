package com.app.inn.peepsin.Models;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Lincoln on 18/05/16.
 */
public class Shop extends BaseModel implements Serializable{

    private static final String TAG = "Shop";
    private int shopId;
    private String shopName;
    private String shopBannerImageUrl;
    private Address shopAddress;
    private Integer connectionLevel;
    private Integer businessType;
    private CopyOnWriteArrayList<ProductType> productTypeList;
    private KYCInfo kycInfo;
    private BankDetails bankDetails;

    public Shop(JSONObject jsonResponse){
        this.shopId = getValue(jsonResponse, kShopId, Integer.class);
        this.shopName = getValue(jsonResponse,kShopName,String.class);
        this.shopBannerImageUrl = getValue(jsonResponse, kShopBannerImageURl,String.class);
        try {
            this.shopAddress = new Address(getValue(jsonResponse,kShopAddress,JSONObject.class));
        }catch (Exception e){
            Log.e(TAG, e.getMessage() );
        }
        try {
            this.connectionLevel = getValue(jsonResponse,kConnectionLevel,Integer.class);
        }catch (Exception e){
            Log.e(TAG, e.getMessage() );
        }
        try {
            this.businessType   = getValue(jsonResponse,kBusinessType,Integer.class);
        }catch (Exception e){
            Log.e(TAG, e.getMessage() );
        }
        try {
            this.kycInfo        = new KYCInfo(getValue(jsonResponse,kKycInfo,JSONObject.class));
        }catch (Exception e){
            Log.e(TAG, e.getMessage() );
        }
        try {
            this.bankDetails    = new BankDetails(getValue(jsonResponse,kBankDetails,JSONObject.class));
        }catch (Exception e){
            Log.e(TAG, e.getMessage() );
        }
        try {
            this.productTypeList = handleProductTypes(getValue(jsonResponse, kProductTypeStore, JSONArray.class));
        }catch (Exception e){
            Log.e(TAG, e.getMessage() );
        }

    }

    public int getShopId() {
        return shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public String getShopBannerImageUrl() {
        return shopBannerImageUrl;
    }

    public Address getShopAddress() {
        return shopAddress;
    }

    public Integer getBusinessType() {
        return businessType;
    }

    public KYCInfo getKycInfo() {
        return kycInfo;
    }

    public BankDetails getBankDetails() {
        return bankDetails;
    }

    public Integer getConnectionLevel() {
        return connectionLevel;
    }

    private CopyOnWriteArrayList<ProductType> handleProductTypes(JSONArray jsonArray) throws JSONException {
        CopyOnWriteArrayList<ProductType> productTypes = new CopyOnWriteArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            productTypes.add(new ProductType(jsonArray.getJSONObject(i)));
        }
        return productTypes;
    }

    public CopyOnWriteArrayList<ProductType> getProductTypeList() {
        return productTypeList;
    }
}
