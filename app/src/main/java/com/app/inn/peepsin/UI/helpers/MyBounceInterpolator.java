package com.app.inn.peepsin.UI.helpers;

import android.view.animation.Interpolator;

/**
 * Created by mahesh on 5/1/17.
 */

public class MyBounceInterpolator implements Interpolator {

    double mAmplitude = 1;  //The  value is the bounce amplitude. The higher value produces more pronounced bounces.
    double mFrequency = 10; //The  value is the frequency of the bounces. The higher value creates more wobbles during the animation time period.

    public MyBounceInterpolator(double amplitude, double frequency){
        mAmplitude=amplitude;
        mFrequency=frequency;
    }

    @Override
    public float getInterpolation(float input) {
        return (float) (-1*Math.pow(Math.E,-input/mAmplitude)*Math.cos(mFrequency*input)+1);
    }
}
