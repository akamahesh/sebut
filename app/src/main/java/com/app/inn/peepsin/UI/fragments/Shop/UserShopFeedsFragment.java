package com.app.inn.peepsin.UI.fragments.Shop;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.Address;
import com.app.inn.peepsin.Models.CurrentUser;
import com.app.inn.peepsin.Models.Feeds;
import com.app.inn.peepsin.Models.MyShop;
import com.app.inn.peepsin.Models.Search;
import com.app.inn.peepsin.Models.ShopCategory;
import com.app.inn.peepsin.Models.ShopModel;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.activities.ShoppingCartActivity;
import com.app.inn.peepsin.UI.adapters.FeedAdapter;
import com.app.inn.peepsin.UI.adapters.InventoryAdapter;
import com.app.inn.peepsin.UI.adapters.SearchAdapter;
import com.app.inn.peepsin.UI.adapters.ShopInventoryAdapter;
import com.app.inn.peepsin.UI.fragments.Profile.ShoppingCartFragment;
import com.app.inn.peepsin.UI.fragments.Search.SearchResultFragment;
import com.app.inn.peepsin.UI.helpers.DividerItemRecyclerDecoration;
import com.app.inn.peepsin.UI.helpers.SearchUtil;
import com.app.inn.peepsin.UI.helpers.SharedPreference;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kFeeds;
import static com.app.inn.peepsin.Constants.Constants.kSearchString;
import static com.app.inn.peepsin.Constants.Constants.kShopId;

public class UserShopFeedsFragment extends Fragment implements ShopInventoryAdapter.InventoryFilter {

    private String TAG = getTag();

    @BindView(R.id.recycler_view_feeds)
    RecyclerView recyclerViewFeeds;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.empty_view)
    View emptyView;
    @BindView(R.id.progress_bar)
    ProgressBar progress;
    @BindView(R.id.recycler_view_inventrory_item)
    RecyclerView recyclerInventroryItem;

    @BindView(R.id.bottom_shop_details)
    View bottomSheetView;
    @BindView(R.id.iv_arrow)
    ImageView ivArrow;
    @BindView(R.id.tv_Shop_name)
    TextView tvShopName;
    @BindView(R.id.ratingbar)
    RatingBar ratingBar;
    @BindView(R.id.tv_user_name)
    TextView tvOwner;
    @BindView(R.id.iv_shop_image)
    ImageView ivBanner;
    @BindView(R.id.iv_logo_image)
    ImageView ivLogo;
    @BindView(R.id.tv_email)
    TextView tvEmail;
    @BindView(R.id.tv_phone)
    TextView tvPhone;
    @BindView(R.id.tv_location)
    TextView tvLocation;
    @BindView(R.id.tv_joined_shop_date)
    TextView tvJoined;
    @BindView(R.id.fab)
    FloatingActionButton floatingActionButton;
    @BindView(R.id.tv_cartCount)
    TextView tvCartCount;
    @BindView(R.id.btn_clear_filter)
    Button btnClearFilter;
    private MyShop myShop;

    private static Switcher switcherListener;
    private ProgressDialog progressDialog;
    private int mPage=1;
    private boolean loading;
    private int pageSize;

    private FeedAdapter feedAdapter;
    private LinearLayoutManager mLayoutManager;
    private List<Feeds> feedsList;
    private List<ShopCategory> shopCategoryList;

    private SearchAdapter searchAdapter;
    private List<Search> searchStored;
    private ListView listSearch;
    private TextView tvEmptySearch;
    BottomSheetBehavior bottomSheetBehavior;


    private ShopModel shopModel;
    private int shopId;
    private ShopInventoryAdapter inventoryAdapter;
    // AppBarLayout appbar;


    public static UserShopFeedsFragment newInstance(Switcher switcher, int shopId) {
        UserShopFeedsFragment fragment = new UserShopFeedsFragment();
        switcherListener = switcher;
        Bundle bundle = new Bundle();
        bundle.putInt(kShopId, shopId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = Utils.generateProgressDialog(getContext(), true);
        feedsList = new ArrayList<>();
        shopCategoryList = new ArrayList<>();
        Bundle bundle = getArguments();
        if (bundle != null) {
            shopId = bundle.getInt(kShopId);
            // shopModel = (ShopModel) bundle.getSerializable(kShop);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_feeds, container, false);
        ButterKnife.bind(this, view);

        tvTitle.setText(getString(R.string.title_feeds));

        //Rating star color
        LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(Color.parseColor("#FAD368"), PorterDuff.Mode.SRC_ATOP);

        // init the bottom sheet behavior
        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheetView);
        bottomSheetBehavior.setBottomSheetCallback(bottomSheetCallback);

        // swipe to refresh the new list
        swipeRefreshLayout.setOnRefreshListener(onRefreshListener);

        // Feed Adapter and feed List
        feedAdapter = new FeedAdapter(getContext(), feedsList, switcherListener);
        recyclerViewFeeds.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerViewFeeds.setLayoutManager(mLayoutManager);
        recyclerViewFeeds.setAdapter(feedAdapter);
        RecyclerView.ItemDecoration itemDecoration = new DividerItemRecyclerDecoration(getContext(), R.drawable.canvas_recycler_divider);
        recyclerViewFeeds.addItemDecoration(itemDecoration);
        recyclerViewFeeds.addOnScrollListener(onScrollListener);

        // Inventory Adapter and Inventory List
        inventoryAdapter = new ShopInventoryAdapter(getContext(), shopCategoryList, this);
        LinearLayoutManager HorizontalLayout = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerInventroryItem.setLayoutManager(HorizontalLayout);
        recyclerInventroryItem.setAdapter(inventoryAdapter);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        CurrentUser currentUser = ModelManager.modelManager().getCurrentUser();
        tvCartCount.setText(String.valueOf(currentUser.getShoppingCartCount()));
        loadShopData(shopId);
        loadFeeds(mPage);
        loadShopCategory();
        feedAdapter.notifyDataSetChanged();
        inventoryAdapter.notifyDataSetChanged();
    }

    @Override
    public void onResume() {
        super.onResume();
        CurrentUser currentUser = ModelManager.modelManager().getCurrentUser();
        setCartUpdate(currentUser.getShoppingCartCount());
    }


    public void setCartUpdate(int count) {
        if (count == 0)
            tvCartCount.setVisibility(View.GONE);
        else {
            tvCartCount.setVisibility(View.VISIBLE);
            tvCartCount.setText(String.valueOf(count));
        }
    }

    @OnClick(R.id.btn_clear_filter)
    void onClearFilter() {
        feedAdapter.addItems(feedsList);
        checkEmptyScreen();
        for (ShopCategory shopCategory : shopCategoryList) {
            shopCategory.setSelected(false);
        }
        inventoryAdapter.addItems(shopCategoryList);
        btnClearFilter.setVisibility(View.GONE);
    }

    public void initialState() {
        mPage = 1;
        loading = true;
        pageSize = feedsList.size();
    }

    SwipeRefreshLayout.OnRefreshListener onRefreshListener = this::refreshData;

    private void refreshData() {
        ModelManager.modelManager().getProductFeeds(shopId, 0, 1,(Constants.Status iStatus, GenricResponse<HashMap<String, Object>> genericResponse) -> {
            HashMap<String, Object> map = genericResponse.getObject();
            CopyOnWriteArrayList<Feeds> list = (CopyOnWriteArrayList<Feeds>) map.get(kFeeds);
            feedsList = list;
            feedAdapter.addNewItems(list);
            checkEmptyScreen();
            initialState();
            swipeRefreshLayout.setRefreshing(false);
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Log.e(TAG, message);
            checkEmptyScreen();
            swipeRefreshLayout.setRefreshing(false);
        });
    }

    BottomSheetBehavior.BottomSheetCallback bottomSheetCallback = new BottomSheetBehavior.BottomSheetCallback() {
        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_EXPANDED) {
                ivArrow.setImageDrawable(getResources().getDrawable(R.drawable.ic_keyboard_arrow_down_grey_500_24dp));
            } else {
                ivArrow.setImageDrawable(getResources().getDrawable(R.drawable.ic_keyboard_arrow_up_grey_500_24dp));
            }
            // this part hides the button immediately and waits bottom sheet
            // to collapse to show
            if (BottomSheetBehavior.STATE_DRAGGING == newState) {
                floatingActionButton.animate().scaleX(0).scaleY(0).setDuration(300).start();
            } else if (BottomSheetBehavior.STATE_COLLAPSED == newState) {
                floatingActionButton.animate().scaleX(1).scaleY(1).setDuration(300).start();
            }
        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {
        }
    };

    RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            if (dy > 0) //check for scroll down
            {
                int visibleItemCount = mLayoutManager.getChildCount();
                int totalItemCount = mLayoutManager.getItemCount();
                int firstVisibleItemPosition = mLayoutManager.findFirstVisibleItemPosition();

                if (loading) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= pageSize) {
                        loading = false;
                        //Do pagination.. i.e. fetch new data
                        loadMoreData(++mPage);
                    }
                }
            }
        }
    };

    @OnClick(R.id.btn_back)
    void onBack() {
        getFragmentManager().popBackStack();
        Utils.isfeedsClicked = 0;
        //appbar.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.search_view)
    public void onSearch() {
        loadToolBarSearch();
    }

    @OnClick(R.id.btn_cart)
    void onCart() {
        // Utils.isfeedsClicked=1;
        Utils.iscartClicked = 1;
        if(switcherListener!=null)
            switcherListener.switchFragment(ShoppingCartFragment.newInstance(switcherListener),true,true);
        //startActivity(ShoppingCartActivity.getIntent(getContext()));
    }

    public void loadToolBarSearch() {
        searchStored = SharedPreference.getSearchList(getContext(), SearchUtil.PREFS_NAME, SearchUtil.KEY_SEARCH);
        View view = getActivity().getLayoutInflater().inflate(R.layout.view_toolbar_search, null);
        EditText edtToolSearch = (EditText) view.findViewById(R.id.edt_tool_search);
        ImageView ivToolMic = (ImageView) view.findViewById(R.id.img_tool_mic);
        ImageView ivToolBack = (ImageView) view.findViewById(R.id.img_tool_back);
        listSearch = (ListView) view.findViewById(R.id.list_search);
        tvEmptySearch = (TextView) view.findViewById(R.id.txt_empty);

        SearchUtil.setListViewHeightBasedOnChildren(listSearch);
        edtToolSearch.setHint("Search to shop");

        Dialog toolbarSearchDialog = new Dialog(getContext(), R.style.MaterialSearch);
        toolbarSearchDialog.setContentView(view);
        toolbarSearchDialog.setCancelable(false);
        toolbarSearchDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        toolbarSearchDialog.getWindow().setGravity(Gravity.BOTTOM);
        toolbarSearchDialog.show();

        toolbarSearchDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        if (searchStored == null) {
            searchStored = new ArrayList<>();
        }
        searchAdapter = new SearchAdapter(getContext(), searchStored, false);
        listSearch.setAdapter(searchAdapter);

        listSearch.setOnItemClickListener((adapterView, view13, position, l) -> {
            toolbarSearchDialog.dismiss();

            Search item = searchAdapter.getItemPos(position);
            if (searchStored.contains(item)) {
                SharedPreference.removeSearch(getContext(), item, SearchUtil.PREFS_NAME, SearchUtil.KEY_SEARCH);
            }
            SharedPreference.addSearch(getContext(), item, SearchUtil.PREFS_NAME, SearchUtil.KEY_SEARCH);

            if (switcherListener != null)
                switcherListener.switchFragment(SearchResultFragment.newInstance(item.getCategoryId(), switcherListener), true, false);
        });

        edtToolSearch.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                String str = edtToolSearch.getText().toString();
                searchProduct(str);
                return true;
            }
            return false;
        });

        edtToolSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (searchAdapter.getCount() > 0) {
                    searchAdapter.clearList();
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String str = s.toString().toLowerCase();
                char[] ch = str.toCharArray();
                for (char aCh : ch) {
                    if (aCh == ' ') {
                        String withoutSpaceStr = str.replaceAll("\\s+", "");
                        searchProduct(withoutSpaceStr);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        ivToolBack.setOnClickListener(view1 -> toolbarSearchDialog.dismiss());

        ivToolMic.setOnClickListener(view12 -> edtToolSearch.setText(""));
    }

    public void searchProduct(String searchStr) {
        HashMap<String, Object> searchmap = new HashMap<>();
        searchmap.put(kSearchString, searchStr);
        ModelManager.modelManager().getSearch(searchmap, (Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<Search>> genericList) -> {
            searchAdapter.updateList(genericList.getObject(), true);
            checkEmptySearch();
        }, (Constants.Status iStatus, String message) -> {
            Toaster.toast(message);
            checkEmptySearch();
        });
    }

    public void loadFeeds(int page) {
        showProgress(true);
        ModelManager.modelManager().getProductFeeds(shopId, 0,page, (Constants.Status iStatus, GenricResponse<HashMap<String, Object>> genericResponse) -> {
            HashMap<String, Object> map = genericResponse.getObject();
            CopyOnWriteArrayList<Feeds> list = (CopyOnWriteArrayList<Feeds>) map.get(kFeeds);
            feedsList = list;
            feedAdapter.addNewItems(list);
            checkEmptyScreen();
            initialState();
            showProgress(false);
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Log.e(TAG, message);
            checkEmptyScreen();
        });
    }

    public void loadMoreData(int page) {
        ModelManager.modelManager().getProductFeeds(shopId, 0,page, (Constants.Status iStatus, GenricResponse<HashMap<String, Object>> genericResponse) -> {
            HashMap<String, Object> map = genericResponse.getObject();
            CopyOnWriteArrayList<Feeds> list = (CopyOnWriteArrayList<Feeds>) map.get(kFeeds);
            feedAdapter.addItems(list);
            loading = true;
        }, (Constants.Status iStatus, String message) -> Log.e(TAG, message));
    }


    public void loadShopCategory() {
        ModelManager.modelManager().getShopCategoryList(shopId, (Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<ShopCategory>> genericResponse) -> {
            CopyOnWriteArrayList<ShopCategory> shopCategories = genericResponse.getObject();
            shopCategoryList = shopCategories;
            inventoryAdapter.addItems(shopCategories);
        }, (Constants.Status iStatus, String message) -> Log.e(TAG, message));
    }


    private void checkEmptyScreen() {
        if (feedAdapter.getItemCount() > 1) {
            recyclerInventroryItem.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
        } else {
            recyclerInventroryItem.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        }
    }

    private void checkEmptySearch() {
        if (searchAdapter.getCount() > 0) {
            listSearch.setVisibility(View.VISIBLE);
            tvEmptySearch.setVisibility(View.GONE);
        } else {
            listSearch.setVisibility(View.GONE);
            tvEmptySearch.setVisibility(View.VISIBLE);
            tvEmptySearch.setText(getString(R.string.empty_message_search));
        }
    }

    private void showProgress(boolean b) {
        if (b) {
            //progressDialog.show();
            swipeRefreshLayout.setRefreshing(true);
        } else {
            swipeRefreshLayout.setRefreshing(false);
            //if (progressDialog != null) progressDialog.cancel();
        }
    }

    private void loadShopData(Integer shopId) {
        showProgress(true);
        ModelManager.modelManager().getMyShopDetails(shopId, (Constants.Status iStatus, GenricResponse<MyShop> genricResponse) -> {
            showProgress(false);
            myShop = genricResponse.getObject();
            userDetails(myShop);
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Toaster.toast(message);
        });
    }

    private void userDetails(MyShop myShop) {
        String shopName = myShop.getName();
        String owner = myShop.getOwnerFirstName() + " " + myShop.getOwnerLastName();
        Integer rating = myShop.getRating();
        String ownerImageUrl = (myShop.getBannerUrl() == null) ? "" : myShop.getBannerUrl();
        String favouriteCount = myShop.getFavoriteCount().toString();
        String shopCount = myShop.getProductCount().toString();
        String email = myShop.getEmail();
        String phone = (myShop.getPhone().isEmpty()) ? "No Phone Available" : myShop.getPhone();

        Address address = myShop.getAddress();
        String addressText = "Address not Available.";
        if (address != null) {
            addressText = address.getStreetAddress()+" ,"+address.getCity()+" ,"+address.getState()+" ,"+address.getCountry();
        }

        String joinedTxt = "Joined PeepsIn on " + myShop.getJoinedDate();
        tvJoined.setText(joinedTxt);
        tvLocation.setText(addressText);
        tvShopName.setText(shopName);
        tvOwner.setText(owner);
        ratingBar.setRating(Float.valueOf(rating));
        tvEmail.setText(email);
        tvPhone.setText(phone);

        if (!ownerImageUrl.isEmpty()) {
            Picasso.with(getContext())
                    .load(ownerImageUrl)
                    .resize(400, 300)
                    .placeholder(R.drawable.img_product_placeholder)
                    .into(ivBanner);
        }

        String logoUrl = myShop.getLogoUrl();
        if (!logoUrl.isEmpty()) {
            Picasso.with(getContext())
                    .load(logoUrl)
                    .resize(80,80)
                    .placeholder(R.drawable.img_shop_logo)
                    .into(ivLogo);
        }
        else
            ivLogo.setImageResource(R.drawable.img_shop_logo);
    }


    @Override
    public void filter(Integer id) {
        showProgress(true);
        CopyOnWriteArrayList<Feeds> list = new CopyOnWriteArrayList<>();
        for (int i = 0; i < feedsList.size(); i++) {
            if (feedsList.get(i).getCategoryId().equals(id))
                list.add(feedsList.get(i));
        }
        feedAdapter.addNewItems(list);
        checkEmptyScreen();
        showProgress(false);
        btnClearFilter.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        switcherListener = null;
    }


}
