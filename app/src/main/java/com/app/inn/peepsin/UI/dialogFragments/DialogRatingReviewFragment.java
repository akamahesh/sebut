package com.app.inn.peepsin.UI.dialogFragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RatingBar;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.ReviewModel;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kMyRating;
import static com.app.inn.peepsin.Constants.Constants.kMyReview;
import static com.app.inn.peepsin.Constants.Constants.kProductId;

/**
 * Created by Harsh on 6/30/2017.
 */

public class DialogRatingReviewFragment extends DialogFragment {

    @BindView(R.id.ratingBar)
    RatingBar myRatingBar;
    @BindView(R.id.edt_review)
    EditText edtReview;

    private ProgressDialog progressDialog;
    private int productId;
    private Integer rating = 0;
    private String review = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = Utils.generateProgressDialog(getContext(), true);
        setStyle(DialogFragment.STYLE_NORMAL,  R.style.CustomDialog);
        //setStyle(DialogFragment.STYLE_NORMAL, R.style.MY_DIALOG);
        Bundle mArgs = getArguments();
        productId = mArgs.getInt(kProductId);
        rating = mArgs.getInt(kMyRating);
        review = mArgs.getString(kMyReview);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_rating_view, container, false);
        ButterKnife.bind(this, rootView);
        myRatingBar.setRating(rating);
        LayerDrawable stars = (LayerDrawable) myRatingBar.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(Color.parseColor("#FAD368"), PorterDuff.Mode.SRC_ATOP);
        if(!review.isEmpty())
            edtReview.setText(review);
        return rootView;
    }

    @OnClick(R.id.submit)
    void onSubmit(){
        String reviewTxt = edtReview.getText().toString();
        if(reviewTxt.isEmpty())
            edtReview.setError("Write a review");
        else if(myRatingBar.getRating()==0){
            Toaster.toast("Input a rating");
        }
        else {
            showProgress(true);
            int rating = Math.round(myRatingBar.getRating());
            ModelManager.modelManager().editAddRating(productId,reviewTxt,rating,(Constants.Status iStatus, GenricResponse<ReviewModel> genericResponse) -> {
                ReviewModel review = genericResponse.getObject();
                Log.v("Rating & Review","Rating:"+review.getRating()+",Review:"+review.getReview());
                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK,getActivity().getIntent());
                dismiss();
                showProgress(false);
            }, (Constants.Status iStatus, String message) -> {
                showProgress(false);
                Toaster.toast(message);
            });
        }

    }

    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) progressDialog.cancel();
        }
    }
}






