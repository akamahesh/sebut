package com.app.inn.peepsin.UI.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.app.inn.peepsin.Models.SharedProduct;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.fragments.Feeds.SharedProductListFragment;
import com.app.inn.peepsin.UI.fragments.Shop.RecommendedDetailFragment;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import com.squareup.picasso.Picasso;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by dhruv on 31/8/17.
 */

public class RecommendedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<SharedProduct> productList;
    private Context context;
    private static Switcher switcherListener;
    private ProgressDialog progressDialog;

    public RecommendedAdapter(Context context, List<SharedProduct> items, Switcher switcher) {
        switcherListener = switcher;
        progressDialog = Utils.generateProgressDialog(context, false);
        productList = items;
        this.context = context;
    }

    @Override
    public int getItemViewType(int position) {
        if(productList.size()>=7)
            if (position == productList.size()) {
                return 0;
            } else {
                return 1;
            }
        else
            return 1;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 1) {
            View layoutView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.card_recommended_view, parent, false);
            return new FeedViewHolder(layoutView);
        } else {
            View layoutView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.card_all_recommended, parent, false);
            return new FeedProgressHolder(layoutView);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (productList.size()>=7 && position == productList.size()) {
            FeedProgressHolder view = (FeedProgressHolder) viewHolder;
            view.tvSeeAll.setOnClickListener(v -> {
                if(switcherListener!=null) {
                    switcherListener.switchFragment(SharedProductListFragment.newInstance(switcherListener),true,true);
                }
            });
        }else {
            FeedViewHolder holder = (FeedViewHolder) viewHolder;
            SharedProduct item = productList.get(position);
            holder.bindContent(item);
            /*
            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams)holder.itemView.getLayoutParams();
            if(position==0)
                params.setMargins(Utils.dpToPx(context,15), 0, 0,Utils.dpToPx(context,5) );
            else if(position==getItemCount()-1)
                params.setMargins(Utils.dpToPx(context,10), 0, Utils.dpToPx(context,15), Utils.dpToPx(context,5));
            else
                params.setMargins(Utils.dpToPx(context,10), 0, 0, Utils.dpToPx(context,5));
            holder.itemView.setLayoutParams(params);
            */
            String name = item.getName();
            String thumbnailURL = item.getImageUrl();
            holder.tvName.setText(Utils.getFirstLetterInUpperCase(name));
            holder.tvRating.setText(String.valueOf(item.getRating()));

            int costPrice = Integer.parseInt(item.getPrice().replace(".00/-",""));
            String price = context.getString(R.string.Rs)+costPrice;
            int sellPrice = Integer.parseInt(item.getSellingPrice().replace(".00/-",""));
            String sellingPrice = context.getString(R.string.Rs)+sellPrice;
            holder.tvSellingPrice.setText(sellingPrice);
            holder.tvPrice.setText(price);
            holder.tvPrice.setPaintFlags(holder.tvPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

            if(costPrice==sellPrice){
                holder.tvPrice.setVisibility(View.GONE);
                holder.tvDiscount.setVisibility(View.GONE);
            } else{
                holder.tvPrice.setVisibility(View.VISIBLE);
                holder.tvDiscount.setVisibility(View.VISIBLE);
                int discountPrice = costPrice - sellPrice;
                float discount = (float) discountPrice / costPrice;
                String discountTag = (int) (discount * 100) + "% off";
                holder.tvDiscount.setText(discountTag);
            }

            if (!item.getImageUrl().isEmpty())
                Picasso.with(context)
                        .load(thumbnailURL).fit()
                        .placeholder(R.drawable.img_product_placeholder)
                        .into(holder.ivItem);
            else
                holder.ivItem.setImageResource(R.drawable.img_product_placeholder);
        }
    }

    public void addItems(List<SharedProduct> list){
        productList.clear();
        productList.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if(productList.size()>=7)
            return productList.size()+1;
        else
            return productList.size();
    }

    class FeedProgressHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_view)
        View tvSeeAll;

        FeedProgressHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class FeedViewHolder extends RecyclerView.ViewHolder {
        SharedProduct item;
        @BindView(R.id.item_view)
        View itemView;
        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.iv_image)
        ImageView ivItem;
        @BindView(R.id.tv_rating)
        TextView tvRating;
        @BindView(R.id.tv_discount)
        TextView tvDiscount;
        @BindView(R.id.tv_price)
        TextView tvPrice;
        @BindView(R.id.tv_selling_price)
        TextView tvSellingPrice;

        FeedViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.item_view)
        void onRowSelected() {
            if (switcherListener != null){
                switcherListener.switchFragment(RecommendedDetailFragment.newInstance(item.getId(),item, switcherListener), true, true);
            }
        }

        private void bindContent(SharedProduct item) {
            this.item = item;
        }
    }

    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) progressDialog.cancel();
        }
    }
}
