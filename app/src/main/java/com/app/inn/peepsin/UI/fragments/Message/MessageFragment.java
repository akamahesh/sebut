package com.app.inn.peepsin.UI.fragments.Message;

import static com.app.inn.peepsin.Constants.Constants.CART_UPDATE;
import static com.app.inn.peepsin.Constants.Constants.kData;
import static com.app.inn.peepsin.Constants.Constants.kMessage;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.app.inn.peepsin.Constants.Constants.ChatON;
import com.app.inn.peepsin.Managers.BaseManager.DateManager;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Managers.XMPPManager.Constant;
import com.app.inn.peepsin.Managers.XMPPManager.RoosterHelper;
import com.app.inn.peepsin.Models.ChatProduct;
import com.app.inn.peepsin.Models.CurrentUser;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.activities.ChatActivity;
import com.app.inn.peepsin.UI.activities.ChatActivity.ChatType;
import com.app.inn.peepsin.UI.fragments.Profile.ShoppingCartFragment;
import com.app.inn.peepsin.UI.helpers.DividerItemRecyclerDecoration;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.List;

public class MessageFragment extends Fragment {

  final String TAG = getClass().getSimpleName();

  @BindView(R.id.tv_title)
  TextView tvTitle;
  @BindView(R.id.recycler_view_message)
  RecyclerView recyclerView;
  @BindView(R.id.empty_view)
  View emptyView;
  @BindView(R.id.swipe_refresh)
  SwipeRefreshLayout swipeRefreshLayout;
  @BindView(R.id.tv_cartCount)
  TextView tvCartCount;

  private static Switcher switcherListener;
  private CurrentUser currentUser;
  private MessageAdapter messageAdapter;
  ProgressDialog progressDialog;
  private List<ChatProduct> chatProductList;

  @BindView(R.id.appBarLayout)
  AppBarLayout appbar;

  public MessageFragment() {
    // Required empty public constructor
  }

  public static MessageFragment newInstance(Switcher switcher) {
    switcherListener = switcher;
    return new MessageFragment();
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_message, container, false);
    ButterKnife.bind(this, view);
    // appbar.setVisibility(View.GONE);

    tvTitle.setText(getString(R.string.title_chat));
    chatProductList = new ArrayList<>();

    progressDialog = Utils.generateProgressDialog(getContext(), true);
    messageAdapter = new MessageAdapter(chatProductList);
    recyclerView.setHasFixedSize(true);
    recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    recyclerView.setAdapter(messageAdapter);
    recyclerView.addItemDecoration(
        new DividerItemRecyclerDecoration(getActivity(), R.drawable.canvas_recycler_divider));
    swipeRefreshLayout.setOnRefreshListener(refreshListener);
    return view;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    loadData();
    currentUser = ModelManager.modelManager().getCurrentUser();
    setCartUpdate(currentUser.getShoppingCartCount());
  }

  public void setCartUpdate(int count) {
    if (count == 0) {
      tvCartCount.setVisibility(View.GONE);
    } else {
      tvCartCount.setVisibility(View.VISIBLE);
      tvCartCount.setText(String.valueOf(count));
    }
  }

  private BroadcastReceiver mRegistrationBroadcastReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      if (intent.getAction().equals(Constant.kIncomingMessage)) {
        refreshListener.onRefresh();
        ChatProduct chatProduct = (ChatProduct) intent.getSerializableExtra(kMessage);
        for (ChatProduct chatProduct1 : chatProductList) {
          if (chatProduct.getUniqueId().equals(chatProduct1.getUniqueId())) {
            chatProduct1.increaseUnreadChatCount();
            break;
          }
        }
        messageAdapter.notifyDataSetChanged();
      } else if (intent.getAction().equals(CART_UPDATE)) {
        setCartUpdate(intent.getIntExtra(kData, 0));
      }
    }
  };


  @Override
  public void onResume() {
    super.onResume();
    // register cart update receiver
    LocalBroadcastManager.getInstance(getActivity())
        .registerReceiver(mRegistrationBroadcastReceiver,
            new IntentFilter(CART_UPDATE));

    LocalBroadcastManager.getInstance(getActivity())
        .registerReceiver(mRegistrationBroadcastReceiver,
            new IntentFilter(Constant.kIncomingMessage));
  }

  @Override
  public void onPause() {
    super.onPause();

  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
    LocalBroadcastManager.getInstance(getActivity())
        .unregisterReceiver(mRegistrationBroadcastReceiver);
  }

  SwipeRefreshLayout.OnRefreshListener refreshListener = this::loadData;

  @OnClick(R.id.btn_cart)
  void onCart() {
    if (switcherListener != null) {
      switcherListener
          .switchFragment(ShoppingCartFragment.newInstance(switcherListener), true, true);
    }
    //startActivity(ShoppingCartActivity.getIntent(getContext()));
  }

  void checkEmptyState() {
    if (messageAdapter.getItemCount() > 0) {
      emptyView.setVisibility(View.GONE);
    } else {
      emptyView.setVisibility(View.VISIBLE);
    }
  }

  private void loadData() {
    chatProductList = RoosterHelper.getChatThreads();
    messageAdapter.addMessages(chatProductList);
    messageAdapter.notifyDataSetChanged();
    checkEmptyState();
    swipeRefreshLayout.setRefreshing(false);
  }


  class MessageAdapter extends RecyclerView.Adapter<ViewHolder> {

    private List<ChatProduct> list;

    MessageAdapter(List<ChatProduct> messageList) {
      this.list = messageList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      ViewHolder viewHolder = null;
      if (viewType == ChatON.user.getValue()) {
        View layoutView = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.row_user_message_layout, parent, false);
        viewHolder = new UserMessageHolder(layoutView);
      }else if(viewType==ChatON.product.getValue()){
        View layoutView = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.row_message_layout, parent, false);
        viewHolder = new ProductMessageHolder(layoutView);
      }
     return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
      ChatProduct chatUser = list.get(position);
      String username = chatUser.getUserName();
      String messageBody = chatUser.getLastMessage().getBody();
      String userImageURL = chatUser.getUserImageURL();
      String chatOn = chatUser.getChatOn();
      String connectionLevel = chatUser.getConnectionLevel();
      userImageURL = (userImageURL == null) ? "" : userImageURL;
      String productName = chatUser.getProductName();
      String price = chatUser.getPrice();
      String productThumbnailURL = chatUser.getProductThumbnailURL();
      productThumbnailURL = (productThumbnailURL == null) ? "" : productThumbnailURL;
      String time = chatUser.getLastMessage().getTime();
      time = (time == null) ? "" : time;
      time = DateManager.dispalyValue(time);
      Integer unReadChatCount = chatUser.getUnreadChatCount();
      if (holder instanceof UserMessageHolder) {
        UserMessageHolder userMessageHolder = (UserMessageHolder) holder;
        userMessageHolder.bindContent(chatUser);
        userMessageHolder.tvLastMessage.setText(messageBody);
        userMessageHolder.tvRecentTime.setText(time);
        userMessageHolder.tvUnReadChatCount.setText(String.valueOf(unReadChatCount));
        userMessageHolder.tvUserName.setText(username);
        if (!userImageURL.isEmpty()) {
          Picasso.with(getContext())
              .load(userImageURL)
              .resize(100, 100)
              .into(userMessageHolder.ivUserImage);
        } else {
          userMessageHolder.ivUserImage
              .setImageDrawable(getResources().getDrawable(R.drawable.img_profile_placeholder));
        }

      }else if (holder instanceof ProductMessageHolder) {
        ProductMessageHolder productMessageHolder = (ProductMessageHolder) holder;
        productMessageHolder.bindContent(chatUser);

        if (unReadChatCount.equals(0)) {
          productMessageHolder.tvUnReadChatCount.setVisibility(View.INVISIBLE);
        } else {
          productMessageHolder.tvUnReadChatCount.setVisibility(View.VISIBLE);
          productMessageHolder.tvUnReadChatCount.setText(String.valueOf(unReadChatCount));
        }

        productMessageHolder.tvUserName.setText(username);
        productMessageHolder.tvLastMessage.setText(messageBody);
        productMessageHolder.tvProductName.setText(productName);
        productMessageHolder.tvRecentTime.setText(time);
        if (!userImageURL.isEmpty()) {
          Picasso.with(getContext())
              .load(userImageURL)
              .resize(100, 100)
              .into(productMessageHolder.ivUserImage);
        } else {
          productMessageHolder.ivUserImage
              .setImageDrawable(getResources().getDrawable(R.drawable.img_profile_placeholder));
        }

        if (!productThumbnailURL.isEmpty()) {
          Picasso.with(getContext())
              .load(productThumbnailURL)
              .placeholder(R.drawable.img_product_placeholder)
              .resize(100, 100)
              .into(productMessageHolder.ivProductImage);
        } else {
          productMessageHolder.ivUserImage
              .setImageDrawable(getResources().getDrawable(R.drawable.img_profile_placeholder));
        }
      }

    }

    @Override
    public int getItemCount() {
      return list.size();
    }

    @Override
    public int getItemViewType(int position) {
      String chatOn = list.get(position).getChatOn();
      if(chatOn==null)
        return ChatON.user.getValue();
      if (chatOn.equals("1")) {//userchat
        return ChatON.user.getValue();
      } else if (chatOn.equals("2")) {//productChat
        return ChatON.product.getValue();
      }
      return super.getItemViewType(position);
    }

    void addMessages(List<ChatProduct> list) {
      this.list.clear();
      this.list.addAll(list);
      notifyDataSetChanged();
    }

    class ProductMessageHolder extends RecyclerView.ViewHolder {

      private ChatProduct chatUser;
      @BindView(R.id.iv_product_image)
      ImageView ivProductImage;
      @BindView(R.id.iv_user_image_circular)
      ImageView ivUserImage;
      @BindView(R.id.tv_user_name)
      TextView tvUserName;
      @BindView(R.id.tv_product_name)
      TextView tvProductName;
      @BindView(R.id.tv_recent_time)
      TextView tvRecentTime;
      @BindView(R.id.tv_last_mesage)
      TextView tvLastMessage;
      @BindView(R.id.tv_unread_chat_count)
      TextView tvUnReadChatCount;


      ProductMessageHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
      }

      @OnClick(R.id.view_message_row)
      void getChatWindow() {
        chatUser.setUnreadChatCount(0);
        startActivity(ChatActivity.getIntent(getContext(), chatUser, ChatType.PRODUCT_CHAT));
      }

      void bindContent(ChatProduct chatUser) {
        this.chatUser = chatUser;
      }
    }

    class UserMessageHolder extends RecyclerView.ViewHolder {

      private ChatProduct chatUser;
      @BindView(R.id.iv_user_image_circular)
      ImageView ivUserImage;
      @BindView(R.id.tv_user_name)
      TextView tvUserName;
      @BindView(R.id.tv_recent_time)
      TextView tvRecentTime;
      @BindView(R.id.tv_last_mesage)
      TextView tvLastMessage;
      @BindView(R.id.tv_unread_chat_count)
      TextView tvUnReadChatCount;


      UserMessageHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
      }

      @OnClick(R.id.view_message_row)
      void getChatWindow() {
        chatUser.setUnreadChatCount(0);
        startActivity(ChatActivity.getIntent(getContext(), chatUser, ChatType.USER_CHAT));
      }

      void bindContent(ChatProduct chatUser) {
        this.chatUser = chatUser;
      }
    }
  }
}
