package com.app.inn.peepsin.UI.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.Address;
import com.app.inn.peepsin.Models.Feeds;
import com.app.inn.peepsin.Models.FinalBillSummary;
import com.app.inn.peepsin.Models.FinalProductsRecords;
import com.app.inn.peepsin.Models.ProductList;
import com.app.inn.peepsin.Models.ShoppingCart;
import com.app.inn.peepsin.Models.ShoppingProduct;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.fragments.Feeds.ProductDetailFragment;
import com.app.inn.peepsin.UI.helpers.RoundedCornersTransformation;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.squareup.picasso.Picasso;

import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NewTransactionDetailAdapter extends RecyclerView.Adapter<NewTransactionDetailAdapter.ViewHolder> {

    private Context context;
    private CopyOnWriteArrayList<FinalProductsRecords> shopList;
    private FinalBillSummary finalBillSummary;
    private CopyOnWriteArrayList<ProductList> productLists;
    public NewTransactionDetailAdapter(Context context, CopyOnWriteArrayList<ProductList> productLists) {
        this.context = context;
        //this.finalBillSummary = finalBillSummary;
       // this.shopList = finalBillSummary.getShopRecords();
        this.productLists=productLists;


    }
    @Override
    public NewTransactionDetailAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.transaction_detail_child_layout, parent, false);


        return new NewTransactionDetailAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final NewTransactionDetailAdapter.ViewHolder holder, int position) {




            holder.bindContent(productLists);

/*

        ImageView ivShopBanner = (ImageView) convertView.findViewById(R.id.iv_shop_banner);
        TextView tvShopName = (TextView) convertView.findViewById(R.id.tv_shop_name);
        TextView tvTotalProduct = (TextView) convertView.findViewById(R.id.tv_total_product);
        TextView tvShippedAddress = (TextView) convertView.findViewById(R.id.tv_shipped_address);
*/

        holder.tvProductName.setText(productLists.get(position).getProductName());
        holder.tvQuantity.setText(String.valueOf(productLists.get(position).getProductQuantity()));
        String price = context.getString(R.string.Rs) + productLists.get(position).getPrice();
        holder.tvPrice.setText(price);
        Picasso.with(context)
                .load(productLists.get(position).getCoverImageThumbnailURL())
                .placeholder(R.drawable.img_product_placeholder)
                .into(holder.ivProduct);


    }

    @Override
    public int getItemCount() {
        return productLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CopyOnWriteArrayList<ProductList>  item;
        @BindView(R.id.iv_product)
        ImageView ivProduct;
        @BindView(R.id.tv_product_name)
        TextView tvProductName;
        @BindView(R.id.tv_quantity)
        TextView tvQuantity;
        @BindView(R.id.tv_product_price)
        TextView tvPrice;



        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }



        public void bindContent(CopyOnWriteArrayList<ProductList>  product) {
            this.item = product;
        }
    }


}
