package com.app.inn.peepsin.Models;

import java.util.Map;

import static com.app.inn.peepsin.Constants.Constants.kEmptyString;
import static com.app.inn.peepsin.Constants.Constants.kGoogleContactsEmail;
import static com.app.inn.peepsin.Constants.Constants.kGoogleContactsName;
import static com.app.inn.peepsin.Constants.Constants.kGoogleContactsPhone;
import static com.app.inn.peepsin.Constants.Constants.kGoogleContactsPhoto;

/**
 * Created by mahesh on 30/5/17.
 */

public class GoogleContact {
    private String imageURL;
    private String name;
    private String contactId;
    private boolean isSelected = false;
    private boolean isRequestSent = false;

    public GoogleContact(Map<String, Object> objectMap) {
        String name = (String) objectMap.get(kGoogleContactsName);
        String email = (String) objectMap.get(kGoogleContactsEmail);
        String phone = (String) objectMap.get(kGoogleContactsPhone);
        String photo = (String) objectMap.get(kGoogleContactsPhoto);
        this.name = name;
        phone = (phone != null) ? phone : kEmptyString;
        email = (email != null) ? email : kEmptyString;
        this.imageURL = (photo != null) ? photo : kEmptyString;
        if(email.isEmpty())
            this.contactId = phone;
        else this.contactId = email;
    }

    public String getImageURL() {
        return imageURL;
    }

    public String getName(){
        return name;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isRequestSent() {
        return isRequestSent;
    }

    public void setRequestSent(boolean requestSent) {
        isRequestSent = requestSent;
    }

    @Override
    public String toString() {
        return "Google Contact : {" +"\n"+
                " contactId : " +contactId+"\n"+
                " name : " +name+"\n"+
                " imageURL :" +imageURL+"\n"+
                "}";
    }
}
