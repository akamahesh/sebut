package com.app.inn.peepsin.Managers.XMPPManager;

//Constant is private interface for XMPP Manager
public interface Constant {
    String kQueueNameXMPP                                 = "com.queue.serial.xmpp";
    String kXMPPHost                                       = "peepsin.epazy.com";
    String kXMPPPassword                                   = "123";
    String kXMPPProtocol                                   = "TLSv1.2";
    int    kXMPPPort                                       = 5222;
    String kElementMessageData                             = "messagedata";
    String kElementDelay                                   = "delay";

    String kNamespaceDelay                                 = "urn:xmpp:delay";
    String kNamespaceJabberClient                          = "jabber:client";

    String kFrom                                           = "from";
    String kId                                             = "id";
    String kIqId                                           = "iqid";
    String kTo                                             = "to";
    int    kMessageHistoryCount                            = 100;
    String kIncomingMessage = "INCOMING_MESSAGE";
}