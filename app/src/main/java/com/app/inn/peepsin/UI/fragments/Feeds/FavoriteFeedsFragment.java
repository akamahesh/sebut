package com.app.inn.peepsin.UI.fragments.Feeds;

import android.app.ProgressDialog;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.CurrentUser;
import com.app.inn.peepsin.Models.Feeds;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.activities.HomeActivity;
import com.app.inn.peepsin.UI.activities.ShoppingCartActivity;
import com.app.inn.peepsin.UI.fragments.Profile.ShoppingCartFragment;
import com.app.inn.peepsin.UI.helpers.DividerItemRecyclerDecoration;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kImageUrl;
import static com.app.inn.peepsin.Constants.Constants.kLatitude;
import static com.app.inn.peepsin.Constants.Constants.kLongitude;
import static com.app.inn.peepsin.Constants.Constants.kOwnerName;

public class FavoriteFeedsFragment extends Fragment {

    private static Switcher switcherListener;
    @BindView(R.id.recycler_view_feeds)
    RecyclerView recyclerViewFeeds;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.empty_view)
    View emptyView;
    @BindView(R.id.tv_empty_title)
    TextView tvEmptyTitle;
    @BindView(R.id.tv_empty_message)
    TextView tvEmptyMessage;
    @BindView(R.id.tv_cartCount) TextView tvCartCount;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    private CurrentUser currentUser;
    private FeedAdapter feedAdapter;
    private ProgressDialog progressDialog;
    private List<Feeds> feeds;
    private CurrentUser user;
    //AppBarLayout appbarLayout;

    public static Fragment newInstance(Switcher switcher) {
        switcherListener = switcher;
        return new FavoriteFeedsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = Utils.generateProgressDialog(getContext(), false);
        feeds = new ArrayList<>();
        user = ModelManager.modelManager().getCurrentUser();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favorite_feeds, container, false);
        ButterKnife.bind(this, view);
        /*appbarLayout=((HomeActivity) getActivity()).getTabLayout();
        Utils.isprofileClicked=1;*/
        tvTitle.setText(getString(R.string.title_favorite_products));
        tvEmptyTitle.setText(getString(R.string.empty_title_no_favorite_feeds));
        tvEmptyMessage.setText(getString(R.string.empty_message_no_favorite_feeds));

        feedAdapter = new FeedAdapter(feeds);
        recyclerViewFeeds.setHasFixedSize(true);
        recyclerViewFeeds.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerViewFeeds.setAdapter(feedAdapter);
        recyclerViewFeeds.addItemDecoration(new DividerItemRecyclerDecoration(getActivity(), R.drawable.canvas_recycler_divider));
        swipeRefreshLayout.setOnRefreshListener(onRefreshListener);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData();
    }

    @Override
    public void onResume() {
        super.onResume();
        currentUser = ModelManager.modelManager().getCurrentUser();
        setCartUpdate(currentUser.getShoppingCartCount());
        //appbarLayout.setVisibility(View.GONE);

    }


    SwipeRefreshLayout.OnRefreshListener onRefreshListener = this::refreshData;

    public void setCartUpdate(int count){
        if(count==0)
            tvCartCount.setVisibility(View.GONE);
        else{
            tvCartCount.setVisibility(View.VISIBLE);
            tvCartCount.setText(String.valueOf(count));
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        switcherListener = null;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @OnClick(R.id.btn_cart)
    void onCart(){
        if(switcherListener!=null)
            switcherListener.switchFragment(ShoppingCartFragment.newInstance(switcherListener),true,true);
        //startActivity(ShoppingCartActivity.getIntent(getContext()));
    }



    @OnClick(R.id.btn_back)
    void onBack() {
        getFragmentManager().popBackStack();
        /*appbarLayout.setVisibility(View.VISIBLE);
        Utils.isprofileClicked=0;*/
    }


    private void refreshData() {
        ModelManager.modelManager().getFavouriteProducts((Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<Feeds>> genericResponse) -> {
            feedAdapter.addNewItems(genericResponse.getObject());
            feedAdapter.notifyDataSetChanged();
            checkEmptyScreen();
            swipeRefreshLayout.setRefreshing(false);
        }, (Constants.Status iStatus, String message) -> {
            Toaster.toast(message);
            checkEmptyScreen();
            swipeRefreshLayout.setRefreshing(false);
        });
    }


    private void loadData() {
        showProgress(true);
        ModelManager.modelManager().getFavouriteProducts((Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<Feeds>> genericResponse) -> {
            feeds.clear();
            feeds.addAll(genericResponse.getObject());
            feedAdapter.notifyDataSetChanged();
            showProgress(false);
            checkEmptyScreen();
        }, (Constants.Status iStatus, String message) -> {
            Toaster.toast(message);
            showProgress(false);
            checkEmptyScreen();
        });
    }

    private void checkEmptyScreen() {
        if (feedAdapter.getItemCount() > 0) {
            emptyView.setVisibility(View.GONE);
        } else {
            emptyView.setVisibility(View.VISIBLE);
        }
    }


    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) progressDialog.cancel();
        }
    }

    public class FeedAdapter extends RecyclerView.Adapter<FeedAdapter.FeedViewHolder> {
        private List<Feeds> feedsList;


        private FeedAdapter(List<Feeds> items) {
            this.feedsList = items;
        }

        public void addNewItems(CopyOnWriteArrayList<Feeds> list) {
            feedsList.clear();
            feedsList.addAll(list);
            notifyDataSetChanged();
        }


        @Override
        public FeedViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_product_layout, parent, false);
            return new FeedViewHolder(layoutView);
        }

        @Override
        public void onBindViewHolder(FeedViewHolder holder, int position) {
            Feeds item = feedsList.get(position);
            holder.bindContent(item);
            String name = item.getName();
            String postingDate = getString(R.string.postedOn) + " " + item.getPostingDate();
            String sellerName = "";
            int connectionLevel = 0;
            if(item.getSeller()!=null){
                sellerName = item.getSeller().getFirstName() + " " + item.getSeller().getLastName();
                connectionLevel = item.getSeller().getConnectionLevel();
            }
            String thumbnailURL = item.getImageThumbUrl();
            String addressTxt = "No Location Available";
            if (item.getAddress() != null)
                addressTxt = item.getAddress().getCity() + ", " + item.getAddress().getState() +", "+ item.getAddress().getCountry();

            String price = item.getPrice();
            String sellingPrice = item.getSellingPrice();
            String discountTagline = item.getDiscountTagline();
            holder.tvSellingPrice.setText(getContext().getString(R.string.Rs)+sellingPrice);
            holder.tvPrice.setText(getContext().getString(R.string.Rs)+price);
            holder.tvPrice.setPaintFlags(holder.tvPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.tvOffer.setText(discountTagline);

            holder.textViewItemName.setText(name);
            holder.textViewConnection.setText(Utils.getUserName(sellerName,connectionLevel));
            holder.textViewTime.setText(postingDate);
            holder.tvStarRating.setText(String.valueOf(item.getRating()));

            Float pricefloat = Float.parseFloat(price.replace('/','0').replace('-','0'));
            Float sellingfloat = Float.parseFloat(sellingPrice.replace('/','0').replace('-','0'));

            if (pricefloat <= sellingfloat) {
                holder.tvPrice.setVisibility(View.GONE);
            }

            if (item.getFavourite()) {
                holder.imageViewFav.setImageResource(R.drawable.ic_favorite);
            } else {
                holder.imageViewFav.setImageResource(R.drawable.ic_heart_empty);
            }
            if (!item.getImageUrl().isEmpty())
                Picasso.with(getContext())
                        .load(thumbnailURL)
                        .placeholder(R.drawable.img_product_placeholder)
                        .into(holder.imageViewItem);

        }

        @Override
        public int getItemCount() {
            return feedsList.size();
        }

        class FeedViewHolder extends RecyclerView.ViewHolder {
            @BindView(R.id.tv_name)
            TextView textViewItemName;
            @BindView(R.id.tv_connection)
            TextView textViewConnection;
            @BindView(R.id.tv_price)
            TextView tvPrice;
            @BindView(R.id.tv_selling_price)
            TextView tvSellingPrice;
            @BindView(R.id.tv_offer)
            TextView tvOffer;
            @BindView(R.id.tv_time)
            TextView textViewTime;
            @BindView(R.id.iv_item)
            ImageView imageViewItem;
            @BindView(R.id.iv_fav)
            ImageView imageViewFav;
            @BindView(R.id.tv_star_rating)
            TextView tvStarRating;
            private Feeds item;


            FeedViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            @OnClick(R.id.item_view)
            void onItemSelected() {
                getFeedDetails(item.getId());
            }

            private void getFeedDetails(int productId) {

                showProgress(true);
                ModelManager.modelManager().getProductDetails(productId,(Constants.Status iStatus, GenricResponse<Feeds> genericResponse) -> {
                    showProgress(false);
                    if (switcherListener != null){
                        switcherListener.switchFragment(ProductDetailFragment.newInstance(productId, genericResponse.getObject(), switcherListener), true, true);
                    }
                }, (Constants.Status iStatus, String message) -> {
                    Toaster.toast(message);
                    showProgress(false);
                });
            }

            @OnClick(R.id.iv_fav)
            void makeFavourite() {
                ModelManager.modelManager().setFavouriteProduct(item.getId(), 0, (Constants.Status iStatus) -> {
                    feedsList.remove(getAdapterPosition());
                    notifyDataSetChanged();
                    checkEmptyScreen();
                }, (Constants.Status iStatus, String message) -> {
                    Toaster.toast(message);
                    checkEmptyScreen();
                });
            }

            @OnClick(R.id.iv_location)
            void getLocation() {
                try{
                    String lat = user.getPrimaryAddress().getLatitude();
                    String sLat = item.getAddress().getLatitude();
                    String sLon = item.getAddress().getLongitude();
                    if(!lat.isEmpty() && !sLat.isEmpty()){
                        MapDialogFragment mapDialogFragment = MapDialogFragment.newInstance();
                        Bundle bdl = new Bundle();
                        bdl.putString(kLatitude,sLat);
                        bdl.putString(kLongitude,sLon);
                        bdl.putString(kOwnerName,item.getName());
                        bdl.putString(kImageUrl,item.getSeller().getProfilePicUrl());
                        mapDialogFragment.setArguments(bdl);
                        mapDialogFragment.setCancelable(true);
                        mapDialogFragment.show(getChildFragmentManager(), mapDialogFragment.getClass().getSimpleName());

                    }else
                        Toaster.toast("Sorry Lat Long UnAvailable");
                }catch (Exception e){
                    Toaster.toast("Sorry Lat Long UnAvailable");
                    e.printStackTrace();
                }
            }




            private void bindContent(Feeds item) {
                this.item = item;
            }

        }
    }

}
