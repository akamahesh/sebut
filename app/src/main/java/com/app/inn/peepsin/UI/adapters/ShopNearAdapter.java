package com.app.inn.peepsin.UI.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.Address;
import com.app.inn.peepsin.Models.CurrentUser;
import com.app.inn.peepsin.Models.ShopModel;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.fragments.Feeds.MapDialogFragment;
import com.app.inn.peepsin.UI.fragments.Shop.NewShopDetailsFragment;
import com.app.inn.peepsin.UI.fragments.Shop.ShoppingFragment;
import com.app.inn.peepsin.UI.helpers.MyBounceInterpolator;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kImageUrl;
import static com.app.inn.peepsin.Constants.Constants.kLatitude;
import static com.app.inn.peepsin.Constants.Constants.kLongitude;
import static com.app.inn.peepsin.Constants.Constants.kOwnerName;

/**
 * Created by dhruv on 1/9/17.
 */

public class ShopNearAdapter extends RecyclerView.Adapter<ShopNearAdapter.MyViewHolder> {

    private Context mContext;
    private List<ShopModel> shopList;
    private ShoppingFragment fragment;
    private static Switcher switcherListener;

    public ShopNearAdapter(Context mContext, List<ShopModel> albumList, Switcher switcher,
                           ShoppingFragment fragment) {
        this.mContext = mContext;
        this.shopList = albumList;
        this.fragment = fragment;
        switcherListener = switcher;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_shop_near_me_layout, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        ShopModel shop = shopList.get(position);
        holder.bindContent(shop);
        String shopName = shop.getName();
        Integer connectionLevel = shop.getConnectionLevel();
        holder.tvShopName.setText(Utils.getFirstLetterInUpperCase(shopName));
        holder.tvOwnerName.setText(Utils.getFirstLetterInUpperCase(shop.getOwnerName()));
        holder.tvProductCount.setText(String.valueOf(shop.getProductCount()));
        holder.tvRating.setText(String.valueOf(shop.getRating()));
        holder.tvConnectionLevel.setText(Utils.getLevel(connectionLevel));

        if (shop.getFavorite()) {
            holder.ivLike.setImageResource(R.drawable.ic_heart);
        } else {
            holder.ivLike.setImageResource(R.drawable.ic_heart_grey);
        }

        Address address = shop.getAddress();
        String location = "No Location Available";
        if (address != null) {
            location = address.getCity() + "," + address.getState() + "," + address.getCountry();
        }
        holder.tvAddress.setText(location);

        TextDrawable drawable = TextDrawable.builder()
                .buildRoundRect(String.valueOf(shopName.toUpperCase().charAt(0)),
                        ColorGenerator.MATERIAL.getRandomColor(), 5);
        if (!shop.getBannerUrl().isEmpty()) {
            Picasso.with(mContext)
                    .load(shop.getBannerUrl()).fit()
                    .placeholder(drawable)
                    .error(drawable)
                    .into(holder.ivBannerImage);
        } else
            holder.ivBannerImage.setImageDrawable(drawable);

        if (!shop.getLogoUrl().isEmpty()) {
            Picasso.with(mContext)
                    .load(shop.getLogoUrl()).fit()
                    .placeholder(R.drawable.img_shop_logo)
                    .error(R.drawable.img_shop_logo)
                    .into(holder.ivLogoImage);
        } else
            holder.ivLogoImage.setImageResource(R.drawable.img_shop_logo);

        if (!shop.getProfileUrl().isEmpty()) {
            Picasso.with(mContext)
                    .load(shop.getProfileUrl()).fit()
                    .placeholder(R.drawable.img_profile_placeholder)
                    .error(R.drawable.img_profile_placeholder)
                    .into(holder.ivProfile);
        } else
            holder.ivProfile.setImageResource(R.drawable.img_profile_placeholder);

    }

    public void addItems(List<ShopModel> list) {
        shopList.clear();
        shopList.addAll(list);
        notifyDataSetChanged();
    }

    public void addMoreItems(List<ShopModel> list) {
        shopList.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return shopList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ShopModel item;
        @BindView(R.id.iv_banner_image)
        ImageView ivBannerImage;
        @BindView(R.id.iv_profile_image)
        ImageView ivProfile;
        @BindView(R.id.iv_shop_logo)
        ImageView ivLogoImage;
        @BindView(R.id.tv_shop_name)
        TextView tvShopName;
        @BindView(R.id.tv_owner_name)
        TextView tvOwnerName;
        @BindView(R.id.tv_address)
        TextView tvAddress;
        @BindView(R.id.iv_like)
        ImageView ivLike;
        @BindView(R.id.tv_growth_text)
        TextView tvProductCount;
        @BindView(R.id.tv_rating)
        TextView tvRating;
        @BindView(R.id.tv_level)
        TextView tvConnectionLevel;
        @BindView(R.id.near_progressBar)
        ProgressBar progressBar;
        @BindView(R.id.near_progressBar1)
        ProgressBar progressBarLoc;

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        @OnClick(R.id.card_view)
        void OnItemClick() {
            if (switcherListener != null) {
                switcherListener.switchFragment(
                        NewShopDetailsFragment.newInstance(switcherListener, item.getId()), true, true);
            }
            //FeedsFragment
        }

        @OnClick(R.id.iv_like)
        void onLike(ImageView imageView) {
            Boolean isFavorite = item.getFavorite();
            Animation favAnim = AnimationUtils.loadAnimation(mContext, R.anim.bounce);
            MyBounceInterpolator favInterpolar = new MyBounceInterpolator(.2, 20);
            favAnim.setInterpolator(favInterpolar);
            // imageView.startAnimation(favAnim);
            if (isFavorite) {
                fragment.makeshopFavorite(progressBar, ivLike, 0, item.getId(), getAdapterPosition());
            } else {
                fragment.makeshopFavorite(progressBar, ivLike, 1, item.getId(), getAdapterPosition());
            }
        }


        @OnClick(R.id.iv_location)
        void getLocation() {
            try {
                // progressBarLoc.setVisibility(View.VISIBLE);
                CurrentUser user = ModelManager.modelManager().getCurrentUser();
                String lat = user.getPrimaryAddress().getLatitude();
                String sLat = item.getAddress().getLatitude();
                String sLon = item.getAddress().getLongitude();
                if (!lat.isEmpty() && !sLat.isEmpty()) {
                    MapDialogFragment mapDialogFragment = MapDialogFragment.newInstance();
                    Bundle bdl = new Bundle();
                    bdl.putString(kLatitude, sLat);
                    bdl.putString(kLongitude, sLon);
                    bdl.putString(kOwnerName, item.getName());
                    bdl.putString(kImageUrl, item.getBannerUrl());
                    mapDialogFragment.setArguments(bdl);
                    mapDialogFragment.setCancelable(true);
                    //progressBarLoc.setVisibility(View.GONE);

                    mapDialogFragment.show(((AppCompatActivity) mContext).getSupportFragmentManager(),
                            mapDialogFragment.getClass().getSimpleName());
                } else {
                    //progressBarLoc.setVisibility(View.GONE);
                    Toaster.toast("Sorry Lat Long UnAvailable");
                }
            } catch (Exception e) {
                //progressBarLoc.setVisibility(View.GONE);

                Toaster.toast("Sorry Lat Long UnAvailable");
                e.printStackTrace();
            }
        }

        public void bindContent(ShopModel shop) {
            this.item = shop;
        }
    }

}
