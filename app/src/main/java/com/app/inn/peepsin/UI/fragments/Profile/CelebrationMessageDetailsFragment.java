package com.app.inn.peepsin.UI.fragments.Profile;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.CelebrationMessage;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import com.squareup.picasso.Picasso;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kEventMap;

/**
 * Created by dhruv on 3/10/17.
 */

public class CelebrationMessageDetailsFragment extends Fragment {

    private final String TAG = getClass().getSimpleName() + ">>>";
    private static Switcher switcherListener;

    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.ivEventImage)
    ImageView ivEventImage;
    @BindView(R.id.tv_event)
    TextView tvName;
    @BindView(R.id.tv_date)
    TextView tvDate;
    @BindView(R.id.tv_message)
    TextView tvMessage;
    @BindView(R.id.iv_like)
    ImageView ivLike;
    @BindView(R.id.tv_like)
    TextView tvLike;
    @BindView(R.id.tv_sender_name)
    TextView tvSender;
    @BindView(R.id.iv_profile_image)
    ImageView ivSender;
    @BindColor(R.color.theme_color)
    int redColor;
    @BindColor(R.color.text_color_regular)
    int greyColor;

    private ProgressDialog progressDialog;
    private CelebrationMessage message;

    public static Fragment newInstance(CelebrationMessage message, Switcher switcher) {
        switcherListener = switcher;
        CelebrationMessageDetailsFragment fragment = new CelebrationMessageDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(kEventMap,message);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = Utils.generateProgressDialog(getContext(), false);
        Bundle bdl = getArguments();
        if(bdl!=null)
            message = (CelebrationMessage) bdl.getSerializable(kEventMap);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_message_details_layout, container, false);
        ButterKnife.bind(this, view);
        tvTitle.setText(getString(R.string.message_details));
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadMessageDetails();
    }

    private void setEventInfo(CelebrationMessage message){
        tvName.setText(message.getMessageTitle());
        tvDate.setText(Utils.getTimeStampDate(message.getSchedulingTimeStamp()));
        tvSender.setText("Message : "+message.getSenderName());
        tvMessage.setText(message.getMessage());
        setMessageFavorite();

        String imageUrl = message.getEventImageUrl();
        if(!imageUrl.isEmpty())
            Picasso.with(getContext()).load(imageUrl).fit()
                    .placeholder(R.drawable.img_event_banner).into(ivEventImage);

        String senderUrl = message.getSenderImageUrl();
        if(!senderUrl.isEmpty())
            Picasso.with(getContext()).load(senderUrl).fit()
                    .placeholder(R.drawable.img_profile_placeholder).into(ivSender);
    }

    void setMessageFavorite(){
        if (message.getFavorite()) {
            ivLike.setImageResource(R.drawable.ic_thumb_up_red_24dp);
            tvLike.setTextColor(redColor);
        } else {
            ivLike.setImageResource(R.drawable.ic_thumb_up_grey_500_24dp);
            tvLike.setTextColor(greyColor);
        }
    }

    @OnClick(R.id.thanks_view)
    void onThanks(){
        if(message!=null)
            Toaster.kalaToast("Thanks: "+message.getSenderName());
    }

    @OnClick(R.id.btn_edit)
    void onEdit() {
        if(message !=null)
            openBottomSheetMenu();
    }

    @OnClick(R.id.btn_back)
    public void onBackPress() {
        getFragmentManager().popBackStack();
    }

    @OnClick(R.id.like_view)
    void eventLike(){
        Boolean isFavorite = message.getFavorite();
        if (isFavorite)
            onLike(0);
        else
            onLike(1);
    }

    void onLike(int isFav){
        ModelManager.modelManager().setFavouriteMessage(message.getMessageId(),isFav, (Constants.Status iStatus) -> {
            message.setFavorite(isFav==1);
            setMessageFavorite();
        }, (Constants.Status iStatus, String message) -> Toaster.toast(message));
    }

    private void openBottomSheetMenu() {
        LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.bottom_sheet_shop_detais, null);
        final Dialog mBottomSheetDialog = new Dialog(getContext(), R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.canvas_dialog_background_inset));
        mBottomSheetDialog.show();
        view.findViewById(R.id.view_shop_status).setVisibility(View.GONE);
        view.findViewById(R.id.view_edit_shop).setVisibility(View.GONE);

        TextView tvDelete = (TextView)view.findViewById(R.id.tv_delete_shop);
        tvDelete.setText(getString(R.string.message_delete));
        tvDelete.setOnClickListener(v -> {
            mBottomSheetDialog.dismiss();
            Utils.showAlertDialog(getContext(), "Alert Message", "Do you really want to delete message?", (dialog, which) -> deleteMessage());
        });

        view.findViewById(R.id.tv_cancel).setOnClickListener(v -> mBottomSheetDialog.dismiss());
    }

    void deleteMessage(){
        showProgress(true);
        ModelManager.modelManager().deleteCelebrationMessage(message.getMessageId(), (Constants.Status iStatus) -> {
            showProgress(false);
            onBackPress();
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Log.e(TAG,message);
            Toaster.toast(message);
        });
    }

    void loadMessageDetails(){
        showProgress(true);
        ModelManager.modelManager().getCelebrationMessageDetails(message.getMessageId(), (Constants.Status iStatus, GenricResponse<CelebrationMessage> genricResponse) -> {
            showProgress(false);
            message = genricResponse.getObject();
            setEventInfo(message);
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Log.e(TAG,message);
            Toaster.toast(message);
        });
    }

    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) progressDialog.dismiss();
        }
    }
}
