package com.app.inn.peepsin.UI.dialogFragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.helpers.Toaster;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kType;

/**
 * Created by akamahesh on 3/5/17.
 */

public class PolicyDialogFragment extends DialogFragment {

    @BindView(R.id.webview_policy)
    WebView webView;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.progress_bar) ProgressBar progressBar;
    private static int TYPE = 0;
    private static int PRIVACY_TYPE = 1;
    private static int CONDITION_TYPE = 2;
    private static int SELLER_TYPE = 3;


    /**
     * type = 1 for privacy policy
     * type = 2 for terms and condition
     *
     * @param type
     * @return PolicyDialogFragment
     */
    public static DialogFragment newInstance(int type) {
        DialogFragment dialogFragment = new PolicyDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(kType, type);
        dialogFragment.setArguments(bundle);
        return dialogFragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Black_NoTitleBar);
        Bundle bundle = getArguments();
        if (bundle != null)
            TYPE = bundle.getInt(kType);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_frament_policy, container, false);
        ButterKnife.bind(this, view);
        if (TYPE == PRIVACY_TYPE) {
            tvTitle.setText(getString(R.string.title_privacy_policy));
            loadPrivacy();
        } else if (TYPE == CONDITION_TYPE) {
            tvTitle.setText(getString(R.string.title_terms_condition));
            loadTerms();
        } else if (TYPE == SELLER_TYPE) {
            tvTitle.setText(getString(R.string.title_seller_agreement));
            loadSeller();
        }

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

    @OnClick(R.id.ib_back)
    void dismissDialog() {
        dismiss();
    }

    void loadPrivacy() {
        ModelManager.modelManager().getPrivacyPolicy((Constants.Status iStatus, GenricResponse<String> genericResponse) -> {
            String formattedHTML = genericResponse.getObject();
            String term = String.valueOf(Html.fromHtml("<![CDATA[<body style=\"text-align:justify; \">"
                    + formattedHTML
                    + "</body>]]>"));
            String text = "<html><head>"
                + "<style type=\"text/css\">body{color: #4D4C52; background-color: #fff;}"
                + "</style></head>"
                + "<body>"
                + formattedHTML
                + "</body></html>";
            webView.getSettings();
            String htmlText = " %s ";
           // webViewPolicy.loadData(String.format(htmlText, term), "text/html", "utf-8");
            webView.loadData(text,"text/html", "utf-8");
            progressBar.setVisibility(View.GONE);

        }, (Constants.Status iStatus, String message) -> {
            Toaster.toast(message);
        });
    }

    void loadTerms() {
        ModelManager.modelManager().getTermsOfUse((Constants.Status iStatus, GenricResponse<String> genericResponse) -> {
            String formattedHTML = genericResponse.getObject();
            String text = "<html><head>"
                + "<style type=\"text/css\">body{color: #4D4C52; background-color: #fff;}"
                + "</style></head>"
                + "<body>"
                + formattedHTML
                + "</body></html>";
            webView.loadData(text, "text/html", "utf-8");
            progressBar.setVisibility(View.GONE);
        }, (Constants.Status iStatus, String message) -> {
            Toaster.toast(message);
        });
    }

    void loadSeller() {
        ModelManager.modelManager().getSellerAgreement((Constants.Status iStatus, GenricResponse<String> genericResponse) -> {
            String formattedHTML = genericResponse.getObject();
            String text = "<html><head>"
                + "<style type=\"text/css\">body{color: #4D4C52; background-color: #fff;}"
                + "</style></head>"
                + "<body>"
                + formattedHTML
                + "</body></html>";
            webView.loadData(text, "text/html", "utf-8");
            progressBar.setVisibility(View.GONE);


        }, (Constants.Status iStatus, String message) -> {
            Toaster.toast(message);
        });
    }

}
