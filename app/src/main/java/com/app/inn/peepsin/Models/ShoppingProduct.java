package com.app.inn.peepsin.Models;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by root on 5/7/17.
 */

public class ShoppingProduct extends BaseModel implements Serializable{

    private Integer id;
    private String name;
    private String price;
    private String netPrice;
    private String discountTagLine;
    private Integer quantity;
    private String category;
    private String coverImageUrl;

    public ShoppingProduct(JSONObject jsonResponse){
        this.id = getValue(jsonResponse, kProductId, Integer.class);
        this.name = getValue(jsonResponse,kCartProductName,String.class);
        this.price = getValue(jsonResponse,kProductPrice,String.class);
        this.coverImageUrl = getValue(jsonResponse,kProductThumbUrl,String.class);
        try {
            this.discountTagLine = getValue(jsonResponse,kDiscountTagLine,String.class);
        }catch (Exception e){
            e.printStackTrace();
        }
        try {
            this.netPrice = getValue(jsonResponse,kNetPrice,String.class);
        }catch (Exception e){
            e.printStackTrace();
        }
        this.quantity = getValue(jsonResponse,kProductQuantity,Integer.class);
        this.category = getValue(jsonResponse,kProductCategory,String.class);
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPrice() {
        return price;
    }

    public String getNetPrice() {
        return netPrice;
    }

    public String getDiscountTagLine() {
        return discountTagLine;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public String getCategory() {
        return category;
    }

    public String getCoverImageUrl() {
        return coverImageUrl;
    }
}
