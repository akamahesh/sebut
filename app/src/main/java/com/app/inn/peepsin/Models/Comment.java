package com.app.inn.peepsin.Models;

import android.util.Log;

import com.app.inn.peepsin.Constants.Constants;

import org.json.JSONObject;

/**
 * Created by mahesh on 8/6/17.
 */

public class Comment extends BaseModel implements Constants {

    private Integer commentId;
    private Integer userId;
    private String comment;
    private String userName;
    private String time;
    private String profilePicURL;

    public Comment() {
    }

    public Comment(JSONObject jsonResponse) {
        try {
            this.userId                 = getValue(jsonResponse, kUserId,           Integer.class);
            this.commentId              = getValue(jsonResponse, kCommentId,        Integer.class);
            this.comment                = getValue(jsonResponse, kComment,          String.class);
            this.userName               = getValue(jsonResponse, kUserName,         String.class);
            this.time                   = getValue(jsonResponse, kTime,             String.class);
            this.profilePicURL          = getValue(jsonResponse, kProfilePicUrl,    String.class);
        }catch (Exception e){
            Log.e(getClass().getSimpleName(),e.getMessage()+" comment");
        }

    }

    public Integer getCommentId() {
        return commentId;
    }

    public void setCommentId(Integer commentId) {
        this.commentId = commentId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getProfilePicURL() {
        return profilePicURL;
    }

    public void setProfilePicURL(String profilePicURL) {
        this.profilePicURL = profilePicURL;
    }
}
