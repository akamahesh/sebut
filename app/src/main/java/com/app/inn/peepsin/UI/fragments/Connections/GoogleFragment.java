package com.app.inn.peepsin.UI.fragments.Connections;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.app.inn.peepsin.Models.PhoneContact;
import com.app.inn.peepsin.UI.activities.HomeActivity;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.api.services.people.v1.People;
import com.google.api.services.people.v1.PeopleScopes;
import com.google.api.services.people.v1.model.EmailAddress;
import com.google.api.services.people.v1.model.ListConnectionsResponse;
import com.google.api.services.people.v1.model.Name;
import com.google.api.services.people.v1.model.Person;
import com.google.api.services.people.v1.model.PhoneNumber;
import com.google.api.services.people.v1.model.Photo;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.DataBase.DataBaseHandler;
import com.app.inn.peepsin.Managers.APIManager.ReachabilityManager;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.CurrentUser;
import com.app.inn.peepsin.Models.GoogleContact;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.helpers.DividerItemRecyclerDecoration;
import com.app.inn.peepsin.UI.helpers.PeopleHelper;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kGoogleClientId;

/**
 * Created by akamahesh on 1/5/17.
 */

public class GoogleFragment extends Fragment implements
        GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks {

    private final String TAG = getClass().getSimpleName();
    private ProgressDialog progressDialog;
    private List<GoogleContact> googleContactList;
    private ContactAdapter contactAdapter;

    @BindView(R.id.empty_view) View emptyView;
    @BindView(R.id.tv_title) TextView tvToolbarTitle;
    @BindView(R.id.tv_empty_message) TextView tvEmptyMessage;
    @BindView(R.id.tv_empty_title) TextView tvEmptyTitle;
    @BindView(R.id.recycler_view_phone) RecyclerView recyclerView;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.edt_search) EditText edtSearch;
    @BindView(R.id.view_invite) View inviteView;
    @BindView(R.id.btn_contacts) Button btnImportContacts;

    @BindView(R.id.btn_select_all)
    TextView tvSelectAll;
    @BindView(R.id.select_view)
    View selectView;
    @BindView(R.id.ib_checkbox)
    ImageView ivCheckBox;

    private DataBaseHandler dbHandler;
    private CurrentUser user;
    private String filterString;
    private String mUserId;

    private static final int RC_SIGN_IN = 9001;
    private GoogleApiClient mGoogleApiClient;
   // AppBarLayout appbarLayout;

    public static Fragment newInstance( ){
        return new GoogleFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        googleContactList = new ArrayList<>();
        dbHandler = new DataBaseHandler(getContext());
        user = ModelManager.modelManager().getCurrentUser();
        mUserId = String.valueOf(user.getUserId());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        scopePermission();
        View view =  inflater.inflate(R.layout.fragment_google, container, false);
        ButterKnife.bind(this,view);
       /* appbarLayout=((HomeActivity) getActivity()).getTabLayout();
        Utils.isfriendClicked=1;*/

        tvToolbarTitle.setText(getString(R.string.title_gmail_contacts));
        tvEmptyMessage.setText(R.string.empty_message_gmail_contact);
        tvEmptyTitle.setText(R.string.empty_title_gmail_contact);
        progressDialog = Utils.generateProgressDialog(getContext(), true);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.addItemDecoration(new DividerItemRecyclerDecoration(getContext(), R.drawable.canvas_recycler_divider));
        contactAdapter = new ContactAdapter(googleContactList);
        recyclerView.setAdapter(contactAdapter);

        swipeRefreshLayout.setEnabled(false);
        swipeRefreshLayout.setOnRefreshListener(onRefreshListener);

        edtSearch.setOnEditorActionListener(actionListener);
        edtSearch.addTextChangedListener(textWatcher);
        return view;
    }

    SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            importContacts(btnImportContacts);
        }
    };

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        List<GoogleContact> contactList = dbHandler.getGoogleContactFromDatabase(mUserId,"0");
        List<GoogleContact> sentContactList = dbHandler.getGoogleContactFromDatabase(mUserId,"1");
        googleContactList.addAll(contactList);
        contactAdapter.notifyDataSetChanged();
        if(contactList.isEmpty()&&sentContactList.isEmpty()){
            btnImportContacts.setVisibility(View.VISIBLE);
        }else{
            btnImportContacts.setVisibility(View.GONE);
            checkEmptyState();
        }
    }

    TextView.OnEditorActionListener actionListener = (v, actionId, event) -> {
        if(actionId == EditorInfo.IME_ACTION_SEARCH){
            Utils.hideKeyboard(getContext());
        }
        return true;
    };

    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            s = s.toString().toLowerCase();
            filterString = s.toString().toLowerCase();
            contactAdapter.getFilter().filter(s);
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    @OnClick(R.id.view_invite)
    void invite(){
        CopyOnWriteArrayList<GoogleContact> contactList = new CopyOnWriteArrayList<>();
        for (GoogleContact contact: googleContactList) {
            if(contact.isSelected())
                contactList.add(contact);
        }

        if(contactList.isEmpty()){
            Toaster.toast("Please select any contact to invite ");
            return;
        }
        inviteContact(contactList);
    }

    @OnClick(R.id.select_view)
    void selectAll(){
        if(tvSelectAll.getText().toString().equals("Select All")){
            for(int i=0;i<googleContactList.size();i++){
                googleContactList.get(i).setSelected(true);
            }
            tvSelectAll.setText(getString(R.string.unselect_all));
            ivCheckBox.setImageDrawable(getResources().getDrawable(R.drawable.ic_invite_check));
        }else {
            for(int i=0;i<googleContactList.size();i++){
                googleContactList.get(i).setSelected(false);
            }
            tvSelectAll.setText(getString(R.string.select_all));
            ivCheckBox.setImageDrawable(getResources().getDrawable(R.drawable.ic_circle_thin));
        }
        contactAdapter.notifyDataSetChanged();
    }

    private void inviteContact(CopyOnWriteArrayList<GoogleContact> contactList){
        showProgress(true);
        ModelManager.modelManager().inviteGoogleContact(dbHandler,contactList, (Constants.Status iStatus) -> {
            contactAdapter.removeItems(contactList);
            contactAdapter.notifyDataSetChanged();
            contactAdapter.getFilter().filter("");
            checkEmptyState();
            showProgress(false);
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Toaster.toast(message);
            checkEmptyState();
        });
    }

    @OnClick(R.id.btn_back)
    void onBack(){
        Utils.hideKeyboard(getContext());
        getFragmentManager().popBackStack();
       /* appbarLayout.setVisibility(View.VISIBLE);
        Utils.isfriendClicked=0;*/
    }
    @Override
    public void onResume() {
        super.onResume();
       // appbarLayout.setVisibility(View.GONE);

    }
    @OnClick(R.id.btn_contacts)
    void importContacts(Button button){
        if( ReachabilityManager.getNetworkStatus()){
            button.setVisibility(View.GONE);
            mGoogleApiClient.connect();
            signIn();
        }
        else
            Toaster.toast("Please get an Internet Connection");
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    public void signOut(){
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(status -> Log.e(TAG,"Result: " +status));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }


    public void scopePermission() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestServerAuthCode(kGoogleClientId)
                .requestEmail()
                .requestScopes(new Scope(Scopes.PLUS_LOGIN),
                        new Scope(PeopleScopes.CONTACTS),
                        new Scope(PeopleScopes.CONTACTS_READONLY),
                        new Scope(PeopleScopes.USERINFO_EMAIL),
                        new Scope(PeopleScopes.USER_EMAILS_READ),
                        new Scope(PeopleScopes.USER_ADDRESSES_READ),
                        new Scope(PeopleScopes.USERINFO_PROFILE),
                        new Scope(PeopleScopes.USER_BIRTHDAY_READ),
                        new Scope(PeopleScopes.USER_PHONENUMBERS_READ))
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .addOnConnectionFailedListener(this)
                .addConnectionCallbacks(this)
                .build();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        btnImportContacts.setVisibility(View.GONE);
    }

    @Override
    public void onConnectionSuspended(int i) {
        btnImportContacts.setVisibility(View.VISIBLE);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        btnImportContacts.setVisibility(View.VISIBLE);
    }

    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            if(user.getEmail().getContactId().equals(acct.getEmail())) {
                new PeoplesAsync().execute(acct != null ? acct.getServerAuthCode() : null);
            }else {
               Utils.showAlertDialog(getContext(),"Error","Please login google with "+ user.getEmail().getContactId());
               signOut();
               mGoogleApiClient.disconnect();
               btnImportContacts.setVisibility(View.VISIBLE);
            }
            swipeRefreshLayout.setRefreshing(false);
        } else {
            Utils.showAlertDialog(getContext(),"Login Fail","Please check your connection");
            btnImportContacts.setVisibility(View.VISIBLE);
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    // Getting Google Contacts List from Google Plus
    @SuppressLint("StaticFieldLeak")
    class PeoplesAsync extends AsyncTask<String, Void, List<GoogleContact>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress(true);
        }

        @Override
        protected List<GoogleContact> doInBackground(String... params) {
            List<GoogleContact> googleContacts = new ArrayList<>();
            try {
                People peopleService = PeopleHelper.setUp(getContext(), params[0]);
                ListConnectionsResponse response = peopleService.people().connections()
                        .list("people/me")
                        .setRequestMaskIncludeField("person.names,person." +
                                "emailAddresses,person.phoneNumbers,person.photos")
                        .execute();
                List<Person> connections = response.getConnections();
                for (final Person person : connections) {
                    HashMap<String, Object> googleContactMap = new HashMap<>();
                    if (!person.isEmpty()) {
                        List<Photo> photos = person.getPhotos();
                        List<Name> names = person.getNames();
                        List<EmailAddress> emailAddresses = person.getEmailAddresses();
                        List<PhoneNumber> phoneNumbers = person.getPhoneNumbers();
                        if (photos != null)
                            for (final Photo photo : photos)
                                googleContactMap.put(Constants.kGoogleContactsPhoto, photo.getUrl());
                        if (phoneNumbers != null)
                            for (final PhoneNumber phoneNumber : phoneNumbers)
                                googleContactMap.put(Constants.kGoogleContactsPhone, phoneNumber.getValue());
                        if (emailAddresses != null)
                            for (final EmailAddress emailAddress : emailAddresses)
                                googleContactMap.put(Constants.kGoogleContactsEmail, emailAddress.getValue());
                        if (names != null)
                            for (final Name name : names)
                                googleContactMap.put(Constants.kGoogleContactsName, name.getDisplayName());
                    }
                    GoogleContact googleContact = new GoogleContact(googleContactMap);
                    googleContacts.add(googleContact);
                    if(!dbHandler.isGoogleUserExist(mUserId,googleContact.getContactId())&&!googleContact.getName().isEmpty()) {
                        Log.i(getClass().getSimpleName(),googleContact.toString());
                        dbHandler.addGoogleContacts(mUserId,googleContact.getName(),googleContact.getContactId(),googleContact.getImageURL(),"0");
                    }
                }
            } catch (NullPointerException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return googleContacts;
        }

        @Override
        protected void onPostExecute(List<GoogleContact> googleContacts) {
            super.onPostExecute(googleContacts);
            List<GoogleContact> contactList = dbHandler.getGoogleContactFromDatabase(mUserId,"0");
            googleContactList.clear();
            googleContactList.addAll(contactList);
            contactAdapter.notifyDataSetChanged();
            checkEmptyState();
            showProgress(false);
        }
    }

    private void checkEmptyState() {
        if(contactAdapter.getItemCount()>0) {
            emptyView.setVisibility(View.GONE);
            selectView.setVisibility(View.VISIBLE);
            swipeRefreshLayout.setEnabled(true);
        } else{
            emptyView.setVisibility(View.VISIBLE);
            selectView.setVisibility(View.GONE);
            swipeRefreshLayout.setEnabled(false);
        }
    }

    private void showProgress(boolean b) {
        if(b){
            progressDialog.show();
        }else {
            if(progressDialog!=null) progressDialog.cancel();
        }
    }

    class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ContactHolder> {
        private List<GoogleContact> contactList;
        private List<GoogleContact> filterList;
        private ColorGenerator generator = ColorGenerator.MATERIAL;

        ContactAdapter(List<GoogleContact> contactList) {
            this.contactList = contactList;
            this.filterList = contactList;
        }

        @Override
        public ContactAdapter.ContactHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_phone_layout, parent, false);
            return new ContactAdapter.ContactHolder(layoutView);
        }

        @Override
        public void onBindViewHolder(ContactAdapter.ContactHolder holder, int position) {
            GoogleContact contact = contactList.get(position);
            holder.bindContent(contact);
            String name = contact.getName();
            String profileURL = contact.getImageURL();

            String contactId = contact.getContactId();
            if(Utils.isValidEmail(contactId)){
                holder.ivStatusIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_email));
                holder.tvStatus.setText(getString(R.string.invite_via_email_text));
            }else{
                holder.ivStatusIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_phone));
                holder.tvStatus.setText(getString(R.string.invite_via_message_text));
            }

            if(contact.isSelected()){
                holder.ibCheckBox.setImageDrawable(getResources().getDrawable(R.drawable.ic_invite_check));
            }else {
                holder.ibCheckBox.setImageDrawable(getResources().getDrawable(R.drawable.ic_circle_thin));
            }

            holder.tvName.setText(name);
            holder.tvTitle.setText(contactId);
            contact.setContactId(contactId);
            if(profileURL.isEmpty()){
                holder.ivCircularUserImage.setVisibility(View.INVISIBLE);
                holder.ivUserImage.setVisibility(View.VISIBLE);
            }else {
                holder.ivCircularUserImage.setVisibility(View.VISIBLE);
                holder.ivUserImage.setVisibility(View.INVISIBLE);
            }


            TextDrawable drawable = TextDrawable.builder().buildRound(String.valueOf(name.toUpperCase().charAt(0)), generator.getRandomColor());

            if (!profileURL.isEmpty()) {
                Picasso.with(getContext())
                        .load(profileURL)
                        .resize(100,100)
                        .placeholder(R.drawable.img_profile_placeholder)
                        .into(holder.ivCircularUserImage);
            } else {
                holder.ivUserImage.setImageDrawable(drawable);
            }

        }

        @Override
        public int getItemCount() {
            return contactList.size();
        }

        public void removeItems(CopyOnWriteArrayList<GoogleContact> list) {
            contactList.removeAll(list);
            contactAdapter.notifyDataSetChanged();
            checkEmptyState();
        }

        private Filter getFilter() {
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence charSequence) {

                    String charString = charSequence.toString();
                    if (charString.isEmpty()) {
                        contactList = filterList;
                    } else {
                        ArrayList<GoogleContact> filteredList = new ArrayList<>();
                        for (GoogleContact contacts : contactList) {
                            if (contacts.getName().toLowerCase().contains(charString) || contacts.getContactId().toLowerCase().contains(charString)) {
                                filteredList.add(contacts);
                            }
                        }
                        contactList = filteredList;
                    }
                    FilterResults filterResults = new FilterResults();
                    filterResults.values = contactList;
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                    contactList = (List<GoogleContact>) filterResults.values;
                    notifyDataSetChanged();
                    checkEmptyState();
                }
            };
        }

        class ContactHolder extends RecyclerView.ViewHolder {
            private GoogleContact contacts;
            @BindView(R.id.tv_name) TextView tvName;
            @BindView(R.id.tv_title) TextView tvTitle;
            @BindView(R.id.tv_status) TextView tvStatus;
            @BindView(R.id.iv_status_icon) ImageView ivStatusIcon;
            @BindView(R.id.iv_user_image) ImageView ivUserImage;
            @BindView(R.id.iv_user_image_circular) ImageView ivCircularUserImage;
            @BindView(R.id.ib_checkbox) ImageButton ibCheckBox;


            ContactHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this,itemView);
            }

            @OnClick(R.id.item_view)
            void onRowSelected(){
                if(contacts.isSelected()){
                    contacts.setSelected(false);
                    ibCheckBox.setImageDrawable(getResources().getDrawable(R.drawable.ic_circle_thin));
                }else {
                    contacts.setSelected(true);
                    ibCheckBox.setImageDrawable(getResources().getDrawable(R.drawable.ic_invite_check));
                }
            }
            @OnClick(R.id.ib_checkbox)
            void onCheckBox(){
                if(contacts.isSelected()){
                    contacts.setSelected(false);
                    ibCheckBox.setImageDrawable(getResources().getDrawable(R.drawable.ic_circle_thin));
                }else {
                    contacts.setSelected(true);
                    ibCheckBox.setImageDrawable(getResources().getDrawable(R.drawable.ic_invite_check));
                }
            }

            void bindContent(GoogleContact contacts){
                this.contacts = contacts;
            }
        }
    }

}
