package com.app.inn.peepsin.Constants;

import com.app.inn.peepsin.UI.activities.ChatActivity.ChatType;

public interface Constants {

    /*****************************Public Static Constant and Keys**********************************/

    String kDefaultAppName = "PeepsIn";
    String kIsFirstTime = "isFirstTime";
    String kApiVersion = "apiVersion";
    String kAppPreferences = "PeepsInPreferences";


    String kGoogleClientId = "1034521322476-p7ngl17fl4plmu9qdgnskelusvqflitf.apps.googleusercontent.com";
    String kGoogleSecretClientId = "VnGyPac4viPadn8ZE8arHAlO";
    String kGoogleRedirectUrl = "https://peepsin-5406b.firebaseapp.com/__/auth/handler";
    /**********************API RequestParameters And ResponseParameters****************************/

    int REQUEST_CODE_AUTOCOMPLETE = 105;
    int REQUEST_LOCATION = 115;
    int PHOTO_RESULT = 200;
    int ADD_ADDRESS_CODE = 101;
    int REVIEW_RESULT = 20;
    int ADDRESS_RESULT = 24;
    int CATEGORY_RESULT = 25;
    int SUB_CATEGORY_RESULT = 26;
    int PRODUCT_RESULT = 27;
    int GALLERY_RESULT = 102;
    int SHARE_PRODUCT = 45;
    String CART_UPDATE = "cartupdate";
    String PROGRESS_UPDATE = "progress";

    //API Base Key
    String kChats = "chats";
    String kStatus = "status";
    String kMessage = "message";
    String kSuccess = "success";
    String kResult = "result";
    String kRecords = "records";
    String kRecord  ="record";
    String kPage = "page";
    String kFlag = "flag";

    String kAPNSToken = "apnsToken";
    String kCurrentUser = "currentUser";

    String kCurrentTimeStamp = "CurrentTimeStamp";
    String kTimestamp = "timeStamp";         //used in XMPP XML only

    String kResults = "results";
    String kTotalRecords = "totalRecords";

    String kAuthToken = "authToken";
    String kGenericAuthToken = "genericAuthToken";
    String kDefaultAuthToken = "defaultAuthToken";
    String kUserId = "userId";
    String kJabberId = "jabberId";

    String kReferralCode = "referralCode";
    String kIsReferralCodeValidated = "isReferralCodeValidated";
    String kReferralCodeMessage = "referralCodeMessage";

    String kFirstName = "firstName";
    String kLastName = "lastName";
    String kEmail = "email";
    String kPhone = "phone";
    String kGender = "gender";
    String kDateOfBirth = "dateOfBirth";
    String kPassword = "password";
    String kRegType = "regType";
    String kDeviceType = "deviceType";
    String kContactId = "contactId";
    String kContactIdType = "contactIdType";
    String kJoinedDate = "joinedDate";
    String kFriendsCount = "friendsCount";
    String kFriendsCountSecondLevel = "friendsCountSecondLevel";
    String kFriendsCountThirdLevel = "friendsCountThirdLevel";
    String kPeepsBoardCount = "peepsBoardCount";
    String kFavoriteProductCount = "favoriteProductCount";
    String kFavoriteShopCount = "favoriteShopCount";
    String kSellingProductCount = "sellingProductCount";
    String kPrimaryAddress = "primaryAddress";
    String kUserSocialLinks = "userSocialLinks";
    String kIsAccountActivated = "isAccountActivated";
    String kIsAlreadyRegistered = "isAlreadyRegistered";

    String kSocialId = "socialId";
    String kIsNewUser = "isNewUser";
    String kIsVerified = "isVerified";
    String kIsValidate = "isValidate";
    String kRequestType = "requestType";

    // Common Constants
    String kInventoryId = "inventoryId";
    String kUserImageURL = "userImageURL";
    String kProfilePic = "profilePic";
    String kProfilePicUrl = "profilePicURL";
    String kIsFavorite = "isFavorite";
    String kFavouriteCount = "favouriteCount";
    String kShareCount = "shareCount";
    String kSuggestionTag = "suggesstionTag";
    String kFormattedHtml = "formattedHTML";
    String kUserConnectionStatus = "userConnectionStatus";


    //Address constants
    String kAddress = "address";
    String kAddressId = "addressId";
    String kFullName = "fullName";
    String kStreetAddress = "streetAddress";
    String kCity = "city";
    String kState = "state";
    String kCountry = "country";
    String kZipcode = "zipCode";
    String kIsPrimary = "isPrimary";
    String kLatitude = "lat";
    String kLongitude = "long";

    String kMedia = "media";
    String kMediaType = "mediaType";
    String kRequestId = "requestId";
    String kMediaIndex = "mediaIndex";
    String kMediaUploadId = "mediaUploadId";

    String kCoverImageUploadId = "coverImageUploadId";
    String kImageUploadId1 = "image1ImageUploadId";
    String kImageUploadId2 = "image2ImageUploadId";
    String kImageUploadId3 = "image3ImageUploadId";
    String kImageUploadId4 = "image4ImageUploadId";
    String kImageUploadId5 = "image5ImageUploadId";

    //Shop constants
    String kShop = "shop";
    String kShopId = "shopId";
    String kShopName = "shopName";
    String kSharedBy = "sharedBy";
    String kShopBannerImage = "shopBannerImage";
    String kShopBannerImageURl = "shopBannerImageURL";
    String kShopLogoImage = "shopLogoImage";
    String kShopLogoImageURL = "shopLogoImageURL";
    String kShopAddress = "shopAddress";
    String kOwnerName = "ownerName";
    String kOwnerFirstName = "ownerFirstName";
    String kOwnerLastName = "ownerLastName";
    String kFavoriteCount = "favoriteCount";
    String kProductCount = "productCount";
    String kReviewCount = "reviewCount";
    String kReceivedOrderCount = "receivedOrderCount";
    String kShopDetails = "shopDetails";
    String kShopStatus = "shopStatus";
    String kBusinessType = "businessType";
    String kShopType = "shopType";
    String kVisibilityRange = "visibilityRange";
    String kVisibilityLevel = "visibilityLevel";

    //Shop KYC
    String kKycInfo = "kycInfo";
    String kPAN = "PAN";
    String kTAN = "TAN";
    String kGST = "GST";

    //Company Info
    String kCompanyRegistrationDetail = "companyRegistrationDetail";
    String kCompanyCinNumber = "companyCinNumber";
    String kCompanyRegisteredAddress = "companyRegisteredAddress";

    //Bank Details
    String kBankDetails = "bankDetails";
    String kAccountHolderName = "accountHolderName";
    String kAccountNumber = "accountNumber";
    String kbankId = "bankId";
    String kBankName = "bankName";
    String kIFSC = "IFSC";
    String kAccountType = "accountType";

    //Product Type
    String kProductType = "productType";
    String kProductTypeId = "productTypeId";
    String kProductTypeName = "productTypeName";
    String kProductTypeStore = "productTypeStore";
    String kProductTypeImageURL = "productTypeImageURL";

    //Delivery Type
    String kDeliveryTimeId = "deliveryTimeId";
    String kDeliveryTime = "deliveryTime";

    //Shop Category
    String kCategoryName = "categoryName";
    String kCategoryImageURL = "categoryImageURL";

    //Shopping Cart
    String kTotalCartPrice = "totalCartPrice";
    String kCartItemCount = "cartItemCount";
    String kPromoCode = "promoCode";
    String kIsValid = "isValid";

    //Shopping Product
    String kProductList = "productList";
    String kTotalProductCount = "totalProductCount";
    String kCartProductName = "productName";
    String kDiscountTagLine = "discountTagLine";
    String kSpecification = "specification";
    //String kProductQuantity = "productQuantity";

    //Share Product Constants
    String kWantToShare2ndLevel = "wantToShare2ndLevel";
    String kWantToShare3ndLevel = "wantToShare3ndLevel";

    //Rating product
    String kRating = "rating";
    String kTotalRatingCount = "totalRatingCount";
    String kMyRating = "myRating";
    String kMyReview = "myReview";
    String kSeperateRatingReview = "seperateRatingReview";
    String kFirstRating = "1";
    String kSecondRating = "2";
    String kThirdRating = "3";
    String kFourthRating = "4";
    String kFifthRating = "5";
    String kReview = "review";
    String kRatingReviewId = "ratingReviewId";
    String kTimeStamp = "timeStamp";
    String kReviewList = "reviewList";

    //Products constants

    String kOtherImageURLList = "otherImageURLList";
    String kOtherImageThumbnailURLList = "otherImageThumbnailURLList";

    String kProduct = "product";
    String kProductRef = "product";
    String kProductId = "productId";
    String kName = "name";
    String kProductName = "productName";
    String kProductDesc = "description";
    String kProductImageUrl = "coverImageURL";
    String kCoverImageUrl = "coverImageURL";
    String kProductThumbUrl = "coverImageThumbnailURL";
    String kProductThumbnailURL = "productThumbnailURL";
    String kProductImageUrlList = "otherImageURLList";
    String kProductThumbUrlList = "otherImageThumbnailURLList";
    String kPostingDate = "postingDate";
    String kProductCategory = "productCategory";
    String kProductCategoryId = "productCategoryId";
    String kProductSubCategory = "productSubcategory";
    String kProductSubCategoryId = "productSubcategoryId";
    String kProductPrice = "price";
    String kProductSellPrice = "sellingPrice";
    String kProductAddress = "productAddress";
    String kFavProducts = "favoriteProducts";
    String kQuantity  = "quantity";
    String kSKUNumber = "SKUNumber";
    String kProductStatus = "productStatus";
    String kProductIdList = "productIdList";
    String kItemQuantity  = "Quantity";
    String kReasonOfFailure = "reasonOfFailure";
    String kStockStatus = "stockStatus";
    String kNetPrice = "netPrice";
    String kTotalTax = "totalTax";
    String kProductBasicPrice = "productBasicPrice";

    //Category Constants
    String kCategoryStore   = "categoryStore";
    String kCategoryId      = "categoryId";
    String kCategory        = "category";

    //tax
    String kTax         = "tax";
    String kTaxList     = "taxList";
    String kTaxId       = "taxId";
    String kTaxName     = "taxName";
    String kTaxValue    = "taxValue";
    String kTaxType     = "taxType";


    //SubCategory Constants
    String kSubCategoryId = "subcategoryId";
    String kSubCategory = "subCategory";
    String ksubCategoryList ="subCategoryList";

    //Seller Details constants
    String kSellerDetails = "sellerDetails";
    String kConnectionLevel = "connectionLevel";

    //Add and Edit Product constants
    String kCoverImage = "coverImage";
    String kCoverImage1 = "image1";
    String kCoverImage2 = "image2";
    String kCoverImage3 = "image3";
    String kCoverImage4 = "image4";
    String kCoverImage5 = "image5";

    //Feed and Editable
    String kFeeds = "feedList";
    String kImageUrl = "url";
    String kImagePath = "path";
    String kEditable = "editable";

    //Celebration Event
    String kEvent = "event";
    String kEventId = "eventId";
    String kEventType = "eventType";
    String kEventTypeName = "eventTypeName";
    String kEventTitle = "eventTitle";
    String kSchedulingTimestamp = "schedulingTimestamp";
    String kTimeZone = "timeZone";
    String kNotifyMeDuration = "notifyMeDuration";
    String kRepetitionInterval = "repetitionInterval";
    String kCelebrationMode = "celebrationMode";
    String kImage = "image";
    String kImageId = "imageId";
    String kImageURL = "imageURL";
    String kEventMap = "eventMap";

    //Celebration Message
    String kMSGId = "messageId";
    String kMessageTitle = "messageTitle";
    String kEventCelebrationimageURl = "evenCelebrationimageURL";
    String kSenderImageUrl = "senderImageUrl";
    String kSenderName = "senderName";

    //Skills
    String kSkill = "skill";
    String kSkillId = "skillId";
    String kSkillArea = "skillArea";
    String kSkillAreaId = "skillAreaId";
    String kUserSkillId = "userSkillId";
    String kExperienceLevel = "experienceLevel";
    String kExperienceLevelId = "experienceLevelId";
    String kYearsOfExperience = "yearsOfExperience";
    String kYearsOfExperienceId = "yearsOfExperienceId";
    String kDescription = "description";

    //Dialog constants
    String KSCREENWIDTH = "screenwidth";
    String KSCREENHEIGHT = "screenheight";

    //connections
    String kConnections = "connections";
    String kUserIdList = "userIdList";
    String kIsUserBlocked = "isUserBlocked";
    String kBlockedUsers = "blockedUsers";
    String kOnlineSearchString = "searchString";

    //Request
    String kRequestSentMode = "requestSentMode";
    String kConnectionRequestReceived = "connectionRequestReceived";
    String kConnectionRequestSent = "connectionRequestSent";

    //Connection Contact
    String kContactList = "contactList";
    String kContactType = "contactType";

    //Google Contact api
    String kGoogleContactsAppName = "PeepsIn";
    String kGoogleContactsEmail = "emailAddresses";
    String kGoogleContactsPhone = "phoneNumbers";
    String kGoogleContactsName = "name";
    String kGoogleContactsPhoto = "photos";

    //Facebook Contact api
    String kFacebookCommanFriends = "friends";
    String kFacebookCommanData = "data";
    String kFacebookCommanNameKey = "name";
    String kFacebookCommanIdKey = "id";
    String kFacebookCommanImageLink = "http://graph.facebook.com/";
    String kFacebookCommanImageSize = "/picture?type=large";
    String kFacebookCommanFields = "fields";
    String kFacebookCommanAllFields = "email,first_name,last_name,gender,id,link,name,friends";

    //Google map api constants
    String kPremise = "premise";
    String kStreetNumber = "street_number";
    String kRoute = "route";
    String kLocality = "locality";
    String kAdministrativeAreaLevel2 = "administrative_area_level_2";
    String kAdministrativeAreaLevel1 = "administrative_area_level_1";
    String kPostalCode = "postal_code";


    //PeepsBoard constants
    String kBoardId = "boardId";
    String kBoardtTime = "boardTime";
    String kLikeCount = "likeCount";
    String kCommentCount = "commentCount";
    String kLastComment = "lastComment";
    String kIsLike = "isLike";

    String kCommentId = "commentId";
    String kUserName = "userName";
    String kComment = "comment";
    String kComments = "comments";
    String kTime = "time";


    /*Board's Message key*/
    String kMessageId = "MessageId";
    String kMessageDirection = "MessageDirection";
    String kMessageStatus = "MessageStatus";
    String kMessageType = "MessageType";

    //api chat
    String MESSAGE_ID = "messageId";
    String MESSAGE_TYPE = "messageType";
    String MESSAGE_DIRECTION = "messageDirection";


    /*privacy and setting key*/
    String kEmailVisibility = "emailVisibility";
    String kPhoneVisibility = "phoneVisibility";
    String kPrimaryLocationVisibility = "primaryLocationVisibility";
    String kProfileVisibility = "profileVisibility";
       //used in version 0
    /*String kSeeMyProducts = "seeMyProducts";
    String kOtherProductVisibility = "otherProductVisibility";*/
      //used in version 1
    String kSeeMyProducts = "seeMyShop";
    String kOtherProductVisibility = "otherShopVisibility";


    /*Search*/
    String kSearchString = "searchString";


    /*Payment through Paytm*/
    // String kOrderId = "orderId";
    String kMID = "MID";
    String kORDER_ID = "ORDER_ID";
    String kCUST_ID = "CUST_ID";
    String kINDUSTRY_TYPE_ID = "INDUSTRY_TYPE_ID";
    String kCHANNEL_ID = "CHANNEL_ID";
    String kTXN_AMOUNT = "TXN_AMOUNT";
    String kWEBSITE = "WEBSITE";
    String kCALLBACK_URL = "CALLBACK_URL";
    String kCHECKSUMHASH = "CHECKSUMHASH";
    String kChecksumDetails = "checksumDetails";

    //Order Constants
    String kOrderList = "orderList";
    String kOrderId = "orderId";
    String kOrderStatusId = "orderStatusId";
    String kOrderStatus = "orderStatus";
    String kOrderDate = "orderDate";
    String kPaymentType = "paymentType";

    String kIsSuccessfullyCanceled = "isSuccessfullyCanceled";
    String kIsSuccessfullyRemoved = "isSuccessfullyRemoved";

    /*Final Bill Summary*/
    String kDeliveryAddress = "deliveryAddress";
    String kFProductName = "productName";
    String kPrice = "price";
    String kPromoCodeTagLine = "promoCodeTagLine";
    String kTotalCartPriceBeforePromoCode = "totalCartPriceBeforePromoCode";
    String kFinalAmountAfterPromoCode = "finalAmountAfterPromoCode";
    String kDiscountedPrice = "discountedPrice";
    String kTotalItemCount = "totalItemCount";
    String kSellingPrice = "sellingPrice";
    String kProductQuantity = "productQuantity";
    String kCoverImageThumbnailURL = "coverImageThumbnailURL";
    String kShopid = "Shopid";
    String kshopName = "shopName";
    String kShopBannerImageURL = "shopBannerImageURL";
    String kTotalAmountWithoutDiscount = "totalAmountWithoutDiscount";
    String kNetPaybaleAmount = "netPaybaleAmount";
    String kInvoiceURL = "invoiceURL";
    String kExpectedDeliveryDate = "expectedDeliveryDate";

 /*Payment through PayUMoney*/
    String kPFirstName = "firstname";
    String kKey = "key";
    String kMerchantId = "merchantId";
    String kAmount = "amount";
    String kProductinfo = "productinfo";
    String kSurl = "surl";
    String kFurl = "furl";
    String kTxnid = "txnid";
    String kEnvironment = "environment";
    String kSalt = "salt";
    String kHash = "hash";
    String kPayUmoneyHashDetails= "payUmoneyHashDetails";
    /**********************API RequestParameters And ResponseParameters****************************/


    /****************************************Notification Identifier**************************************/
    String kNotificaitonMessageDidReceived = "com.epazer.xmpp.messageDidReceived";
    String kNotificaitonMediaUploaded = "com.epazer.xmpp.MediaUploaded";
    /****************************************Notification Identifier**************************************/

    String kDATABASE_NAME = "SQLiteDatabas.db";

    //TODO - Need to send to BaseModel


    String kSeperator = "__";

    String kEmptyString = "";
    String kWhitespace = " ";
    Number kEmptyNumber = 0;

    String kMessageInternalInconsistency = "Some internal inconsistency occurred. Please try again.";
    String kMessageNetworkError = "Device does not connect to internet.";
    String kSocketTimeOut = "PeepsIn Server not responding..";
    String kMessageServerNotRespondingError = "PeepsIn server not responding!";
    String kMessageConnecting = "Connecting...";
    String kError = "Error";


    String kErrorNickNameAlreadyExist = "Nick Name you provide already exist";
    String kErrorEmailAlreadyExist = "Email you provide already exist";
    String kErrorPhoneAlreadyExist = "Phone you provide already exist";


    /********************************File Manager constants****************************************/

    String kPlaceHolderUrl = "http://placehold.it/200x200";
    String kPlaceHolderThumbnails = "http://placehold.it/150/";

    //Push notificaitons
    String kData = "data";
    String kType = "type";
    String kId = "Id";
    String kNotification = "notification";
    String kBody = "body";
    String kTitle = "title";
    String kReload = "reload";


    String kViewType = "viewType";
    String kPictureList = "picList";
    String kSize = "size";


    String kOTP = "OTP";
    //google map contstants
    String kDistance = "distance";
    String kDuration = "duration";
    String kText = "text";


    String kShareLink="shareLink";



    //regarding the updation of app.
    String kCurrentVersion="currentVersion";
    String kBuildNumber="buildNumber";
    String kVersionNumber="versionNumber";
    String kVersionDescription="versionDescription";
    String kIsMandatoryUpdate="isMandatoryUpdate";
    String kRolloutTimestamp="rolloutTimestamp";
    String kVersionMandatoryTimestamp="versionMandatoryTimestamp";
    String kBuildType="buildType";
    String kMinOSVersion="minOSVersion";
    String kMaxOSVersion="maxOSVersion";
    String kLanguageSupported="languageSupported";






    /*****************************Public Static Constant and Keys**********************************/

    /**
     * Message direction Enum
     */
    enum MessageDirection {
        send(2),
        receive(1);


        private int value;

        MessageDirection(int value) {
            this.value = value;
        }

        /**
         * Convert int to MessageDirection
         */
        public static MessageDirection getMessageDirection(int value) {
            for (MessageDirection direction : MessageDirection.values()) {
                if (direction.value == value) {
                    return direction;
                }
            }
            return send;
        }

        /**
         * To get Integer value of corresponding emum
         */
        public Integer getValue() {
            return this.value;
        }
    }

    enum MessageStatus {
        error(0),              /* an error occured */
        sending(1),          /*sending message is in progress*/
        send(2),            /* message has been sent to server  */
        sent(3),            /* message has been arrived at receivers end */
        uploadedToSever(4);         /*Image uploaded to server*/

        private int value;

        MessageStatus(int value) {
            this.value = value;
        }

        /**
         * Convert int to MessageStatus
         */
        public static MessageStatus getMessageStatus(int value) {
            for (MessageStatus status : MessageStatus.values()) {
                if (status.value == value) {
                    return status;
                }
            }
            return sending;
        }

        /**
         * To get Integer value of corresponding emum
         */
        public Integer getValue() {
            return this.value;
        }
    }

    enum ConversationStatus {
        publicStatus(0),
        privateStatus(1),
        noNotificationStatus(2);

        private int value;

        ConversationStatus(int value) {
            this.value = value;
        }

        /**
         * Convert int to ConversationStatus
         */
        public static ConversationStatus getConversationStatus(int value) {
            for (ConversationStatus status : ConversationStatus.values()) {
                if (status.value == value) {
                    return status;
                }
            }
            return publicStatus;
        }

        /**
         * To get Integer value of corresponding emum
         */
        public Integer getValue() {
            return this.value;
        }
    }

    /**
     * Http Status for API Response
     */
    enum HTTPStatus {
        success(200),
        badRequest(400),
        unauthorized(401),
        notFound(404),
        methodNotAllowed(405),
        notAcceptable(406),
        proxyAuthenticationRequired(407),
        requestTimeout(408),
        error(-100);         //No option found.

        //Defination
        private int httpStatus;

        HTTPStatus(int httpStatus) {
            this.httpStatus = httpStatus;
        }

        public static HTTPStatus getStatus(int status) {
            for (HTTPStatus httpStatus : HTTPStatus.values()) {
                if (httpStatus.httpStatus == status) {
                    return httpStatus;
                }
            }
            return error;
        }

        public Integer getValue() {
            return this.httpStatus;
        }
    }

    /**
     * Status Enumration for Task Status
     */
    enum Status {
        success(0),
        fail(1),
        reachLimit(2),
        noChange(3),
        history(4),            //If xmpp message is history
        normal(5),            //If Normal xmpp message
        discard(6);

        //Defination
        private int value;

        Status(int status) {
            this.value = status;
        }

        public static Status getStatus(int value) {
            for (Status status : Status.values()) {
                if (status.value == value) {
                    return status;
                }
            }
            return fail;
        }

        /**
         * To get Integer value of corresponding emum
         */
        public Integer getValue() {
            return this.value;
        }
    }

    /**
     * This enum is used for message type
     */
    enum MessageType {
        text(0),
        image(1),
        audio(2),
        video(3),
        file(4),                //image file, video file, audio file
        history(5),
        none(6);

        private int value;

        MessageType(int value) {
            this.value = value;
        }

        /**
         * Convert int to Gender Type
         */
        public static MessageType getMessageType(int value) {
            for (MessageType messageType : MessageType.values()) {
                if (messageType.value == value) {
                    return messageType;
                }
            }
            return none;
        }

        /**
         * To get Integer value of corresponding emum
         */
        public Integer getValue() {
            return this.value;
        }
    }


    public enum ContactIDType {
        EMAIL, PHONE
    }

    /**
     * This enum is used for message type
     */
    enum ChatON {
        user(1),
        product(2),
        none(0);

        private int value;

        ChatON(int value) {
            this.value = value;
        }

        /**
         * Convert int to Gender Type
         */
        public static ChatON getMessageType(int value) {
            for (ChatON chatON : ChatON.values()) {
                if (chatON.value == value) {
                    return chatON;
                }
            }
            return none;
        }

        /**
         * To get Integer value of corresponding emum
         */
        public Integer getValue() {
            return this.value;
        }
    }


    /**
     * This enum is used for message type
     * 0 unpublish,  1 publish, 2 Active, 3 InActive,5 delete
     */
    enum ProductStatus {
        unpublish(0),
        publish(1),
        active(2),
        inActive(3),
        delete(5),
        draft(6),
        none(7);

        private int value;

        ProductStatus(int value) {
            this.value = value;
        }

        /**
         * Convert int to Gender Type
         */
        public static ProductStatus getProductStatus(int value) {
            for (ProductStatus productStatus : ProductStatus.values()) {
                if (productStatus.value == value) {
                    return productStatus;
                }
            }
            return none;
        }

        /**
         * To get Integer value of corresponding emum
         */
        public Integer getValue() {
            return this.value;
        }
    }
    /**
     * This enum is used for message type
     * shopStatus		[Int] {1: Pending for Approval, 2: Waiting for Customer, 3: Pending Document Verification, 4: Pending Document, 5: Approved,   6: Active,   7: Inactive}
     */
    enum ShopStatus {
        pendingForApproval(1),
        waitingForCustomer(2),
        pendingDocumentVerification(3),
        pendingDocument(4),
        approved(5),
        active(6),
        inactive(7),
        none(0);

        private int value;

        ShopStatus(int value) {
            this.value = value;
        }

        /**
         * Convert int to Gender Type
         */
        public static ShopStatus getShopStatus(int value) {
            for (ShopStatus shopStatus : ShopStatus.values()) {
                if (shopStatus.value == value) {
                    return shopStatus;
                }
            }
            return none;
        }

        /**
         * To get Integer value of corresponding emum
         */
        public Integer getValue() {
            return this.value;
        }
    }

    /**
     * This enum is used for order status
     */
    enum OrderStatus {
        newOrder(1),
        inProgress(2),
        outDelivery(3),
        delivered(4),
        cancelled(5),
        declined(6),
        returned(7);

        private int value;

        OrderStatus(int value) {
            this.value = value;
        }

        /**
         * Convert int to Gender Type
         */
        public static OrderStatus getMessageType(int value) {
            for (OrderStatus orderStatus : OrderStatus.values()) {
                if (orderStatus.value == value) {
                    return orderStatus;
                }
            }
            return newOrder;
        }

        /**
         * To get Integer value of corresponding emum
         */
        public Integer getValue() {
            return this.value;
        }
    }


    /**
     * enum For MediaType
     */
    enum MediaType {
        image(1),
        audio(2),
        video(3),
        file(4);

        private int value;

        MediaType(int value) {
            this.value = value;
        }

        /**
         * Convert int to Media Type
         */
        public static MediaType getMediaType(int value) {
            for (MediaType mediaType : MediaType.values()) {
                if (mediaType.value == value) {
                    return mediaType;
                }
            }
            return image;
        }

        /**
         * To get Integer value of corresponding emum
         */
        public Integer getValue() {
            return this.value;
        }
    }

    enum Gender {
        male(1),
        female(2),
        other(3);
        //Defination
        private int value;

        Gender(int gender) {
            this.value = gender;
        }

        /**
         * Convert int to Gender Type
         */
        public static Gender getGender(int value) {
            for (Gender gender : Gender.values()) {
                if (gender.value == value) {
                    return gender;
                }
            }
            return male;
        }

        /**
         * To get Integer value of corresponding emum
         */
        public Integer getValue() {
            return this.value;
        }
    }

    enum UserRegType {
        system(1),
        facebook(2),
        google(3),
        twitter(4);

        private int value;

        UserRegType(int regType) {
            this.value = regType;
        }

        /**
         * Convert int to UserRegType Type
         */
        public static UserRegType getRegType(int value) {
            for (UserRegType regType : UserRegType.values()) {
                if (regType.value == value) {
                    return regType;
                }
            }
            return system;
        }

        /**
         * To get Integer value of corresponding emum
         */
        public Integer getValue() {
            return this.value;
        }
    }

    enum ContactType {
        system(1),
        facebook(2),
        google(3),
        twitter(4),
        phone(5);

        private int value;

        ContactType(int contactType) {
            this.value = contactType;
        }

        /**
         * Convert int to DeviceType Type
         */
        public static ContactType getContactType(int value) {
            for (ContactType contactType : ContactType.values()) {
                if (contactType.value == value) {
                    return contactType;
                }
            }
            return system;
        }

        /**
         * To get Integer value of corresponding emum
         */
        public Integer getValue() {
            return this.value;
        }
    }

    enum DeviceType {
        iOS(1),
        android(2);

        private int value;

        DeviceType(int deviceType) {
            this.value = deviceType;
        }

        /**
         * Convert int to DeviceType Type
         */
        public static DeviceType getDeviceType(int value) {
            for (DeviceType deviceType : DeviceType.values()) {
                if (deviceType.value == value) {
                    return deviceType;
                }
            }
            return android;
        }

        /**
         * To get Integer value of corresponding emum
         */
        public Integer getValue() {
            return this.value;
        }
    }

    enum GroupType {
        publicGroup(1),
        privateGroup(2);

        private int value;

        GroupType(int groupType) {
            this.value = groupType;
        }

        /**
         * Convert int to GroupType Type
         */
        public static GroupType getGroupType(int value) {
            for (GroupType groupType : GroupType.values()) {
                if (groupType.value == value) {
                    return groupType;
                }
            }
            return publicGroup;
        }

        /**
         * To get Integer value of corresponding emum
         */
        public Integer getValue() {
            return this.value;
        }
    }

    enum NotificationType {
        groupInvitationReceived(1),
        groupRequestApproved(2),
        groupRequestRejected(3),
        groupSubscriptionCancelled(4),
        friendRequestReceived(5),
        friendRequestAccepted(6),
        connectionRemoved(7);

        private int value;

        NotificationType(int i) {
            this.value = i;
        }


    }

    enum GeoAddressType {
        premise(1),
        street_number(2),
        route(3),
        locality(4),
        administrative_area_level_2(5),
        administrative_area_level_1(6),
        country(7),
        postal_code(8);

        private int value;

        GeoAddressType(int deviceType) {
            this.value = deviceType;
        }

        /**
         * Convert int to DeviceType Type
         */
        public static GeoAddressType geoAddressType(int value) {
            for (GeoAddressType geoAddressType : GeoAddressType.values()) {
                if (geoAddressType.value == value) {
                    return geoAddressType;
                }
            }
            return premise;
        }

        /**
         * To get Integer value of corresponding emum
         */
        public Integer getValue() {
            return this.value;
        }

    }


}