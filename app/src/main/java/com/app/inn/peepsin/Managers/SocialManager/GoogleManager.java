package com.app.inn.peepsin.Managers.SocialManager;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.app.inn.peepsin.Models.SocialUser;
import com.app.inn.peepsin.UI.helpers.Toaster;

/**
 * Created by mahesh on 19/4/17.
 */

public class GoogleManager implements  GoogleApiClient.OnConnectionFailedListener,GoogleApiClient.ConnectionCallbacks {

    public static final int RC_SIGN_IN = 9001;
    private GoogleApiClient mGoogleApiClient;
    private AppCompatActivity activity;
    private GoogleManagerInterface googleManagerListener;

    public GoogleManager(AppCompatActivity context, GoogleManagerInterface googleManagerListener) {
        this.activity = context;
        this.googleManagerListener=googleManagerListener;
        googleLoginPermission();
    }

    private void googleLoginPermission(){
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(activity)
                .enableAutoManage(activity,this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    public void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        activity.startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    public void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();
            SocialUser googleUser = new SocialUser(acct);
            googleManagerListener.success(googleUser);

        } else {
            signOut();
        }
    }

    private void signOut(){
        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(status -> {
            googleManagerListener.failure();
        });
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Toaster.toast("onConnected "+bundle);
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        googleManagerListener.failure();
    }

    public interface GoogleManagerInterface{
        void success(SocialUser googleUser);
        void failure();
    }

}
