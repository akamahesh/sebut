package com.app.inn.peepsin.UI.fragments.Profile;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.CelebrationEvent;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.adapters.CelebrationEventAdapter;
import com.app.inn.peepsin.UI.helpers.DividerItemRecyclerDecoration;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kFullName;
import static com.app.inn.peepsin.Constants.Constants.kUserId;

/**
 * Created by dhruv on 28/9/17.
 */

public class CelebrationEventFragment extends Fragment {
    private ProgressDialog progressDialog;
    private CelebrationEventAdapter eventAdapter;
    private static Switcher profileSwitcherListener;

    @BindView(R.id.recycler_view_events)
    RecyclerView recyclerView;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.empty_view)
    View emptyView;
    @BindView(R.id.fab)
    FloatingActionButton fab;

    private int mUserId = 0;
    private String mUserName = "";

    public static CelebrationEventFragment newInstance(int userId, String name, Switcher switcher) {
        CelebrationEventFragment fragment = new CelebrationEventFragment();
        profileSwitcherListener = switcher;
        Bundle bundle = new Bundle();
        bundle.putString(kFullName, name);
        bundle.putInt(kUserId, userId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = Utils.generateProgressDialog(getContext(), false);
        Bundle bundle = getArguments();
        if (bundle != null) {
            mUserId = bundle.getInt(kUserId);
            mUserName = bundle.getString(kFullName);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_event_list_layout, container, false);
        ButterKnife.bind(this, view);

        List<CelebrationEvent> itemList = new ArrayList<>();
        String title = mUserName + "'s Events";
        tvTitle.setText(title);

        eventAdapter = new CelebrationEventAdapter(getContext(), itemList, true, profileSwitcherListener);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(eventAdapter);
        recyclerView.addItemDecoration(new DividerItemRecyclerDecoration(getActivity(), R.drawable.canvas_recycler_divider));
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadEvents();
    }

    private void loadEvents() {
        showProgress(true);
        ModelManager.modelManager().getCelebrationEventList(mUserId,(Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<CelebrationEvent>> genricResponse) -> {
            CopyOnWriteArrayList<CelebrationEvent> events = genricResponse.getObject();
            if (events.size()!=0) {
                eventAdapter.addItems(events);
            }
            checkEmptyView();
            showProgress(false);
        }, (Constants.Status iStatus, String message) -> {
            Toaster.toast(message);
            checkEmptyView();
            showProgress(false);
        });
    }

    @OnClick(R.id.fab)
    void addSkills() {
        if (profileSwitcherListener != null)
            profileSwitcherListener.switchFragment(AddEventFragment.newInstance(mUserId,null,1,profileSwitcherListener), true, true);
    }

    @OnClick(R.id.btn_back)
    void onBack() {
        getFragmentManager().popBackStack();
    }

    private void checkEmptyView(){
        if(eventAdapter.getItemCount()>0)
            emptyView.setVisibility(View.GONE);
        else
            emptyView.setVisibility(View.VISIBLE);
    }

    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) progressDialog.cancel();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        profileSwitcherListener = null;
    }

}
