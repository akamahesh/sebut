package com.app.inn.peepsin.UI.fragments.SellStuffModule;

import static com.app.inn.peepsin.Constants.Constants.CATEGORY_RESULT;
import static com.app.inn.peepsin.Constants.Constants.KSCREENHEIGHT;
import static com.app.inn.peepsin.Constants.Constants.KSCREENWIDTH;
import static com.app.inn.peepsin.Constants.Constants.SUB_CATEGORY_RESULT;
import static com.app.inn.peepsin.Constants.Constants.kDiscountTagLine;
import static com.app.inn.peepsin.Constants.Constants.kName;
import static com.app.inn.peepsin.Constants.Constants.kNetPrice;
import static com.app.inn.peepsin.Constants.Constants.kPictureList;
import static com.app.inn.peepsin.Constants.Constants.kProductBasicPrice;
import static com.app.inn.peepsin.Constants.Constants.kProductCategory;
import static com.app.inn.peepsin.Constants.Constants.kProductCategoryId;
import static com.app.inn.peepsin.Constants.Constants.kProductDesc;
import static com.app.inn.peepsin.Constants.Constants.kProductPrice;
import static com.app.inn.peepsin.Constants.Constants.kProductSellPrice;
import static com.app.inn.peepsin.Constants.Constants.kProductStatus;
import static com.app.inn.peepsin.Constants.Constants.kProductSubCategory;
import static com.app.inn.peepsin.Constants.Constants.kProductSubCategoryId;
import static com.app.inn.peepsin.Constants.Constants.kProductType;
import static com.app.inn.peepsin.Constants.Constants.kQuantity;
import static com.app.inn.peepsin.Constants.Constants.kSKUNumber;
import static com.app.inn.peepsin.Constants.Constants.kShopId;
import static com.app.inn.peepsin.Constants.Constants.kSpecification;
import static com.app.inn.peepsin.Constants.Constants.kTotalTax;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.CurrentUser;
import com.app.inn.peepsin.Models.ProductType;
import com.app.inn.peepsin.Models.ShopCatSubCategory;
import com.app.inn.peepsin.Models.TAX;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.Views.SFDLTextView;
import com.app.inn.peepsin.UI.Views.SFDREditText;
import com.app.inn.peepsin.UI.adapters.ProductSpinnerAdapter;
import com.app.inn.peepsin.UI.dialogFragments.ProductCategoryDialogFragment;
import com.app.inn.peepsin.UI.dialogFragments.ProductSubCategoryDialogFragment;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.squareup.picasso.Picasso;
import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by root on 19/5/17.
 */

public class SellingProductAddFragment extends Fragment {

  @BindView(R.id.view_pager_images)
  ImageView ivProduct;
  @BindView(R.id.tv_product_category_details)
  TextView tvCategory;
  @BindView(R.id.tv_main_category_name)
  TextView tvCategoryName;
  @BindView(R.id.tv_sub_category_name)
  TextView tvSubCategoryName;
  @BindView(R.id.tv_product_name)
  TextView tvName;
  @BindView(R.id.tv_price)
  EditText etPrice;
  @BindView(R.id.tv_sale_price)
  EditText etSalePrice;
  @BindView(R.id.tv_quantity)
  EditText tvQuantity;
  @BindView(R.id.tv_discount)
  EditText tvDiscount;
  @BindView(R.id.edt_specification)
  EditText edtSpecification;
  @BindView(R.id.tv_product_desc)
  EditText etDescription;
  @BindView(R.id.edt_sku_no)
  EditText edtSkuNo;
  @BindView(R.id.edt_product_type)
  EditText edtProductType;
  @BindView(R.id.discount_tag_view)
  View discountTagView;

  @BindView(R.id.view_tax_linear)
  LinearLayout viewTaxLayout;
  @BindView(R.id.tv_mrp)
  TextView tvMRP;
  @BindView(R.id.tv_offer_price)
  TextView tvOfferPrice;
  @BindView(R.id.tv_tax_rate)
  TextView tvTaxRate;
  @BindView(R.id.tv_tax_type)
  TextView tvTaxType;
  @BindView(R.id.tv_total_tax)
  TextView tvTotalTax;
  @BindView(R.id.tv_base_price)
  TextView tvBasePrice;
  @BindView(R.id.tv_net_price)
  TextView tvNetPrice;

  @BindView(R.id.radio_group)
  RadioGroup radioGroup;
  @BindView(R.id.radio_button_draft)
  RadioButton radioButtonDraft;
  @BindView(R.id.radio_button_publish)
  RadioButton radioButtonPublish;
  @BindView(R.id.spinner_product_type)
  Spinner spinnerProduct;

  private String TAG = getTag();
  int productTypeId = 1;
  private ProgressDialog progressDialog;
  private List<String> imageList;
  private int productPrice;
  private String productName;
  private CurrentUser currentUser;

  private ProductSpinnerAdapter productAdapter;
  private CopyOnWriteArrayList<ShopCatSubCategory> categoryList;
  private String categoryName;
  private Integer categoryId = 0, subCategoryId = 0;
  private SellingProductInteractionListener mListener;
  private CopyOnWriteArrayList<TAX> mTaxList;
  private List<SFDREditText> mEditTexts = new ArrayList<>();

  public static SellingProductAddFragment newInstance(List<String> paths, int price, String name) {
    SellingProductAddFragment fragment = new SellingProductAddFragment();
    Bundle bdl = new Bundle();
    bdl.putSerializable(kPictureList, (Serializable) paths);
    bdl.putInt("price", price);
    bdl.putString("name", name);
    fragment.setArguments(bdl);
    return fragment;
  }



  public interface SellingProductInteractionListener {

    void submitAd(HashMap<String, Object> hashMap);
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    if (context instanceof SellingProductInteractionListener) {
      mListener = (SellingProductInteractionListener) context;
    } else {
      throw new RuntimeException(
          context.toString() + " must implement FragmentInteractionListener");
    }
  }

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    progressDialog = Utils.generateProgressDialog(getContext(), true);
    currentUser = ModelManager.modelManager().getCurrentUser();
    mTaxList = currentUser.getTaxList();
    Bundle bundle = getArguments();
    if (bundle != null) {
      imageList = (List<String>) bundle.getSerializable(kPictureList);
      productPrice = bundle.getInt("price");
      productName = bundle.getString("name");
    }
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_edit_selling_product_details, container, false);
    ButterKnife.bind(this, view);
    try {
      String price = String.valueOf(productPrice);
      tvName.setText(productName);
      etPrice.setText(price);
      tvMRP.setText(price);
      etSalePrice.setText(price);
      tvOfferPrice.setText(price);
      if (!imageList.isEmpty()) {
        File filePath = new File(imageList.get(0));
        Picasso.with(getContext()).load(filePath).fit().into(ivProduct);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    edtProductType.setKeyListener(null);
    discountTagView.setVisibility(View.GONE);
    etPrice.addTextChangedListener(priceWatcher);
    etSalePrice.addTextChangedListener(priceWatcher);

    return view;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    loadSpinnerData();
    updateTaxes();

  }

  private void updateTaxes() {
    showProgress(true);
    ModelManager.modelManager().getTaxInfo((Constants.Status iStatus) -> {
      showProgress(false);
      mTaxList = currentUser.getTaxList();
      setTaxView(mTaxList);
    }, (Constants.Status iStatus, String message) -> {
      showProgress(false);
      Toaster.toast(message);
    });
  }

  public void setTaxView(List<TAX> taxes) {
    Integer textsize = getResources().getColor(R.color.text_color_regular);
    int ssd = Utils.dpToPx(getContext(),textsize);
    for (TAX tataTax :taxes) {
      LinearLayout layout = new LinearLayout(getContext());
      layout.setOrientation(LinearLayout.HORIZONTAL);
      {
        SFDLTextView view = new SFDLTextView(getContext());
        view.setText(tataTax.getTaxName());
        view.setGravity(Gravity.CENTER);
        view.setTextColor(getResources().getColor(R.color.text_color_regular));
        view.setTextSize(ssd);
        view.setPadding(10,10,10,10);
        layout.addView(view);
      }
      {
        SFDREditText view = new SFDREditText(getContext());
        mEditTexts.add(view);
        view.setId(tataTax.getTaxId());
        view.setText(tataTax.getTaxValue());
        view.setGravity(Gravity.CENTER);
        view.setInputType(InputType.TYPE_CLASS_NUMBER);
        InputFilter[] fArray = new InputFilter[1];
        fArray[0] = new InputFilter.LengthFilter(2);
        view.setFilters(fArray);
        view.setBackground(getResources().getDrawable(R.drawable.canvas_gray_rounded_corner));
        view.setGravity(Gravity.CENTER);
        view.setTextColor(getResources().getColor(R.color.text_color_regular));
        view.setTextSize(ssd);
        view.setPadding(30,10,30,30);
        layout.addView(view);
      }
      {
        SFDLTextView view = new SFDLTextView(getContext());
        view.setText(tataTax.getTaxType());
        view.setGravity(Gravity.CENTER);
        view.setTextColor(getResources().getColor(R.color.text_color_regular));
        view.setTextSize(ssd);
        view.setPadding(10,10,10,10);

        layout.addView(view);
      }
      viewTaxLayout.addView(layout);
    }
    for (EditText editText : mEditTexts) {
      editText.addTextChangedListener(taxWatcher);
    }
    taxWatcher.afterTextChanged(etSalePrice.getText());
    StringBuilder taxType = new StringBuilder();
    if(taxes.size()>1)
      taxType.append("(").append(taxes.get(0).getTaxName()).append(",")
              .append(taxes.get(1).getTaxName()).append(")");
    else
      taxType.append("(").append(taxes.get(0).getTaxName()).append(")");
    tvTaxType.setText(taxType);
  }

  TextWatcher priceWatcher = new TextWatcher() {
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable start) {
      if (!etSalePrice.getText().toString().isEmpty() && !etPrice.getText().toString().isEmpty()) {
        int costPrice = Integer.parseInt(etPrice.getText().toString());
        int sellPrice = Integer.parseInt(etSalePrice.getText().toString());
        tvOfferPrice.setText(String.valueOf(sellPrice));
        tvMRP.setText(String.valueOf(costPrice));
        taxWatcher.afterTextChanged(etSalePrice.getText());
        if (costPrice > sellPrice) {
          discountTagView.setVisibility(View.VISIBLE);
          int discountPrice = costPrice - sellPrice;
          float discount = (float) discountPrice / costPrice;
          String discountTag = (int) (discount * 100) + "% off";
          tvDiscount.setText(discountTag);
        } else {
          discountTagView.setVisibility(View.GONE);
          tvDiscount.setText("");
        }
      } else{
        discountTagView.setVisibility(View.GONE);
        tvDiscount.setText("");
      }
    }
  };

  TextWatcher taxWatcher = new TextWatcher() {
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable start) {
      boolean state = true;
      for (EditText editText : mEditTexts) {
        if(editText.getText().toString().isEmpty()){
          state = false;
          break;
        }
      }
      if (state && !etSalePrice.getText().toString().isEmpty()) {
        int sellPrice = Integer.parseInt(etSalePrice.getText().toString());
        int taxRate = 0;
        for (EditText editText : mEditTexts) {
          taxRate += Integer.parseInt(editText.getText().toString());
        }
        float netValue = (sellPrice*100)/(100+taxRate);
        float taxValue = sellPrice - netValue;
        tvTaxRate.setText(String.valueOf(taxRate));
        tvTotalTax.setText(String.valueOf(taxValue));
        tvBasePrice.setText(String.valueOf(netValue));
        tvNetPrice.setText(String.valueOf(sellPrice));
      }else {
        tvTaxRate.setText("");
        tvTotalTax.setText("");
        tvBasePrice.setText("");
        tvNetPrice.setText("");
      }
    }
  };



  private void loadSpinnerData() {
    // Product Type Spinner Data
    ModelManager.modelManager().getProductTypeList(
        (Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<ProductType>> genricResponse) -> {
          if(getContext() != null) {
            productAdapter = new ProductSpinnerAdapter(getContext(),
                    R.layout.business_spinner_item, genricResponse.getObject());
            spinnerProduct.setAdapter(productAdapter);
          }
          if (currentUser.getProductType() != null) {
            spinnerProduct.setSelection(currentUser.getProductType() - 1);
          }
        }, (Constants.Status iStatus, String message) -> Log.e(TAG, message));

    spinnerProduct.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
        productTypeId = productAdapter.getItem(position).getId();
        edtProductType.setText(productAdapter.getItem(position).getName());
      }

      @Override
      public void onNothingSelected(AdapterView<?> adapter) {
      }
    });

  }

    /*@OnClick(R.id.view_change_address)
    void onAddAddress() {
        DialogFragment fragment = AddressDialogFragment.newInstance();
        Bundle bdl = new Bundle();
        bdl.putInt(KSCREENWIDTH, screenWidth);
        bdl.putInt(KSCREENHEIGHT, screenHeight);
        fragment.setArguments(bdl);
        fragment.setTargetFragment(this, ADDRESS_RESULT);
        fragment.show(getChildFragmentManager(), "Dialog Fragment");
    }*/

  @OnClick(R.id.view_product_category)
  void selectCategory() {
    showProgress(true);
    ModelManager.modelManager().getCatSubCategoryList(
        (Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<ShopCatSubCategory>> genricResponse) -> {
          showProgress(false);
          categoryList = genricResponse.getObject();
          ProductCategoryDialogFragment fragment = new ProductCategoryDialogFragment();
          Bundle bdl = new Bundle();
          bdl.putInt(KSCREENWIDTH, 0);
          bdl.putInt(KSCREENHEIGHT, 0);
          bdl.putSerializable(kProductCategory, categoryList);
          fragment.setArguments(bdl);
          fragment.setTargetFragment(this, CATEGORY_RESULT);
          fragment.show(getChildFragmentManager(), "Dialog Fragment");
        }, (Constants.Status iStatus, String message) -> {
          showProgress(false);
          Toaster.toast(message);
        });
  }

  void selectSubCategory(int categoryId) {
    for (int i = 0; i < categoryList.size(); i++) {
      if (categoryList.get(i).getId() == categoryId) {
        ProductSubCategoryDialogFragment fragment = new ProductSubCategoryDialogFragment();
        Bundle bdl = new Bundle();
        bdl.putInt(KSCREENWIDTH, 0);
        bdl.putInt(KSCREENHEIGHT, 0);
        bdl.putSerializable(kProductSubCategory, categoryList.get(i).getSubCategories());
        fragment.setArguments(bdl);
        fragment.setTargetFragment(this, SUB_CATEGORY_RESULT);
        fragment.show(getChildFragmentManager(), "Dialog Fragment");
      }
    }
  }

  @OnClick(R.id.btn_back)
  void onBack() {
    getActivity().finish();
  }

  @OnClick(R.id.invite)
  void onEditProduct() {

    int productstatus;
    if(radioButtonPublish.isChecked())
      productstatus = 1;
    else productstatus=6;


    try {
      int costPrice = Integer.parseInt(etPrice.getText().toString());
      int sellPrice = Integer.parseInt(etSalePrice.getText().toString());
      if (categoryId == 0 || subCategoryId == 0) {
        Toast.makeText(getContext(), "Category and Subcategory are must", Toast.LENGTH_SHORT)
            .show();
            /*}else if(address == null){
                Toast.makeText(getContext(),"Address are must",Toast.LENGTH_SHORT).show();*/
      } else if (tvName.getText().toString().isEmpty()) {
        tvName.setError("Product Name is must");
        tvName.requestFocus();
      } else if (etDescription.getText().toString().isEmpty()) {
        etDescription.setError("Description is must");
        etDescription.requestFocus();
      } else if (etPrice.getText().toString().isEmpty()) {
        etPrice.setError("Price is must");
        etPrice.requestFocus();
      } else if (etSalePrice.getText().toString().isEmpty()) {
        etSalePrice.setError("Sale Price is must");
        etSalePrice.requestFocus();
      } else if (tvQuantity.getText().toString().isEmpty()) {
        tvQuantity.setError("Quantity is must");
        tvQuantity.requestFocus();
      } else if (edtSkuNo.getText().toString().isEmpty()) {
        edtSkuNo.setError("SKU Number is Must");
        edtSkuNo.requestFocus();
      } else if (edtSpecification.getText().toString().isEmpty()) {
        edtSpecification.setError("Specification is Must");
        edtSpecification.requestFocus();
      } else if (tvTaxRate.getText().toString().isEmpty()) {
        Toaster.kalaToast("Please input all tax value");
      } else if (!tvTaxRate.getText().toString().isEmpty() && Integer.parseInt(tvTaxRate.getText().toString())>28) {
        Toaster.kalaToast("Tax value cannot exceeds 28%");
      } else if (costPrice < sellPrice) {
        Toaster.customToast("Cost Price cannot be less than sell price");
      } else {
        Integer shopId = ModelManager.modelManager().getCurrentUser().getShopId();
        try {
          for (EditText editText :mEditTexts){
            Integer id = editText.getId();
            for(TAX tax : mTaxList){
              if(tax.getTaxId().equals(id)){
                tax.setTaxValue(editText.getText().toString().trim());
                break;
              }
            }
          }
        }catch (Exception e){
          e.printStackTrace();
        }
        ModelManager.modelManager().getCurrentUser().setTaxList(mTaxList);
        String productName = Utils.getProperText(tvName);
        String productDescription = Utils.getProperText(etDescription);
        String productPrice = Utils.getProperText(etPrice);
        String sellingPrice = Utils.getProperText(etSalePrice);
        String quantity = Utils.getProperText(tvQuantity);
        String discountTag = Utils.getProperText(tvDiscount);
        Integer ctgID = categoryId;
        Integer subctgID = subCategoryId;
        String skuno = Utils.getProperText(edtSkuNo);
        String specification = Utils.getProperText(edtSpecification);
        Integer producttype = productTypeId;
        HashMap<String, Object> productMap = new HashMap<>();
        productMap.put(kName, productName);
        productMap.put(kShopId, shopId);
        productMap.put(kProductDesc, productDescription);
        productMap.put(kProductPrice, productPrice);
        productMap.put(kProductSellPrice, sellingPrice);
        productMap.put(kQuantity, quantity);
        productMap.put(kDiscountTagLine, discountTag);
        productMap.put(kProductCategoryId, ctgID);
        productMap.put(kProductSubCategoryId, subctgID);
        productMap.put(kSKUNumber, skuno);
        productMap.put(kSpecification, specification);
        productMap.put(kProductType, producttype);
        productMap.put(kProductStatus, productstatus);
        productMap.put(kNetPrice,Utils.getProperText(tvNetPrice));
        productMap.put(kTotalTax,Utils.getProperText(tvTaxRate));
        productMap.put(kProductBasicPrice,Utils.getProperText(tvBasePrice));
        mListener.submitAd(productMap);
      }
    } catch (Exception e) {
      e.printStackTrace();
      Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
    }
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    switch (requestCode) {
      case CATEGORY_RESULT:
        if (resultCode == Activity.RESULT_OK) {
          categoryId = data.getIntExtra("id", 0);
          categoryName = data.getStringExtra("data");
          selectSubCategory(categoryId);
        }
        break;
      case SUB_CATEGORY_RESULT:
        if (resultCode == Activity.RESULT_OK) {
          subCategoryId = data.getIntExtra("id", 0);
          tvCategoryName.setText(categoryName);
          tvSubCategoryName.setText(data.getStringExtra("data"));
          tvCategory.setVisibility(View.GONE);
        }
        break;
        /*    case ADDRESS_RESULT:
                if (resultCode == Activity.RESULT_OK) {
                    address = (Address) data.getSerializableExtra("data");
                    tvAddress.setTextColor(getResources().getColor(R.color.text_color_regular));
                    tvAddress.setText(address.getCity().concat(" , ")
                            .concat(address.getState()).concat(" , ")
                            .concat(address.getCountry()));
                }
                break;*/
    }
  }

  private void showProgress(boolean b) {
    if (b) {
      progressDialog.show();
    } else {
      if (progressDialog != null) {
        progressDialog.cancel();
      }
    }
  }

  @Override
  public void onDetach() {
    super.onDetach();
    mListener = null;
  }
}
