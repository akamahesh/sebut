package com.app.inn.peepsin.UI.fragments.Shop;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.MyShop;
import com.app.inn.peepsin.Models.ProductType;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.fragments.Profile.ProfileFragment;
import com.app.inn.peepsin.UI.fragments.SellStuffModule.SellingStuffFragment;
import com.app.inn.peepsin.UI.helpers.GridSpacingItemDecoration;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kShopId;

/**
 * Created by root on 3/7/17.
 */

public class MyShopFragment extends Fragment {

    @BindView(R.id.tv_title) TextView textViewTitle;
    @BindView(R.id.iv_shop_image) ImageView ivShopImage;
    @BindView(R.id.iv_logo_image) ImageView ivShopLogo;
    @BindView(R.id.tv_shop_name) TextView tvShopName;
    @BindView(R.id.rating_score_bar) RatingBar ratingScoreBar;
    @BindView(R.id.tv_name) TextView tvName;
    @BindView(R.id.tv_email) TextView tvEmail;
    @BindView(R.id.tv_phone) TextView tvPhone;
    @BindView(R.id.tv_joined) TextView tvJoined;
    @BindView(R.id.tv_location) TextView tvLocation;
    @BindView(R.id.tv_address) TextView tvLoc;
    @BindView(R.id.tv_product_count) TextView tvProductCount;
    @BindView(R.id.view_selling_product) LinearLayout productView;
    @BindView(R.id.edit)
    Button btnEdit;

    @BindView(R.id.recycler_view_product) RecyclerView rvProduct;
    @BindView(R.id.tv_shop_type) TextView tvShopType;
    @BindView(R.id.iv_shop_type) ImageView ivShopType;
    @BindView(R.id.tv_shopVisibility) TextView tvShopVisibility;
    @BindView(R.id.tv_business) TextView tvBusiness;
    @BindView(R.id.tv_pan) TextView tvPAN;
    @BindView(R.id.tv_tin) TextView tvTIN;
    @BindView(R.id.tv_vat) TextView tvVAT;
    @BindView(R.id.tv_cin) TextView tvCIN;
    @BindView(R.id.tv_company_address) TextView tvAddress;
    @BindView(R.id.tv_product_type) TextView tvProductType;
    @BindView(R.id.tv_delivery_type) TextView tvDeliveryTime;
    @BindView(R.id.tv_account_name) TextView tvAccountName;
    @BindView(R.id.tv_account_no) TextView tvAccountNo;
    @BindView(R.id.tv_bank_name) TextView tvBankName;
    @BindView(R.id.tv_ifsc) TextView tvIFSC;
    @BindView(R.id.tv_account_type) TextView tvAccountType;

    private static Switcher profileSwitcherListener;
    private Integer shopId;
    private MyShop myShop;
    private ProgressDialog progressDialog;

    public static Fragment newInstance(Switcher switcher, int shopId) {
        Fragment fragment = new MyShopFragment();
        Bundle bdl = new Bundle();
        bdl.putInt(kShopId, shopId);
        fragment.setArguments(bdl);
        profileSwitcherListener = switcher;
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        progressDialog = Utils.generateProgressDialog(getContext(), false);
        if (bundle != null) {
            shopId = bundle.getInt(kShopId);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_shop, container, false);
        ButterKnife.bind(this, view);
        textViewTitle.setText(getString(R.string.my_shop_title));
        //Rating star color
        LayerDrawable stars = (LayerDrawable) ratingScoreBar.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(Color.parseColor("#FAD368"), PorterDuff.Mode.SRC_ATOP);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadShopData(shopId);
    }

    @Override
    public void onResume() {
        super.onResume();
        loadShopData(shopId);
    }

    private void loadShopData(Integer shopId) {
        showProgress(true);
        ModelManager.modelManager().getMyShopDetails(shopId, (Constants.Status iStatus, GenricResponse<MyShop> genricResponse) -> {
            showProgress(false);
            myShop = genricResponse.getObject();
            updateData(myShop);
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Toaster.toast(message);
        });
    }

    private void updateData(MyShop shop) {
        tvShopName.setText(shop.getName());
        String ownerName = "Owner is "+shop.getOwnerFirstName() + " " + shop.getOwnerLastName();
        tvName.setText(ownerName);
        String email = "Email on "+shop.getEmail();
        tvEmail.setText(email);
        String phone = "Contact on "+shop.getPhone();
        tvPhone.setText(phone);
        String joined = "Joined PeepsIn on "+shop.getJoinedDate();
        tvJoined.setText(joined);
        tvProductCount.setText(String.valueOf(shop.getProductCount()));
        ratingScoreBar.setRating(Float.valueOf(shop.getRating()));

        String address = "No Address Available";
        String loc="";
        if (shop.getAddress() != null){
            loc = shop.getAddress().getStreetAddress() + " , "
                    + shop.getAddress().getCity() + " , "
                    + shop.getAddress().getState() + " , "
                    + shop.getAddress().getCountry();
            address = loc;
        }
        tvLocation.setText(address);
        tvLoc.setText(loc);

        String bannerUrl = shop.getBannerUrl();
        if (!bannerUrl.isEmpty()) {
            Picasso.with(getContext())
                    .load(bannerUrl)
                    .placeholder(R.drawable.img_banner_placeholder)
                    .into(ivShopImage);
        } else
            ivShopImage.setImageResource(R.drawable.img_banner_placeholder);

        String logoUrl = shop.getLogoUrl();
        if (!logoUrl.isEmpty()) {
            Picasso.with(getContext())
                    .load(logoUrl)
                    .placeholder(R.drawable.img_shop_logo)
                    .into(ivShopLogo);
        } else
            ivShopLogo.setImageResource(R.drawable.img_shop_logo);

        String shopVisibility = "Visibility Range : "+String.valueOf(shop.getVisibilityRange())+" Km";
        if(shop.getShopType()==1){
            ivShopType.setImageResource(R.drawable.ic_lock_close);
            tvShopType.setText(getString(R.string.text_private));
            switch (shop.getVisibilityLevel()){
                case 0:
                    shopVisibility = "Visibility : "+getString(R.string.privacy_setting_only_me);
                    break;
                case 1:
                    shopVisibility = "Visibility : "+getString(R.string.privacy_setting_level_1_friend);
                    break;
                case 2:
                    shopVisibility = "Visibility : "+getString(R.string.privacy_setting_level_friend);
                    break;
                case 3:
                    shopVisibility = "Visibility : "+getString(R.string.privacy_setting_level_3_friend);
                    break;
            }
        }else {
            ivShopType.setImageResource(R.drawable.ic_lock_open);
            tvShopType.setText(getString(R.string.text_public));

        }
        tvShopVisibility.setText(shopVisibility);

        tvBusiness.setText(Utils.getBusinessType(shop.getBusinessType()));
        String PAN = "PAN: "+shop.getKycInfo().getPAN();
        tvPAN.setText(PAN);
        String TIN = "TAN: "+shop.getKycInfo().getTAN();
        tvTIN.setText(TIN);
        String VAT = "GST: "+shop.getKycInfo().getGST();
        tvVAT.setText(VAT);
        String CIN = "CIN: "+shop.getCompanyDetails().getCompanyCINNumber();
        tvCIN.setText(CIN);
        tvAddress.setText(shop.getCompanyDetails().getCompanyAddress());
        String accountName = "A/c Holder: "+shop.getBankDetails().getAccountHolderName();
        tvAccountName.setText(accountName);
        String accountNo = "A/c Number: "+shop.getBankDetails().getAccountNumber();
        tvAccountNo.setText(accountNo);
        String bankName = "Bank: "+shop.getBankDetails().getBankName();
        tvBankName.setText(bankName);
        String ifsc = "IFSC Code: "+shop.getBankDetails().getIFSCCode();
        tvIFSC.setText(ifsc);
        String accountType = shop.getBankDetails().getAccountType()+" Account";
        tvAccountType.setText(accountType);

        ProductTypeAdapter adapter = new ProductTypeAdapter(getContext(),shop.getProductTypeStore());
        rvProduct.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        rvProduct.setHasFixedSize(false);
        rvProduct.setNestedScrollingEnabled(false);
        rvProduct.addItemDecoration(new GridSpacingItemDecoration(2, Utils.dpToPx(getContext(),0), true));
        rvProduct.setItemAnimator(new DefaultItemAnimator());
        rvProduct.setAdapter(adapter);

        tvProductType.setText(shop.getProductTypeStore().get(0).getName());
        tvDeliveryTime.setText(shop.getDeliveryType().getName());
    }

    @OnClick(R.id.view_selling_product)
    void onSellingProduct() {
        int userId = ModelManager.modelManager().getCurrentUser().getUserId();
        int shopId = ModelManager.modelManager().getCurrentUser().getShopId();
        if (profileSwitcherListener != null)
            profileSwitcherListener.switchFragment(SellingStuffFragment.newInstance(profileSwitcherListener, shopId, userId), true, true);
    }

    @OnClick(R.id.btn_edit)
    void onEdit() {
        if(myShop!=null)
            openBottomSheetMenu(myShop.getShopStatus());
    }

    @OnClick(R.id.btn_back)
    public void onBackPress() {
        getFragmentManager().popBackStack();
    }

    private void openBottomSheetMenu(int orderStatus) {
        LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.bottom_sheet_shop_detais, null);
        final Dialog mBottomSheetDialog = new Dialog(getContext(), R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.canvas_dialog_background_inset));
        mBottomSheetDialog.show();
        TextView tvStatus = (TextView)view.findViewById(R.id.tv_status);
        if(orderStatus==1||orderStatus==2||orderStatus==3||orderStatus==4)
            view.findViewById(R.id.view_shop_status).setVisibility(View.GONE);
        else {
            view.findViewById(R.id.view_shop_status).setVisibility(View.VISIBLE);
            if(orderStatus==5 || orderStatus==7)
                tvStatus.setText(getString(R.string.activate_shop));
            else
                tvStatus.setText(getString(R.string.deactivate_shop));
        }

        view.findViewById(R.id.tv_edit_shop).setOnClickListener(v -> {
            mBottomSheetDialog.dismiss();
            if (profileSwitcherListener != null)
                profileSwitcherListener.switchFragment(CreateShopFragment.newInstance(CreateShopFragment.Type.EDIT_SHOP, myShop, profileSwitcherListener), true, true);
        });
        view.findViewById(R.id.tv_delete_shop).setOnClickListener(v -> {
            mBottomSheetDialog.dismiss();
            Utils.showAlertDialog(getContext(), "Alert Message", "Do you really want to delete shop?", (dialog, which) -> deleteMyShop());
        });
        view.findViewById(R.id.tv_status).setOnClickListener(v -> {

            mBottomSheetDialog.dismiss();
            if(orderStatus==5 || orderStatus==7)
                changeShopStatus(6);
            else
                changeShopStatus(7);

        });
        view.findViewById(R.id.tv_cancel).setOnClickListener(v -> mBottomSheetDialog.dismiss());
    }

    void changeShopStatus(int status) {
        showProgress(true);
        ModelManager.modelManager().activateMyShop(shopId, status, (Constants.Status iStatus, GenricResponse<Integer> genericResponse) -> {
            Integer shopStatus = genericResponse.getObject();
            myShop.setShopStatus(shopStatus);
            showProgress(false);
        }, (Constants.Status iStatus, String message) -> {
            Log.e("Order Status", message);
            showProgress(false);
        });
    }

    void deleteMyShop(){
        showProgress(true);
        ModelManager.modelManager().deleteMyShop(shopId, (Constants.Status iStatus) -> {
            showProgress(false);
            if (profileSwitcherListener != null)
                profileSwitcherListener.switchFragment(ProfileFragment.newInstance(profileSwitcherListener), true, true);
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Toaster.toast(message);
        });
    }

    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) progressDialog.cancel();
        }
    }

    public class ProductTypeAdapter extends RecyclerView.Adapter<ProductTypeAdapter.ViewHolder> {

        private Context context;
        private List<ProductType> inventoryList;

        public ProductTypeAdapter(Context context, List<ProductType> inventoryList) {
            this.context = context;
            this.inventoryList = inventoryList;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_product_grid_base, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            ProductType type = inventoryList.get(position);
            holder.bindContent(type);
            holder.checkBox.setVisibility(View.GONE);
            String name = type.getName();
            holder.tvItemName.setText(name);

            String imageURL = type.getUrl();
            if (!imageURL.isEmpty())
                Picasso.with(context)
                        .load(imageURL)
                        .placeholder(R.drawable.img_product_placeholder)
                        .into(holder.ivItemImage);
            else
                holder.ivItemImage.setImageDrawable(
                        context.getResources().getDrawable(R.drawable.img_product_placeholder));
        }

        @Override
        public int getItemCount() {
            return inventoryList.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            ProductType itemType;
            @BindView(R.id.iv_item_image)
            ImageView ivItemImage;
            @BindView(R.id.tv_item_name)
            TextView tvItemName;
            @BindView(R.id.checkBox_btn)
            CheckBox checkBox;

            public ViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            public void bindContent(ProductType shop) {
                this.itemType = shop;
            }
        }
    }

}
