package com.app.inn.peepsin.UI.fragments.Shop;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.ShopRating;
import com.app.inn.peepsin.Models.ShopReview;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.activities.HomeActivity;
import com.app.inn.peepsin.UI.helpers.DividerItemRecyclerDecoration;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kShopId;

/**
 * Created by dhruv on 1/8/17.
 */

public class ShopRatingReviewFragment extends Fragment {
    private String TAG = getTag();
    @BindView(R.id.tv_title) TextView tvTitle;
    @BindView(R.id.tv_rating)
    TextView tvRating;
    @BindView(R.id.tv_ratings_points)
    TextView tvTotalRating;
    @BindView(R.id.progress_bar1)
    ProgressBar progressBar1;
    @BindView(R.id.progress_bar2)
    ProgressBar progressBar2;
    @BindView(R.id.progress_bar3)
    ProgressBar progressBar3;
    @BindView(R.id.progress_bar4)
    ProgressBar progressBar4;
    @BindView(R.id.progress_bar5)
    ProgressBar progressBar5;
    @BindView(R.id.recycler_view_review)
    RecyclerView reviewRecycler;

    private int shopId;
    private ShopRating shopRating;
    private ProgressDialog progressDialog;
    static Switcher switcherListener;

    private ReviewAdapter reviewAdapter;
    private LinearLayoutManager mLayoutManager;
    private int mPage=1;
    private boolean loading;
    private int pageSize;

    public static Fragment newInstance(Switcher switcher, int shopId) {
        switcherListener=switcher;
        Fragment fragment = new ShopRatingReviewFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(kShopId,shopId);
        //bundle.putSerializable(kShop, rating);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = Utils.generateProgressDialog(getContext(), false);
        Bundle bundle = getArguments();
        if (bundle != null) {
            shopId = bundle.getInt(kShopId);
            //shopRating = (ShopRating) bundle.getSerializable(kShop);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_shop_rating, container, false);
        ButterKnife.bind(this, view);
        tvTitle.setText(getString(R.string.shop_rating_title));
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getProductRating();
    }

    @OnClick(R.id.btn_back)
    void onBack(){
        getFragmentManager().popBackStack();
    }

    RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            if(dy > 0) //check for scroll down
            {
                int visibleItemCount = mLayoutManager.getChildCount();
                int totalItemCount = mLayoutManager.getItemCount();
                int firstVisibleItemPosition = mLayoutManager.findFirstVisibleItemPosition();

                if (loading)
                {
                    if ( (visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= pageSize) {
                        loading = false;
                        ++mPage;
                        //Do pagination.. i.e. fetch new data
                        loadMoreReview();
                    }
                }
            }
        }
    };

    public void initialState(List<ShopReview> reviewList){
        mPage=1;
        loading=true;
        pageSize=reviewList.size();
    }

    private void getProductRating(){
        showProgress(true);
        ModelManager.modelManager().getShopRatingReview(shopId,1,(Constants.Status iStatus, GenricResponse<ShopRating> genericResponse) -> {
            showProgress(false);
            ShopRating rating = genericResponse.getObject();
            ratingViewUpdate(rating);
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Log.e(TAG,message);
        });
    }

    private void loadMoreReview(){
        showProgress(true);
        ModelManager.modelManager().getShopRatingReview(shopId,mPage,(Constants.Status iStatus, GenricResponse<ShopRating> genericResponse) -> {
            showProgress(false);
            ShopRating rating = genericResponse.getObject();
            reviewAdapter.addItems(rating.getShopReviewList());
            loading = !genericResponse.getObject().getShopReviewList().isEmpty();
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Log.e(TAG,message);
        });
    }

    public void ratingViewUpdate(ShopRating rating){
        if(rating.getRating().isEmpty()) {
            tvRating.setText("0.0");}
        else {
            tvRating.setText(rating.getRating());}
        Activity activity = getActivity();
        if(activity != null){
            String totalRating = rating.getTotalRating()+" "+getString(R.string.ratings);
            tvTotalRating.setText(totalRating);
        }
        progressBar1.setProgress(rating.getFifthProgress());
        progressBar2.setProgress(rating.getForthProgress());
        progressBar3.setProgress(rating.getThirdProgress());
        progressBar4.setProgress(rating.getSecondProgress());
        progressBar5.setProgress(rating.getFirstProgress());

        reviewListUpdate(rating.getShopReviewList());
    }

    public void reviewListUpdate(List<ShopReview> reviewList){
        try {
            initialState(reviewList);
            reviewAdapter = new ReviewAdapter(reviewList);
            reviewRecycler.setNestedScrollingEnabled(false);
            reviewRecycler.setHasFixedSize(false);
            mLayoutManager = new LinearLayoutManager(getActivity());
            reviewRecycler.setLayoutManager(mLayoutManager);
            reviewRecycler.setAdapter(reviewAdapter);
            reviewRecycler.addItemDecoration(new DividerItemRecyclerDecoration(getContext(), R.drawable.canvas_recycler_divider));
            reviewRecycler.addOnScrollListener(onScrollListener);
        }catch (NullPointerException e){e.printStackTrace();}
    }

    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) progressDialog.cancel();
        }
    }

    public class ReviewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private List<ShopReview> mReviewList;

        private ReviewAdapter(List<ShopReview> items) {
            mReviewList = items;
        }

        @Override
        public int getItemViewType(int position) {
            if(position == mReviewList.size())
                return 0;
            else
                return 1;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            if(viewType == 1) {
                View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_shop_rating, parent, false);
                return new ReviewHolder(layoutView);
            }
            else{
                View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_feed_progress, parent, false);
                return new ProgressHolder(layoutView);
            }
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

            if(position == mReviewList.size()) {
                ProgressHolder view = (ProgressHolder) viewHolder;
                if(mReviewList.size()>=pageSize){
                    view.progressBar.setVisibility(View.GONE);
                    view.tvFeedCount.setVisibility(View.GONE);
                    /*new Handler().postDelayed(() -> {
                        //Hide the refresh after 2sec
                        ((HomeActivity)getContext()).runOnUiThread(() -> {
                            view.progressBar.setVisibility(View.GONE);
                            view.tvFeedCount.setVisibility(View.VISIBLE);
                            String countTxt = getString(R.string.feed_total_text)+ mReviewList.size();
                            view.tvFeedCount.setText(countTxt);

                        });
                    }, 500);*/
                }

            }else{
                ReviewHolder holder = (ReviewHolder) viewHolder;
                ShopReview item = mReviewList.get(position);
                holder.bindContent(item);
                String name = item.getUsername();
                String review = item.getReview();
                int rating = item.getRating();
                String ratingValue = Utils.getReviewLevel(item.getRating());
                String productUrl = item.getCoverImageUrl();
                String postingDate = DateFormat.format("MMM d, yyyy", Long.parseLong(item.getTimeStamp())).toString();


                holder.tvUserName.setText(name);
                holder.tvReview.setText(review);
                holder.tvRating.setText(String.valueOf(rating));
                holder.tvTimestamp.setText(postingDate);
                holder.tvRatingValue.setText(ratingValue);
                if(item.getProductName()!=null)
                    holder.tvProductName.setText(item.getProductName());

                if(!productUrl.isEmpty())
                    Picasso.with(getContext())
                            .load(productUrl)
                            .placeholder(R.drawable.img_product_placeholder)
                            .into(holder.ivProduct);
            }
        }

        @Override
        public int getItemCount() {
            return mReviewList.size()+1;
        }

        public void addItems(List<ShopReview> list){
            mReviewList.addAll(list);
            notifyDataSetChanged();
        }

        class ProgressHolder extends RecyclerView.ViewHolder{

            @BindView(R.id.tv_feed_count)
            TextView tvFeedCount;
            @BindView(R.id.progress_bar)
            ProgressBar progressBar;

            ProgressHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }
        }

        class ReviewHolder extends RecyclerView.ViewHolder {
            ShopReview item;
            @BindView(R.id.tv_user_name)
            TextView tvUserName;
            @BindView(R.id.tv_product_name)
            TextView tvProductName;
            @BindView(R.id.tv_review)
            TextView tvReview;
            @BindView(R.id.tv_rating)
            TextView tvRating;
            @BindView(R.id.tv_timestamp)
            TextView tvTimestamp;
            @BindView(R.id.tv_rating_expression)
            TextView tvRatingValue;
            @BindView(R.id.iv_product_image)
            ImageView ivProduct;

            ReviewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            private void bindContent(ShopReview item) {
                this.item = item;
            }
        }
    }

}
