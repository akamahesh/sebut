package com.app.inn.peepsin.UI.fragments.Base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.fragments.Message.MessageFragment;
import com.app.inn.peepsin.UI.helpers.FragmentUtil;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;

/**
 * Created by mahesh on 25/4/17.
 */

public class RootMessageFragment extends Fragment{

    public static RootMessageFragment newInstance() {
        return new RootMessageFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_root, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        FragmentUtil.changeFragment(getChildFragmentManager(), MessageFragment.newInstance(switcher),true,false);
    }

    Switcher switcher = (fragment, saveInBackStack, animate) -> {
        Utils.hideKeyboard(getContext());
        FragmentUtil.changeFragment(getChildFragmentManager(),fragment,saveInBackStack,animate);
    };


    @Override
    public void onDetach() {
        super.onDetach();
        switcher = null;
    }

}
