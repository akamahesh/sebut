package com.app.inn.peepsin.UI.fragments.Shop;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.CurrentUser;
import com.app.inn.peepsin.Models.ShopModel;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.adapters.ShopAdapter;
import com.app.inn.peepsin.UI.fragments.Profile.ShoppingCartFragment;
import com.app.inn.peepsin.UI.helpers.GridSpacingItemDecoration;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.CART_UPDATE;
import static com.app.inn.peepsin.Constants.Constants.kData;
import static com.app.inn.peepsin.Constants.Constants.kLatitude;
import static com.app.inn.peepsin.Constants.Constants.kLongitude;

public class ShopsFragment extends Fragment{

    private String TAG = getTag();
    static Switcher switchListener;

    @BindView(R.id.tv_title) TextView tvTitle;
    @BindView(R.id.tv_cartCount)
    TextView tvCartCount;
    @BindView(R.id.recycler_view_shop) RecyclerView recyclerView;
    @BindView(R.id.swipeRefreshLayout) SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.progress_bar) ProgressBar progressBar;
    @BindView(R.id.empty_view) View emptyView;

    private ProgressDialog progressDialog;
    private int mPage;
    private boolean loading;
    private int pageSize;

    private String latitude,longitude;
    private List<ShopModel> shopList;
    private ShopAdapter shopAdapter;
    GridLayoutManager mLayoutManager;

    public static Fragment newInstance(Switcher switcher,String lat, String lng) {
        switchListener = switcher;
        Fragment fragment = new ShopsFragment();
        Bundle bdl = new Bundle();
        bdl.putString(kLatitude,lat);
        bdl.putString(kLongitude,lng);
        fragment.setArguments(bdl);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = Utils.generateProgressDialog(getContext(), true);
        shopList = new ArrayList<>();
        Bundle bundle = getArguments();
        if(bundle!=null){
            latitude = bundle.getString(kLatitude);
            longitude = bundle.getString(kLongitude);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_shop, container, false);
        ButterKnife.bind(this, view);
        tvTitle.setText(getString(R.string.title_shop));

        shopAdapter = new ShopAdapter(getActivity(), shopList,switchListener);
        mLayoutManager = new GridLayoutManager(getActivity(), 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setHasFixedSize(false);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(1, Utils.dpToPx(getContext(),5), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(shopAdapter);
        recyclerView.addOnScrollListener(onScrollListener);

        swipeRefreshLayout.setOnRefreshListener(onRefreshListener);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        CurrentUser user = ModelManager.modelManager().getCurrentUser();
        setCartUpdate(user.getShoppingCartCount());
        loadShopList();
    }

    SwipeRefreshLayout.OnRefreshListener onRefreshListener = this::loadShopList;

    public void initialState(){
        mPage=1;
        loading=true;
        pageSize=shopList.size();
    }

    RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            if(dy > 0) //check for scroll down
            {
                int visibleItemCount = mLayoutManager.getChildCount();
                int totalItemCount = mLayoutManager.getItemCount();
                int firstVisibleItemPosition = mLayoutManager.findFirstVisibleItemPosition();

                if (loading)
                {
                    if ( (visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= pageSize) {
                        loading = false;
                        ++mPage;
                        //Do pagination.. i.e. fetch new data
                        loadMoreShop();
                    }
                }
            }
        }
    };

    @OnClick(R.id.btn_back)
    public void onBackPress(){
        getFragmentManager().popBackStack();
    }

    private void loadShopList() {
        showProgress(true);
        HashMap<String ,Object> map = new HashMap<>();
        map.put(kLatitude, latitude);
        map.put(kLongitude, longitude);
        ModelManager.modelManager().getShopList(map,1,(Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<ShopModel>> genericResponse) -> {
            shopList = genericResponse.getObject();
            shopAdapter.addItems(shopList);
            checkEmptyScreen();
            initialState();
            showProgress(false);
        }, (Constants.Status iStatus, String message) -> {
            Log.e(TAG, message);
            checkEmptyScreen();
            showProgress(false);
        });
    }

    public void loadMoreShop() {
        showProgress(true);
        HashMap<String ,Object> map = new HashMap<>();
        map.put(kLatitude, latitude);
        map.put(kLongitude, longitude);
        ModelManager.modelManager().getShopList(map,mPage,(Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<ShopModel>> genericResponse) -> {
            shopList.addAll(genericResponse.getObject());
            shopAdapter.addMoreItems(genericResponse.getObject());
            loading = !genericResponse.getObject().isEmpty();
            showProgress(false);
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Log.e(TAG, message);
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        // register cart update receiver
        LocalBroadcastManager.getInstance(getContext())
                .registerReceiver(mBroadcastReceiver, new IntentFilter(CART_UPDATE));
    }

    BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(CART_UPDATE)) {
                setCartUpdate(intent.getIntExtra(kData, 0));
            }
        }
    };

    public void setCartUpdate(int count) {
        if (count == 0) {
            tvCartCount.setVisibility(View.GONE);
        } else {
            tvCartCount.setVisibility(View.VISIBLE);
            tvCartCount.setText(String.valueOf(count));
        }
    }

    @OnClick(R.id.btn_cart)
    void onCart() {
        if (switchListener != null)
            switchListener.switchFragment(ShoppingCartFragment.newInstance(switchListener), true, true);
    }

    private void checkEmptyScreen(){
        if(shopAdapter.getItemCount()>0)
            emptyView.setVisibility(View.GONE);
        else
            emptyView.setVisibility(View.VISIBLE);
    }

    private void showProgress(boolean b) {
        if (b) {
            swipeRefreshLayout.setRefreshing(true);
            //progressDialog.show();
        } else {
            swipeRefreshLayout.setRefreshing(false);
            //if (progressDialog != null) progressDialog.cancel();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        switchListener = null;
    }

}
