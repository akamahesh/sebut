package com.app.inn.peepsin.UI.adapters;

import android.app.Dialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.OrderHistory;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.fragments.Profile.ReceivedOrderDetailFragment;
import com.app.inn.peepsin.UI.fragments.Profile.ReceivedOrderFragment;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kOrderStatus;

/**
 * Created by dhruv on 2/8/17.
 */

public class ReceivedOrderAdapter extends RecyclerView.Adapter<ReceivedOrderAdapter.ViewHolder> {

    private List<OrderHistory> orderList;
    private Switcher switchListener;
    private Context context;
    private Integer mStatus;
    private ProgressDialog progressDialog;
    private ReceivedOrderFragment.PlaceholderFragment fragment;

    public ReceivedOrderAdapter(Switcher switcher, Context context, ArrayList<OrderHistory> orderHistoryList,
                                int mStatus,ReceivedOrderFragment.PlaceholderFragment fragment) {
        progressDialog = Utils.generateProgressDialog(context, true);
        this.orderList = orderHistoryList;
        this.switchListener = switcher;
        this.context = context;
        this.mStatus = mStatus;
        this.fragment = fragment;
    }

    @Override
    public ReceivedOrderAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_received_order_layout, parent, false);
        return new ReceivedOrderAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ReceivedOrderAdapter.ViewHolder holder, int position) {
        OrderHistory item = orderList.get(position);
        holder.bindContent(item);
        String orderDate      = item.getOrderDate();
        String orderId        = String.valueOf(item.getOrderId());
        holder.tvOrderDate.setText(orderDate);
        holder.tvOrderIdNumber.setText(orderId);

        if (position == 0)
            holder.dateLayout.setVisibility(View.GONE);
        else {
            if (!orderDate.equals(orderList.get(position - 1).getOrderDate()))
                holder.dateLayout.setVisibility(View.VISIBLE);
            else
                holder.dateLayout.setVisibility(View.GONE);
        }

        holder.tvOrderStatus.setText(Utils.getStatus(item.getOrderStatus()));
        if (item.getOrderStatus() == 5) {
            holder.tvOrderStatus.setTextColor(Color.parseColor("#FFFF0000"));
        } else {
            holder.tvOrderStatus.setTextColor(Color.parseColor("#30C430"));
        }

        String totalItem = "0";
        if (item.getTotalItemCount() != null)
            try {
                int quantity = 0;
                for(int i=0;i<item.getProductRecords().size();i++) {
                    quantity += item.getProductRecords().get(i).getProductQuantity();
                }
                if (quantity == 1) {
                    totalItem = String.valueOf(item.getTotalItemCount()) + " " +
                            "Product /" + " " + (quantity + " " + "Quantity");
                } else {
                    totalItem = String.valueOf(item.getTotalItemCount()) + " " +
                            "Product /" + " " + (quantity + " " + "Quantities");
                }
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        holder.tvOrderItem.setText(totalItem);

        String ordertotalammount = "";
        if (item.getFinalAmountAfterPromoCode() != null)
            ordertotalammount = item.getFinalAmountAfterPromoCode();
        String paymentbeforepromocode = "";
        if (item.getTotalCartPriceBeforePromoCode() != null)
            paymentbeforepromocode = item.getTotalCartPriceBeforePromoCode();
        String promocodeammount = "---";
        if (!item.getDiscountedPrice().equals(""))
            promocodeammount = item.getDiscountedPrice();
        String discounrtag = item.getPromoCodeTagLine();

        String addressTxt = "No Location Available";
        String userName = "";
        String phoneNumber = "";
        if (item.getDeliveryAddress() != null){
            userName = item.getDeliveryAddress().getFullName();
            phoneNumber   = item.getDeliveryAddress().getPhone();
            addressTxt = item.getDeliveryAddress().getStreetAddress() + ", "
                    + item.getDeliveryAddress().getCity() + ", "
                    + item.getDeliveryAddress().getState() + ", "
                    + item.getDeliveryAddress().getCountry();
        }

        holder.tvOrderAddress.setText(addressTxt);
        holder.tvOrderPhone.setText(userName);
        holder.tvOrderUserName.setText(phoneNumber);
        holder.tvOrderTotalAmmount.setText(ordertotalammount);
        holder.tvPaymentBeforePromoAmmount.setText(paymentbeforepromocode);
        holder.tvPromoCodeAmmount.setText(promocodeammount);
        holder.tvDiscountTagLine.setText(discounrtag);

        if (item.getPaymentType() != null) {
            switch (item.getPaymentType()) {
                case 1:
                    holder.tvPaymentMethod.setText(R.string.text_paytm);
                    break;
                case 2:
                    holder.tvPaymentMethod.setText(R.string.text_pay_u_money);
                    break;
                case 3:
                    holder.tvPaymentMethod.setText(R.string.text_cash_on_delivery);
                    break;
                default:
                    holder.tvPaymentMethod.setText(R.string.pending_payment);
            }
        }

    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }

    public OrderHistory getItem(int position) {
        return orderList.get(position);
    }

    public void addItems(List<OrderHistory> inventoryList) {
        this.orderList.clear();
        this.orderList.addAll(inventoryList);
        notifyDataSetChanged();
    }

    public void addNewItems(List<OrderHistory> inventoryList) {
        this.orderList.addAll(inventoryList);
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        OrderHistory order;
        @BindView(R.id.tv_order_date)
        TextView tvOrderDate;
        @BindView(R.id.tv_order_status)
        TextView tvOrderStatus;
        @BindView(R.id.tv_order_item)
        TextView tvOrderItem;
        @BindView(R.id.tv_order_id_number)
        TextView tvOrderIdNumber;
        @BindView(R.id.tv_order_address_user_name)
        TextView tvOrderUserName;
        @BindView(R.id.tv_order_address_user)
        TextView tvOrderAddress;
        @BindView(R.id.tv_order_address_number)
        TextView tvOrderPhone;
        @BindView(R.id.tv_payment_mehtod)
        TextView tvPaymentMethod;
        @BindView(R.id.tv_order_total_ammount)
        TextView tvOrderTotalAmmount;
        @BindView(R.id.tv_payment_before_promo_ammount)
        TextView tvPaymentBeforePromoAmmount;
        @BindView(R.id.tv_promo_code_ammount)
        TextView tvPromoCodeAmmount;
        @BindView(R.id.tv_discount_tag_line)
        TextView tvDiscountTagLine;
        @BindView(R.id.date_view)
        View dateLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindContent(OrderHistory item) {
            this.order = item;
        }

        @OnClick(R.id.btn_show_product)
        void onItemClick() {
            if (switchListener != null)
                switchListener.switchFragment(ReceivedOrderDetailFragment.newInstance(orderList.get(getAdapterPosition()).getProductRecords()), true, true);
        }

        @OnClick(R.id.status_btn)
        void onStatusChange() {
            openBottomSheetMenu(order.getOrderId(), order.getOrderStatus(), getAdapterPosition());
        }
    }

    private void openBottomSheetMenu(int orderId, int orderStatus, int position) {
        if(orderStatus==Constants.OrderStatus.cancelled.getValue()){
            Utils.showAlertDialog(context,"Process Alert","Product has been cancelled!");
            return;
        }else if(orderStatus==Constants.OrderStatus.delivered.getValue()){
            Utils.showAlertDialog(context,"Process Alert","Product is already delivered!");
            return;
        }else if(orderStatus==Constants.OrderStatus.declined.getValue()){
            Utils.showAlertDialog(context,"Process Alert","Product has been declined!");
            return;
        }else if(orderStatus==Constants.OrderStatus.returned.getValue()){
            Utils.showAlertDialog(context,"Process Alert","Product has been returned!");
            return;
        }

        LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.bottom_sheet_received_order, null);
        final Dialog mBottomSheetDialog = new Dialog(context, R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.getWindow().setBackgroundDrawable(context.getResources().getDrawable(R.drawable.canvas_dialog_background_inset));
        mBottomSheetDialog.show();
        switch (orderStatus){
            case 1:
                view.findViewById(R.id.view_out_delivery).setVisibility(View.GONE);
                view.findViewById(R.id.view_delivered).setVisibility(View.GONE);
                view.findViewById(R.id.view_returned).setVisibility(View.GONE);
                break;
            case 2:
                view.findViewById(R.id.view_processing).setVisibility(View.GONE);
                view.findViewById(R.id.view_delivered).setVisibility(View.GONE);
                view.findViewById(R.id.view_returned).setVisibility(View.GONE);
                view.findViewById(R.id.view_declined).setVisibility(View.GONE);
                break;
            case 3:
                view.findViewById(R.id.view_processing).setVisibility(View.GONE);
                view.findViewById(R.id.view_out_delivery).setVisibility(View.GONE);
                view.findViewById(R.id.view_declined).setVisibility(View.GONE);
                break;
        }

        view.findViewById(R.id.view_processing).setOnClickListener(v -> {
            changeOrderStatus(orderId, Constants.OrderStatus.inProgress.getValue(), position);
            mBottomSheetDialog.dismiss();
        });
        view.findViewById(R.id.view_out_delivery).setOnClickListener(v -> {
            changeOrderStatus(orderId, Constants.OrderStatus.outDelivery.getValue(), position);
            mBottomSheetDialog.dismiss();
        });
        view.findViewById(R.id.view_delivered).setOnClickListener(v -> {
            changeOrderStatus(orderId, Constants.OrderStatus.delivered.getValue(), position);
            mBottomSheetDialog.dismiss();
        });
        view.findViewById(R.id.tv_cancel).setOnClickListener(v -> {
            changeOrderStatus(orderId, Constants.OrderStatus.cancelled.getValue(), position);
            mBottomSheetDialog.dismiss();
        });
        view.findViewById(R.id.view_declined).setOnClickListener(v -> {
            changeOrderStatus(orderId, Constants.OrderStatus.declined.getValue(), position);
            mBottomSheetDialog.dismiss();
        });
        view.findViewById(R.id.view_returned).setOnClickListener(v -> {
            changeOrderStatus(orderId, Constants.OrderStatus.returned.getValue(), position);
            mBottomSheetDialog.dismiss();
        });
    }

    private void changeOrderStatus(int orderId, int orderStatus, int position) {
        showProgress(true);
        ModelManager.modelManager().changeOrderStatus(orderId, orderStatus, (Constants.Status iStatus, GenricResponse<HashMap<String, Object>> genericResponse) -> {
            HashMap<String, Object> map = genericResponse.getObject();
            Integer orderStat = (Integer) map.get(kOrderStatus);
            orderList.get(position).setOrderStatus(orderStat);
            notifyDataSetChanged();
            fragment.loadData(mStatus);
            showProgress(false);
        }, (Constants.Status iStatus, String message) -> {
            Log.e("Order Status", message);
            showProgress(false);
        });
    }


    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) progressDialog.cancel();
        }
    }

}
