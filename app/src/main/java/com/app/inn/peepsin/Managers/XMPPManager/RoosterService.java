package com.app.inn.peepsin.Managers.XMPPManager;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.app.inn.peepsin.Managers.APIManager.ReachabilityManager;


/**
 * Created by akaMahesh on 3/7/17.
 * copyright to : Innverse Technologies
 */

public class RoosterService extends Service implements ReachabilityManager.ConnectivityReceiverListener {
    private static final String TAG = RoosterService.class.getSimpleName();
    private RoosterManager roosterManager;
    @Override
    public void onCreate() {
        super.onCreate();
        roosterManager = RoosterManager.roost();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        roosterManager.connectUserToXMPPServer();
        return Service.START_STICKY;
        //RETURNING START_STICKY CAUSES OUR CODE TO STICK AROUND WHEN THE APP ACTIVITY HAS DIED.
    }

    @Override
    public void onDestroy() {
        stop();
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void stop() {
        RoosterManager.roost().disconnectFromXMPPServer();
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if(isConnected){
            if(roosterManager!=null)
                roosterManager.connectUserToXMPPServer();
        }else
            if(roosterManager!=null)
                roosterManager.disconnectFromXMPPServer();
    }
}
