package com.app.inn.peepsin.UI.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.app.inn.peepsin.Models.OrderHistoryModel;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.adapters.ActivityOrderHistoryAdapter;
import com.app.inn.peepsin.UI.helpers.Toaster;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class OrderHistoryActivity extends AppCompatActivity {

    ExpandableListView expandableListView;
    ExpandableListAdapter expandableListAdapter;
    List<String> expandableListTitle;
    HashMap<String, List<String>> expandableListDetail;
    Integer orderId=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_history);
        ButterKnife.bind(this);

      //  Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

        TextView tvTitle = (TextView)findViewById(R.id.tv_title);
        tvTitle.setText("Order History");

        orderId=getIntent().getIntExtra("OrderId",0);

        expandableListView = (ExpandableListView) findViewById(R.id.expandable_list_view);
        expandableListDetail = OrderHistoryModel.getData();
        expandableListTitle = new ArrayList<>(expandableListDetail.keySet());
        expandableListAdapter = new ActivityOrderHistoryAdapter(this, expandableListTitle, expandableListDetail);
        expandableListView.setAdapter(expandableListAdapter);

        expandableListView.setOnChildClickListener((parent, v, groupPosition, childPosition, id) -> {
            Toaster.toast(expandableListTitle.get(groupPosition)
                    + " -> "
                    + expandableListDetail.get(expandableListTitle.
                    get(groupPosition)).get(childPosition));
            return false;
        });
        loadShoppingCart(orderId);
    }
    @OnClick(R.id.ib_back)
    public void onBack(){
        onBackPressed();
    }


    public void loadShoppingCart(Integer orderId) {

   /* public void loadShoppingCart(Integer orderId, HashMap<String,Object> deliveryAddress) {


        ModelManager.modelManager().getSummaryAfterPayment(orderId,(Constants.Status iStatus, GenricResponse<FinalBillSummary> genericResponse) -> {

            finalBillSummary=genericResponse.getObject();

           *//* expandableListDetail = TransactionDetailModel.getLisData(finalBillSummary);
            expandableListTitle = new ArrayList<String>(expandableListDetail.keySet());
            expandableListAdapter = new TransactionDetailAdapter(this, expandableListTitle, expandableListDetail,finalBillSummary);
            expandableListView.setAdapter(expandableListAdapter);*//*

        }, (Constants.Status iStatus, String message) -> {
            //Log.e(Tag,message);
        });
    }*/
    }
}
