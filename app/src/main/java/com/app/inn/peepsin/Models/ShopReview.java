package com.app.inn.peepsin.Models;

import org.json.JSONObject;

/**
 * Created by dhruv on 1/8/17.
 */

public class ShopReview extends BaseModel{

    private Integer userId;
    private Integer productId;
    private String productName;
    private String username;
    private String coverImageUrl;
    private Integer ratingReviewId;
    private String review;
    private String timeStamp;
    private Integer rating;

    public ShopReview(JSONObject jsonResponse){
        this.userId = getValue(jsonResponse, kUserId, Integer.class);
        this.username = getValue(jsonResponse,kUserName,String.class);
        this.coverImageUrl = getValue(jsonResponse,kCoverImageThumbnailURL,String.class);
        this.productId = getValue(jsonResponse,kProductId,Integer.class);
        this.ratingReviewId = getValue(jsonResponse,kRatingReviewId,Integer.class);
        this.rating = getValue(jsonResponse,kRating,Integer.class);
        this.timeStamp = getValue(jsonResponse,kTimeStamp,String.class);
        this.review = getValue(jsonResponse,kReview,String.class);
        try {
            this.productName = getValue(jsonResponse,kProductName,String.class);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public Integer getUserId() {
        return userId;
    }

    public Integer getProductId() {
        return productId;
    }

    public String getProductName() {
        return productName;
    }

    public String getUsername() {
        return username;
    }

    public String getCoverImageUrl() {
        return coverImageUrl;
    }

    public Integer getRatingReviewId() {
        return ratingReviewId;
    }

    public String getReview() {
        return review;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public Integer getRating() {
        return rating;
    }
}
