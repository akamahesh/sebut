package com.app.inn.peepsin.Managers.XMPPManager;

import org.jivesoftware.smack.packet.ExtensionElement;

/**
 * Created by akaMahesh on 22/11/16.
 * copyright to : Mahesh Bhatt
 * contact : mckay1718@gmail.com
 */
class XMPPMessageExtension implements ExtensionElement, Constant    {
    private String messageType;     //0 for sent and 1 for received
    private String userId;          //senders userId
    private String productId;       //productid for which chat is going on
    private String userImageUrl;    // user sender image url
    private String jabberId;        // senders jabberId
    private String productThumbnailUrl;
    private String userName;   //username of the sender
    private String productName; // product name
    private String timeStamp;   //timestamp
    private String price;   //price
    private String chatOn; //1 for user 2 for product
    private String connectionLevel;


    @Override
    public String getElementName() {
        return kElementMessageData;
    }

    @Override
    public String getNamespace() {
        return String.format(" xmlns=\"%s\"", kNamespaceJabberClient);      //It has no use
    }

    @Override
    public String toXML() {
        StringBuilder builder = new StringBuilder();
        builder.append("<").append(getElementName()).append(">");


        if (messageType != null) {
            builder.append("<messageType>").append(messageType).append("</messageType>");
        }
        if (userId != null) {
            builder.append("<userId>").append(userId).append("</userId>");
        }
        if (productId != null) {
            builder.append("<productId>").append(productId).append("</productId>");
        }
        if (userImageUrl != null) {
            builder.append("<userImageURL>").append(userImageUrl).append("</userImageURL>");
        }
        if (jabberId != null) {
            builder.append("<jabberId>").append(jabberId).append("</jabberId>");
        }
        if (productThumbnailUrl != null) {
            builder.append("<productThumbnailURL>").append(productThumbnailUrl).append("</productThumbnailURL>");
        }
        if (userName != null) {
            builder.append("<userName>").append(userName).append("</userName>");
        }
        if (chatOn != null) {
            builder.append("<chatOn>").append(chatOn).append("</chatOn>");
        }
        if (connectionLevel != null) {
            builder.append("<connectionLevel>").append(connectionLevel).append("</connectionLevel>");
        }
        if (productName != null) {
            builder.append("<productName>").append(productName).append("</productName>");
        }
        if (timeStamp != null) {
            builder.append("<timeStamp>").append(timeStamp).append("</timeStamp>");
        }
        if (price != null) {
            builder.append("<price>").append(price).append("</price>");
        }

        builder.append("</").append(getElementName()).append(">");
        return builder.toString();
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getUserImageUrl() {
        return userImageUrl;
    }

    public void setUserImageUrl(String userImageUrl) {
        this.userImageUrl = userImageUrl;
    }

    public String getJabberId() {
        return jabberId;
    }

    public void setJabberId(String jabberId) {
        this.jabberId = jabberId;
    }

    public String getProductThumbnailUrl() {
        return productThumbnailUrl;
    }

    public void setProductThumbnailUrl(String productThumbnailUrl) {
        this.productThumbnailUrl = productThumbnailUrl;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getChatOn() {
        return chatOn;
    }

    public void setChatOn(String chatOn) {
        this.chatOn = chatOn;
    }

    public String getConnectionLevel() {
        return connectionLevel;
    }

    public void setConnectionLevel(String connectionLevel) {
        this.connectionLevel = connectionLevel;
    }
}
