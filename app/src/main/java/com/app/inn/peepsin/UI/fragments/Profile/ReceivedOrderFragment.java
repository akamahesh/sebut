package com.app.inn.peepsin.UI.fragments.Profile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.OrderHistory;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.adapters.ReceivedOrderAdapter;
import com.app.inn.peepsin.UI.helpers.ItemOffsetDecoration;
import com.app.inn.peepsin.UI.interfaces.Switcher;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kRecords;
import static com.app.inn.peepsin.Constants.Constants.kShopId;
import static com.app.inn.peepsin.Constants.Constants.kStatus;

/**
 * Created by akaMahesh on 2/8/17
 * contact : mckay1718@gmail.com
 */

public class ReceivedOrderFragment extends Fragment implements ViewPager.OnPageChangeListener {

    private String TAG = getTag();
    static Switcher profileSwitcherListener;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.view_pager)
    ViewPager viewPager;
    @BindView(R.id.btn_categories)
    Button btnInProcess;
    @BindView(R.id.btn_shop)
    Button btnNew;
    @BindView(R.id.btn_shared_product)
    Button btnCompleted;

    private int shopId;

    public static Fragment newInstance(Switcher switcher, int shopId) {
        profileSwitcherListener = switcher;
        Fragment fragment = new ReceivedOrderFragment();
        Bundle bdl = new Bundle();
        bdl.putInt(kShopId, shopId);
        fragment.setArguments(bdl);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            shopId = bundle.getInt(kShopId);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_order_layout, container, false);
        ButterKnife.bind(this, view);
        tvTitle.setText(R.string.my_order_history);
        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(getChildFragmentManager());
        sectionsPagerAdapter.addItem(2);
        sectionsPagerAdapter.addItem(1);
        sectionsPagerAdapter.addItem(3);
        viewPager.setAdapter(sectionsPagerAdapter);
        viewPager.addOnPageChangeListener(this);
        btnInProcess.setText("In Process");
        btnNew.setText("New");
        btnCompleted.setText("Completed");
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewPager.setCurrentItem(1);
    }


    @OnClick(R.id.btn_categories)
    void onInProcess() {
        reset();
        btnInProcess.setBackground(this.getResources().getDrawable(R.color.ButtonShopCategoryColor));
        btnInProcess.setTextColor(this.getResources().getColor(R.color.text_color_white));
        viewPager.setCurrentItem(0);

    }

    @OnClick(R.id.btn_shop)
    void onNew() {
        reset();
        btnNew.setBackground(this.getResources().getDrawable(R.color.ButtonShopCategoryColor));
        btnNew.setTextColor(this.getResources().getColor(R.color.text_color_white));
        viewPager.setCurrentItem(1);
    }

    @OnClick(R.id.btn_shared_product)
    void onCompleted() {
        reset();
        btnCompleted.setBackground(this.getResources().getDrawable(R.color.ButtonShopCategoryColor));
        btnCompleted.setTextColor(this.getResources().getColor(R.color.text_color_white));
        viewPager.setCurrentItem(2);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        Log.d(TAG, "onPageScrolled: ");
    }

    @Override
    public void onPageSelected(int position) {
        if(position==0){
            onInProcess();
        }else if(position==1){
            onNew();
        }else if(position==2){
            onCompleted();
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        Log.d(TAG, "onPageScrolled: ");
    }


    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        ArrayList<Integer> statuses = new ArrayList<>();

        SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return PlaceholderFragment.newInstance(shopId,statuses.get(position));
        }

        public void addItem(Integer status) {
            statuses.add(status);
        }

        @Override
        public int getCount() {
            return statuses.size();
        }
    }

    void reset() {
        btnInProcess.setBackground(null);
        btnInProcess.setTextColor(this.getResources().getColor(R.color.action_color));
        btnNew.setBackground(null);
        btnNew.setTextColor(this.getResources().getColor(R.color.action_color));
        btnCompleted.setBackground(null);
        btnCompleted.setTextColor(this.getResources().getColor(R.color.action_color));
    }


    @OnClick(R.id.btn_back)
    void onBack() {
        getFragmentManager().popBackStack();
    }

    public static class PlaceholderFragment extends Fragment {

        @BindView(R.id.empty_view)
        View emptyView;
        @BindView(R.id.tv_empty_title)
        TextView tvEmptyTitle;
      /*  @BindView(R.id.tv_empty_message)
        TextView tvEmptyMessage;*/
        @BindView(R.id.rv_order_history)
        RecyclerView mRecyclerView;
        @BindView(R.id.swipe_refresh)
        SwipeRefreshLayout swipeRefreshLayout;
        @BindView(R.id.top_date_layout)
        View dateLayout;
        @BindView(R.id.tv_order_date)
        TextView tvOrderDate;

        private int mPage = 1;
        private boolean isLoading = true;
        private int pageSize;

        private Integer mShopID;
        private Integer mStatus;
        private ArrayList<OrderHistory> orderList;
        private LinearLayoutManager mLayoutManager;
        private ReceivedOrderAdapter receivedOrderAdapter;

        public static PlaceholderFragment newInstance(int shopID,int status) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(kShopId,shopID);
            args.putInt(kStatus,status);

            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            orderList = new ArrayList<>();
            Bundle bundle = getArguments();
            if (bundle != null) {
                mShopID = bundle.getInt(kShopId,0);
                mStatus = bundle.getInt(kStatus,0);
            }
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_placeholder, container, false);
            ButterKnife.bind(this, rootView);
            tvEmptyTitle.setText(R.string.order_no_received_order_found);
            //tvEmptyMessage.setText(R.string.order_history_empty_message);

            receivedOrderAdapter = new ReceivedOrderAdapter(profileSwitcherListener, getContext(), orderList,mStatus,this);
            mRecyclerView.setHasFixedSize(true);
            mLayoutManager = new LinearLayoutManager(getActivity());
            mRecyclerView.setLayoutManager(mLayoutManager);
            mRecyclerView.setAdapter(receivedOrderAdapter);
            mRecyclerView.addOnScrollListener(onScrollListener);
            swipeRefreshLayout.setOnRefreshListener(onRefreshListener);

            return rootView;
        }

        @Override
        public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);
            loadData(mStatus);
        }

        public void initialState(){
            mPage=1;
            isLoading=true;
            pageSize= orderList.size();
        }

        SwipeRefreshLayout.OnRefreshListener onRefreshListener = () -> {
            loadData(mStatus);
        };

        RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int visibleItemCount = mLayoutManager.getChildCount();
                int totalItemCount = mLayoutManager.getItemCount();
                int firstVisibleItemPosition = mLayoutManager.findFirstVisibleItemPosition();
                if(dy > 0) //check for scroll down
                {
                    if (isLoading)
                    {
                        if ( (visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                                && firstVisibleItemPosition >= 0
                                && totalItemCount >= pageSize) {
                            isLoading = false;
                            ++mPage;
                            //Do pagination.. i.e. fetch new data
                            loadMoreData(mStatus);
                        }
                    }
                }
                tvOrderDate.setText(receivedOrderAdapter.getItem(firstVisibleItemPosition).getOrderDate());
            }
        };

        public void loadData(int status) {
            swipeRefreshLayout.setRefreshing(true);
            ModelManager.modelManager().getReceivedOrder(mShopID, 1, status,(Constants.Status iStatus, GenricResponse<HashMap<String, Object>> genericResponse) -> {
                HashMap<String, Object> map = genericResponse.getObject();
                List<OrderHistory> orderHistoryList = (List<OrderHistory>) map.get(kRecords);
                receivedOrderAdapter.addItems(orderHistoryList);
                checkEmptyScreen();
                initialState();
                swipeRefreshLayout.setRefreshing(false);
            }, (Constants.Status iStatus, String message) -> {
                Log.e("received order", message);
                swipeRefreshLayout.setRefreshing(false);
                checkEmptyScreen();
            });
        }

        private void loadMoreData(int status) {
            swipeRefreshLayout.setRefreshing(true);
            ModelManager.modelManager().getReceivedOrder(mShopID, mPage, status,(Constants.Status iStatus, GenricResponse<HashMap<String, Object>> genericResponse) -> {
                HashMap<String, Object> map = genericResponse.getObject();
                List<OrderHistory> orderHistoryList = (List<OrderHistory>) map.get(kRecords);
                receivedOrderAdapter.addNewItems(orderHistoryList);
                isLoading = !genericResponse.getObject().isEmpty();
                swipeRefreshLayout.setRefreshing(false);
            }, (Constants.Status iStatus, String message) -> {
                Log.e("received order", message);
                swipeRefreshLayout.setRefreshing(false);
                checkEmptyScreen();
            });
        }

        private void checkEmptyScreen() {
            if (receivedOrderAdapter.getItemCount() > 0) {
                emptyView.setVisibility(View.GONE);
                tvOrderDate.setText(receivedOrderAdapter.getItem(0).getOrderDate());
            } else {
                emptyView.setVisibility(View.VISIBLE);
                dateLayout.setVisibility(View.GONE);
            }
        }
    }


}
