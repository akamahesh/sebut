package com.app.inn.peepsin.UI.fragments.Connections;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.MyShop;
import com.app.inn.peepsin.Models.Requests;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.interfaces.Switcher;

import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kFirstName;
import static com.app.inn.peepsin.Constants.Constants.kShop;
import static com.app.inn.peepsin.Constants.Constants.kUserId;

/**
 * Created by mahesh on 6/4/17.
 */

public class RequestsFragment extends Fragment {
    private final String TAG = getClass().getSimpleName();
    String defaultReceived;
    String defaultSent;
    int tabselected=0;

    @BindView(R.id.btn_request_received) Button btnReceived;
    @BindView(R.id.btn_request_sent) Button btnSent;

    public static Switcher switcherListener;

    public static Fragment newInstance(Switcher switcher,int tabselected){
        switcherListener = switcher;
        RequestsFragment fragment = new RequestsFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("tabSelected",tabselected);

        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_requests, container, false);
        ButterKnife.bind(this,view);

        defaultReceived = "Request Received(0)";
        defaultSent = "Request Sent(0)";
        btnSent.setText(defaultSent);
        btnReceived.setText(defaultReceived);
        Bundle bundle = getArguments();
        if(bundle!= null){
            tabselected = bundle.getInt("tabSelected");

        }
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadRequestReceived();
        loadRequestSent();

    }

    @Override
    public void onResume() {
        super.onResume();


    }

    @OnClick(R.id.btn_request_sent)
    void requestSent(){
        switcherListener.switchFragment(RequestSentFragment.newInstance(switcherListener,tabselected),true,true);
    }

    @OnClick(R.id.btn_request_received)
    void requestReceived(){
        switcherListener.switchFragment(RequestReceivedFragment.newInstance(switcherListener,tabselected),true,true);
    }

    public void loadRequestReceived() {
         ModelManager.modelManager().getRequestReceived((Constants.Status iStatus,GenricResponse<CopyOnWriteArrayList<Requests>> genricResponse) -> {
    try {
    String count = getString(R.string.request_received_text) + "("
            + String.valueOf(genricResponse.getObject().size()) + ")";
    btnReceived.setText(count);
    }catch (Exception ex){
      ex.printStackTrace();
     }
        }, (Constants.Status iStatus, String message) -> {
            Log.d(TAG,  message);
        });
    }

    public void loadRequestSent() {
        ModelManager.modelManager().getRequestSent((Constants.Status iStatus,GenricResponse<CopyOnWriteArrayList<Requests>> genricResponse) -> {
            try {
                String count = getString(R.string.request_sent_text) + "("
                        + String.valueOf(genricResponse.getObject().size()) + ")";
                btnSent.setText(count);
            }
            catch(Exception e) {
                e.printStackTrace();
            }
        }, (Constants.Status iStatus, String message) -> {
            Log.d(TAG,  message);
        });

    }

    @Override
    public void onDetach() {
        super.onDetach();
        switcherListener=null;
    }

}
