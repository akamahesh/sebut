package com.app.inn.peepsin.UI.fragments.Shop;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.BaseModel;
import com.app.inn.peepsin.Models.SellingProduct;
import com.app.inn.peepsin.Models.ShopCategory;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.adapters.ManageInventoryAdapter;
import com.app.inn.peepsin.UI.adapters.ShopInventoryAdapter;
import com.app.inn.peepsin.UI.helpers.DividerItemRecyclerDecoration;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kShopId;

/**
 * Created by Harsh on 8/23/2017.
 */

public class ManageInventoryFragment extends Fragment implements ShopInventoryAdapter.InventoryFilter{

    private static Switcher switcherListener;
    private String TAG = getTag();
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.empty_view)
    View emptyView;
    @BindView(R.id.btn_filter)
    ImageButton btnFilter;
    @BindView(R.id.recycler_view_category)
    RecyclerView recyclerViewCategory;
    @BindView(R.id.recycler_view_inventory)
    RecyclerView recyclerView;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    private List<SellingProduct> productList;
    private List<SellingProduct> filterList;
    private List<ShopCategory> inventoryList;
    private ProgressDialog progressDialog;
    private int userId;
    private int shopId;

    private ManageInventoryAdapter manageInventoryAdapter;
    private ShopInventoryAdapter inventoryAdapter;

    public static Fragment newInstance(Switcher switcher,int shopId) {
        switcherListener=switcher;
        ManageInventoryFragment fragment = new ManageInventoryFragment();
        Bundle bdl = new Bundle();
        bdl.putInt(kShopId,shopId);
        fragment.setArguments(bdl);
        return fragment;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = Utils.generateProgressDialog(getContext(),false);
        productList = new ArrayList<>();
        filterList= new ArrayList<>();
        inventoryList = new ArrayList<>();
        userId = ModelManager.modelManager().getCurrentUser().getUserId();
        Bundle bundle = getArguments();
        if(bundle!=null)
            shopId = bundle.getInt(kShopId);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_manage_inventory, container, false);
        ButterKnife.bind(this, view);
        tvTitle.setText(getString(R.string.title_manage_inventory));

        manageInventoryAdapter = new ManageInventoryAdapter(getContext(), productList,this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(manageInventoryAdapter);
        recyclerView.addItemDecoration(new DividerItemRecyclerDecoration(getActivity(), R.drawable.canvas_recycler_divider));
        swipeRefreshLayout.setOnRefreshListener(onRefreshListener);

        // Inventory Adapter and Inventory List
        inventoryAdapter = new ShopInventoryAdapter(getContext(), inventoryList, this);
        LinearLayoutManager HorizontalLayout = new LinearLayoutManager(getContext(),
                LinearLayoutManager.HORIZONTAL, false);
        recyclerViewCategory.setLayoutManager(HorizontalLayout);
        recyclerViewCategory.setAdapter(inventoryAdapter);
        return view;
    }

    SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            loadData(userId);
            loadShopCategory();
        }
    };

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData(userId);
        loadShopCategory();
    }

    @OnClick(R.id.btn_back)
    void onBack(){
        getFragmentManager().popBackStack();
    }

    @OnClick(R.id.btn_filter)
    void onFilter(){
        if(recyclerViewCategory.getVisibility()==View.VISIBLE) {
            clearFilter();
            recyclerViewCategory.setVisibility(View.GONE);
        } else
            recyclerViewCategory.setVisibility(View.VISIBLE);
    }

    public void loadData(int userId) {
        swipeRefreshLayout.setRefreshing(true);
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(BaseModel.kUserId, userId);
        ModelManager.modelManager().getSellingProducts(parameters, (Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<SellingProduct>> genericResponse) -> {
            productList = genericResponse.getObject();
            manageInventoryAdapter.addItems(productList);
            swipeRefreshLayout.setRefreshing(false);
            checkEmptyScreen();
        }, (Constants.Status iStatus, String message) -> {
            Toaster.toast(message);
            swipeRefreshLayout.setRefreshing(false);
            checkEmptyScreen();
        });
    }

    public void loadShopCategory() {
        ModelManager.modelManager().getShopCategoryListSeller(shopId, (Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<ShopCategory>> genericResponse) -> {
            inventoryList = genericResponse.getObject();
            inventoryAdapter.addItems(inventoryList);
        }, (Constants.Status iStatus, String message) -> {
            Log.e(TAG, message);
            Toaster.toast(message);
        });
    }

    void clearFilter() {
        manageInventoryAdapter.addItems(productList);
        checkEmptyScreen();
        for (ShopCategory shopCategory : inventoryList) {
            shopCategory.setSelected(false);
        }
    }

    private void checkEmptyScreen() {
        if (manageInventoryAdapter.getItemCount() > 0) {
            emptyView.setVisibility(View.GONE);
            btnFilter.setVisibility(View.VISIBLE);
        } else {
            emptyView.setVisibility(View.VISIBLE);
            btnFilter.setVisibility(View.INVISIBLE);
        }
    }

    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) {
                progressDialog.cancel();
            }
        }
    }

    @Override
    public void filter(Integer id) {
        showProgress(true);
        filterList.clear();
        for (SellingProduct sellingP:productList) {
            if(sellingP.getCategoryId().equals(id))
                filterList.add(sellingP);
        }
        manageInventoryAdapter.addItems(filterList);
        checkEmptyScreen();
        showProgress(false);
    }
}
