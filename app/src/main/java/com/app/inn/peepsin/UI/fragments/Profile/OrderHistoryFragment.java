package com.app.inn.peepsin.UI.fragments.Profile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.OrderHistory;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.adapters.OrderHistoryAdapter;
import com.app.inn.peepsin.UI.interfaces.Switcher;

import java.util.HashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kOrderList;
import static com.app.inn.peepsin.Constants.Constants.kRecords;

public class OrderHistoryFragment extends Fragment {

    private String TAG = getTag();
    static Switcher profileSwitcherListener;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.empty_view)
    View emptyView;
    @BindView(R.id.tv_empty_title)
    TextView tvEmptyTitle;
    @BindView(R.id.tv_empty_message)
    TextView tvEmptyMessage;
    /*@BindView(R.id.lv_expandble_order_history)
    ExpandableListView ivExpandbleOrderHistory;*/
    @BindView(R.id.rv_order_history)
    RecyclerView mRecyclerView;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.top_date_layout)
    View dateLayout;
    @BindView(R.id.tv_order_date)
    TextView tvOrderDate;

    //private NewOrderHistoryAdapter newOrderHistoryAdapter;
    private OrderHistoryAdapter orderHistoryAdapter;
    private CopyOnWriteArrayList<OrderHistory> orderList;
    private LinearLayoutManager mLayoutManager;

    // AppBarLayout appBarLayout;
    private int mPage = 1;
    private boolean isLoading = true;
    private int pageSize;

    public static Fragment newInstance(Switcher switcher, CopyOnWriteArrayList<OrderHistory> orderList) {
        profileSwitcherListener = switcher;
        Fragment fragment = new OrderHistoryFragment();
        Bundle bdl = new Bundle();
        bdl.putSerializable(kOrderList, orderList);
        fragment.setArguments(bdl);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        orderList = new CopyOnWriteArrayList<>();
        Bundle bundle = getArguments();
        if (bundle != null)
            orderList = (CopyOnWriteArrayList<OrderHistory>) bundle.getSerializable(kOrderList);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_order_history, container, false);
        ButterKnife.bind(this, view);
        /*appBarLayout=((HomeActivity) getActivity()).getTabLayout();
        Utils.isprofileClicked=1;*/
        tvEmptyMessage.setVisibility(View.VISIBLE);
        tvTitle.setText(R.string.order_history);
        tvEmptyTitle.setText(R.string.order_history_empty_title);
        tvEmptyMessage.setText(R.string.order_history_empty_message);

        orderHistoryAdapter = new OrderHistoryAdapter(profileSwitcherListener, getContext(), orderList);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(orderHistoryAdapter);
        checkEmptyScreen();
        mRecyclerView.addOnScrollListener(onScrollListener);
        swipeRefreshLayout.setOnRefreshListener(onRefreshListener);
        return view;
    }

    public void initialState() {
        mPage = 1;
        isLoading = true;
        pageSize = orderList.size();
    }

    SwipeRefreshLayout.OnRefreshListener onRefreshListener = this::refreshOrderHistory;

    RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            int visibleItemCount = mLayoutManager.getChildCount();
            int totalItemCount = mLayoutManager.getItemCount();
            int firstVisibleItemPosition = mLayoutManager.findFirstVisibleItemPosition();
            if (dy > 0) //check for scroll down
            {
                if (isLoading) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= pageSize) {
                        isLoading = false;
                        ++mPage;
                        //Do pagination.. i.e. fetch new data
                        loadMoreOrder();
                    }
                }
            }
            tvOrderDate.setText(orderHistoryAdapter.getItem(firstVisibleItemPosition).getOrderDate());
        }
    };

    public void refreshOrderHistory() {
        swipeProgress(true);
        ModelManager.modelManager().getOrderHistory(1, (Constants.Status iStatus, GenricResponse<HashMap<String, Object>> genericResponse) -> {
            HashMap<String, Object> map = genericResponse.getObject();
            orderList = (CopyOnWriteArrayList<OrderHistory>) map.get(kRecords);
            orderHistoryAdapter.addItems(orderList);
            checkEmptyScreen();
            initialState();
            swipeProgress(false);
            //setOrderHistory(list);
        }, (Constants.Status iStatus, String message) -> {
            Log.e(TAG, message);
            checkEmptyScreen();
            swipeProgress(false);
        });
    }

    public void loadMoreOrder() {
        swipeProgress(true);
        ModelManager.modelManager().getOrderHistory(mPage, (Constants.Status iStatus, GenricResponse<HashMap<String, Object>> genericResponse) -> {
            HashMap<String, Object> map = genericResponse.getObject();
            CopyOnWriteArrayList<OrderHistory> list = (CopyOnWriteArrayList<OrderHistory>) map.get(kRecords);
            orderList.addAll(list);
            orderHistoryAdapter.addMoreItems(list);
            isLoading = !genericResponse.getObject().isEmpty();
            swipeProgress(false);
        }, (Constants.Status iStatus, String message) -> {
            Log.e(TAG, message);
            swipeProgress(false);
        });
    }

    /*private void setOrderHistory(CopyOnWriteArrayList<OrderHistory> orderList){
        newOrderHistoryAdapter = new NewOrderHistoryAdapter(getContext(), orderList);
        ivExpandbleOrderHistory.setAdapter(newOrderHistoryAdapter);
    }*/

    @Override
    public void onResume() {
        super.onResume();
        //appBarLayout.setVisibility(View.GONE);
    }

    @OnClick(R.id.btn_back)
    void onBack() {
        getFragmentManager().popBackStack();
        /*Utils.isprofileClicked=0;
        appBarLayout.setVisibility(View.VISIBLE);*/
    }

    private void swipeProgress(boolean b) {
        if (b) {
            swipeRefreshLayout.setRefreshing(true);
        } else {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void checkEmptyScreen() {
        if (orderHistoryAdapter.getItemCount() > 0) {
            emptyView.setVisibility(View.GONE);
            tvOrderDate.setText(orderHistoryAdapter.getItem(0).getOrderDate());
        } else {
            emptyView.setVisibility(View.VISIBLE);
            dateLayout.setVisibility(View.GONE);
        }
    }
}
