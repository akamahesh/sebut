package com.app.inn.peepsin.Models;

import java.io.Serializable;
import org.json.JSONObject;

/**
 * Created by Harsh on 9/28/2017.
 */

public class CheckLatestBuild extends BaseModel implements Serializable {

  private String versionNumber;
  private String buildNumber;
  private String versionDescription;
  private Boolean isMandatoryUpdate;
  private String rolloutTimestamp;
  private String versionMandatoryTimestamp;
  private Integer buildType;
  private Integer deviceType;
  private String minOSVersion;
  private String maxOSVersion;

  public CheckLatestBuild(JSONObject jsonResponse) {
    this.versionNumber = getValue(jsonResponse, kVersionNumber, String.class);
    this.buildNumber = getValue(jsonResponse, kBuildNumber, String.class);
    this.versionDescription = getValue(jsonResponse, kVersionDescription, String.class);
    this.isMandatoryUpdate = getValue(jsonResponse, kIsMandatoryUpdate, Boolean.class);
    this.rolloutTimestamp = getValue(jsonResponse, kRolloutTimestamp, String.class);
    this.versionMandatoryTimestamp = getValue(jsonResponse, kVersionMandatoryTimestamp,
        String.class);
    this.buildType = getValue(jsonResponse, kBuildType, Integer.class);
    this.deviceType = getValue(jsonResponse, kDeviceType, Integer.class);
    this.minOSVersion = getValue(jsonResponse, kMinOSVersion, String.class);
    this.maxOSVersion = getValue(jsonResponse, kMaxOSVersion, String.class);

  }

  public String getVersionNumber() {
    return versionNumber;
  }

  public String getBuildNumber() {
    return buildNumber;
  }

  public String getVersionDescription() {
    return versionDescription;
  }


  public String getRolloutTimestamp() {
    return rolloutTimestamp;
  }

  public String getVersionMandatoryTimestamp() {
    return versionMandatoryTimestamp;
  }

  public Integer getBuildType() {
    return buildType;
  }

  public Integer getDeviceType() {
    return deviceType;
  }

  public String getMinOSVersion() {
    return minOSVersion;
  }

  public String getMaxOSVersion() {
    return maxOSVersion;
  }

  public void setVersionMandatoryTimestamp(String versionMandatoryTimestamp) {
    this.versionMandatoryTimestamp = versionMandatoryTimestamp;
  }

  public void setVersionNumber(String versionNumber) {
    this.versionNumber = versionNumber;
  }

  public void setBuildNumber(String buildNumber) {
    this.buildNumber = buildNumber;
  }

  public void setVersionDescription(String versionDescription) {
    this.versionDescription = versionDescription;
  }

  public Boolean getMandatoryUpdate() {
    return isMandatoryUpdate;
  }

  public void setMandatoryUpdate(Boolean mandatoryUpdate) {
    isMandatoryUpdate = mandatoryUpdate;
  }

  public void setRolloutTimestamp(String rolloutTimestamp) {
    this.rolloutTimestamp = rolloutTimestamp;
  }

  public void setBuildType(Integer buildType) {
    this.buildType = buildType;
  }

  public void setDeviceType(Integer deviceType) {
    this.deviceType = deviceType;
  }

  public void setMinOSVersion(String minOSVersion) {
    this.minOSVersion = minOSVersion;
  }

  public void setMaxOSVersion(String maxOSVersion) {
    this.maxOSVersion = maxOSVersion;
  }
}
