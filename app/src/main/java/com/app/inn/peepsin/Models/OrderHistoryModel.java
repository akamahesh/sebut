package com.app.inn.peepsin.Models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class OrderHistoryModel {
    public static HashMap<String, List<String>> getData() {
        HashMap<String, List<String>> expandableListDetail = new HashMap<String, List<String>>();

        List<String> cricket = new ArrayList<String>();
        cricket.add("India");
        cricket.add("Pakistan");
        cricket.add("Australia");
        cricket.add("England");
        cricket.add("South Africa");

        List<String> football = new ArrayList<String>();
        football.add("Brazil");
        football.add("Spain");
        football.add("Germany");
        football.add("Netherlands");
        football.add("Italy");

        List<String> basketball = new ArrayList<String>();
        basketball.add("United States");
        basketball.add("Spain");
        basketball.add("Argentina");
        basketball.add("France");
        basketball.add("Russia");



        expandableListDetail.put("Order Details (8)", cricket);
        expandableListDetail.put("Order Details (9)", football);
       /* expandableListDetail.put("Order Details (10)", basketball);
        expandableListDetail.put("Order Details (11)", basketball);
        expandableListDetail.put("Order Details (13)", basketball);
        expandableListDetail.put("Order Details (14)", basketball);
        expandableListDetail.put("Order Details (15)", basketball);
        expandableListDetail.put("Order Details (16)", basketball);*/
        return expandableListDetail;
    }
}
