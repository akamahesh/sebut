package com.app.inn.peepsin.UI.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.app.inn.peepsin.Models.ShopCategory;
import com.app.inn.peepsin.R;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by kim on 18/7/17.
 */

public class FeedsCategoryAdapter extends RecyclerView.Adapter<FeedsCategoryAdapter.FeedInventoryViewHolder> {

    private List<ShopCategory> inventoryList;
    private ColorGenerator generator = ColorGenerator.MATERIAL;
    Context context;
    private FeedsCategoryFilter feedsCategoryListener;


    public interface FeedsCategoryFilter{
        void Categoryfilter(Integer id);
    }


    public FeedsCategoryAdapter(List<ShopCategory> list, Context context,FeedsCategoryFilter feedsCategoryListener) {
        this.inventoryList = list;
        this.context = context;
        this.feedsCategoryListener = feedsCategoryListener;
    }

    @Override
    public FeedInventoryViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_feeds_inventrory_base, viewGroup, false);
        return new FeedInventoryViewHolder(v);
    }

    @Override
    public void onBindViewHolder(FeedInventoryViewHolder holder, int position) {
        ShopCategory shop = inventoryList.get(position);
        String name = shop.getName();
        holder.bindContent(shop);

        TextDrawable drawable = TextDrawable.builder().buildRound(String.valueOf(name.toUpperCase().charAt(0)), generator.getRandomColor());

        if (!shop.getImageUrl().isEmpty()) {
            Picasso.with(context)
                    .load(shop.getImageUrl())
                    .placeholder(R.drawable.img_product_placeholder)
                    .into(holder.ivItemImage);
        } else {
            holder.ivItemImage.setImageDrawable(drawable);
        }

        holder.tvItemName.setText(inventoryList.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return inventoryList.size();
    }

    public void addItems(CopyOnWriteArrayList<ShopCategory> list){
        inventoryList.clear();
        inventoryList.addAll(list);
        notifyDataSetChanged();
    }

    public void addItem(CopyOnWriteArrayList<ShopCategory> list) {
        inventoryList.addAll(list);
        inventoryList.clear();
        notifyDataSetChanged();
    }

    class FeedInventoryViewHolder extends RecyclerView.ViewHolder{
        ShopCategory item;

        @BindView(R.id.iv_item_image)
        ImageView ivItemImage;
        @BindView(R.id.tv_item_name)
        TextView tvItemName;

        FeedInventoryViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        @OnClick(R.id.item_view)
        void onInventory(){
            feedsCategoryListener.Categoryfilter(item.getId());
        }

        public void bindContent(ShopCategory shop) {
            this.item = shop;
        }
    }
}

