package com.app.inn.peepsin.UI.fragments.Profile;

import android.app.ProgressDialog;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.Address;
import com.app.inn.peepsin.Models.Connection;
import com.app.inn.peepsin.Models.SocialLink;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.helpers.DividerItemRecyclerDecoration;
import com.app.inn.peepsin.UI.helpers.ItemOffsetDecoration;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.view.View.GONE;
import static com.app.inn.peepsin.Constants.Constants.kRecords;
import static com.app.inn.peepsin.Constants.Constants.kUserId;

/**
 * Created by root on 5/3/2017.
 */

public class UserProfileFriendFragment extends Fragment {

    @BindView(R.id.recycler_view_feeds) RecyclerView recyclerViewFeeds;
    @BindView(R.id.tv_title) TextView tvTitle;
    @BindView(R.id.empty_view) View emptyView;
    @BindView(R.id.tv_friends) TextView tvFriends;
    @BindView(R.id.tv_empty_message) TextView tvEmptyTextMessage;
    @BindView(R.id.iv_profile_image) ImageView ivProfileImage;
    @BindView(R.id.tv_user_name) TextView tvUserName;
    @BindView(R.id.tv_level) TextView tvConnectionLevel;

    private static Switcher switcherListener;
    private ProgressDialog progressDialog;
    private FriendsAdapter friendsAdapter;
    private Connection connection;
    private CopyOnWriteArrayList<Connection> connectionList;

    public static Fragment newInstance(Switcher switcher,Connection connection) {
        switcherListener = switcher;
        Fragment fragment =  new UserProfileFriendFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(kRecords,connection);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = Utils.generateProgressDialog(getContext(),false);
        connectionList= new CopyOnWriteArrayList<>();
        Bundle bundle = getArguments();
        if(bundle!=null){
            connection = (Connection) bundle.getSerializable(kRecords);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_friends_user_profile, container, false);
        ButterKnife.bind(this,view);
        String title = connection.getFirstName()+"'s Friends";
        tvTitle.setText(title);
        tvFriends.setText(title);
        tvEmptyTextMessage.setText(getString(R.string.empty_message_no_user_friends));

        friendsAdapter = new FriendsAdapter(connectionList);
        recyclerViewFeeds.setHasFixedSize(true);
        recyclerViewFeeds.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerViewFeeds.addItemDecoration(new ItemOffsetDecoration(getContext(),R.dimen.item_offset_linear));
        recyclerViewFeeds.setAdapter(friendsAdapter);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        userCircleData(connection);
        loadData(connection.getUserId());
    }

    @OnClick(R.id.btn_back)
    void onBack(){
        getFragmentManager().popBackStack();
    }




    private void userCircleData(Connection connection){
        String imageUrl = connection.getProfilePicURL();
        String name = connection.getFullName();
        Integer connectionLevel = connection.getConnectionLevel();


        if (connectionLevel == 1) {
            tvConnectionLevel.setText("1st");
        } else if (connectionLevel == 2) {
            tvConnectionLevel.setText("2nd");
        } else if (connectionLevel == 3) {
            tvConnectionLevel.setText("3rd");
        } else {
            tvConnectionLevel.setVisibility(View.INVISIBLE);
        }

        tvUserName.setText(name);
        if (!imageUrl.isEmpty()) {
            Picasso.with(getContext())
                    .load(imageUrl)
                    .resize(200, 200)
                    .placeholder(R.drawable.img_profile_placeholder)
                    .into(ivProfileImage);
        }

    }





    public void loadData(int userid) {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(kUserId,userid);
        showProgress(true);
        ModelManager.modelManager().getUserConnections(parameters,(Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<Connection>> genricResponse) -> {
            connectionList.clear();
            connectionList.addAll(genricResponse.getObject());
            friendsAdapter.notifyDataSetChanged();
            showProgress(false);
            checkEmptyScreen();
        }, (Constants.Status iStatus, String message) -> {
            Toaster.toast(message);
            showProgress(false);
            checkEmptyScreen();
        });
    }

    void checkEmptyScreen(){
        if(friendsAdapter.getItemCount()>0){
            emptyView.setVisibility(GONE);
        }else {
            emptyView.setVisibility(View.VISIBLE);

        }
    }

    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) progressDialog.cancel();
        }
    }

    class FriendsAdapter extends RecyclerView.Adapter<UserProfileFriendFragment.FriendsAdapter.StuffHolder>{

        private List<Connection> list;
        FriendsAdapter(List<Connection> connectionList) {
            this.list = connectionList;
        }

        @Override
        public UserProfileFriendFragment.FriendsAdapter.StuffHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_user_friend, parent, false);
            return new StuffHolder(layoutView);
        }

        @Override
        public void onBindViewHolder(StuffHolder holder, int position) {
            Connection item = list.get(position);
            holder.bindContent(item);
            String name = item.getFullName();
            String fname = item.getFirstName();
            Integer  connectionLevel = item.getConnectionLevel();
            String profileURL = item.getProfilePicURL();
            String sAddress = "No Location Available";
            Address address = item.getPrimaryAddress();
            String rating = Integer.toString(item.getRating());
            if(address!=null){
                String street = "";
                if(address.getStreetAddress() != "") {
                    street =address.getStreetAddress() + ", ";
                }
                String city = "";
                String state = "";
                String country = "";
                if(address.getCity() != ""){
                    city = address.getCity()+ ", ";}
                if(address.getCountry() != ""){
                    state = address.getState()+ ", ";}
                if(address.getCountry() != "") {
                    country = address.getCountry();}
                sAddress = street + city + state + country;
            }

            holder.tvName.setText(name);
            holder.tvProfile.setText(fname);
            holder.tvAddress.setText(sAddress);


            if(item.getShopName() !="") {
                holder.vLayout.setVisibility(View.VISIBLE);
                holder.tvProfile.setText(/*"Shop: " + */item.getShopName());
            }else holder.vLayout.setVisibility(View.INVISIBLE);

            holder.tvShopRating.setText(rating);


            if(connectionLevel ==1) {
                holder.tvConnectionLevel.setText("1st");
            }else if(connectionLevel ==2){
                holder.tvConnectionLevel.setText("2nd");
            }else if(connectionLevel == 3){
                holder.tvConnectionLevel.setText("3rd");
            }else {
                holder.tvConnectionLevel.setVisibility(View.INVISIBLE);
            }


            if(profileURL.isEmpty()){
                holder.ivCircularUserImage.setVisibility(View.INVISIBLE);
                holder.ivUserImage.setVisibility(View.VISIBLE);
            }else {
                holder.ivCircularUserImage.setVisibility(View.VISIBLE);
                holder.ivUserImage.setVisibility(View.INVISIBLE);
            }

            TextDrawable drawable = TextDrawable.builder().buildRound(String.valueOf(name.toUpperCase().charAt(0)), ColorGenerator.MATERIAL.getRandomColor());
            if (!profileURL.isEmpty()) {
                Picasso.with(getContext())
                        .load(profileURL)
                        .placeholder(holder.profilePlaceholder)
                        .into(holder.ivCircularUserImage);
            } else {
                holder.ivUserImage.setImageDrawable(drawable);
            }
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        class StuffHolder extends RecyclerView.ViewHolder{
            private Connection conn;
            @BindView(R.id.tv_name) TextView tvName;
            @BindDrawable(R.drawable.img_profile_placeholder)
            Drawable profilePlaceholder;
            @BindView(R.id.tv_profile) TextView tvProfile;
            @BindView(R.id.iv_user_image_circular) ImageView ivCircularUserImage;
            @BindView(R.id.iv_user_image) ImageView ivUserImage;
            @BindView(R.id.tv_address) TextView tvAddress;
            @BindView(R.id.ll_layout) View vLayout;
            @BindView(R.id.tv_shop_rating) TextView tvShopRating;
            @BindView(R.id.tv_level) TextView tvConnectionLevel;

            StuffHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this,itemView);
            }

            @OnClick(R.id.item_view)
            void onItemClick(){
                if(ModelManager.modelManager().getCurrentUser().getEmail().getContactId().equals(conn.getEmail().getContactId()) ) {
                    Toaster.customToast("You are already logged-in user");
                }else {
                    if(connection.getConnectionLevel()<=3)
                        getUserProfile(conn.getUserId());
                    else
                        Utils.showAlertDialog(getContext(),"Message","You would be able to see 3rd level friends");
                }
            }

            private void bindContent(Connection item) {
                this.conn = item;
            }
        }
    }


    void getUserProfile(int userId) {
        showProgress(true);
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(kUserId, userId);
        ModelManager.modelManager().getUserProfile(parameters, (Constants.Status iStatus, GenricResponse<Connection> genricResponse) -> {
            showProgress(false);
            Connection connection = genricResponse.getObject();
            if (connection.getProfileVisibility()!=0) {
                if (switcherListener != null)
                    switcherListener.switchFragment(UserProfileFragment.newInstance(switcherListener, connection), true, true);
            } else
                Utils.showAlertDialog(getContext(), "Message", "User Profile permission is denied");

        }, (Constants.Status iStatus, String message) -> {
            Log.e("User Profile", message);
            Toaster.toast(message);
            showProgress(false);
        });
    }




}
