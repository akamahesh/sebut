package com.app.inn.peepsin.UI.fragments.Feeds;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Constants.Constants.ChatON;
import com.app.inn.peepsin.DataBase.DataBaseHandler;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Managers.XMPPManager.RoosterHelper;
import com.app.inn.peepsin.Models.Address;
import com.app.inn.peepsin.Models.ChatProduct;
import com.app.inn.peepsin.Models.CurrentUser;
import com.app.inn.peepsin.Models.Feeds;
import com.app.inn.peepsin.Models.RatingModel;
import com.app.inn.peepsin.Models.ReviewModel;
import com.app.inn.peepsin.Models.Seller;
import com.app.inn.peepsin.Models.Shop;
import com.app.inn.peepsin.Models.ShoppingCart;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.activities.ChatActivity;
import com.app.inn.peepsin.UI.activities.ChatActivity.ChatType;
import com.app.inn.peepsin.UI.activities.HomeActivity;
import com.app.inn.peepsin.UI.adapters.ReviewAdapter;
import com.app.inn.peepsin.UI.dialogFragments.ZoomDialogFragment;
import com.app.inn.peepsin.UI.fragments.Profile.ShoppingCartFragment;
import com.app.inn.peepsin.UI.fragments.Shop.UserShopFragment;
import com.app.inn.peepsin.UI.helpers.DividerItemRecyclerDecoration;
import com.app.inn.peepsin.UI.helpers.MyBounceInterpolator;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import at.blogc.android.views.ExpandableTextView;
import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.CART_UPDATE;
import static com.app.inn.peepsin.Constants.Constants.REVIEW_RESULT;
import static com.app.inn.peepsin.Constants.Constants.kData;
import static com.app.inn.peepsin.Constants.Constants.kEmptyString;
import static com.app.inn.peepsin.Constants.Constants.kFeeds;
import static com.app.inn.peepsin.Constants.Constants.kImageUrl;
import static com.app.inn.peepsin.Constants.Constants.kLatitude;
import static com.app.inn.peepsin.Constants.Constants.kLongitude;
import static com.app.inn.peepsin.Constants.Constants.kOwnerName;
import static com.app.inn.peepsin.Constants.Constants.kProductId;
import static com.app.inn.peepsin.Constants.Constants.kProductImageUrlList;


/**
 * Created by akamahesh on 2/5/17.
 */

public class ProductDetailFragment extends Fragment {

    private String TAG = getTag();
    @BindDrawable(R.drawable.img_product_placeholder)
    Drawable placeholder;
    @BindDrawable(R.drawable.img_shop_logo)
    Drawable shopPlaceholder;

    @BindView(R.id.view_pager_images)
    ViewPager viewPager;
    @BindView(R.id.tab_carousel)
    TabLayout tabCarouselLayout;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_product_name)
    TextView tvProductName;
    @BindView(R.id.tv_description_details)
    TextView tvDescription;
    @BindView(R.id.tv_price)
    TextView tvPrice;
    @BindView(R.id.tv_skui)
    TextView tvSkui;
    @BindView(R.id.tv_stockAvilableCount)
    TextView tvStockAvilable;
    @BindView(R.id.tv_quantityCounts)
    TextView tvQuantityCounts;
    @BindView(R.id.iv_sold_stock)
    ImageView icSoldStock;
    @BindView(R.id.tv_selling_price)
    TextView tvSellingPrice;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.tv_offer)
    TextView tvOffer;
    @BindView(R.id.tv_name)
    TextView tvShopName;
    @BindView(R.id.iv_favorite)
    ImageView ivFav;
    @BindView(R.id.iv_previous)
    ImageView ivPrevious;
    @BindView(R.id.iv_next)
    ImageView ivNext;
    @BindView(R.id.iv_shop_image)
    ImageView ivShopImage;
    @BindView(R.id.tv_product_rating)
    TextView productRating;
    @BindView(R.id.tv_rating)
    TextView tvRating;
    @BindView(R.id.tv_ratings_points)
    TextView tvTotalRating;
    @BindView(R.id.progress_bar1)
    ProgressBar progressBar1;
    @BindView(R.id.progress_bar2)
    ProgressBar progressBar2;
    @BindView(R.id.progress_bar3)
    ProgressBar progressBar3;
    @BindView(R.id.progress_bar4)
    ProgressBar progressBar4;
    @BindView(R.id.progress_bar5)
    ProgressBar progressBar5;
    @BindView(R.id.recycler_view_review)
    RecyclerView reviewRecycler;
    @BindView(R.id.btn_show_all_review)
    Button btnShowReviews;
    @BindView(R.id.tv_cartCount)
    TextView tvCartCount;
    @BindView(R.id.btn_discription)
    Button btnDescription;
    @BindView(R.id.btn_specification)
    Button btnSpecification;
    @BindView(R.id.fab)
    FloatingActionButton btnfab;
    int count = 1;
    private int myRating = 0;
    private String myReview = "";
    private List<ReviewModel> reviewList;
    DataBaseHandler dbHandler;
    CurrentUser currentUser;
    MapDialogFragment mapDialogFragment = null;
    CustomBottomSheetDialogFragment bottomSheetDialogFragment = null;

    private CurrentUser user;
    private Feeds product;
    private int productId = 0;
    private ProgressDialog progressDialog;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    static Switcher switcherListener;
    //AppBarLayout appbar;
    String shopName = "";
    String shopImageURL = "";
    @BindView(R.id.expandableTextView)
    ExpandableTextView expandableTextView;
    @BindView(R.id.button_toggle)
    Button buttonToggle;
    int isselected = 1;
    @BindView(R.id.btn_decrease)
    Button btnDecrease;
    @BindView(R.id.btn_increase)
    Button btnIncrease;

    public static Fragment newInstance(Integer productId, Feeds feed, Switcher switcher) {
        switcherListener = switcher;
        Fragment fragment = new ProductDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(kProductId, productId);
        bundle.putSerializable(kFeeds, feed);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = Utils.generateProgressDialog(getContext(), false);
        user = ModelManager.modelManager().getCurrentUser();
        Bundle bundle = getArguments();
        if (bundle != null) {
            productId = bundle.getInt(kProductId);
            product = (Feeds) bundle.getSerializable(kFeeds);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bottom_layout, container, false);
        ButterKnife.bind(this, view);
        dbHandler = new DataBaseHandler(getContext());
        currentUser = ModelManager.modelManager().getCurrentUser();
        tvTitle.setText(product.getName());
        setUpUI(product);
        setupViewPager(product);
        getProductRating(productId);
        getRatingReviewList(productId);
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        CurrentUser user = ModelManager.modelManager().getCurrentUser();
        setCartUpdate(user.getShoppingCartCount());
        LocalBroadcastManager.getInstance(getContext())
                .registerReceiver(mBroadcastReceiver, new IntentFilter(CART_UPDATE));
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getContext())
                .unregisterReceiver(mBroadcastReceiver);
    }

    BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(CART_UPDATE)) {
                setCartUpdate(intent.getIntExtra(kData, 0));
            }
        }
    };

    public void setCartUpdate(int count) {
        if (count == 0) {
            tvCartCount.setVisibility(View.GONE);
        } else {
            tvCartCount.setVisibility(View.VISIBLE);
            tvCartCount.setText(String.valueOf(count));
        }
    }

    @OnClick(R.id.item_view)
    void onUserProfile() {
        Shop shop = product.getShop();
        if (shop != null) {
            if (switcherListener != null) {
                switcherListener
                        .switchFragment(
                                UserShopFragment.newInstance(switcherListener, shop.getShopId(), 0, ""),
                                true, true);
            } else {
                Utils.showAlertDialog(getContext(), "Error!", "Opps! \n No Shop Details Found :( ");
            }
        }
    }

    @OnClick(R.id.btn_rate_review)
    void onRateReview() {
        if (switcherListener != null)
            switcherListener.switchFragment(AddNewRateReviewFragment.newInstance(switcherListener, productId, myRating, myReview, shopName, shopImageURL), true, true);


    }

    @OnClick(R.id.btn_show_all_review)
    void onReviewList() {
        if (switcherListener != null) {
            switcherListener
                    .switchFragment(ReviewFragment.newInstance(productId, reviewList), true, true);
        }
    }

    @OnClick(R.id.fab)
    void onAddCart() {
        showProgress(true);
        ModelManager.modelManager().addShoppingCart(productId, count, (Constants.Status iStatus) -> {
            count = 1;
            tvQuantityCounts.setText(Integer.toString(1));
            updateCart();
            Toaster.toastRangeen("Product is added to shopping cart");
        }, (Constants.Status iStatus, String message) -> {
            Toaster.toastRangeen("Out of Stock");
            showProgress(false);
        });
    }

    @OnClick(R.id.btn_cart)
    void onCart() {
        if (switcherListener != null) {
            ((HomeActivity) getActivity()).getTabLayout().setVisibility(View.GONE);
            switcherListener.switchFragment(ShoppingCartFragment.newInstance(switcherListener), true, true);
        }
    }

    private void updateCart() {
        ModelManager.modelManager().getShoppingCart(
                (Constants.Status iStatus, GenricResponse<ShoppingCart> genericResponse) -> {
                    showProgress(false);
                    setCartUpdate(user.getShoppingCartCount());
                }, (Constants.Status iStatus, String message) -> {
                    Toaster.toast(message);
                    showProgress(false);
                });
    }

    public void setUpUI(Feeds product) {
        String name = product.getName();
        String price = getString(R.string.Rs) + " " + product.getPrice();
        String rating = String.valueOf(product.getRating());
        String offer = getString(R.string.no_offer);
        if (!product.getDiscountTagline().isEmpty()) {
            offer = product.getDiscountTagline();
        }
        String description = product.getDescription();
        String skui = "SKU No. " + product.getSkuNumber();
        if (product.getSellingPrice().equals(product.getPrice())) {
            tvPrice.setVisibility(View.GONE);
        }
        String sellingPrice = getString(R.string.Rs) + " " + product.getSellingPrice();
        Address address = product.getAddress();
        String addressTxt = "No Location Available";
        if (address != null) {
            String street = "";
            if (!product.getAddress().getStreetAddress().isEmpty()) {
                street = product.getAddress().getStreetAddress() + ", ";
            }
            String city = product.getAddress().getCity();
            String state = product.getAddress().getState();
            String country = product.getAddress().getCountry();
            addressTxt = street + city + ", " + state + ", " + country;
        }


        int connectionLevel = 0;
        Shop shop = product.getShop();
        if (shop != null) {
            shopImageURL = shop.getShopBannerImageUrl();
            shopName = shop.getShopName();
            connectionLevel = shop.getConnectionLevel();
        }

        if (product.getFavourite()) {
            ivFav.setImageResource(R.drawable.ic_favorite);
        } else {
            ivFav.setImageResource(R.drawable.ic_heart_empty);
        }

        if (!shopImageURL.isEmpty()) {
            Picasso.with(getContext())
                    .load(shopImageURL)
                    .resize(100, 100)
                    .placeholder(shopPlaceholder)
                    .error(shopPlaceholder)
                    .into(ivShopImage);
        } else {
            ivShopImage.setImageDrawable(shopPlaceholder);
        }


        if (product.getQuantity() == 0) {
            tvQuantityCounts.setText(String.valueOf(product.getQuantity()));
            tvStockAvilable.setText("Sold out");
            icSoldStock.setVisibility(View.VISIBLE);
            tvStockAvilable.setTextColor(Color.parseColor("#F8636A"));
            btnDecrease.setTextColor(getResources().getColor(R.color.text_color_normal));
            btnIncrease.setTextColor(getResources().getColor(R.color.text_color_normal));
            btnfab.setVisibility(View.GONE);




        } else {
            icSoldStock.setVisibility(View.GONE);
            tvStockAvilable.setText(product.getQuantity() + " In stock online");
            tvQuantityCounts.setText(String.valueOf(1));
            btnIncrease.setTextColor(getResources().getColor(R.color.link_color));
            btnDecrease.setTextColor(getResources().getColor(R.color.link_color));


        }

        tvSkui.setText(skui);
        tvProductName.setText(name);
        getExpandableView(1);
        // tvDescription.setText(Html.fromHtml(description));
        tvPrice.setText(price);
        tvSellingPrice.setText(sellingPrice);
        tvPrice.setPaintFlags(tvPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        tvShopName.setText(Utils.getUserName(shopName, connectionLevel));
        tvAddress.setText(addressTxt);
        tvOffer.setText(offer);
        productRating.setText(rating);
    }


    @OnClick(R.id.btn_increase)
    public void onIncrese() {
        if (count < product.getQuantity()) {
            count++;
            tvQuantityCounts.setText(Integer.toString(count));
        }
    }


    @OnClick(R.id.btn_decrease)
    public void ondecrease() {
        if (count > 1) {
            count--;
            tvQuantityCounts.setText(Integer.toString(count));
        }

    }


    @OnClick(R.id.btn_discription)
    public void description() {
        isselected = 1;
        btnSpecification.setBackground(getResources().getDrawable(android.R.color.transparent));
        btnSpecification.setTextColor(Color.parseColor("#F8636A"));
        btnDescription
                .setBackgroundResource(R.drawable.canvas_left_rounded_corner_light_red_background);
        btnDescription.setTextColor(Color.parseColor("#FFFFFF"));
        getExpandableView(isselected);
    /*if (!product.getDescription().isEmpty()) {
      tvDescription.setText(Html.fromHtml(product.getDescription()));
    } else {
      tvDescription.setText(R.string.no_description_avilable);
    }*/
    }

    @OnClick(R.id.btn_specification)
    public void specification() {
        isselected = 2;
        btnDescription.setBackground(getResources().getDrawable(android.R.color.transparent));
        btnDescription.setTextColor(Color.parseColor("#F8636A"));
        btnSpecification
                .setBackgroundResource(R.drawable.canvas_right_rounded_corner_light_red_background);
        btnSpecification.setTextColor(Color.parseColor("#FFFFFF"));
        getExpandableView(isselected);
    /*if (!product.getSpecification().isEmpty()) {
      tvDescription.setText(Html.fromHtml(product.getSpecification()));
    } else {
      tvDescription.setText(R.string.no_specification_avilable);
    }*/
    }

    public void ratingViewUpdate(RatingModel rating) {
        if (rating.getRating().isEmpty()) {
            tvRating.setText("0.0");
            productRating.setText("0.0");
        } else {
            tvRating.setText(rating.getRating());
            productRating.setText(rating.getRating());
        }

        Activity activity = getActivity();
        if (activity != null) {
            String totalRating = rating.getTotalRating() + " " + getString(R.string.ratings);
            tvTotalRating.setText(totalRating);
        }
        progressBar1.setProgress(rating.getFifthProgress());
        progressBar2.setProgress(rating.getForthProgress());
        progressBar3.setProgress(rating.getThirdProgress());
        progressBar4.setProgress(rating.getSecondProgress());
        progressBar5.setProgress(rating.getFirstProgress());
        myRating = rating.getMyRating();
        myReview = rating.getMyReview();
    }

    public void reviewListUpdate(List<ReviewModel> reviewList) {
        try {
            ReviewAdapter reviewAdapter = new ReviewAdapter(getContext(), reviewList, reviewList.size());
            reviewRecycler.setHasFixedSize(true);
            reviewRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
            reviewRecycler.setAdapter(reviewAdapter);
            RecyclerView.ItemDecoration itemDecoration = new DividerItemRecyclerDecoration(getContext(),
                    R.drawable.canvas_recycler_divider);
            reviewRecycler.addItemDecoration(itemDecoration);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }


    private void getProductRating(int productId) {
        ModelManager.modelManager().getProductRating(productId,
                (Constants.Status iStatus, GenricResponse<RatingModel> genericResponse) -> {
                    RatingModel rating = genericResponse.getObject();
                    ratingViewUpdate(rating);
                }, (Constants.Status iStatus, String message) -> {
                    Log.e(TAG, message);
                    Toaster.toast(message);
                });
    }

    private void getRatingReviewList(int productId) {
        ModelManager.modelManager().getRatingReviewList(1, productId,
                (Constants.Status iStatus, GenricResponse<List<ReviewModel>> genericResponse) -> {
                    reviewList = genericResponse.getObject();
                    if (reviewList.size() >= 3) {
                        List<ReviewModel> list = new ArrayList<>();
                        for (int i = 0; i < 3; i++) {
                            list.add(reviewList.get(i));
                        }
                        reviewListUpdate(list);
                        btnShowReviews.setVisibility(View.VISIBLE);
                    } else {
                        reviewListUpdate(reviewList);
                        btnShowReviews.setVisibility(View.GONE);
                    }
                    showProgress(false);
                }, (Constants.Status iStatus, String message) -> {
                    showProgress(false);
                    Toaster.toast(message);
                });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REVIEW_RESULT && resultCode == Activity.RESULT_OK) {
            getProductRating(productId);
            getRatingReviewList(productId);
        }
    }


    @OnClick(R.id.iv_previous)
    void onPrevious() {
        int current = viewPager.getCurrentItem();
        if (current > 0) {
            viewPager.setCurrentItem(--current);
        }
    }

    @OnClick(R.id.iv_next)
    void onNext() {
        int current = viewPager.getCurrentItem();
        if (current < mSectionsPagerAdapter.getCount()) {
            viewPager.setCurrentItem(++current);
        }
    }


    @OnClick(R.id.btn_make_offer)
    void makeOffer() {
        if (product.getSeller() != null) {
            ChatProduct chatUser = makeChatUser(product);
            List<ChatProduct> chatProductList = RoosterHelper.getChatThreads();
            for (ChatProduct chatur : chatProductList) {
                String chaturUNIQUEID = chatur.getUniqueId();
                String chatterUNIQUEID = chatUser.getUniqueId();
                if (chaturUNIQUEID.equals(chatterUNIQUEID)) {
                    chatUser = chatur;
                }
            }
            startActivity(ChatActivity.getIntent(getContext(), chatUser, ChatType.MAKE_OFFER));
        } else {
            Toaster.customToast("No Seller available right now!");
        }
    }

    @OnClick(R.id.iv_favorite)
    void makeFavourite(ImageView imageView) {
        Boolean isFavorite = product.getFavourite();
        Animation favAnim = AnimationUtils.loadAnimation(getContext(), R.anim.bounce);
        MyBounceInterpolator favInterpolar = new MyBounceInterpolator(.2, 20);
        favAnim.setInterpolator(favInterpolar);
        imageView.startAnimation(favAnim);
        if (isFavorite) {
            makeFavorite(0, product.getId());
        } else {
            makeFavorite(1, product.getId());
        }
    }

    private void makeFavorite(int isFav, Integer productId) {
        ModelManager.modelManager()
                .setFavouriteProduct(productId, isFav, (Constants.Status iStatus) -> {
                    if (isFav == 1) {
                        ivFav.setImageResource(R.drawable.ic_favorite);
                        product.setFavourite(true);
                    } else {
                        ivFav.setImageResource(R.drawable.ic_heart_empty);
                        product.setFavourite(false);
                    }
                }, (Constants.Status iStatus, String message) -> Toaster.toast(message));
    }

    @OnClick(R.id.iv_chat)
    void getChat() {
        if (product.getSeller() != null) {
            ChatProduct chatUser = makeChatUser(product);
            boolean hasChat = false;
            List<ChatProduct> chatProductList = RoosterHelper.getChatThreads();
            for (ChatProduct chatur : chatProductList) {
                String chaturUNIQUEID = chatur.getUniqueId();
                String chatterUNIQUEID = chatUser.getUniqueId();
                if (chaturUNIQUEID.equals(chatterUNIQUEID)) {
                    hasChat = true;
                    chatUser = chatur;
                    break;
                }
            }
            if (hasChat) {
                startActivity(ChatActivity.getIntent(getContext(), chatUser, ChatType.PRODUCT_CHAT));
            } else {
                Utils.showAlertDialog(getContext(), "Sorry!",
                        "No Offer Made!\nPlease Make an offer first in order to start chat!");
            }
        } else {
            Toaster.customToast("No Seller available right now!");
        }

    }

    private ChatProduct makeChatUser(Feeds product) {
        ChatProduct chatUser = new ChatProduct();
        Seller seller = product.getSeller();
        if (seller != null) {
            chatUser.setUserId(seller.getId().toString());
            chatUser.setUserName(seller.getFullName());
            chatUser.setUserImageURL(seller.getProfilePicUrl());
            chatUser.setJabberId(seller.getJabberId());
            chatUser.setProductId(product.getId().toString());
            chatUser.setPrice(product.getSellingPrice());
            chatUser.setChatOn(String.valueOf(ChatON.product.getValue()));
            chatUser.setProductName(product.getName());
            chatUser.setProductThumbnailURL(product.getImageThumbUrl());
        }
        return chatUser;
    }

    @OnClick(R.id.iv_share)
    void onShare() {
        if (bottomSheetDialogFragment == null) {
            if (dbHandler.getConnectionsFromDatabase(String.valueOf(currentUser.getUserId()), "0").isEmpty()) {
                Toaster.customToast("You don't have any connections");
            } else {
                bottomSheetDialogFragment = CustomBottomSheetDialogFragment
                        .newInstance(2, product.getId()); //1 for map and 2 for share
                bottomSheetDialogFragment
                        .show(getChildFragmentManager(), bottomSheetDialogFragment.getTag());
            }
        } else if (!bottomSheetDialogFragment.isVisible()) {
            bottomSheetDialogFragment = null;
            onShare();
        }
    }


    @OnClick(R.id.iv_location)
    void getLocation() {

        if (mapDialogFragment == null) {
            try {
                String lat = user.getPrimaryAddress().getLatitude();
                String sLat = product.getAddress().getLatitude();
                String sLon = product.getAddress().getLongitude();
                if (!lat.isEmpty() && !sLat.isEmpty()) {
                    mapDialogFragment = MapDialogFragment.newInstance();
                    Bundle bdl = new Bundle();
                    bdl.putString(kLatitude, sLat);
                    bdl.putString(kLongitude, sLon);
                    bdl.putString(kOwnerName, product.getSeller().getFirstName());
                    bdl.putString(kImageUrl, product.getSeller().getProfilePicUrl());
                    mapDialogFragment.setArguments(bdl);
                    mapDialogFragment.setCancelable(true);
                    mapDialogFragment
                            .show(getChildFragmentManager(), mapDialogFragment.getClass().getSimpleName());

                } else {
                    Toaster.toast("Sorry Lat Long UnAvailable");
                }
            } catch (Exception e) {
                Toaster.toast("Sorry Lat Long UnAvailable");
                e.printStackTrace();
            }
        } else if (!mapDialogFragment.isVisible()) {
            mapDialogFragment = null;
            getLocation();
        }
    }

    @OnClick(R.id.ib_back)
    void onBack() {
        getFragmentManager().popBackStack();
        Utils.isfeedsClicked = 0;
    }

    public void setupViewPager(Feeds feed) {
        //@otherImageThumnailURL for carousel images

        ArrayList<String> otherImageUrl = new ArrayList<>();
        otherImageUrl.add(feed.getImageUrl());
        otherImageUrl.addAll(feed.getImageUrlList());
        mSectionsPagerAdapter = new SectionsPagerAdapter(getChildFragmentManager());
        mSectionsPagerAdapter.addItems(otherImageUrl);
        viewPager.setAdapter(mSectionsPagerAdapter);
        viewPager.setOffscreenPageLimit(3);

        //@otherImageThumnailURL for tab images
        ArrayList<String> otherImageThumnailURL = new ArrayList<>();
        otherImageThumnailURL.add(feed.getImageThumbUrl());
        otherImageThumnailURL.addAll(feed.getImageThumbUrlList());
        tabCarouselLayout.setupWithViewPager(viewPager);
        setupTabIcons(otherImageThumnailURL);

        if (otherImageThumnailURL.size() == 1) {
            ivPrevious.setVisibility(View.INVISIBLE);
            ivNext.setVisibility(View.INVISIBLE);
        } else {
            ivPrevious.setVisibility(View.VISIBLE);
            ivNext.setVisibility(View.VISIBLE);
        }
    }

    private void setupTabIcons(List<String> imageUrlList) {
        int i = 0;
        for (String imageUrl : imageUrlList) {
            tabCarouselLayout.getTabAt(i).setCustomView(R.layout.tab_carousal_layout);
            ImageView imageView = (ImageView) tabCarouselLayout.getTabAt(i++).getCustomView()
                    .findViewById(R.id.iv_tab_image);
            if (imageUrl.isEmpty())
                imageView.setImageDrawable(placeholder);
            else
                Picasso.with(getContext())
                        .load(imageUrl)
                        .error(placeholder)
                        .resize(30, 30)
                        .memoryPolicy(MemoryPolicy.NO_STORE)
                        .placeholder(placeholder)
                        .into(imageView);
        }
    }


    public static class PlaceholderFragment extends Fragment {

        String imageURL = kEmptyString;
        ArrayList<String> imageURLList;
        @BindView(R.id.iv_product_image)
        ImageView ivProductImage;

        public PlaceholderFragment() {
        }

        public static PlaceholderFragment newInstance(String imageurl, ArrayList<String> imageUrls) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putString(kImageUrl, imageurl);
            args.putStringArrayList(kProductImageUrlList, imageUrls);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            Bundle bundle = getArguments();
            if (bundle != null) {
                imageURL = bundle.getString(kImageUrl);
                imageURLList = bundle.getStringArrayList(kProductImageUrlList);
            }
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            ButterKnife.bind(this, rootView);

            if (!imageURL.isEmpty()) {
                Picasso.with(getContext())
                        .load(imageURL)
                        .resize(250, 250)
                        .memoryPolicy(MemoryPolicy.NO_STORE)
                        .error(getResources().getDrawable(R.drawable.img_product_placeholder))
                        .placeholder(getResources().getDrawable(R.drawable.img_product_placeholder))
                        .into(ivProductImage);
            }

            return rootView;
        }

        @OnClick(R.id.container)
        void zoomImage() {
            if (!imageURLList.get(0).isEmpty()) {
                DialogFragment dialogFragment = ZoomDialogFragment.newInstance(imageURLList);
                showDialog(dialogFragment, false);
            }
        }

        void showDialog(DialogFragment dialogFragment, boolean saveInBackstack) {
            String backStateName = dialogFragment.getClass().getName();
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction ft = fragmentManager.beginTransaction();
            if (saveInBackstack) {
                ft.addToBackStack(backStateName);
            }
            dialogFragment.show(fragmentManager, backStateName);
        }
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        ArrayList<String> otherImageUrlList = new ArrayList<>();

        SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return PlaceholderFragment.newInstance(otherImageUrlList.get(position), otherImageUrlList);
        }

        public void addItems(ArrayList<String> imageUrl) {
            otherImageUrlList.addAll(imageUrl);
        }

        @Override
        public int getCount() {
            return otherImageUrlList.size();
        }
    }

    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null)
                progressDialog.cancel();
        }
    }

    private void getExpandableView(int isselected) {
        int lineCount = 0;
        // set animation duration via code, but preferable in your layout files by using the animation_duration attribute
        expandableTextView.setAnimationDuration(750L);
        //Typeface type = Typeface.createFromAsset(getAssets(),"fonts/SanFranciscoDisplay-Regular.otf");

        //expandableTextView.setTypeface(type);
        if (isselected == 1) {
            if (!product.getDescription().isEmpty()) {
                expandableTextView.setText(Html.fromHtml(product.getDescription().replace("<br>", "<br />")));
            } else {
                expandableTextView.setText(R.string.no_description_avilable);
            }
        } else if (isselected == 2) {
            if (!product.getSpecification().isEmpty()) {
                expandableTextView.setText(Html.fromHtml(product.getSpecification().replace("<br>", "<br />")));
            } else {
                expandableTextView.setText(R.string.no_specification_avilable);
            }
        }
        Typeface font = Typeface.createFromAsset(getContext().getAssets(), "fonts/SanFranciscoText-Regular.otf");
        expandableTextView.setTypeface(font);
        // set interpolators for both expanding and collapsing animations
        expandableTextView.setInterpolator(new OvershootInterpolator());

        // or set them separately
        expandableTextView.setExpandInterpolator(new OvershootInterpolator());
        expandableTextView.setCollapseInterpolator(new OvershootInterpolator());

        expandableTextView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                try {

                    final int lineCount = expandableTextView.getLineCount();
                    // Log.e("linecount",lineCount+"");
                    if (lineCount > 4) {
                        buttonToggle.setVisibility(View.VISIBLE);
                    } else {
                        buttonToggle.setVisibility(View.GONE);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                return true;
            }
        });

        // toggle the ExpandableTextView
        buttonToggle.setOnClickListener(new View.OnClickListener() {
            @SuppressWarnings("ConstantConditions")
            @Override
            public void onClick(final View v) {
                expandableTextView.toggle();
                buttonToggle.setText(expandableTextView.isExpanded() ? R.string.collapse : R.string.expand);
            }
        });

        // but, you can also do the checks yourself
        buttonToggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                if (expandableTextView.isExpanded()) {
                    expandableTextView.collapse();
                    buttonToggle.setText(R.string.expand);
                } else {
                    expandableTextView.expand();
                    buttonToggle.setText(R.string.collapse);
                }
            }
        });

        // listen for expand / collapse events
        expandableTextView.setOnExpandListener(new at.blogc.android.views.ExpandableTextView.OnExpandListener() {
            @Override
            public void onExpand(final at.blogc.android.views.ExpandableTextView view) {
                // Log.d(TAG, "ExpandableTextView expanded");
            }

            @Override
            public void onCollapse(final at.blogc.android.views.ExpandableTextView view) {
                //Log.d(TAG, "ExpandableTextView collapsed");
            }
        });

    }

}
