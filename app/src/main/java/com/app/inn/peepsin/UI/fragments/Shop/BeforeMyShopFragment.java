package com.app.inn.peepsin.UI.fragments.Shop;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.BaseManager.DateManager;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.MyShop;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.Services.PublishAdService;
import com.app.inn.peepsin.UI.activities.AddProductActivity;
import com.app.inn.peepsin.UI.activities.ContactUsActivity;
import com.app.inn.peepsin.UI.fragments.Profile.ReceivedOrderFragment;
import com.app.inn.peepsin.UI.fragments.SellStuffModule.SellingStuffFragment;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static com.app.inn.peepsin.Constants.Constants.PRODUCT_RESULT;
import static com.app.inn.peepsin.Constants.Constants.kData;
import static com.app.inn.peepsin.Constants.Constants.kMessage;
import static com.app.inn.peepsin.Constants.Constants.kProductImageUrlList;
import static com.app.inn.peepsin.Constants.Constants.kRecords;
import static com.app.inn.peepsin.Constants.Constants.kShopId;
import static com.app.inn.peepsin.Constants.Constants.kTimeStamp;

/**
 * Created by Harsh on 8/1/2017.
 */

public class BeforeMyShopFragment extends Fragment {

    @BindView(R.id.tv_title)
    TextView textViewTitle;
    @BindView(R.id.tv_message)
    TextView tvMessage;
    @BindView(R.id.tv_time) TextView tvTime;

    @BindView(R.id.iv_shop_image)
    ImageView ivShopImage;
    @BindView(R.id.iv_logo_image)
    ImageView ivShopLogo;
    @BindView(R.id.tv_shop_name)
    TextView tvShopName;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.rating_score_bar)
    RatingBar ratingScoreBar;
    @BindView(R.id.tv_allReviewCount)
    TextView tvReviewCount;
    @BindView(R.id.tv_recievedOrderCount)
    TextView tvOrderCount;
    @BindView(R.id.tv_productCount)
    TextView tvProductCount;
    @BindView(R.id.tv_inventory_count)
    TextView tvInventoryCount;
    @BindView(R.id.tv_product_count)
    TextView tvProductCt;
    @BindView(R.id.tv_shop_status)
    TextView tvshopStatus;
    @BindView(R.id.shop_process_view)
    View shopProcessView;
    @BindView(R.id.shop_approval_view)
    View shopApprovalView;
    @BindView(R.id.tv_status_tag)
    TextView tvStatusTag;

    private final int REQUEST_PERMISSION_CAMERA_STORAGE = 101;
    private static Switcher switcherListener;
    private Integer shopId;
    private Integer userId;
    private MyShop myShop;
    private ProgressDialog progressDialog;
    // AppBarLayout appbarLayout;

    public static Fragment newInstance(Switcher switcher, int shopId) {
        Fragment fragment = new BeforeMyShopFragment();
        Bundle bdl = new Bundle();
        bdl.putInt(kShopId, shopId);
        fragment.setArguments(bdl);
        switcherListener = switcher;
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        progressDialog = Utils.generateProgressDialog(getContext(), false);
        userId = ModelManager.modelManager().getCurrentUser().getUserId();
        if (bundle != null) {
            shopId = bundle.getInt(kShopId);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_before_my_shop, container, false);
        ButterKnife.bind(this, view);
        /*appbarLayout=((HomeActivity) getActivity()).getTabLayout();
        Utils.isprofileClicked=1;*/
        textViewTitle.setText(getString(R.string.my_shop_title));

        //Rating star color
        LayerDrawable stars = (LayerDrawable) ratingScoreBar.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(Color.parseColor("#FAD368"), PorterDuff.Mode.SRC_ATOP);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        loadShopData(shopId);
        loadMessages(shopId, 1);
    }

    @OnClick(R.id.view_contact_admin)
    void onContactUs() {
        // Toaster.kalaToast("Under Construction");
        startActivityForResult(ContactUsActivity.getIntent(getContext(), shopId), 101);
    }


    private void loadShopData(Integer shopId) {
        showProgress(true);
        ModelManager.modelManager().getMyShopDetails(shopId, (Constants.Status iStatus, GenricResponse<MyShop> genricResponse) -> {
            showProgress(false);
            myShop = genricResponse.getObject();


            if (myShop.getShopStatus() == 1 || myShop.getShopStatus() == 2 || myShop.getShopStatus() == 3
                    || myShop.getShopStatus() == 4 || myShop.getShopStatus() == 5) {
                shopProcessView.setVisibility(View.VISIBLE);
                shopApprovalView.setVisibility(View.GONE);
                if (myShop.getShopStatus() == 5)
                    tvshopStatus.setTextColor(getResources().getColor(android.R.color.holo_green_light));
                else
                    tvshopStatus.setTextColor(getResources().getColor(android.R.color.holo_red_light));
                updateData(myShop);
            } else {
                shopProcessView.setVisibility(View.GONE);
                shopApprovalView.setVisibility(View.VISIBLE);
                updateData(myShop);
            }
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Toaster.toast(message);
        });
    }


    private void loadMessages(Integer mShopId, int page) {
        ModelManager.modelManager().receiveMessageHistory(mShopId, page, (Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<HashMap<String, Object>>> genericResponse) -> {
            CopyOnWriteArrayList<HashMap<String, Object>> hashMaps = genericResponse.getObject();
            Log.v("tAG", "hashMaps: " + hashMaps.size());
            if (hashMaps.isEmpty())
                return;
            try {
                HashMap<String, Object> hashMap = hashMaps.get(hashMaps.size() - 1);
                String timestamp = (String) hashMap.get(kTimeStamp);
                String message = (String) hashMap.get(kMessage);
                timestamp = DateManager.dispalyValue(timestamp);
                tvTime.setText(timestamp);
                tvMessage.setText(message);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }, (Constants.Status iStatus, String message) -> {
            Toaster.toast(message);
        });
    }


    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) progressDialog.cancel();
        }
    }


    private void updateData(MyShop shop) {
        try {
            int shopStatus = myShop.getShopStatus();
            tvShopName.setText(shop.getName());
            ratingScoreBar.setRating(Float.valueOf(shop.getRating()));
            tvProductCt.setText(String.valueOf(shop.getProductCount()));
            if (shop.getProductCount() > 0) {
                tvProductCount.setVisibility(View.VISIBLE);
                tvProductCount.setText(String.valueOf(shop.getProductCount()));
            } else
                tvProductCount.setVisibility(View.INVISIBLE);
            if (shop.getReviewCount() > 0) {
                tvReviewCount.setVisibility(View.VISIBLE);
                tvReviewCount.setText(String.valueOf(shop.getReviewCount()));
            } else
                tvReviewCount.setVisibility(View.INVISIBLE);
            if (shop.getReceivedOrderCount() > 0) {
                tvOrderCount.setVisibility(View.VISIBLE);
                tvOrderCount.setText(String.valueOf(shop.getReceivedOrderCount()));
            } else
                tvOrderCount.setVisibility(View.INVISIBLE);
            if (shop.getProductCount() > 0) {
                tvInventoryCount.setVisibility(View.VISIBLE);
                tvInventoryCount.setText(String.valueOf(shop.getProductCount()));
            } else
                tvInventoryCount.setVisibility(View.INVISIBLE);

            tvshopStatus.setText(Utils.getShopStatus(shopStatus));
            if (shopStatus > 5) {
                tvStatusTag.setVisibility(View.VISIBLE);
                if (shopStatus == 6) {
                    tvStatusTag.setBackground(getResources().getDrawable(R.drawable.img_green_flag));
                    tvStatusTag.setText("Active");
                    if(myShop.getProductCount()==0) {
                        showAddressDialog();
                    }

                } else {
                    tvStatusTag.setBackground(getResources().getDrawable(R.drawable.img_red_flag));
                    tvStatusTag.setText("InActive");
                }
            } else
                tvStatusTag.setVisibility(View.GONE);

            String location = "";
            if (shop.getAddress() != null)
                location = shop.getAddress().getStreetAddress() + " , " +
                        shop.getAddress().getCity() + " , " +
                        shop.getAddress().getState() + " , " +
                        shop.getAddress().getCountry();
            tvAddress.setText(location);

            String bannerUrl = shop.getBannerUrl();
            if (!bannerUrl.isEmpty()) {
                Picasso.with(getContext())
                        .load(bannerUrl)
                        .placeholder(R.drawable.img_banner_placeholder)
                        .into(ivShopImage);
            } else
                ivShopImage.setImageResource(R.drawable.img_banner_placeholder);

            String logoUrl = shop.getLogoUrl();
            if (!logoUrl.isEmpty()) {
                Picasso.with(getContext())
                        .load(logoUrl)
                        .placeholder(R.drawable.img_shop_logo)
                        .into(ivShopLogo);
            } else
                ivShopLogo.setImageResource(R.drawable.img_shop_logo);
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        // appbarLayout.setVisibility(View.GONE);
    }

    @OnClick(R.id.btn_add_product)
    void addSellingStuff() {
        checkPermission();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PRODUCT_RESULT && resultCode == Activity.RESULT_OK) {
            HashMap<String, Object> productMap = (HashMap<String, Object>) data.getSerializableExtra(kData);
            List<String> imagePathList = (List<String>) data.getSerializableExtra(kProductImageUrlList);
            publishAd(productMap, imagePathList);
        }
        loadMessages(shopId, 1);
    }

    @OnClick(R.id.i_icon)
    public void openMyShop() {
        if (switcherListener != null)
            switcherListener.switchFragment(MyShopFragment.newInstance(switcherListener, shopId), true, true);
    }

    @OnClick(R.id.view_all_review)
    public void onReview() {
        if (switcherListener != null)
                switcherListener.switchFragment(ShopRatingReviewFragment.newInstance(switcherListener, shopId), true, true);
    }

    @OnClick(R.id.view_inventory)
    public void manageInventory() {
        if (switcherListener != null)
            switcherListener.switchFragment(ManageInventoryFragment.newInstance(switcherListener, shopId), true, true);
    }

    @OnClick(R.id.view_recieved_order)
    public void onOrder() {
        if (switcherListener != null)
            switcherListener.switchFragment(ReceivedOrderFragment.newInstance(switcherListener, shopId), true, true);
    }


    @OnClick(R.id.view_selling_product)
    void onSellingProduct() {
        if (switcherListener != null)
            switcherListener.switchFragment(SellingStuffFragment.newInstance(switcherListener, shopId, userId), true, true);
    }

    @OnClick(R.id.view_product)
    void onProduct() {
        if (switcherListener != null)
            switcherListener.switchFragment(SellingStuffFragment.newInstance(switcherListener, shopId, userId), true, true);
    }

    @OnClick(R.id.ib_back)
    public void onBackPress() {
        getFragmentManager().popBackStack();
        /*appbarLayout.setVisibility(View.VISIBLE);
        Utils.isprofileClicked=0;*/
    }

    private void publishAd(HashMap<String, Object> productMap, List<String> imagePathList) {
        Intent intent = new Intent(getContext(), PublishAdService.class);
        // add infos for the service which file to download and where to store
        intent.putExtra(kRecords, productMap);
        intent.putExtra(kProductImageUrlList, (Serializable) imagePathList);
        getActivity().startService(intent);
    }

    private void checkPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(), CAMERA) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(getActivity(), READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            addProduct();
        } else if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), CAMERA) && ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), READ_EXTERNAL_STORAGE)) {
            // We've been denied once before. Explain why we need the permission, then ask again.
            Utils.showDialog(getContext(), "Camera & Gallary permissions are required to upload profile images!", "Ask Permission", "Discard", (dialog, which) -> {
                if (which == -1)
                    requestPermissions(new String[]{CAMERA, READ_EXTERNAL_STORAGE}, REQUEST_PERMISSION_CAMERA_STORAGE);
                else
                    dialog.dismiss();
            });
        } else {
            // We've never asked. Just do it.
            requestPermissions(new String[]{CAMERA, READ_EXTERNAL_STORAGE}, REQUEST_PERMISSION_CAMERA_STORAGE);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_PERMISSION_CAMERA_STORAGE && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            addProduct();
        } else {
            // We were not granted permission this time, so don't try to show the contact picker
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    void addProduct() {
        startActivityForResult(AddProductActivity.getIntent(getContext()), PRODUCT_RESULT);
    }


    void showAddressDialog() {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_productnot_avilable);
        View view =ButterKnife.findById(dialog,R.id.item);
        view.setOnClickListener(v -> {
            dialog.dismiss();});
        TextView tvLive = ButterKnife.findById(dialog, R.id.tv_live);
        setProductLiveMessage(tvLive);
        Button btnContinue = ButterKnife.findById(dialog, R.id.btn_AddProduct);
        btnContinue.setOnClickListener(v -> {
            dialog.dismiss();
            checkPermission();
        });
        dialog.show();
    }


    //Different color string provide
    private void setProductLiveMessage(TextView tvLive) {
        SpannableStringBuilder builder = new SpannableStringBuilder();
        SpannableString strYouShop = new SpannableString("You Shop is");
        strYouShop.setSpan(new ForegroundColorSpan(Color.parseColor("#000000")), 0, strYouShop.length(), 0);
        builder.append(strYouShop);

        SpannableString strActive = new SpannableString(" Active");
        strActive.setSpan(new ForegroundColorSpan(Color.parseColor("#58DDA3")), 0, strActive.length(), 0);
        builder.append(strActive);

        SpannableString strBut = new SpannableString(" but");
        strBut.setSpan(new ForegroundColorSpan(Color.parseColor("#000000")), 0, strBut.length(), 0);
        builder.append(strBut);

        SpannableString strNotLive = new SpannableString(" Not Live. "+"\n");
        strNotLive.setSpan(new ForegroundColorSpan(Color.parseColor("#FF9048")), 0, strNotLive.length(), 0);
        builder.append(strNotLive);

        SpannableString strFor = new SpannableString(" For Live,Please add at least one "+"\n");
        strFor.setSpan(new ForegroundColorSpan(Color.parseColor("#000000")), 0, strFor.length(), 0);
        builder.append(strFor);

        SpannableString strProduct = new SpannableString(" Product. ");
        strProduct.setSpan(new ForegroundColorSpan(Color.parseColor("#000000")), 0, strProduct.length(), 0);
        builder.append(strProduct);


        tvLive.setText(builder, TextView.BufferType.SPANNABLE);
    }

}
