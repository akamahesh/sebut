package com.app.inn.peepsin.UI.fragments.SignIn;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.activities.LogInActivity;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.smscodereader.interfaces.OTPListener;
import com.app.inn.peepsin.smscodereader.receivers.OtpReader;
import com.chaos.view.PinView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kAuthToken;
import static com.app.inn.peepsin.Constants.Constants.kContactId;
import static com.app.inn.peepsin.Constants.Constants.kOTP;
import static com.app.inn.peepsin.Constants.Constants.kType;

/**
 * Created by akaMahesh on 2/5/17
 * contact : mckay1718@gmail.com
 */

public class SingInOTPFragment extends Fragment implements OTPListener {

    @BindView(R.id.tv_otp)
    TextView tvOTP;
    @BindView(R.id.btn_verify)
    Button btnVerify;
    @BindView(R.id.otp_view)
    PinView otpView;

    int TYPE = 1;
    String PIN;
    private static final int PHONE = 1;
    private static final int EMAIL = 2;
    private static final int PASSWORD = 3;
    private OnOTPFragmentInteractionListener mListener;
    private String mContactID;
    private String mAuthToken;
    private ProgressDialog progressDialog;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    public Dialog otpDialog;


    public static Fragment newInstance(int type, String pin,
                                       String contactId, String authtoken) { //1 for phone 2 for email
        Fragment fragment = new SingInOTPFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(kType, type);
        bundle.putString(kOTP, pin);
        bundle.putString(kContactId, contactId);
        bundle.putString(kAuthToken, authtoken);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnOTPFragmentInteractionListener) {
            mListener = (OnOTPFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnOTPFragmentInteractionListener");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            TYPE = bundle.getInt(kType);
            PIN = bundle.getString(kOTP);
            mContactID = bundle.getString(kContactId);
            mAuthToken = bundle.getString(kAuthToken);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_otp, container, false);
        ButterKnife.bind(this, view);
        progressDialog = Utils.generateProgressDialog(getContext(), false);
        //otpDialog = Utils.generateOTPProgressDialog(getActivity(),false);

        ((LogInActivity) getActivity()).setTitle("Sign In With OTP");
        if (TYPE == PHONE) {
            btnVerify.setText("Verify Phone");
        } else if (TYPE == EMAIL) {
            btnVerify.setText("Verify Email");
        } else if (TYPE == PASSWORD) {
            btnVerify.setText("Reset Password");

        }
        Toaster.toastRangeen(
                "OTP sent to " + mContactID + "\n" + " Please enter OTP to complete SignIn process");
        //Toaster.toastOTP("OTP: "+PIN);
        /*showOTPprogress(true);
        Handler handler = new Handler();
        handler.postDelayed(()->{
            showOTPprogress(false);
            Toaster.kalaToast("Please enter otp manually or tap resend.");
        },30000);*/
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        OtpReader.bind(this, getString(R.string.otp_sender_name));
        checkAndRequestPermissions();
    }

    private boolean checkAndRequestPermissions() {
        int permissionSendMessage = ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.SEND_SMS);

        int receiveSMS = ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.RECEIVE_SMS);

        int readSMS = ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.READ_SMS);
        List<String> listPermissionsNeeded = new ArrayList<>();

        if (receiveSMS != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.RECEIVE_MMS);
        }
        if (readSMS != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_SMS);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.SEND_SMS);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(getActivity(),
                    listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),
                    REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    void showOTPprogress(boolean status){
       if(status){
           otpDialog.show();
       }else {
           if(otpDialog!=null) otpDialog.dismiss();
       }
    }

    @OnClick(R.id.btn_verify)
    void verify() {
        String otpString = otpView.getText().toString();
        if (otpString.equals(PIN)) {
            if (TYPE == PHONE || TYPE == EMAIL) {
                mListener.onSignInVerified(mAuthToken, PIN);
            } else {
                //LoginDialogFragment.mInstance.onResetPassword(mContactID, PIN);
            }
            Utils.hideKeyboard(getContext());
        } else {
            otpView.setText("");
            Utils.showKeyboard(getContext(), otpView);
            Toaster.toast("Worng OTP");
        }
    }

    @OnClick(R.id.tv_resend)
    void resend() {
        resetOTP(mContactID);
    }

    private void resetOTP(String contactID) {
        showProgress(true);
        ModelManager.modelManager().loginByOtp(contactID, (Constants.Status iStatus, GenricResponse<HashMap<String, Object>> genricResponse) -> {
            HashMap<String, Object> map = genricResponse.getObject();
            PIN = (String) map.get(kOTP);
            showProgress(false);
            Toaster.toastRangeen(
                    "OTP sent to " + mContactID + "\n" + " Please enter OTP to complete SignIn process");
            Toaster.toastOTP("OTP: " + PIN);
        }, (Constants.Status iStatus, String message) -> {
            Utils.showAlertDialog(getContext(), "Otp Failed!", message);
            showProgress(false);
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        }
    }

    @Override
    public void otpReceived(String otp) {
        otpView.setText(otp);
        showOTPprogress(false);
        PIN = otp;
        verify();
    }


    public interface OnOTPFragmentInteractionListener {
        void onSignInVerified(String mAuthToken, String PIN);
    }
}
