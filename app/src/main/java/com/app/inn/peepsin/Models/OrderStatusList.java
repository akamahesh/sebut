package com.app.inn.peepsin.Models;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by dhruv on 11/10/17.
 */

public class OrderStatusList extends BaseModel implements Serializable{

    private int orderStatusId;
    private String orderStatus;

    public OrderStatusList(JSONObject jsonResponse){
        this.orderStatusId = getValue(jsonResponse,kOrderStatusId,   Integer.class);
        this.orderStatus = getValue(jsonResponse,kOrderStatus,  String.class);
    }

    public int getOrderStatusId() {
        return orderStatusId;
    }

    public String getOrderStatus() {
        return orderStatus;
    }
}
