package com.app.inn.peepsin.UI.fragments.Shop;

/**
 * Created by root on 6/7/17.
 */

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.CurrentUser;
import com.app.inn.peepsin.Models.ShopModel;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.fragments.Feeds.FeedsFragment;
import com.app.inn.peepsin.UI.fragments.Feeds.MapDialogFragment;
import com.app.inn.peepsin.UI.fragments.Profile.ShoppingCartFragment;
import com.app.inn.peepsin.UI.helpers.GridSpacingItemDecoration;
import com.app.inn.peepsin.UI.helpers.MyBounceInterpolator;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kImageUrl;
import static com.app.inn.peepsin.Constants.Constants.kLatitude;
import static com.app.inn.peepsin.Constants.Constants.kLongitude;
import static com.app.inn.peepsin.Constants.Constants.kOwnerName;

public class FavoriteShopsFragment extends Fragment {

    private static Switcher switcherListener;
    @BindView(R.id.recycler_view_feeds)
    RecyclerView recyclerViewFeeds;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.empty_view)
    View emptyView;
    @BindView(R.id.tv_empty_title)
    TextView tvEmptyTitle;
    @BindView(R.id.tv_empty_message)
    TextView tvEmptyMessage;
    @BindView(R.id.tv_cartCount)
    TextView tvCartCount;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    private ShopAdapter shopAdapter;
    private ProgressDialog progressDialog;
    private List<ShopModel> shopList;
    private CurrentUser user;
    //AppBarLayout appbarLayout;

    public static Fragment newInstance(Switcher switcher) {
        switcherListener = switcher;
        return new FavoriteShopsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = Utils.generateProgressDialog(getContext(), false);
        shopList = new ArrayList<>();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favorite_shops, container, false);
        ButterKnife.bind(this, view);
        /*appbarLayout=((HomeActivity) getActivity()).getTabLayout();
        Utils.isprofileClicked=1;*/
        tvTitle.setText(getString(R.string.title_favorite_shops));
        tvEmptyTitle.setText(getString(R.string.favorite_shops_empty_title));
        tvEmptyMessage.setText(R.string.favorite_shops_empty);
        shopAdapter = new ShopAdapter(shopList);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 1);
        recyclerViewFeeds.setLayoutManager(mLayoutManager);
        recyclerViewFeeds.addItemDecoration(new GridSpacingItemDecoration(1, Utils.dpToPx(getContext(), 5), true));
        recyclerViewFeeds.setItemAnimator(new DefaultItemAnimator());
        recyclerViewFeeds.setAdapter(shopAdapter);
        swipeRefreshLayout.setOnRefreshListener(onRefreshListener);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData();
    }

    SwipeRefreshLayout.OnRefreshListener onRefreshListener = this::refreshData;

    @Override
    public void onResume() {
        super.onResume();
        user = ModelManager.modelManager().getCurrentUser();
        setCartUpdate(user.getShoppingCartCount());
        //appbarLayout.setVisibility(View.GONE);

    }

    public void setCartUpdate(int count) {
        if (count == 0)
            tvCartCount.setVisibility(View.GONE);
        else {
            tvCartCount.setVisibility(View.VISIBLE);
            tvCartCount.setText(String.valueOf(count));
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        switcherListener = null;
    }

    @OnClick(R.id.btn_cart)
    void onCart() {
        if (switcherListener != null)
            switcherListener.switchFragment(ShoppingCartFragment.newInstance(switcherListener), true, true);
        //startActivity(ShoppingCartActivity.getIntent(getContext()));
    }


    @OnClick(R.id.btn_back)
    void onBack() {
        getFragmentManager().popBackStack();
        /*appbarLayout.setVisibility(View.VISIBLE);
        Utils.isprofileClicked=0;*/
    }

    private void refreshData() {
        ModelManager.modelManager().getFavouriteShopList((Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<ShopModel>> genericResponse) -> {
            showProgress(false);
            shopList = genericResponse.getObject();
            shopAdapter.addItems(shopList);
            checkEmptyScreen();
            swipeRefreshLayout.setRefreshing(false);
        }, (Constants.Status iStatus, String message) -> {
            Toaster.toast(message);
            checkEmptyScreen();
            swipeRefreshLayout.setRefreshing(false);
        });
    }

    private void loadData() {
        showProgress(true);
        ModelManager.modelManager().getFavouriteShopList((Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<ShopModel>> genericResponse) -> {
            showProgress(false);
            shopList = genericResponse.getObject();
            shopAdapter.addItems(shopList);
            checkEmptyScreen();
        }, (Constants.Status iStatus, String message) -> {
            Toaster.toast(message);
            showProgress(false);
            checkEmptyScreen();
        });
    }

    private void checkEmptyScreen() {
        if (shopAdapter.getItemCount() > 0) {
            emptyView.setVisibility(View.GONE);
        } else {
            emptyView.setVisibility(View.VISIBLE);
        }
    }


    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) progressDialog.cancel();
        }
    }

    public class ShopAdapter extends RecyclerView.Adapter<ShopAdapter.ShopViewHolder> {
        private List<ShopModel> itemList;

        private ShopAdapter(List<ShopModel> items) {
            this.itemList = items;
        }

        @Override
        public ShopViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_shop_near_me_layout, parent, false);
            return new ShopViewHolder(layoutView);
        }

        @Override
        public void onBindViewHolder(ShopViewHolder holder, int position) {
            ShopModel shop = itemList.get(position);
            holder.bindContent(shop);
            String shopName = shop.getName();
            holder.tvShopName.setText(shopName);
            holder.tvOwnerName.setText(shop.getOwnerName());
            holder.tvProductCount.setText(String.valueOf(shop.getProductCount()));
            holder.tvRating.setText(String.valueOf(shop.getRating()));
            String address = shop.getAddress().getStreetAddress() + " " + shop.getAddress().getCity();
            holder.tvAddress.setText(address);
            Integer connectionLevel = shop.getConnectionLevel();

            if (connectionLevel == 1) {
                holder.tvConnectionLevel.setText("1st");
            } else if (connectionLevel == 2) {
                holder.tvConnectionLevel.setText("2nd");
            } else if (connectionLevel == 3) {
                holder.tvConnectionLevel.setText("3rd");
            } else {
                holder.tvConnectionLevel.setVisibility(View.INVISIBLE);
            }

            TextDrawable drawable = TextDrawable.builder().buildRoundRect(String.valueOf(shopName.toUpperCase().charAt(0)), ColorGenerator.MATERIAL.getRandomColor(), 5);
            if (!shop.getBannerUrl().isEmpty())
                Picasso.with(getContext())
                        .load(shop.getBannerUrl())
                        .fit()
                        .placeholder(drawable)
                        .error(drawable)
                        .into(holder.ivBannerImage);
            else {
                holder.ivBannerImage.setImageDrawable(drawable);
            }
            if (!shop.getLogoUrl().isEmpty())
                Picasso.with(getContext())
                        .load(shop.getLogoUrl())
                        .fit()
                        .into(holder.ivLogoImage);

            if (!shop.getProfileUrl().isEmpty())
                Picasso.with(getContext())
                        .load(shop.getProfileUrl())
                        .fit()
                        .into(holder.ivProfile);
        }

        @Override
        public int getItemCount() {
            return itemList.size();
        }

        public void addItems(List<ShopModel> shopList) {
            itemList.clear();
            itemList.addAll(shopList);
            notifyDataSetChanged();
        }

        class ShopViewHolder extends RecyclerView.ViewHolder {
            private ShopModel item;
            @BindView(R.id.iv_banner_image)
            ImageView ivBannerImage;
            @BindView(R.id.iv_profile_image)
            ImageView ivProfile;
            @BindView(R.id.iv_shop_logo)
            ImageView ivLogoImage;
            @BindView(R.id.tv_shop_name)
            TextView tvShopName;
            @BindView(R.id.tv_owner_name)
            TextView tvOwnerName;
            @BindView(R.id.tv_address)
            TextView tvAddress;
            @BindView(R.id.iv_like)
            ImageView ivLike;
            @BindView(R.id.tv_growth_text)
            TextView tvProductCount;
            @BindView(R.id.tv_rating)
            TextView tvRating;
            @BindView(R.id.tv_level)
            TextView tvConnectionLevel;
            @BindView(R.id.near_progressBar)
            ProgressBar progressBar;

            ShopViewHolder(View view) {
                super(view);
                ButterKnife.bind(this, view);
            }

            @OnClick(R.id.card_view)
            void OnItemClick() {
                if (switcherListener != null)
                    switcherListener.switchFragment(FeedsFragment.newInstance(switcherListener, item.getId()), true, true);
            }

            @OnClick(R.id.iv_like)
            void onLike(ImageView imageView) {
                Animation favAnim = AnimationUtils.loadAnimation(getContext(), R.anim.bounce);
                MyBounceInterpolator favInterpolar = new MyBounceInterpolator(.2, 20);
                favAnim.setInterpolator(favInterpolar);
               // imageView.startAnimation(favAnim);
                makeFavourite();
            }

            void makeFavourite() {
                showProgress(progressBar,ivLike, true);
                ModelManager.modelManager().setFavouriteShop(item.getId(), 0, (Constants.Status iStatus) -> {
                    itemList.remove(getAdapterPosition());
                    notifyDataSetChanged();
                    checkEmptyScreen();
                    showProgress(progressBar, ivLike,false);

                }, (Constants.Status iStatus, String message) -> {
                    Toaster.toast(message);
                    checkEmptyScreen();
                });
            }

            @OnClick(R.id.iv_location)
            void getLocation() {
                try {
                    String lat = user.getPrimaryAddress().getLatitude();
                    String sLat = item.getAddress().getLatitude();
                    String sLon = item.getAddress().getLongitude();
                    if (!lat.isEmpty() && !sLat.isEmpty()) {
                        MapDialogFragment mapDialogFragment = MapDialogFragment.newInstance();
                        Bundle bdl = new Bundle();
                        bdl.putString(kLatitude, sLat);
                        bdl.putString(kLongitude, sLon);
                        bdl.putString(kOwnerName, item.getName());
                        bdl.putString(kImageUrl, item.getBannerUrl());
                        mapDialogFragment.setArguments(bdl);
                        mapDialogFragment.setCancelable(true);
                        mapDialogFragment.show(getChildFragmentManager(), mapDialogFragment.getClass().getSimpleName());
                    } else
                        Toaster.toast("Sorry Lat Long UnAvailable");
                } catch (Exception e) {
                    Toaster.toast("Sorry Lat Long UnAvailable");
                    e.printStackTrace();
                }
            }

            private void bindContent(ShopModel item) {
                this.item = item;
            }

        }

    }
    private void showProgress(ProgressBar bar,ImageView view, boolean b) {
        if (b) {
            view.setVisibility(View.INVISIBLE);
            bar.setVisibility(View.VISIBLE);
            bar.getIndeterminateDrawable().setColorFilter(Color.RED, PorterDuff.Mode.MULTIPLY);

        } else {
            view.setVisibility(View.VISIBLE);
            bar.setVisibility(View.GONE);

        }
    }
}

