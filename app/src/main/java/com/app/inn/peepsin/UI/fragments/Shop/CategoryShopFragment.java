package com.app.inn.peepsin.UI.fragments.Shop;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.CurrentUser;
import com.app.inn.peepsin.Models.ProductType;
import com.app.inn.peepsin.Models.ShopModel;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.adapters.CategoryShopAdapter;
import com.app.inn.peepsin.UI.fragments.Profile.ShoppingCartFragment;
import com.app.inn.peepsin.UI.helpers.DividerItemRecyclerDecoration;
import com.app.inn.peepsin.UI.helpers.ItemOffsetDecoration;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kCategoryId;
import static com.app.inn.peepsin.Constants.Constants.kCategoryImageURL;
import static com.app.inn.peepsin.Constants.Constants.kCategoryName;
import static com.app.inn.peepsin.Constants.Constants.kCategoryStore;
import static com.app.inn.peepsin.Constants.Constants.kProductTypeId;

/**
 * Created by dhruv on 6/9/17.
 */

public class  CategoryShopFragment extends Fragment {

    private String TAG = getTag();

    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.empty_view)
    View emptyView;
    @BindView(R.id.swipeRefreshLayout)

    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.recycler_view_shops)
    RecyclerView recyclerShops;

    @BindView(R.id.tv_cartCount)
    TextView tvCartCount;

    @BindView(R.id.iv_image_category)
    ImageView ivImageCategory;

    @BindView(R.id.tv_change_category)
    TextView tvChangeCategory;

    private static Switcher switcherListener;
    private ProgressDialog progressDialog;
    private int mPage;
    private boolean loading;
    private int pageSize;

    private LinearLayoutManager mLayoutManager;
    private List<ShopModel> shopCategoryList;
    private CategoryShopAdapter shopCategoryAdapter;
    private List<ProductType> categoryList;
    private int categoryId;
    private String categoryName;
    private String categoryImageUrl;
    private Dialog dialog;

    public static Fragment newInstance(Switcher switcher, int categoryId,String categoryName,String categoryImageUrl, List<ProductType> categoryList) {
        Fragment fragment = new CategoryShopFragment();
        switcherListener = switcher;
        Bundle bundle = new Bundle();
        bundle.putInt(kCategoryId, categoryId);
        bundle.putString(kCategoryName,categoryName);
        bundle.putString(kCategoryImageURL,categoryImageUrl);
        bundle.putSerializable(kCategoryStore, (Serializable) categoryList);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = Utils.generateProgressDialog(getContext(), true);
        shopCategoryList = new ArrayList<>();
        Bundle bundle = getArguments();
        if (bundle != null) {
            categoryId = bundle.getInt(kCategoryId);
            categoryName=bundle.getString(kCategoryName);
            categoryImageUrl=bundle.getString(kCategoryImageURL);
            categoryList = (List<ProductType>)bundle.getSerializable(kCategoryStore);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_category_shop, container, false);
        ButterKnife.bind(this, view);
        tvTitle.setText(categoryName);

        if (!categoryImageUrl.isEmpty()) {
            Picasso.with(getContext())
                    .load(categoryImageUrl)
                    .placeholder(R.drawable.img_product_placeholder)
                    .into(ivImageCategory);
        } else {
            ivImageCategory.setImageDrawable(this.getResources().getDrawable(R.drawable.img_product_placeholder));

        }

        // Shop Adapter and Category Shop List
        shopCategoryAdapter = new CategoryShopAdapter(getContext(),shopCategoryList,switcherListener);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerShops.setLayoutManager(mLayoutManager);
        recyclerShops.setAdapter(shopCategoryAdapter);
        recyclerShops.addItemDecoration(new ItemOffsetDecoration(getContext(), R.dimen.item_offset));
        recyclerShops.addOnScrollListener(onScrollListener);
        swipeRefreshLayout.setOnRefreshListener(onRefreshListener);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadShopList();
    }

    @OnClick(R.id.btn_back)
    void onBack() {
        getFragmentManager().popBackStack();
        Utils.isShopBack = 1;
    }

    @OnClick(R.id.btn_cart)
    void onCart() {
        if(switcherListener!=null)
            switcherListener.switchFragment(ShoppingCartFragment.newInstance(switcherListener),true,true);
    }

    @Override
    public void onResume() {
        super.onResume();
        CurrentUser currentUser = ModelManager.modelManager().getCurrentUser();
        setCartUpdate(currentUser.getShoppingCartCount());
    }

    public void setCartUpdate(int count) {
        if (count == 0)
            tvCartCount.setVisibility(View.GONE);
        else {
            tvCartCount.setVisibility(View.VISIBLE);
            tvCartCount.setText(String.valueOf(count));
        }
    }

    @OnClick(R.id.category_view)
    void onCategoryChange(){
        dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_category_shop_filter);
        RecyclerView rvInventory = ButterKnife.findById(dialog, R.id.recycler_view_inventory);
        InventoryAdapter inventoryAdapter = new InventoryAdapter(categoryList);
        rvInventory.setHasFixedSize(true);
        rvInventory.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvInventory.addItemDecoration(new DividerItemRecyclerDecoration(getActivity(), R.drawable.canvas_recycler_divider));
        rvInventory.setItemAnimator(new DefaultItemAnimator());
        rvInventory.setAdapter(inventoryAdapter);
        dialog.show();
    }

    public void initialState() {
        mPage = 1;
        loading = true;
        pageSize = shopCategoryList.size();
    }

    SwipeRefreshLayout.OnRefreshListener onRefreshListener = this::loadShopList;

    RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            if (dy > 0) //check for scroll down
            {
                int visibleItemCount = mLayoutManager.getChildCount();
                int totalItemCount = mLayoutManager.getItemCount();
                int firstVisibleItemPosition = mLayoutManager.findFirstVisibleItemPosition();

                if (loading) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= pageSize) {
                        loading = false;
                        mPage++;
                        //Do pagination.. i.e. fetch new data
                        loadMoreShopList();
                    }
                }
            }
        }
    };

    // Refresh list on Swipe
    public void loadShopList() {
        showProgress(true);
        HashMap<String ,Object> map = new HashMap<>();
        map.put(kProductTypeId,categoryId);
        ModelManager.modelManager().getShopList(map,1, (Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<ShopModel>> genericResponse) -> {
            shopCategoryList = genericResponse.getObject();
            shopCategoryAdapter.addItems(shopCategoryList);
            initialState();
            checkEmptyScreen();
            showProgress(false);
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            checkEmptyScreen();
            Log.e(TAG, message);
        });
    }

    public void loadMoreShopList() {
        showProgress(true);
        HashMap<String ,Object> map = new HashMap<>();
        map.put(kProductTypeId,categoryId);
        ModelManager.modelManager().getShopList(map, mPage, (Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<ShopModel>> genericResponse) -> {
            shopCategoryList.addAll(genericResponse.getObject());
            shopCategoryAdapter.addMoreItems(genericResponse.getObject());
            loading = !genericResponse.getObject().isEmpty();
            showProgress(false);
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Log.e(TAG, message);
        });
    }

    private void checkEmptyScreen() {
        if (shopCategoryAdapter.getItemCount() > 0) {
            emptyView.setVisibility(View.GONE);
        } else {
            emptyView.setVisibility(View.VISIBLE);
        }
    }


    private void showProgress(boolean b) {
        if (b) {
            swipeRefreshLayout.setRefreshing(true);
            //progressDialog.show();
        } else {
            swipeRefreshLayout.setRefreshing(false);
            //if (progressDialog != null) progressDialog.cancel();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        switcherListener = null;
    }

    public class InventoryAdapter extends RecyclerView.Adapter<InventoryAdapter.ViewHolder> {

        private List<ProductType> inventoryList;

        private InventoryAdapter(List<ProductType> inventoryList) {
            this.inventoryList = inventoryList;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_category_shop_filter, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            ProductType shop = inventoryList.get(position);
            holder.bindContent(shop);
            String name = shop.getName();
            holder.tvItemName.setText(name);

            String imageURL = shop.getUrl();
            if (!imageURL.isEmpty())
                Picasso.with(getContext())
                        .load(imageURL)
                        .placeholder(R.drawable.img_product_placeholder)
                        .into(holder.ivItemImage);
            else
                holder.ivItemImage.setImageDrawable(
                        getContext().getResources().getDrawable(R.drawable.img_product_placeholder));
        }

        @Override
        public int getItemCount () {
            return inventoryList.size();
        }

        public void addItems (List < ProductType > inventoryList) {
            this.inventoryList.clear();
            this.inventoryList.addAll(inventoryList);
            notifyDataSetChanged();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            ProductType shopCategory;
            @BindView(R.id.iv_item_image)
            ImageView ivItemImage;
            @BindView(R.id.tv_item_name)
            TextView tvItemName;

            public ViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            public void bindContent(ProductType shop) {
                this.shopCategory = shop;
            }

            @OnClick(R.id.item_view)
            void onInventory() {
                categoryId = shopCategory.getId();
                loadShopList();
                String catname=shopCategory.getName();
                tvChangeCategory.setText(catname);
                dialog.dismiss();
            }
        }
    }


}
