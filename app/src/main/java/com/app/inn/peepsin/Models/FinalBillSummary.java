package com.app.inn.peepsin.Models;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by root on 18/5/17.
 */

public class FinalBillSummary extends BaseModel implements Serializable{

    private Integer orderId;
    private String  promoCode;
    private Integer isValid;
    private String promoCodeTagLine;
    private String totalCartPriceBeforePromoCode;
    private String finalAmountAfterPromoCode;
    private String discountedPrice;
    private Integer cartItemCount;
    private Address deliveryAddress;
    private CopyOnWriteArrayList<FinalProductsRecords> shopRecords;
    //private String checksumHash;

    public FinalBillSummary(JSONObject jsonResponse) {
        this.orderId              = getValue(jsonResponse, kOrderId ,         Integer.class);
        this.promoCode            = getValue(jsonResponse, kPromoCode ,       String.class);
        this.isValid              = getValue(jsonResponse, kIsValid,          Integer.class);
        this.promoCodeTagLine     = getValue(jsonResponse, kPromoCodeTagLine, String.class);
        this.totalCartPriceBeforePromoCode = getValue(jsonResponse, kTotalCartPriceBeforePromoCode, String.class);
        this.finalAmountAfterPromoCode     = getValue(jsonResponse, kFinalAmountAfterPromoCode,     String.class);
        this.discountedPrice      = getValue(jsonResponse, kDiscountedPrice,  String.class);
        this.cartItemCount        = getValue(jsonResponse, kCartItemCount,    Integer.class);
        try { //to handle @ClassCastException when addressId is emptyString, not to be removed
            this.deliveryAddress            = new Address(getValue(jsonResponse, kDeliveryAddress,   JSONObject.class));
        }catch (Exception e){
            Log.e("Error : ",e.getMessage());
        }
        try {
            this.shopRecords    = handleShopList(getValue(jsonResponse, kRecords,   JSONObject.class));
        }catch (Exception e){
            Log.e("Error : ",e.getMessage());
        }
    }

    public Integer getOrderId() {
        return orderId;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public Integer getIsValid() {
        return isValid;
    }

    public String getPromoCodeTagLine() {
        return promoCodeTagLine;
    }

    public Address getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(Address deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String getTotalCartPriceBeforePromoCode() {
        return totalCartPriceBeforePromoCode;
    }

    public String getFinalAmountAfterPromoCode() {
        return finalAmountAfterPromoCode;
    }

    public String getDiscountedPrice() {
        return discountedPrice;
    }

    public Integer getCartItemCount() {
        return cartItemCount;
    }

    private CopyOnWriteArrayList<FinalProductsRecords> handleShopList(JSONArray jsonArray) throws JSONException {
        CopyOnWriteArrayList<FinalProductsRecords> billShopList = new CopyOnWriteArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            billShopList.add(new FinalProductsRecords(jsonArray.getJSONObject(i)));
        }

        return billShopList;
    }

    public CopyOnWriteArrayList<FinalProductsRecords> getShopRecords() {
        return shopRecords;
    }
}
