package com.app.inn.peepsin.UI.fragments.Feeds;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.SharedProduct;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.adapters.SharedProductAdapter;
import com.app.inn.peepsin.UI.helpers.DividerItemRecyclerDecoration;
import com.app.inn.peepsin.UI.interfaces.Switcher;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by dhruv on 27/7/17.
 */

public class SharedProductListFragment extends Fragment {

    private static final String TAG = SharedProductListFragment.class.getSimpleName();

    @BindView(R.id.recycler_view_feeds)
    RecyclerView recyclerViewFeeds;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.empty_view) View emptyView;
    @BindView(R.id.tv_empty_title) TextView tvEmptyTitle;
    @BindView(R.id.tv_empty_message) TextView tvEmptyMessage;
    @BindView(R.id.tv_title) TextView tvTitle;

    private static Switcher switcherListener;
    private int mPage;
    private boolean loading;
    private int pageSize;

    private SharedProductAdapter productAdapter;
    private LinearLayoutManager mLayoutManager;
    private List<SharedProduct> productsList;

    public static SharedProductListFragment newInstance(Switcher switcher) {
        switcherListener = switcher;
        return new SharedProductListFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        productsList = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_shared_product, container, false);
        ButterKnife.bind(this, view);
        tvTitle.setText("Recommended");
        tvEmptyTitle.setText(R.string.shared_product_title);
        tvEmptyMessage.setText(R.string.shared_product_message);

        // swipe to refresh the new list
        swipeRefreshLayout.setOnRefreshListener(onRefreshListener);

        // Feed Adapter and feed List
        productAdapter = new SharedProductAdapter(getContext(), productsList, switcherListener);
        recyclerViewFeeds.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerViewFeeds.setLayoutManager(mLayoutManager);
        recyclerViewFeeds.setAdapter(productAdapter);
        recyclerViewFeeds.addItemDecoration(new DividerItemRecyclerDecoration(getContext(),R.drawable.canvas_recycler_divider));
        recyclerViewFeeds.addOnScrollListener(onScrollListener);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadProducts();
    }

    public void initialState(){
        mPage=1;
        loading=true;
        pageSize=productsList.size();
    }

    @OnClick(R.id.btn_back)
    void btnBack(){
        getFragmentManager().popBackStack();
    }

    SwipeRefreshLayout.OnRefreshListener onRefreshListener = this::loadProducts;

    RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            if(dy > 0) //check for scroll down
            {
                int visibleItemCount = mLayoutManager.getChildCount();
                int totalItemCount = mLayoutManager.getItemCount();
                int firstVisibleItemPosition = mLayoutManager.findFirstVisibleItemPosition();

                if (loading)
                {
                    if ( (visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= pageSize) {
                        loading = false;
                        ++mPage;
                        //Do pagination.. i.e. fetch new data
                        loadMoreData();
                    }
                }
            }
        }
    };

    public void loadProducts() {
        showProgress(true);
        ModelManager.modelManager().getSharedProductList(1,(Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<SharedProduct>> genericResponse) -> {
            CopyOnWriteArrayList<SharedProduct> list = genericResponse.getObject();
            productsList = list;
            productAdapter.addNewItems(list);
            checkEmptyScreen();
            initialState();
            showProgress(false);
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Log.e(TAG,message);
            checkEmptyScreen();
            showProgress(false);
        });
    }

    public void loadMoreData() {
        showProgress(true);
        ModelManager.modelManager().getSharedProductList(mPage,(Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<SharedProduct>> genericResponse) -> {
            CopyOnWriteArrayList<SharedProduct> list = genericResponse.getObject();
            showProgress(false);
            productAdapter.addItems(list);
            loading = !genericResponse.getObject().isEmpty();
        }, (Constants.Status iStatus, String message) -> {
            Log.e(TAG,message);
            showProgress(false);
        });
    }

    private void checkEmptyScreen() {
        if (productAdapter.getItemCount() > 1) {
            emptyView.setVisibility(View.GONE);
        } else {
            emptyView.setVisibility(View.VISIBLE);

        }
    }

    private void showProgress(boolean b) {
        if (b) {
            //progressDialog.show();
            swipeRefreshLayout.setRefreshing(true);
        } else {
            swipeRefreshLayout.setRefreshing(false);
            //if (progressDialog != null) progressDialog.cancel();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        switcherListener = null;
    }
}
