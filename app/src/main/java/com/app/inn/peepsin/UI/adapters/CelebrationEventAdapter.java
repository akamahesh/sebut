package com.app.inn.peepsin.UI.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.inn.peepsin.Models.CelebrationEvent;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.fragments.Profile.CelebrationEventDetailsFragment;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by dhruv on 27/9/17.
 */

public class CelebrationEventAdapter extends RecyclerView.Adapter<CelebrationEventAdapter.EventViewHolder>{
    private Context context;
    private List<CelebrationEvent> eventList;
    private boolean editEnabled;
    private Switcher switcherListener;

    public CelebrationEventAdapter(Context context, List<CelebrationEvent> eventList, boolean editEnable, Switcher profileSwitcher) {
        this.context = context;
        this.eventList = eventList;
        switcherListener = profileSwitcher;
        editEnabled = editEnable;
    }

    @Override
    public EventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_event_item_layout, parent, false);
        return new EventViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(EventViewHolder holder, int position) {
        CelebrationEvent event = eventList.get(position);
        holder.bindContent(event);
        String title = event.getEventTitle();
        String message = event.getMessage();
        String eventType = event.getEventTypeName();
        String date = Utils.getTimeStampDate(event.getSchedulingTimeStamp());

        holder.tvTitle.setText(title);
        holder.tvMessage.setText(message);
        holder.tvEventType.setText(eventType);
        holder.tvDate.setText(date);

        String imageUrl = event.getImageUrl();
        if(!imageUrl.isEmpty())
            Picasso.with(context).load(imageUrl).fit()
                    .placeholder(R.drawable.img_event_banner).into(holder.ivEvent);
        else
            holder.ivEvent.setImageResource(R.drawable.img_event_banner);
    }

    @Override
    public int getItemCount() {
        return eventList.size();
    }

    public void addItems(List<CelebrationEvent> eventList){
        this.eventList.clear();
        this.eventList.addAll(eventList);
        notifyDataSetChanged();
    }

    public void addItem(CelebrationEvent event){
        this.eventList.add(event);
        notifyDataSetChanged();
    }

    class EventViewHolder extends RecyclerView.ViewHolder{
        private CelebrationEvent item;
        @BindView(R.id.tv_name)
        TextView tvTitle;
        @BindView(R.id.tv_message) TextView tvMessage;
        @BindView(R.id.tv_event_type) TextView tvEventType;
        @BindView(R.id.tv_date) TextView tvDate;
        @BindView(R.id.iv_event)
        ImageView ivEvent;


        EventViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        void bindContent(CelebrationEvent event) {
            this.item= event;
        }

        @OnClick(R.id.item_view)
        void eventDetails(){
            if(editEnabled && switcherListener !=null){
                switcherListener.switchFragment(CelebrationEventDetailsFragment.newInstance(item,switcherListener),true,true);
            }
        }
    }
}
