package com.app.inn.peepsin.UI.helpers;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;

import com.app.inn.peepsin.Libraries.ImageEditor.ImageController;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.app.inn.peepsin.Constants.Constants.PHOTO_RESULT;
import static com.app.inn.peepsin.Constants.Constants.kData;

/**
 * Created by dhruv on 9/10/17.
 */

public class ImageIntentController {

    private Fragment fragment;
    private Context context;
    private ImageUtilListener mListener;
    public ImageIntentController(Fragment fragment) {
        this.fragment = fragment;
        this.context = fragment.getContext();
        this.mListener = (ImageUtilListener) fragment;
    }

    //Callbacks for ImageUtil events
    public interface ImageUtilListener {
        void onImageLoadFailed();
        void onImageLoadSuccess(String filepath);
    }

    public void pickPhoto() {
        fragment.startActivityForResult(getPickImageChooserIntent(), PHOTO_RESULT);
    }

    /**
     * Create a chooser intent to select the source to get image from.<br/>
     * The source can be camera's (ACTION_IMAGE_CAPTURE) or gallery's (ACTION_GET_CONTENT).<br/>
     * All possible sources are added to the intent chooser.
     */
    private Intent getPickImageChooserIntent() {

        // Determine Uri of camera image to save.
        Uri outputFileUri = getCaptureImageOutputUri();

        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = context.getPackageManager();

        // collect all camera intents
        Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }

        // collect all gallery intents
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }

        // the main intent is the last in the list (fucking android) so pickup the useless one
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);

        // Create a chooser from the main intent
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");

        // Add all other intents
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));

        return chooserIntent;
    }

    /**
     * Get URI to image received from capture by camera.
     */
    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = context.getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "profile.png"));
        }
        return outputFileUri;
    }

    public void onActivityResult(int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            Uri imageUri = data.getData();
            if(imageUri==null){
                Bitmap bitmap = (Bitmap) data.getExtras().get(kData);
                imageUri = ImageController.getImageUri(fragment.getContext(),bitmap);
            }
            String filepath ;
            if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.M){
                filepath = Utils.getPathFromURI_API21(fragment.getContext(),imageUri);
            }else {
                filepath = Utils.getPathFromURI_API18(fragment.getContext(),imageUri);
            }
            mListener.onImageLoadSuccess(filepath);
        }
    }
}
