package com.app.inn.peepsin.UI.adapters;

/**
 * Created by root on 3/7/17.
 */

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.inn.peepsin.Managers.BaseManager.DateManager;
import com.app.inn.peepsin.Models.ReviewModel;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.activities.HomeActivity;
import com.app.inn.peepsin.UI.helpers.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ReviewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ReviewModel> mReviewList;
    private Context context;
    private int pageSize;

    public ReviewAdapter(Context context, List<ReviewModel> items,int pageSize) {
        mReviewList = items;
        this.context = context;
        this.pageSize = pageSize;
    }

    @Override
    public int getItemViewType(int position) {
        if(position == mReviewList.size())
            return 0;
        else
            return 1;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == 1) {
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_review_layout, parent, false);
            return new ReviewHolder(layoutView);
        }
        else{
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_feed_progress, parent, false);
            return new ProgressHolder(layoutView);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

        if(position == mReviewList.size()) {
            ProgressHolder view = (ProgressHolder) viewHolder;
            view.progressBar.setVisibility(View.GONE);
            view.tvFeedCount.setVisibility(View.GONE);
            /*if(mReviewList.size()>5){
                view.progressBar.setVisibility(View.VISIBLE);
                view.tvFeedCount.setVisibility(View.GONE);
            }
            if(mReviewList.size()>=pageSize){
                new Handler().postDelayed(() -> {
                    //Hide the refresh after 2sec
                    ((HomeActivity)context).runOnUiThread(() -> {
                        view.progressBar.setVisibility(View.GONE);
                        view.tvFeedCount.setVisibility(View.VISIBLE);
                        String countTxt = context.getString(R.string.feed_total_text)+ mReviewList.size();
                        view.tvFeedCount.setText(countTxt);
                    });
                }, 1000);
            }*/

        }else{
            ReviewHolder holder = (ReviewHolder) viewHolder;
            ReviewModel item = mReviewList.get(position);
            holder.bindContent(item);
            String name = item.getUsername();
            String review = item.getReview();
            int rating = item.getRating();
          //  int dates= Integer.parseInt(DateManager.dispalyValue(item.getTimeStamp()));
          //  String postingDate = String.valueOf(dates/1000);
            String ratingValue = Utils.getReviewLevel(item.getRating());
            Long stamp = Long.parseLong(item.getTimeStamp())/1000;
            String postingDate = DateManager.dispalyValue(stamp.toString());

            holder.tvUserName.setText(name);
            holder.tvReview.setText(review);
            holder.tvRating.setText(String.valueOf(rating));
            holder.tvTimestamp.setText(postingDate);
            holder.tvRatingValue.setText(ratingValue);
        }
    }

    @Override
    public int getItemCount() {
        return mReviewList.size()+1;
    }

    class ProgressHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.tv_feed_count)
        TextView tvFeedCount;
        @BindView(R.id.progress_bar)
        ProgressBar progressBar;

        ProgressHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class ReviewHolder extends RecyclerView.ViewHolder {
        ReviewModel item;
        @BindView(R.id.tv_user_name)
        TextView tvUserName;
        @BindView(R.id.tv_review)
        TextView tvReview;
        @BindView(R.id.tv_rating)
        TextView tvRating;
        @BindView(R.id.tv_timestamp)
        TextView tvTimestamp;
        @BindView(R.id.tv_rating_expression)
        TextView tvRatingValue;

        ReviewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void bindContent(ReviewModel item) {
            this.item = item;
        }
    }
}

