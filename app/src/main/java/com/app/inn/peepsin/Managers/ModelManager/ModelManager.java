package com.app.inn.peepsin.Managers.ModelManager;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.app.inn.peepsin.Constants.Blocks.Block;
import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.DataBase.DataBaseHandler;
import com.app.inn.peepsin.Libraries.DispatchQueue.DispatchQueue;
import com.app.inn.peepsin.Managers.APIManager.APIManager;
import com.app.inn.peepsin.Managers.APIManager.APIRequestHelper;
import com.app.inn.peepsin.Managers.BaseManager.ApplicationManager;
import com.app.inn.peepsin.Managers.BaseManager.BaseManager;
import com.app.inn.peepsin.Models.Address;
import com.app.inn.peepsin.Models.BankType;
import com.app.inn.peepsin.Models.BaseModel;
import com.app.inn.peepsin.Models.BlockedUser;
import com.app.inn.peepsin.Models.Board;
import com.app.inn.peepsin.Models.CelebrationEvent;
import com.app.inn.peepsin.Models.CelebrationMessage;
import com.app.inn.peepsin.Models.CheckLatestBuild;
import com.app.inn.peepsin.Models.CheckSumDetails;
import com.app.inn.peepsin.Models.Comment;
import com.app.inn.peepsin.Models.Connection;
import com.app.inn.peepsin.Models.CurrentUser;
import com.app.inn.peepsin.Models.DeliveryType;
import com.app.inn.peepsin.Models.EventImages;
import com.app.inn.peepsin.Models.FacebookContact;
import com.app.inn.peepsin.Models.Feeds;
import com.app.inn.peepsin.Models.FinalBillSummary;
import com.app.inn.peepsin.Models.GoogleContact;
import com.app.inn.peepsin.Models.MyShop;
import com.app.inn.peepsin.Models.OrderHistory;
import com.app.inn.peepsin.Models.OrderStatusList;
import com.app.inn.peepsin.Models.PayUmoneyDetails;
import com.app.inn.peepsin.Models.People;
import com.app.inn.peepsin.Models.PhoneContact;
import com.app.inn.peepsin.Models.ProductInventory;
import com.app.inn.peepsin.Models.ProductType;
import com.app.inn.peepsin.Models.RatingModel;
import com.app.inn.peepsin.Models.Requests;
import com.app.inn.peepsin.Models.ReviewModel;
import com.app.inn.peepsin.Models.Search;
import com.app.inn.peepsin.Models.SellingProduct;
import com.app.inn.peepsin.Models.SharedProduct;
import com.app.inn.peepsin.Models.Shop;
import com.app.inn.peepsin.Models.ShopCatSubCategory;
import com.app.inn.peepsin.Models.ShopCategory;
import com.app.inn.peepsin.Models.ShopModel;
import com.app.inn.peepsin.Models.ShopProduct;
import com.app.inn.peepsin.Models.ShopRating;
import com.app.inn.peepsin.Models.ShoppingCart;
import com.app.inn.peepsin.Models.SocialLink;
import com.app.inn.peepsin.Models.SubCategory;
import com.app.inn.peepsin.Models.TAX;
import com.app.inn.peepsin.Models.UserLocation;
import com.app.inn.peepsin.UI.helpers.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kAcceptConnectionRequest;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kActivateMyShop;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kAddBoard;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kAddEditReview;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kAddShoppingCart;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kAuthenticateUser;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kCancelConnectionRequest;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kCancelOrderBeforePayment;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kCancelPlacedOrder;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kChangeOrderStatus;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kCheckRegistration;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kDeleteMyShop;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kDeleteProduct;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kEditComments;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kEditProductInventory;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kGenerateCheckSum;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kGenerateHashCode;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kGetComments;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kGetFeaturedShopList;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kGetFinalBillSummary;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kGetFriendSuggestions;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kGetMyBoardList;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kGetMySharedProduct;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kGetMyShopDetails;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kGetOrderCurrentStatus;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kGetOrderHistory;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kGetOrderStatus;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kGetOrderSummaryAfterPayment;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kGetPeepsInBoardList;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kGetProductDetails;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kGetProductFeeds;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kGetProductRating;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kGetRatingReviewList;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kGetRecievedOrder;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kGetRequestReceived;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kGetRequestSent;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kGetSearchProduct;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kGetShopDetails;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kGetShopList;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kGetShopProducts;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kGetShopRatingReview;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kGetShoppingCart;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kGetUserConnections;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kGetUserProfileDetails;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kGetValidatePromoCode;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kInviteContact;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kInviteFacebookMutualFriends;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kLikeDislikeBoard;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kLinkSocialAccount;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kLoginByOtp;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kManageCartQuantity;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kOnlineSearchFriends;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kRegisterUser;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kRejectConnectionRequest;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kRemoveBoard;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kRemoveComment;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kRemoveConnection;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kRemovePlacedOrder;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kRemoveShoppingCart;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kSaveComments;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kSendConnectionRequest;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kSetFavoriteMessage;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kSetFavouriteProduct;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kSetFavouriteShop;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kSetPaymentType;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kSetProductStatus;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kShareBoard;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kShareProductConnection;
import static com.app.inn.peepsin.Managers.APIManager.APIRequestHelper.kValidateUserCredentials;

/**
 * Created by mahesh on 21/4/17.
 * Singleton class to manage all models in projects. this is basically provide data to view in the
 * form of models
 */
public class ModelManager extends BaseManager implements Constants {

    private final static String TAG = ModelManager.class.getSimpleName();
    //Static Properties
    private static ModelManager _ModelManager;

    //Instance Properties
    private static CurrentUser mCurrentUser = null;
    private static String mGenericAuthToken = "";
    private DispatchQueue dispatchQueue =
            new DispatchQueue("com.queue.serial.modelmanager", DispatchQueue.QoS.userInitiated);

    /**
     * private constructor make it to be Singleton class
     */
    private ModelManager() {
    }

    /**
     * method to create a threadsafe singleton class instance
     *
     * @return a thread safe singleton object of model manager
     */
    public static synchronized ModelManager modelManager() {
        if (_ModelManager == null) {
            _ModelManager = new ModelManager();
            mCurrentUser = getDataFromPreferences(kCurrentUser, CurrentUser.class);
            mGenericAuthToken = getDataFromPreferences(kGenericAuthToken, String.class);
            Log.e(TAG, mCurrentUser + " ");
            Log.e(TAG, "genericAuthToken: " + mGenericAuthToken);
        }
        return _ModelManager;
    }

    public DispatchQueue getDispatchQueue() {
        return dispatchQueue;
    }

    /**
     * to initialize the singleton object
     */
    public void initializeModelManager() {
        System.out.println("ModelManager object initialized.");
    }

    /**
     * getter and setter method for current user
     *
     * @return {@link CurrentUser} if user already logged in, null otherwise
     */
    public synchronized CurrentUser getCurrentUser() {
        return mCurrentUser;
    }

    public synchronized void setCurrentUser(CurrentUser o) {
        mCurrentUser = o;
        archiveCurrentUser();
    }

    /**
     * set response to @User
     *
     * @param genricResponse contains JSONObject with user information in it
     */
    private void setupCurrentUser(GenricResponse<JSONObject> genricResponse) {
        mCurrentUser = new CurrentUser(genricResponse.getObject());
        archiveCurrentUser();
    }

    /**
     * Stores {@link CurrentUser} to the share preferences and synchronize sharedpreferece
     */
    public synchronized void archiveCurrentUser() {
        saveDataIntoPreferences(mCurrentUser, BaseModel.kCurrentUser);
    }

    /**
     * getter method for genericAuthToken
     */
    public synchronized String getGenericAuthToken() {
        return mGenericAuthToken;
    }

    public synchronized void setGenericAuthToken() {
        mGenericAuthToken = "";
        archiveGenericAuthToken();
    }

    /**
     * set response to mGenericAuthToken
     */
    private void setupGenericAuthToken(String response) {
        mGenericAuthToken = response;
        archiveGenericAuthToken();
    }

    /**
     * Stores genericAuthToken to the share preferences and synchronize SharedPreferences
     */
    public synchronized void archiveGenericAuthToken() {
        saveDataIntoPreferences(mGenericAuthToken, BaseModel.kGenericAuthToken);
    }


    /**
     * method will be called when  user register through system eg. with email and password
     *
     * @param parameters include user info provided by user
     * @param status     Block passed as callback for success condition
     * @param failure    Block passed as callback for failure condition
     */
    public void userSystemRegisteration(HashMap<String, Object> parameters, Block.Success<String> status, Block.Failure failure) {
        dispatchQueue.async(() ->
                APIManager.APIManager().processFormRequest(APIRequestHelper.kUserSystemRegistration, parameters, (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                    try {
                        JSONObject jsonObject = genricResponse.getObject();
                        String authToken = jsonObject.getString(kAuthToken);
                        GenricResponse<String> genericAuthToken = new GenricResponse<>(authToken);
                        DispatchQueue.main(() -> status.iSuccess(iStatus, genericAuthToken));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        DispatchQueue.main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                    }
                }, (Status statusFail, String message) -> {
                    DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                }));
    }

    /**
     * method will be called when  user login through system eg. with email and password
     *
     * @param parameters include user info provided by F, G, T accounts
     * @param success    Block passed as callback for success condition include Map<String,Object>
     * @param failure    Block passed as callback for failure condition
     */
    public void userSystemLogin(HashMap<String, Object> parameters,
                                Block.Success<Map<String, Object>> success, Block.Failure failure) {
        dispatchQueue.async(() -> APIManager.APIManager()
                .processRawRequest(APIRequestHelper.kUserSystemLogin, parameters,
                        (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                            JSONObject jsonObject = genricResponse.getObject();
                            String authToken = BaseModel.getValue(jsonObject, kAuthToken, String.class);
                            String email = BaseModel.getValue(jsonObject, kEmail, String.class);
                            Map<String, Object> map = new HashMap<>();
                            map.put(kAuthToken, authToken);
                            map.put(kEmail, email);

                            DispatchQueue.main(() -> success.iSuccess(iStatus, new GenricResponse<>(map)));
                        }, (Status statusFail, String message) -> {
                            DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                        }));
    }

    /**
     * method will be called when  user login through system eg. with email and password
     *
     * @param parameters include user info provided by F, G, T accounts
     * @param status     Block passed as callback for success condition include Map<String,Object>
     * @param failure    Block passed as callback for failure condition
     */
    public void registerDevice(HashMap<String, Object> parameters, Block.Status status,
                               Block.Failure failure) {
        dispatchQueue.async(() -> {
            APIManager.APIManager()
                    .processRawRequest(APIRequestHelper.kRegisterDevice, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                DispatchQueue.main(() -> status.iStatus(iStatus));
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * API will return the registered device details so that app can check the authentication of
     * device.
     * There is only one device per user at a time to maintain the consistency in the system.
     *
     * @param success Block passed as callback for success condition include Map<String,Object>
     * @param failure Block passed as callback for failure condition
     */
    public void getRegisterDevice(Block.Success<HashMap<String, Object>> success,
                                  Block.Failure failure) {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(kAuthToken, getCurrentUser().getAuthToken());
        dispatchQueue.async(() -> APIManager.APIManager()
                .processRawRequest(APIRequestHelper.kGetRegisteredDevice, parameters,
                        (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {

                        }, (Status statusFail, String message) -> {
                            DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                        }));
    }

    /**
     * method will be called when to get user profile,this method process
     * request with form-data as it may include file e.g. profile image
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getGenericAuthToken(Block.Success<String> success, Block.Failure failure) {
        HashMap<String, Object> parameters = new HashMap<>();
        dispatchQueue.async(() -> {
            APIManager.APIManager().processRawRequest(APIRequestHelper.kGetGenericAuthToken, parameters, (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                JSONObject jsonObject = genricResponse.getObject();
                try {
                    String genericAuthToken = jsonObject.getString(kGenericAuthToken);
                    setupGenericAuthToken(genericAuthToken);
                    GenricResponse<String> genricString = new GenricResponse<>(genericAuthToken);
                    DispatchQueue.main(() -> success.iSuccess(iStatus, genricString));
                } catch (JSONException e) {
                    e.printStackTrace();
                    DispatchQueue.main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                }

            }, (Status statusFail, String message) -> {
                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
            });
        });
    }

    /**
     * method will be called when to get user profile,this method process
     * request with form-data as it may include file e.g. profile image
     *
     * @param authtoken authtoken is required to get userprofile as a request
     * @param status    Block passed as callback for success condition
     * @param failure   Block passed as callback for failure condition
     */
    public void getMyProfile(String authtoken, Block.Status status, Block.Failure failure) {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(kAuthToken, authtoken);
        dispatchQueue.async(() -> {
            APIManager.APIManager().processRawRequest(APIRequestHelper.kGetMyProfileDetails, parameters, (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                setupCurrentUser(genricResponse);
                DispatchQueue.main(() -> status.iStatus(iStatus));
            }, (Status statusFail, String message) -> {
                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
            });
        });
    }

    /**
     * method will be called when to edit user profile,this method process
     * request with form-data as it may include file e.g. profile image
     * Request parameters are optional so user can send even a single entity.
     *
     * @param parameters contains fields to be edited
     * @param status     Block passed as callback for success condition
     * @param failure    Block passed as callback for failure condition
     */
    public void editMyProfile(HashMap<String, Object> parameters, Block.Status status, Block.Failure failure) {
        dispatchQueue.async(() -> {
            APIManager.APIManager().processFormRequest(APIRequestHelper.kEditMyProfileDetails, parameters, (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                try {
                    JSONObject jsonObject = genricResponse.getObject();
                    if (jsonObject.has(kPhone)) {
                        SocialLink phone = new SocialLink(jsonObject.getJSONObject(kPhone));
                        getCurrentUser().setPhone(phone);
                    }
                    if (jsonObject.has(kGender)) {
                        Integer gender = jsonObject.getInt(kGender);
                        getCurrentUser().setGender(gender);
                    }
                    if (jsonObject.has(kProfilePicUrl)) {
                        String profilePicURL = jsonObject.getString(kProfilePicUrl);
                        getCurrentUser().setProfilePicURL(profilePicURL);
                    }

                    if (jsonObject.has(kFirstName)) {
                        String firstName = jsonObject.getString(kFirstName);
                        getCurrentUser().setFirstName(firstName);
                    }

                    if (jsonObject.has(kLastName)) {
                        String lastName = jsonObject.getString(kLastName);
                        getCurrentUser().setLastName(lastName);
                    }

                    if (jsonObject.has(kDateOfBirth)) {
                        String dateOfBirth = jsonObject.getString(kDateOfBirth);
                        getCurrentUser().setDateOfBirth(dateOfBirth);
                    }
                    archiveCurrentUser();
                    DispatchQueue.main(() -> status.iStatus(iStatus));
                } catch (Exception e) {
                    e.printStackTrace();
                    DispatchQueue.main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                }
            }, (Status statusFail, String message) -> {
                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
            });
        });
    }

    /**
     * method will be called when  user login through social (Facebook, Google, Twitter)
     * this method process request with form-data as it may include file e.g. profile image
     *
     * @param parameters include user info provided by F, G, T accounts
     * @param success    Block passed as callback for success condition include Map<String,Object>
     * @param failure    Block passed as callback for failure condition
     */
    public void userSocialLogin(HashMap<String, Object> parameters, Block.Success<Map<String, Object>> success, Block.Failure failure) {
        dispatchQueue.async(() -> {
            APIManager.APIManager().processFormRequest(APIRequestHelper.kUserSocialLogin, parameters, (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                JSONObject jsonObject = genricResponse.getObject();
                String authToken = BaseModel.getValue(jsonObject, kAuthToken, String.class);
                String email = BaseModel.getValue(jsonObject, kEmail, String.class);
                int isNewUser = BaseModel.getValue(jsonObject, kIsNewUser, Integer.class);
                Map<String, Object> map = new HashMap<>();
                map.put(kAuthToken, authToken);
                map.put(kEmail, email);
                map.put(kIsNewUser, isNewUser);

                DispatchQueue.main(() -> success.iSuccess(iStatus, new GenricResponse<>(map)));
            }, (Status statusFail, String message) -> {
                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
            });
        });
    }

    /**
     * method will be called when  user login through social (Facebook, Google, Twitter)
     * this method process request with form-data as it may include file e.g. profile image
     *
     * @param success Block passed as callback for success condition include Map<String,Object>
     * @param failure Block passed as callback for failure condition
     */
    public void getMyAddress(Block.Success<CopyOnWriteArrayList<Address>> success, Block.Failure failure) {
        dispatchQueue.async(() -> {
            HashMap<String, Object> parameters = new HashMap<>();
            parameters.put(kAuthToken, getCurrentUser().getAuthToken());
            parameters.put(kUserId, getCurrentUser().getUserId());
            APIManager.APIManager().processRawRequest(APIRequestHelper.kGetUserAddresses, parameters, (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                JSONObject jsonObject;
                JSONArray jsonArray;
                CopyOnWriteArrayList<Address> addressList = new CopyOnWriteArrayList<>();
                try {
                    jsonObject = genricResponse.getObject();
                    jsonArray = jsonObject.getJSONArray(kRecords);

                    for (int i = 0; i < jsonArray.length(); i++) {
                        addressList.add(new Address(jsonArray.getJSONObject(i)));
                    }

                    GenricResponse<CopyOnWriteArrayList<Address>> genericList =
                            new GenricResponse<>(addressList);
                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericList));
                } catch (JSONException e) {
                    e.printStackTrace();
                    DispatchQueue.main(
                            () -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                }
            }, (Status statusFail, String message) -> {
                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
            });
        });
    }

    /**
     * method will be called for deletion of address
     *
     * @param success Block passed as callback for success condition include Map<String,Object>
     * @param failure Block passed as callback for failure condition
     */
    public void getDeleteAddress(int addressId, Block.Success<CopyOnWriteArrayList<Address>> success, Block.Failure failure) {
        dispatchQueue.async(() -> {
            HashMap<String, Object> parameters = new HashMap<>();
            parameters.put(kAuthToken, getCurrentUser().getAuthToken());
            parameters.put(kAddressId, addressId);
            APIManager.APIManager().processRawRequest(APIRequestHelper.kDeleteAddress, parameters, (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                JSONObject jsonObject;
                JSONArray jsonArray;
                CopyOnWriteArrayList<Address> addressList = new CopyOnWriteArrayList<>();
                try {
                    jsonObject = genricResponse.getObject();
                    jsonArray = jsonObject.getJSONArray(kRecords);

                    for (int i = 0; i < jsonArray.length(); i++) {
                        addressList.add(new Address(jsonArray.getJSONObject(i)));
                    }

                    GenricResponse<CopyOnWriteArrayList<Address>> genericList =
                            new GenricResponse<>(addressList);
                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericList));
                } catch (JSONException e) {
                    e.printStackTrace();
                    DispatchQueue.main(
                            () -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                }
            }, (Status statusFail, String message) -> {
                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
            });
        });
    }

    /**
     * method will be called when to get user profile,this method process
     * request with form-data as it may include file e.g. profile image
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getPrivacyPolicy(Block.Success<String> success, Block.Failure failure) {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(kAuthToken, getGenericAuthToken());
        dispatchQueue.async(() -> {
            APIManager.APIManager().processRawRequest(APIRequestHelper.kGetPrivacyPolicy, parameters, (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                JSONObject jsonObject = genricResponse.getObject();
                String formattedHtml = null;
                try {
                    formattedHtml = jsonObject.getString(kFormattedHtml);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                GenricResponse<String> genricString = new GenricResponse<String>(formattedHtml);
                DispatchQueue.main(() -> success.iSuccess(iStatus, genricString));
            }, (Status statusFail, String message) -> {
                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
            });
        });
    }

    /**
     * method will be called when to get user profile,this method process
     * request with form-data as it may include file e.g. profile image
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getTermsOfUse(Block.Success<String> success, Block.Failure failure) {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(kAuthToken, getGenericAuthToken());
        dispatchQueue.async(() -> {
            APIManager.APIManager().processRawRequest(APIRequestHelper.kGetTermsOfUse, parameters, (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                JSONObject jsonObject = genricResponse.getObject();
                String formattedHtml = null;
                try {
                    formattedHtml = jsonObject.getString(kFormattedHtml);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                GenricResponse<String> genericString = new GenricResponse<>(formattedHtml);
                DispatchQueue.main(() -> success.iSuccess(iStatus, genericString));
            }, (Status statusFail, String message) -> {
                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
            });
        });
    }

    /**
     * method will be called when to get user profile,this method process
     * request with form-data as it may include file e.g. profile image
     *
     * @param status  Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getDeactivateAccount(Block.Status status, Block.Failure failure) {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(kAuthToken, getCurrentUser().getAuthToken());
        dispatchQueue.async(() -> {
            APIManager.APIManager().processRawRequest(APIRequestHelper.kDeactivateAccount, parameters, (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                getCurrentUser().setAccountActivated(false);
                archiveCurrentUser();
                DispatchQueue.main(() -> status.iStatus(iStatus));
            }, (Status statusFail, String message) -> {
                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
            });
        });
    }

    /**
     * method will be called when to get user profile,this method process
     * request with form-data as it may include file e.g. profile image
     *
     * @param status  Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getReactivateAccount(Block.Status status, Block.Failure failure) {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(kAuthToken, getCurrentUser().getAuthToken());
        dispatchQueue.async(() -> {
            APIManager.APIManager().processRawRequest(APIRequestHelper.kReactivateAccount, parameters, (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                getCurrentUser().setAccountActivated(true);
                archiveCurrentUser();
                DispatchQueue.main(() -> status.iStatus(iStatus));
            }, (Status statusFail, String message) -> {
                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
            });
        });
    }

    /**
     * method will be called when to get user profile,this method process
     * request with form-data as it may include file e.g. profile image
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getPrivacySettings(Block.Success<HashMap<String, Object>> success, Block.Failure failure) {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(kAuthToken, getCurrentUser().getAuthToken());
        dispatchQueue.async(() -> {
            APIManager.APIManager().processRawRequest(APIRequestHelper.kGetMySettings, parameters, (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                try {
                    JSONObject jsonObject = genricResponse.getObject();
                    HashMap<String, Object> map = new HashMap<>();
                    map.put(kEmailVisibility, jsonObject.getInt(kEmailVisibility));
                    map.put(kPhoneVisibility, jsonObject.getInt(kPhoneVisibility));
                    map.put(kPrimaryLocationVisibility,
                            jsonObject.getInt(kPrimaryLocationVisibility));
                    map.put(kProfileVisibility, jsonObject.getInt(kProfileVisibility));
                    map.put(kSeeMyProducts, jsonObject.getInt(kSeeMyProducts));
                    map.put(kOtherProductVisibility, jsonObject.get(kOtherProductVisibility));
                    GenricResponse<HashMap<String, Object>> genericString = new GenricResponse<>(map);
                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericString));
                } catch (JSONException e) {
                    e.printStackTrace();
                    DispatchQueue.main(() -> failure.iFailure(iStatus, kMessageInternalInconsistency));
                }
            }, (Status statusFail, String message) -> {
                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
            });
        });
    }

    /**
     * method will be called when to get user profile,this method process
     * request with form-data as it may include file e.g. profile image
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void editPrivacySetting(HashMap<String, Object> parameters, Block.Success<HashMap<String, Object>> success, Block.Failure failure) {
        parameters.put(kAuthToken, getCurrentUser().getAuthToken());
        dispatchQueue.async(() -> {
            APIManager.APIManager().processRawRequest(APIRequestHelper.kEditMySettings, parameters,
                    (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                        try {
                            JSONObject jsonObject = genricResponse.getObject();
                            HashMap<String, Object> map = new HashMap<>();
                            map.put(kEmailVisibility, jsonObject.getInt(kEmailVisibility));
                            map.put(kPrimaryLocationVisibility,
                                    jsonObject.getInt(kPrimaryLocationVisibility));
                            map.put(kPhoneVisibility, jsonObject.getInt(kPhoneVisibility));
                            map.put(kProfileVisibility, jsonObject.getInt(kProfileVisibility));
                            map.put(kSeeMyProducts, jsonObject.getInt(kSeeMyProducts));
                            map.put(kOtherProductVisibility, jsonObject.get(kOtherProductVisibility));
                            GenricResponse<HashMap<String, Object>> genericString = new GenricResponse<>(map);
                            DispatchQueue.main(() -> success.iSuccess(iStatus, genericString));
                        } catch (JSONException e) {
                            e.printStackTrace();
                            DispatchQueue.main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                        }
                    }, (Status statusFail, String message) -> {
                        DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                    });
        });
    }

    /**
     * method will be called when to get user profile,this method process
     * request with form-data as it may include file e.g. profile image
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void blockUnblockUser(DataBaseHandler dbHandler, HashMap<String, Object> parameters, Block.Success<Integer> success, Block.Failure failure) {
        dispatchQueue.async(() -> {
            parameters.put(kAuthToken, getCurrentUser().getAuthToken());
            APIManager.APIManager().processRawRequest(APIRequestHelper.kBlockUnblockUser, parameters, (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                JSONObject jsonObject = genricResponse.getObject();
                try {
                    Integer isBlocked = jsonObject.getInt(kIsUserBlocked);
                    Integer userId = jsonObject.getInt(kUserId);
                    if (dbHandler != null) {
                        dbHandler.updateConnections(String.valueOf(userId),
                                String.valueOf(getCurrentUser().getUserId()), String.valueOf(isBlocked));
                    }
                    GenricResponse<Integer> generic = new GenricResponse<>(isBlocked);
                    for (Connection connection : getCurrentUser().getConnectionList()) {
                        if (connection.getUserId().equals(userId)) {
                            connection.setIsBlocked(false);
                        }
                    }

                    archiveCurrentUser();
                    DispatchQueue.main(() -> success.iSuccess(iStatus, generic));
                } catch (JSONException e) {
                    e.printStackTrace();
                    DispatchQueue.main(() -> failure.iFailure(iStatus, kMessageInternalInconsistency));
                }
            }, (Status statusFail, String message) -> {
                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
            });
        });
    }


    /**
     * method will be called when to get user profile,this method process
     * request with form-data as it may include file e.g. profile image
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getBlockedUserList(Block.Success<CopyOnWriteArrayList<BlockedUser>> success, Block.Failure failure) {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(kAuthToken, getCurrentUser().getAuthToken());
        dispatchQueue.async(() -> {
            APIManager.APIManager().processRawRequest(APIRequestHelper.kGetBlockedUserList, parameters, (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                try {
                    CopyOnWriteArrayList<BlockedUser> blockedUserList = new CopyOnWriteArrayList<>();
                    JSONObject jsonObject = genricResponse.getObject();
                    JSONArray jsonArray = jsonObject.getJSONArray(kBlockedUsers);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        blockedUserList.add(new BlockedUser(jsonArray.getJSONObject(i)));
                    }
                    GenricResponse<CopyOnWriteArrayList<BlockedUser>> genericList =
                            new GenricResponse<>(blockedUserList);
                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericList));
                } catch (JSONException e) {
                    e.printStackTrace();
                    DispatchQueue.main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                }
            }, (Status statusFail, String message) -> {
                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
            });
        });
    }


    /**
     * method will be to logout user from application
     *
     * @param status  Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getLogout(String firebaseToken, Block.Status status, Block.Failure failure) {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(kAuthToken, getCurrentUser().getAuthToken());
        parameters.put(kDeviceType, Constants.DeviceType.android.getValue());
        parameters.put(kAPNSToken, firebaseToken);
        dispatchQueue.async(() -> {
            APIManager.APIManager()
                    .processRawRequest(APIRequestHelper.kLogout, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                DispatchQueue.main(() -> status.iStatus(iStatus));
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * method will be called when to get user profile,this method process
     * request with form-data as it may include file e.g. profile image
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void blockUnblockUser(HashMap<String, Object> parameters, Block.Success<Integer> success, Block.Failure failure) {
        dispatchQueue.async(() -> {
            parameters.put(kAuthToken, getCurrentUser().getAuthToken());
            APIManager.APIManager().processRawRequest(APIRequestHelper.kBlockUnblockUser, parameters, (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                try {
                    JSONObject jsonObject = genricResponse.getObject();
                    Integer isBlocked = jsonObject.getInt(kIsUserBlocked);
                    Integer userId = jsonObject.getInt(kUserId);
                    GenricResponse<Integer> generic = new GenricResponse<>(isBlocked);
                    for (Connection connection : getCurrentUser().getConnectionList()) {
                        if (connection.getUserId().equals(userId)) {
                            connection.setIsBlocked(false);
                        }
                    }
                    archiveCurrentUser();
                    DispatchQueue.main(() -> success.iSuccess(iStatus, generic));
                } catch (JSONException e) {
                    e.printStackTrace();
                    DispatchQueue.main(
                            () -> failure.iFailure(iStatus, kMessageInternalInconsistency));
                }
            }, (Status statusFail, String message) -> {
                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
            });
        });
    }

    /**
     * method will be called when to get user profile,this method process
     * request with form-data as it may include file e.g. profile image
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getSearch(HashMap<String, Object> parameters,
                          Block.Success<CopyOnWriteArrayList<Search>> success, Block.Failure failure) {
        parameters.put(kAuthToken, getCurrentUser().getAuthToken());
        dispatchQueue.async(() -> {
            APIManager.APIManager()
                    .processRawRequest(APIRequestHelper.kSearch, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                try {
                                    JSONObject jsonObject = genricResponse.getObject();
                                    JSONArray jsonArray = jsonObject.getJSONArray(kProductCategory);
                                    CopyOnWriteArrayList<Search> searchList = new CopyOnWriteArrayList<>();
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        searchList.add(new Search(jsonArray.getJSONObject(i)));
                                    }
                                    GenricResponse<CopyOnWriteArrayList<Search>> genericList =
                                            new GenricResponse<>(searchList);
                                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericList));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    DispatchQueue.main(
                                            () -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                                }
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * API will return the search products list. Sort date/time wise,
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getSearchProduct(HashMap<String, Object> parameters,
                                 Block.Success<CopyOnWriteArrayList<Feeds>> success, Block.Failure failure) {
        parameters.put(kAuthToken, getCurrentUser().getAuthToken());
        dispatchQueue.async(() -> {
            APIManager.APIManager()
                    .processRawRequest(kGetSearchProduct, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                JSONObject jsonObject = genricResponse.getObject();
                                CopyOnWriteArrayList<Feeds> feedsList = new CopyOnWriteArrayList<>();
                                JSONArray jsonArray;
                                try {
                                    jsonArray = jsonObject.getJSONArray(kRecords);
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        feedsList.add(new Feeds(jsonArray.getJSONObject(i)));
                                    }
                                    GenricResponse<CopyOnWriteArrayList<Feeds>> genericFeeds =
                                            new GenricResponse<>(feedsList);
                                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericFeeds));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    DispatchQueue.main(
                                            () -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                                }
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * method will be called when  user login through social (Facebook, Google, Twitter)
     * this method process request with form-data as it may include file e.g. profile image
     *
     * @param status  Block passed as callback for success condition include Map<String,Object>
     * @param failure Block passed as callback for failure condition
     */
    public void addNewAddress(HashMap<String, Object> parameters, Block.Status status, Block.Failure failure) {
        dispatchQueue.async(() -> {
            APIManager.APIManager().processRawRequest(APIRequestHelper.kAddNewAddress, parameters, (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                try {
                    JSONObject jsonObject = genricResponse.getObject();
                    Address address = new Address(jsonObject);
                    if (address.getPrimary()) {
                        getCurrentUser().setPrimaryAddress(address);
                        archiveCurrentUser();
                    }
                    DispatchQueue.main(() -> status.iStatus(iStatus));
                } catch (Exception e) {
                    e.printStackTrace();
                    DispatchQueue.main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                }
            }, (Status statusFail, String message) -> {
                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
            });
        });
    }

    /**
     * method will be called when  user login through social (Facebook, Google, Twitter)
     * this method process request with form-data as it may include file e.g. profile image
     *
     * @param status  Block passed as callback for success condition include Map<String,Object>
     * @param failure Block passed as callback for failure condition
     */
    public void editAddress(HashMap<String, Object> parameters, Block.Status status, Block.Failure failure) {
        dispatchQueue.async(() ->
                APIManager.APIManager().processRawRequest(APIRequestHelper.kEditAddress, parameters, (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                    try {
                        JSONObject jsonObject = genricResponse.getObject();
                        Address address = new Address(jsonObject);
                        if (address.getPrimary()) {
                            getCurrentUser().setPrimaryAddress(address);
                            archiveCurrentUser();
                        }
                        DispatchQueue.main(() -> status.iStatus(iStatus));
                    } catch (Exception e) {
                        e.printStackTrace();
                        DispatchQueue.main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                    }
                }, (Status statusFail, String message) -> {
                    DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                }));
    }

    /**
     * This method is used to update the user current location so that app can search nearby shops,
     * can show People May Know etc. This will not be the user address only app will use it to show
     * the more appropriate result.
     *
     * @param status  Block passed as callback for success condition include Map<String,Object>
     * @param failure Block passed as callback for failure condition
     */
    public void getUserLocation(Block.Success<UserLocation> status, Block.Failure failure) {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(BaseModel.kAuthToken, getCurrentUser().getAuthToken());
        parameters.put(BaseModel.kUserId, getCurrentUser().getUserId());
        dispatchQueue.async(() -> APIManager.APIManager()
                .processRawRequest(APIRequestHelper.kGetUserLocation, parameters,
                        (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                            try {
                                JSONObject jsonObject = genricResponse.getObject();
                                UserLocation address = new UserLocation(jsonObject);
                                getCurrentUser().setUserLocation(address);
                                archiveCurrentUser();
                                GenricResponse<UserLocation> generic = new GenricResponse<>(address);
                                DispatchQueue.main(() -> status.iSuccess(iStatus, generic));
                            } catch (Exception e) {
                                e.printStackTrace();
                                DispatchQueue
                                        .main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                            }
                        }, (Status statusFail, String message) -> {
                            DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                        }));
    }


    /**
     * This method is used to update the user current location so that app can search nearby shops,
     * can show People May Know etc. This will not be the user address only app will use it to show
     * the more appropriate result.
     *
     * @param status  Block passed as callback for success condition include Map<String,Object>
     * @param failure Block passed as callback for failure condition
     */
    public void updateUserLocation(HashMap<String, Object> parameters, Block.Status status,
                                   Block.Failure failure) {
        dispatchQueue.async(() -> APIManager.APIManager()
                .processRawRequest(APIRequestHelper.kUpdateUserLocation, parameters,
                        (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                            try {
                                JSONObject jsonObject = genricResponse.getObject();
                                UserLocation address = new UserLocation(jsonObject);
                                getCurrentUser().setUserLocation(address);
                                archiveCurrentUser();
                                DispatchQueue.main(() -> status.iStatus(iStatus));
                            } catch (Exception e) {
                                e.printStackTrace();
                                DispatchQueue
                                        .main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                            }
                        }, (Status statusFail, String message) -> {
                            DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                        }));
    }

    /**
     * method will be called when to get user profile,this method process
     * request with form-data as it may include file e.g. profile image
     *
     * @param contactId is required to get otp
     * @param success   Block passed as callback for success condition
     * @param failure   Block passed as callback for failure condition
     */
    public void getForgotPasswordOTP(String contactId, Block.Success<HashMap<String, Object>> success,
                                     Block.Failure failure) {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(kContactId, contactId);
        dispatchQueue.async(() -> {
            APIManager.APIManager()
                    .processRawRequest(APIRequestHelper.kGetForgotPasswordOTP, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {

                                try {
                                    JSONObject jsonObject = genricResponse.getObject();
                                    HashMap<String, Object> otpMap = new HashMap<>();
                                    otpMap.put(kOTP, jsonObject.getString(kOTP));

                                    GenricResponse<HashMap<String, Object>> genericOTP = new GenricResponse<>(otpMap);

                                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericOTP));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    DispatchQueue.main(() -> failure.iFailure(iStatus, e.getMessage()));
                                }
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * method will be called when to get user profile,this method process
     * request with form-data as it may include file e.g. profile image
     *
     * @param parameters is required to get otp it includes contactId, password, otp
     * @param status     Block passed as callback for success condition
     * @param failure    Block passed as callback for failure condition
     */
    public void resetPassword(HashMap<String, Object> parameters, Block.Status status,
                              Block.Failure failure) {
        dispatchQueue.async(() -> {
            APIManager.APIManager()
                    .processRawRequest(APIRequestHelper.kResetPassword, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {

                                DispatchQueue.main(() -> status.iStatus(iStatus));
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * method to cancel request sent
     *
     * @param contactId list of contacts
     * @param status    Block passed as callback for success condition
     * @param failure   Block passed as callback for failure condition
     */
    public void requestSentCancel(DataBaseHandler dbHandler, String contactId, int requestSentMode,
                                  Block.Status status, Block.Failure failure) {
        dispatchQueue.async(() -> {
            final HashMap<String, Object> parameters = new HashMap<>();
            String contact = (contactId.isEmpty()) ? "nothing" : contactId;
            parameters.put(BaseModel.kContactId, contact);
            parameters.put(BaseModel.kRequestSentMode, requestSentMode);
            parameters.put(BaseModel.kAuthToken, getCurrentUser().getAuthToken());
            APIManager.APIManager()
                    .processRawRequest(kCancelConnectionRequest, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {

                                //updating db
                                if (dbHandler != null) {
                                    dbHandler.updatePhoneContacts(String.valueOf(mCurrentUser.getUserId()), contactId,
                                            "0");
                                    dbHandler.updateGoogleContacts(String.valueOf(mCurrentUser.getUserId()),
                                            contactId, "0");
                                    dbHandler.updateFacebookContacts(String.valueOf(mCurrentUser.getUserId()),
                                            contactId, "0");
                                }

                                DispatchQueue.main(() -> status.iStatus(iStatus));
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }


    /**
     * method to invite phone contacts to connect
     *
     * @param contactList list of contacts
     * @param status      Block passed as callback for success condition
     * @param failure     Block passed as callback for failure condition
     */
    public void invitePhoneContact(DataBaseHandler dataBaseHandler,
                                   CopyOnWriteArrayList<PhoneContact> contactList, Block.Status status, Block.Failure failure) {
        dispatchQueue.async(() -> {
            final HashMap<String, Object> parameters = new HashMap<>();
            parameters.put(BaseModel.kAuthToken, mCurrentUser.getAuthToken());
            parameters.put(BaseModel.kContactList, Utils.generateJsonArrayFromPhoneList(contactList));
            APIManager.APIManager()
                    .processRawRequest(kInviteContact, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                for (PhoneContact contact : contactList) {
                                    dataBaseHandler.updatePhoneContacts(String.valueOf(mCurrentUser.getUserId()),
                                            contact.getContactId(), "1");
                                }
                                DispatchQueue.main(() -> status.iStatus(iStatus));
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * method to invite google contacts to connect
     *
     * @param contactList list of contacts
     * @param status      Block passed as callback for success condition
     * @param failure     Block passed as callback for failure condition
     */
    public void inviteGoogleContact(DataBaseHandler dataBaseHandler,
                                    CopyOnWriteArrayList<GoogleContact> contactList, Block.Status status, Block.Failure failure) {
        dispatchQueue.async(() -> {
            final HashMap<String, Object> parameters = new HashMap<>();
            parameters.put(BaseModel.kAuthToken, mCurrentUser.getAuthToken());
            parameters.put(BaseModel.kContactList, Utils.generateJsonArrayFromGoogleList(contactList));
            APIManager.APIManager()
                    .processRawRequest(kInviteContact, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                for (GoogleContact contact : contactList) {
                                    dataBaseHandler.updateGoogleContacts(String.valueOf(mCurrentUser.getUserId()),
                                            contact.getContactId(), "1");
                                }
                                DispatchQueue.main(() -> status.iStatus(iStatus));
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * method to invite facebook contacts to connect
     *
     * @param contact list of contacts
     * @param status  Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void inviteFacebookContact(DataBaseHandler dataBaseHandler, FacebookContact contact,
                                      Block.Status status, Block.Failure failure) {
        dispatchQueue.async(() -> {
            final HashMap<String, Object> parameters = new HashMap<>();
            parameters.put(BaseModel.kAuthToken, mCurrentUser.getAuthToken());
            parameters.put(BaseModel.kContactList, Utils.generateJsonArrayFromFacebookList(contact));
            APIManager.APIManager()
                    .processRawRequest(kInviteFacebookMutualFriends, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                dataBaseHandler.updateFacebookContacts(String.valueOf(mCurrentUser.getUserId()),
                                        contact.getId(), "1");
                                DispatchQueue.main(() -> status.iStatus(iStatus));
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * method to reject the request received by user
     *
     * @param status  Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void requestReceivedReject(Integer userId, Block.Status status, Block.Failure failure) {
        dispatchQueue.async(() -> {
            final HashMap<String, Object> parameters = new HashMap<>();
            parameters.put(BaseModel.kAuthToken, getCurrentUser().getAuthToken());
            parameters.put(BaseModel.kUserId, userId);
            APIManager.APIManager()
                    .processRawRequest(kRejectConnectionRequest, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                DispatchQueue.main(() -> status.iStatus(iStatus));
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * method to accept the request received by user
     *
     * @param status  Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void requestReceivedAccept(Integer userId, Block.Status status, Block.Failure failure) {
        dispatchQueue.async(() -> {
            final HashMap<String, Object> parameters = new HashMap<>();
            parameters.put(BaseModel.kAuthToken, getCurrentUser().getAuthToken());
            parameters.put(BaseModel.kUserId, userId);
            APIManager.APIManager().processRawRequest(kAcceptConnectionRequest, parameters, (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                int friendsCount = getCurrentUser().getFriendsCount();
                getCurrentUser().setFriendsCount(++friendsCount);
                archiveCurrentUser();
                DispatchQueue.main(() -> status.iStatus(iStatus));
            }, (Status statusFail, String message) -> {
                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
            });
        });
    }

    /**
     * method to get the connection request received by the logged in user.
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getRequestReceived(Block.Success<CopyOnWriteArrayList<Requests>> success, Block.Failure failure) {
        dispatchQueue.async(() -> {
            final HashMap<String, Object> parameters = new HashMap<>();
            if (mCurrentUser == null) {
                return;
            }
            parameters.put(BaseModel.kAuthToken, mCurrentUser.getAuthToken());
            parameters.put(BaseModel.kPage, 1);
            APIManager.APIManager()
                    .processRawRequest(kGetRequestReceived, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                JSONObject jsonObject = genricResponse.getObject();
                                CopyOnWriteArrayList<Requests> requestsList = new CopyOnWriteArrayList<>();
                                JSONArray jsonArray;
                                try {
                                    jsonArray = jsonObject.getJSONArray(kConnectionRequestReceived);
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        requestsList.add(new Requests(jsonArray.getJSONObject(i)));
                                    }

                                    GenricResponse<CopyOnWriteArrayList<Requests>> genericList =
                                            new GenricResponse<>(requestsList);
                                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericList));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    DispatchQueue.main(
                                            () -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                                }
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * method to get the connection request sent by the logged in user.
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getRequestSent(Block.Success<CopyOnWriteArrayList<Requests>> success, Block.Failure failure) {
        dispatchQueue.async(() -> {
            final HashMap<String, Object> parameters = new HashMap<>();
            parameters.put(BaseModel.kAuthToken, getCurrentUser().getAuthToken());
            parameters.put(BaseModel.kPage, 1);
            APIManager.APIManager().processRawRequest(kGetRequestSent, parameters, (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                try {
                    JSONObject jsonObject = genricResponse.getObject();
                    CopyOnWriteArrayList<Requests> requestsList = new CopyOnWriteArrayList<>();
                    JSONArray jsonArray = jsonObject.getJSONArray(kConnectionRequestSent);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        requestsList.add(new Requests(jsonArray.getJSONObject(i)));
                    }
                    GenricResponse<CopyOnWriteArrayList<Requests>> genericList = new GenricResponse<>(requestsList);
                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericList));
                } catch (Exception e) {
                    e.printStackTrace();
                    DispatchQueue.main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                }
            }, (Status statusFail, String message) -> {
                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
            });
        });
    }

    /**
     * method to get user connections
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */

    public void getMyConnections(DataBaseHandler dataBaseHandler, Block.Success<CopyOnWriteArrayList<Connection>> success, Block.Failure failure) {
        dispatchQueue.async(() -> {
            HashMap<String, Object> parameters = new HashMap<>();
            parameters.put(kAuthToken, getCurrentUser().getAuthToken());
            parameters.put(kUserId, getCurrentUser().getUserId());
            APIManager.APIManager().processRawRequest(kGetUserConnections, parameters, (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                try {
                    JSONObject jsonObject = genricResponse.getObject();
                    CopyOnWriteArrayList<Connection> connectionList = new CopyOnWriteArrayList<>();
                    JSONArray jsonArray;
                    jsonArray = jsonObject.getJSONArray(kConnections);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        connectionList.add(new Connection(jsonArray.getJSONObject(i)));
                        String address =
                                connectionList.get(i).getPrimaryAddress().getStreetAddress() + ","
                                        + connectionList.get(i).getPrimaryAddress().getCity() + "," +
                                        connectionList.get(i).getPrimaryAddress().getState() + ","
                                        + connectionList.get(i).getPrimaryAddress().getCountry();
                        if (!address.equalsIgnoreCase("") && !address.equalsIgnoreCase(",,,")) {
                            address = address;
                        } else {
                            address = "No Location Available";
                        }
                        if (!dataBaseHandler
                                .isConnectionUserExist(String.valueOf(getCurrentUser().getUserId()),
                                        String.valueOf(connectionList.get(i).getUserId()))) {
                            dataBaseHandler.addConnections(String.valueOf(getCurrentUser().getUserId()),
                                    String.valueOf(connectionList.get(i).getUserId()),
                                    String.valueOf(connectionList.get(i).getFirstName()),
                                    String.valueOf(connectionList.get(i).getLastName()),
                                    String.valueOf(connectionList.get(i).getProfilePicURL()),
                                    String.valueOf(connectionList.get(i).getSellingProductCount()),
                                    String.valueOf(connectionList.get(i).getFriendsCount()),
                                    String.valueOf(connectionList.get(i).getUserBlocked()),
                                    String.valueOf(connectionList.get(i).getConnectionLevel()),
                                    String.valueOf(connectionList.get(i).getDateOfBirth()),
                                    String.valueOf(connectionList.get(i).getJoinedDate()), address,
                                    String.valueOf(connectionList.get(i).getShopName()),
                                    String.valueOf(connectionList.get(i).getRating()),
                                    String.valueOf(connectionList.get(i).getShopId()));
                        }
                    }

                    int size = connectionList.size();
                    if (getCurrentUser().getFriendsCount() != size) {
                        getCurrentUser().setFriendsCount(size);
                        archiveCurrentUser();
                    }
                    GenricResponse<CopyOnWriteArrayList<Connection>> genericConnection = new GenricResponse<>(connectionList);
                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericConnection));
                } catch (Exception e) {
                    e.printStackTrace();
                    DispatchQueue.main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                }
            }, (Status statusFail, String message) -> {
                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
            });
        });
    }

    /**
     * method to get user connections
     *
     * @param parameters contains userId, and authToken
     * @param success    Block passed as callback for success condition
     * @param failure    Block passed as callback for failure condition
     */
    public void getUserConnections(HashMap<String, Object> parameters, Block.Success<CopyOnWriteArrayList<Connection>> success, Block.Failure failure) {
        dispatchQueue.async(() -> {
            parameters.put(kAuthToken, getCurrentUser().getAuthToken());
            APIManager.APIManager().processRawRequest(kGetUserConnections, parameters, (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                JSONObject jsonObject = genricResponse.getObject();
                CopyOnWriteArrayList<Connection> connectionList = new CopyOnWriteArrayList<>();
                JSONArray jsonArray;
                try {
                    jsonArray = jsonObject.getJSONArray(kConnections);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        connectionList.add(new Connection(jsonArray.getJSONObject(i)));
                    }
                    GenricResponse<CopyOnWriteArrayList<Connection>> genericConnection =
                            new GenricResponse<>(connectionList);
                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericConnection));
                } catch (Exception e) {
                    e.printStackTrace();
                    DispatchQueue.main(
                            () -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                }
            }, (Status statusFail, String message) -> {
                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
            });
        });
    }

    /**
     * method to get user connections
     *
     * @param parameters contains userId, and authToken
     * @param success    Block passed as callback for success condition
     * @param failure    Block passed as callback for failure condition
     */
    public void getUserProfile(HashMap<String, Object> parameters, Block.Success<Connection> success, Block.Failure failure) {
        dispatchQueue.async(() -> {
            parameters.put(kAuthToken, getCurrentUser().getAuthToken());
            APIManager.APIManager().processRawRequest(kGetUserProfileDetails, parameters, (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                try {
                    JSONObject jsonObject = genricResponse.getObject();
                    Connection connection = new Connection(jsonObject);
                    GenricResponse<Connection> genericConnection = new GenricResponse<>(connection);
                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericConnection));
                } catch (Exception e) {
                    e.printStackTrace();
                    DispatchQueue.main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                }
            }, (Status statusFail, String message) -> {
                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
            });
        });
    }

    /**
     * method will be  used to unfriend the user or remove connection.
     * As user remove from connection, app will remove it from database.
     *
     * @param userId  userId of the user whom you want to remove contactId	(to which request
     *                sent)	(Optional) But one is necessary userId	(to which request sent)	(Optional) But one is
     *                necessary
     * @param status  Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */

    public void removeUserConnection(DataBaseHandler dbHandler, int userId, Block.Status status, Block.Failure failure) {
        dispatchQueue.async(() -> {
            HashMap<String, Object> parameters = new HashMap<>();
            parameters.put(kUserId, userId);
            parameters.put(kAuthToken, getCurrentUser().getAuthToken());
            APIManager.APIManager().processRawRequest(kRemoveConnection, parameters, (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                //updating db
                if (dbHandler != null) {
                    dbHandler.updateConnections(String.valueOf(userId),
                            String.valueOf(getCurrentUser().getUserId()), "1");
                }
                //reducing friends count by one
                int friendsCount = getCurrentUser().getFriendsCount();
                getCurrentUser().setFriendsCount(--friendsCount);
                archiveCurrentUser();
                archiveCurrentUser();
                DispatchQueue.main(() -> status.iStatus(iStatus));
            }, (Status statusFail, String message) -> {
                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
            });
        });
    }


    /**
     * method to get the list of product feedList by the logged in user.
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getProductFeeds(int shopId, int categoryId, int page, Block.Success<HashMap<String, Object>> success, Block.Failure failure) {
        final HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(BaseModel.kAuthToken, getCurrentUser().getAuthToken());
        parameters.put(BaseModel.kShopId, shopId);
        parameters.put(kPage, 0);
        parameters.put(BaseModel.kCategoryId, categoryId);
        dispatchQueue.async(() -> {
            APIManager.APIManager().processRawRequest(kGetProductFeeds, parameters, (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                JSONObject jsonObject = genricResponse.getObject();
                HashMap<String, Object> feedMap = new HashMap<>();
                CopyOnWriteArrayList<Feeds> feedsList = new CopyOnWriteArrayList<>();
                try {

                    JSONArray jsonArray = jsonObject.getJSONArray(kRecords);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        Feeds feeds = new Feeds(jsonArray.getJSONObject(i));
                        feedsList.add(feeds);
                        readyPicasso(feeds.getImageThumbUrl());
                    }
                    Shop shop = new Shop(jsonObject.getJSONObject(kShopDetails));
                    feedMap.put(kShopDetails, shop);
                    feedMap.put(kFeeds, feedsList);
                    //saveFeeds(feedsList);
                    GenricResponse<HashMap<String, Object>> genericFeeds =
                            new GenricResponse<>(feedMap);
                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericFeeds));
                } catch (Exception e) {
                    e.printStackTrace();
                    DispatchQueue.main(
                            () -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                }
            }, (Status statusFail, String message) -> {
                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
            });
        });
    }

    /**
     * method to get the product details of feed by the logged in user.
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getProductDetails(int productId, Block.Success<Feeds> success,
                                  Block.Failure failure) {
        final HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(BaseModel.kAuthToken, getCurrentUser().getAuthToken());
        parameters.put(BaseModel.kProductId, productId);
        dispatchQueue.async(() -> {
            APIManager.APIManager().processRawRequest(kGetProductDetails, parameters, (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                try {
                    JSONObject jsonObject = genricResponse.getObject();
                    Feeds feed = new Feeds(jsonObject);
                    GenricResponse<Feeds> genericFeed = new GenricResponse<>(feed);
                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericFeed));
                } catch (Exception e) {
                    e.printStackTrace();
                    DispatchQueue.main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                }
            }, (Status statusFail, String message) -> {
                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
            });
        });
    }

    /**
     * method to set favourite and un-favourite product by the logged in user.
     *
     * @param status  Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void setFavouriteProduct(int productId, int favourite, Block.Status status, Block.Failure failure) {
        dispatchQueue.async(() -> {
            final HashMap<String, Object> parameters = new HashMap<>();
            parameters.put(BaseModel.kAuthToken, getCurrentUser().getAuthToken());
            parameters.put(BaseModel.kProductId, productId);
            parameters.put(BaseModel.kIsFavorite, favourite);
            APIManager.APIManager().processRawRequest(kSetFavouriteProduct, parameters, (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                try {
                    JSONObject jsonObject = genricResponse.getObject();
                    Integer fav = jsonObject.getInt(kIsFavorite);
                    Integer productID = jsonObject.getInt(kProductId);
                    Integer favCount = getCurrentUser().getFavoriteProductCount();
                    /*CopyOnWriteArrayList<Feeds> feeds = getFeedList();
                    for (Feeds feed: feeds) {
                        if(feed.getId().equals(productID)){
                            feed.setFavourite(fav==1);
                        }
                    }
                    getCurrentUser().setFeedsList(feeds);*/
                    if (fav == 1) {
                        ++favCount;
                    } else {
                        --favCount;
                    }
                    getCurrentUser().setFavoriteProductCount(favCount);
                    archiveCurrentUser();
                    DispatchQueue.main(() -> status.iStatus(iStatus));
                } catch (JSONException e) {
                    e.printStackTrace();
                    DispatchQueue.main(() -> failure.iFailure(iStatus, kMessageInternalInconsistency));
                }
            }, (Status statusFail, String message) -> {
                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
            });
        });
    }

    /**
     * method to share the product detail feed with other connection in list
     *
     * @param status  Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getShareProduct(int productId, int share2ndLevel, int share3rdLevel, CopyOnWriteArrayList<Integer> userIdList, Block.Status status, Block.Failure failure) {
        final HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(BaseModel.kAuthToken, getCurrentUser().getAuthToken());
        parameters.put(BaseModel.kProductId, productId);
        parameters.put(BaseModel.kUserIdList, Utils.generateJsonList(userIdList));
        parameters.put(BaseModel.kWantToShare2ndLevel, share2ndLevel);
        parameters.put(BaseModel.kWantToShare3ndLevel, share3rdLevel);
        dispatchQueue.async(() -> {
            APIManager.APIManager().processRawRequest(kShareProductConnection, parameters, (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                JSONObject jsonObject = genricResponse.getObject();

                DispatchQueue.main(() -> status.iStatus(iStatus));
            }, (Status statusFail, String message) -> {
                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
            });
        });
    }

    /**
     * method to get the list of shared products by other user in app.
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getSharedProductList(int page,
                                     Block.Success<CopyOnWriteArrayList<SharedProduct>> success, Block.Failure failure) {
        final HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(BaseModel.kAuthToken, getCurrentUser().getAuthToken());
        parameters.put(BaseModel.kPage, page);

        dispatchQueue.async(() -> {
            APIManager.APIManager()
                    .processRawRequest(kGetMySharedProduct, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                try {
                                    JSONObject jsonObject = genricResponse.getObject();
                                    CopyOnWriteArrayList<SharedProduct> productList = new CopyOnWriteArrayList<>();
                                    JSONArray jsonArray = jsonObject.getJSONArray(kRecords);
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        SharedProduct products = new SharedProduct(jsonArray.getJSONObject(i));
                                        productList.add(products);
                                        readyPicasso(products.getImageThumbUrl());
                                    }
                                    getCurrentUser().setRecommendedList(productList);
                                    archiveCurrentUser();
                                    GenricResponse<CopyOnWriteArrayList<SharedProduct>> genericProducts =
                                            new GenricResponse<>(productList);
                                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericProducts));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    DispatchQueue.main(
                                            () -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                                }
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * method to get the list of shared product feedList by the logged in user.
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getSharedProducts(HashMap<String, Object> parameters,
                                  Block.Success<CopyOnWriteArrayList<Feeds>> success, Block.Failure failure) {
        dispatchQueue.async(() -> {
            APIManager.APIManager()
                    .processRawRequest(APIRequestHelper.kGetShareProductRecommended, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                JSONObject jsonObject = genricResponse.getObject();
                                CopyOnWriteArrayList<Feeds> feedsList = new CopyOnWriteArrayList<>();
                                JSONArray jsonArray;
                                try {
                                    jsonArray = jsonObject.getJSONArray(kRecords);
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        feedsList.add(new Feeds(jsonArray.getJSONObject(i)));
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                GenricResponse<CopyOnWriteArrayList<Feeds>> genericFeedList =
                                        new GenricResponse<>(feedsList);

                                DispatchQueue.main(() -> success.iSuccess(iStatus, genericFeedList));
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * method to get the list of favourite product feedList of the user logged in .
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getFavouriteProducts(Block.Success<CopyOnWriteArrayList<Feeds>> success, Block.Failure failure) {
        final HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(BaseModel.kAuthToken, getCurrentUser().getAuthToken());
        dispatchQueue.async(() -> {
            APIManager.APIManager().processRawRequest(APIRequestHelper.kGetFavProductFeeds, parameters, (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                try {
                    JSONObject jsonObject = genricResponse.getObject();
                    CopyOnWriteArrayList<Feeds> feedsList = new CopyOnWriteArrayList<>();
                    JSONArray jsonArray = jsonObject.getJSONArray(kFavProducts);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        feedsList.add(new Feeds(jsonArray.getJSONObject(i)));
                    }
                    int size = feedsList.size();
                    if (size != getCurrentUser().getFavoriteProductCount()) {
                        getCurrentUser().setFavoriteProductCount(size);
                        archiveCurrentUser();
                    }
                    GenricResponse<CopyOnWriteArrayList<Feeds>> genericFeedList = new GenricResponse<>(feedsList);
                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericFeedList));
                } catch (Exception e) {
                    e.printStackTrace();
                    DispatchQueue.main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                }
            }, (Status statusFail, String message) -> {
                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
            });
        });
    }

    /**
     * method to get the list of selling product feedList of the user logged in
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getSellingProducts(HashMap<String, Object> parameters, Block.Success<CopyOnWriteArrayList<SellingProduct>> success, Block.Failure failure) {
        parameters.put(BaseModel.kAuthToken, getCurrentUser().getAuthToken());
        dispatchQueue.async(() -> {
            APIManager.APIManager().processRawRequest(APIRequestHelper.kGetSellingProducts, parameters, (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                try {
                    JSONObject jsonObject = genricResponse.getObject();
                    CopyOnWriteArrayList<SellingProduct> sellList = new CopyOnWriteArrayList<>();
                    JSONArray jsonArray = jsonObject.getJSONArray(kRecords);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        sellList.add(new SellingProduct(jsonArray.getJSONObject(i)));
                    }
                    int size = sellList.size();
                    if (getCurrentUser().getSellingProductCount() != size) {
                        getCurrentUser().setSellingProductCount(size);
                        archiveCurrentUser();
                    }
                    GenricResponse<CopyOnWriteArrayList<SellingProduct>> genericFeedList = new GenricResponse<>(sellList);
                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericFeedList));
                } catch (Exception e) {
                    e.printStackTrace();
                    DispatchQueue.main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                }
            }, (Status statusFail, String message) -> {
                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
            });
        });
    }

    /**
     * method to get add a product feedList to selling list of the user logged in .
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void addSellingProduct(HashMap<String, Object> parameters, Block.Success<SellingProduct> success, Block.Failure failure) {
        parameters.put(kAuthToken, getCurrentUser().getAuthToken());
        parameters.put(kTaxList, Utils.generateTAXJsonList(getCurrentUser().getTaxList()));
        dispatchQueue.async(() -> {
            APIManager.APIManager().processFormRequest(APIRequestHelper.kAddSellingProduct, parameters, (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                try {
                    JSONObject jsonObject = genricResponse.getObject();
                    SellingProduct feed = new SellingProduct(jsonObject);
                    GenricResponse<SellingProduct> genericFeed = new GenricResponse<>(feed);
                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericFeed));
                } catch (Exception e) {
                    e.printStackTrace();
                    DispatchQueue.main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                }
            }, (Status statusFail, String message) -> {
                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
            });
        });
    }

    /**
     * method to edit the product feedList that has been added by the user logged in .
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void editSellingProduct(HashMap<String, Object> parameters, Block.Success<SellingProduct> success, Block.Failure failure) {
        parameters.put(kAuthToken, getCurrentUser().getAuthToken());
        parameters.put(kTaxList, Utils.generateTAXJsonList(getCurrentUser().getTaxList()));
        dispatchQueue.async(() -> {
            APIManager.APIManager().processFormRequest(APIRequestHelper.kEditSellingProduct, parameters, (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                try {
                    SellingProduct feed = new SellingProduct(genricResponse.getObject());
                    GenricResponse<SellingProduct> genericFeed = new GenricResponse<>(feed);

                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericFeed));
                } catch (Exception e) {
                    e.printStackTrace();
                    DispatchQueue.main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                }
            }, (Status statusFail, String message) -> {
                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
            });
        });
    }

    /**
     * method to get the list of product category by the logged in user.
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getProductCategory(Block.Success<CopyOnWriteArrayList<ShopCategory>> success,
                                   Block.Failure failure) {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(kAuthToken, mCurrentUser.getAuthToken());
        dispatchQueue.async(() -> {
            APIManager.APIManager()
                    .processRawRequest(APIRequestHelper.kGetProductCategory, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                try {
                                    JSONObject jsonObject = genricResponse.getObject();
                                    CopyOnWriteArrayList<ShopCategory> categoryList = new CopyOnWriteArrayList<>();
                                    JSONArray jsonArray = jsonObject.getJSONArray(kProductCategory);
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        ShopCategory shopCategory = new ShopCategory(jsonArray.getJSONObject(i));
                                        categoryList.add(shopCategory);
                                    }
                                    GenricResponse<CopyOnWriteArrayList<ShopCategory>> genericFeedList =
                                            new GenricResponse<>(categoryList);
                                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericFeedList));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    DispatchQueue.main(
                                            () -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                                }
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * method to get the list of product sub-category by the logged in user.
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getProductSubCategory(Integer categoryId,
                                      Block.Success<CopyOnWriteArrayList<SubCategory>> success, Block.Failure failure) {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(kAuthToken, mCurrentUser.getAuthToken());
        parameters.put(kCategoryId, categoryId);
        dispatchQueue.async(() -> {
            APIManager.APIManager()
                    .processRawRequest(APIRequestHelper.kGetProductSubCategory, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                JSONObject jsonObject = genricResponse.getObject();
                                CopyOnWriteArrayList<SubCategory> subCategoryList = new CopyOnWriteArrayList<>();
                                JSONArray jsonArray;
                                try {
                                    jsonArray = jsonObject.getJSONArray(kProductSubCategory);
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        subCategoryList.add(new SubCategory(jsonArray.getJSONObject(i)));
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                GenricResponse<CopyOnWriteArrayList<SubCategory>> genericFeedList =
                                        new GenricResponse<>(subCategoryList);
                                DispatchQueue.main(() -> success.iSuccess(iStatus, genericFeedList));
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * method to get the list of product category and sub category by the logged in user.
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getCatSubCategoryList(Block.Success<CopyOnWriteArrayList<ShopCatSubCategory>> success,
                                      Block.Failure failure) {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(kAuthToken, getGenericAuthToken());
        dispatchQueue.async(() -> {
            APIManager.APIManager().processRawRequest(APIRequestHelper.kGetCatSubCategoryList, parameters,
                    (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                        try {
                            JSONObject jsonObject = genricResponse.getObject();
                            CopyOnWriteArrayList<ShopCatSubCategory> categoryList = new CopyOnWriteArrayList<>();
                            JSONArray jsonArray = jsonObject.getJSONArray(kProductCategory);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                ShopCatSubCategory shopCatSubCategory = new ShopCatSubCategory(
                                        jsonArray.getJSONObject(i));
                                categoryList.add(shopCatSubCategory);
                            }
                            GenricResponse<CopyOnWriteArrayList<ShopCatSubCategory>> genericFeedList =
                                    new GenricResponse<>(categoryList);
                            DispatchQueue.main(() -> success.iSuccess(iStatus, genericFeedList));
                        } catch (Exception e) {
                            e.printStackTrace();
                            DispatchQueue.main(
                                    () -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                        }
                    }, (Status statusFail, String message) -> {
                        DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                    });
        });
    }

    /**
     * method to get list of people that user may know.
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getFriendSuggestions(int page, String lat, String lng,
                                     Block.Success<CopyOnWriteArrayList<People>> success,
                                     Block.Failure failure) {
        dispatchQueue.async(() -> {
            final HashMap<String, Object> parameters = new HashMap<>();
            parameters.put(BaseModel.kAuthToken, getCurrentUser().getAuthToken());
            parameters.put(BaseModel.kLatitude, lat);
            parameters.put(BaseModel.kLongitude, lng);
            parameters.put(BaseModel.kPage, page);
            APIManager.APIManager()
                    .processRawRequest(kGetFriendSuggestions, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                try {
                                    JSONObject jsonObject = genricResponse.getObject();
                                    CopyOnWriteArrayList<People> connectionList = new CopyOnWriteArrayList<>();
                                    JSONArray jsonArray = jsonObject.getJSONArray(kConnections);
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        connectionList.add(new People(jsonArray.getJSONObject(i)));
                                    }
                                    GenricResponse<CopyOnWriteArrayList<People>> genericList =
                                            new GenricResponse<>(connectionList);
                                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericList));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    DispatchQueue.main(
                                            () -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                                }
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }


    /**
     * method to get list of people that user may know.
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void OnlineSearchFriends(int page, String searchString,
                                    Block.Success<CopyOnWriteArrayList<People>> success,
                                    Block.Failure failure) {
        dispatchQueue.async(() -> {
            final HashMap<String, Object> parameters = new HashMap<>();
            parameters.put(BaseModel.kAuthToken, getCurrentUser().getAuthToken());
            parameters.put(BaseModel.kOnlineSearchString, searchString);
            parameters.put(BaseModel.kPage, page);
            APIManager.APIManager()
                    .processRawRequest(kOnlineSearchFriends, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                try {
                                    JSONObject jsonObject = genricResponse.getObject();
                                    CopyOnWriteArrayList<People> connectionList = new CopyOnWriteArrayList<>();
                                    JSONArray jsonArray = jsonObject.getJSONArray(kRecords);
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        connectionList.add(new People(jsonArray.getJSONObject(i)));
                                    }
                                    GenricResponse<CopyOnWriteArrayList<People>> genericList =
                                            new GenricResponse<>(connectionList);
                                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericList));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    DispatchQueue.main(
                                            () -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                                }
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }


    /**
     * Method will verify the contact (phone and email) that associated with the user.
     * it will  return the OTP that user need to enter in the app.
     * Also server will send email or SMS to email or Phone.
     * Only one email or phone can link with one user.
     *
     * @param contactId is required to get otp
     * @param success   Block passed as callback for success condition
     * @param failure   Block passed as callback for failure condition
     */
    public void contactVerification(String contactId, Block.Success<HashMap<String, Object>> success, Block.Failure failure) {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(kAuthToken, getCurrentUser().getAuthToken());
        parameters.put(kContactId, contactId);
        dispatchQueue.async(() -> {
            APIManager.APIManager().processRawRequest(APIRequestHelper.kContactVerification, parameters, (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                try {
                    JSONObject jsonObject = genricResponse.getObject();
                    String otp = jsonObject.get(kOTP).toString();
                    HashMap<String, Object> hashMap = new HashMap<>();
                    hashMap.put(kOTP, otp);
                    GenricResponse<HashMap<String, Object>> genericOTP = new GenricResponse<>(hashMap);
                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericOTP));
                } catch (JSONException e) {
                    e.printStackTrace();
                    DispatchQueue.main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                }
            }, (Status statusFail, String message) -> {
                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
            });
        });
    }

    /**
     * method will tell the server that user has enterd correct OTP in the app.
     *
     * @param parameters is required to get otp
     * @param success    Block passed as callback for success condition
     * @param failure    Block passed as callback for failure condition
     */
    public void otpVerification(HashMap<String, Object> parameters,
                                Block.Success<HashMap<String, Object>> success, Block.Failure failure) {
        dispatchQueue.async(() -> {
            APIManager.APIManager()
                    .processRawRequest(APIRequestHelper.kOTPVerification, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                try {
                                    JSONObject jsonObject = genricResponse.getObject();
                                    HashMap<String, Object> hashMap = new HashMap<>();
                                    String contactId = jsonObject.getString(kContactId);
                                    Integer isVerified = jsonObject.getInt(kIsVerified);
                                    hashMap.put(kContactId, contactId);
                                    hashMap.put(kIsVerified, isVerified);
                                    GenricResponse<HashMap<String, Object>> genericVerification =
                                            new GenricResponse<>(hashMap);
                                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericVerification));
                                } catch (JSONException e) {
                                    DispatchQueue.main(
                                            () -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                                }
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * method will validate current password with the user
     * check whether entered password is correct for contactId or not.
     * 0: if not Validate, 1: if validate
     *
     * @param parameters must contain contactId and password
     * @param success    Block passed as callback for success condition
     * @param failure    Block passed as callback for failure condition
     */
    public void validatePassword(HashMap<String, Object> parameters,
                                 Block.Success<HashMap<String, Object>> success, Block.Failure failure) {
        dispatchQueue.async(() -> {
            APIManager.APIManager()
                    .processRawRequest(APIRequestHelper.kValidateCurrentPassword, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                try {
                                    JSONObject jsonObject = genricResponse.getObject();
                                    HashMap<String, Object> dataMap = new HashMap<>();
                                    Integer isValidate =
                                            Integer.parseInt(String.valueOf(jsonObject.get(kIsValidate)));
                                    String OTP = (String) jsonObject.get(kOTP);
                                    dataMap.put(kIsValidate, isValidate);
                                    dataMap.put(kOTP, OTP);
                                    GenricResponse<HashMap<String, Object>> genericVerification =
                                            new GenricResponse<>(dataMap);
                                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericVerification));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    DispatchQueue.main(
                                            () -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                                }
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * method to get the list of skills of loggedin users
     *
     * @Param parameters includes userId of the user whose skill to be accessed and authToken of the
     * logged in user
     */
    public void sendFriendRequest(HashMap<String, Object> parameters, Block.Status status,
                                  Block.Failure failure) {
        dispatchQueue.async(() -> {
            parameters.put(kAuthToken, ModelManager.modelManager().getCurrentUser().getAuthToken());
            APIManager.APIManager()
                    .processRawRequest(kSendConnectionRequest, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {

                                DispatchQueue.main(() -> status.iStatus(iStatus));
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * method to link social account
     *
     * @param status  Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     * @Param parameters includes userId of the user whose skill to be accessed and authToken of the
     * logged in user
     */
    public void linkSocialAccount(HashMap<String, Object> parameters, Block.Status status, Block.Failure failure) {
        dispatchQueue.async(() -> {
            parameters.put(kAuthToken, mCurrentUser.getAuthToken());
            APIManager.APIManager().processRawRequest(kLinkSocialAccount, parameters, (Constants.Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                try {
                    JSONObject jsonObject = genricResponse.getObject();
                    JSONArray jsonArray = jsonObject.getJSONArray(kUserSocialLinks);
                    CopyOnWriteArrayList<SocialLink> userSocialLink = new CopyOnWriteArrayList<>();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        userSocialLink.add(new SocialLink(jsonArray.getJSONObject(i)));
                    }
                    getCurrentUser().setUserSocialLinks(userSocialLink);
                    archiveCurrentUser();
                    DispatchQueue.main(() -> status.iStatus(iStatus));
                } catch (Exception e) {
                    e.printStackTrace();
                    DispatchQueue.main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                }
            }, (Constants.Status statusFail, String message) -> {
                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
            });
        });
    }

    //PeepsBoard apis

    /**
     * method to get the list of product feedList by the logged in user.
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getPeepsInBoards(int page, Block.Success<HashMap<String, Object>> success, Block.Failure failure) {
        final HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(BaseModel.kAuthToken, getCurrentUser().getAuthToken());
        parameters.put(BaseModel.kPage, page);
        dispatchQueue.async(() -> {
            APIManager.APIManager().processRawRequest(kGetPeepsInBoardList, parameters, (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                try {
                    JSONObject jsonObject = genricResponse.getObject();
                    Integer totalRecords = jsonObject.getInt(kTotalRecords);
                    CopyOnWriteArrayList<Board> boardList = new CopyOnWriteArrayList<>();
                    HashMap<String, Object> hashMap = new HashMap<>();
                    hashMap.put(kTotalRecords, totalRecords);
                    JSONArray jsonArray = jsonObject.getJSONArray(kRecords);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        Board board = new Board(jsonArray.getJSONObject(i));
                        boardList.add(board);
                        readyPicasso(board.getProfilePicURL());
                    }
                    hashMap.put(kRecords, boardList);
                    GenricResponse<HashMap<String, Object>> mapGenricResponse =
                            new GenricResponse<>(hashMap);
                    DispatchQueue.main(() -> success.iSuccess(iStatus, mapGenricResponse));
                } catch (Exception e) {
                    e.printStackTrace();
                    DispatchQueue.main(
                            () -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                }
            }, (Status statusFail, String message) -> {
                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
            });
        });
    }

    /**
     * method to get the list of product feedList by the logged in user.
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getMyBoards(int page, Block.Success<CopyOnWriteArrayList<Board>> success, Block.Failure failure) {
        final HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(BaseModel.kAuthToken, getCurrentUser().getAuthToken());
        parameters.put(BaseModel.kPage, page);
        dispatchQueue.async(() -> {
            APIManager.APIManager().processRawRequest(kGetMyBoardList, parameters, (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                try {
                    JSONObject jsonObject = genricResponse.getObject();
                    CopyOnWriteArrayList<Board> boardList = new CopyOnWriteArrayList<>();
                    JSONArray jsonArray = jsonObject.getJSONArray(kRecords);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        Board board = new Board(jsonArray.getJSONObject(i));
                        boardList.add(board);
                    }
                    GenricResponse<CopyOnWriteArrayList<Board>> genericFeeds =
                            new GenricResponse<>(boardList);
                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericFeeds));
                } catch (Exception e) {
                    e.printStackTrace();
                    DispatchQueue.main(
                            () -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                }
            }, (Status statusFail, String message) -> {
                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
            });
        });
    }

    /**
     * method to get the list of product feedList by the logged in user.
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getComments(Integer boardId, int page, Block.Success<CopyOnWriteArrayList<Comment>> success, Block.Failure failure) {
        dispatchQueue.async(() -> {
            final HashMap<String, Object> parameters = new HashMap<>();
            parameters.put(BaseModel.kAuthToken, getCurrentUser().getAuthToken());
            parameters.put(BaseModel.kBoardId, boardId);
            parameters.put(BaseModel.kPage, page);
            APIManager.APIManager().processRawRequest(kGetComments, parameters, (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                try {
                    JSONObject jsonObject = genricResponse.getObject();
                    CopyOnWriteArrayList<Comment> commentList = new CopyOnWriteArrayList<>();
                    JSONArray jsonArray = jsonObject.getJSONArray(kComments);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        Comment comment = new Comment(jsonArray.getJSONObject(i));
                        commentList.add(comment);
                        readyPicasso(comment.getProfilePicURL());
                    }
                    GenricResponse<CopyOnWriteArrayList<Comment>> listGenricResponse = new GenricResponse<>(commentList);
                    DispatchQueue.main(() -> success.iSuccess(iStatus, listGenricResponse));
                } catch (Exception e) {
                    e.printStackTrace();
                    DispatchQueue.main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                }
            }, (Status statusFail, String message) -> {
                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
            });
        });
    }

    /**
     * method to get the list of product feedList by the logged in user.
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void saveComment(int boardId, String comment, Block.Success<Comment> success,
                            Block.Failure failure) {
        final HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(BaseModel.kAuthToken, getCurrentUser().getAuthToken());
        parameters.put(BaseModel.kBoardId, boardId);
        parameters.put(BaseModel.kComment, comment);
        dispatchQueue.async(() -> {
            APIManager.APIManager()
                    .processRawRequest(kSaveComments, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                try {
                                    JSONObject jsonObject = genricResponse.getObject().getJSONObject(kRecords);
                                    Comment commentObj = new Comment(jsonObject);
                                    GenricResponse<Comment> generic = new GenricResponse<>(commentObj);
                                    DispatchQueue.main(() -> success.iSuccess(iStatus, generic));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    DispatchQueue.main(
                                            () -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                                }
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * method to get the list of product feedList by the logged in user.
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void likeDislikeBoard(int boardId, Boolean isLike,
                                 Block.Success<HashMap<String, Object>> success, Block.Failure failure) {
        final HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(BaseModel.kAuthToken, getCurrentUser().getAuthToken());
        parameters.put(BaseModel.kBoardId, boardId);
        parameters.put(BaseModel.kIsLike, (isLike ? 0 : 1));
        dispatchQueue.async(() -> {
            APIManager.APIManager()
                    .processRawRequest(kLikeDislikeBoard, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                try {
                                    JSONObject jsonObject = genricResponse.getObject();
                                    HashMap<String, Object> map = new HashMap<>();
                                    Integer resultIsLike = BaseModel.getValue(jsonObject, kIsLike, Integer.class);
                                    Integer resultLikecount =
                                            BaseModel.getValue(jsonObject, kLikeCount, Integer.class);

                                    map.put(kIsLike, resultIsLike);
                                    map.put(kLikeCount, resultLikecount);
                                    GenricResponse<HashMap<String, Object>> mapGenricResponse =
                                            new GenricResponse<>(map);
                                    DispatchQueue.main(() -> success.iSuccess(iStatus, mapGenricResponse));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    DispatchQueue.main(
                                            () -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                                }
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * method to get the list of product feedList by the logged in user.
     *
     * @param status  Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getshareBoard(int boardId, int share2ndLevel, int share3rdLevel,
                              CopyOnWriteArrayList<Integer> userIdList,
                              Block.Status status, Block.Failure failure) {
        final HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(BaseModel.kAuthToken, getCurrentUser().getAuthToken());
        parameters.put(BaseModel.kBoardId, boardId);
        parameters.put(BaseModel.kUserIdList, Utils.generateJsonList(userIdList));
        parameters.put(BaseModel.kWantToShare2ndLevel, share2ndLevel);
        parameters.put(BaseModel.kWantToShare3ndLevel, share3rdLevel);
        dispatchQueue.async(() -> {
            APIManager.APIManager()
                    .processRawRequest(kShareBoard, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {

                                DispatchQueue.main(() -> status.iStatus(iStatus));
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * method to get the list of product feedList by the logged in user.
     *
     * @param status  Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void addBoard(String description, Block.Status status, Block.Failure failure) {
        final HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(BaseModel.kAuthToken, getCurrentUser().getAuthToken());
        parameters.put(BaseModel.kDescription, description);
        dispatchQueue.async(() -> {
            APIManager.APIManager()
                    .processRawRequest(kAddBoard, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                getCurrentUser().setPeepsBoardCount(getCurrentUser().getPeepsBoardCount() + 1);
                                archiveCurrentUser();
                                DispatchQueue.main(() -> status.iStatus(iStatus));
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * method to get the list of product feedList by the logged in user.
     *
     * @param status  Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void removeBoard(int boardId, Block.Status status, Block.Failure failure) {
        final HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(BaseModel.kAuthToken, getCurrentUser().getAuthToken());
        parameters.put(BaseModel.kBoardId, boardId);
        dispatchQueue.async(() -> {
            APIManager.APIManager()
                    .processRawRequest(kRemoveBoard, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                getCurrentUser().setPeepsBoardCount(getCurrentUser().getPeepsBoardCount() - 1);
                                archiveCurrentUser();
                                DispatchQueue.main(() -> status.iStatus(iStatus));
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * method to get the list of product feedList by the logged in user.
     *
     * @param status  Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void removeComment(int boardId, int commentId, Block.Status status,
                              Block.Failure failure) {
        final HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(BaseModel.kAuthToken, getCurrentUser().getAuthToken());
        parameters.put(BaseModel.kBoardId, boardId);
        parameters.put(BaseModel.kCommentId, commentId);
        dispatchQueue.async(() -> {
            APIManager.APIManager()
                    .processRawRequest(kRemoveComment, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {

                                DispatchQueue.main(() -> status.iStatus(iStatus));
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * method to get the list of product feedList by the logged in user.
     *
     * @param status  Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void editComment(int boardId, int commentId, String comment, Block.Status status,
                            Block.Failure failure) {
        final HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(BaseModel.kAuthToken, getCurrentUser().getAuthToken());
        parameters.put(BaseModel.kBoardId, boardId);
        parameters.put(BaseModel.kCommentId, commentId);
        parameters.put(BaseModel.kComment, comment);
        dispatchQueue.async(() -> {
            APIManager.APIManager()
                    .processRawRequest(kEditComments, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                DispatchQueue.main(() -> status.iStatus(iStatus));
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * method to get the product rating and review for product in details
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getProductRating(int productId, Block.Success<RatingModel> success,
                                 Block.Failure failure) {
        final HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(BaseModel.kAuthToken, getCurrentUser().getAuthToken());
        parameters.put(BaseModel.kProductId, productId);
        dispatchQueue.async(() -> {
            APIManager.APIManager()
                    .processRawRequest(kGetProductRating, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                try {
                                    JSONObject jsonObject = genricResponse.getObject();
                                    RatingModel rating = new RatingModel(jsonObject);
                                    GenricResponse<RatingModel> generic = new GenricResponse<>(rating);
                                    DispatchQueue.main(() -> success.iSuccess(iStatus, generic));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    DispatchQueue.main(
                                            () -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                                }
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * method to get the list of rating and review fro particular product
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getRatingReviewList(int page, int productId, Block.Success<List<ReviewModel>> success,
                                    Block.Failure failure) {
        final HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(BaseModel.kAuthToken, getCurrentUser().getAuthToken());
        parameters.put(BaseModel.kPage, page);
        parameters.put(BaseModel.kProductId, productId);
        dispatchQueue.async(() -> {
            APIManager.APIManager()
                    .processRawRequest(kGetRatingReviewList, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                try {
                                    JSONObject jsonObject = genricResponse.getObject();
                                    List<ReviewModel> reviewList = new ArrayList<>();
                                    JSONArray jsonArray = jsonObject.getJSONArray(kRecords);
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        ReviewModel review = new ReviewModel(jsonArray.getJSONObject(i));
                                        reviewList.add(review);
                                    }
                                    GenricResponse<List<ReviewModel>> generic = new GenricResponse<>(reviewList);
                                    DispatchQueue.main(() -> success.iSuccess(iStatus, generic));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    DispatchQueue.main(
                                            () -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                                }
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * method to add and edit rating and review for product in details
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void editAddRating(int productId, String review, int rating,
                              Block.Success<ReviewModel> success, Block.Failure failure) {
        final HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(BaseModel.kAuthToken, getCurrentUser().getAuthToken());
        parameters.put(BaseModel.kProductId, productId);
        parameters.put(BaseModel.kReview, review);
        parameters.put(BaseModel.kRating, rating);
        dispatchQueue.async(() -> {
            APIManager.APIManager()
                    .processRawRequest(kAddEditReview, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                try {
                                    JSONObject jsonObject = genricResponse.getObject().getJSONObject(kRecords);
                                    ReviewModel mreview = new ReviewModel(jsonObject);
                                    GenricResponse<ReviewModel> generic = new GenricResponse<>(mreview);
                                    DispatchQueue.main(() -> success.iSuccess(iStatus, generic));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    DispatchQueue.main(
                                            () -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                                }
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * method to create a new shop to peeps by user that is loged-in
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void createNewShop(HashMap<String, Object> parameters, Block.Success<Shop> success,
                              Block.Failure failure) {
        parameters.put(kAuthToken, getCurrentUser().getAuthToken());
        dispatchQueue.async(() -> {
            APIManager.APIManager()
                    .processFormRequest(APIRequestHelper.kCreateShop, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                try {
                                    Shop shop = new Shop(genricResponse.getObject());
                                    getCurrentUser().setShopId(shop.getShopId());
                                    archiveCurrentUser();
                                    GenricResponse<Shop> genericFeed = new GenricResponse<>(shop);
                                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericFeed));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    DispatchQueue.main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                                }
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * method to get the list of product type in shop by the logged in user.
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getBankList(Block.Success<CopyOnWriteArrayList<BankType>> success,
                            Block.Failure failure) {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(kAuthToken, getGenericAuthToken());
        dispatchQueue.async(() -> {
            APIManager.APIManager().processRawRequest(APIRequestHelper.kGetBankList, parameters,
                    (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                        try {
                            JSONObject jsonObject = genricResponse.getObject();
                            CopyOnWriteArrayList<BankType> bankList = new CopyOnWriteArrayList<>();
                            JSONArray jsonArray = jsonObject.getJSONArray(kRecords);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                bankList.add(new BankType(jsonArray.getJSONObject(i)));
                            }
                            GenricResponse<CopyOnWriteArrayList<BankType>> genericList = new GenricResponse<>(
                                    bankList);
                            DispatchQueue.main(() -> success.iSuccess(iStatus, genericList));
                        } catch (Exception e) {
                            e.printStackTrace();
                            DispatchQueue.main(
                                    () -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                        }
                    }, (Status statusFail, String message) -> {
                        DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                    });
        });
    }

    /**
     * method to get the list of product type in shop by the logged in user.
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getProductTypeList(Block.Success<CopyOnWriteArrayList<ProductType>> success, Block.Failure failure) {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(kAuthToken, getGenericAuthToken());
        dispatchQueue.async(() -> {

            APIManager.APIManager().processRawRequest(APIRequestHelper.kGetProductTypeList, parameters, (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                try {
                    JSONObject jsonObject = genricResponse.getObject();
                    CopyOnWriteArrayList<ProductType> productList = new CopyOnWriteArrayList<>();
                    JSONArray jsonArray = jsonObject.getJSONArray(kRecords);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        productList.add(new ProductType(jsonArray.getJSONObject(i)));
                    }
                    getCurrentUser().setProductList(productList);
                    archiveCurrentUser();
                    GenricResponse<CopyOnWriteArrayList<ProductType>> genericList = new GenricResponse<>(productList);
                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericList));
                } catch (Exception e) {
                    e.printStackTrace();
                    DispatchQueue.main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                }
            }, (Status statusFail, String message) -> {
                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
            });
        });
    }

    /**
     * method to get the list of delivery type in shop by the logged in user.
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getDeliveryTimeList(Block.Success<CopyOnWriteArrayList<DeliveryType>> success, Block.Failure failure) {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(kAuthToken, getGenericAuthToken());
        dispatchQueue.async(() ->
                APIManager.APIManager().processRawRequest(APIRequestHelper.kGetDeliveryTime, parameters, (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                    try {
                        JSONObject jsonObject = genricResponse.getObject();
                        CopyOnWriteArrayList<DeliveryType> deliveryList = new CopyOnWriteArrayList<>();
                        JSONArray jsonArray = jsonObject.getJSONArray(kRecords);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            deliveryList.add(new DeliveryType(jsonArray.getJSONObject(i)));
                        }
                        GenricResponse<CopyOnWriteArrayList<DeliveryType>> genericList = new GenricResponse<>(
                                deliveryList);
                        DispatchQueue.main(() -> success.iSuccess(iStatus, genericList));
                    } catch (Exception e) {
                        e.printStackTrace();
                        DispatchQueue.main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                    }
                }, (Status statusFail, String message) -> {
                    DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                }));
    }

    /**
     * method to edit a shop added to peeps by user that is logged-in
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void editShop(HashMap<String, Object> parameters, Block.Success<Shop> success,
                         Block.Failure failure) {
        parameters.put(kAuthToken, getCurrentUser().getAuthToken());
        dispatchQueue.async(() -> {
            APIManager.APIManager().processFormRequest(APIRequestHelper.kEditShop, parameters, (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                try {
                    Shop shop = new Shop(genricResponse.getObject());
                    GenricResponse<Shop> genericFeed = new GenricResponse<>(shop);
                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericFeed));
                } catch (Exception e) {
                    e.printStackTrace();
                    DispatchQueue.main(
                            () -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                }
            }, (Status statusFail, String message) -> {
                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
            });
        });
    }

    /**
     * method to get the list of shops by the logged in user.
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getShopList(HashMap<String, Object> parameters, int page, Block.Success<CopyOnWriteArrayList<ShopModel>> success, Block.Failure failure) {
        parameters.put(kAuthToken, getCurrentUser().getAuthToken());
        parameters.put(kPage, page);
        dispatchQueue.async(() -> {
            APIManager.APIManager().processRawRequest(kGetShopList, parameters, (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                try {
                    JSONObject jsonObject = genricResponse.getObject();
                    CopyOnWriteArrayList<ShopModel> shopList = new CopyOnWriteArrayList<>();
                    JSONArray jsonArray = jsonObject.getJSONArray(kRecords);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        ShopModel shop = new ShopModel(jsonArray.getJSONObject(i));
                        shopList.add(shop);
                        readyPicasso(shop.getBannerUrl());
                    }
                    getCurrentUser().setShopNearList(shopList);
                    archiveCurrentUser();
                    GenricResponse<CopyOnWriteArrayList<ShopModel>> genericList = new GenricResponse<>(shopList);
                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericList));
                } catch (Exception e) {
                    e.printStackTrace();
                    DispatchQueue.main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                }
            }, (Status statusFail, String message) -> {
                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
            });
        });
    }

    /**
     * method to get the shop details of particular shopId by the logged in user.
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */

    public void getShopDetails(int shopId, Block.Success<MyShop> success, Block.Failure failure) {
        final HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(BaseModel.kAuthToken, getCurrentUser().getAuthToken());
        parameters.put(BaseModel.kShopId, shopId);
        dispatchQueue.async(() -> {
            APIManager.APIManager().processRawRequest(kGetShopDetails, parameters,
                    (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                        try {
                            JSONObject jsonObject = genricResponse.getObject();
                            MyShop shop = new MyShop(jsonObject);

                            //will set the shop to current user
                            if (shopId == getCurrentUser().getShopId()) {
                                getCurrentUser().setMyShop(shop);
                            }
                            GenricResponse<MyShop> genericShop = new GenricResponse<>(shop);
                            DispatchQueue.main(() -> success.iSuccess(iStatus, genericShop));
                        } catch (Exception e) {
                            e.printStackTrace();
                            DispatchQueue
                                    .main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                        }
                    }, (Status statusFail, String message) -> {
                        DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                    });
        });
    }

    /**
     * method to get the shop details of particular shopId by the logged in user.
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */

    public void getMyShopDetails(int shopId, Block.Success<MyShop> success, Block.Failure failure) {
        final HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(BaseModel.kAuthToken, getCurrentUser().getAuthToken());
        parameters.put(BaseModel.kShopId, shopId);
        dispatchQueue.async(() -> {
            APIManager.APIManager().processRawRequest(kGetMyShopDetails, parameters,
                    (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                        try {
                            JSONObject jsonObject = genricResponse.getObject();
                            MyShop shop = new MyShop(jsonObject);
                            if (shopId == getCurrentUser().getShopId()) {
                                getCurrentUser().setMyShop(shop);
                                getCurrentUser().setShopStatus(shop.getShopStatus());
                                archiveCurrentUser();
                            }
                            GenricResponse<MyShop> genericShop = new GenricResponse<>(shop);
                            DispatchQueue.main(() -> success.iSuccess(iStatus, genericShop));
                        } catch (Exception e) {
                            e.printStackTrace();
                            DispatchQueue.main(
                                    () -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                        }
                    }, (Status statusFail, String message) -> {
                        DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                    });
        });
    }

    /**
     * method to get the list of shop category by the logged in user.
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getShopCategoryList(int shopId,
                                    Block.Success<CopyOnWriteArrayList<ShopCategory>> success, Block.Failure failure) {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(kAuthToken, mCurrentUser.getAuthToken());
        parameters.put(kShopId, shopId);
        dispatchQueue.async(() -> {
            APIManager.APIManager()
                    .processRawRequest(APIRequestHelper.kGetShopCategoryList, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                try {
                                    JSONObject jsonObject = genricResponse.getObject();
                                    CopyOnWriteArrayList<ShopCategory> categoryList = new CopyOnWriteArrayList<>();
                                    JSONArray jsonArray = jsonObject.getJSONArray(kRecords);
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        categoryList.add(new ShopCategory(jsonArray.getJSONObject(i)));
                                    }
                                    GenricResponse<CopyOnWriteArrayList<ShopCategory>> genericList =
                                            new GenricResponse<>(categoryList);
                                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericList));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    DispatchQueue.main(
                                            () -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                                }
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * method to set favourite and un-favourite shop by the logged in user.
     *
     * @param status  Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void setFavouriteShop(int shopId, int favourite, Block.Status status,
                                 Block.Failure failure) {
        dispatchQueue.async(() -> {
            final HashMap<String, Object> parameters = new HashMap<>();
            parameters.put(BaseModel.kAuthToken, getCurrentUser().getAuthToken());
            parameters.put(BaseModel.kShopId, shopId);
            parameters.put(BaseModel.kIsFavorite, favourite);
            APIManager.APIManager()
                    .processRawRequest(kSetFavouriteShop, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                try {
                                    JSONObject jsonObject = genricResponse.getObject();
                                    Integer fav = jsonObject.getInt(kIsFavorite);

                                    Integer favCount = getCurrentUser().getFavoriteShopCount();

                                    if (fav == 1) {
                                        favCount = favCount + 1;
                                    } else {
                                        favCount = favCount - 1;
                                    }
                                    getCurrentUser().setFavoriteShopCount(favCount);
                                    archiveCurrentUser();
                                    DispatchQueue.main(() -> status.iStatus(iStatus));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    DispatchQueue
                                            .main(() -> failure.iFailure(iStatus, kMessageInternalInconsistency));
                                }
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * method to get the list of favourite shops of the user logged in .
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getFavouriteShopList(Block.Success<CopyOnWriteArrayList<ShopModel>> success,
                                     Block.Failure failure) {
        final HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(BaseModel.kAuthToken, getCurrentUser().getAuthToken());
        dispatchQueue.async(() -> {
            APIManager.APIManager().processRawRequest(APIRequestHelper.kGetFavoriteShopList, parameters,
                    (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                        try {
                            JSONObject jsonObject = genricResponse.getObject();
                            CopyOnWriteArrayList<ShopModel> shopList = new CopyOnWriteArrayList<>();
                            JSONArray jsonArray = jsonObject.getJSONArray(kRecords);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                shopList.add(new ShopModel(jsonArray.getJSONObject(i)));
                            }
                            getCurrentUser().setFavShopList(shopList);
                            archiveCurrentUser();
                            GenricResponse<CopyOnWriteArrayList<ShopModel>> genericList =
                                    new GenricResponse<>(shopList);
                            DispatchQueue.main(() -> success.iSuccess(iStatus, genericList));
                        } catch (Exception e) {
                            e.printStackTrace();
                            DispatchQueue.main(
                                    () -> failure.iFailure(iStatus, kMessageInternalInconsistency));
                        }
                    }, (Status statusFail, String message) -> {
                        DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                    });
        });
    }

    /**
     * method to get the shop rating and review list for shopList
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getShopRatingReview(int shopId, int page, Block.Success<ShopRating> success,
                                    Block.Failure failure) {
        final HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(BaseModel.kAuthToken, getCurrentUser().getAuthToken());
        parameters.put(BaseModel.kShopId, shopId);
        parameters.put(BaseModel.kPage, page);
        dispatchQueue.async(() -> {
            APIManager.APIManager()
                    .processRawRequest(kGetShopRatingReview, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                try {
                                    JSONObject jsonObject = genricResponse.getObject();
                                    ShopRating rating = new ShopRating(jsonObject);
                                    GenricResponse<ShopRating> generic = new GenricResponse<>(rating);
                                    DispatchQueue.main(() -> success.iSuccess(iStatus, generic));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    DispatchQueue.main(
                                            () -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                                }
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * method to delete the product in shop by the logged in user.
     *
     * @param status  Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void deleteProduct(int productId, Block.Status status, Block.Failure failure) {
        final HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(BaseModel.kAuthToken, getCurrentUser().getAuthToken());
        parameters.put(BaseModel.kProductId, productId);
        dispatchQueue.async(() -> {
            APIManager.APIManager().processRawRequest(kDeleteProduct, parameters,
                    (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                        getCurrentUser().setPeepsBoardCount(getCurrentUser().getSellingProductCount() - 1);
                        archiveCurrentUser();
                        DispatchQueue.main(() -> status.iStatus(iStatus));
                    }, (Status statusFail, String message) -> {
                        DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                    });
        });
    }

    /**
     * method to set the status of the product in shop of logged in user
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void setProductStatus(int shopId, JSONArray productArray,
                                 Block.Success<HashMap<String, Object>> success,
                                 Block.Failure failure) {
        final HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(BaseModel.kAuthToken, getCurrentUser().getAuthToken());
        parameters.put(BaseModel.kShopId, shopId);
        parameters.put(BaseModel.kProductIdList, productArray);
        dispatchQueue.async(() -> {
            APIManager.APIManager().processRawRequest(kSetProductStatus, parameters,
                    (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                        try {
                            JSONObject jsonObject = genricResponse.getObject();
                            JSONArray jsonArray = jsonObject.getJSONArray(kProductIdList);
                            JSONObject ids = jsonArray.getJSONObject(0);
                            HashMap<String, Object> productMap = new HashMap<>();
                            productMap.put(kProductId, ids.get(kProductId));
                            productMap.put(kProductStatus, ids.get(kProductStatus));
                            GenricResponse<HashMap<String, Object>> genricResponse1 = new GenricResponse<HashMap<String, Object>>(
                                    productMap);
                            DispatchQueue.main(() -> success.iSuccess(iStatus, genricResponse1));
                        } catch (Exception e) {
                            e.printStackTrace();
                            DispatchQueue
                                    .main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                        }
                    }, (Status statusFail, String message) -> {
                        DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                    });
        });
    }

    /**
     * method to delete the shop created by the logged in user.
     *
     * @param status  Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void deleteMyShop(int shopId, Block.Status status, Block.Failure failure) {
        final HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(BaseModel.kAuthToken, getCurrentUser().getAuthToken());
        parameters.put(BaseModel.kShopId, shopId);
        dispatchQueue.async(() -> {
            APIManager.APIManager().processRawRequest(kDeleteMyShop, parameters,
                    (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                        getCurrentUser().setShopId(-1);
                        getCurrentUser().setMyShop(null);
                        getCurrentUser().setSellingProductCount(0);
                        archiveCurrentUser();
                        DispatchQueue.main(() -> status.iStatus(iStatus));
                    }, (Status statusFail, String message) -> {
                        DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                    });
        });
    }

    /**
     * method to delete the shop created by the logged in user.
     *
     * @param status  Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void activateMyShop(int shopId, int status, Block.Success<Integer> success,
                               Block.Failure failure) {
        final HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(BaseModel.kAuthToken, getCurrentUser().getAuthToken());
        parameters.put(BaseModel.kShopId, shopId);
        parameters.put(BaseModel.kShopStatus, status);
        dispatchQueue.async(() -> {
            APIManager.APIManager().processRawRequest(kActivateMyShop, parameters,
                    (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                        try {
                            JSONObject jsonObject = genricResponse.getObject();
                            int shopStatus = jsonObject.getInt(kShopStatus);
                            getCurrentUser().setShopStatus(shopStatus);
                            archiveCurrentUser();
                            GenricResponse<Integer> genericShop = new GenricResponse<>(shopStatus);
                            DispatchQueue.main(() -> success.iSuccess(iStatus, genericShop));
                        } catch (JSONException e) {
                            e.printStackTrace();
                            DispatchQueue.main(() -> failure.iFailure(iStatus, kMessageInternalInconsistency));
                        }
                    }, (Status statusFail, String message) -> {
                        DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                    });
        });
    }

    /**
     * method to get the shopping cart by the logged in user.
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getShoppingCart(Block.Success<ShoppingCart> success, Block.Failure failure) {
        final HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(BaseModel.kAuthToken, getCurrentUser().getAuthToken());
        dispatchQueue.async(() -> {
            APIManager.APIManager().processRawRequest(kGetShoppingCart, parameters,
                    (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                        try {
                            JSONObject jsonObject = genricResponse.getObject();
                            ShoppingCart shopCart = new ShoppingCart(jsonObject);
                            getCurrentUser().setShoppingCartCount(jsonObject.getInt(kCartItemCount));
                            archiveCurrentUser();

                            Intent registrationCompleteIntent = new Intent(CART_UPDATE);
                            registrationCompleteIntent.putExtra(kData,
                                    genricResponse.getObject().getInt(kCartItemCount));
                            LocalBroadcastManager.getInstance(ApplicationManager.getContext())
                                    .sendBroadcast(registrationCompleteIntent);

                            GenricResponse<ShoppingCart> genericShop = new GenricResponse<>(shopCart);
                            DispatchQueue.main(() -> success.iSuccess(iStatus, genericShop));
                        } catch (Exception e) {
                            e.printStackTrace();
                            DispatchQueue.main(
                                    () -> failure.iFailure(iStatus, kMessageInternalInconsistency));
                        }
                    }, (Status statusFail, String message) -> {
                        DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                    });

        });
    }

    /**
     * method to add product to shopping cart by the logged in user
     *
     * @param status  Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void addShoppingCart(int productId, int count, Block.Status status,
                                Block.Failure failure) {
        final HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(BaseModel.kAuthToken, getCurrentUser().getAuthToken());
        parameters.put(BaseModel.kProductId, productId);
        parameters.put(BaseModel.kProductQuantity, count);
        dispatchQueue.async(() -> {
            APIManager.APIManager().processRawRequest(kAddShoppingCart, parameters,
                    (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                        JSONObject jsonObject = genricResponse.getObject();

                        DispatchQueue.main(() -> status.iStatus(iStatus));
                    }, (Status statusFail, String message) -> {
                        DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                    });
        });
    }

    /**
     * method to remove product to shopping cart by the logged in user
     *
     * @param status  Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void removeShoppingCart(int productId, Block.Status status, Block.Failure failure) {
        final HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(BaseModel.kAuthToken, getCurrentUser().getAuthToken());
        parameters.put(BaseModel.kProductId, productId);
        dispatchQueue.async(() -> {
            APIManager.APIManager().processRawRequest(kRemoveShoppingCart, parameters,
                    (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                        JSONObject jsonObject = genricResponse.getObject();

                        DispatchQueue.main(() -> status.iStatus(iStatus));
                    }, (Status statusFail, String message) -> {
                        DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                    });
        });
    }

    /**
     * method to remove product to shopping cart by the logged in user
     *
     * @param failure Block passed as callback for failure condition
     */
    public void addRemoveProduct(int productId, int quantity, Block.Success<ShoppingCart> success,
                                 Block.Failure failure) {
        final HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(BaseModel.kAuthToken, getCurrentUser().getAuthToken());
        parameters.put(BaseModel.kProductId, productId);
        parameters.put(BaseModel.kItemQuantity, quantity);
        dispatchQueue.async(() -> {
            APIManager.APIManager().processRawRequest(kManageCartQuantity, parameters,
                    (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                        try {
                            ShoppingCart shopCart = new ShoppingCart(genricResponse.getObject());
                            getCurrentUser().setShoppingCartCount(
                                    genricResponse.getObject().getInt(kCartItemCount));
                            archiveCurrentUser();

                            Intent registrationCompleteIntent = new Intent(CART_UPDATE);
                            registrationCompleteIntent.putExtra(kData,
                                    genricResponse.getObject().getInt(kCartItemCount));
                            LocalBroadcastManager.getInstance(ApplicationManager.getContext())
                                    .sendBroadcast(registrationCompleteIntent);
                            GenricResponse<ShoppingCart> genericShop = new GenricResponse<>(shopCart);
                            DispatchQueue.main(() -> success.iSuccess(iStatus, genericShop));
                        } catch (Exception e) {
                            e.printStackTrace();
                            DispatchQueue.main(
                                    () -> failure.iFailure(iStatus, kMessageInternalInconsistency));
                        }
                    }, (Status statusFail, String message) -> {
                        DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                    });
        });
    }


    /**
     * method to cancel order before payment by the logged in user
     *
     * @param status  Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void cancelOrderBeforePayment(int orderId, Block.Status status, Block.Failure failure) {
        final HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(BaseModel.kAuthToken, getCurrentUser().getAuthToken());
        parameters.put(BaseModel.kUserId, getCurrentUser().getUserId());
        parameters.put(BaseModel.kOrderId, orderId);
        dispatchQueue.async(() -> {
            APIManager.APIManager().processRawRequest(kCancelOrderBeforePayment, parameters,
                    (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                        JSONObject jsonObject = genricResponse.getObject();

                        DispatchQueue.main(() -> status.iStatus(iStatus));
                    }, (Status statusFail, String message) -> {
                        DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                    });
        });
    }

    /**
     * method to validate the promo code for product in shopping cart by the logged in user
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void validatePromoCode(String promoCode, Integer orderId, Block.Success<Integer> success,
                                  Block.Failure failure) {
        final HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(BaseModel.kAuthToken, getCurrentUser().getAuthToken());
        parameters.put(BaseModel.kPromoCode, promoCode);
        parameters.put(BaseModel.kOrderId, orderId);
        dispatchQueue.async(() -> {
            APIManager.APIManager().processRawRequest(kGetValidatePromoCode, parameters,
                    (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                        try {
                            JSONObject jsonObject = genricResponse.getObject();
                            int isValid = jsonObject.getInt(kIsValid);

                            GenricResponse<Integer> genericShop = new GenricResponse<>(isValid);
                            DispatchQueue.main(() -> success.iSuccess(iStatus, genericShop));
                        } catch (JSONException e) {
                            e.printStackTrace();
                            DispatchQueue.main(
                                    () -> failure.iFailure(iStatus, kMessageInternalInconsistency));
                        }
                    }, (Status statusFail, String message) -> {
                        DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                    });
        });
    }

    /**
     * method to settype of payment : 1(for paytm),2(PayUMoney), 3(Cash on delivery), 4(pending
     * payment)
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void setPaymentType(Integer orderId, Integer paymentType, Block.Success<Integer> success,
                               Block.Failure failure) {
        final HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(BaseModel.kAuthToken, getCurrentUser().getAuthToken());
        parameters.put(BaseModel.kOrderId, orderId);
        parameters.put(BaseModel.kPaymentType, paymentType);
        dispatchQueue.async(() -> {
            APIManager.APIManager()
                    .processRawRequest(kSetPaymentType, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                try {
                                    JSONObject jsonObject = genricResponse.getObject();
                                    int paymenttypeVal = jsonObject.getInt(kPaymentType);

                                    GenricResponse<Integer> genericShop = new GenricResponse<>(paymenttypeVal);
                                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericShop));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    DispatchQueue.main(
                                            () -> failure.iFailure(iStatus, kMessageInternalInconsistency));
                                }
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * API will used to get FinalBill Summary,
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getFinalBillSummary(String promoCode, HashMap<String, Object> deliveryAddress,
                                    Block.Success<FinalBillSummary> success, Block.Failure failure) {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(kAuthToken, getCurrentUser().getAuthToken());
        parameters.put(kPromoCode, promoCode);
        parameters.put(kDeliveryAddress, deliveryAddress);
        dispatchQueue.async(() -> {
            APIManager.APIManager()
                    .processRawRequest(kGetFinalBillSummary, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                JSONObject jsonObject = genricResponse.getObject();
                                try {
                                    FinalBillSummary billsummary = new FinalBillSummary(jsonObject);
                                    GenricResponse<FinalBillSummary> genericShop = new GenricResponse<>(billsummary);
                                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericShop));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    DispatchQueue.main(
                                            () -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                                }
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * API will used to get Bill Summary after payment,
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getSummaryAfterPayment(Integer orderID, Block.Success<OrderHistory> success,
                                       Block.Failure failure) {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(kAuthToken, getCurrentUser().getAuthToken());
        parameters.put(kOrderId, orderID);
        dispatchQueue.async(() -> {
            APIManager.APIManager()
                    .processRawRequest(kGetOrderSummaryAfterPayment, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                JSONObject jsonObject = genricResponse.getObject();
                                try {
                                    OrderHistory orderHistory = new OrderHistory(jsonObject);
                                    GenricResponse<OrderHistory> ordersummary = new GenricResponse<>(orderHistory);
                                    DispatchQueue.main(() -> success.iSuccess(iStatus, ordersummary));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    DispatchQueue.main(
                                            () -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                                }
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * API will used to generate Checksum for Paytm,
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void generateChecksum(Integer orderId, Block.Success<CheckSumDetails> success,
                                 Block.Failure failure) {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(kAuthToken, getCurrentUser().getAuthToken());
        parameters.put(kOrderId, orderId);
        dispatchQueue.async(() -> {
            APIManager.APIManager()
                    .processRawRequest(kGenerateCheckSum, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                JSONObject jsonObject = genricResponse.getObject();
                                // CopyOnWriteArrayList<CheckSumDetails> checkSumDetails = new CopyOnWriteArrayList<>();
                                JSONArray jsonArray;
                                JSONObject checksumObject;
                                try {
                                    checksumObject = jsonObject.getJSONObject(kChecksumDetails);
                                    CheckSumDetails checkSumDetails = new CheckSumDetails(checksumObject);
                                    //checkSumDetails.add(new CheckSumDetails(checksumObject));
                                    GenricResponse<CheckSumDetails> genericFeeds =
                                            new GenricResponse<>(checkSumDetails);
                                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericFeeds));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    DispatchQueue.main(
                                            () -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                                }
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * API will used to generate Hashcode for PayUMoney,
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void generateHashCode(Integer orderId, Block.Success<PayUmoneyDetails> success,
                                 Block.Failure failure) {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(kAuthToken, getCurrentUser().getAuthToken());
        parameters.put(kOrderId, orderId);
        dispatchQueue.async(() -> {
            APIManager.APIManager()
                    .processRawRequest(kGenerateHashCode, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                JSONObject jsonObject = genricResponse.getObject();
                                JSONArray jsonArray;
                                JSONObject payUdetail;
                                try {
                                    payUdetail = jsonObject.getJSONObject(kPayUmoneyHashDetails);
                                    PayUmoneyDetails hashdetail = new PayUmoneyDetails(payUdetail);
                                    GenricResponse<PayUmoneyDetails> genericFeeds = new GenricResponse<>(hashdetail);
                                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericFeeds));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    DispatchQueue.main(
                                            () -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                                }
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * method to cancel the order placed by the logged in user
     *
     * @param success Block passed as callback for success conditgion
     * @param failure Block passed as callback for failure condition
     */
    public void cancelPlacedOrder(int orderId, Block.Success<HashMap<String, Object>> success,
                                  Block.Failure failure) {
        final HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(BaseModel.kAuthToken, getCurrentUser().getAuthToken());
        parameters.put(BaseModel.kOrderId, orderId);
        dispatchQueue.async(() -> {
            APIManager.APIManager()
                    .processRawRequest(kCancelPlacedOrder, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                try {
                                    JSONObject jsonObject = genricResponse.getObject();
                                    HashMap<String, Object> orderMap = new HashMap<>();
                                    orderMap.put(kOrderId, jsonObject.getInt(kOrderId));
                                    orderMap.put(kIsSuccessfullyCanceled, jsonObject.getInt(kIsSuccessfullyCanceled));

                                    GenricResponse<HashMap<String, Object>> genericCancelOrder =
                                            new GenricResponse<>(orderMap);
                                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericCancelOrder));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    DispatchQueue.main(
                                            () -> failure.iFailure(iStatus, kMessageInternalInconsistency));
                                }
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * method to remove the order placed by the logged in user
     *
     * @param success Block passed as callback for success conditgion
     * @param failure Block passed as callback for failure condition
     */
    public void removePlacedOrder(int orderId, Block.Success<HashMap<String, Object>> success,
                                  Block.Failure failure) {
        final HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(BaseModel.kAuthToken, getCurrentUser().getAuthToken());
        parameters.put(BaseModel.kOrderId, orderId);
        dispatchQueue.async(() -> {
            APIManager.APIManager()
                    .processRawRequest(kRemovePlacedOrder, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                try {
                                    JSONObject jsonObject = genricResponse.getObject();
                                    HashMap<String, Object> orderMap = new HashMap<>();
                                    orderMap.put(kOrderId, jsonObject.getInt(kOrderId));
                                    orderMap.put(kIsSuccessfullyRemoved, jsonObject.getInt(kIsSuccessfullyRemoved));

                                    GenricResponse<HashMap<String, Object>> genericCancelOrder =
                                            new GenricResponse<>(orderMap);
                                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericCancelOrder));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    DispatchQueue.main(
                                            () -> failure.iFailure(iStatus, kMessageInternalInconsistency));
                                }
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * method to get the order status placed by the logged in user
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getOrderCurrentStatus(int orderId, Block.Success<String> success, Block.Failure failure) {
        final HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(BaseModel.kAuthToken, getCurrentUser().getAuthToken());
        parameters.put(BaseModel.kOrderId, orderId);
        dispatchQueue.async(() -> {
            APIManager.APIManager()
                    .processRawRequest(kGetOrderCurrentStatus, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                try {
                                    JSONObject jsonObject = genricResponse.getObject();
                                    String orderStatus = jsonObject.getString(kOrderStatus);

                                    GenricResponse<String> genericShop = new GenricResponse<>(orderStatus);
                                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericShop));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    DispatchQueue.main(
                                            () -> failure.iFailure(iStatus, kMessageInternalInconsistency));
                                }
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * method to get the order status list by the logged in user
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getOrderStatusList(Block.Success<CopyOnWriteArrayList<OrderStatusList>> success, Block.Failure failure) {
        final HashMap<String, Object> parameters = new HashMap<>();
        dispatchQueue.async(() -> {
            APIManager.APIManager()
                    .processRawRequest(kGetOrderStatus, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                try {
                                    JSONObject jsonObject = genricResponse.getObject();

                                    CopyOnWriteArrayList<OrderStatusList> orderStatusList = new CopyOnWriteArrayList<>();
                                    JSONArray jsonArray = jsonObject.getJSONArray(kOrderStatus);
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        OrderStatusList statusList = new OrderStatusList(jsonArray.getJSONObject(i));
                                        orderStatusList.add(statusList);
                                    }
                                    getCurrentUser().setOrderStatus(orderStatusList);
                                    archiveCurrentUser();
                                    GenricResponse<CopyOnWriteArrayList<OrderStatusList>> genericList =
                                            new GenricResponse<>(orderStatusList);
                                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericList));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    DispatchQueue.main(() -> failure.iFailure(iStatus, kMessageInternalInconsistency));
                                }
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    public void getOrderHistory(Integer page, Block.Success<HashMap<String, Object>> success,
                                Block.Failure failure) {
        final HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(kAuthToken, getCurrentUser().getAuthToken());
        parameters.put(kPage, page);
        dispatchQueue.async(() -> {
            APIManager.APIManager().processRawRequest(kGetOrderHistory, parameters,
                    (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                        try {
                            JSONObject jsonObject = genricResponse.getObject();
                            HashMap<String, Object> ordermap = new HashMap<>();
                            Integer totalRecords = (Integer) jsonObject.get(kTotalRecords);

                            CopyOnWriteArrayList<OrderHistory> orderHistoryList =
                                    new CopyOnWriteArrayList<>();
                            JSONArray jsonArray = jsonObject.getJSONArray(kRecords);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                OrderHistory orderhistory = new OrderHistory(jsonArray.getJSONObject(i));
                                orderHistoryList.add(orderhistory);
                            }
                            ordermap.put(kTotalRecords, totalRecords);
                            ordermap.put(kRecords, orderHistoryList);
                            GenricResponse<HashMap<String, Object>> genrichistory =
                                    new GenricResponse<>(ordermap);
                            DispatchQueue.main(() -> success.iSuccess(iStatus, genrichistory));
                        } catch (Exception e) {
                            e.printStackTrace();
                            DispatchQueue
                                    .main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                        }
                    }, (Status statusFail, String message) -> {
                        DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                    });
        });
    }

    public void getReceivedOrder(int shopId, int page, int orderStatus,
                                 Block.Success<HashMap<String, Object>> success, Block.Failure failure) {
        final HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(kAuthToken, getCurrentUser().getAuthToken());
        parameters.put(kShopId, shopId);
        parameters.put(kPage, page);
        parameters.put(kOrderStatus, orderStatus);
        dispatchQueue.async(() -> {
            APIManager.APIManager().processRawRequest(kGetRecievedOrder, parameters,
                    (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                        try {
                            JSONObject jsonObject = genricResponse.getObject();
                            HashMap<String, Object> ordermap = new HashMap<>();
                            Integer totalRecords = (Integer) jsonObject.get(kTotalRecords);

                            CopyOnWriteArrayList<OrderHistory> orderHistoryList =
                                    new CopyOnWriteArrayList<>();
                            JSONArray jsonArray = jsonObject.getJSONArray(kRecords);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                OrderHistory orderhistory = new OrderHistory(jsonArray.getJSONObject(i));
                                orderHistoryList.add(orderhistory);
                            }
                            ordermap.put(kTotalRecords, totalRecords);
                            ordermap.put(kRecords, orderHistoryList);

                            GenricResponse<HashMap<String, Object>> genrichistory = new GenricResponse<>(
                                    ordermap);
                            DispatchQueue.main(() -> success.iSuccess(iStatus, genrichistory));
                        } catch (Exception e) {
                            e.printStackTrace();
                            DispatchQueue.main(
                                    () -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                        }
                    }, (Status statusFail, String message) -> {
                        DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                    });
        });
    }

    /**
     * method to remove the order placed by the logged in user
     *
     * @param success Block passed as callback for success conditgion
     * @param failure Block passed as callback for failure condition
     */
    public void changeOrderStatus(int orderId, int orderStatus,
                                  Block.Success<HashMap<String, Object>> success,
                                  Block.Failure failure) {
        final HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(BaseModel.kAuthToken, getCurrentUser().getAuthToken());
        parameters.put(BaseModel.kOrderId, orderId);
        parameters.put(BaseModel.kOrderStatus, orderStatus);
        dispatchQueue.async(() -> {
            APIManager.APIManager().processRawRequest(kChangeOrderStatus, parameters,
                    (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                        try {
                            JSONObject jsonObject = genricResponse.getObject();
                            HashMap<String, Object> orderMap = new HashMap<>();
                            orderMap.put(kOrderId, jsonObject.getInt(kOrderId));
                            orderMap.put(kOrderStatus, jsonObject.getInt(kOrderStatus));

                            GenricResponse<HashMap<String, Object>> genericCancelOrder = new GenricResponse<>(
                                    orderMap);
                            DispatchQueue.main(() -> success.iSuccess(iStatus, genericCancelOrder));
                        } catch (JSONException e) {
                            e.printStackTrace();
                            DispatchQueue.main(
                                    () -> failure.iFailure(iStatus, kMessageInternalInconsistency));
                        }
                    }, (Status statusFail, String message) -> {
                        DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                    });
        });
    }


    /**
     * method to check the registration for particular contact id
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void checkRegistration(String contactId, Block.Success<HashMap<String, Object>> success,
                                  Block.Failure failure) {
        final HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(BaseModel.kContactId, contactId);
        dispatchQueue.async(() -> {
            APIManager.APIManager().processRawRequest(kCheckRegistration, parameters, (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                try {
                    JSONObject jsonObject = genricResponse.getObject();
                    HashMap<String, Object> registrationMap = new HashMap<>();
                    registrationMap.put(kContactId, jsonObject.getString(kContactId));
                    registrationMap.put(kAuthToken, jsonObject.getString(kAuthToken));
                    registrationMap.put(kIsAlreadyRegistered,
                            jsonObject.getInt(kIsAlreadyRegistered));

                    GenricResponse<HashMap<String, Object>> mapGenricResponse =
                            new GenricResponse<>(registrationMap);
                    DispatchQueue.main(() -> success.iSuccess(iStatus, mapGenricResponse));
                } catch (JSONException e) {
                    e.printStackTrace();
                    DispatchQueue.main(
                            () -> failure.iFailure(iStatus, kMessageInternalInconsistency));
                }
            }, (Status statusFail, String message) -> {
                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
            });
        });
    }

    /**
     * method to login the user with help of otp (case forgot password)
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void loginByOtp(String contactId, Block.Success<HashMap<String, Object>> success,
                           Block.Failure failure) {
        final HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(BaseModel.kContactId, contactId);
        dispatchQueue.async(() -> {
            APIManager.APIManager()
                    .processRawRequest(kLoginByOtp, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                try {
                                    JSONObject jsonObject = genricResponse.getObject();
                                    HashMap<String, Object> loginMap = new HashMap<>();
                                    loginMap.put(kContactId, jsonObject.getString(kContactId));
                                    loginMap.put(kAuthToken, jsonObject.getString(kAuthToken));
                                    loginMap.put(kOTP, jsonObject.getString(kOTP));

                                    GenricResponse<HashMap<String, Object>> genericCancelOrder =
                                            new GenricResponse<>(loginMap);
                                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericCancelOrder));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    DispatchQueue.main(
                                            () -> failure.iFailure(iStatus, kMessageInternalInconsistency));
                                }
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * method to authenticate user after login otp process
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void authenticateUser(String contactId, Block.Success<HashMap<String, Object>> success,
                                 Block.Failure failure) {
        final HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(BaseModel.kContactId, contactId);
        parameters.put(BaseModel.kStatus, "OTP Varified");
        dispatchQueue.async(() -> {
            APIManager.APIManager()
                    .processRawRequest(kAuthenticateUser, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                try {
                                    JSONObject jsonObject = genricResponse.getObject();
                                    HashMap<String, Object> loginMap = new HashMap<>();
                                    loginMap.put(kContactId, jsonObject.getString(kContactId));
                                    loginMap.put(kAuthToken, jsonObject.getString(kAuthToken));

                                    GenricResponse<HashMap<String, Object>> genericCancelOrder =
                                            new GenricResponse<>(loginMap);
                                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericCancelOrder));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    DispatchQueue.main(
                                            () -> failure.iFailure(iStatus, kMessageInternalInconsistency));
                                }
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * method to register the user to app after otp verification
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void validateUserCredentials(String contactId, int requestType, String password,
                                        Block.Success<HashMap<String, Object>> success, Block.Failure failure) {
        final HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(BaseModel.kContactId, contactId);
        parameters.put(BaseModel.kPassword, password);
        parameters.put(BaseModel.kRequestType, requestType);
        dispatchQueue.async(() -> {
            APIManager.APIManager()
                    .processRawRequest(kValidateUserCredentials, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                try {
                                    JSONObject jsonObject = genricResponse.getObject();
                                    HashMap<String, Object> loginMap = new HashMap<>();
                                    loginMap.put(kContactId, jsonObject.getString(kContactId));
                                    loginMap.put(kAuthToken, jsonObject.getString(kAuthToken));
                                    loginMap.put(kOTP, jsonObject.getString(kOTP));

                                    GenricResponse<HashMap<String, Object>> genericCancelOrder =
                                            new GenricResponse<>(loginMap);
                                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericCancelOrder));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    DispatchQueue.main(
                                            () -> failure.iFailure(iStatus, kMessageInternalInconsistency));
                                }
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * method to register the user to app after otp verification
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void registerUser(String contactId, String password,
                             Block.Success<HashMap<String, Object>> success, Block.Failure failure) {
        final HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(BaseModel.kContactId, contactId);
        parameters.put(BaseModel.kPassword, password);
        parameters.put(BaseModel.kStatus, "OTP Varified");
        dispatchQueue.async(() -> {
            APIManager.APIManager()
                    .processRawRequest(kRegisterUser, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                try {
                                    JSONObject jsonObject = genricResponse.getObject();
                                    HashMap<String, Object> loginMap = new HashMap<>();
                                    loginMap.put(kContactId, jsonObject.getString(kContactId));
                                    loginMap.put(kAuthToken, jsonObject.getString(kAuthToken));

                                    GenricResponse<HashMap<String, Object>> genericRes =
                                            new GenricResponse<>(loginMap);
                                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericRes));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    DispatchQueue.main(
                                            () -> failure.iFailure(iStatus, kMessageInternalInconsistency));
                                }
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * method will be called when to get invite link for share
     * request with raw_data as it may include file e.g.text-data
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getInviteLink(Block.Success<String> success, Block.Failure failure) {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(kAuthToken, getCurrentUser().getAuthToken());
        dispatchQueue.async(() -> {
            APIManager.APIManager()
                    .processRawRequest(APIRequestHelper.kInviteLink, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                JSONObject jsonObject = genricResponse.getObject();
                                String invitelink = null;
                                try {
                                    invitelink = jsonObject.getString(kShareLink);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                GenricResponse<String> genricString = new GenricResponse<String>(invitelink);
                                DispatchQueue.main(() -> success.iSuccess(iStatus, genricString));
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * API will used to edit the product inventory
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void editProductInventory(HashMap<String, Object> parameters,
                                     Block.Success<ProductInventory> success, Block.Failure failure) {
        parameters.put(kAuthToken, getCurrentUser().getAuthToken());
        dispatchQueue.async(() -> {
            APIManager.APIManager().processRawRequest(kEditProductInventory, parameters,
                    (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                        JSONObject jsonObject = genricResponse.getObject();
                        try {
                            ProductInventory inventory = new ProductInventory(jsonObject);
                            GenricResponse<ProductInventory> genericShop = new GenricResponse<>(inventory);
                            DispatchQueue.main(() -> success.iSuccess(iStatus, genericShop));
                        } catch (Exception e) {
                            e.printStackTrace();
                            DispatchQueue.main(
                                    () -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                        }
                    }, (Status statusFail, String message) -> {
                        DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                    });
        });
    }

    /**
     * method to call th seller agreement for the shop details
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getSellerAgreement(Block.Success<String> success, Block.Failure failure) {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(kAuthToken, getGenericAuthToken());
        dispatchQueue.async(() -> {
            APIManager.APIManager().processRawRequest(APIRequestHelper.kGetSellerAgreement, parameters,
                    (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                        JSONObject jsonObject = genricResponse.getObject();
                        String formattedHtml = null;
                        try {
                            formattedHtml = jsonObject.getString(kFormattedHtml);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        GenricResponse<String> genericString = new GenricResponse<>(formattedHtml);
                        DispatchQueue.main(() -> success.iSuccess(iStatus, genericString));
                    }, (Status statusFail, String message) -> {
                        DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                    });
        });
    }

    /**
     * method to call th seller agreement for the shop details
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void receiveMessageHistory(Integer Id, Integer page,
                                      Block.Success<CopyOnWriteArrayList<HashMap<String, Object>>> success, Block.Failure failure) {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(kAuthToken, getCurrentUser().getAuthToken());
        parameters.put(kShopId, Id);
        parameters.put(kPage, page);
        dispatchQueue.async(() -> {
            APIManager.APIManager().processRawRequest(APIRequestHelper.kReceiveMessageHistory, parameters,
                    (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                        try {
                            JSONObject jsonObject = genricResponse.getObject();
                            Integer totalRecords = (Integer) jsonObject.get(kTotalRecords);
                            CopyOnWriteArrayList<HashMap<String, Object>> hashMaps = new CopyOnWriteArrayList<>();
                            JSONArray jsonArray = jsonObject.getJSONArray(kRecords);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                HashMap<String, Object> hashMap = new HashMap<>();
                                Integer messageId = object.getInt(MESSAGE_ID);
                                String message = object.getString(kMessage);
                                Integer messageType = object.getInt(MESSAGE_TYPE);
                                Integer messageDirection = object.getInt(MESSAGE_DIRECTION);
                                String time = object.getString(kTimestamp);
                                Integer shopId = object.getInt(kShopId);
                                hashMap.put(MESSAGE_ID, messageId);
                                hashMap.put(kMessage, message);
                                hashMap.put(MESSAGE_TYPE, messageType);
                                hashMap.put(MESSAGE_DIRECTION, messageDirection);
                                hashMap.put(kTimeStamp, time);
                                hashMap.put(kShopId, shopId);
                                hashMap.put(kTotalRecords, totalRecords);
                                hashMaps.add(hashMap);
                            }
                            GenricResponse<CopyOnWriteArrayList<HashMap<String, Object>>> hashMapGenricResponse = new GenricResponse<>(
                                    hashMaps);
                            DispatchQueue.main(() -> success.iSuccess(iStatus, hashMapGenricResponse));
                        } catch (Exception e) {
                            e.printStackTrace();
                            DispatchQueue
                                    .main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                        }
                    }, (Status statusFail, String message) -> {
                        DispatchQueue.main(() -> failure.iFailure(statusFail, kMessageInternalInconsistency));
                    });
        });
    }

    /**
     * method to call th seller agreement for the shop details
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void sendMessage(Integer shopId, String messagi,
                            Block.Success<HashMap<String, Object>> success, Block.Failure failure) {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(kAuthToken, getCurrentUser().getAuthToken());
        parameters.put(kShopId, shopId);
        parameters.put(kMessage, messagi);
        dispatchQueue.async(() -> {
            APIManager.APIManager().processRawRequest(APIRequestHelper.kSendMessageToOpponent, parameters,
                    (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                        try {
                            JSONObject jsonObject = genricResponse.getObject();
                            HashMap<String, Object> hashMap = new HashMap<>();
                            String message = jsonObject.getString(kMessage);
                            String time = jsonObject.getString(kTimeStamp);
                            Integer messageDirection = jsonObject.getInt(MESSAGE_DIRECTION);
                            hashMap.put(kMessage, message);
                            hashMap.put(MESSAGE_DIRECTION, messageDirection);
                            hashMap.put(kTimeStamp, time);
                            GenricResponse<HashMap<String, Object>> hashMapGenricResponse = new GenricResponse<>(
                                    hashMap);
                            DispatchQueue.main(() -> success.iSuccess(iStatus, hashMapGenricResponse));
                        } catch (Exception e) {
                            e.printStackTrace();
                            DispatchQueue
                                    .main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                        }
                    }, (Status statusFail, String message) -> {
                        DispatchQueue.main(() -> failure.iFailure(statusFail, kMessageInternalInconsistency));
                    });
        });
    }

    /**
     * method to call the tax info for add and edit product
     *
     * @param status  Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getTaxInfo(Block.Status status, Block.Failure failure) {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(kAuthToken, getGenericAuthToken());
        dispatchQueue.async(() -> {
            APIManager.APIManager().processRawRequest(APIRequestHelper.kGetTaxList, parameters, (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                try {
                    JSONObject jsonObject = genricResponse.getObject();
                    JSONArray jsonArray = jsonObject.getJSONArray(kTaxList);
                    CopyOnWriteArrayList<TAX> taxes = new CopyOnWriteArrayList<>();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        TAX tax = new TAX(jsonArray.getJSONObject(i));
                        taxes.add(tax);
                    }
                    getCurrentUser().setTaxList(taxes);
                    archiveCurrentUser();
                    DispatchQueue.main(() -> status.iStatus(iStatus));
                } catch (Exception e) {
                    e.printStackTrace();
                    DispatchQueue.main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                }
            }, (Status statusFail, String message) -> {
                DispatchQueue.main(() -> failure.iFailure(statusFail, kMessageInternalInconsistency));
            });
        });
    }

    /**
     * method to validate the referral code by the registering user
     *
     * @param status  Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void validateReferralCode(String code, String authToken, Block.Success<HashMap<String, Object>> status, Block.Failure failure) {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(BaseModel.kAuthToken, authToken);
        parameters.put(BaseModel.kReferralCode, code);
        dispatchQueue.async(() -> {
            APIManager.APIManager().processRawRequest(APIRequestHelper.kValidateReferralCode, parameters, (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                try {
                    HashMap<String, Object> map = new HashMap<>();
                    JSONObject jsonObject = genricResponse.getObject();
                    map.put(kIsReferralCodeValidated, jsonObject.getInt(kIsReferralCodeValidated));
                    map.put(kReferralCodeMessage, jsonObject.getString(kReferralCodeMessage));
                    GenricResponse<HashMap<String, Object>> hashMap = new GenricResponse<>(map);
                    DispatchQueue.main(() -> status.iSuccess(iStatus, hashMap));
                } catch (Exception e) {
                    e.printStackTrace();
                    DispatchQueue.main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                }
            }, (Status statusFail, String message) -> {
                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
            });
        });
    }

    /**
     * method to get the list of top shops by the logged in user.
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getFeatureShopList(int page, String lat, String lon, Block.Success<CopyOnWriteArrayList<ShopModel>> success, Block.Failure failure) {
        final HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(kAuthToken, getCurrentUser().getAuthToken());
        parameters.put(kLatitude, lat);
        parameters.put(kLongitude, lon);
        parameters.put(kPage, page);
        dispatchQueue.async(() -> {
            APIManager.APIManager().processRawRequest(kGetFeaturedShopList, parameters, (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                try {
                    JSONObject jsonObject = genricResponse.getObject();
                    CopyOnWriteArrayList<ShopModel> shopList = new CopyOnWriteArrayList<>();
                    JSONArray jsonArray = jsonObject.getJSONArray(kRecords);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        ShopModel shop = new ShopModel(jsonArray.getJSONObject(i));
                        shopList.add(shop);
                        readyPicasso(shop.getBannerUrl());
                    }
                    getCurrentUser().setFeatureShopList(shopList);
                    archiveCurrentUser();
                    GenricResponse<CopyOnWriteArrayList<ShopModel>> genericList =
                            new GenricResponse<>(shopList);
                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericList));
                } catch (Exception e) {
                    e.printStackTrace();
                    DispatchQueue.main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                }
            }, (Status statusFail, String message) -> {
                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
            });
        });
    }

    /**
     * method to get the list of top shops by the logged in user.
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getShopProduct(int shopId, int productTypeId, int page,
                               Block.Success<CopyOnWriteArrayList<ShopProduct>> success, Block.Failure failure) {
        final HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(kAuthToken, getCurrentUser().getAuthToken());
        parameters.put(kProductTypeId, productTypeId);
        parameters.put(kShopId, shopId);
        parameters.put(kPage, page);
        dispatchQueue.async(() -> {
            APIManager.APIManager().processRawRequest(kGetShopProducts, parameters,
                    (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                        try {
                            JSONObject jsonObject = genricResponse.getObject();
                            CopyOnWriteArrayList<ShopProduct> productList = new CopyOnWriteArrayList<>();
                            JSONArray jsonArray = jsonObject.getJSONArray(kRecords);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                try {
                                    ShopProduct shopProduct = new ShopProduct(jsonArray.getJSONObject(i));
                                    productList.add(shopProduct);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            GenricResponse<CopyOnWriteArrayList<ShopProduct>> genericList =
                                    new GenricResponse<>(productList);
                            DispatchQueue.main(() -> success.iSuccess(iStatus, genericList));
                        } catch (Exception e) {
                            e.printStackTrace();
                            DispatchQueue.main(
                                    () -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                        }
                    }, (Status statusFail, String message) -> {
                        DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                    });
        });
    }

    /**
     * method to get the list of shop category by the logged in user.
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getShopCategoryListSeller(int shopId,
                                          Block.Success<CopyOnWriteArrayList<ShopCategory>> success, Block.Failure failure) {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(kAuthToken, mCurrentUser.getAuthToken());
        parameters.put(kShopId, shopId);
        dispatchQueue.async(() -> {
            APIManager.APIManager()
                    .processRawRequest(APIRequestHelper.kGetShopCategoryListAtSellerEnd, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                try {
                                    JSONObject jsonObject = genricResponse.getObject();
                                    CopyOnWriteArrayList<ShopCategory> categoryList = new CopyOnWriteArrayList<>();
                                    JSONArray jsonArray = jsonObject.getJSONArray(kRecords);
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        categoryList.add(new ShopCategory(jsonArray.getJSONObject(i)));
                                    }
                                    GenricResponse<CopyOnWriteArrayList<ShopCategory>> genericList =
                                            new GenricResponse<>(categoryList);
                                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericList));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    DispatchQueue.main(
                                            () -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                                }
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * method to create a new celebration event to peeps by user that is loged-in
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void addCelebrationEvent(DataBaseHandler dataBaseHandler, HashMap<String, Object> parameters, Block.Success<CelebrationEvent> success, Block.Failure failure) {
        parameters.put(kAuthToken, getCurrentUser().getAuthToken());
        dispatchQueue.async(() -> {
            APIManager.APIManager().processFormRequest(APIRequestHelper.kAddCelebrationEvent, parameters, (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                try {
                    CelebrationEvent event = new CelebrationEvent(genricResponse.getObject());
                    dataBaseHandler.addEvents(event);
                    GenricResponse<CelebrationEvent> genericFeed = new GenricResponse<>(event);
                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericFeed));
                } catch (Exception e) {
                    e.printStackTrace();
                    DispatchQueue.main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                }
            }, (Status statusFail, String message) -> {
                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
            });
        });
    }

    /**
     * method to edit existing celebration event to peeps by user that is loged-in
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void editCelebrationEvent(DataBaseHandler dataBaseHandler, HashMap<String, Object> parameters,
                                     Block.Success<CelebrationEvent> success,
                                     Block.Failure failure) {
        parameters.put(kAuthToken, getCurrentUser().getAuthToken());
        dispatchQueue.async(() -> {
            APIManager.APIManager()
                    .processFormRequest(APIRequestHelper.kEditCelebrationEvent, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                try {
                                    CelebrationEvent event = new CelebrationEvent(genricResponse.getObject());
                                    dataBaseHandler.updateEvents(event);
                                    GenricResponse<CelebrationEvent> genericFeed = new GenricResponse<>(event);
                                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericFeed));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    DispatchQueue
                                            .main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                                }
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * method to delete a celebration event to peeps by user that is loged-in
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void deleteCelebrationEvent(DataBaseHandler dataBaseHandler, int userId, int eventId,
                                       Block.Status success,
                                       Block.Failure failure) {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(kAuthToken, getCurrentUser().getAuthToken());
        parameters.put(kUserId, userId);
        parameters.put(kEventId, eventId);
        dispatchQueue.async(() -> {
            APIManager.APIManager()
                    .processRawRequest(APIRequestHelper.kDeleteCelebrationEvent, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                try {
                                    HashMap<String, Object> map = new HashMap<>();
                                    map.put(kEventId, genricResponse.getObject().getInt(kEventId));
                                    map.put(kUserId, genricResponse.getObject().getInt(kUserId));
                                    dataBaseHandler.deleteEvent(userId, eventId);
                                    GenricResponse<HashMap<String, Object>> genericFeed = new GenricResponse<>(map);
                                    DispatchQueue.main(() -> success.iStatus(iStatus));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    DispatchQueue
                                            .main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                                }
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * method to get the list of celebration events by the logged in user.
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getCelebrationEventList(int userId,
                                        Block.Success<CopyOnWriteArrayList<CelebrationEvent>> success, Block.Failure failure) {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(kAuthToken, mCurrentUser.getAuthToken());
        parameters.put(kUserId, userId);
        dispatchQueue.async(() -> {
            APIManager.APIManager()
                    .processRawRequest(APIRequestHelper.kGetCelebrationEventList, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                try {
                                    JSONObject jsonObject = genricResponse.getObject();
                                    CopyOnWriteArrayList<CelebrationEvent> eventsList = new CopyOnWriteArrayList<>();
                                    JSONArray jsonArray = jsonObject.getJSONArray(kRecords);
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        eventsList.add(new CelebrationEvent(jsonArray.getJSONObject(i)));
                                    }
                                    GenricResponse<CopyOnWriteArrayList<CelebrationEvent>> genericList =
                                            new GenricResponse<>(eventsList);
                                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericList));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    DispatchQueue
                                            .main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                                }
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * method to get the details of celebration event by the logged in user.
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getCelebrationEventDetails(int eventId,
                                           Block.Success<CelebrationEvent> success, Block.Failure failure) {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(kAuthToken, mCurrentUser.getAuthToken());
        parameters.put(kEventId, eventId);
        dispatchQueue.async(() -> {
            APIManager.APIManager()
                    .processRawRequest(APIRequestHelper.kGetCelebrationEventDetails, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                try {
                                    CelebrationEvent event = new CelebrationEvent(genricResponse.getObject());
                                    GenricResponse<CelebrationEvent> genericFeed = new GenricResponse<>(event);
                                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericFeed));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    DispatchQueue
                                            .main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                                }
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * method to get the list of celebration events images by the logged in user.
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getCelebrationEventImages(Block.Success<CopyOnWriteArrayList<EventImages>> success,
                                          Block.Failure failure) {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(kAuthToken, mCurrentUser.getAuthToken());
        dispatchQueue.async(() -> {
            APIManager.APIManager()
                    .processRawRequest(APIRequestHelper.kgetCelebrationEventImages, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                try {
                                    JSONObject jsonObject = genricResponse.getObject();
                                    CopyOnWriteArrayList<EventImages> eventsList = new CopyOnWriteArrayList<>();
                                    JSONArray jsonArray = jsonObject.getJSONArray(kRecords);
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        eventsList.add(new EventImages(jsonArray.getJSONObject(i)));
                                    }
                                    GenricResponse<CopyOnWriteArrayList<EventImages>> genericList =
                                            new GenricResponse<>(eventsList);
                                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericList));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    DispatchQueue
                                            .main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                                }
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * method to get the : API will return the all informations that required to prompt the user
     * regarding the updation of app.
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getLatestBuildInfo(HashMap<String, Object> parameters,
                                   Block.Success<CheckLatestBuild> success, Block.Failure failure) {
        dispatchQueue.async(() -> {
            APIManager.APIManager().processFormRequest(APIRequestHelper.kGetLatestBuildInfo, parameters,
                    (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                        try {
                            JSONObject jsonObject = genricResponse.getObject();
                            CheckLatestBuild latestBuild = new CheckLatestBuild(jsonObject);
                            GenricResponse<CheckLatestBuild> genericFeed = new GenricResponse<>(latestBuild);
                            DispatchQueue.main(() -> success.iSuccess(iStatus, genericFeed));
                        } catch (Exception e) {
                            e.printStackTrace();
                            DispatchQueue.main(
                                    () -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                        }
                    }, (Status statusFail, String message) -> {
                        DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                    });
        });
    }

    /**
     * method to get the list of celebration events by the logged in user.
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getCelebrationMessageList(
            Block.Success<CopyOnWriteArrayList<CelebrationMessage>> success, Block.Failure failure) {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(kAuthToken, mCurrentUser.getAuthToken());
        dispatchQueue.async(() -> {
            APIManager.APIManager()
                    .processRawRequest(APIRequestHelper.kGetCelebrationMessageList, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                try {
                                    JSONObject jsonObject = genricResponse.getObject();
                                    CopyOnWriteArrayList<CelebrationMessage> messageList = new CopyOnWriteArrayList<>();
                                    JSONArray jsonArray = jsonObject.getJSONArray(kRecords);
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        messageList.add(new CelebrationMessage(jsonArray.getJSONObject(i)));
                                    }
                                    GenricResponse<CopyOnWriteArrayList<CelebrationMessage>> genericList =
                                            new GenricResponse<>(messageList);
                                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericList));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    DispatchQueue
                                            .main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                                }
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * method to get the details of celebration event by the logged in user.
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void getCelebrationMessageDetails(int messageId,
                                             Block.Success<CelebrationMessage> success, Block.Failure failure) {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(kAuthToken, mCurrentUser.getAuthToken());
        parameters.put(kMSGId, messageId);
        dispatchQueue.async(() -> {
            APIManager.APIManager()
                    .processRawRequest(APIRequestHelper.kGetCelebrationMessageDetails, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                try {
                                    CelebrationMessage message = new CelebrationMessage(genricResponse.getObject());
                                    GenricResponse<CelebrationMessage> genericFeed = new GenricResponse<>(message);
                                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericFeed));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    DispatchQueue
                                            .main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                                }
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * method to delete a celebration event message by user that is loged-in
     *
     * @param success Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void deleteCelebrationMessage(int messageId, Block.Status success,
                                         Block.Failure failure) {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(kAuthToken, getCurrentUser().getAuthToken());
        parameters.put(kMSGId, messageId);
        dispatchQueue.async(() -> {
            APIManager.APIManager()
                    .processRawRequest(APIRequestHelper.kDeleteCelebrationMessage, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                try {
                                    DispatchQueue.main(() -> success.iStatus(iStatus));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    DispatchQueue
                                            .main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                                }
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }

    /**
     * method to set favourite and un-favourite event message by the logged in user.
     *
     * @param status  Block passed as callback for success condition
     * @param failure Block passed as callback for failure condition
     */
    public void setFavouriteMessage(int messageId, int favourite, Block.Status status,
                                    Block.Failure failure) {
        dispatchQueue.async(() -> {
            final HashMap<String, Object> parameters = new HashMap<>();
            parameters.put(BaseModel.kAuthToken, getCurrentUser().getAuthToken());
            parameters.put(BaseModel.kMSGId, messageId);
            parameters.put(BaseModel.kIsFavorite, favourite);
            APIManager.APIManager()
                    .processRawRequest(kSetFavoriteMessage, parameters,
                            (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                                try {
                                    JSONObject jsonObject = genricResponse.getObject();
                                    Integer fav = jsonObject.getInt(kIsFavorite);
                                    Integer messageID = jsonObject.getInt(kMSGId);
                                    /*Integer favCount = getCurrentUser().getFavoriteProductCount();
                                    if (fav == 1) {
                                        ++favCount;
                                    } else {
                                        --favCount;
                                    }
                                    getCurrentUser().setFavoriteProductCount(favCount);
                                    archiveCurrentUser();*/
                                    DispatchQueue.main(() -> status.iStatus(iStatus));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    DispatchQueue
                                            .main(() -> failure.iFailure(iStatus, kMessageInternalInconsistency));
                                }
                            }, (Status statusFail, String message) -> {
                                DispatchQueue.main(() -> failure.iFailure(statusFail, message));
                            });
        });
    }


    private void readyPicasso(String imageThumbUrl) {
   /* try {
      if (imageThumbUrl.isEmpty()) {
        return;
      }
      Picasso.with(ApplicationManager.getContext()).load(imageThumbUrl);
    } catch (Exception e) {
      e.printStackTrace();
    }*/
    }

    public CurrentUser getSavedUser() {
        return getDataFromPreferences(kCurrentUser, CurrentUser.class);
    }
}
