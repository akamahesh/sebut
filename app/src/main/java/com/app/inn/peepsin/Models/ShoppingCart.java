package com.app.inn.peepsin.Models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by root on 5/7/17.
 */

public class ShoppingCart extends BaseModel {

    private String totalCartPrice;
    private Integer cartItemCount;
    private CopyOnWriteArrayList<CartShop> shopList = new CopyOnWriteArrayList<>();

    public ShoppingCart(JSONObject jsonResponse){
        this.totalCartPrice = getValue(jsonResponse, kTotalCartPrice,String.class);
        this.cartItemCount  = getValue(jsonResponse,kCartItemCount,Integer.class);
        try {
            this.shopList = handleShopList(getValue(jsonResponse,kRecords,JSONArray.class));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private CopyOnWriteArrayList<CartShop> handleShopList(JSONArray jsonArray) throws JSONException {
        CopyOnWriteArrayList<CartShop> cartShopList = new CopyOnWriteArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            cartShopList.add(new CartShop(jsonArray.getJSONObject(i)));
        }
        return cartShopList;
    }

    public String getTotalCartPrice() {
        return totalCartPrice;
    }

    public Integer getCartItemCount() {
        return cartItemCount;
    }

    public CopyOnWriteArrayList<CartShop> getShopList() {
        return shopList;
    }
}
