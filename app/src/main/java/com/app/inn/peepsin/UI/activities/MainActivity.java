package com.app.inn.peepsin.UI.activities;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.google.firebase.messaging.FirebaseMessaging;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Libraries.Firebase.app.Config;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.fragments.Base.RootBoardFragment;
import com.app.inn.peepsin.UI.fragments.Base.RootConnectionFragment;
import com.app.inn.peepsin.UI.fragments.Base.RootShopFragment;
import com.app.inn.peepsin.UI.fragments.Base.RootMessageFragment;
import com.app.inn.peepsin.UI.fragments.Base.RootProfileFragment;
import com.app.inn.peepsin.UI.fragments.SellStuffModule.SellingStuffDetailFragment;
import com.app.inn.peepsin.UI.fragments.SellStuffModule.SellingStuffFragment;
import com.app.inn.peepsin.UI.helpers.ProgressUtil;
import com.app.inn.peepsin.UI.helpers.Toaster;
import java.lang.reflect.Field;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.app.inn.peepsin.Constants.Constants.kBody;
import static com.app.inn.peepsin.Constants.Constants.kTitle;

public class MainActivity extends AppCompatActivity implements
        SellingStuffDetailFragment.FragmentInteractionListener {

    private BroadcastReceiver mRegistrationBroadcastReceiver;
    /**
     * Indicate that app will be closed on next back press
     */
    private boolean isAppReadyToFinish = false;
    @BindView(R.id.progress_overlay) View progressOverlay;

    public static Intent getIntent(Context context) {
        return new Intent(context, MainActivity.class);
    }


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = item -> {
        Fragment selectedFragment = null;
        switch (item.getItemId()) {
            case R.id.navigation_feeds:
                selectedFragment = RootShopFragment.newInstance();
                break;
            case R.id.navigation_connections:
                selectedFragment = RootConnectionFragment.newInstance();
                break;
            case R.id.navigation_search:
                selectedFragment = RootBoardFragment.newInstance();
                break;
            case R.id.navigation_message:
                selectedFragment = RootMessageFragment.newInstance();
                break;
            case R.id.navigation_account:
                selectedFragment = RootProfileFragment.newInstance();
                break;
        }

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.root_container, selectedFragment);
        transaction.commit();
        return true;
    };

    private BottomNavigationView.OnNavigationItemReselectedListener onNavigationItemReselectedListener =
            item -> {};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
        ButterKnife.bind(this);
        handleFirebase();
        displayFirebaseRegID();
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.setOnNavigationItemReselectedListener(onNavigationItemReselectedListener);
        disableShiftMode(navigation);
        //changeFragment(RootShopFragment.newInstance());
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.root_container, RootShopFragment.newInstance());
        transaction.commit();
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        // NotificationUtils.clearNotifications(getApplicationContext());
    }

    @Override
    public void onBackPressed() {
        if (isAppReadyToFinish)
            finishAffinity();
        else {
            // BackStack is empty. For closing the app user have to tap the back button two times in two seconds.
            Toaster.toast(getString(R.string.another_click_for_leaving_app));
            isAppReadyToFinish = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    isAppReadyToFinish = false;
                }
            }, 2000);
        }
    }


    private void handleFirebase() {
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(intent.getAction().equals(Config.REGISTRATION_COMPLETE)){
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);
                    displayFirebaseRegID();
                }else if(intent.getAction().equals(Config.PUSH_NOTIFICATION)){
                    String notificationbody = intent.getStringExtra(kBody);
                    String title= intent.getStringExtra(kTitle);
                    Toaster.toast(title +" : "+notificationbody);

                }
            }
        };
    }

    void changeFragment(Fragment fragment) {
        String backstackname = fragment.getClass().getName();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        //transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        Fragment oldFrag = fragmentManager.findFragmentByTag(backstackname);
        if(oldFrag!=null)
            transaction.replace(R.id.root_container,oldFrag,oldFrag.getClass().getName());
        else
            transaction.replace(R.id.root_container,fragment,backstackname);
        transaction.addToBackStack(backstackname);
        // Commit the transaction
        transaction.commit();
    }

    private void displayFirebaseRegID() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);
        Log.e("Firebase ==> ", "Firebase reg id: " + regId);
       // Toaster.toast(regId);
        if (TextUtils.isEmpty(regId))
            Log.e("Firebase ==> ", "Firebase Reg Id is not received yet!");
    }


    // Method for disabling ShiftMode of BottomNavigationView
    @SuppressLint("RestrictedApi")
    private void disableShiftMode(BottomNavigationView view) {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
        try {
            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);
            for (int i = 0; i < menuView.getChildCount(); i++) {
                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
                item.setShiftingMode(false);
                // set once again checked value, so view will be updated
                item.setChecked(item.getItemData().isChecked());
            }
        } catch (NoSuchFieldException e) {
            Log.e("BNVHelper", "Unable to get shift mode field", e);
        } catch (IllegalAccessException e) {
            Log.e("BNVHelper", "Unable to change value of shift mode", e);
        }
    }


    @Override
    public void onEditProgress() {
        ProgressUtil.animateView(progressOverlay, View.VISIBLE, 1, 200);
    }

    @Override
    public void onEditDone() {
        ProgressUtil.animateView(progressOverlay, View.GONE, 0, 200);
    }
}
