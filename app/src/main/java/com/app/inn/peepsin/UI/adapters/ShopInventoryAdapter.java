package com.app.inn.peepsin.UI.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.inn.peepsin.Models.ShopCategory;
import com.app.inn.peepsin.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by dhruv on 23/8/17.
 */

public class ShopInventoryAdapter extends RecyclerView.Adapter<ShopInventoryAdapter.ViewHolder> {

    private List<ShopCategory> inventoryList;
    private Context context;
    private InventoryFilter inventoryFilterListener;
    int previousPosition = 0;


    public interface InventoryFilter{
        void filter(Integer id);
    }

    public ShopInventoryAdapter(Context context, List<ShopCategory> inventoryList, InventoryFilter inventoryFilterListener) {
        this.inventoryList = inventoryList;
        this.context = context;
        this.inventoryFilterListener = inventoryFilterListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_feeds_inventrory_base, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ShopCategory shop = inventoryList.get(position);
        holder.bindContent(shop);
        Integer id = shop.getId();
        String name         = shop.getName();
        String imageURL     = shop.getImageUrl();

        if (!imageURL.isEmpty()) {
            Picasso.with(context)
                    .load(imageURL)
                    .placeholder(R.drawable.img_product_placeholder)
                    .into(holder.ivItemImage);
        } else {
            holder.ivItemImage.setImageDrawable(context.getResources().getDrawable(R.drawable.img_product_placeholder));
        }

        if(shop.isSelected()){
            holder.ivItemImage.setBackgroundResource(R.drawable.canvas_circular_image_background);
            holder.tvItemName.setTextColor(context.getResources().getColor(R.color.favorite_highlight_color));

        }else{
            holder.ivItemImage.setBackgroundResource(R.color.window_background);
            holder.tvItemName.setTextColor(context.getResources().getColor(R.color.text_color_lite));
        }

        holder.tvItemName.setText(name);
    }

    @Override
    public int getItemCount() {
        return  inventoryList.size();
    }

    public void addItems(List<ShopCategory> inventoryList) {
        this.inventoryList.clear();
        this.inventoryList.addAll(inventoryList);
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        ShopCategory shopCategory;
        @BindView(R.id.iv_item_image)
        ImageView ivItemImage;
        @BindView(R.id.tv_item_name)
        TextView tvItemName;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        public void bindContent(ShopCategory shop) {
            this.shopCategory = shop;
        }

        @OnClick(R.id.item_view)
        void onInventory() {
            for (ShopCategory shopCategory:inventoryList) {
                shopCategory.setSelected(false);
            }
            shopCategory.setSelected(true);
            notifyDataSetChanged();
            inventoryFilterListener.filter(shopCategory.getId());
        }
    }
}
