package com.app.inn.peepsin.UI.helpers;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.app.inn.peepsin.Constants.Constants.kDistance;
import static com.app.inn.peepsin.Constants.Constants.kDuration;
import static com.app.inn.peepsin.Constants.Constants.kText;

/**
 * Created by anupamchugh on 27/11/15.
 */

public class DistanceJSONParser {

    /** Receives a JSONObject and returns a list of lists containing latitude and longitude */
    public List<Map<String, String>> parse(JSONObject jObject){

        List<Map<String,String>> routeMapList = new ArrayList<>();
        JSONArray jRoutes = null;
        JSONArray jLegs = null;
        JSONObject jDistance = null;
        JSONObject jDuration = null;

        try {

            jRoutes = jObject.getJSONArray("routes");

            /** Traversing all routes */
            for(int i=0;i<jRoutes.length();i++){
                jLegs = ( (JSONObject)jRoutes.get(i)).getJSONArray("legs");
                List path = new ArrayList<HashMap<String, String>>();

                /** Traversing all legs */
                for(int j=0;j<jLegs.length();j++){
                    jDistance = ( (JSONObject)jLegs.get(j)).getJSONObject(kDistance);
                    jDuration = ( (JSONObject)jLegs.get(j)).getJSONObject(kDuration);
                    Map<String,String> routeMap = new HashMap<>();
                    routeMap.put(kDistance,jDistance.getString(kText));
                    routeMap.put(kDuration,jDuration.getString(kText));
                    routeMapList.add(routeMap);

                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }catch (Exception e){
        }

        return routeMapList;
    }

    /**
     * Method to decode polyline points
     * Courtesy : http://jeffreysambells.com/2010/05/27/decoding-polylines-from-google-maps-direction-api-with-java
     * */
    private List decodePoly(String encoded) {

        List poly = new ArrayList();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }
}