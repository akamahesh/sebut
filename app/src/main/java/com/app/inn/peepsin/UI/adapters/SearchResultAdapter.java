package com.app.inn.peepsin.UI.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.CurrentUser;
import com.app.inn.peepsin.Models.Feeds;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.fragments.Feeds.MapDialogFragment;
import com.app.inn.peepsin.UI.fragments.Feeds.ProductDetailFragment;
import com.app.inn.peepsin.UI.helpers.MyBounceInterpolator;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import com.squareup.picasso.Picasso;

import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kImageUrl;
import static com.app.inn.peepsin.Constants.Constants.kLatitude;
import static com.app.inn.peepsin.Constants.Constants.kLongitude;
import static com.app.inn.peepsin.Constants.Constants.kOwnerName;

/**
 * Created by Harsh on 5/29/2017.
 */

public class SearchResultAdapter extends RecyclerView.Adapter<SearchResultAdapter.SearchResultViewHolder> {
    public CopyOnWriteArrayList<Feeds> feedsList;
    private Context context;
    private static Switcher switcherListener;
    private ProgressDialog progressDialog;
    private CurrentUser user;

    public SearchResultAdapter(Context context, CopyOnWriteArrayList<Feeds> items, Switcher switcher) {
        switcherListener = switcher;
        user = ModelManager.modelManager().getCurrentUser();
        progressDialog = Utils.generateProgressDialog(context, false);
        feedsList = items;
        this.context =context;
    }

    @Override
    public SearchResultViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_product_layout, parent, false);
        return new SearchResultViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(SearchResultAdapter.SearchResultViewHolder holder, int position) {
        Feeds item = feedsList.get(position);
        holder.bindContent(item);
        String name = item.getName() + "(In Stock :"+item.getQuantity()+")";
        String postingDate = context.getString(R.string.postedOn)+" " +item.getPostingDate();
        String thumbnailURL = item.getImageThumbUrl();
        String addressTxt = "No Location Available";
        if (item.getAddress() != null)
            addressTxt = item.getAddress().getCity() + ", " + item.getAddress().getState() +", "+ item.getAddress().getCountry();



        String sellerName = "";
        int connectionLevel = 0;
        if(item.getSeller()!=null){
            sellerName = item.getSeller().getFirstName()+" " +item.getSeller().getLastName();
            connectionLevel = item.getSeller().getConnectionLevel();
        }

        String price = item.getPrice();
        String sellingPrice = item.getSellingPrice();
        String discountTagline = item.getDiscountTagline();
        holder.tvSellingPrice.setText(sellingPrice);
        holder.tvPrice.setText(price);
        holder.tvPrice.setPaintFlags(holder.tvPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        holder.tvOffer.setText(discountTagline);



        holder.textViewItemName.setText(name);
        holder.textViewConnection.setText(Utils.getUserName(sellerName,connectionLevel));
        holder.textViewTime.setText(postingDate);
        if (item.getFavourite()) {
            holder.imageViewFav.setImageResource(R.drawable.ic_favorite);
        } else {
            holder.imageViewFav.setImageResource(R.drawable.ic_heart_empty);
        }
        if (!thumbnailURL.isEmpty())
            Picasso.with(context)
                    .load(thumbnailURL)
                    .placeholder(R.drawable.img_product_placeholder)
                    .resize(100, 100)
                    .into(holder.imageViewItem);
    }

    @Override
    public int getItemCount() {
        return feedsList.size();
    }

    public void addItems(CopyOnWriteArrayList<Feeds> items) {
        feedsList.clear();
        feedsList.addAll(items);
        notifyDataSetChanged();
    }

    public void clear() {
        feedsList.clear();
    }

    class SearchResultViewHolder extends RecyclerView.ViewHolder {
        Feeds item;
        @BindView(R.id.tv_name) TextView textViewItemName;
        @BindView(R.id.tv_connection) TextView textViewConnection;
        @BindView(R.id.tv_time) TextView textViewTime;
        @BindView(R.id.iv_item) ImageView imageViewItem;
        @BindView(R.id.iv_fav) ImageView imageViewFav;
        @BindView(R.id.tv_offer) TextView tvOffer;
        @BindView(R.id.tv_price) TextView tvPrice;
        @BindView(R.id.tv_selling_price) TextView tvSellingPrice;

        SearchResultViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.item_view)
        void onRowSelected(){
            getFeedDetails(item.getId());
        }

        private void getFeedDetails(int productId) {
            showProgress(true);
            ModelManager.modelManager().getProductDetails(productId,(Constants.Status iStatus, GenricResponse<Feeds> genericResponse) -> {
                showProgress(false);
                if (switcherListener != null){
                    switcherListener.switchFragment(ProductDetailFragment.newInstance(productId, genericResponse.getObject(), switcherListener), true, true);
                }
            }, (Constants.Status iStatus, String message) -> {
                Toaster.toast(message);
                showProgress(false);
            });
        }

        @OnClick(R.id.iv_fav)
        void makeFavourite(ImageView imageView) {
            Boolean isFavorite = item.getFavourite();
            Animation favAnim = AnimationUtils.loadAnimation(context, R.anim.bounce);
            MyBounceInterpolator interpolator = new MyBounceInterpolator(.2, 20);
            favAnim.setInterpolator(interpolator);
            imageView.startAnimation(favAnim);
            if(isFavorite){
                makeFavorite(0,item.getId(),getAdapterPosition());
            }else {
                makeFavorite(1,item.getId(),getAdapterPosition());
            }
        }
        private void makeFavorite(int isFav, Integer productId, int position) {
            ModelManager.modelManager().setFavouriteProduct(productId, isFav, (Constants.Status iStatus) -> {
                feedsList.get(position).setFavourite(isFav==1);
                notifyItemChanged(position);
            }, (Constants.Status iStatus, String message) -> {
                Toaster.toast(message);
            });
        }


        @OnClick(R.id.iv_location)
        void getLocation() {
            try{
                String lat = user.getPrimaryAddress().getLatitude();
                String sLat = item.getAddress().getLatitude();
                String sLon = item.getAddress().getLongitude();
                if(!lat.isEmpty() && !sLat.isEmpty()){
                    MapDialogFragment mapDialogFragment = MapDialogFragment.newInstance();
                    Bundle bdl = new Bundle();
                    bdl.putString(kLatitude,sLat);
                    bdl.putString(kLongitude,sLon);
                    bdl.putString(kOwnerName,item.getName());
                    bdl.putString(kImageUrl,item.getSeller().getProfilePicUrl());
                    mapDialogFragment.setArguments(bdl);
                    mapDialogFragment.setCancelable(true);
                    mapDialogFragment.show(((AppCompatActivity)context).getSupportFragmentManager(), mapDialogFragment.getClass().getSimpleName());

                }else
                    Toaster.toast("Sorry Lat Long UnAvailable");
            }catch (Exception e){
                Toaster.toast("Sorry Lat Long UnAvailable");
                e.printStackTrace();
            }
        }


        private void bindContent(Feeds item) {
            this.item = item;
        }
    }

    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) progressDialog.cancel();
        }
    }
}
