package com.app.inn.peepsin.Managers.EventManager;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.inn.peepsin.Models.CelebrationEvent;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.activities.HomeActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.app.inn.peepsin.Constants.Constants.kData;
import static com.app.inn.peepsin.Constants.Constants.kEventMap;

/**
 * Created by dhruv on 4/10/17.
 */

public class EventBroadcastManager extends BroadcastReceiver {

    private String title = "Celebration Event";
    private String message = "Many Congratulations to you";
    private String type = "Event";
    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bdl = intent.getExtras().getBundle(kData);
        if(intent.getAction().equals("event data")){
            if (bdl != null) {
                CelebrationEvent event = (CelebrationEvent)bdl.getSerializable(kEventMap);
                if (event != null) {
                    title = event.getEventTitle();
                    message = event.getMessage();
                    type = event.getEventTypeName();
                }

                if(isAppForeground(context))
                    eventToast(context,event);
                else
                    Notification(context);
            }
        }
    }

    public void Notification(Context context) {

        long currentTimeMillis = System.currentTimeMillis();
        // Open NotificationView Class on Notification Click
        Intent intent = new Intent(context, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        // Open NotificationView.java Activity
        PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        // Create Notification using NotificationCompat.Builder
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                // Set Icon
                .setSmallIcon(R.mipmap.ic_launcher)
                // Set Ticker Message
                .setTicker(message).setWhen(currentTimeMillis)
                // Set Title
                .setContentTitle(title+"("+type+")")
                // Set Text
                .setContentText(message)
                // Set Default types
                .setDefaults(Notification.DEFAULT_ALL)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                // Set PendingIntent into Notification
                .setContentIntent(pIntent)
                // Dismiss Notification
                .setAutoCancel(true);

        // Create Notification Manager
        NotificationManager notificationmanager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        // Build Notification with Notification Manager
        notificationmanager.notify(0, builder.build());
    }

    public static void eventToast(Context context,CelebrationEvent event) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.custom_event_toast, null);
        TextView tvTitle = (TextView) layout.findViewById(R.id.tv_title);
        TextView tvMessage = (TextView) layout.findViewById(R.id.tv_message);
        TextView tvType = (TextView) layout.findViewById(R.id.tv_event_type);
        ImageView ivEvent = (ImageView) layout.findViewById(R.id.iv_event);

        Toast toast = new Toast(context);
        toast.setGravity(Gravity.TOP, 0, 45);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);

        tvTitle.setText(event.getEventTitle());
        tvMessage.setText(event.getMessage());
        tvType.setText(event.getEventTypeName());
        if(event.getImageUrl()!=null)
            Picasso.with(context).load(event.getImageUrl())
                    .fit().placeholder(R.drawable.img_product_placeholder)
                    .error(R.drawable.img_product_placeholder).into(ivEvent);
        toast.show();
    }

    public boolean isAppForeground(Context mContext) {

        ActivityManager am = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            if (!topActivity.getPackageName().equals(mContext.getPackageName())) {
                return false;
            }
        }
        return true;
    }
}
