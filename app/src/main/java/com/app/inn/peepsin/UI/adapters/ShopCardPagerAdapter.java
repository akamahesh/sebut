package com.app.inn.peepsin.UI.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.Address;
import com.app.inn.peepsin.Models.CurrentUser;
import com.app.inn.peepsin.Models.ShopModel;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.activities.BaseActivity;
import com.app.inn.peepsin.UI.fragments.Feeds.FeedsFragment;
import com.app.inn.peepsin.UI.fragments.Feeds.MapDialogFragment;
import com.app.inn.peepsin.UI.fragments.Shop.NewShopDetailsFragment;
import com.app.inn.peepsin.UI.helpers.MyBounceInterpolator;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.interfaces.CardInterface;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kImageUrl;
import static com.app.inn.peepsin.Constants.Constants.kLatitude;
import static com.app.inn.peepsin.Constants.Constants.kLongitude;
import static com.app.inn.peepsin.Constants.Constants.kOwnerName;

public class ShopCardPagerAdapter extends PagerAdapter implements CardInterface {

    private List<CardView> mViews;
    private List<ShopModel> mData;
    private float mBaseElevation;
    private Context context;
    private Switcher switcherListener;

    public ShopCardPagerAdapter(Context context, Switcher switcher) {
        this.context = context;
        this.switcherListener = switcher;
        mViews = new ArrayList<>();
        mData = new ArrayList<>();
    }

    public void addCardItem(ShopModel item) {
        mViews.add(null);
        mData.add(item);
        notifyDataSetChanged();
    }

    @Override
    public float getPageWidth(int position) {
        return super.getPageWidth(position);
        /*if(BaseActivity.getSpan()==3)
            return (super.getPageWidth(position) / 2f);
        else
            return (super.getPageWidth(position) / 1.5f);*/
    }

    public float getBaseElevation() {
        return mBaseElevation;
    }

    @Override
    public CardView getCardViewAt(int position) {
        return mViews.get(position);
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.card_top_shop_layout, container, false);
        container.addView(view);
        ButterKnife.bind(this, view);
        bind(mData.get(position));
        /*CardView cardView = (CardView) view.findViewById(R.id.card_view);
        if (mBaseElevation == 0) {
            mBaseElevation = cardView.getCardElevation();
        }
        cardView.setMaxCardElevation(mBaseElevation * MAX_ELEVATION_FACTOR);
        mViews.set(position,cardView);*/
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
        //mViews.set(position, null);
    }

    @BindView(R.id.iv_banner_image)
    ImageView ivBanner;
    @BindView(R.id.iv_shop_logo)
    ImageView ivLogo;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.iv_location)
    ImageView ivLocation;
    @BindView(R.id.card_view)
    View itemView;
    private void bind(ShopModel item) {
        tvTitle.setText(item.getName());
        Address address = item.getAddress();
        String loc = "No Location Available";
        if(address!=null)
            loc = address.getCity()+","+address.getState()+","+address.getCountry();
        tvAddress.setText(loc);

        String bannerUrl = item.getBannerUrl();
        TextDrawable drawable = TextDrawable.builder().buildRoundRect(String.valueOf(item.getName().toUpperCase().charAt(0)), ColorGenerator.MATERIAL.getRandomColor(),5);
        if(!bannerUrl.isEmpty())
            Picasso.with(context)
                    .load(bannerUrl)
                    .fit()
                    .placeholder(drawable)
                    .error(drawable)
                    .into(ivBanner);
        else
            ivBanner.setImageDrawable(drawable);

        String logoUrl = item.getLogoUrl();
        if(!logoUrl.isEmpty())
            Picasso.with(context)
                    .load(logoUrl)
                    .fit()
                    .placeholder(R.drawable.img_shop_logo)
                    .error(R.drawable.img_shop_logo)
                    .into(ivLogo);
        else
            ivLogo.setImageResource(R.drawable.img_shop_logo);

        ivLocation.setOnClickListener(v -> getLocation(item));
        itemView.setOnClickListener(v -> {
            if(switcherListener!=null)
                switcherListener.switchFragment(NewShopDetailsFragment.newInstance(switcherListener,item.getId()),true,true);
                //switcherListener.switchFragment(FeedsFragment.newInstance(switcherListener,item.getId()),true,true);
        });
    }

    void getLocation(ShopModel item) {
        try{
            CurrentUser user = ModelManager.modelManager().getCurrentUser();
            String lat = user.getPrimaryAddress().getLatitude();
            String sLat = item.getAddress().getLatitude();
            String sLon = item.getAddress().getLongitude();
            if(!lat.isEmpty() && !sLat.isEmpty()){
                MapDialogFragment mapDialogFragment = MapDialogFragment.newInstance();
                Bundle bdl = new Bundle();
                bdl.putString(kLatitude,sLat);
                bdl.putString(kLongitude,sLon);
                bdl.putString(kOwnerName,item.getName());
                bdl.putString(kImageUrl,item.getBannerUrl());
                mapDialogFragment.setArguments(bdl);
                mapDialogFragment.setCancelable(true);
                mapDialogFragment.show(((AppCompatActivity)context).getSupportFragmentManager(), mapDialogFragment.getClass().getSimpleName());
            }else
                Toaster.toast("Sorry Lat Long UnAvailable");
        }catch (Exception e){
            Toaster.toast("Sorry Lat Long UnAvailable");
            e.printStackTrace();
        }
    }

}
