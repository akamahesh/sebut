package com.app.inn.peepsin.Models;

import android.util.Log;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 18/5/17.
 */

public class BillSummaryAfterPayment extends BaseModel implements Serializable{

    private Integer orderId;
    private String  promoCode;
    private String isValid;
    private String promoCodeTagLine;
    private String totalCartPriceBeforePromoCode;
    private String finalAmountAfterPromoCode;
    private String discountedPrice;
    private String totalItemCount;
    private Address address;
    private ProductList productList;
    private String expectedDeliveryDate;
    private String orderDate;
    private String orderStatus;
    private ShopAddress shopAddress;
    private FinalProductsRecords productRescords;
    //private String checksumHash;


    public BillSummaryAfterPayment(JSONObject productObject, Integer orderId, String promoCode, String isValid, String promoCodeTagLine, String totalCartPriceBeforePromoCode,
                                   String finalAmountAfterPromoCode, String discountedPrice, String totalItemCount,String expectedDeliveryDate,
                                   String orderDate,String orderStatus
                            ) {
       this.orderId              = orderId;
       this.promoCode            = promoCode;
       this.isValid              = isValid;
       this.promoCodeTagLine    = promoCodeTagLine;
       this.totalCartPriceBeforePromoCode        = totalCartPriceBeforePromoCode;
       this.finalAmountAfterPromoCode      = finalAmountAfterPromoCode;
       this.discountedPrice           = discountedPrice;
        this.totalItemCount      = totalItemCount;
        this.expectedDeliveryDate      = expectedDeliveryDate;
        this.orderDate           = orderDate;
        this.orderStatus      = orderStatus;
        try { //to handle @ClassCastException when addressId is emptyString, not to be removed
            this.address            = new Address(getValue(productObject, kDeliveryAddress,   JSONObject.class));
        }catch (Exception e){
            Log.e("Error : ",e.getMessage());
        }
        try { //to handle @ClassCastException when addressId is emptyString, not to be removed
            this.productRescords    = new FinalProductsRecords(getValue(productObject, kRecords,   JSONObject.class));
        }catch (Exception e){
            Log.e("Error : ",e.getMessage());
        }



    }


    public Integer getOrderId() {
        return orderId;
    }
    public void setOrderId(Integer orderId) {
        orderId = orderId;
    }
    public String getpromoCode() {
        return promoCode;
    }



    public String getIsValid() {
        return isValid;
    }

    public String getPromoCodeTagLine() {
        return promoCodeTagLine;
    }

    public String getTotalCartPriceBeforePromoCode() {
        return totalCartPriceBeforePromoCode;
    }

    public String getFinalAmountAfterPromoCode() {
        return finalAmountAfterPromoCode;
    }

    public String getdiscountedPrice() {
        return discountedPrice;
    }

    public String gettotalItemCount() {
        return totalItemCount;
    }

    public void setpromoCode(String promoCode) {
        promoCode = promoCode;
    }

    public void setIsValid(String isValid) {
        isValid = isValid;
    }

    public void setpromoCodeTagLine(String promoCodeTagLine) {
        promoCodeTagLine = promoCodeTagLine;
    }

    public void setTotalCartPriceBeforePromoCode(String totalCartPriceBeforePromoCode) {
        totalCartPriceBeforePromoCode = totalCartPriceBeforePromoCode;
    }

    public void setFinalAmountAfterPromoCode(String finalAmountAfterPromoCode) {
        finalAmountAfterPromoCode = finalAmountAfterPromoCode;
    }

    public void setdiscountedPrice(String discountedPrice) {
        discountedPrice = discountedPrice;
    }


    public void settotalItemCount(String totalItemCount) {
        totalItemCount = totalItemCount;
    }

    private List<String> ShopAddress(List<String>shopAddress) {
        List<String> list= new ArrayList<>();
        for(int i = 0; i<shopAddress.size();i++){

            list.add(shopAddress.get(i));

        }
        return list;
    }
    public void setShopid(Integer Shopid) {
        Shopid = Shopid;
    }


    public void setshopName(String shopName) {
        shopName = shopName;
    }


    public void setshopBannerImageURL(String shopBannerImageURL) {
        shopBannerImageURL = shopBannerImageURL;
    }





    public void setownerName(String ownerName) {
        ownerName = ownerName;
    }



    public void setconnectionLevel(String connectionLevel) {
        connectionLevel = connectionLevel;
    }



    public void settotalAmountWithoutDiscount(String totalAmountWithoutDiscount) {
        totalAmountWithoutDiscount = totalAmountWithoutDiscount;
    }


    public void setnetPaybaleAmount(String netPaybaleAmount) {
        netPaybaleAmount = netPaybaleAmount;
    }


}
