package com.app.inn.peepsin.UI.dialogFragments;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.helpers.Utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kName;
import static com.app.inn.peepsin.Constants.Constants.kRecords;
import static com.app.inn.peepsin.Constants.Constants.kTimeZone;

/**
 * Created by dhruv on 26/9/17.
 */

public class TimeZoneListDialogFragment extends DialogFragment {

    private ItemClickListener mListener;
    private List<HashMap<String, Object>> TimeZoneList = new ArrayList<>();
    private TimeZoneAdapter adapter;

    @BindView(R.id.edt_search)
    EditText edtSearch;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    public interface ItemClickListener {
        void onItemClicked(String name);
    }

    public static TimeZoneListDialogFragment newInstance(List<HashMap<String, Object>> timeZoneList) {
        TimeZoneListDialogFragment fragment = new TimeZoneListDialogFragment();
        Bundle args = new Bundle();
        args.putSerializable(kRecords, (Serializable) timeZoneList);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        final Fragment parent = getParentFragment();
        if (parent != null) {
            mListener = (ItemClickListener) parent;
        } else {
            mListener = (ItemClickListener) context;
        }
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.MY_DIALOG);
        Bundle bundle = getArguments();
        TimeZoneList = (List<HashMap<String, Object>>) bundle.getSerializable(kRecords);
        Dialog d = getDialog();
        if (d != null) {
            d.getWindow().setLayout(100, 100 );
            d.getWindow().getAttributes().windowAnimations = R.style.MaterialDialogSheetAnimation;
            Drawable drawable = getResources().getDrawable(R.drawable.canvas_dialog_bg);
            d.getWindow().setBackgroundDrawable(drawable);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_timezone_dialog, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        adapter = new TimeZoneAdapter(TimeZoneList);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
        edtSearch.setOnEditorActionListener(actionListener);
        edtSearch.addTextChangedListener(searchTextWatcher);
    }

    TextView.OnEditorActionListener actionListener = (v, actionId, event) -> {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            Utils.hideKeyboard(getContext());
        }
        return true;
    };

    TextWatcher searchTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

        @Override
        public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
            charSequence = charSequence.toString().toLowerCase();
            final List<HashMap<String, Object>> filteredList = new ArrayList<>();
            String name;
            for (int i = 0; i < TimeZoneList.size(); i++) {
                //skill area list
                name = TimeZoneList.get(i).get(kName).toString().toLowerCase();
                if (name.contains(charSequence)) {
                    filteredList.add(TimeZoneList.get(i));
                }
            }

            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            adapter = new TimeZoneAdapter(filteredList);
            recyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }

        @Override
        public void afterTextChanged(Editable s) {}
    };


    @Override
    public void onDetach() {
        mListener = null;
        super.onDetach();
    }

    class TimeZoneAdapter extends RecyclerView.Adapter<TimeZoneAdapter.ViewHolder> {

        List<HashMap<String, Object>> mapList;

        TimeZoneAdapter(List<HashMap<String, Object>> itemList) {
            mapList = itemList;
        }

        @Override
        public TimeZoneAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View layoutView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.fragment_skill_list_dialog_item, parent, false);
            return new TimeZoneAdapter.ViewHolder(layoutView);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            HashMap<String, Object> map = mapList.get(position);
            //skill area list
            String name = (String) map.get(kTimeZone);
            holder.tvItem.setText(name);
        }

        @Override
        public int getItemCount() {
            return mapList.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            @BindView(R.id.text)
            TextView tvItem;

            public ViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            @OnClick(R.id.text)
            void onItemClick() {
                if (mListener != null) {
                    mListener.onItemClicked(tvItem.getText().toString());
                    dismiss();
                }
            }

        }

    }
}
