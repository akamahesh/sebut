package com.app.inn.peepsin.Services;

import android.app.Activity;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.webkit.URLUtil;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Managers.UploadManager.UploadManager;
import com.app.inn.peepsin.Models.SellingProduct;
import com.app.inn.peepsin.UI.helpers.Toaster;

import java.util.HashMap;
import java.util.List;

import static com.app.inn.peepsin.Constants.Constants.kCoverImage;
import static com.app.inn.peepsin.Constants.Constants.kEmptyString;
import static com.app.inn.peepsin.Constants.Constants.kImageUploadId1;
import static com.app.inn.peepsin.Constants.Constants.kImageUploadId2;
import static com.app.inn.peepsin.Constants.Constants.kImageUploadId3;
import static com.app.inn.peepsin.Constants.Constants.kImageUploadId4;
import static com.app.inn.peepsin.Constants.Constants.kImageUploadId5;
import static com.app.inn.peepsin.Constants.Constants.kProductImageUrlList;
import static com.app.inn.peepsin.Constants.Constants.kRecords;
import static com.app.inn.peepsin.Constants.Constants.kRequestId;

/**
 * Created by akaMahesh on 12/7/17.
 * copyright to : Innverse Technologies
 */

public class UpdateAdService extends IntentService {
    private String TAG = getClass().getSimpleName();
    private int result = Activity.RESULT_CANCELED;
    public static final String kProductUpdateNotification = "com.app.inn.peepsin.Services.receiver";
    private NotificationManager mNotifyManager;
    private Notification.Builder mBuilder;
    private Integer notificationId = 1;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public UpdateAdService(String name) {
        super(name);
    }


    public UpdateAdService() {
        super("PublishAdService");
    }

    // will be called asynchronously by Android
    @Override
    protected void onHandleIntent(Intent intent) {
        HashMap<String, Object> productMap = (HashMap<String, Object>) intent.getSerializableExtra(kRecords);
        List<String> pathList = intent.getStringArrayListExtra(kProductImageUrlList);
        showNotification();
        while (pathList.size() < 6) {
            pathList.add(kEmptyString);
        }

        HashMap<String, Object> hashMap = new HashMap<>();
        for (int i = 0; i < pathList.size(); i++) {
            if (!URLUtil.isValidUrl(pathList.get(i))) {
                switch (i) {
                    case 0: hashMap.put(kCoverImage, pathList.get(i));
                    break;
                    case 1: hashMap.put(kImageUploadId1, pathList.get(i));
                    break;
                    case 2: hashMap.put(kImageUploadId2, pathList.get(i));
                    break;
                    case 3: hashMap.put(kImageUploadId3, pathList.get(i));
                    break;
                    case 4: hashMap.put(kImageUploadId4, pathList.get(i));
                    break;
                    case 5: hashMap.put(kImageUploadId5, pathList.get(i));
                    break;
                }
            }
        }

        boolean addImages = false;
        for(String keys : hashMap.keySet()){
            if(!hashMap.get(keys).equals(kEmptyString))
                addImages= true;
        }
        if(addImages){
            UploadManager.uploadManager().editUploadImage(hashMap, (Constants.Status iStatus, GenricResponse<HashMap<String, Object>> genricResponse) -> {
                try {
                    HashMap<String, Object> partialMap = genricResponse.getObject();
                    productMap.putAll(partialMap);
                    editAd(productMap);
                }catch (Exception e){
                    Toaster.toastRangeen("Process Failed! \n Please try updating product after sometime.");
                    itemUploadednotification(false);
                }

            });
        }else {
            productMap.put(kRequestId,-1);
            editAd(productMap);
        }




    }

    private void editAd(HashMap<String, Object> productMap) {
        ModelManager.modelManager().editSellingProduct(productMap, (Constants.Status iStatus, GenricResponse<SellingProduct> genricResponse) -> {
            itemUploadednotification(true);
            publishResults(genricResponse.getObject());
            Toaster.toastRangeen("Congratulations! \n Your Ad updated successfully!");
        }, (Constants.Status iStatus, String message) -> {
            Toaster.toastRangeen("Process Failed! \n Please try updating product after sometime.");
            itemUploadednotification(false);
        });
    }

    private void itemUploadednotification(boolean success) {
        if (success) {
            mBuilder.setContentTitle("PeepsIn")
                    .setContentText("Product updated successfully")
                    .setSmallIcon(android.R.drawable.stat_sys_upload_done)
                    .setProgress(0, 0, false)
                    .setTicker("Product updated successfully.")
                    .setOngoing(false);

            mNotifyManager.notify(notificationId, mBuilder.build());
        } else {
            mBuilder.setContentTitle("PeepsIn")
                    .setContentText("Product submission failed Please try again!")
                    .setSmallIcon(android.R.drawable.stat_sys_warning)
                    .setProgress(0, 0, false)
                    .setTicker("Product publishing failed.")
                    .setOngoing(false);
            mNotifyManager.notify(notificationId, mBuilder.build());
        }

    }

    private void publishResults(SellingProduct object) {
        Intent intent = new Intent(kProductUpdateNotification);
        intent.putExtra(kRecords, object);
        sendBroadcast(intent);
    }


    void showNotification() {
        mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mBuilder = new Notification.Builder(this);
        mBuilder.setContentTitle("PeepsIn")
                .setContentText("Product updating...")
                .setSmallIcon(android.R.drawable.stat_sys_upload)
                .setOngoing(true)
                .setTicker("PeepsIn: Product updating...")
                .setProgress(100, 10, true);
        mNotifyManager.notify(notificationId, mBuilder.build());
    }
}
