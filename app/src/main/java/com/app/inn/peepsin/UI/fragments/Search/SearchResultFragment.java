package com.app.inn.peepsin.UI.fragments.Search;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.Feeds;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.adapters.SearchResultAdapter;
import com.app.inn.peepsin.UI.helpers.DividerItemRecyclerDecoration;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import java.util.HashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kCategoryId;
import static com.app.inn.peepsin.Constants.Constants.kPage;

/**
 * Created by Harsh on 5/10/2017.
 */

public class SearchResultFragment extends Fragment {

    @BindView(R.id.recycler_view_feeds) RecyclerView recyclerViewSearchResult;
    @BindView(R.id.tv_title) TextView tvTitle;
    @BindView(R.id.empty_view) View emptyView;
    @BindView(R.id.tv_empty_message) TextView emptyMessage;
    private ProgressDialog progressDialog;
    public SearchResultAdapter searchResultAdapter;
    private static Switcher profileSwitcherListener;
    private int categoryId;

    public static SearchResultFragment newInstance(Integer CategoryId,Switcher switcher) {
        SearchResultFragment fragment = new SearchResultFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(kCategoryId , CategoryId);
        fragment.setArguments(bundle);
        profileSwitcherListener=switcher;
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if(bundle!=null)
            categoryId = bundle.getInt(kCategoryId);
        Utils.hideKeyboard(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search_result, container, false);
        ButterKnife.bind(this, view);

        tvTitle.setText(getString(R.string.title_search));
        progressDialog = Utils.generateProgressDialog(getContext(),true);

        searchResultAdapter = new SearchResultAdapter(getContext(),new CopyOnWriteArrayList<>(),profileSwitcherListener);
        recyclerViewSearchResult.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerViewSearchResult.setLayoutManager(layoutManager);
        recyclerViewSearchResult.setAdapter(searchResultAdapter);
        recyclerViewSearchResult.addItemDecoration(new DividerItemRecyclerDecoration(getActivity(), R.drawable.canvas_recycler_divider));
        checkEmptyScreen();

        return view;
    }

    @OnClick(R.id.btn_back)
    void onBack() {
        getFragmentManager().popBackStack();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData(1);
    }

    private void loadData(int page) {
        showProgress(true);
        HashMap<String,Object> searchProduct=new HashMap<>();
        searchProduct.put(kCategoryId,categoryId);
        searchProduct.put(kPage,page);
        ModelManager.modelManager().getSearchProduct(searchProduct,(Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<Feeds>> genericResponse) -> {
            searchResultAdapter.addItems(genericResponse.getObject());
            showProgress(false);
            checkEmptyScreen();
        }, (Constants.Status iStatus, String message) -> {
            Toaster.toast(message);
            showProgress(false);
            checkEmptyScreen();
        });
    }

    private void checkEmptyScreen() {
        if(searchResultAdapter.getItemCount()>0){
            emptyView.setVisibility(View.GONE);
        }else {
            emptyView.setVisibility(View.VISIBLE);
            emptyMessage.setText(getString(R.string.empty_message_no_search));
        }
    }
    private void showProgress(boolean b) {
        if(b){
            progressDialog.show();
        }else {
            if(progressDialog!=null) progressDialog.cancel();
        }
    }

    @Override
    public void onDetach() {
        profileSwitcherListener = null;
        super.onDetach();
    }


}
