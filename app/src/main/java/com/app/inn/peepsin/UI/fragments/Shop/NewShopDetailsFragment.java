package com.app.inn.peepsin.UI.fragments.Shop;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.CurrentUser;
import com.app.inn.peepsin.Models.Feeds;
import com.app.inn.peepsin.Models.MyShop;
import com.app.inn.peepsin.Models.ProductType;
import com.app.inn.peepsin.Models.Search;
import com.app.inn.peepsin.Models.ShopProduct;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.adapters.SearchAdapter;
import com.app.inn.peepsin.UI.fragments.Feeds.FeedsFragment;
import com.app.inn.peepsin.UI.fragments.Feeds.ProductDetailFragment;
import com.app.inn.peepsin.UI.fragments.Profile.ShoppingCartFragment;
import com.app.inn.peepsin.UI.fragments.Search.SearchResultFragment;
import com.app.inn.peepsin.UI.helpers.ItemOffsetDecoration;
import com.app.inn.peepsin.UI.helpers.SearchUtil;
import com.app.inn.peepsin.UI.helpers.SharedPreference;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindColor;
import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.CART_UPDATE;
import static com.app.inn.peepsin.Constants.Constants.kData;
import static com.app.inn.peepsin.Constants.Constants.kProductList;
import static com.app.inn.peepsin.Constants.Constants.kProductTypeId;
import static com.app.inn.peepsin.Constants.Constants.kSearchString;
import static com.app.inn.peepsin.Constants.Constants.kShopId;


/**
 * Created by akaMahesh on 1/9/17
 * contact : mckay1718@gmail.com
 */

public class NewShopDetailsFragment extends Fragment {

    private static final String TAG = NewShopDetailsFragment.class.getSimpleName();
    private static final float PERCENTAGE_TO_HIDE_ShopLogo = 0.5f;
    private static final float PERCENTAGE_TO_HIDE_TITLE_DETAILS = 0.5f;
    private static final int ALPHA_ANIMATIONS_DURATION = 400;
    private boolean mIsTheTitleContainerVisible = true;
    private boolean mIsLogoVisible = true;

    @BindView(R.id.app_bar)
    AppBarLayout appBarLayout;
    @BindView(R.id.view_shops)
    View shopsView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.seller_details)
    View sellerView;
    @BindView(R.id.iv_shop_logo)
    ImageView ivShopLOGO;
    @BindView(R.id.iv_shop_banner)
    ImageView ivShopBanner;
    @BindView(R.id.iv_profile_image)
    ImageView ivProfileImage;
    @BindView(R.id.tv_owner_name)
    TextView tvOwnerName;
    @BindView(R.id.tv_shop_address)
    TextView tvShopAddress;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_cart_count)
    TextView tvCartCount;
    @BindView(R.id.rating_score_bar)
    RatingBar ratingBar;
    @BindView(R.id.empty_view)
    View emptyView;
    @BindView(R.id.view_container)
    LinearLayout viewcontainer;
    @BindView(R.id.iv_shop_logo_toolbar)
    ImageView ivShopLogoToolbar;
    @BindDrawable(R.drawable.img_product_placeholder)
    Drawable placeholder;
    @BindDrawable(R.drawable.img_profile_placeholder)
    Drawable profilePlaceholder;
    private static Switcher switcherListener;
    private ProgressDialog progressDialog;

    @BindColor(R.color.text_color_regular)
    int textColor;

    private List<ProductType> productTypes;
    private boolean isFirstTime = true;
    // List<ProductAdapter> productAdapters = new ArrayList<>();
    List<HashMap<String, Object>> hashMapList = new ArrayList<>();

    private SearchAdapter searchAdapter;
    private List<Search> searchStored;
    private ListView listSearch;
    private TextView tvEmptySearch;
    private int mShopId;

    public static Fragment newInstance(Switcher switcher, int shopId) {
        Fragment fragment = new NewShopDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(kShopId, shopId);
        fragment.setArguments(bundle);
        switcherListener = switcher;
        return fragment;
    }

    public NewShopDetailsFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            mShopId = bundle.getInt(kShopId, 0);
        }
        productTypes = new CopyOnWriteArrayList<>();
        progressDialog = Utils.generateProgressDialog(getContext(), false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_shop, container, false);
        ButterKnife.bind(this, view);
        appBarLayout.addOnOffsetChangedListener(offsetChangedListener);

        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadShopData(mShopId);
    }



    public void checkEmptyState() {
        if (productTypes.isEmpty()) {
            emptyView.setVisibility(View.VISIBLE);
        } else {
            emptyView.setVisibility(View.GONE);
        }
    }

    public void setCartUpdate(int count) {
        if (count == 0) {
            tvCartCount.setVisibility(View.GONE);
        } else {
            tvCartCount.setVisibility(View.VISIBLE);
            tvCartCount.setText(String.valueOf(count));
        }
    }


    @OnClick(R.id.cart_view)
    void onCart() {
        if (switcherListener != null) {
            switcherListener
                    .switchFragment(ShoppingCartFragment.newInstance(switcherListener), true, true);
        }
    }

    @OnClick(R.id.iv_search)
    void onSearch() {
        loadToolBarSearch();
    }


    @OnClick(R.id.btn_back)
    void onBack() {
        getFragmentManager().popBackStack();
    }



    @Override
    public void onResume() {
        super.onResume();
        CurrentUser user = ModelManager.modelManager().getCurrentUser();
        setCartUpdate(user.getShoppingCartCount());
        LocalBroadcastManager.getInstance(getContext())
                .registerReceiver(mBroadcastReceiver, new IntentFilter(CART_UPDATE));
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getContext())
                .unregisterReceiver(mBroadcastReceiver);
    }

    BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(CART_UPDATE)) {
                setCartUpdate(intent.getIntExtra(kData, 0));
            }
        }
    };


    int size = 0;

    private void loadData(Integer productTypeID) {
        ModelManager.modelManager().getShopProduct(mShopId, productTypeID, 1, (Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<ShopProduct>> genericResponse) -> {
            List<ShopProduct> products = genericResponse.getObject();
            size++;
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put(kProductTypeId, productTypeID);
            hashMap.put(kProductList, products);
            hashMapList.add(hashMap);
            if (size >= productTypes.size()) {
                try {
                    setupList();
                } catch (Exception e) {
                    e.printStackTrace();
                    showProgress(false);
                    emptyView.setVisibility(View.VISIBLE);
                }
            }
        }, (Constants.Status iStatus, String message) -> {
            Toaster.toast(message);
            showProgress(false);
            size++;
            emptyView.setVisibility(View.VISIBLE);
        });
    }

    private void setupList() {
        viewcontainer.removeAllViewsInLayout();
        for (ProductType productType : productTypes) {
            List<ShopProduct> shopProducts = null;
            for (HashMap<String, Object> hashMap : hashMapList) {
                Integer productId = (Integer) hashMap.get(kProductTypeId);
                if (productType.getId().equals(productId)) {
                    shopProducts = (List<ShopProduct>) hashMap.get(kProductList);
                }
            }
            RelativeLayout relativeLayout = new RelativeLayout(getContext());
            {
                TextView textView = new TextView(getContext());
                textView.setId(R.id.tv_product_type);
                textView.setText(productType.getName());
                textView.setTextColor(textColor);
                textView.setPadding(5, 10, 5, 10);
                relativeLayout.addView(textView);
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) textView
                        .getLayoutParams();
                params.addRule(RelativeLayout.ALIGN_PARENT_START);
                params.addRule(RelativeLayout.ALIGN_END);
                if (shopProducts != null && shopProducts.isEmpty())
                    textView.setVisibility(View.GONE);
            }
            {
                TextView textView = new TextView(getContext());
                if (shopProducts != null) {
                    if (shopProducts.size() > 1) {
                        textView.setText("See All > ");
                    } else {
                        textView.setVisibility(View.GONE);
                    }
                }
                textView.setTextColor(textColor);
                textView.setGravity(Gravity.CENTER);
                textView.setPadding(5, 10, 5, 10);
                relativeLayout.addView(textView);
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) textView
                        .getLayoutParams();
                params.addRule(RelativeLayout.ALIGN_PARENT_END);
                params.addRule(RelativeLayout.ALIGN_END);
                textView.setOnClickListener(onClickListener);
            }

            {
                ProductAdapter productAdapter = new ProductAdapter(shopProducts);
                LayoutManager layoutManager = new LinearLayoutManager(getContext(),
                        LinearLayoutManager.HORIZONTAL, false);
                RecyclerView recyclerView = new RecyclerView(getContext());
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.setHasFixedSize(true);
                recyclerView.setNestedScrollingEnabled(false);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.addItemDecoration(new ItemOffsetDecoration(getContext(), R.dimen.item_offset));
                recyclerView.setAdapter(productAdapter);
                relativeLayout.addView(recyclerView);
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) recyclerView
                        .getLayoutParams();
                params.addRule(RelativeLayout.BELOW, R.id.tv_product_type);

            }
            viewcontainer.addView(relativeLayout);
        }
        showProgress(false);
        checkEmptyState();
    }

    TextView.OnClickListener onClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (switcherListener != null) {
                switcherListener
                        .switchFragment(FeedsFragment.newInstance(switcherListener, mShopId), true,
                                true);
            }
        }
    };

    AppBarLayout.OnOffsetChangedListener offsetChangedListener = (appBarLayout, verticalOffset) -> {
        int maxScroll = appBarLayout.getTotalScrollRange();
        float percentage = (float) Math.abs(verticalOffset) / (float) maxScroll;
        handleAlphaOnLayout(percentage);
        handleShopLogo(percentage);
    };

    private void handleShopLogo(float percentage) {
        if (percentage >= PERCENTAGE_TO_HIDE_ShopLogo) {
            if (mIsLogoVisible) {
                startAlphaAnimation(ivShopLOGO, ALPHA_ANIMATIONS_DURATION, View.INVISIBLE);
                startAlphaAnimation(ivShopLogoToolbar, ALPHA_ANIMATIONS_DURATION, View.VISIBLE);
                mIsLogoVisible = false;
            }
        } else {
            if (!mIsLogoVisible) {
                startAlphaAnimation(ivShopLOGO, ALPHA_ANIMATIONS_DURATION, View.VISIBLE);
                startAlphaAnimation(ivShopLogoToolbar, ALPHA_ANIMATIONS_DURATION, View.INVISIBLE);
                mIsLogoVisible = true;
            }
        }
    }

    private void handleAlphaOnLayout(float percentage) {
        if (percentage >= PERCENTAGE_TO_HIDE_TITLE_DETAILS) {
            if (mIsTheTitleContainerVisible) {
                startAlphaAnimation(sellerView, ALPHA_ANIMATIONS_DURATION, View.INVISIBLE);
                mIsTheTitleContainerVisible = false;
            }
        } else {
            if (!mIsTheTitleContainerVisible) {
                startAlphaAnimation(sellerView, ALPHA_ANIMATIONS_DURATION, View.VISIBLE);
                mIsTheTitleContainerVisible = true;
            }
        }
    }

    public static void startAlphaAnimation(View v, long duration, int visibility) {
        AlphaAnimation alphaAnimation = (visibility == View.VISIBLE)
                ? new AlphaAnimation(0f, 1f)
                : new AlphaAnimation(1f, 0f);
        alphaAnimation.setDuration(duration);
        alphaAnimation.setFillAfter(true);
        v.startAnimation(alphaAnimation);
    }

    public void loadShopData(int shopId) {
        showProgress(true);
        ModelManager.modelManager().getMyShopDetails(shopId,
                (Constants.Status iStatus, GenricResponse<MyShop> genricResponse) -> {
                    MyShop shop = genricResponse.getObject();
                    productTypes = shop.getProductTypeStore();
                    for (ProductType productType : productTypes) {
                        loadData(productType.getId());
                    }
                    setupShop(shop);
                }, (Constants.Status iStatus, String message) -> {
                    Log.e(TAG, message);
                    Toaster.toast(message);
                    showProgress(false);
                });
    }

    public void setupShop(MyShop upShop) {
        String shopName = upShop.getName();
        String ownerName = upShop.getOwnerName();
        String bannerURL = upShop.getBannerUrl();
        String profileURL = upShop.getProfilePicURL();
        String logoURL = upShop.getLogoUrl();
        String address = Utils.getAddressString(upShop.getAddress());
        Integer ratings = upShop.getRating();
        ratingBar.setRating(ratings);
        tvShopAddress.setText(Utils.getFirstLetterInUpperCase(address));
        tvOwnerName.setText(Utils.getFirstLetterInUpperCase(ownerName));
        tvTitle.setText(Utils.getFirstLetterInUpperCase(shopName));
        TextDrawable bannerDrawable = TextDrawable.builder()
                .buildRoundRect("", ColorGenerator.MATERIAL.getRandomColor(), 5);

        if (!bannerURL.isEmpty()) {
            Picasso.with(getContext())
                    .load(bannerURL)
                    .placeholder(bannerDrawable)
                    .error(bannerDrawable)
                    .resize(400, 200)
                    .into(ivShopBanner);
        } else {
            ivShopBanner.setImageDrawable(bannerDrawable);
        }

        if (!profileURL.isEmpty()) {
            Picasso.with(getContext())
                    .load(profileURL)
                    .error(profilePlaceholder)
                    .placeholder(profilePlaceholder)
                    .resize(100, 100)
                    .into(ivProfileImage);
        } else {
            ivProfileImage.setImageDrawable(profilePlaceholder);
        }

        if (!logoURL.isEmpty()) {
            Picasso.with(getContext())
                    .load(logoURL)
                    .placeholder(placeholder)
                    .error(placeholder)
                    .resize(100, 100)
                    .into(ivShopLOGO);
            Picasso.with(getContext())
                    .load(logoURL)
                    .placeholder(placeholder)
                    .error(placeholder)
                    .resize(50, 50)
                    .into(ivShopLogoToolbar);
        } else {
            ivShopLOGO.setImageDrawable(placeholder);
            ivShopLogoToolbar.setImageDrawable(placeholder);
        }
    }

    private void showProgress(boolean b) {
        if (b) {
            progressBar.setVisibility(View.VISIBLE);
            appBarLayout.setVisibility(View.GONE);
            shopsView.setVisibility(View.GONE);
            //progressDialog.show();
        } else {
            appBarLayout.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);

            shopsView.setVisibility(View.VISIBLE);
            //if (progressDialog != null) progressDialog.cancel();
        }
    }

    public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {

        List<ShopProduct> shopProductList;

        ProductAdapter(List<ShopProduct> shopProducts) {
            this.shopProductList = shopProducts;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_shop_product_item, parent, false);
            return new ViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            ShopProduct shopProduct = shopProductList.get(position);
            holder.bindContent(shopProduct);
            String imageurl = shopProduct.getCoverImageURL();
            String sellingPrice = shopProduct.getSellingPrice();
            String price = shopProduct.getPrice();
            String offer = shopProduct.getDiscountTagLine();

            Integer rating = 0;
            if (shopProduct.getRating() != null) {
                rating = shopProduct.getRating();
            }
            holder.tvTitle.setText(shopProduct.getName());
            holder.tvOffer.setText(offer);

            holder.tvPrice.setText(getString(R.string.Rs) + price);
            holder.tvSellingPrice.setText(getString(R.string.Rs) + sellingPrice);
            if (rating == 0) {
                holder.tvRating.setVisibility(View.GONE);
            } else {
                holder.tvRating.setVisibility(View.VISIBLE);

                holder.tvRating.setText(rating + "");
            }
            if (!imageurl.isEmpty()) {
                Picasso.with(getContext())
                        .load(imageurl)
                        .fit()
                        .placeholder(R.drawable.img_product_placeholder)
                        .into(holder.ivImage);
            }

            try {
                price = price.replaceAll("/", "").replaceAll("-", "");
                sellingPrice = sellingPrice.replaceAll("/", "").replaceAll("-", "");
                Float intPrice = Float.parseFloat(price);
                Float intSellingPrice = Float.parseFloat(sellingPrice);
                if (intPrice > intSellingPrice && !offer.equalsIgnoreCase("")) {
                    holder.tvOffer.setVisibility(View.VISIBLE);
                    holder.tvPrice.setVisibility(View.VISIBLE);
                    holder.tvPrice
                            .setPaintFlags(holder.tvPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                } else {
                    holder.tvOffer.setVisibility(View.INVISIBLE);
                    holder.tvPrice.setVisibility(View.GONE);
                }
                if (offer.isEmpty()) {
                    holder.tvOffer.setVisibility(View.GONE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return shopProductList.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            ShopProduct shopProduct;
            @BindView(R.id.tv_name)
            TextView tvTitle;
            @BindView(R.id.tv_offer)
            TextView tvOffer;
            @BindView(R.id.tv_selling_price)
            TextView tvSellingPrice;
            @BindView(R.id.tv_price)
            TextView tvPrice;
            @BindView(R.id.tv_rating)
            TextView tvRating;
            @BindView(R.id.iv_image)
            ImageView ivImage;

            public ViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            void bindContent(ShopProduct shopProduct) {
                this.shopProduct = shopProduct;
            }

            @OnClick(R.id.item_view)
            void onItemClick() {
                getFeedDetails(shopProduct.getProductId());
            }

            private void getFeedDetails(int productId) {
                progressDialog.show();
                ModelManager.modelManager().getProductDetails(productId, (Constants.Status iStatus, GenricResponse<Feeds> genericResponse) -> {
                    if (progressDialog != null) progressDialog.dismiss();
                    if (switcherListener != null) {
                        switcherListener.switchFragment(ProductDetailFragment.newInstance(productId, genericResponse.getObject(), switcherListener), true, true);
                    }
                }, (Constants.Status iStatus, String message) -> {
                    Toaster.toast(message);
                    if (progressDialog != null) progressDialog.dismiss();
                });
            }
        }

    }

    public void loadToolBarSearch() {
        searchStored = SharedPreference
                .getSearchList(getContext(), SearchUtil.PREFS_NAME, SearchUtil.KEY_SEARCH);
        View view = getActivity().getLayoutInflater().inflate(R.layout.view_toolbar_search, null);
        EditText edtToolSearch = (EditText) view.findViewById(R.id.edt_tool_search);
        ImageView ivToolMic = (ImageView) view.findViewById(R.id.img_tool_mic);
        ImageView ivToolBack = (ImageView) view.findViewById(R.id.img_tool_back);
        listSearch = (ListView) view.findViewById(R.id.list_search);
        tvEmptySearch = (TextView) view.findViewById(R.id.txt_empty);

        SearchUtil.setListViewHeightBasedOnChildren(listSearch);
        edtToolSearch.setHint("Search to shop");

        Dialog toolbarSearchDialog = new Dialog(getContext(), R.style.MaterialSearch);
        toolbarSearchDialog.setContentView(view);
        toolbarSearchDialog.setCancelable(false);
        toolbarSearchDialog.getWindow()
                .setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        toolbarSearchDialog.getWindow().setGravity(Gravity.BOTTOM);
        toolbarSearchDialog.show();

        toolbarSearchDialog.getWindow()
                .setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        if (searchStored == null) {
            searchStored = new ArrayList<>();
        }
        searchAdapter = new SearchAdapter(getContext(), searchStored, false);
        listSearch.setAdapter(searchAdapter);

        listSearch.setOnItemClickListener((adapterView, view13, position, l) -> {
            toolbarSearchDialog.dismiss();
            Search item = searchAdapter.getItemPos(position);
            if (searchStored.contains(item)) {
                SharedPreference
                        .removeSearch(getContext(), item, SearchUtil.PREFS_NAME, SearchUtil.KEY_SEARCH);
            }
            SharedPreference.addSearch(getContext(), item, SearchUtil.PREFS_NAME, SearchUtil.KEY_SEARCH);

            if (switcherListener != null) {
                switcherListener.switchFragment(
                        SearchResultFragment.newInstance(item.getCategoryId(), switcherListener), true, false);
            }
        });

        edtToolSearch.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                String str = edtToolSearch.getText().toString();
                searchProduct(str);
                return true;
            }
            return false;
        });

        edtToolSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (searchAdapter.getCount() > 0) {
                    searchAdapter.clearList();
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String str = s.toString().toLowerCase();
                char[] ch = str.toCharArray();
                for (char aCh : ch) {
                    if (aCh == ' ') {
                        String withoutSpaceStr = str.replaceAll("\\s+", "");
                        searchProduct(withoutSpaceStr);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        ivToolBack.setOnClickListener(view1 -> toolbarSearchDialog.dismiss());

        ivToolMic.setOnClickListener(view12 -> edtToolSearch.setText(""));
    }

    public void searchProduct(String searchStr) {
        HashMap<String, Object> searchmap = new HashMap<>();
        searchmap.put(kSearchString, searchStr);
        ModelManager.modelManager().getSearch(searchmap,
                (Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<Search>> genericList) -> {
                    searchAdapter.updateList(genericList.getObject(), true);
                    checkEmptySearch();
                }, (Constants.Status iStatus, String message) -> {
                    Toaster.toast(message);
                    checkEmptySearch();
                });
    }

    private void checkEmptySearch() {
        if (searchAdapter.getCount() > 0) {
            listSearch.setVisibility(View.VISIBLE);
            tvEmptySearch.setVisibility(View.GONE);
        } else {
            listSearch.setVisibility(View.GONE);
            tvEmptySearch.setVisibility(View.VISIBLE);
            tvEmptySearch.setText(getString(R.string.empty_message_search));
        }
    }
}
