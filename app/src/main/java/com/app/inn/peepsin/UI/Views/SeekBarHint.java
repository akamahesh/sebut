package com.app.inn.peepsin.UI.Views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;

/**
 * Created by dhruv on 9/8/17.
 */

public class SeekBarHint extends android.support.v7.widget.AppCompatSeekBar {

    private String text = "";
    private Paint vTextPaintRed;

    public SeekBarHint(Context context) {
        super(context);
        initText();
    }

    public SeekBarHint(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initText();
    }

    public SeekBarHint(Context context, AttributeSet attrs) {
        super(context, attrs);
        initText();
    }

    private void initText(){
        vTextPaintRed = new Paint();
        vTextPaintRed.setTextSize(18);
        vTextPaintRed.setColor(Color.RED);
        vTextPaintRed.setTextAlign(Paint.Align.CENTER);
    }

    public void setProgressText(String progressText){
        this.text = progressText;
    }

    @Override
    protected void onDraw(Canvas c) {
        super.onDraw(c);
        int offset = this.getThumbOffset();
        float percent = ((float)this.getProgress())/(float)this.getMax();
        int width = this.getWidth() - this.getThumbOffset();

        int x_val =((int)(width*percent +offset ));
        int thumb_x = (int) (( (double)this.getProgress()/this.getMax() ) * (double)this.getWidth());
        int y_val = (int)(this.getHeight()/2);
        // your drawing code here, ie Canvas.drawText();
        c.drawText(text,thumb_x,y_val,vTextPaintRed);
    }
}


