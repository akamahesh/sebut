package com.app.inn.peepsin.UI.fragments.Board;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.Board;
import com.app.inn.peepsin.Models.Comment;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.activities.AddBoardActivity;
import com.app.inn.peepsin.UI.helpers.ItemOffsetDecoration;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kBoardId;
import static com.app.inn.peepsin.Constants.Constants.kRecords;
import static com.app.inn.peepsin.Constants.Constants.kTotalRecords;

/**
 * Created by akamahesh on 2/5/17.
 */

public class CommentFragment extends Fragment {

    private Integer mBoardId = 0;
    @BindDrawable(R.drawable.img_profile_placeholder)
    Drawable placeholderDrawable;
    @BindView(R.id.tv_title) TextView tvTitle;
    @BindView(R.id.tv_user_name) TextView tvUserName;
    @BindView(R.id.tv_time) TextView tvTime;
    @BindView(R.id.iv_user_image_board) ImageView ivUserImg;
    @BindView(R.id.tv_post) TextView tvPost;
    @BindView(R.id.iv_user_text_board) ImageView ivUserTextImg;

    @BindView(R.id.recycler_view_comments)
    RecyclerView mRecyclerView;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.empty_view)
    View emptyView;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.iv_user_image)
    ImageView ivUserImage;
    @BindView(R.id.iv_user_text)
    ImageView ivUserText;
    @BindView(R.id.tv_empty_title)
    TextView tvEmptyText;
    @BindView(R.id.edt_comment)
    EditText edtComment;
    @BindView(R.id.btn_comment)
    Button btnComment;
    private Integer mTotalRecords;

    private CopyOnWriteArrayList<Comment> mCommentList;
    private CommentAdapter mCommentAdapter;
    private LinearLayoutManager mLayoutManager;

    int mPage = 1;
    boolean isLoading = true;
    int pageSize;

    private int mCurrentUserId;
    private ProgressDialog progressDialog;

    public static Fragment newInstance(Integer boardId) {
        Fragment fragment = new CommentFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(kBoardId, boardId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCurrentUserId = ModelManager.modelManager().getCurrentUser().getUserId();
        progressDialog = Utils.generateProgressDialog(getContext(), false);
        Bundle bundle = getArguments();
        if (bundle != null)
            mBoardId = bundle.getInt(kBoardId);

        mCommentList = new CopyOnWriteArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_post_detail_list, container, false);
        ButterKnife.bind(this, view);
        tvTitle.setText(getString(R.string.title_comments));
        tvEmptyText.setText("No Comments");

        mCommentAdapter = new CommentAdapter(mCommentList);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mCommentAdapter);
        mRecyclerView.addItemDecoration(new ItemOffsetDecoration(getContext(), R.dimen.item_offset));
        mRecyclerView.addOnScrollListener(onScrollListener);
        checkEmptyScreen();

        swipeRefreshLayout.setOnRefreshListener(onRefreshListener);
        addTextListener(edtComment);
        edtComment.setOnEditorActionListener(actionListener);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData();
        loadData(mBoardId);
        setupProfilePic();
        setUpItemTouchHelper();

    }

    private void setupProfilePic() {
        String imageURL = ModelManager.modelManager().getCurrentUser().getProfilePicURL();
        String userName = ModelManager.modelManager().getCurrentUser().getFirstName();

        if(!imageURL.isEmpty()){
            ivUserText.setVisibility(View.INVISIBLE);
            ivUserImage.setVisibility(View.VISIBLE);
        }else {
            ivUserText.setVisibility(View.VISIBLE);
            ivUserImage.setVisibility(View.INVISIBLE);
        }
        TextDrawable draw = TextDrawable.builder().buildRound(String.valueOf(userName.toUpperCase().charAt(0)), ColorGenerator.MATERIAL.getRandomColor());

        if (!imageURL.isEmpty()) {
            Picasso.with(getContext())
                    .load(imageURL)
                    .resize(100, 100)
                    .placeholder(placeholderDrawable)
                    .into(ivUserImage);
        } else ivUserText.setImageDrawable(draw);
    }

    SwipeRefreshLayout.OnRefreshListener onRefreshListener = () -> loadData(mBoardId);

    public void initialState(){
        mPage=1;
        isLoading=true;
        pageSize = mCommentList.size();
    }

    RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            if(dy > 0) //check for scroll down
            {
                int visibleItemCount = mLayoutManager.getChildCount();
                int totalItemCount = mLayoutManager.getItemCount();
                int firstVisibleItemPosition = mLayoutManager.findFirstVisibleItemPosition();

                if (isLoading)
                {
                    if ( (visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= pageSize) {
                        isLoading = false;
                        ++mPage;
                        //Do pagination.. i.e. fetch new data
                        loadMoreData(mBoardId);
                    }
                }
            }
        }
    };



    TextView.OnEditorActionListener actionListener = (v, actionId, event) -> {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            Utils.hideKeyboard(getContext());
        }
        return true;
    };

    private void addTextListener(EditText edtSearch) {
        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                s = s.toString().toLowerCase();
                if(s.equals("")) {
                    btnComment.setTextColor(Color.parseColor("#4D4C52"));
                }else{ btnComment.setTextColor(Color.parseColor("#D1404B"));}
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    private void loadData() {
        ModelManager.modelManager().getPeepsInBoards(1, (Constants.Status iStatus, GenricResponse<HashMap<String,Object>> genericResponse) -> {
            HashMap<String,Object> hashMap = genericResponse.getObject();
            CopyOnWriteArrayList<Board> boards = (CopyOnWriteArrayList<Board>) hashMap.get(kRecords);
            mTotalRecords  = (Integer) hashMap.get(kTotalRecords);
            for (int i=0;i<boards.size();i++){
            if(boards.get(i).getBoardId().equals(mBoardId)){
                String userProfileURL = boards.get(i).getProfilePicURL();
                tvUserName.setText(boards.get(i).getFullName());
                tvTime.setText(boards.get(i).getBoardTime());
                tvPost.setMovementMethod(new ScrollingMovementMethod());
                tvPost.setText(boards.get(i).getDescription());
                if(!userProfileURL.isEmpty()){
                    ivUserTextImg.setVisibility(View.INVISIBLE);
                    ivUserImg.setVisibility(View.VISIBLE);
                }else {
                    ivUserTextImg.setVisibility(View.VISIBLE);
                    ivUserImg.setVisibility(View.INVISIBLE);
                }
                TextDrawable drawable = TextDrawable.builder().buildRound(String.valueOf(boards.get(i).getFullName().toUpperCase().charAt(0)), ColorGenerator.MATERIAL.getRandomColor());
                if(!userProfileURL.isEmpty()){
                    Picasso.with(getContext())
                            .load(userProfileURL)
                            .resize(100,100)
                            .into(ivUserImg);
                }else ivUserTextImg.setImageDrawable(drawable);
            }}

        }, (Constants.Status iStatus, String message) -> {
        });
    }

    public void setUpItemTouchHelper(){
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int swipedPosition = viewHolder.getAdapterPosition();
                if(mCommentList.get(swipedPosition).getUserId()==mCurrentUserId){
                    int commentId = mCommentList.get(swipedPosition).getCommentId();
                    Utils.showConfirmDialog(getContext(),"Are you sure you want to remove it?", (dialog, which) -> {
                        //yes click listener
                        removeComment(mBoardId,commentId,swipedPosition);
                    }, (dialog, which) -> {
                        //no click listerner
                        mCommentAdapter.notifyDataSetChanged();
                    },"Yes","No");
                }else {
                    mCommentAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };
        ItemTouchHelper mItemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        mItemTouchHelper.attachToRecyclerView(mRecyclerView);
    }

    private void loadData(Integer boardId) {
        swipeProgress(true);
        ModelManager.modelManager().getComments(boardId, 1, (Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<Comment>> genericResponse) -> {
            mCommentList = genericResponse.getObject();
            mCommentAdapter.addItems(mCommentList);
            swipeProgress(false);
            initialState();
            checkEmptyScreen();
        }, (Constants.Status iStatus, String message) -> {
            Toaster.toast(message);
            swipeProgress(false);
            checkEmptyScreen();
        });
    }

    private void loadMoreData(Integer boardId) {
        swipeProgress(true);
        ModelManager.modelManager().getComments(boardId, mPage, (Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<Comment>> genericResponse) -> {
            mCommentList.addAll(genericResponse.getObject());
            mCommentAdapter.addMoreItems(genericResponse.getObject());
            isLoading = !genericResponse.getObject().isEmpty();
            swipeProgress(false);
        }, (Constants.Status iStatus, String message) -> {
            Toaster.toast(message);
            swipeProgress(false);
        });
    }

    private void swipeProgress(boolean b) {
        if (b) {
            swipeRefreshLayout.setRefreshing(true);
        } else {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void checkEmptyScreen() {
        if (mCommentAdapter.getItemCount() > 0) {
            emptyView.setVisibility(View.GONE);
        } else emptyView.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.ib_back)
    void onBack() {
        Utils.hideKeyboard(getContext());
        AddBoardActivity addBoardActivity = (AddBoardActivity) getActivity();
        addBoardActivity.onBackPressed();
    }

    @OnClick(R.id.btn_comment)
    void onComment() {
        String comment = edtComment.getText().toString().trim();
        if (comment.isEmpty()) {
            Toaster.toast("comment can not be empty");
            return;
        }
        addComment(comment, mBoardId);
    }

    private void addComment(String comment, Integer mBoardId) {
        showProgress(true);
        ModelManager.modelManager().saveComment(mBoardId, comment, (Constants.Status iStatus, GenricResponse<Comment> genericResponse) -> {
            mCommentList.add(genericResponse.getObject());
            mCommentAdapter.notifyDataSetChanged();
            showProgress(false);
            edtComment.setText("");
            mRecyclerView.smoothScrollToPosition(mCommentAdapter.getItemCount());
            checkEmptyScreen();
        }, (Constants.Status iStatus, String message) -> {
            Toaster.toast(message);
            showProgress(false);
            checkEmptyScreen();
        });
    }

    public void removeComment(int boardId, int commentId, int adapterPosition) {
        showProgress(true);
        ModelManager.modelManager().removeComment(boardId, commentId, (Constants.Status iStatus) -> {
            mCommentList.remove(adapterPosition);
            mCommentAdapter.notifyDataSetChanged();
            showProgress(false);
            checkEmptyScreen();
        }, (Constants.Status iStatus, String message) -> {
            Toaster.toast(message);
            checkEmptyScreen();
            showProgress(false);
        });
    }

    class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.ViewHolder> {
        List<Comment> comments;
        Dialog dialog;

        CommentAdapter(List<Comment> posts) {
            this.comments = posts;
        }

        @Override
        public CommentAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View layoutView = LayoutInflater.from(getContext()).inflate(R.layout.row_comment_layout, viewGroup, false);
            return new CommentAdapter.ViewHolder(layoutView);
        }

        private void addItems(CopyOnWriteArrayList<Comment> object) {
            comments.clear();
            comments = object;
            notifyDataSetChanged();
        }

        private void addMoreItems(CopyOnWriteArrayList<Comment> object) {
            comments.addAll(object);
            notifyDataSetChanged();
        }

        @Override
        public void onBindViewHolder(CommentAdapter.ViewHolder holder, int position) {
            Comment comment = mCommentList.get(position);
            holder.bindContent(comment);
            String commentDescription = comment.getComment();
            String commentTime = comment.getTime();
            String userName = comment.getUserName();
            String imageUrl = comment.getProfilePicURL();

            holder.tvCommentDescription.setText(commentDescription);
            holder.tvCommentUsername.setText(userName);
            holder.tvCommentTime.setText(commentTime);

            if(comment.getUserId()!=mCurrentUserId){
                holder.itemView.setFocusableInTouchMode(false);
            }

            if(!imageUrl.isEmpty()){
                holder.ivCommentUserText.setVisibility(View.INVISIBLE);
                holder.ivCommentUserImage.setVisibility(View.VISIBLE);
            }else {
                holder.ivCommentUserText.setVisibility(View.VISIBLE);
                holder.ivCommentUserImage.setVisibility(View.INVISIBLE);
            }
            TextDrawable draw = TextDrawable.builder().buildRound(String.valueOf(userName.toUpperCase().charAt(0)), ColorGenerator.MATERIAL.getRandomColor());

            if(!imageUrl.isEmpty()){
                Picasso.with(getContext())
                        .load(imageUrl)
                        .resize(100,100)
                        .into(holder.ivCommentUserImage);
            }else holder.ivCommentUserText.setImageDrawable(draw);
        }

        @Override
        public int getItemCount() {
            return comments.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            @BindView(R.id.tv_comment_user_name)
            TextView tvCommentUsername;
            @BindView(R.id.tv_comment_time)
            TextView tvCommentTime;
            @BindView(R.id.tv_comment)
            TextView tvCommentDescription;
            @BindView(R.id.iv_comment_user_text)
            ImageView ivCommentUserText;
            @BindView(R.id.iv_comment_user_image)
            ImageView ivCommentUserImage;

            private Comment comment;

            public ViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            void bindContent(Comment comment) {
                this.comment = comment;
            }

            @OnClick(R.id.item_view)
            void onItemClick(){
                if(comment.getUserId()==mCurrentUserId){
                    editComment();
                }
            }

            private void editComment(){
                if (dialog == null){
                    dialog = new Dialog(getContext());
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog.setContentView(R.layout.dialog_edit_comment);
                    /*dialog.findViewById(R.id.btn_remove).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //Onremove click listener
                            dialog.dismiss();
                            dialog = null;
                        }
                    });*/
                    EditText editText = (EditText) dialog.findViewById(R.id.et_comment);
                    editText.append(comment.getComment());
                    dialog.findViewById(R.id.btn_update).setOnClickListener(v -> {
                        //onUpdate click listener
                        dialog.dismiss();
                        dialog = null;
                        String updatedCommentText = editText.getText().toString().trim();
                        updateDateComment(mBoardId,comment.getCommentId(),updatedCommentText);
                    });
                    dialog.setCancelable(false);
                    dialog.show();
                }
            }


            private void updateDateComment(int boardId, int commentId, String commentText) {
                showProgress(true);
                ModelManager.modelManager().editComment(boardId, commentId, commentText, (Constants.Status iStatus) -> {
                    comment.setComment(commentText);
                    notifyDataSetChanged();
                    showProgress(false);
                    checkEmptyScreen();
                }, (Constants.Status iStatus, String message) -> {
                    Toaster.toast(message);
                    checkEmptyScreen();
                    showProgress(false);
                });
            }
        }
    }

    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) progressDialog.dismiss();
        }
    }


}
