package com.app.inn.peepsin.UI.fragments.Profile;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.APIManager.APIManager;
import com.app.inn.peepsin.Managers.APIManager.APIRequestHelper;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.Address;
import com.app.inn.peepsin.Models.GeoAddress;
import com.app.inn.peepsin.Models.Shop;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.fragments.Shop.CreateShopFragment;
import com.app.inn.peepsin.UI.fragments.Shop.ProductIntermediateFragment;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import com.chaos.view.PinView;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.app.inn.peepsin.Constants.Constants.kAddress;
import static com.app.inn.peepsin.Constants.Constants.kAddressId;
import static com.app.inn.peepsin.Constants.Constants.kAuthToken;
import static com.app.inn.peepsin.Constants.Constants.kBankDetails;
import static com.app.inn.peepsin.Constants.Constants.kContactId;
import static com.app.inn.peepsin.Constants.Constants.kContactIdType;
import static com.app.inn.peepsin.Constants.Constants.kCurrentUser;
import static com.app.inn.peepsin.Constants.Constants.kIsVerified;
import static com.app.inn.peepsin.Constants.Constants.kLatitude;
import static com.app.inn.peepsin.Constants.Constants.kLongitude;
import static com.app.inn.peepsin.Constants.Constants.kOTP;
import static com.app.inn.peepsin.Constants.Constants.kRecords;
import static com.app.inn.peepsin.Constants.Constants.kResults;
import static com.app.inn.peepsin.Constants.Constants.kShopType;
import static com.app.inn.peepsin.Constants.Constants.kType;
import static com.app.inn.peepsin.Constants.Constants.kZipcode;

/**
 * Created by akaMahesh on 2/5/17
 * contact : mckay1718@gmail.com
 */

public class ShopOTPFragment extends Fragment {

    private final static String TAG = ProfileFragment.class.getSimpleName();
    static Switcher profileSwitcherListener;

    @BindView(R.id.tv_otp)
    TextView tvOTP;
    @BindView(R.id.btn_verify)
    Button btnVerify;
    @BindView(R.id.otp_view)
    PinView otpView;
    @BindView(R.id.tv_title)
    TextView tvTitle;

    CreateShopFragment.Type mShopType ;
    Constants.ContactIDType contactIDType;
    private String ContactId;
    private String PIN;
    private ProgressDialog progressDialog;
    private static HashMap<String, Object> userMap;

    public static Fragment newInstance(Constants.ContactIDType contactIDType, CreateShopFragment.Type type, Switcher switcherListener, String pin, String contactId, HashMap<String, Object> hashMap) { //1 for phone 2 for email
        ShopOTPFragment fragment = new ShopOTPFragment();
        profileSwitcherListener = switcherListener;
        userMap = hashMap;
        Bundle bundle = new Bundle();
        bundle.putSerializable(kContactIdType, contactIDType);
        bundle.putSerializable(kShopType, type);
        bundle.putString(kOTP, pin);
        bundle.putString(kContactId, contactId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = Utils.generateProgressDialog(getContext(), false);
        Bundle bundle = getArguments();
        if (bundle != null) {
            mShopType = (CreateShopFragment.Type) bundle.getSerializable(kShopType);
            contactIDType = (Constants.ContactIDType) bundle.getSerializable(kContactIdType);
            PIN = bundle.getString(kOTP);
            ContactId = bundle.getString(kContactId);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile_otp, container, false);
        ButterKnife.bind(this, view);
        if (contactIDType.equals(Constants.ContactIDType.PHONE)) {
           // tvTitle.setText(getString(R.string.title_phone_verification));
            btnVerify.setText("Verify Phone");
        } else if (contactIDType.equals(Constants.ContactIDType.EMAIL)) {
           // tvTitle.setText(getString(R.string.title_email_verificaition));
            btnVerify.setText("Verify Email");
        }

        //To show otp
        Toaster.toastOTP(PIN);
        tvTitle.setText("OTP Verification");
        return view;
    }

    @OnClick(R.id.btn_back)
    void onBack() {
        Utils.hideKeyboard(getContext());
        getFragmentManager().popBackStack();
    }

    @OnClick(R.id.btn_verify)
    void verify() {
        Utils.hideKeyboard(getContext());
        String otpString = otpView.getText().toString();
        if (otpString.equals(PIN)) {
            verifyOTP(ContactId, otpString);
        } else {
            otpView.setText("");
            Utils.showKeyboard(getContext(), otpView);
            Toaster.toast("Worng OTP");
        }
    }

    private void verifyOTP(String contactId, String OTP) {
        String authToken = ModelManager.modelManager().getCurrentUser().getAuthToken();
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(kAuthToken, authToken);
        parameters.put(kContactId, contactId);
        parameters.put(kOTP, OTP);
        showProgress(true);
        ModelManager.modelManager().otpVerification(parameters, (Constants.Status iStatus, GenricResponse<HashMap<String, Object>> genericResponse) -> {
            HashMap<String, Object> hashMap = genericResponse.getObject();
            Integer isVerified = (Integer) hashMap.get(kIsVerified);
            if (isVerified.equals(1)) {
                createShop();
            } else {
                showProgress(false);
                Utils.showAlertDialog(getContext(), "Process Error", "Sorry , Your credential were not verified!");
                onBack();
            }
            otpView.setText("");
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Log.e(TAG, message);
            Toaster.toast(message);
        });
    }



    private void createShop() {
        showProgress(true);
        ModelManager.modelManager().createNewShop(userMap, (Constants.Status iStatus, GenricResponse<Shop> genricResponse) -> {
            showProgress(false);
            if (mShopType.equals(CreateShopFragment.Type.FROM_PROFILE) && profileSwitcherListener!=null)
                profileSwitcherListener.switchFragment(ProfileFragment.newInstance(profileSwitcherListener), false, true);
            else if(mShopType.equals(CreateShopFragment.Type.CREATE_SHOP) && profileSwitcherListener!=null)
                profileSwitcherListener.switchFragment(ProductIntermediateFragment.newInstance(profileSwitcherListener), true, true);
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Toaster.toast(message);
        });
    }

    @OnClick(R.id.tv_resend)
    void resend() {
        resetOTP(ContactId);
    }

    private void resetOTP(String email) {
        showProgress(true);
        ModelManager.modelManager().contactVerification(email, (Constants.Status iStatus, GenricResponse<HashMap<String, Object>> genericResponse) -> {
            HashMap<String, Object> map = genericResponse.getObject();
            PIN = (String) map.get(kOTP);
            showProgress(false);
            Toaster.toastOTP(PIN);
        }, (Constants.Status iStatus, String message) -> {
            Toaster.toast(message);
            showProgress(false);
        });
    }

    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) progressDialog.dismiss();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        profileSwitcherListener = null;
    }

}
