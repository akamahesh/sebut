package com.app.inn.peepsin.Models;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by root on 30/6/17.
 */

public class ReviewModel extends BaseModel implements Serializable{

    private Integer userId;
    private String username;
    private Integer ratingReviewId;
    private String review;
    private String timeStamp;
    private Integer rating;

    public ReviewModel(JSONObject jsonResponse){
        try {
            this.userId         = getValue(jsonResponse, kUserId, Integer.class);
            this.username       = getValue(jsonResponse,kUserName,String.class);
            this.ratingReviewId = getValue(jsonResponse,kRatingReviewId,Integer.class);
            this.rating         = getValue(jsonResponse,kRating,Integer.class);
            this.timeStamp = getValue(jsonResponse,kTimeStamp,String.class);
            this.review = getValue(jsonResponse,kReview,String.class);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public Integer getUserId() {
        return userId;
    }

    public String getUsername() {
        return username;
    }

    public Integer getRatingReviewId() {
        return ratingReviewId;
    }

    public String getReview() {
        return review;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public Integer getRating() {
        return rating;
    }
}
