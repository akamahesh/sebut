package com.app.inn.peepsin.Models;


import java.io.Serializable;
import org.json.JSONObject;

/**
 * Created by akaMahesh on 24/8/17
 * contact : mckay1718@gmail.com
 */

public class TAX extends BaseModel implements Serializable {
  private Integer taxId;
  private String taxName;
  private String taxValue;
  private String taxType;

  public TAX(JSONObject jsonResponse) {
    this.taxId      = getValue(jsonResponse, kTaxId,      Integer.class);
    this.taxName    = getValue(jsonResponse, kTaxName,    String.class);
    this.taxValue   = getValue(jsonResponse, kTaxValue,   String.class);
    this.taxType    = getValue(jsonResponse, kTaxType,    String.class);

  }

  public Integer getTaxId() {
    return taxId;
  }

  public void setTaxId(Integer taxId) {
    this.taxId = taxId;
  }

  public String getTaxName() {
    return taxName;
  }

  public void setTaxName(String taxName) {
    this.taxName = taxName;
  }

  public String getTaxValue() {
    return taxValue;
  }

  public void setTaxValue(String taxValue) {
    this.taxValue = taxValue;
  }

  public String getTaxType() {
    return taxType;
  }

  public void setTaxType(String taxType) {
    this.taxType = taxType;
  }
}
