package com.app.inn.peepsin.Managers.SocialManager;

import android.content.Context;
import android.os.Bundle;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.app.inn.peepsin.Models.SocialUser;

import org.json.JSONObject;

import static com.app.inn.peepsin.Managers.SocialManager.SocialConstants.kFacebookAllFields;
import static com.app.inn.peepsin.Managers.SocialManager.SocialConstants.kFacebookFields;

/**
 * Created by mahesh on 19/4/17.
 */

public class FacebookManager {
    private CallbackManager callbackManager;
    private Context context;
    private FacebookManagerInterface facebookManagerListener;

    public FacebookManager(Context context, FacebookManagerInterface facebookManagerListener) {
        this.context =context;
        this.facebookManagerListener = facebookManagerListener;
        callbackManager= CallbackManager.Factory.create();
    }

    public CallbackManager getCallbackManager(){
        return callbackManager;
    }

    public FacebookCallback<LoginResult> getFacebookCallback(){
        return facebookCallback;
    }

    //this method deals with Facebook callbacks
    private FacebookCallback<LoginResult> facebookCallback=new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            requestData();
        }

        @Override
        public void onCancel() {
            logout();
            facebookManagerListener.failure("Facebook Login Cancelled");
        }

        @Override
        public void onError(FacebookException error) {
            logout();
            facebookManagerListener.failure(error.toString());
        }
    };

    //this method request for user specification and reflect changer to the activity through interfaces
    private void requestData(){
        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), (object, response) -> {
            JSONObject jsonObject = response.getJSONObject();
            SocialUser facebookUser = new SocialUser(jsonObject);
            facebookManagerListener.success(facebookUser);
        });
        Bundle parameters = new Bundle();
        parameters.putString(kFacebookFields,kFacebookAllFields );
        request.setParameters(parameters);
        request.executeAsync();
    }

    public void onLogout(){
        new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest
                .Callback() {
            @Override
            public void onCompleted(GraphResponse graphResponse) {
                logout();
            }
        }).executeAsync();
    }

    public void logout() {
        LoginManager.getInstance().logOut();
    }


    public interface FacebookManagerInterface{
        void success(SocialUser facebookUser);
        void failure(String s);
    }

}
