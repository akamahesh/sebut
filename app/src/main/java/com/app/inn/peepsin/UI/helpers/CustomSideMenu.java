package com.app.inn.peepsin.UI.helpers;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Libraries.Firebase.app.Config;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Managers.XMPPManager.RoosterHelper;
import com.app.inn.peepsin.Managers.XMPPManager.RoosterManager;
import com.app.inn.peepsin.Models.BaseModel;
import com.app.inn.peepsin.Models.CurrentUser;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.activities.FrontActivity;
import com.squareup.picasso.Picasso;
import com.yarolegovich.slidingrootnav.SlidingRootNav;

import butterknife.ButterKnife;

import static com.app.inn.peepsin.Constants.Constants.kChats;
import static com.app.inn.peepsin.Constants.Constants.kCurrentUser;

/**
 * Created by Triveni on 8/10/2017.
 */

public class CustomSideMenu {
    public static  ProgressDialog progressDialog;
   public static SlidingRootNav slidingRootNav;
   // CurrentUser currentUser;

    public static void setCustomSideMenu(Activity activity, SlidingRootNav customSlidingRootNav,
                                  CustomViewPager viewPager){
        progressDialog = Utils.generateProgressDialog(activity, true);
        slidingRootNav=customSlidingRootNav;
        CurrentUser currentUser = ModelManager.modelManager().getCurrentUser();
        String imageUrl = currentUser.getProfilePicURL();

        RelativeLayout rLShopList= ButterKnife.findById(slidingRootNav.getLayout(),R.id.relative_shop_list);
        RelativeLayout rLMyCircle= ButterKnife.findById(slidingRootNav.getLayout(),R.id.relative_my_circle);
        RelativeLayout rLPeepsBoard= ButterKnife.findById(slidingRootNav.getLayout(),R.id.relative_peepsboards);
        RelativeLayout rLChat= ButterKnife.findById(slidingRootNav.getLayout(),R.id.relative_chat);
        RelativeLayout rLProfile= ButterKnife.findById(slidingRootNav.getLayout(),R.id.retative_profile);
        RelativeLayout rLLogout= ButterKnife.findById(slidingRootNav.getLayout(),R.id.relative_logout);
        ImageView profileimg= ButterKnife.findById(slidingRootNav.getLayout(),R.id.iv_user_image_circular);
        TextView tvusername=ButterKnife.findById(slidingRootNav.getLayout(),R.id.tv_user_name);
        tvusername.setText(currentUser.getFullName());
        if (!imageUrl.isEmpty()) {
            Picasso.with(activity)
                    .load(imageUrl)
                    .placeholder(R.drawable.img_profile_placeholder)
                    .into(profileimg);
        }
        rLShopList.setOnClickListener(v -> {
            //  FragmentUtil.changeFragment(getChildFragmentManager(), ShopsFragment.newInstance(switchListener),true,false);
            viewPager.setCurrentItem(0);
            slidingRootNav.closeMenu();
        });
        rLMyCircle.setOnClickListener(v -> {
            //FragmentUtil.changeFragment(getChildFragmentManager(), RootConnectionFragment.newInstance(),true,false);
            viewPager.setCurrentItem(1);
            slidingRootNav.closeMenu();
        });
        rLPeepsBoard.setOnClickListener(v -> {
            viewPager.setCurrentItem(2);
            slidingRootNav.closeMenu();
        });
        rLChat.setOnClickListener(v -> {
            viewPager.setCurrentItem(3);
            slidingRootNav.closeMenu();
        });
        rLProfile.setOnClickListener(v -> {
            viewPager.setCurrentItem(4);
            slidingRootNav.closeMenu();
        });
        rLLogout.setOnClickListener(v -> logoutAccount(activity));

    }


    public static void logoutAccount(Activity activity) {
        showProgress(true);
        SharedPreferences pref = activity.getSharedPreferences(Config.SHARED_PREF, 0);
        String firebaseToken = pref.getString("regId", null);

        ModelManager.modelManager().getLogout(firebaseToken, (Constants.Status iStatus) -> {
            showProgress(false);
            clearContent(activity);
            Intent intent = FrontActivity.getIntent(activity);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            activity.startActivity(intent);
            activity.finish();
        }, (Constants.Status iStatus, String message) -> {
            Toaster.toast(message);
            showProgress(false);
        });
    }

    public static void clearContent(Activity activity) {

       /* if (Build.VERSION_CODES.KITKAT <= Build.VERSION.SDK_INT && false) {
            ((ActivityManager) getContext().getSystemService(ACTIVITY_SERVICE))
                    .clearApplicationUserData(); // note: it has a return value!
        }*/
        SharedPreferences preferences = activity.getSharedPreferences(BaseModel.kAppPreferences, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(kCurrentUser);
        editor.commit();
        SharedPreferences chatpreferences = activity.getSharedPreferences(RoosterHelper.kChatPreferences, Context.MODE_PRIVATE);
        SharedPreferences.Editor chateditor = chatpreferences.edit();
        chateditor.remove(kChats);
        chateditor.commit();
        {
            ModelManager.modelManager().setCurrentUser(null);
        }
        RoosterManager.roost().disconnectFromXMPPServer();
    }

    public static void showProgress(boolean b) {

        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) progressDialog.cancel();
        }
    }
}
