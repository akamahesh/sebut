package com.app.inn.peepsin.UI.fragments.Profile;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.DataBase.DataBaseHandler;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.Address;
import com.app.inn.peepsin.Models.BlockedUser;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.helpers.DividerItemRecyclerDecoration;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kCurrentUser;
import static com.app.inn.peepsin.Constants.Constants.kIsUserBlocked;
import static com.app.inn.peepsin.Constants.Constants.kUserId;

/**
 * Created by Harsh on 5/9/2017.
 */

public class BlockedUserFragment extends Fragment {

    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_user_image)
    ImageView ivUserImage;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_about)
    TextView tvAbout;
    @BindView(R.id.tv_unblockUser)
    TextView tvUnblockUser;
    @BindView(R.id.recycler_view_feeds)
    RecyclerView recyclerViewFeeds;
    @BindView(R.id.empty_view)
    View emptyView;
    @BindView(R.id.tv_empty_message)
    TextView tvEmptyTextMessage;
    @BindView(R.id.tv_empty_title)
    TextView tvEmptyTitle;
    ProgressDialog progressDialog;
    Paint p = new Paint();

    private FriendsBlockedAdapter friendsBlockedAdapter;
    private List<BlockedUser> blockedUsersList;
    DataBaseHandler dbHandler;

    public static BlockedUserFragment newInstance() {
        return new BlockedUserFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_blocked_user, container, false);
        ButterKnife.bind(this, view);
        dbHandler= new DataBaseHandler(getContext());
        progressDialog = Utils.generateProgressDialog(getContext(), false);
        blockedUsersList = new ArrayList<>();
        friendsBlockedAdapter = new FriendsBlockedAdapter(blockedUsersList);
        recyclerViewFeeds.setHasFixedSize(true);
        recyclerViewFeeds.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerViewFeeds.setAdapter(friendsBlockedAdapter);
        recyclerViewFeeds.addItemDecoration(new DividerItemRecyclerDecoration(getActivity(), R.drawable.canvas_recycler_divider));
        swipeToDismissTouchHelper.attachToRecyclerView(recyclerViewFeeds);

        tvName.setText(ModelManager.modelManager().getCurrentUser().getFullName());
        tvTitle.setText(getString(R.string.blocked_people));
        tvAbout.setText(ModelManager.modelManager().getCurrentUser().getFirstName().concat(getString(R.string.emmays_blocked_people)));
        tvUnblockUser.setText(getString(R.string.swipe_to_unblock));
        tvEmptyTitle.setText(getString(R.string.empty_title_blocked_user));
        tvEmptyTextMessage.setText(getString(R.string.empty_message_blocked_user));
        String currentuserImage = ModelManager.modelManager().getCurrentUser().getProfilePicURL();
        if (!currentuserImage.isEmpty()) {
            Picasso.with(getContext()).load(currentuserImage).into(ivUserImage);
        } else {
            ivUserImage.setImageResource(R.drawable.img_profile_placeholder);
        }
        return view;
    }


    ItemTouchHelper swipeToDismissTouchHelper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            return false;
        }


        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            int swipedPosition = viewHolder.getAdapterPosition();
            BlockedUser genericUser = friendsBlockedAdapter.getItem(swipedPosition);
            HashMap<String, Object> blockedUserMap = new HashMap<>();

            blockedUserMap.put(kUserId, genericUser.getUserId());
            blockedUserMap.put(kIsUserBlocked,0);
            blockUnblockUser(blockedUserMap, genericUser);
        }

        @Override
        public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
            Bitmap icon;
            if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
                View itemView = viewHolder.itemView;
                float height = (float) itemView.getBottom() - (float) itemView.getTop();
                float width = height / 3;

                p.setColor(Color.parseColor("#D32F2F"));
                RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(), (float) itemView.getRight(), (float) itemView.getBottom());
                c.drawRect(background, p);
                icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_close_white_24dp);
                RectF icon_dest = new RectF((float) itemView.getRight() - 2 * width, (float) itemView.getTop() + width, (float) itemView.getRight() - width, (float) itemView.getBottom() - width);
                c.drawBitmap(icon, null, icon_dest, p);

            }
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
        }
    });


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData();
    }

    @OnClick(R.id.btn_back)
    void onBack() {
        getFragmentManager().popBackStack();
    }

    public void blockUnblockUser(HashMap<String, Object> privacyMap, BlockedUser genericUser) {
        showProgress(true);
        ModelManager.modelManager().blockUnblockUser(dbHandler,privacyMap, (Constants.Status iStatus,GenricResponse<Integer> genricResponse) -> {
            blockedUsersList.remove(genericUser);
            friendsBlockedAdapter.notifyDataSetChanged();
            showProgress(false);
            checkEmptyScreen();
            broadCastChanges();
        }, (Constants.Status iStatus, String message) -> {
            Toaster.toast(message);
            showProgress(false);
            friendsBlockedAdapter.notifyDataSetChanged();
            checkEmptyScreen();
        });

    }

    private void broadCastChanges() {
        Intent changeIntent = new Intent(kCurrentUser);
        LocalBroadcastManager.getInstance(getContext()).sendBroadcast(changeIntent);
    }

    private void loadData() {
        showProgress(true);
        ModelManager.modelManager().getBlockedUserList((Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<BlockedUser>> genericList) -> {
            showProgress(false);
            blockedUsersList.clear();
            blockedUsersList.addAll(genericList.getObject());
            friendsBlockedAdapter.notifyDataSetChanged();
            checkEmptyScreen();
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Toaster.toast(message);
            checkEmptyScreen();
        });
    }


    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) progressDialog.cancel();
        }
    }

    private void checkEmptyScreen() {
        if (friendsBlockedAdapter.getItemCount() > 0) {
            emptyView.setVisibility(View.GONE);
        } else {
            emptyView.setVisibility(View.VISIBLE);
        }
    }

    class FriendsBlockedAdapter extends RecyclerView.Adapter<FriendsBlockedAdapter.StuffHolder> {
        private List<BlockedUser> blockedUsersList;
        private ColorGenerator generator = ColorGenerator.MATERIAL;

        FriendsBlockedAdapter(List<BlockedUser> blockedUsers) {
            this.blockedUsersList = blockedUsers;
        }


        @Override
        public BlockedUserFragment.FriendsBlockedAdapter.StuffHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_user_profile_friends_layout, parent, false);
            return new BlockedUserFragment.FriendsBlockedAdapter.StuffHolder(layoutView);
        }

        @Override
        public void onBindViewHolder(BlockedUserFragment.FriendsBlockedAdapter.StuffHolder holder, int position) {
            BlockedUser blockedUser = blockedUsersList.get(position);
            holder.bindContent(blockedUser);

            String name = blockedUser.getFirstName();
            holder.tvName.setText(blockedUser.getFirstName());

            Address primaryAddress = blockedUser.getPrimaryAddress();
            String address = "No Location Available";
            if(primaryAddress!=null){
                address = primaryAddress.getCity()+", "+primaryAddress.getState()+", "+primaryAddress.getCountry();
            }
            holder.tvAddress.setText(address);

            String profilepicurl = blockedUser.getProfilePicURL();
            if(profilepicurl.isEmpty()){
                holder.ivUserImageCircular.setVisibility(View.INVISIBLE);
                holder.ivUserImage.setVisibility(View.VISIBLE);
            }else {
                holder.ivUserImageCircular.setVisibility(View.VISIBLE);
                holder.ivUserImage.setVisibility(View.INVISIBLE);
            }

            if(!name.isEmpty()){
                TextDrawable drawable = TextDrawable.builder().buildRound(String.valueOf(name.toUpperCase().charAt(0)), generator.getRandomColor());

                if (!profilepicurl.isEmpty()) {
                    Picasso.with(getContext())
                            .load(profilepicurl)
                            .placeholder(R.drawable.placeholder_300)
                            .into(holder.ivUserImageCircular);
                }else
                    holder.ivUserImage.setImageDrawable(drawable);
            }
        }

        BlockedUser getItem(int index) {
            return blockedUsersList.get(index);
        }


        @Override
        public int getItemCount() {
            return blockedUsersList.size();
        }

        class StuffHolder extends RecyclerView.ViewHolder {
            BlockedUser user;
            @BindView(R.id.tv_name)
            TextView tvName;
            @BindView(R.id.iv_user_image_circular)
            ImageView ivUserImageCircular;
            @BindView(R.id.iv_user_image)
            ImageView ivUserImage;
            @BindView(R.id.tv_address)
            TextView tvAddress;

            StuffHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
              /*  itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                    }
                });*/
            }

            public void bindContent(BlockedUser blockedUser) {
                this.user = blockedUser;
            }
        }
    }
}
