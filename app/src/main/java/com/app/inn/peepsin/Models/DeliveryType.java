package com.app.inn.peepsin.Models;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by dhruv on 17/8/17.
 */

public class DeliveryType extends BaseModel implements Serializable {

    private Integer id;
    private String name;

    public DeliveryType(JSONObject jsonResponse) {
        this.id   = getValue(jsonResponse, kDeliveryTimeId, Integer.class);
        this.name = getValue(jsonResponse, kDeliveryTime, String.class);
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
