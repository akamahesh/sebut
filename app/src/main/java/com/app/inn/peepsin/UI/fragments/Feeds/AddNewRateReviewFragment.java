package com.app.inn.peepsin.UI.fragments.Feeds;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.ReviewModel;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import com.squareup.picasso.Picasso;
import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import static com.app.inn.peepsin.Constants.Constants.kMyRating;
import static com.app.inn.peepsin.Constants.Constants.kMyReview;
import static com.app.inn.peepsin.Constants.Constants.kProductId;
import static com.app.inn.peepsin.Constants.Constants.kShopBannerImageURl;
import static com.app.inn.peepsin.Constants.Constants.kshopName;

/**
 * Created by Harsh on 9/22/2017.
 */

public class AddNewRateReviewFragment extends Fragment {

    static Switcher switcherListener;
    @BindView(R.id.ratingBar)
    RatingBar myRatingBar;
    @BindView(R.id.edt_review)
    EditText edtReview;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_rating)
    TextView tvRating;
    @BindView(R.id.tv_shop_name)
    TextView tvShopName;
    @BindView(R.id.iv_item)
    ImageView ivItem;
    @BindDrawable(R.drawable.img_product_placeholder)
    Drawable placeholder;

    private ProgressDialog progressDialog;
    private int productId;
    private Integer rating = 1;
    private String review = "";
    private String shopName="";
    private String shopImageUrl="";



    public static Fragment newInstance(Switcher switcher,int productId,int rating,String review,String shopName, String shopImageUrl) {
        switcherListener = switcher;
        Fragment fragment = new AddNewRateReviewFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(kProductId, productId);
        bundle.putInt(kMyRating, rating);
        bundle.putString(kMyReview, review);
        bundle.putString(kshopName,shopName);
        bundle.putString(kShopBannerImageURl,shopImageUrl);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = Utils.generateProgressDialog(getContext(), true);
        Bundle bundle = getArguments();
        if (bundle != null) {
            productId = bundle.getInt(kProductId);
            rating = bundle.getInt(kMyRating);
            review = bundle.getString(kMyReview);
            shopName=bundle.getString(kshopName);
            shopImageUrl=bundle.getString(kShopBannerImageURl);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_new_rate_review_layout, container, false);
        ButterKnife.bind(this, view);
        if(rating==0) { rating=1; myRatingBar.setRating(rating);}
        myRatingBar.setRating(rating);
        tvTitle.setText("Add a new review");
        LayerDrawable stars = (LayerDrawable) myRatingBar.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(Color.parseColor("#FAD368"), PorterDuff.Mode.SRC_ATOP);
        if(!review.isEmpty())
            edtReview.setText(review);
        String ratings =String.valueOf(rating);
        tvRating.setText(ratings);
        tvShopName.setText(shopName);
        if (!shopImageUrl.isEmpty()) {
            Picasso.with(getContext())
                    .load(shopImageUrl)
                    .error(placeholder)
                    .placeholder(placeholder)
                    .into(ivItem);

        }else
            ivItem.setImageDrawable(placeholder);
        return view;
    }


    @OnClick(R.id.btn_save)
    void onSubmit(){
        String reviewTxt = edtReview.getText().toString();
        if(reviewTxt.isEmpty())
            edtReview.setError("Review can not be empty");
        else if(myRatingBar.getRating()==0){
            Toaster.toast("Input a rating");
        }
        else {
            showProgress(true);
            int rating = Math.round(myRatingBar.getRating());
            ModelManager.modelManager().editAddRating(productId,reviewTxt,rating,(Constants.Status iStatus, GenricResponse<ReviewModel> genericResponse) -> {
                ReviewModel review = genericResponse.getObject();
                Log.v("Rating & Review","Rating:"+review.getRating()+",Review:"+review.getReview());
               // getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK,getActivity().getIntent());
               // dismiss();
                getFragmentManager().popBackStack();
                showProgress(false);
            }, (Constants.Status iStatus, String message) -> {
                showProgress(false);
                Toaster.toast(message);
            });
        }

    }
    @OnClick(R.id.btn_cancel)
    void onCancel(){
        getFragmentManager().popBackStack();
        Utils.hideKeyboard(getContext());
    }

    @OnClick(R.id.btn_back)
    void onBack() {
        getFragmentManager().popBackStack();
        Utils.hideKeyboard(getContext());
    }

    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) progressDialog.cancel();
        }
    }


}
