package com.app.inn.peepsin.UI.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.CurrentUser;
import com.app.inn.peepsin.Models.Feeds;
import com.app.inn.peepsin.Models.SharedProduct;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.activities.HomeActivity;
import com.app.inn.peepsin.UI.fragments.Feeds.MapDialogFragment;
import com.app.inn.peepsin.UI.fragments.Feeds.ProductDetailFragment;
import com.app.inn.peepsin.UI.helpers.MyBounceInterpolator;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kImageUrl;
import static com.app.inn.peepsin.Constants.Constants.kLatitude;
import static com.app.inn.peepsin.Constants.Constants.kLongitude;
import static com.app.inn.peepsin.Constants.Constants.kOwnerName;

/**
 * Created by dhruv on 27/7/17.
 */

public class SharedProductAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {

    private List<SharedProduct> filterList;
    private List<SharedProduct> productList;
    private Context context;
    private static Switcher switcherListener;
    private ProgressDialog progressDialog;
    private CurrentUser user;
    private int pageSize;

    public SharedProductAdapter(Context context, List<SharedProduct> items, Switcher switcher) {
        switcherListener = switcher;
        user = ModelManager.modelManager().getCurrentUser();
        progressDialog = Utils.generateProgressDialog(context, false);
        productList = items;
        filterList = items;
        this.context = context;
        this.pageSize = items.size();
    }

    @Override
    public int getItemViewType(int position) {
        if(position == productList.size())
            return 0;
        else
            return 1;
    }

    public void addNewItems(CopyOnWriteArrayList<SharedProduct> list){
        productList.clear();
        productList.addAll(list);
        notifyDataSetChanged();
    }

    public void addItems(CopyOnWriteArrayList<SharedProduct> list){
        productList.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == 1) {
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_shared_product_layout, parent, false);
            return new SharedProductAdapter.FeedViewHolder(layoutView);
        }
        else{
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_feed_progress, parent, false);
            return new SharedProductAdapter.FeedProgressHolder(layoutView);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

        if(position == productList.size()) {
            SharedProductAdapter.FeedProgressHolder view = (SharedProductAdapter.FeedProgressHolder) viewHolder;
            if(productList.size()>=pageSize){
                view.progressBar.setVisibility(View.VISIBLE);
                view.tvFeedCount.setVisibility(View.GONE);
                new Handler().postDelayed(() -> {
                    //Hide the refresh after 2sec
                    ((HomeActivity)context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            view.progressBar.setVisibility(View.GONE);
                            view.tvFeedCount.setVisibility(View.VISIBLE);
                            String countTxt = context.getString(R.string.feed_total_text)+ productList.size();
                            view.tvFeedCount.setText(countTxt);
                        }
                    });
                }, 500);
            }

        }else{
            SharedProductAdapter.FeedViewHolder holder = (SharedProductAdapter.FeedViewHolder) viewHolder;
            SharedProduct item = productList.get(position);
            holder.bindContent(item);
            String name = item.getName();
            String shopName = item.getShopName();
            String postingDate =item.getPostingDate();
            String sellerName = "";
            int connectionLevel = item.getConnectionLevel();
            if(item.getSeller()!=null){
                sellerName = item.getSeller().getFirstName() + " " + item.getSeller().getLastName();
            }
            String thumbnailURL = item.getImageThumbUrl();
            String addressTxt = "No Location Available";
            if (item.getAddress() != null)
                addressTxt = item.getAddress().getCity() + ", " + item.getAddress().getState() +", "+ item.getAddress().getCountry();

            String price = item.getPrice();
            String sellingPrice = item.getSellingPrice();


            String discountTagline = item.getDiscountTagline();
            if(discountTagline.isEmpty())
                discountTagline = "No Offer Available Now!";

            holder.tvSellingPrice.setText(context.getString(R.string.Rs)+sellingPrice);
            holder.tvPrice.setText(context.getString(R.string.Rs)+price);
            holder.tvPrice.setPaintFlags(holder.tvPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.tvOffer.setText(discountTagline);
            holder.tvShopName.setText(shopName);
            holder.textViewItemName.setText(name);
            holder.textViewLocation.setText(addressTxt);
            holder.textViewConnection.setText(Utils.getUserName(sellerName,connectionLevel));
            holder.textViewTime.setText(postingDate);
            holder.tvStarRating.setText(String.valueOf(item.getRating()));

            if (item.getFavourite()) {
                holder.imageViewFav.setImageResource(R.drawable.ic_favorite_red_700_24dp);
            } else {
                holder.imageViewFav.setImageResource(R.drawable.ic_favorite_border_24dp);
            }


            price           = price.replaceAll("/","0").replaceAll("-","0");
            sellingPrice    = sellingPrice.replaceAll("/","0").replaceAll("-","0");
            try {
                Float pricefloat = Float.parseFloat(price);
                Float sellingfloat = Float.parseFloat(sellingPrice);
                if (pricefloat <= sellingfloat) {
                    holder.tvPrice.setVisibility(View.GONE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (!item.getImageUrl().isEmpty())
                Picasso.with(context)
                        .load(thumbnailURL)
                        .placeholder(R.drawable.img_product_placeholder)
                        .into(holder.imageViewItem);
        }
    }

    @Override
    public int getItemCount() {
        return productList.size()+1;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    productList = filterList;
                } else {
                    ArrayList<SharedProduct> filteredList = new ArrayList<>();
                    for (SharedProduct SharedProduct : productList) {
                        if (SharedProduct.getName().toLowerCase().contains(charString)) {
                            filteredList.add(SharedProduct);
                        }
                    }
                    productList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = productList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                productList = (List<SharedProduct>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    class FeedProgressHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.tv_feed_count)
        TextView tvFeedCount;
        @BindView(R.id.progress_bar)
        ProgressBar progressBar;

        FeedProgressHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class FeedViewHolder extends RecyclerView.ViewHolder {
        SharedProduct item;
        @BindView(R.id.tv_shop_name)
        TextView tvShopName;
        @BindView(R.id.tv_name)
        TextView textViewItemName;
        @BindView(R.id.tv_connection)
        TextView textViewConnection;
        @BindView(R.id.tv_location)
        TextView textViewLocation;
        @BindView(R.id.tv_time)
        TextView textViewTime;
        @BindView(R.id.iv_item)
        ImageView imageViewItem;
        @BindView(R.id.iv_fav)
        ImageView imageViewFav;
        @BindView(R.id.tv_star_rating)
        TextView tvStarRating;

        @BindView(R.id.tv_price)
        TextView tvPrice;
        @BindView(R.id.tv_selling_price)
        TextView tvSellingPrice;
        @BindView(R.id.tv_offer)
        TextView tvOffer;



        FeedViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.item_view)
        void onRowSelected() {
            getFeedDetails(item.getId());
        }

        private void getFeedDetails(int productId) {
            showProgress(true);
            ModelManager.modelManager().getProductDetails(productId,(Constants.Status iStatus, GenricResponse<Feeds> genericResponse) -> {
                showProgress(false);
                if (switcherListener != null){
                    switcherListener.switchFragment(ProductDetailFragment.newInstance(productId, genericResponse.getObject(), switcherListener), true, true);
                }
            }, (Constants.Status iStatus, String message) -> {
                Toaster.toast(message);
                showProgress(false);
            });
        }

        @OnClick(R.id.iv_fav)
        void onMakeFavorite(ImageView imageView) {
            Boolean isFavorite = item.getFavourite();
            Animation favAnim = AnimationUtils.loadAnimation(context, R.anim.bounce);
            MyBounceInterpolator favInterpolar = new MyBounceInterpolator(.2, 20);
            favAnim.setInterpolator(favInterpolar);
            imageView.startAnimation(favAnim);
            if(isFavorite){
                makeFavorite(0,item.getId(),getAdapterPosition());
            }else {
                makeFavorite(1,item.getId(),getAdapterPosition());
            }
        }

        @OnClick(R.id.iv_location)
        void getLocation() {
            try{
                String lat = user.getPrimaryAddress().getLatitude();
                String sLat = item.getAddress().getLatitude();
                String sLon = item.getAddress().getLongitude();
                if(!lat.isEmpty() && !sLat.isEmpty()){
                    MapDialogFragment mapDialogFragment = MapDialogFragment.newInstance();
                    Bundle bdl = new Bundle();
                    bdl.putString(kLatitude,sLat);
                    bdl.putString(kLongitude,sLon);
                    bdl.putString(kOwnerName,item.getName());
                    bdl.putString(kImageUrl,item.getSeller().getProfilePicUrl());
                    mapDialogFragment.setArguments(bdl);
                    mapDialogFragment.setCancelable(true);
                    mapDialogFragment.show(((AppCompatActivity)context).getSupportFragmentManager(), mapDialogFragment.getClass().getSimpleName());

                }else
                    Toaster.toast("Sorry Lat Long UnAvailable");
            }catch (Exception e){
                Toaster.toast("Sorry Lat Long UnAvailable");
                e.printStackTrace();
            }
        }

        private void makeFavorite(Integer isFav,Integer productId,int position) {
            ModelManager.modelManager().setFavouriteProduct(productId, isFav, (Constants.Status iStatus) -> {
                productList.get(position).setFavourite(isFav==1);
                notifyItemChanged(position);
            }, (Constants.Status iStatus, String message) -> {
                Toaster.toast(message);
            });
        }

        private void bindContent(SharedProduct item) {
            this.item = item;
        }
    }

    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) progressDialog.cancel();
        }
    }
}
