package com.app.inn.peepsin.UI.helpers;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.widget.Toast;

import java.io.File;
import java.util.Random;


/**
 * Created by mahesh on 9/1/17.
 */

public class ImageEditor {
    private static ImageEditor _ImageEditor;


    public static ImageEditor getInstance(){
        if(_ImageEditor==null){
            _ImageEditor =new ImageEditor();
        }
        return _ImageEditor;
    }

    public File cropImage(Activity activity, Uri picUri, int code){
        //File imageFile=new File(FileManager.fileManager().getCacheDirectory(),"temp01");
        Random r=new Random();
        int n= (int)(Math.random()*10);

        String dir = Environment.getExternalStorageDirectory()+File.separator+"MyQuizly";
        //create folder
        File folder = new File(dir); //folder name
        folder.mkdirs();
        File imageFile = new File(dir, n+".jpg");

        Intent intent= cropImage(picUri, imageFile);
        if(intent!=null){
            activity.startActivityForResult(intent, code);
        }else {
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Toast.makeText(activity, errorMessage, Toast.LENGTH_SHORT).show();
        }
        return imageFile;
    }

    private Intent cropImage( Uri picUri, File outPutFile){
        int aspectX         = 0;
        int aspectY         = 0;
        int spotlightX      = 100;
        int spotlighty      = 100;
        Intent cropIntent = null;
        try {
            cropIntent = new Intent("com.android.camera.action.CROP");
            cropIntent.setDataAndType(picUri, "image");
            // set crop properties
            cropIntent.putExtra("crop", "true");
            cropIntent.putExtra("aspectX",aspectX);
            cropIntent.putExtra("aspectY", aspectY);
            cropIntent.putExtra("scaleUpIfNeeded", true);
            cropIntent.putExtra("spotlightX", spotlightX);
            cropIntent.putExtra("spotlightY", spotlighty);
            cropIntent.putExtra("scale", true);

            //Create output file here
            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(outPutFile));
        }
        catch (Exception e) {
            e.printStackTrace();
            cropIntent = null;
        }
        return cropIntent;
    }



}
