package com.app.inn.peepsin.UI.dialogFragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.inn.peepsin.Models.Comment;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.helpers.DividerItemRecyclerDecoration;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class CommentsDialogFragment extends DialogFragment {

    @BindView(R.id.tv_title) TextView tvTitle;
    @BindView(R.id.recycler_view_comments) RecyclerView mRecyclerView;
    @BindView(R.id.empty_view) View emptyView;
    @BindView(R.id.progress_bar) ProgressBar progressBar;
    @BindView(R.id.iv_user_image) ImageView ivUserImage;
    @BindView(R.id.edt_comment) EditText edtComment;
    @BindView(R.id.btn_comment)
    Button btnComment;
    private List<Comment> mCommentList;
    private LinearLayoutManager mLinearLayoutManager;
    private CommentAdapter mCommentAdapter;

    public static CommentsDialogFragment newInstance() {
        return new CommentsDialogFragment();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Black_NoTitleBar);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_post_detail_list, container, false);
        ButterKnife.bind(this, view);
        tvTitle.setText(getString(R.string.title_feeds));
        mCommentList = new ArrayList<>();
        mCommentAdapter = new CommentAdapter(mCommentList);
        mRecyclerView.setHasFixedSize(true);
        mLinearLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerView.setAdapter(mCommentAdapter);
        mRecyclerView.addItemDecoration(new DividerItemRecyclerDecoration(getActivity(), R.drawable.canvas_recycler_divider));
        edtComment.setOnEditorActionListener(onEditorActionListener);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mCommentList.add(new Comment());
        mCommentList.add(new Comment());
        mCommentList.add(new Comment());
        mCommentList.add(new Comment());
        mCommentList.add(new Comment());
        mCommentAdapter.notifyDataSetChanged();

    }

    EditText.OnEditorActionListener onEditorActionListener = (v, actionId, event) -> {
        if (actionId == EditorInfo.IME_ACTION_SEND ){
            String comment = edtComment.getText().toString().trim();
            if(comment.isEmpty())
                return false;
            comment(comment);
            return true;
        }
        return false;
    };

    private void comment(String comment) {
        mCommentList.add(new Comment());
        mCommentAdapter.notifyDataSetChanged();
    }

    @OnClick(R.id.ib_back)
    void onClose() {
        dismiss();
    }

    @OnClick(R.id.btn_comment)
    void onComment(){
        String comment = edtComment.getText().toString().trim();
        if(comment.isEmpty()) return;
        mCommentList.add(new Comment());
        mCommentAdapter.notifyDataSetChanged();
        edtComment.setText("");
        mRecyclerView.smoothScrollToPosition(mCommentAdapter.getItemCount());
    }

    class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.ViewHolder>{
        List<Comment> posts;

        CommentAdapter(List<Comment> posts) {
            this.posts = posts;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View layoutView = LayoutInflater.from(getContext()).inflate(R.layout.row_comment_layout, viewGroup, false);
            return new ViewHolder(layoutView);
        }

        @Override
        public void onBindViewHolder(ViewHolder viewHolder, int i) {

        }

        @Override
        public int getItemCount() {
            return posts.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder{

            public ViewHolder(View itemView) {
                super(itemView);
            }
        }
    }


}
