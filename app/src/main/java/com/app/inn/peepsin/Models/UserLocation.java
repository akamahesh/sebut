package com.app.inn.peepsin.Models;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by dhruv on 14/9/17.
 */

public class UserLocation extends BaseModel implements Serializable {

    private String streetAddress;
    private String city;
    private String state;
    private String country;
    private String zipCode;
    private String latitude;
    private String longitude;

    public UserLocation(JSONObject jsonResponse) {
        this.streetAddress            = getValue(jsonResponse, kStreetAddress,        String.class);
        this.city                     = getValue(jsonResponse, kCity,                 String.class);
        this.state                    = getValue(jsonResponse, kState,                String.class);
        this.country                  = getValue(jsonResponse, kCountry,              String.class);
        this.zipCode                  = getValue(jsonResponse, kZipcode,              String.class);
        this.latitude                 = getValue(jsonResponse, kLatitude,             String.class);
        this.longitude                = getValue(jsonResponse, kLongitude,            String.class);
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public String getCountry() {
        return country;
    }

    public String getZipCode() {
        return zipCode;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }
}
