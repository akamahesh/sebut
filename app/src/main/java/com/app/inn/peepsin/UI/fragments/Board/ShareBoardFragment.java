package com.app.inn.peepsin.UI.fragments.Board;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.Connection;
import com.app.inn.peepsin.Models.CurrentUser;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.activities.ShoppingCartActivity;
import com.app.inn.peepsin.UI.activities.TransactionDetailActivity;
import com.app.inn.peepsin.UI.dialogFragments.DialogShareProductFragment;
import com.app.inn.peepsin.UI.fragments.Feeds.CustomBottomSheetDialogFragment;
import com.app.inn.peepsin.UI.fragments.Profile.UserProfileFriendFragment;
import com.app.inn.peepsin.UI.helpers.DividerItemRecyclerDecoration;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.google.common.cache.RemovalCause;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TooManyListenersException;
import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnEditorAction;

import static com.app.inn.peepsin.Constants.Constants.SHARE_PRODUCT;
import static com.app.inn.peepsin.Constants.Constants.kId;
import static com.app.inn.peepsin.Constants.Constants.kUserId;
import static com.app.inn.peepsin.Constants.Constants.kWantToShare2ndLevel;
import static com.app.inn.peepsin.Constants.Constants.kWantToShare3ndLevel;

/**
 * Created by root on 15/6/17.
 */

public class ShareBoardFragment extends Fragment {
    @BindView(R.id.edt_search)
    EditText edtSearch;
    @BindView(R.id.empty_view)
    View emptyView;
    @BindView(R.id.recycler_view_connections)
    RecyclerView recyclerView;

    @BindView(R.id.btn_select_all)
    TextView tvSelectAll;
    @BindView(R.id.select_view)
    View selectView;
    @BindView(R.id.ib_checkbox)
    ImageView ivCheckBox;
    private ProgressDialog progressDialog;
    ConnectionAdapter connectionAdapter;
    private int boardId;
    private static Dialog dialog;
    private static CustomBottomSheetDialogFragment frag;

    private List<Connection> userList;

    public static Fragment newInstance(int productId, Dialog refer, CustomBottomSheetDialogFragment ref) {
        dialog = refer;
        frag = ref;
        Fragment fragment = new ShareBoardFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(kId, productId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = Utils.generateProgressDialog(getContext(), false);
        Bundle bundle = getArguments();
        if (bundle != null) {
            boardId = bundle.getInt(kId);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_share_product, container, false);
        ButterKnife.bind(this, view);
        CurrentUser currentUser = ModelManager.modelManager().getCurrentUser();
        connectionAdapter = new ConnectionAdapter(new ArrayList<>());
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DividerItemRecyclerDecoration(getActivity(), R.drawable.canvas_recycler_divider));
       // userList = ModelManager.modelManager().getConnections();
        loadData(currentUser.getUserId());

       // connectionAdapter = new ConnectionAdapter(userList);
        recyclerView.setAdapter(connectionAdapter);

        addTextListener(edtSearch);
        return view;
    }

    @OnClick(R.id.select_view)
    void selectAll() {
        if (tvSelectAll.getText().toString().equals("Select All")) {
            for (int i = 0; i < userList.size(); i++) {
                userList.get(i).setSelected(true);
            }
            tvSelectAll.setText(getString(R.string.unselect_all));
            ivCheckBox.setImageDrawable(getResources().getDrawable(R.drawable.ic_invite_check));
        } else
            clearSelection(userList);
        connectionAdapter.notifyDataSetChanged();
    }

    public void clearSelection(List<Connection> userList) {
        for (int i = 0; i < userList.size(); i++) {
            userList.get(i).setSelected(false);
        }
        tvSelectAll.setText(getString(R.string.select_all));
        ivCheckBox.setImageDrawable(getResources().getDrawable(R.drawable.ic_circle_thin));
    }



    public void loadData(int userid) {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(kUserId,userid);
        ModelManager.modelManager().getUserConnections(parameters,(Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<Connection>> genricResponse) -> {
           userList = getFilteredConnectionList(genricResponse.getObject());
            connectionAdapter.addItems(userList);
            clearSelection(userList);
            //checkEmptyScreen();
        }, (Constants.Status iStatus, String message) -> {
           // Toaster.toast(message);
           // checkEmptyScreen();
        });
    }



    public List<Connection> getFilteredConnectionList(CopyOnWriteArrayList<Connection>  con) {
        List<Connection> connections = new ArrayList<>();
        for (Connection connection : con) {
            if (!connection.getIsBlocked())
                connections.add(connection);
        }
        return connections;
    }

    void checkEmptyState() {
        if (connectionAdapter.getItemCount() > 0) {
            selectView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
        } else {
            selectView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.tv_share)
    void share() {
        CopyOnWriteArrayList<Integer> selectList = new CopyOnWriteArrayList<>();
        for (Connection user : userList) {
            if (user.isSelected())
                selectList.add(user.getUserId());
        }
        if (selectList.isEmpty()) {
            shareDialogOpen("Please Select to whom you want to share");
            return;
        }
        shareDialog();
    }

    //Show dialog
    private void shareDialogOpen(String title) {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_dialog_share);
        TextView tvTitle = ButterKnife.findById(dialog, R.id.tv_dialog);
        Button tvOk = ButterKnife.findById(dialog, R.id.btn_ok);
        tvTitle.setText(title);
        tvOk.setOnClickListener(v -> {
            dialog.dismiss();
        });
        dialog.show();
    }


    private void shareDialog() {
        FragmentManager fm = getFragmentManager();
        DialogShareProductFragment dialogFragment = new DialogShareProductFragment();
        dialogFragment.setCancelable(false);
        dialogFragment.setTargetFragment(this, SHARE_PRODUCT);
        dialogFragment.show(fm, dialogFragment.getClass().getSimpleName());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SHARE_PRODUCT && resultCode == Activity.RESULT_OK) {
            showProgress(true);
            int level2ndShare = data.getIntExtra(kWantToShare2ndLevel, 0);
            int level3rdShare = data.getIntExtra(kWantToShare3ndLevel, 0);
            shareBoard(level2ndShare, level3rdShare);
        }
    }

    public void shareBoard(int level2ndShare, int level3rdShare) {
        CopyOnWriteArrayList<Integer> selectList = new CopyOnWriteArrayList<>();
        for (Connection user : userList) {
            if (user.isSelected())
                selectList.add(user.getUserId());
        }
        ModelManager.modelManager().getshareBoard(boardId, level2ndShare, level3rdShare, selectList, (Constants.Status iStatus) -> {
            showProgress(false);
            shareDialogOpen("Successfully shared your friends!");
            dialog.dismiss();
            frag.getCount();
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            dialog.dismiss();
            Toaster.toast(message);
        });
    }


    private void addTextListener(EditText edtSearch) {
        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                s = s.toString().toLowerCase();
                List<Connection> filteredList = new ArrayList<>();
                for (int i = 0; i < userList.size(); i++) {
                    final String text = userList.get(i).getFullName().toLowerCase();
                    if (text.contains(s)) {
                        filteredList.add(userList.get(i));
                    }
                }

                recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                connectionAdapter = new ConnectionAdapter(filteredList);
                recyclerView.setAdapter(connectionAdapter);
                connectionAdapter.notifyDataSetChanged();
                checkEmptyState();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    @OnEditorAction(R.id.edt_search)
    protected boolean onEditSearch(int actionId) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            Utils.hideKeyboard(getContext());
            return true;
        }
        return false;
    }

    private void showProgress(boolean b) {
        if (b) {
            progressDialog.show();
        } else {
            if (progressDialog != null) progressDialog.dismiss();
        }
    }


    class ConnectionAdapter extends RecyclerView.Adapter<ConnectionAdapter.ConnectionViewHolder> {

        private List<Connection> userList;

        private List<Connection> selectedList;
        private ColorGenerator generator = ColorGenerator.MATERIAL;

        ConnectionAdapter(List<Connection> userList) {
            this.userList = userList;
            this.selectedList = new ArrayList<>();
        }

        @Override
        public ConnectionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(getContext()).inflate(R.layout.row_share_product_layout, parent, false);
            return new ConnectionViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ConnectionViewHolder holder, int position) {
            Connection user = userList.get(position);
            holder.bindContent(user);


            String name = user.getFullName();
            String nickName = user.getFirstName();
            String profileURL = user.getProfilePicURL();
            String addressTxt = "No location Available";
            if (user.getPrimaryAddress() != null)
                addressTxt = user.getPrimaryAddress().getCity() + ", " + user.getPrimaryAddress().getState() + ", " + user.getPrimaryAddress().getCountry();


            holder.tvName.setText(name);
            holder.tvTitle.setText(nickName);
            holder.tvAddress.setText(addressTxt);




            if (user.isSelected()) {
                holder.ibCheckBox.setImageDrawable(getResources().getDrawable(R.drawable.ic_invite_check));
            } else {
                holder.ibCheckBox.setImageDrawable(getResources().getDrawable(R.drawable.ic_circle_thin));
            }

            if (profileURL.isEmpty()) {
                holder.ivCircularUserImage.setVisibility(View.INVISIBLE);
                holder.ivUserImage.setVisibility(View.VISIBLE);
            } else {
                holder.ivCircularUserImage.setVisibility(View.VISIBLE);
                holder.ivUserImage.setVisibility(View.INVISIBLE);
            }

            TextDrawable drawable = TextDrawable.builder().buildRound(String.valueOf(name.toUpperCase().charAt(0)), generator.getRandomColor());

            if (!profileURL.isEmpty()) {
                Picasso.with(getContext())
                        .load(profileURL)
                        .placeholder(drawable)
                        .into(holder.ivCircularUserImage);
            } else {
                holder.ivUserImage.setImageDrawable(drawable);
            }
        }

        void addItems(List<Connection> connections){
            userList.clear();
            userList.addAll(connections);
            notifyDataSetChanged();
        }


        @Override
        public int getItemCount() {
            return userList.size();
        }

        List<Connection> getSelectedList() {
            return selectedList;
        }

        public void clearSelection() {
            for (int i = 0; i < getItemCount(); i++) {
                userList.get(i).setSelected(false);
            }
            notifyDataSetChanged();
        }

        class ConnectionViewHolder extends RecyclerView.ViewHolder {
            private Connection connection;
            @BindView(R.id.tv_name)
            TextView tvName;
            @BindView(R.id.tv_title)
            TextView tvTitle;
            @BindView(R.id.tv_address)
            TextView tvAddress;
            @BindView(R.id.ib_checkbox)
            ImageView ibCheckBox;
            @BindView(R.id.iv_user_image)
            ImageView ivUserImage;
            @BindView(R.id.iv_user_image_circular)
            ImageView ivCircularUserImage;

            ConnectionViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            void bindContent(Connection connection) {
                this.connection = connection;
            }

            @OnClick(R.id.item_view)
            void onItemView() {
                if (connection.isSelected()) {
                    connection.setSelected(false);
                    ibCheckBox.setImageDrawable(getResources().getDrawable(R.drawable.ic_circle_thin));
                    selectedList.remove(connection);
                } else {
                    connection.setSelected(true);
                    ibCheckBox.setImageDrawable(getResources().getDrawable(R.drawable.ic_invite_check));
                    selectedList.add(connection);
                }
            }

        }
    }


  }
