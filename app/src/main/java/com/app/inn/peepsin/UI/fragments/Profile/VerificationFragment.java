package com.app.inn.peepsin.UI.fragments.Profile;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.activities.HomeActivity;
import com.app.inn.peepsin.UI.fragments.ResetPasswordFragment;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.helpers.Validations;
import com.app.inn.peepsin.UI.interfaces.Switcher;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.inn.peepsin.Constants.Constants.kContactId;
import static com.app.inn.peepsin.Constants.Constants.kIsValidate;
import static com.app.inn.peepsin.Constants.Constants.kOTP;
import static com.app.inn.peepsin.Constants.Constants.kPassword;
import static com.app.inn.peepsin.Constants.Constants.kType;

/**
 * Created by akamahesh on 2/5/17.
 */

public class VerificationFragment extends Fragment {


    @BindView(R.id.tv_title) TextView tvTitle;
    @BindView(R.id.edt_credential) EditText edtCredential;
    @BindView(R.id.btn_submit) Button btnSubmit;

    private ProgressDialog progressDialog;

    int TYPE = 1;
    private static final int PHONE = 1;
    private static final int EMAIL = 2;
    private static final int PASSWORD = 3;
    static Switcher profileSwitcherListener;
    //AppBarLayout appbarLayout;
    String addresss="";


    /**
     * @param type 1 for phone , 2 for email, and 3 for password
     * @param switcherListener
     * @return
     */
    public static Fragment newInstance(int type, Switcher switcherListener){ //1 for phone 2 for email 3 for password
        Fragment fragment=  new VerificationFragment();
        profileSwitcherListener = switcherListener ;
        Bundle bundle = new Bundle();
        bundle.putInt(kType,type);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if(bundle!=null)
            TYPE = bundle.getInt(kType);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_verification, container, false);
        ButterKnife.bind(this,view);
       /* appbarLayout=((HomeActivity) getActivity()).getTabLayout();
        Utils.isprofileClicked=1;*/
        progressDialog = Utils.generateProgressDialog(getContext(),false);
        if(TYPE == PHONE){
            tvTitle.setText(getString(R.string.title_phone_verification));
            edtCredential.setInputType(InputType.TYPE_CLASS_PHONE|InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
            edtCredential.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_phone),null,null,null);
            edtCredential.setHint("Phone Number");
        }else if(TYPE == EMAIL){
            tvTitle.setText(getString(R.string.title_email_verificaition));
            edtCredential.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS|InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
            edtCredential.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_email),null,null,null);
            edtCredential.setHint("Email Address");
        }else if(TYPE == PASSWORD){
            tvTitle.setText(getString(R.string.title_reset_password));
            edtCredential.setInputType(InputType.TYPE_CLASS_TEXT|InputType.TYPE_TEXT_VARIATION_PASSWORD);
            edtCredential.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_key),null,null,null);
            edtCredential.setHint("Current password");
            btnSubmit.setText(getString(R.string.reset_password_text));
        }

        return view;
    }

    @OnClick(R.id.btn_back)
    void onBack(){
        Utils.hideKeyboard(getContext());
        getFragmentManager().popBackStack();
      /*  appbarLayout.setVisibility(View.VISIBLE);
        Utils.isprofileClicked=0;*/
    }
    @Override
    public void onResume() {
        super.onResume();
        //appbarLayout.setVisibility(View.GONE);

    }

    @OnClick(R.id.btn_submit)
    void submit(){
        String credential =edtCredential.getText().toString().trim();
        if(credential.isEmpty()){
            edtCredential.setError("Field must not be empty");
            return;
        }

        if(TYPE==PHONE){
            if(Validations.isValidPhone(credential)&&profileSwitcherListener!=null){
                profileSwitcherListener.switchFragment(ProfileOTPFragment.newInstance(PHONE,-1,profileSwitcherListener,String.valueOf(Utils.generateOTP()) ,"testing",addresss,null,new HashMap<>()),false,true);
            }else {
                edtCredential.setError("Please provide a valid phone number!");
            }

        }else if(TYPE==EMAIL){
            if(Validations.isValidEmail(credential)&&profileSwitcherListener!=null){
                profileSwitcherListener.switchFragment(ProfileOTPFragment.newInstance(EMAIL,-1,profileSwitcherListener,String.valueOf(Utils.generateOTP()),"testing",addresss,null,new HashMap<>()),false,true);
            }else {
                edtCredential.setError("Please provide a valid email address!");
            }
        }else if(TYPE==PASSWORD){
            if(Validations.isValidPassword(credential)&&profileSwitcherListener!=null){
                String contactId = ModelManager.modelManager().getCurrentUser().getContactId();
                validateCurrentPassword(contactId,credential);
                }else {
                edtCredential.setError("Please provide a valid email address!");
            }
        }

    }


    private void validateCurrentPassword(String contactId,String password) {
        HashMap<String,Object> parameters=new HashMap<>();
        parameters.put(kContactId,contactId);
        parameters.put(kPassword,password);
        showProgress(true);
        ModelManager.modelManager().validatePassword(parameters,(Constants.Status iStatus, GenricResponse<HashMap<String,Object>> genericResponse) -> {
            showProgress(false);
            HashMap<String,Object> dataMap = genericResponse.getObject();
            Integer isValidate= (Integer) dataMap.get(kIsValidate);
            String  OTP= (String) dataMap.get(kOTP);
            if(isValidate==1)
                profileSwitcherListener.switchFragment(ResetPasswordFragment.newInstance(contactId,password,OTP,profileSwitcherListener),true,true);
            else if(isValidate ==0){
                edtCredential.setText("");
                Utils.showAlertDialog(getContext(),"Process Failed!","Please enter your valid current password.");
            }
        }, (Constants.Status iStatus, String message) -> {
            Toaster.toast(message);
            showProgress(false);
        });
    }

    private void showProgress(boolean b) {
        if(b){
            progressDialog.show();
        }else {
            if(progressDialog!=null) progressDialog.cancel();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        profileSwitcherListener = null;
    }

}
