package com.app.inn.peepsin.UI.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.inn.peepsin.Models.OrderHistory;
import com.app.inn.peepsin.Models.ProductList;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.squareup.picasso.Picasso;

/**
 * Created by Harsh on 7/19/2017.
 */

public class OrderSummaryAdapter extends BaseExpandableListAdapter {

    Context context;
    private OrderHistory orderHistory;


    public OrderSummaryAdapter(Context context, OrderHistory orderHistory) {
        this.context = context;
        this.orderHistory = orderHistory;
    }

    //Parent View
    @Override
    public Object getGroup(int groupPosition) {
        return orderHistory;
    }

    @Override
    public int getGroupCount() {
        return 1;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        OrderHistory item = (OrderHistory) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.order_summary_adapter, null);
        }
        TextView tvOrderDate = (TextView) convertView.findViewById(R.id.tv_order_date);
        TextView tvOrderStatus = (TextView) convertView.findViewById(R.id.tv_order_status);
        TextView tvOrderItem = (TextView) convertView.findViewById(R.id.tv_order_item);
        TextView tvOrderIdNumber = (TextView) convertView.findViewById(R.id.tv_order_id_number);
        TextView tvOrderAddressUserName = (TextView) convertView.findViewById(R.id.tv_order_address_user_name);
        TextView tvOrderAddressUser = (TextView) convertView.findViewById(R.id.tv_order_address_user);
        TextView tvOrderAddressNumber = (TextView) convertView.findViewById(R.id.tv_order_address_number);
        TextView tvPaymentMethod = (TextView) convertView.findViewById(R.id.tv_payment_mehtod);
        TextView tvOrderTotalAmmount = (TextView) convertView.findViewById(R.id.tv_order_total_ammount);
        TextView tvPaymentBeforePromoAmmount = (TextView) convertView.findViewById(R.id.tv_payment_before_promo_ammount);
        TextView tvPromoCodeAmmount = (TextView) convertView.findViewById(R.id.tv_promo_code_ammount);
        TextView tvDiscountTagLine = (TextView) convertView.findViewById(R.id.tv_discount_tag_line);
        Button btnProduct = (Button) convertView.findViewById(R.id.btn_show_product);
        ImageView ivStatusIcon=(ImageView) convertView.findViewById(R.id.img_status_icon) ;

        // Populating Data
        String orderdate = item.getOrderDate();
        String orderidnumber = String.valueOf(item.getOrderId());
        tvOrderDate.setText(orderdate);
        tvOrderIdNumber.setText(orderidnumber);

        tvOrderStatus.setText(Utils.getStatus(item.getOrderStatus()));
        if (item.getOrderStatus() == 5) {
            tvOrderStatus.setTextColor(Color.parseColor("#FFFF0000"));
        } else {
            tvOrderStatus.setTextColor(Color.parseColor("#30C430"));
        }
        switch(item.getOrderStatus()) {
            case 1 :
                ivStatusIcon.setImageResource(R.drawable.ic_packed_img);
                break; // optional
            case 2 :
                ivStatusIcon.setImageResource(R.drawable.ic_processing_img);
                break; // optional
            case 3 :
                ivStatusIcon.setImageResource(R.drawable.ic_shipp_img);
                break; // optional
            case 4 :
                ivStatusIcon.setImageResource(R.drawable.ic_delivered_img);
                break; // optional
            case 5 :
                ivStatusIcon.setImageResource(R.drawable.ic_cancellation_img);
                break; // optional
            case 6 :
                ivStatusIcon.setImageResource(R.drawable.ic_not_recognize_img);
                break; // optional
            case 7 :
                ivStatusIcon.setImageResource(R.drawable.ic_placed_img);
                break; // optional
            default :
                ivStatusIcon.setImageResource(R.drawable.ic_not_recognize_img);
        }

        String ordertotalammount = "";
        if (item.getFinalAmountAfterPromoCode() != null)
            ordertotalammount = item.getFinalAmountAfterPromoCode();
        tvOrderTotalAmmount.setText(ordertotalammount);

        String paymentbeforepromocode = "";
        if (item.getTotalCartPriceBeforePromoCode() != null)
            paymentbeforepromocode = item.getTotalCartPriceBeforePromoCode();
        tvPaymentBeforePromoAmmount.setText(paymentbeforepromocode);

        String promocodeammount = "---";
        if (!item.getDiscountedPrice().equals(""))
            promocodeammount = item.getDiscountedPrice();
        tvPromoCodeAmmount.setText(promocodeammount);
        String discounrtag = item.getPromoCodeTagLine();
        tvDiscountTagLine.setText(discounrtag);

        String orderAddressUserName = "";
        if (item.getDeliveryAddress() != null)
            orderAddressUserName = item.getDeliveryAddress().getFullName();
        tvOrderAddressUserName.setText(orderAddressUserName);

        String totalcartitem = "";
        if (item.getCartItemCount() != null)
            totalcartitem = String.valueOf(item.getCartItemCount()) + " " + "item";
        tvOrderItem.setText(totalcartitem);

        String addressTxt = "No Location Available";
        if (item.getDeliveryAddress() != null)
            addressTxt = item.getDeliveryAddress().getStreetAddress() + ", "
                    + item.getDeliveryAddress().getCity() + ", "
                    + item.getDeliveryAddress().getState() + ", "
                    + item.getDeliveryAddress().getCountry() + ","
                    + item.getDeliveryAddress().getZipCode();
        tvOrderAddressUser.setText(addressTxt);

        String orderaddressnumber = "";
        if (item.getDeliveryAddress() != null)
            orderaddressnumber = item.getDeliveryAddress().getPhone();
        tvOrderAddressNumber.setText(orderaddressnumber);

        if (item.getPaymentType() != null) {
            switch (item.getPaymentType()) {
                case 1:
                    tvPaymentMethod.setText(R.string.text_paytm);
                    break;
                case 2:
                    tvPaymentMethod.setText(R.string.text_pay_u_money);
                    break;
                case 3:
                    tvPaymentMethod.setText(R.string.text_cash_on_delivery);
                    break;
                default:
                    tvPaymentMethod.setText(R.string.pending_payment);
            }
        }
        if (isExpanded) {
            btnProduct.setText("Hide Product");
        } else {
            btnProduct.setText("Show Product");
        }

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    // Child View Group
    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this.orderHistory.getShopRecords().get(groupPosition).getProductRecords().get(childPosition);

    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.orderHistory.getShopRecords().get(groupPosition).getProductRecords().size();
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View childView, ViewGroup parent) {

        final ProductList product = (ProductList) getChild(groupPosition, childPosition);
        if (childView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            childView = layoutInflater.inflate(R.layout.row_new_child_order_history, null);
        }
        TextView tvProductName = (TextView) childView.findViewById(R.id.tv_product_name);
        TextView tvProductRupees = (TextView) childView.findViewById(R.id.tv_product_rupees);
        TextView tvUserTitle = (TextView) childView.findViewById(R.id.tv_user_title);
        ImageView ivProductImage = (ImageView) childView.findViewById(R.id.iv_product_image);
        TextView tvProductQuantity=(TextView)childView.findViewById(R.id.tv_product_quantity);
        if (product != null) {
            String producturl = product.getCoverImageThumbnailURL();
            String price = context.getString(R.string.Rs) + product.getPrice();
            String productname = product.getProductName();
            //String shopName = "From:" + orderHistory.getShopRecords().get(0).getshopName();

            Picasso.with(context)
                    .load(producturl)
                    .placeholder(R.drawable.img_product_placeholder)
                    .into(ivProductImage);
            tvProductRupees.setText(price);
            tvProductName.setText(productname);
            String quantity="Quantity: "+product.getProductQuantity();
            tvProductQuantity.setText(quantity);
            //tvUserTitle.setText(shopName);
        }

        return childView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}
