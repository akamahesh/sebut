package com.app.inn.peepsin.Models;

import com.facebook.Profile;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.twitter.sdk.android.core.models.User;

import org.json.JSONObject;

import java.io.Serializable;

import static com.app.inn.peepsin.Managers.SocialManager.SocialConstants.kFacebookEmail;
import static com.app.inn.peepsin.Managers.SocialManager.SocialConstants.kFacebookFirstName;
import static com.app.inn.peepsin.Managers.SocialManager.SocialConstants.kFacebookGender;
import static com.app.inn.peepsin.Managers.SocialManager.SocialConstants.kFacebookLastName;
import static com.app.inn.peepsin.Managers.SocialManager.SocialConstants.kID;


/**
 * Created by mahesh on 19/4/17.
 */

public class SocialUser extends BaseModel implements Serializable {

    private String nickName;
    private String id;
    private String firstName;
    private String lastName;
    private String email;
    private String profileURL;
    private String address;
    private String gender;
    private String dob;

    //twitter User
    public SocialUser(User user,String email){
        this.nickName = (user.screenName!=null)?user.screenName:kEmptyString;
        String[] parts = user.name.split(" ");
        this.firstName = (parts[0]!=null)?parts[0]:kEmptyString;
        this.lastName = (parts[1]!=null)?parts[1]:kEmptyString;
        this.email = email;
        this.id= String.valueOf((user.id));
        this.dob   = kEmptyString;
        this.profileURL=(user.profileImageUrl!=null)?user.profileImageUrl:kEmptyString;
        this.address = (user.location!=null)?user.location:kEmptyString;

    }
    /**
     * @see SocialUser constructor to be used for facebook login
     * @param account details provided by google
     */
    public SocialUser(JSONObject account) {
        this.nickName       = kEmptyString;
        this.id             = getValue(account,kID,String.class);
        this.email          = getValue(account,kFacebookEmail,String.class);
        this.firstName      = getValue(account,kFacebookFirstName,String.class);
        this.lastName       = getValue(account,kFacebookLastName,String.class);
        this.gender         = getValue(account,kFacebookGender,String.class);
        try {
            this.profileURL     = Profile.getCurrentProfile().getProfilePictureUri(400,400).toString();
        }catch (Exception e){
            e.printStackTrace();
            profileURL = kEmptyString;
        }
        this.dob            = kEmptyString;
        this.address        = kEmptyString;
    }

    /**
     * @see SocialUser constructor to be used for google login
     * @param account details provided by google
     */
    public SocialUser(GoogleSignInAccount account) {
        super();
        this.id             = account.getId();
        this.nickName       = account.getDisplayName();
        this.firstName      = account.getGivenName();
        this.lastName       = account.getFamilyName();
        this.email          = account.getEmail();
        this.address        = kEmptyString;
        this.dob            = kEmptyString;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getContact(){
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfileURL() {
        return profileURL;
    }

    public void setProfileURL(String profileURL) {
        this.profileURL = profileURL;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
