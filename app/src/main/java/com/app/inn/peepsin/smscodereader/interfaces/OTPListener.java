package com.app.inn.peepsin.smscodereader.interfaces;

/**
 * Created by akaMahesh on 13-12-2015
 * contact : mckay1718@gmail.com
 */
public interface OTPListener {

    public void otpReceived(String messageText);
}
