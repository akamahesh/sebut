package com.app.inn.peepsin.UI.fragments.SellStuffModule;

import static com.app.inn.peepsin.Constants.Constants.CATEGORY_RESULT;
import static com.app.inn.peepsin.Constants.Constants.KSCREENHEIGHT;
import static com.app.inn.peepsin.Constants.Constants.KSCREENWIDTH;
import static com.app.inn.peepsin.Constants.Constants.SUB_CATEGORY_RESULT;
import static com.app.inn.peepsin.Constants.Constants.kAddressId;
import static com.app.inn.peepsin.Constants.Constants.kDiscountTagLine;
import static com.app.inn.peepsin.Constants.Constants.kName;
import static com.app.inn.peepsin.Constants.Constants.kNetPrice;
import static com.app.inn.peepsin.Constants.Constants.kProduct;
import static com.app.inn.peepsin.Constants.Constants.kProductBasicPrice;
import static com.app.inn.peepsin.Constants.Constants.kProductCategory;
import static com.app.inn.peepsin.Constants.Constants.kProductCategoryId;
import static com.app.inn.peepsin.Constants.Constants.kProductDesc;
import static com.app.inn.peepsin.Constants.Constants.kProductId;
import static com.app.inn.peepsin.Constants.Constants.kProductPrice;
import static com.app.inn.peepsin.Constants.Constants.kProductSellPrice;
import static com.app.inn.peepsin.Constants.Constants.kProductStatus;
import static com.app.inn.peepsin.Constants.Constants.kProductSubCategory;
import static com.app.inn.peepsin.Constants.Constants.kProductSubCategoryId;
import static com.app.inn.peepsin.Constants.Constants.kProductType;
import static com.app.inn.peepsin.Constants.Constants.kQuantity;
import static com.app.inn.peepsin.Constants.Constants.kSKUNumber;
import static com.app.inn.peepsin.Constants.Constants.kShopId;
import static com.app.inn.peepsin.Constants.Constants.kSpecification;
import static com.app.inn.peepsin.Constants.Constants.kTotalTax;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.Address;
import com.app.inn.peepsin.Models.CurrentUser;
import com.app.inn.peepsin.Models.ProductType;
import com.app.inn.peepsin.Models.SellingProduct;
import com.app.inn.peepsin.Models.ShopCatSubCategory;
import com.app.inn.peepsin.Models.TAX;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.Views.SFDLTextView;
import com.app.inn.peepsin.UI.Views.SFDREditText;
import com.app.inn.peepsin.UI.adapters.ProductSpinnerAdapter;
import com.app.inn.peepsin.UI.dialogFragments.ProductCategoryDialogFragment;
import com.app.inn.peepsin.UI.dialogFragments.ProductSubCategoryDialogFragment;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Harsh on 5/12/2017.
 */

public class SellingProductEditFragment extends Fragment {

  @BindView(R.id.view_pager_images)
  ImageView ivProduct;
  @BindView(R.id.tv_title)
  TextView tvTitle;

  @BindView(R.id.tv_product_name)
  EditText tvName;
  @BindView(R.id.tv_price)
  EditText tvPrice;
  @BindView(R.id.tv_sale_price)
  EditText tvSalePrice;
  @BindView(R.id.tv_quantity)
  EditText tvQuantity;
  @BindView(R.id.tv_discount)
  EditText tvDiscount;
  @BindView(R.id.tv_product_desc)
  EditText tvDescription;
  /* @BindView(R.id.tv_address)
   TextView tvAddress;*/
  @BindView(R.id.discount_tag_view)
  View discountTagView;
  @BindView(R.id.tv_product_category_details)
  TextView tvCategory;
  @BindView(R.id.tv_main_category_name)
  TextView tvCategoryName;
  @BindView(R.id.tv_sub_category_name)
  TextView tvSubCategoryName;
  @BindView(R.id.edt_product_type)
  EditText edtProductType;
  @BindView(R.id.edt_specification)
  EditText edtSpecification;
  @BindView(R.id.spinner_product_type)
  Spinner spinnerProduct;
  @BindView(R.id.edt_sku_no)
  EditText edtSkuNo;

  @BindView(R.id.view_tax_linear)
  LinearLayout viewTaxLayout;
  @BindView(R.id.tv_mrp)
  TextView tvMRP;
  @BindView(R.id.tv_offer_price)
  TextView tvOfferPrice;
  @BindView(R.id.tv_tax_rate)
  TextView tvTaxRate;
  @BindView(R.id.tv_tax_type)
  TextView tvTaxType;
  @BindView(R.id.tv_total_tax)
  TextView tvTotalTax;
  @BindView(R.id.tv_base_price)
  TextView tvBasePrice;
  @BindView(R.id.tv_net_price)
  TextView tvNetPrice;

  @BindView(R.id.radio_group)
  RadioGroup radioGroup;
  @BindView(R.id.radio_button_draft)
  RadioButton radioButtonDraft;
  @BindView(R.id.radio_button_publish)
  RadioButton radioButtonPublish;

  private ProgressDialog progressDialog;
  private CurrentUser currentUser;
  private CopyOnWriteArrayList<ShopCatSubCategory> categoryList;
  private String categoryName;
  Integer addressId = 0, categoryId = 0, subCategoryId = 0;
  private EditProductInteractionListener mListener;
  private SellingProduct product;
  private ProductSpinnerAdapter productAdapter;
  private String TAG = getTag();
  int productTypeId = 1;
  private CopyOnWriteArrayList<TAX> mTaxList;
  private List<SFDREditText> mEditTexts = new ArrayList<>();

  public static SellingProductEditFragment getInstance(SellingProduct product) {
    SellingProductEditFragment fragment = new SellingProductEditFragment();
    Bundle bundle = new Bundle();
    bundle.putSerializable(kProduct, product);
    fragment.setArguments(bundle);
    return fragment;
  }

  public interface EditProductInteractionListener {
    void onCamera();
    void submitAd(HashMap<String, Object> productMap);
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    if (context instanceof EditProductInteractionListener) {
      mListener = (EditProductInteractionListener) context;
    } else {
      throw new RuntimeException(
          context.toString() + " must implement CameraPreviewInteractionListener");
    }
  }

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    progressDialog = Utils.generateProgressDialog(getContext(), true);
    currentUser = ModelManager.modelManager().getCurrentUser();
    mTaxList = currentUser.getTaxList();
    Bundle bundle = getArguments();
    if (bundle != null) {
      product = (SellingProduct) bundle.getSerializable(kProduct);
    }
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_edit_selling_product_details, container, false);
    ButterKnife.bind(this, view);
    tvTitle.setText(getString(R.string.product_edit_details));
    tvCategory.setVisibility(View.GONE);
    edtProductType.setKeyListener(null);
    return view;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    try {
      loadData(product);
    } catch (Exception e) {
      e.printStackTrace();
    }
    loadSpinnerData();
    setTaxView(product.getTaxList());
    tvPrice.addTextChangedListener(priceWatcher);
    tvSalePrice.addTextChangedListener(priceWatcher);
    for (EditText editText : mEditTexts) {
      editText.addTextChangedListener(taxWatcher);
    }
    taxWatcher.afterTextChanged(tvSalePrice.getText());
  }


  public void setTaxView(List<TAX> taxes) {
    Integer textsize = getResources().getColor(R.color.text_color_regular);
    int ssd = Utils.dpToPx(getContext(), textsize);
    for (TAX tataTax : taxes) {
      LinearLayout layout = new LinearLayout(getContext());
      layout.setOrientation(LinearLayout.HORIZONTAL);
      {
        SFDLTextView view = new SFDLTextView(getContext());
        view.setText(tataTax.getTaxName());
        view.setGravity(Gravity.CENTER);
        view.setTextColor(getResources().getColor(R.color.text_color_regular));
        view.setTextSize(ssd);
        view.setPadding(10, 10, 10, 10);
        layout.addView(view);
      }
      {
        SFDREditText taxView = new SFDREditText(getContext());
        mEditTexts.add(taxView);
        taxView.setId(tataTax.getTaxId());
        taxView.setText(tataTax.getTaxValue());
        taxView.setGravity(Gravity.CENTER);
        taxView.setInputType(InputType.TYPE_CLASS_NUMBER);
        InputFilter[] fArray = new InputFilter[1];
        fArray[0] = new InputFilter.LengthFilter(2);
        taxView.setFilters(fArray);
        taxView.setBackground(getResources().getDrawable(R.drawable.canvas_gray_rounded_corner));
        taxView.setGravity(Gravity.CENTER);
        taxView.setTextColor(getResources().getColor(R.color.text_color_regular));
        taxView.setTextSize(ssd);
        taxView.setPadding(30, 10, 10, 30);
        layout.addView(taxView);
      }
      {
        SFDLTextView view = new SFDLTextView(getContext());
        view.setText(tataTax.getTaxType());
        view.setGravity(Gravity.CENTER);
        view.setTextColor(getResources().getColor(R.color.text_color_regular));
        view.setTextSize(ssd);
        view.setPadding(10, 10, 10, 10);
        layout.addView(view);
      }
      viewTaxLayout.addView(layout);
    }
    StringBuilder taxType = new StringBuilder();
    if(taxes.size()>1)
      taxType.append("(").append(taxes.get(0).getTaxName()).append(",")
              .append(taxes.get(1).getTaxName()).append(")");
    else
      taxType.append("(").append(taxes.get(0).getTaxName()).append(")");
    tvTaxType.setText(taxType);
  }

  TextWatcher priceWatcher = new TextWatcher() {
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable start) {
      if (!tvSalePrice.getText().toString().isEmpty() && !tvPrice.getText().toString().isEmpty()) {
        int costPrice = Integer.parseInt(tvPrice.getText().toString());
        int sellPrice = Integer.parseInt(tvSalePrice.getText().toString());
        tvMRP.setText(String.valueOf(costPrice));
        tvOfferPrice.setText(String.valueOf(sellPrice));
        taxWatcher.afterTextChanged(tvSalePrice.getText());
        if (costPrice > sellPrice) {
          discountTagView.setVisibility(View.VISIBLE);
          int discountPrice = costPrice - sellPrice;
          float discount = (float) discountPrice / costPrice;
          String discountTag = (int) (discount * 100) + "% off";
          tvDiscount.setText(discountTag);
        } else {
          discountTagView.setVisibility(View.GONE);
          tvDiscount.setText("");
        }
      } else{
        discountTagView.setVisibility(View.GONE);
        tvDiscount.setText("");
      }
    }
  };

  TextWatcher taxWatcher = new TextWatcher() {
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable start) {
      boolean state = true;
      for (EditText editText : mEditTexts) {
        if(editText.getText().toString().isEmpty()){
          state = false;
          break;
        }
      }
      if (state && !tvSalePrice.getText().toString().isEmpty()) {
        int sellPrice = Integer.parseInt(tvSalePrice.getText().toString());
        int taxRate = 0;
        for (EditText editText : mEditTexts) {
          taxRate += Integer.parseInt(editText.getText().toString());
        }
        float netValue = (sellPrice*100)/(100+taxRate);
        float taxValue = sellPrice - netValue;
        tvTaxRate.setText(String.valueOf(taxRate));
        tvTotalTax.setText(String.valueOf(taxValue));
        tvBasePrice.setText(String.valueOf(netValue));
        tvNetPrice.setText(String.valueOf(sellPrice));
      }else {
        tvTaxRate.setText("");
        tvTotalTax.setText("");
        tvBasePrice.setText("");
        tvNetPrice.setText("");
      }
    }
  };


  private void loadSpinnerData() {
    // Product Type Spinner Data
    ModelManager.modelManager().getProductTypeList((Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<ProductType>> genricResponse) -> {
      CopyOnWriteArrayList<ProductType> productList = genricResponse.getObject();
      if(getContext() != null) {
        productAdapter = new ProductSpinnerAdapter(getContext(),
                R.layout.business_spinner_item, productList);
        spinnerProduct.setAdapter(productAdapter);
      }
      for(int i=0;i<productList.size();i++) {
        if (productList.get(i).getId().equals(product.getProductType())) {
          int spinnerPosition = productAdapter.getPosition(productList.get(i));
          spinnerProduct.setSelection(spinnerPosition);
        }
      }
      productTypeId = product.getProductType();
    }, (Constants.Status iStatus, String message) -> Log.e(TAG, message));

    spinnerProduct.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
        productTypeId = productAdapter.getItem(position).getId();
        edtProductType.setText(productAdapter.getItem(position).getName());
      }

      @Override
      public void onNothingSelected(AdapterView<?> adapter) {
      }
    });
  }

  private void loadData(SellingProduct product) throws Exception {
    String name = product.getName();
    String description = product.getDescription();
    String price = product.getPrice();
    price = price.replace("/-", "");
    String salePrice = product.getSellingPrice();
    salePrice = salePrice.replace("/-", "");
    String quantity = String.valueOf(product.getQuantity());
    String tagLine = product.getDiscountTagLine();
    String productImageUrL = product.getImageUrl();
    Address address = product.getAddress();
    String skuno = product.getSKUNumber();
    String specification = product.getSpecification();
    String addressTXT = "No address available";

    int productStatus = product.getProductStatus();
    if(productStatus==6)
      radioButtonDraft.setChecked(true);
    else {
      radioButtonPublish.setChecked(true);
    }

    int totalPrice = (int) Float.parseFloat(price);
    int offerPrice = (int) Float.parseFloat(salePrice);
    if (totalPrice <= offerPrice) {
      discountTagView.setVisibility(View.GONE);
    } else {
      discountTagView.setVisibility(View.VISIBLE);
    }

    try {
      addressId = address.getAddressId();
      addressTXT = address.getCity()+", "+address.getState()+", "+address.getCountry();
    }catch (Exception e){
      e.printStackTrace();
    }
    categoryId = product.getCategoryId();
    subCategoryId = product.getSubcategoryId();

    tvName.setText(name);
    tvDescription.setText(description);
    tvPrice.setText(String.valueOf(totalPrice));
    tvSalePrice.setText(String.valueOf(offerPrice));
    tvQuantity.setText(quantity);
    tvDiscount.setText(tagLine);
    tvCategoryName.setText(product.getCategory());
    tvSubCategoryName.setVisibility(View.VISIBLE);
    tvSubCategoryName.setText(product.getSubcategory());
    edtSkuNo.setText(skuno);
    edtSpecification.setText(specification);
    tvMRP.setText(product.getPrice().replace(".00/-", ""));
    tvOfferPrice.setText(product.getSellingPrice().replace(".00/-", ""));
    tvTaxRate.setText(product.getTotalTax().replace(".00/-", ""));
    tvNetPrice.setText(product.getNetPrice().replace(".00/-", ""));

    if (!productImageUrL.isEmpty()) {
      Picasso.with(getContext())
          .load(productImageUrL)
          .placeholder(R.drawable.img_product_placeholder)
          .into(ivProduct);
    }

    /* tvAddress.setTextColor(getResources().getColor(R.color.text_color_regular));
       tvAddress.setText(addressTXT);*/
  }

  @OnClick(R.id.view_pager_images)
  void onCameraEdit() {
    mListener.onCamera();
  }

     /* @OnClick(R.id.view_change_address)
    void onAddAddress() {
        DialogFragment fragment = AddressDialogFragment.newInstance();
        Bundle bdl = new Bundle();
        bdl.putInt(KSCREENWIDTH, screenWidth);
        bdl.putInt(KSCREENHEIGHT, screenHeight);
        fragment.setArguments(bdl);
        fragment.setTargetFragment(this, ADDRESS_RESULT);
        fragment.show(getChildFragmentManager(), "Dialog Fragment");
    }*/

  @OnClick(R.id.view_product_category)
  void selectCategory() {
    showProgress(true);
    ModelManager.modelManager().getCatSubCategoryList(
        (Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<ShopCatSubCategory>> genricResponse) -> {
          showProgress(false);
          categoryList = genricResponse.getObject();
          ProductCategoryDialogFragment fragment = new ProductCategoryDialogFragment();
          Bundle bdl = new Bundle();
          bdl.putInt(KSCREENWIDTH, 0);
          bdl.putInt(KSCREENHEIGHT, 0);
          bdl.putSerializable(kProductCategory, categoryList);
          fragment.setArguments(bdl);
          fragment.setTargetFragment(this, CATEGORY_RESULT);
          fragment.show(getChildFragmentManager(), "Dialog Fragment");
        }, (Constants.Status iStatus, String message) -> {
          showProgress(false);
          Toaster.toast(message);
        });
  }

  void selectSubCategory(int categoryId) {
    for (int i = 0; i < categoryList.size(); i++) {
      if (categoryList.get(i).getId() == categoryId) {
        ProductSubCategoryDialogFragment fragment = new ProductSubCategoryDialogFragment();
        Bundle bdl = new Bundle();
        bdl.putInt(KSCREENWIDTH, 0);
        bdl.putInt(KSCREENHEIGHT, 0);
        bdl.putSerializable(kProductSubCategory, categoryList.get(i).getSubCategories());
        fragment.setArguments(bdl);
        fragment.setTargetFragment(this, SUB_CATEGORY_RESULT);
        fragment.show(getChildFragmentManager(), "Dialog Fragment");
      }
    }
  }

  @OnClick(R.id.btn_back)
  void onBack() {
    getActivity().finish();
  }

  @OnClick(R.id.invite)
  void onEditProduct() {
    int productstatus;
    if(radioButtonDraft.isChecked())
      productstatus = 6;
    else productstatus = 1;

    try {
      int totalprice = Integer.parseInt(tvPrice.getText().toString());
      int offerPrice = Integer.parseInt(tvSalePrice.getText().toString());
      if (categoryId == 0 || subCategoryId == 0) {
        Toaster.kalaToast("Category and Subcategory are must");
      } else if (addressId == 0) {
        Toaster.kalaToast("Address is must");
      } else if (tvName.getText().toString().isEmpty()) {
        tvName.setError("Product Name is must");
      } else if (tvDescription.getText().toString().isEmpty()) {
        tvDescription.setError("Description is must");
      } else if (tvPrice.getText().toString().isEmpty()) {
        tvPrice.setError("Price is must");
      } else if (tvSalePrice.getText().toString().isEmpty()) {
        tvSalePrice.setError("Sale Price is must");
      } else if (tvQuantity.getText().toString().isEmpty()) {
        tvQuantity.setError("Quantity is must");
      } else if (edtSkuNo.getText().toString().isEmpty()) {
        edtSkuNo.setError("SKU Number is Must");
      } else if (edtSpecification.getText().toString().isEmpty()) {
        edtSpecification.setError("Specification is Must");
      } else if (tvTaxRate.getText().toString().isEmpty()) {
        Toaster.kalaToast("Please input all tax value");
      } else if (!tvTaxRate.getText().toString().isEmpty() && Integer.parseInt(tvTaxRate.getText().toString())>28) {
        Toaster.kalaToast("Tax value cannot exceeds 28%");
      } else if (totalprice < offerPrice) {
        Toaster.kalaToast("Cost Price cannot be less than sell price");
      } else {

        try {
          for (EditText editText : mEditTexts) {
            Integer id = editText.getId();
            for (TAX tax : mTaxList) {
              if (tax.getTaxId().equals(id)) {
                tax.setTaxValue(editText.getText().toString().trim());
                break;
              }
            }
          }
        } catch (Exception e) {
          e.printStackTrace();
        }
        ModelManager.modelManager().getCurrentUser().setTaxList(mTaxList);

        HashMap<String, Object> productMap = new HashMap<>();
        productMap.put(kShopId, ModelManager.modelManager().getCurrentUser().getShopId());
        productMap.put(kProductId, product.getId());
        productMap.put(kName, Utils.getProperText(tvName));
        productMap.put(kProductDesc, Utils.getProperText(tvDescription));
        productMap.put(kSpecification, Utils.getProperText(edtSpecification));
        productMap.put(kProductPrice, Utils.getProperText(tvPrice));
        productMap.put(kProductSellPrice, Utils.getProperText(tvSalePrice));
        productMap.put(kQuantity, Utils.getProperText(tvQuantity));
        productMap.put(kDiscountTagLine, Utils.getProperText(tvDiscount));
        productMap.put(kAddressId, addressId);
        productMap.put(kProductCategoryId, categoryId);
        productMap.put(kProductSubCategoryId, subCategoryId);
        productMap.put(kProductType, productTypeId);
        productMap.put(kSKUNumber, Utils.getProperText(edtSkuNo));
        productMap.put(kProductStatus, productstatus);
        productMap.put(kNetPrice,Utils.getProperText(tvNetPrice));
        productMap.put(kTotalTax,Utils.getProperText(tvTaxRate));
        productMap.put(kProductBasicPrice,Utils.getProperText(tvBasePrice));
        mListener.submitAd(productMap);
      }
    } catch (Exception e) {
      e.printStackTrace();
      Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
    }
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {

    switch (requestCode) {
      case CATEGORY_RESULT:
        if (resultCode == Activity.RESULT_OK) {
          categoryId = data.getIntExtra("id", 0);
          categoryName = data.getStringExtra("data");
          selectSubCategory(categoryId);
        }
        break;
      case SUB_CATEGORY_RESULT:
        if (resultCode == Activity.RESULT_OK) {
          subCategoryId = data.getIntExtra("id", 0);
          tvCategoryName.setText(categoryName);
          tvSubCategoryName.setText(data.getStringExtra("data"));
        }
        break;
            /*case ADDRESS_RESULT:
                if (resultCode == Activity.RESULT_OK) {
                    address = (Address) data.getSerializableExtra("data");
                    tvAddress.setText(address.getCity().concat(" , ")
                            .concat(address.getState()).concat(" , ")
                            .concat(address.getCountry()));
                }
                break;*/
    }
  }

  private void showProgress(boolean b) {
    if (b) {
      progressDialog.show();
    } else {
      if (progressDialog != null) {
        progressDialog.cancel();
      }
    }
  }

  @Override
  public void onDetach() {
    super.onDetach();
    mListener = null;
  }
}
