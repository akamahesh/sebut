package com.app.inn.peepsin.UI.fragments.SellStuffModule;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static com.app.inn.peepsin.Constants.Constants.PRODUCT_RESULT;
import static com.app.inn.peepsin.Constants.Constants.kData;
import static com.app.inn.peepsin.Constants.Constants.kProductImageUrlList;
import static com.app.inn.peepsin.Constants.Constants.kRecords;
import static com.app.inn.peepsin.Constants.Constants.kShopId;
import static com.app.inn.peepsin.Constants.Constants.kType;
import static com.app.inn.peepsin.Constants.Constants.kUserId;
import static com.app.inn.peepsin.Services.PublishAdService.kProductUpdateNotification;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.app.inn.peepsin.Constants.Blocks.GenricResponse;
import com.app.inn.peepsin.Constants.Constants;
import com.app.inn.peepsin.Constants.Constants.ProductStatus;
import com.app.inn.peepsin.Managers.ModelManager.ModelManager;
import com.app.inn.peepsin.Models.BaseModel;
import com.app.inn.peepsin.Models.CurrentUser;
import com.app.inn.peepsin.Models.SellingProduct;
import com.app.inn.peepsin.Models.ShopCategory;
import com.app.inn.peepsin.R;
import com.app.inn.peepsin.Services.PublishAdService;
import com.app.inn.peepsin.UI.activities.AddProductActivity;
import com.app.inn.peepsin.UI.adapters.SellingStuffAdapter;
import com.app.inn.peepsin.UI.adapters.ShopInventoryAdapter;
import com.app.inn.peepsin.UI.fragments.Profile.ShoppingCartFragment;
import com.app.inn.peepsin.UI.helpers.DividerItemRecyclerDecoration;
import com.app.inn.peepsin.UI.helpers.Toaster;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.app.inn.peepsin.UI.interfaces.Switcher;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class SellingStuffFragment extends Fragment implements ShopInventoryAdapter.InventoryFilter {
  private final int REQUEST_PERMISSION_CAMERA_STORAGE = 101;

  private String TAG = SellingStuffFragment.class.getSimpleName();
  private static Switcher switcherListener;
  @BindView(R.id.recycler_view_feeds)
  RecyclerView recyclerViewFeeds;
  @BindView(R.id.tv_title)
  TextView tvTitle;
  @BindView(R.id.empty_view)
  View emptyView;
  @BindView(R.id.tv_empty_title)
  TextView tvEmptyTitle;
  @BindView(R.id.tv_empty_message)
  TextView tvEmptyMessage;
  @BindView(R.id.fab)
  FloatingActionButton fabAddStuff;
  @BindView(R.id.tv_cartCount)
  TextView tvCartCount;
  @BindView(R.id.btn_edit)
  ImageButton editMenu;

  @BindView(R.id.recycler_view_inventrory_item)
  RecyclerView recyclerViewInventory;
  @BindView(R.id.swipeRefreshLayout)
  SwipeRefreshLayout swipeRefreshLayout;

  @BindView(R.id.btn_clear_filter)
  Button btnClearFilter;
  private List<SellingProduct> productList;
  private List<SellingProduct> filterList;
  private List<ShopCategory> inventoryList;
  private ProgressDialog progressDialog;
  private SellingStuffAdapter stuffAdapter;
  private ShopInventoryAdapter inventoryAdapter;
  private int shopId;
  private Integer mUserId;

  SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
    @Override
    public void onRefresh() {
      refreshData(mUserId);
      loadShopCategory();
    }
  };

  /**
   * @param switcher listener to switch between fragments
   * @param userId userId for which selling stuff to be shown
   * @return @{@link SellingStuffFragment}
   */
  public static SellingStuffFragment newInstance(Switcher switcher, int shopId, int userId) {
    switcherListener = switcher;
    SellingStuffFragment fragment = new SellingStuffFragment();
    Bundle bundle = new Bundle();
    bundle.putInt(kUserId, userId);
    bundle.putInt(kShopId, shopId);
    fragment.setArguments(bundle);
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    progressDialog = Utils.generateProgressDialog(getContext(), true);
    Bundle bundle = getArguments();
    productList = new ArrayList<>();
    filterList = new ArrayList<>();
    inventoryList = new ArrayList<>();
    if (bundle != null) {
      mUserId = bundle.getInt(kUserId);
      shopId = bundle.getInt(kShopId);
    }
  }


  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_selling_stuff, container, false);
    ButterKnife.bind(this, view);
    tvTitle.setText(getString(R.string.title_product_catalogue));
    tvEmptyTitle.setText(getString(R.string.empty_title_no_selling_stuff));
    tvEmptyMessage.setText(getString(R.string.empty_message_no_selling_stuff));
    stuffAdapter = new SellingStuffAdapter(getContext(), filterList, switcherListener, 0, null);
    recyclerViewFeeds.setHasFixedSize(true);
    recyclerViewFeeds.setLayoutManager(new LinearLayoutManager(getActivity()));
    recyclerViewFeeds.setAdapter(stuffAdapter);
    recyclerViewFeeds.addItemDecoration(
        new DividerItemRecyclerDecoration(getActivity(), R.drawable.canvas_recycler_divider));
    swipeRefreshLayout.setOnRefreshListener(onRefreshListener);

    // Inventory Adapter and Inventory List
    inventoryAdapter = new ShopInventoryAdapter(getContext(), inventoryList, this);
    LinearLayoutManager HorizontalLayout = new LinearLayoutManager(getContext(),
        LinearLayoutManager.HORIZONTAL, false);
    recyclerViewInventory.setLayoutManager(HorizontalLayout);
    recyclerViewInventory.setAdapter(inventoryAdapter);
    return view;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    loadData(mUserId);
    loadShopCategory();
  }

  @OnClick(R.id.btn_cart)
  void onCart() {
    if (switcherListener != null) {
      switcherListener
          .switchFragment(ShoppingCartFragment.newInstance(switcherListener), true, true);
    }
    //startActivity(ShoppingCartActivity.getIntent(getContext()));
  }


  @OnClick(R.id.btn_edit)
  void onClick() {
    //creating a popup menu
    PopupMenu popup = new PopupMenu(getContext(), editMenu);
    //inflating menu from xml resource
    popup.inflate(R.menu.menu_product);
    //adding click listener
    popup.setOnMenuItemClickListener(item -> {
      //(1 publish, 2 Active, 3 InActive,4 waiting for approval, 6 Draft)
      switch (item.getItemId()) {
        case R.id.action_publish:
          loadUpdatedData(ProductStatus.draft);
          break;
        case R.id.action_active:
          loadUpdatedData(Constants.ProductStatus.inActive);
          break;
        case R.id.action_inactive:
          loadUpdatedData(Constants.ProductStatus.active);
          break;
      }
      return false;
    });
    //displaying the popup
    popup.show();
  }

  @OnClick(R.id.btn_clear_filter)
  void onClearFilter() {
    stuffAdapter.addNewItems(productList);
    checkEmptyScreen();
    for (ShopCategory shopCategory : inventoryList) {
      shopCategory.setSelected(false);
    }
    inventoryAdapter.addItems(inventoryList);
    btnClearFilter.setVisibility(View.GONE);
  }

  private void refreshData(Integer userId) {
    HashMap<String, Object> parameters = new HashMap<>();
    parameters.put(BaseModel.kUserId, userId);
    ModelManager.modelManager().getSellingProducts(parameters,
        (Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<SellingProduct>> genericResponse) -> {
          productList = genericResponse.getObject();
          filterList.clear();
          filterList.addAll(genericResponse.getObject());
          stuffAdapter.notifyDataSetChanged();
          stuffAdapter.notifyDataSetChanged();
          checkEmptyScreen();
          swipeRefreshLayout.setRefreshing(false);
        }, (Constants.Status iStatus, String message) -> {
          Toaster.toast(message);
          swipeRefreshLayout.setRefreshing(false);
          checkEmptyScreen();
        });
  }

  @OnClick(R.id.btn_back)
  void onBack() {
    getFragmentManager().popBackStack();
  }


  @OnClick(R.id.fab)
  void addSellingStuff() {
    checkPermission();
  }

  void addProduct() {
    startActivityForResult(AddProductActivity.getIntent(getContext()), PRODUCT_RESULT);
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (requestCode == PRODUCT_RESULT && resultCode == Activity.RESULT_OK) {
      HashMap<String, Object> productMap = (HashMap<String, Object>) data.getSerializableExtra(kData);
      List<String> imagePathList = (List<String>) data.getSerializableExtra(kProductImageUrlList);
      //addTaxes
      publishAd(productMap, imagePathList);
      loadData(mUserId);
    }
  }

  private BroadcastReceiver receiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      Bundle bundle = intent.getExtras();
      SellingProduct sellingProduct = (SellingProduct) bundle.getSerializable(kRecords);
      productList.add(sellingProduct);
      stuffAdapter.notifyItemInserted(stuffAdapter.getItemCount());
      onRefreshListener.onRefresh();
    }
  };


  @Override
  public void onResume() {
    super.onResume();
    getActivity().registerReceiver(receiver, new IntentFilter(kProductUpdateNotification));
    CurrentUser currentUser = ModelManager.modelManager().getCurrentUser();
    setCartUpdate(currentUser.getShoppingCartCount());
    //appbarLayout.setVisibility(View.GONE);

  }

  public void setCartUpdate(int count) {
    if (count == 0) {
      tvCartCount.setVisibility(View.GONE);
    } else {
      tvCartCount.setVisibility(View.VISIBLE);
      tvCartCount.setText(String.valueOf(count));
    }
  }

  @Override
  public void onPause() {
    super.onPause();
    getActivity().unregisterReceiver(receiver);
  }


  private void publishAd(HashMap<String, Object> productMap, List<String> imagePathList) {
    Intent intent = new Intent(getContext(), PublishAdService.class);
    // add infos for the service which file to download and where to store
    intent.putExtra(kRecords, productMap);
    intent.putExtra(kProductImageUrlList, (Serializable) imagePathList);
    getActivity().startService(intent);
  }


  private void loadData(int userId) {
    swipeRefreshLayout.setRefreshing(true);
    HashMap<String, Object> parameters = new HashMap<>();
    parameters.put(BaseModel.kUserId, userId);
    ModelManager.modelManager().getSellingProducts(parameters,
        (Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<SellingProduct>> genericResponse) -> {
          productList = genericResponse.getObject();
          filterList.clear();
          filterList.addAll(genericResponse.getObject());
          stuffAdapter.notifyDataSetChanged();
          swipeRefreshLayout.setRefreshing(false);
          checkEmptyScreen();
        }, (Constants.Status iStatus, String message) -> {
          Toaster.toast(message);
          swipeRefreshLayout.setRefreshing(false);
          checkEmptyScreen();
        });
  }


  public void loadShopCategory() {
    ModelManager.modelManager().getShopCategoryListSeller(shopId,
        (Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<ShopCategory>> genericResponse) -> {
          inventoryList = genericResponse.getObject();
          inventoryAdapter.addItems(inventoryList);
        }, (Constants.Status iStatus, String message) -> {
          Log.e(TAG, message);
          Toaster.toast(message);
        });
  }

  private void loadUpdatedData(Constants.ProductStatus productStatus) {
    showProgress(true);
    HashMap<String, Object> parameters = new HashMap<>();
    parameters.put(BaseModel.kUserId, mUserId);
    parameters.put(BaseModel.kProductStatus, productStatus.getValue());
    ModelManager.modelManager().getSellingProducts(parameters,
        (Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<SellingProduct>> genericResponse) -> {
          CopyOnWriteArrayList<SellingProduct> productList = genericResponse.getObject();
          if (switcherListener != null) {
            switcherListener.switchFragment(ModifySellingStuffFragment
                .newInstance(shopId, productList, productStatus.getValue()), true, true);
          }
          showProgress(false);
        }, (Constants.Status iStatus, String message) -> {
          Toaster.toast(message);
          showProgress(false);
        });
  }

  private void checkEmptyScreen() {
    if (stuffAdapter.getItemCount() > 0) {
      emptyView.setVisibility(View.GONE);
    } else {
      emptyView.setVisibility(View.VISIBLE);
    }
  }

  private void showProgress(boolean b) {
    if (b) {
      progressDialog.show();
    } else {
      if (progressDialog != null) {
        progressDialog.cancel();
      }
    }
  }

  @Override
  public void filter(Integer id) {
    showProgress(true);
    filterList.clear();
    int size = productList.size();
    for (SellingProduct sellingP : productList) {
      if (sellingP.getCategoryId().equals(id)) {
        filterList.add(sellingP);
      }
    }
    int sizeIn = filterList.size();
    stuffAdapter.notifyDataSetChanged();
    checkEmptyScreen();
    showProgress(false);
    if (inventoryAdapter.getItemCount() > 1) {
      btnClearFilter.setVisibility(View.VISIBLE);
    }
  }


  private void checkPermission() {
    if (ContextCompat.checkSelfPermission(getActivity(), CAMERA) == PackageManager.PERMISSION_GRANTED
            && ContextCompat.checkSelfPermission(getActivity(), READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
      addProduct();
    } else if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), CAMERA) && ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), READ_EXTERNAL_STORAGE)) {
      // We've been denied once before. Explain why we need the permission, then ask again.
      Utils.showDialog(getContext(), "Camera & Gallary permissions are required to upload profile images!", "Ask Permission", "Discard", (dialog, which) -> {
        if (which == -1)
          requestPermissions(new String[]{CAMERA, READ_EXTERNAL_STORAGE}, REQUEST_PERMISSION_CAMERA_STORAGE);
        else
          dialog.dismiss();
      });
    } else {
      // We've never asked. Just do it.
      requestPermissions(new String[]{CAMERA, READ_EXTERNAL_STORAGE}, REQUEST_PERMISSION_CAMERA_STORAGE);
    }
  }


  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    if (requestCode == REQUEST_PERMISSION_CAMERA_STORAGE && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
      addProduct();
    } else {
      // We were not granted permission this time, so don't try to show the contact picker
      super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
  }


}
