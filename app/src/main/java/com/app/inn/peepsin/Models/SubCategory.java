package com.app.inn.peepsin.Models;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by root on 19/5/17.
 */

public class SubCategory extends BaseModel implements Serializable{

    private Integer id;
    private String name;

    public SubCategory(JSONObject jsonResponse) {
        this.id   = getValue(jsonResponse, kSubCategoryId, Integer.class);
        this.name = getValue(jsonResponse, kSubCategory, String.class);
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
