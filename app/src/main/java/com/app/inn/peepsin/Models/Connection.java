package com.app.inn.peepsin.Models;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by akamahesh on 15/5/17.
 */

public class Connection extends BaseModel implements Serializable {
    private final String TAG = getClass().getSimpleName();
    private Integer userId;
    private Integer shopId;
    private String jabberId;
    private String firstName;
    private String lastName;
    private SocialLink email;
    private SocialLink phone;
    private Integer gender;
    private String dateOfBirth;
    private String shopName;
    private String joinedDate;
    private String profilePicURL;
    private Integer sellingProductCount;
    private Integer friendsCount;
    private List<SocialLink> userSocialLinks;
    private Address primaryAddress;
    private Integer connectionLevel;
    private Integer rating;
    private Integer friendsCountSecondLevel;
    private Integer friendsCountThirdLevel;
    private Boolean isBlocked;
    private Integer profileVisibility;
    private Integer userConnectionStatus;
    private boolean isSelected = false;
    private Integer isUserBlocked;



    public Connection(JSONObject jsonResponse) {
        this.userId                 = getValue(jsonResponse, kUserId,               Integer.class);
        try{
            this.shopId                 = getValue(jsonResponse, kShopId,               Integer.class);
            this.rating                 = getValue(jsonResponse, kRating,               Integer.class);
        }catch(Exception e){
            e.printStackTrace();
        }
        this.jabberId               = getValue(jsonResponse, kJabberId,             String.class);
        this.firstName              = getValue(jsonResponse, kFirstName,            String.class);
        this.lastName               = getValue(jsonResponse, kLastName,             String.class);
        this.shopName               = getValue(jsonResponse, kShopName,             String.class);
        try {
            this.email                  = new SocialLink(getValue(jsonResponse, kEmail, JSONObject.class));
            this.phone                  = new SocialLink(getValue(jsonResponse, kPhone, JSONObject.class));
        }catch (Exception e){
            e.printStackTrace();
        }

        this.dateOfBirth            = getValue(jsonResponse, kDateOfBirth,          String.class);
        this.gender                 = getValue(jsonResponse, kGender,Integer.class);
        this.profilePicURL          = getValue(jsonResponse, kProfilePicUrl,           String.class);
        this.joinedDate             = getValue(jsonResponse, kJoinedDate,           String.class);
        this.sellingProductCount    = getValue(jsonResponse, kSellingProductCount,  Integer.class);
        this.friendsCount           = getValue(jsonResponse, kFriendsCount,         Integer.class);
        this.connectionLevel        = getValue(jsonResponse, kConnectionLevel,      Integer.class);
        this.friendsCountSecondLevel        = getValue(jsonResponse, kFriendsCountSecondLevel,      Integer.class);
        this.friendsCountThirdLevel        = getValue(jsonResponse, kFriendsCountThirdLevel,      Integer.class);
        this.isBlocked              = getValue(jsonResponse, kIsUserBlocked,        Boolean.class);
        this.userConnectionStatus   = getValue(jsonResponse, kUserConnectionStatus, Integer.class);
        this.isUserBlocked             = getValue(jsonResponse, kIsUserBlocked,        Integer.class);

        try {
            this.profileVisibility  = getValue(jsonResponse, kProfileVisibility,  Integer.class);
        }catch (Exception e){
            Log.e(TAG,e.getMessage() + "on visibility");
        }

        try {
            this.primaryAddress = new Address(getValue(jsonResponse, kPrimaryAddress, JSONObject.class));
        } catch (Exception e) {
            Log.e(TAG,e.getMessage() + "on primary address");
        }

        try {
            this.userSocialLinks = handleSocialLinks(getValue(jsonResponse, kUserSocialLinks, JSONArray.class));
        } catch (Exception e) {
            Log.e(TAG,e.getMessage() + "on userSocialLinks");
        }

    }

    private List<SocialLink> handleSocialLinks(JSONArray jsonArray) {
        List<SocialLink> userSocialLinks = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                userSocialLinks.add(new SocialLink(jsonArray.getJSONObject(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return userSocialLinks;
    }

    public Integer getShopId() {
        return shopId;
    }

    public String getJabberId() {
        return jabberId;
    }

    public Integer getConnectionLevel() {
        return connectionLevel;
    }

    public boolean getIsBlocked() {
        return isBlocked;
    }

    public void setIsBlocked(boolean isBlocked) {
        this.isBlocked = isBlocked;
    }

    public Boolean getBlocked() {
        return isBlocked;
    }

    public Integer getUserConnectionStatus() {
        return userConnectionStatus;
    }

    public void setUserConnectionStatus(Integer userConnectionStatus) {
        this.userConnectionStatus = userConnectionStatus;
    }

    public Integer getProfileVisibility() {
        return profileVisibility;
    }

    /**
     * @return fullname = Firstname+lastName
     */
    public String getFullName() {
        return firstName + " " + lastName;
    }

    public Integer getUserId() {
        return userId;
    }

    public Integer getRating() {
        return rating;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public SocialLink getEmail() {
        return email;
    }

    public SocialLink getPhone() {
        return phone;
    }

    public Integer getGender() {
        return gender;
    }

    public String getShopName() {
        return shopName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public String getProfilePicURL() {
        return profilePicURL;
    }

    public String getJoinedDate() {
        return joinedDate;
    }

    public Integer getSellingProductCount() {
        return sellingProductCount;
    }

    public Integer getFriendsCount() {
        return friendsCount;
    }

    public Address getPrimaryAddress() {
        return primaryAddress;
    }

    public Integer getFriendsCountSecondLevel() {
        return friendsCountSecondLevel;
    }

    public Integer getFriendsCountThirdLevel() {
        return friendsCountThirdLevel;
    }

    public List<SocialLink> getUserSocialLinks() {
        return userSocialLinks;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        this.isSelected = selected;
    }

    public Integer getUserBlocked() {
        return isUserBlocked;
    }
}
