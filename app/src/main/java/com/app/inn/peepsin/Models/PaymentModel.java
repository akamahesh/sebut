package com.app.inn.peepsin.Models;

/**
 * Created by Harsh on 7/3/2017.
 */

public class PaymentModel {


    private String name;

    public PaymentModel(String name) {

        this.name = name;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
