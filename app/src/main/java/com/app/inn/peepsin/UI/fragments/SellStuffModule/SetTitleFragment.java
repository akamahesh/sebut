package com.app.inn.peepsin.UI.fragments.SellStuffModule;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.inn.peepsin.R;
import com.app.inn.peepsin.UI.helpers.Utils;
import com.squareup.picasso.Picasso;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnEditorAction;

import static com.app.inn.peepsin.Constants.Constants.kProduct;
import static com.app.inn.peepsin.Constants.Constants.kProductRef;

/**
 * Created by root on 24/5/17.
 */

public class SetTitleFragment extends Fragment {

    @BindView(R.id.tv_message)TextView tvMessage;
    @BindView(R.id.tv_title)TextView tvTitle;
    @BindView(R.id.tv_price)TextView tvPrice;
    @BindView(R.id.edt_price)
    EditText edtProductName;
    @BindView(R.id.iv_product_image)
    ImageView ivProduct;

    private Context context;
    private FragmentInteractionListener mListener;
    private String imagePath;



    public interface FragmentInteractionListener{
        void productNext(String name);
    }

    public static Fragment newInstance(String path){
        Fragment fragment =  new SetTitleFragment();
        Bundle bdl = new Bundle();
        bdl.putString(kProduct,path);
        fragment.setArguments(bdl);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if(bundle!= null){
            imagePath = bundle.getString(kProduct);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        if (context instanceof FragmentInteractionListener) {
            mListener = (FragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement FragmentInteractionListener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_set_price, container, false);
        ButterKnife.bind(this,view);
        tvTitle.setText(getString(R.string.set_title));
        tvMessage.setText(getString(R.string.product_description_product_name));
        tvPrice.setVisibility(View.GONE);
        edtProductName.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        setImageBackGround(imagePath);

        return view;
    }


    public void setImageBackGround(String imagePath) {
        try {
            File imgFile = new  File(imagePath);
            if(imgFile.exists()){
                Picasso.with(context)
                        .load(imgFile)
                        .fit()
                        .into(ivProduct);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @OnEditorAction(R.id.edt_price)
    boolean submit(){
        next();
        return true;
    }

    @OnClick(R.id.btn_next)
    void next(){
        Utils.hideKeyboard(getContext());
        mListener.productNext(edtProductName.getText().toString());
    }

    @OnClick(R.id.btn_cancel)
    void cancel(){
        Utils.hideKeyboard(getContext());
        getActivity().finish();
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener=null;
    }
}
