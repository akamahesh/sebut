#Project-architecture
    -activities
    -adapters
    -authenticator
    -data
        -migration
    -fragments
    -helper
    -interfaces
    -models
    -preferences
    -sync




    com.innverse.xopa

        activities
            Contains all the activities. Classes are all named with Activity at the end. That way, you can immediately know what it is when reading Java code that doesn't have its full package name.

        adapters
            Contains all the adapters.

        authenticator
            Contains any class related to signing a user in. I create a local account and having all related classes together is very handy.

        data
            Contains all classes related to data management such as ContentProvider and SQLiteHelper.

        data.migrations
            Contains all of my SQLite migrations. I created a class for migrations, read about it here, and put them all in this package.

        fragments
            Contains all fragments.

        helpers
            Contains helper classes. A helper class is a place to put code that is used in more than one place. I have a DateHelper for instance. Most of the methods are static.

        interfaces
            Contains all interfaces.

        models
            Contains all local models. When syncing from an HTTP API I parse the JSON into these Java objects using Jackson. I also pull Cursor rows into these models as well.

        preferences
            Contains all classes for custom preferences. When creating the preferences I required a custom PreferenceDialog as well as a custom PreferenceCategory. They live here.

        sync
            Contains all classes related to syncing. I use a SyncAdapter to pull data from an HTTP API. In addition to the SyncAdapter a SyncService is required, so I created a package.


#Layouts
    -activity_
    -adapter_
    -fragment_

#IDs
    -all should be in snake case eg. R.layout.snake_case




#ePazer Innverse Project
    -